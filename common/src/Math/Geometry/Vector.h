/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	MATH_GEOMETRY_VECTOR_H
#define MATH_GEOMETRY_VECTOR_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/General.h>

////////////////////////////////////////////////////////////
// Macro declarations
//	Some tricky methods to simplify the coding
//	todo: Simplify functions with the same procedure
////////////////////////////////////////////////////////////
#define VectorName(Dimension, TypeName) Vector ## Dimension ## TypeName

#define VectorComponents2(Type) struct { Type x, y; };
#define VectorComponents3(Type) struct { Type x, y, z; };
#define VectorComponents4(Type) struct { Type x, y, z, w; };

#define VectorComp(Dimension, Type) VectorComponents ## Dimension(Type)

#define VectorArray(Dimension, Type) Type v[Dimension];

#define VectorDefine(Dimension, Type, TypeName) struct VectorName(Dimension, TypeName) {	\
                                                    union {                                 \
                                                        VectorComp(Dimension, Type)         \
                                                        VectorArray(Dimension, Type)        \
                                                    };                                      \
                                                }

#define VectorDefineDim(Type, TypeName) VectorDefine(2, Type, TypeName);	\
										VectorDefine(3, Type, TypeName);	\
										VectorDefine(4, Type, TypeName)


#define VectorDeclare(Dimension, TypeName, Name) struct Vector ## Dimension ## Typename Name

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define VECTOR_COORD_X		0x00
#define VECTOR_COORD_Y		0x01
#define VECTOR_COORD_Z		0x02
#define VECTOR_COORD_W		0x03

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////

// Define all floating vectors
VectorDefineDim(float, f);

// Define all double precision floating vectors
VectorDefineDim(double, d);

// Define unsigned integer vectors
VectorDefine(2, uinteger, ui);
VectorDefine(3, uinteger, ui);

// Define integer vectors
VectorDefine(2, integer, i);
VectorDefine(3, integer, i);

// Define unsigned byte vectors
VectorDefine(2, uint8, ub);
VectorDefine(3, uint8, ub);
VectorDefine(4, uint8, ub);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

void Vector2fCopy(struct Vector2f * Dest, struct Vector2f * Src);

void Vector2fSet(struct Vector2f * Vector, float x, float y);

void Vector2iSet(struct Vector2i * Vector, integer x, integer y); // todo: see here, duplicated code

void Vector2iSetNull(struct Vector2i * Vector); // todo: see here, duplicated code

struct Vector2f * Vector2fSetImpl(struct Vector2f * Vector, float x, float y);

void Vector2fSetNull(struct Vector2f * Vector);

void Vector3fIndentity(struct Vector3f * Vector);

void Vector3fAxisX(struct Vector3f * Vector);

void Vector3fAxisY(struct Vector3f * Vector);

void Vector3fAxisZ(struct Vector3f * Vector);

void Vector3fCopy(struct Vector3f * Dest, struct Vector3f * Src);

void Vector3fSetNull(struct Vector3f * Vector);

bool Vector3fIsNull(struct Vector3f * Vector);

bool Vector3fIsEqual(struct Vector3f * Left, struct Vector3f * Right);

void Vector3fSet(struct Vector3f * Vector, float x, float y, float z);

void Vector3iSet(struct Vector3i * Vector, integer x, integer y, integer z); // todo: see here, duplicated code

struct Vector3f * Vector3fSetImpl(struct Vector3f * Vector, float x, float y, float z);

void Vector3fAdd(struct Vector3f * Dest, struct Vector3f * Src);

void Vector3fAddEx(struct Vector3f * Dest, struct Vector3f * Left, struct Vector3f * Right);

void Vector3fSubtract(struct Vector3f * Dest, struct Vector3f * Src);

void Vector3fSubtractEx(struct Vector3f * Dest, struct Vector3f * Left, struct Vector3f * Right);

void Vector3fIncrement(struct Vector3f * Vector, float x, float y, float z);

void Vector3fDecrement(struct Vector3f * Vector, float x, float y, float z);

void Vector3fMultiply(struct Vector3f * Vector, float Scale);

void Vector3fMultiplyEx(struct Vector3f * Dest, struct Vector3f * Source, float Scale);

void Vector3fMultiplyVectorEx(struct Vector3f * Dest, struct Vector3f * Source, struct Vector3f * Scale);

void Vector3fDivide(struct Vector3f * Vector, float Scale);

void Vector3fDivideVectorEx(struct Vector3f * Dest, struct Vector3f * Source, struct Vector3f * Scale);

void Vector3fOpposite(struct Vector3f * Dest, struct Vector3f * Src);

float Vector3fLength(struct Vector3f * Vector);

float Vector3fLengthSquared(struct Vector3f * Vector);

void Vector3fNormalize(struct Vector3f * Vector);

void Vector3fRotate(struct Vector3f * Vector, float Degres, uinteger Coord);

float Vector3fDot(struct Vector3f * v1, struct Vector3f * v2);

void Vector3fCross(struct Vector3f * v1, struct Vector3f * v2, struct Vector3f * Result);

void Vector3fToArray(struct Vector3f * Vector, float * Array);

float Vector3fDistance(struct Vector3f * v1, struct Vector3f * v2);

void Vector3fGetDimensions(struct Vector3f * Vertices, float VertsSize, struct Vector3f * Center, float * Size);

void Vector3fLerp(struct Vector3f * Dest, struct Vector3f * Src, float Factor);

void Vector3fLerpEx(struct Vector3f * Left, struct Vector3f * Right, float Factor, struct Vector3f * Dest);

float Vector3fGetPhi(struct Vector3f * Vector);

float Vector3fGetTheta(struct Vector3f * Vector);

void Vector3fSetPhi(struct Vector3f * Vector, float Phi);

void Vector3fSetTheta(struct Vector3f * Vector, float Theta);

void Vector3fRotateSpherical(struct Vector3f * Vector, float ThetaOffset, float PhiOffset);

void Vector3fRotateAxis(struct Vector3f * Axis, struct Vector3f * Target, float Radians);

void Vector4fSet(struct Vector4f * Vector, float x, float y, float z, float w);

#endif // MATH_GEOMETRY_VECTOR_H
