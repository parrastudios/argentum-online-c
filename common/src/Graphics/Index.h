/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_INDEX_H
#define GRAPHICS_INDEX_H

////////////////////////////////////////////////////////////
/// Author comments:
///	This module manages all grh index, animations, and others.
///	The format it's a little bit HORRIBLE.. but i don't want
/// to make a new tools (like indexer), and new format.
/// It could be, in the near future.
/// Remember, we have one index more in all lists.
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct GrhData					///< Contains info about grh, size and animation
{
	int16 sX, sY;
	int32 FileNumber;
	int16 PixelWidth, PixelHeight;
	float TileWidth, TileHeight;
	uint16 FramesCounter;
	uint32 Frames[0xFF];
	float Speed;
};

struct Grh						///< Pointer to GrhData (animation)
{
	int16 Index;
	float FrameCounter, Speed;
	bool  Started;
	int16 Loops;
};

struct BodyData					///< List of bodies
{
	struct Grh Walk[4];
	int32 HeadOffsetX, HeadOffsetY;
};

struct BodyDataLoad				///< List of bodies used for load from ind
{
	int16 Body[4];
	int16 HeadOffsetX, HeadOffsetY;
};

struct FxData					///< List of fx
{
	int16 Animation, OffsetX, OffsetY;
};

///< Walk loader and data struct
struct WalkData { struct Grh Walk[4]; };
struct WalkLoadData { int16 Walk[4]; };

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize all data
////////////////////////////////////////////////////////////
bool IndexInitialize();

////////////////////////////////////////////////////////////
/// Sets up a grh to rendering
////////////////////////////////////////////////////////////
void IndexInitializeGrh(struct Grh * Data, int32 Index, int32 Started);

////////////////////////////////////////////////////////////
/// Get size of the rect
////////////////////////////////////////////////////////////
void IndexGetSize(int32 Index, int32 * Width, int32 * Height);

////////////////////////////////////////////////////////////
/// Get a grh from the list
////////////////////////////////////////////////////////////
struct GrhData * IndexGetGrh(uint32 Index);

#endif // GRAPHICS_INDEX_H
