/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_GENERAL_H
#define WORLD_GENERAL_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Build.h>

////////////////////////////////////////////////////////////
// World Headers
////////////////////////////////////////////////////////////
#include <World/Camera.h>

#include <World/Point.h>

#include <World/Light.h>
#include <World/LightManager.h>

#include <World/Model.h>
#include <World/ModelManager.h>

#include <World/Particle.h>
#include <World/ParticleEmitter.h>

#include <World/Patch.h>
#include <World/Terrain.h>
#include <World/TerrainLoader.h>
#include <World/TerrainManager.h>

#include <World/Entity.h>
#include <World/EntityManager.h>

#include <World/Octree.h>

#include <World/Scene.h>
#include <World/SceneLoader.h>
#include <World/SceneManager.h>

#endif // WORLD_GENERAL_H
