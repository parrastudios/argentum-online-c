/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATATYPE_SET_H
#define DATATYPE_SET_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Types.h>
#include <DataType/Operator.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define SET_HASH_INVALID	0xFFFFFFFFUL

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////

typedef uint32 SetHashType;
typedef SetHashType (*SetHashCallback)(void *);

typedef struct SetNodeType * SetNode;
typedef struct SetType * Set;
typedef struct SetIteratorType * SetIterator;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

Set SetCreate(uinteger TypeSize, SetHashCallback HashFunc, CompareCallback CompareFunc);

Set SetCreateHashOnly(uinteger TypeSize, SetHashCallback HashFunc);

void SetDestroy(Set SetData);

uinteger SetGetSize(Set SetData);

bool SetInsert(Set SetData, void * Value);

void SetRemoveByHash(Set SetData, SetHashType Hash);

void SetClear(Set SetData);

bool SetHasValue(Set SetData, void * Value);

void * SetGetValueByHash(Set SetData, SetHashType Hash);

void SetRehash(Set SetData);

bool SetIteratorBegin(Set SetData, SetIterator Iterator);

bool SetIteratorNext(SetIterator Iterator);

bool SetIteratorEnd(SetIterator Iterator);

void * SetIteratorValue(SetIterator Iterator);

#endif // DATATYPE_SET_H
