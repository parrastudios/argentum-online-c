/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/TerrainCollision.h>

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a collision component of a terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainCollisionCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Initialize terrain collision properties
////////////////////////////////////////////////////////////
struct EntityType * TerrainCollisionInitialize(struct EntityType * Entity /*, ... */);

////////////////////////////////////////////////////////////
/// Destroy a terrain collision component
////////////////////////////////////////////////////////////
void TerrainCollisionDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Check collision of a terrain collision component
////////////////////////////////////////////////////////////
bool TerrainCollisionCollideImpl(struct ComponentType * Collider, struct ComponentType * Target);

////////////////////////////////////////////////////////////
/// Implementation of terrain to box collision between entities
////////////////////////////////////////////////////////////
bool TerrainCollisionCollideImplBox(struct ComponentType * Collider, struct ComponentCollisionData * Target);

////////////////////////////////////////////////////////////
/// Implementation of terrain to sphere collision between entities
////////////////////////////////////////////////////////////
bool TerrainCollisionCollideImplSphere(struct ComponentType * Collider, struct ComponentCollisionData * Target);

////////////////////////////////////////////////////////////
/// Implementation of terrain to frustum collision between entities
////////////////////////////////////////////////////////////
bool TerrainCollisionCollideImplFrustum(struct ComponentType * Collider, struct ComponentCollisionData * Target);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the terrain collision implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentCollisionType * TerrainComponentCollisionAttr()
{
	static struct TerrainComponentCollisionType TerrainComponentCollisionAttrImpl = {
		TypeInit(TODO, NULL)
	};

	static const struct ComponentCollisionType TerrainComponentCollisionAttr = {
		TypeInit(Collide, &TerrainCollisionCollideImpl),
		ExprInit(DispatchImpl,
			ExprInit(Collide,
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_BOX, &TerrainCollisionCollideImplBox),
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_SPHERE, &TerrainCollisionCollideImplSphere),
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_FRUSTUM, &TerrainCollisionCollideImplFrustum)
				)
			),
		UnionInit(TypeImpl, Derived, (const void *)&TerrainComponentCollisionAttrImpl)

	};

	return (const struct ComponentCollisionType *)&TerrainComponentCollisionAttr;
}

////////////////////////////////////////////////////////////
/// Return the terrain collision implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * TerrainComponentCollisionImpl()
{
	static const struct ComponentImplType TerrainComponentCollisionTypeImpl = {
		ExprInit(Base, TypeInit(Create, &TerrainCollisionCreate), TypeInit(Destroy, &TerrainCollisionDestroy)),
		TypeInit(Type, COMPONENT_TYPE_COLLISION),
		UnionInit(Attributes, Collision, &TerrainComponentCollisionAttr)
	};

	return &TerrainComponentCollisionTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a collision component of a terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainCollisionCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Create transform component data reference
        struct ComponentType * Transform = EntityComponentTransform(Entity);
        struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

		// Create collision component data reference
        struct ComponentType * Collision = EntityComponentCollision(Entity);
		struct TerrainComponentCollisionData * CollisionData = ComponentDataDerived(Collision, struct TerrainComponentCollisionData);


        // ...


		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Initialize terrain collision properties
////////////////////////////////////////////////////////////
struct EntityType * TerrainCollisionInitialize(struct EntityType * Entity /*, ... */)
{
	if (Entity)
	{
		// Create transform component data reference
        struct ComponentType * Transform = EntityComponentTransform(Entity);
        struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

        // Create collision component data reference
        struct ComponentType * Collision = EntityComponentCollision(Entity);
		struct TerrainComponentCollisionData * CollisionData = ComponentDataDerived(Collision, struct TerrainComponentCollisionData);


		// ...


		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a terrain collision component
////////////////////////////////////////////////////////////
void TerrainCollisionDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get collision component
		struct ComponentType * Collision = EntityComponentCollision(Entity);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Check collision of a terrain collision component
////////////////////////////////////////////////////////////
bool TerrainCollisionCollideImpl(struct ComponentType * Collider, struct ComponentType * Target)
{
	if (Collider && Target)
	{
		struct ComponentCollisionType * TargetType = ComponentTypeCollision(Target);

		return TargetType->DispatchImpl.Collide[ComponentCollisionTypeGetImpl(Collider->DataImpl.Type)](Target, (struct ComponentCollisionData *)ComponentDataDerived(Collider, struct TerrainComponentCollisionData));
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Implementation of terrain to box collision between entities
////////////////////////////////////////////////////////////
bool TerrainCollisionCollideImplBox(struct ComponentType * Collider, struct ComponentCollisionData * Target)
{
	// todo

	return true;
}

////////////////////////////////////////////////////////////
/// Implementation of terrain to sphere collision between entities
////////////////////////////////////////////////////////////
bool TerrainCollisionCollideImplSphere(struct ComponentType * Collider, struct ComponentCollisionData * Target)
{
	// todo

	return true;
}

////////////////////////////////////////////////////////////
/// Implementation of terrain to frustum collision between entities
////////////////////////////////////////////////////////////
bool TerrainCollisionCollideImplFrustum(struct ComponentType * Collider, struct ComponentCollisionData * Target)
{
	// todo

	return true;
}

////////////////////////////////////////////////////////////
/// Return the terrain collision derived data size
////////////////////////////////////////////////////////////
uinteger TerrainComponentCollisionSize()
{
	return sizeof(struct TerrainComponentCollisionData);
}
