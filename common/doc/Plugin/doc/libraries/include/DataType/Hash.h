/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATATYPE_HASH_H
#define DATATYPE_HASH_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Types.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
typedef enum HashCallbackIdType
{
	HASH_CALLBACK_UINT32_DEFAULT =	0x00,
	HASH_CALLBACK_STR_SDBM =		0x01,
	HASH_CALLBACK_STR_DJB2 =		0x02,
	HASH_CALLBACK_STR_ADLER32 =		0x03,

	HASH_CALLBACK_SIZE
} HashCallbackId;

typedef uint32		HashData;
typedef void *		HashValueData;
typedef HashData	(*HashCallback)(HashValueData);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get default hash function
////////////////////////////////////////////////////////////
HashCallback HashGetCallbackDefault();

////////////////////////////////////////////////////////////
/// Get desired hash function
////////////////////////////////////////////////////////////
HashCallback HashGetCallback(HashCallbackId Id);

#endif // DATATYPE_HASH_H
