/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATABASE_STORED_PROCEDURE_H
#define DATABASE_STORED_PROCEDURE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/Vector.h>
#include <DataBase/Connector.h>
#include <DataBase/StoredProcedureArgument.h>

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct StoredProcedureImplType;
struct StoredProcedureType;

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef void (*StoredProcedureFetchCallback)(struct StoredProcedureArgumentBuilder *, uinteger);

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct StoredProcedureType
{
	char const *								Name;			//< Stored procedure name
	StoredProcedureArgumentList					Prototype;		//< Function prototype arguments (passed by reference)
	Vector										Result;			//< Function result set (passed by value)
	StoredProcedureFetchCallback				Fetch;			//< Fetch callback (executed on success)
	struct StoredProcedureImplType *			Impl;			//< Stored procedure technology implementation
};

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define StoredProcedurePrototypeBind(StoredProcedure, ...) \
	StoredProcedureArgumentSetBuildRef(StoredProcedurePrototype(StoredProcedure), __VA_ARGS__)

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
/// Create a procedure call to the database
/////////////////////////////////////////////////////////////
bool StoredProcedureCreate(const char * Name, const char * Prototype, const char * Body);

/////////////////////////////////////////////////////////////
/// Destroy a procedure call from the database
/////////////////////////////////////////////////////////////
bool StoredProcedureDestroy(const char * Name);

/////////////////////////////////////////////////////////////
/// Initialize and prepare a stored procedure call
/////////////////////////////////////////////////////////////
bool StoredProcedureInitialize(struct StoredProcedureType * StoredProcedure, const char * Name, uinteger ArgumentCount, StoredProcedureFetchCallback Fetch);

/////////////////////////////////////////////////////////////
/// Get stored procedure prototype
/////////////////////////////////////////////////////////////
StoredProcedureArgumentList StoredProcedurePrototype(struct StoredProcedureType * StoredProcedure);

/////////////////////////////////////////////////////////////
/// Clear stored procedure result set
/////////////////////////////////////////////////////////////
void StoredProcedureClear(struct StoredProcedureType * StoredProcedure);

/////////////////////////////////////////////////////////////
/// Execute stored procedure fetch callback
/////////////////////////////////////////////////////////////
void StoredProcedureFetch(struct StoredProcedureType * StoredProcedure);

/////////////////////////////////////////////////////////////
/// Binds parameters to a procedure call
/////////////////////////////////////////////////////////////
bool StoredProcedureBind(struct StoredProcedureType * StoredProcedure);

/////////////////////////////////////////////////////////////
/// Executes a stored procedure call to the database
/////////////////////////////////////////////////////////////
bool StoredProcedureCall(struct StoredProcedureType * StoredProcedure);

/////////////////////////////////////////////////////////////
/// Release a stored procedure call
/////////////////////////////////////////////////////////////
void StoredProcedureRelease(struct StoredProcedureType * StoredProcedure);

#endif // DATABASE_STORED_PROCEDURE_H
