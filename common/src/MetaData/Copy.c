/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/Copy.h>
#include <MetaData/Value.h>
#include <MetaData/Reference.h>
#include <MetaData/Parameter.h>
#include <MetaData/Function.h>
#include <MetaData/Object.h>

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef MetadataStorageIndexType (*MetadataResourceCopyFunc)(MetadataScopeType, MetadataStorageIndexType);

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
static const MetadataResourceCopyFunc MetadataCopy[METADATA_STORAGE_RESOURCE_SIZE] =
{
	ArrayInit(METADATA_STORAGE_RESOURCE_VALUE, &MetadataValueCopy),
	ArrayInit(METADATA_STORAGE_RESOURCE_REFERENCE, &MetadataReferenceCopy),
	ArrayInit(METADATA_STORAGE_RESOURCE_PARAMETER_LIST, &MetadataParameterListCopy),
	ArrayInit(METADATA_STORAGE_RESOURCE_FUNCTION, &MetadataFunctionCopy),
	ArrayInit(METADATA_STORAGE_RESOURCE_OBJECT, &MetadataObjectCopy)
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Copy a resource of a valid type
////////////////////////////////////////////////////////////
MetadataStorageIndexType MetadataCopyResource(MetadataScopeType Scope, MetadataStorageResourceType ResourceType, MetadataStorageIndexType Index)
{
	if (ResourceType != METADATA_STORAGE_RESOURCE_INVALID)
	{
		return MetadataCopy[ResourceType](Scope, Index);
	}
	
	return METADATA_STORAGE_INDEX_INVALID;
}
