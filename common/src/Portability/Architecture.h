/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PORTABILITY_ARCHITECTURE_H
#define PORTABILITY_ARCHITECTURE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Platform.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define ARCH_UNKNOWN				0x00
#define ARCH_ALPHA					0x01
#define ARCH_IA32					0x02
#define ARCH_IA64					0x03
#define ARCH_MIPS					0x04
#define ARCH_HPPA					0x05
#define ARCH_PPC					0x06
#define ARCH_POWER					0x07
#define ARCH_SPARC					0x08
#define ARCH_AMD64					0x09
#define ARCH_ARM					0x0A
#define ARCH_M68K					0x0B
#define ARCH_S390					0x0C
#define ARCH_SH						0x0D
#define ARCH_NIOS2					0x0E

#define ARCHITECTURE_32				0x00
#define ARCHITECTURE_64				0x01

////////////////////////////////////////////////////////////
// Detection
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Identify the architecture type and family
////////////////////////////////////////////////////////////
#if defined(alpha) || defined(__ALPHA) || defined(__alpha) || defined(__alpha__) || defined(_M_ALPHA)
#	define ARCH_TYPE ARCH_ALPHA
#	define ARCH_FAMILY ARCHITECTURE_64
#elif defined(__386__) || defined(i386) || defined(__i386) || defined(__i386__) || defined(__X86) || defined(_M_IX86)
#	define ARCH_TYPE ARCH_IA32
#	define ARCH_FAMILY ARCHITECTURE_32
#elif defined(_IA64) || defined(__IA64__) || defined(__ia64__) || defined(__ia64) || defined(_M_IA64)
#	define ARCH_TYPE ARCH_IA64
#	define ARCH_FAMILY ARCHITECTURE_64
#elif defined(__x86_64__) || defined(_M_X64) || defined(_M_AMD64) || defined(__amd64)
#	define ARCH_TYPE ARCH_AMD64
#	define ARCH_FAMILY ARCHITECTURE_64
#elif defined(__mips__) || defined(__mips) || defined(__MIPS__) || defined(_M_MRX000)
#	define ARCH_TYPE ARCH_MIPS
#	define ARCH_FAMILY ARCHITECTURE_32
#elif defined(__hppa) || defined(__hppa__)
#	define ARCH_TYPE ARCH_HPPA
#	define ARCH_FAMILY ARCHITECTURE_32
#elif defined(__PPC) || defined(__POWERPC__) || defined(__powerpc) || defined(__powerpc64__) || defined(__PPC__) || \
      defined(__powerpc__) || defined(__ppc__) || defined(__ppc) || defined(_ARCH_PPC) || defined(_M_PPC)
#	define ARCH_TYPE ARCH_PPC
#	if defined(__powerpc64__)
#		define ARCH_FAMILY ARCHITECTURE_64
#	else
#		define ARCH_FAMILY ARCHITECTURE_32
#	endif
#elif defined(_POWER) || defined(_ARCH_PWR) || defined(_ARCH_PWR2) || defined(_ARCH_PWR3) || \
      defined(_ARCH_PWR4) || defined(__THW_RS6000)
#	define ARCH_TYPE ARCH_POWER
#	if defined(_ARCH_PPC64)
#		define ARCH_FAMILY ARCHITECTURE_64
#	elif defined(_ARCH_COM) && defined(_ARCH_PPC)
#		define ARCH_FAMILY ARCHITECTURE_32
#	endif
#elif defined(__sparc__) || defined(__sparc) || defined(sparc)
#	define ARCH_TYPE ARCH_SPARC
#	if defined(__arch64__) || defined(__sparcv9) || defined(__sparc_v9__)
#		define ARCH_FAMILY ARCHITECTURE_64
#	else
#		define ARCH_FAMILY ARCHITECTURE_32
#	endif
#elif defined(__arm__) || defined(__arm) || defined(ARM) || defined(_ARM) || defined(_ARM_) || defined(__ARM__) || defined(_M_ARM)
#	define ARCH_TYPE ARCH_ARM
#	define ARCH_FAMILY ARCHITECTURE_32
#elif defined(__m68k__) || defined(mc68000) || defined(_M_M68K)
#	define ARCH_TYPE ARCH_M68K
#	define ARCH_FAMILY ARCHITECTURE_32
#elif defined(__s390__) || defined(__s390x__)
#	define ARCH_TYPE ARCH_S390
#	define ARCH_FAMILY ARCHITECTURE_64
#elif defined(__sh__) || defined(__sh) || defined(SHx) || defined(_SHX_)
#	define ARCH_TYPE ARCH_SH
#	define ARCH_FAMILY ARCHITECTURE_32
#elif defined(nios2) || defined(__nios2) || defined(__nios2__)
#	define ARCH_TYPE ARCH_NIOS2
#	define ARCH_FAMILY ARCHITECTURE_32
#endif

#if !(defined(ARCH_TYPE) && ARCH_TYPE != ARCH_UNKNOWN && defined(ARCH_FAMILY))
#	error Unknown hardware architecture
#endif

#endif // PORTABILITY_ARCHITECTURE_H
