/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_CONDITION_H
#define SYSTEM_CONDITION_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <System/Mutex.h>

#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
#	define WIN32_LEAN_AND_MEAN
#	include <windows.h>
#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_MACOS || PLATFORM_TYPE == PLATFORM_BSD
#	include <pthread.h>
#endif

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
#	if (_WIN32_WINNT < 0x0600)
		typedef struct {
			Mutex	MutexData;
			uint32	Count;
			uint32	Size;
			HANDLE*	Handles;
		} Condition;
#	else
		// Vista+ condition implementation
		typedef CONDITION_VARIABLE Condition;
#	endif
#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_MACOS || PLATFORM_TYPE == PLATFORM_BSD
	typedef pthread_cond_t Condition;
#endif

////////////////////////////////////////////////////////////
/// Initialize a condition variable
////////////////////////////////////////////////////////////
void ConditionInitialize(Condition * ConditionData);

////////////////////////////////////////////////////////////
/// Destroy a condition variable
////////////////////////////////////////////////////////////
void ConditionDestroy(Condition * ConditionData);

////////////////////////////////////////////////////////////
/// Unblock a thread
////////////////////////////////////////////////////////////
void ConditionSignal(Condition * ConditionData);

////////////////////////////////////////////////////////////
/// Unblock all threads
////////////////////////////////////////////////////////////
void ConditionBroadcast(Condition * ConditionData);

////////////////////////////////////////////////////////////
/// Block on a condition variable
////////////////////////////////////////////////////////////
void ConditionWait(Condition * ConditionData, Mutex * Spinlock);

////////////////////////////////////////////////////////////
/// Block on a condition variable (with timeout : milliseconds)
////////////////////////////////////////////////////////////
void ConditionTimedWait(Condition * ConditionData, Mutex * Spinlock, float TimeOut);

#endif // SYSTEM_CONDITION_H
