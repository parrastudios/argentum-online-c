/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Memory/General.h>
#include <DataType/QueueFixed.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

void QueueFixedClearType(struct QueueFixedBase * Queue)
{
	Queue->Current = Queue->Count = 0;
}

void * QueueFixedGetHeadType(struct QueueFixedBase * Queue, uinteger TypeSize)
{
	return (void*)((uinteger)(&Queue->Buffer[0]) + (Queue->Current * TypeSize));
}

void * QueueFixedGetTailType(struct QueueFixedBase * Queue, uinteger Size, uinteger TypeSize)
{
	return (void*)((uinteger)(&Queue->Buffer[0]) + (((Queue->Current + Queue->Count) % Size) * TypeSize));
}

bool QueueFixedEmptyType(struct QueueFixedBase * Queue)
{
	return (Queue->Count == 0);
}

bool QueueFixedFullType(struct QueueFixedBase * Queue, uinteger Size)
{
	return (Queue->Count == Size);
}

void QueueFixedPushType(struct QueueFixedBase * Queue, void * Element, uinteger Size, uinteger TypeSize)
{
	if (Queue->Count < Size)
	{
		void * Dest = QueueFixedGetTailType(Queue, Size, TypeSize);

		// Copy into vector
		MemoryCopy(Dest, Element, TypeSize);

		// Increment current size
		++Queue->Count;
	}
}

void QueueFixedPopType(struct QueueFixedBase * Queue, uinteger Size)
{
	if (Queue->Count > 0)
	{
		--Queue->Count;

		++Queue->Current;

		Queue->Current %= Size;
	}
}

void * QueueFixedIteratorNextType(struct QueueFixedBase * Queue, void * Iterator, uinteger Size, uinteger TypeSize)
{
	void * IteratorNext = (void*)((uinteger)(Iterator)+TypeSize);

	if ((uinteger)(IteratorNext) < (uinteger)(&Queue->Buffer[0]) + (Size * TypeSize))
	{
		return IteratorNext;
	}
	else
	{
		return (void*)&Queue->Buffer[0];
	}
}

void * QueueFixedIteratorPrevType(struct QueueFixedBase * Queue, void * Iterator, uinteger Size, uinteger TypeSize)
{
	void * IteratorPrev = (void*)((uinteger)(Iterator)-TypeSize);

	if ((uinteger)(IteratorPrev) < (uinteger)(&Queue->Buffer[0]))
	{
		return (void*)((uinteger)(&Queue->Buffer[0]) + (Size * TypeSize));
	}
	else
	{
		return IteratorPrev;
	}
}
