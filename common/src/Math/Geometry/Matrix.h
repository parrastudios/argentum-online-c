/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	MATH_GEOMETRY_MATRIX_H
#define MATH_GEOMETRY_MATRIX_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Vector.h>
#include <Math/Geometry/Quaternion.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define MATRIX_AXIS_X	0x00
#define MATRIX_AXIS_Y	0x01
#define MATRIX_AXIS_Z	0x02

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct Quaternion;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct Matrix3f
{
	union
	{
		float m[3][3];
		float v[9];
		struct
		{
		    float _11, _12, _13;
		    float _21, _22, _23;
		    float _31, _32, _33;
		};
	};
};

struct Matrix34f
{
	union
	{
		float m[3][4];
		float v[12];
		struct
		{
		    float _11, _12, _13, _14;
		    float _21, _22, _23, _24;
		    float _31, _32, _33, _34;
		};
	};
};

struct Matrix4f
{
	union
	{
		float m[4][4];
		float v[16];
		struct
		{
		    float _11, _12, _13, _14;
		    float _21, _22, _23, _24;
		    float _31, _32, _33, _34;
		    float _41, _42, _43, _44;
		};
	};
};


////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

void Matrix3fIdentity(struct Matrix3f * Matrix);

void Matrix3fFromQuaternion(struct Matrix3f * Matrix, struct Quaternion * Quat);

void Matrix3fFromQuaternionScale(struct Matrix3f * Matrix, struct Quaternion * Quat, struct Vector3f * Scale);

void Matrix3fFromAdjointTranspose(struct Matrix3f * Dest, struct Matrix34f * Src);

float Matrix3fDeterminant(struct Matrix3f * Matrix);

void Matrix3fRotationVector(struct Matrix3f * Matrix, struct Vector3f * Vector, float Radians);

void Matrix3fTransform(struct Matrix3f * Matrix, struct Vector3f * Vector, struct Vector3f * Result);

void Matrix3fTransposedTransform(struct Matrix3f * Matrix, struct Vector3f * Vector, struct Vector3f * Result);


void Matrix34fCopy(struct Matrix34f * Dest, struct Matrix34f * Source);

void Matrix34fFromQuaternionRotation(struct Matrix34f * Matrix, struct Quaternion * Rotation, struct Vector3f * Translation, struct Vector3f * Scale);

void Matrix34fFromMatrixRotation(struct Matrix34f * Matrix, struct Matrix3f * Rotation, struct Vector3f * Translation);

void Matrix34fAdd(struct Matrix34f * Matrix, struct Matrix34f * A, struct Matrix34f * B);

void Matrix34fScale(struct Matrix34f * Dest, struct Matrix34f * Src, float Scale);

void Matrix34fMultiply(struct Matrix34f * Matrix, struct Matrix34f * A, struct Matrix34f * B);

void Matrix34fInvert(struct Matrix34f * Result, struct Matrix34f * Matrix);

void Matrix34fTransform(struct Matrix34f * Matrix, struct Vector3f * Vector, struct Vector3f * Result);

void Matrix34fTransformNormal(struct Matrix34f * Matrix, struct Vector3f * Vector, struct Vector3f * Result);


void Matrix4fIdentity(struct Matrix4f * Matrix);

void Matrix4fCopy(struct Matrix4f * Dest, struct Matrix4f * Source);

void Matrix4fMultiply(struct Matrix4f * Matrix, struct Matrix4f * A, struct Matrix4f * B);

void Matrix4fVertorMultiply(struct Vector4f * Result, struct Matrix4f * Matrix, struct Vector4f * Vector);

void Matrix4fRotateAngle(struct Matrix4f * Matrix, float Angle, uinteger Axis);

void Matrix4fRotateVector(struct Matrix4f * Matrix, struct Vector3f * Vector);

void Matrix4fRotateQuaternion(struct Matrix4f * Matrix, struct Quaternion * Quat);

void Matrix4fScale(struct Matrix4f * Matrix, float x, float y, float z);

void Matrix4fScaleVector(struct Matrix4f * Matrix, struct Vector3f * Vector);

struct Matrix4f * Matrix4fTranslate(struct Matrix4f * Matrix, float x, float y, float z);

void Matrix4fTransform(struct Matrix4f * Matrix, float x, float y, float z);

void Matrix4fTransformVector(struct Matrix4f * Matrix, struct Vector3f * Vector);

struct Matrix4f * Matrix4fLoadOrtho(struct Matrix4f * Matrix, float Left, float Right, float Bottom, float Top, float Near, float Far);

struct Matrix4f * Matrix4fLoadView(struct Matrix4f * Matrix, struct Vector3f * Front, struct Vector3f * Up, struct Vector3f * Side);

bool Matrix4fInverse(struct Matrix4f * Inverse, struct Matrix4f * Matrix);

#endif // MATH_GEOMETRY_MATRIX_H
