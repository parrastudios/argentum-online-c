/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PORTABILITY_EXTERN_H
#define PORTABILITY_EXTERN_H

////////////////////////////////////////////////////////////
// X Macros
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// X-Macro for begining a extern C block
///
/// Usage:
///		#ifndef MY_C_HEADER_H
///		#define MY_C_HEADER_H
///
///		#include <Portability/ExternBegin.h>
///
///		// My C header stuff
///
///		#include <Portability/ExternEnd.h>
///
///		#endif // MY_C_HEADER_H
////////////////////////////////////////////////////////////
#ifdef __cplusplus
	extern "C" {
#endif

#endif // PORTABILITY_EXTERN_H
