/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/View.h>
#include <Graphics/Device.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

// todo: multiple view ports available

void ViewInitialize(struct ViewType * View, struct Color4f * Color, struct BoundingQuad * Quad)
{
	Color4fCopy(&View->Color, Color);
	BoundingQuadCopy(&View->Quad, Quad, true);
}

void ViewClear(struct ViewType * View, uint32 Flags)
{
	// Reset port
	ViewResetPort(View);

	// Clear color
	glClearColor(View->Color.r, View->Color.g, View->Color.b, View->Color.a);

	// Clear screen
	glClear(Flags);
}

void ViewResize(struct ViewType * View, struct BoundingQuad * Quad)
{
	BoundingQuadCopy(&View->Quad, Quad, true);
}

void ViewResetPort(struct ViewType * View)
{
	glViewport((GLint)View->Quad.Bounds[0].x, (GLint)View->Quad.Bounds[0].y,
			   (GLint)View->Quad.Bounds[1].x, (GLint)View->Quad.Bounds[1].y);
}
