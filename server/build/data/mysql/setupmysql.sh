

# Install mysql from command line
export DEBIAN_FRONTEND=noninteractive
sudo -E apt-get -q -y install mysql-server

# Copy mysql config production file
cp /argentum-online-c-server/data/mysql/config/my.production.cnf /etc/mysql/my.cnf


# Execute mysql secure installation (deletes test db, removes root user...) SETS root password to root
mysql -fu root < /argentum-online-c-server/data/mysql/scripts/mysql_secure_installation.sql

# Restart mysqld
#sudo /etc/init.d/mysql restart
service mysql restart

# This is temp until we export the databases using a CREATE IF NOT EXISTS
mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS aocdb;"
mysql -u root --password=root aocdb < /argentum-online-c-server/data/mysql/aocdb.sql
