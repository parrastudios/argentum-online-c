/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Platform.h>
#include <System/Library.h>
#include <Memory/General.h>

#include <System/IOHelper.h> // todo: refactor into String module

#if defined(PLATFORM_FAMILY_WINDOWS)
#	include <System/Win32/LibraryImpl.h>
#elif defined(PLATFORM_FAMILY_UNIX)
#	include <System/Unix/LibraryImpl.h>
#elif PLATFORM_TYPE == PLATFORM_MACOS
#	include <System/MacOs/LibraryImpl.h>
#elif defined(PLATFORM_FAMILY_BSD)
#	error TODO: Dynamic loading libraries not implemented in BSD platform
#else
#	error Platform not supported for dynamic loading libraries
#endif

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define LIBRARY_NAME_SIZE			UINTEGER_SUFFIX(0xFF)

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef char                    LibraryNameInternalType[LIBRARY_NAME_SIZE];

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct LibraryType
{
	LibraryNameInternalType		Name;
	LibraryFlagsType			Flags;
	uinteger					RefCount;
	LibraryImplType				Impl;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get the library extension for specified platform
////////////////////////////////////////////////////////////
const char * LibraryExtension()
{
	return LibraryExtensionImpl();
}

////////////////////////////////////////////////////////////
/// Loads a library returning the handle to it
////////////////////////////////////////////////////////////
struct LibraryType * LibraryLoad(LibraryNameType Name, LibraryFlagsType Flags)
{
	if (Name != NULL)
	{
		struct LibraryType * Library = MemoryAllocate(sizeof(struct LibraryType));

		if (Library)
		{
		    Library->Impl = LibraryLoadImpl(Name, Flags);

			if (Library->Impl)
			{
				// strncpy(Library->Name, Name, LIBRARY_NAME_SIZE);
				strcpy(Library->Name, Name);
				Library->RefCount = 0;
				Library->Flags = Flags;

				return Library;
			}
			else
			{
				MemoryDeallocate(Library);
			}
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Unloads a library by its handle
////////////////////////////////////////////////////////////
void LibraryUnload(struct LibraryType * Library)
{
	if (Library)
	{
		LibraryUnloadImpl(Library->Impl);

		MemoryDeallocate(Library);
	}
}

////////////////////////////////////////////////////////////
/// Get the library name
////////////////////////////////////////////////////////////
LibraryNameType LibraryName(struct LibraryType * Library)
{
	return Library->Name;
}

////////////////////////////////////////////////////////////
/// Get a symbol from library
////////////////////////////////////////////////////////////
bool LibrarySymbol(struct LibraryType * Library, LibrarySymbolNameType Name, LibrarySymbolType * Address)
{
	if (Library)
	{
		return LibrarySymbolImpl(Library->Impl, Name, Address);
	}

	return false;
}
