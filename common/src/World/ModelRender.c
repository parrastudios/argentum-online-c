/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/ModelRender.h>

#include <Graphics/Device.h>
#include <Graphics/Mesh.h>
#include <Graphics/Skin.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create render component in a entity model
////////////////////////////////////////////////////////////
struct EntityType * ModelRenderCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a model render component
////////////////////////////////////////////////////////////
void ModelRenderDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Render a model
////////////////////////////////////////////////////////////
void ModelRender(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// On model load render callback
////////////////////////////////////////////////////////////
bool ModelRenderOnLoadCallback(ModelLoadCallback Callback)
{
	// Initialize render component from model resource
	if (Callback->Resource)
	{
		// Get entity
		struct EntityType * Entity = EntityGet(ENTITY_TYPE_MODEL, Callback->EntityId);

		// Get base component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct ModelComponentRenderData * RenderData = ComponentDataDerived(Render, struct ModelComponentRenderData);

		// Create mesh from resource
		if (Callback->Resource->Animated)
		{
			// Create animated mesh
			RenderData->Handle = SkinCreate(Callback->Resource);

			RenderData->Type = MODEL_RENDER_SKINNED_MESH;
		}
		else
		{
			// Create static mesh
			RenderData->Handle = MeshCreate(Callback->Resource);

			RenderData->Type = MODEL_RENDER_STATIC_MESH;
		}

		return (RenderData->Handle != NULL);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Initialize model render components
////////////////////////////////////////////////////////////
void ModelFactoryInitializeRenderCallback()
{
	// Register on model render load callback
	ModelLoadRegisterCallback(&ModelRenderOnLoadCallback);
}

////////////////////////////////////////////////////////////
/// Destroy render component pool of model factory
////////////////////////////////////////////////////////////
void ModelFactoryDestroyRenderCallback()
{
	// Unregister on model render load callback
	ModelLoadUnregisterCallback(&ModelRenderOnLoadCallback);
}

////////////////////////////////////////////////////////////
/// Return the model render implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentRenderType * ModelComponentRenderAttr()
{
	static const struct ModelComponentRenderType ModelComponentRenderImplAttr = {
		TypeInit(TODO, NULL)
	};

	static const struct ComponentRenderType ModelComponentRenderAttr = {
		TypeInit(Draw, &ModelRender),
		TypeInit(TypeImpl, (const void *)(&ModelComponentRenderImplAttr))
	};

	return (const void *)&ModelComponentRenderAttr;
}

////////////////////////////////////////////////////////////
/// Return the model render implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * ModelComponentRenderImpl()
{
	static const struct ComponentImplType ModelComponentRenderTypeImpl = {
		ExprInit(Base, TypeInit(Create, &ModelRenderCreate), TypeInit(Destroy, &ModelRenderDestroy)),
		TypeInit(Type, COMPONENT_TYPE_RENDER),
		UnionInit(Attributes, Render, &ModelComponentRenderAttr)
	};

	return &ModelComponentRenderTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create render component in a entity model
////////////////////////////////////////////////////////////
struct EntityType * ModelRenderCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain render component data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct ModelComponentRenderData * RenderData = ComponentDataDerived(Render, struct ModelComponentRenderData);

		// ..

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a model render component
////////////////////////////////////////////////////////////
void ModelRenderDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct ModelComponentRenderData * RenderData = ComponentDataDerived(Render, struct ModelComponentRenderData);

		// Destroy mesh handle
		if (RenderData->Type == MODEL_RENDER_SKINNED_MESH)
		{
			// Destroy animated mesh
			SkinDestroy(RenderData->Handle);
		}
		else if (RenderData->Type = MODEL_RENDER_STATIC_MESH)
		{
			// Destroy static mesh
			MeshDestroy(RenderData->Handle);
		}

		RenderData->Handle = NULL;
	}
}

////////////////////////////////////////////////////////////
/// Render a model
////////////////////////////////////////////////////////////
void ModelRender(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct ModelComponentBaseData * BaseData = ComponentDataDerived(Base, struct ModelComponentBaseData);

		// Get render component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct ModelComponentRenderData * RenderData = ComponentDataDerived(Render, struct ModelComponentRenderData);

		// Get transform component and obtain data reference
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

		// Get collision sphere component and obtain data reference
		struct ComponentType * Collision = EntityComponentCollisionSphere(Entity);
		struct ComponentCollisionSphereData * CollisionData = ComponentDataCollisionSphere(Collision);

		// Render mesh handle
		if (RenderData->Type == MODEL_RENDER_SKINNED_MESH)
		{
			// todo: remove this dirty trick
			static float ElapsedTime = 0.0f;

			// Render animated mesh
			SkinRender(RenderData->Handle);

			SkinAnimate(RenderData->Handle, ElapsedTime);

			ElapsedTime += 0.25f;

			if (ElapsedTime > 75.0f)
			{
				ElapsedTime = 0.0f;
			}
		}
		else if (RenderData->Type = MODEL_RENDER_STATIC_MESH)
		{
			// Render mesh
			MeshRender(RenderData->Handle);
		}
	}
}

////////////////////////////////////////////////////////////
/// Return the model render derived data size
////////////////////////////////////////////////////////////
uinteger ModelComponentRenderSize()
{
	return sizeof(struct ModelComponentRenderData);
}
