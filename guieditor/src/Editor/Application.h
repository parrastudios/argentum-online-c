/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef APPLICATION_H
#define APPLICATION_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Config.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define APPLICATION_INITIALIZE		0x00
#define APPLICATION_INTRO			0x01
#define APPLICATION_RUN				0x02
#define APPLICATION_DESTROY			0x03
#define APPLICATION_TEST			0x04

////////////////////////////////////////////////////////////
/// Initialize the application
///
////////////////////////////////////////////////////////////
bool ApplicationInitialize();

////////////////////////////////////////////////////////////
/// Update the events of the application
///
////////////////////////////////////////////////////////////
void ApplicationEvents();

////////////////////////////////////////////////////////////
/// Run the application
///
////////////////////////////////////////////////////////////
void ApplicationRun();

////////////////////////////////////////////////////////////
/// Destroy and cleanup the application
///
////////////////////////////////////////////////////////////
bool ApplicationDestroy();

////////////////////////////////////////////////////////////
/// Set up the status of the application
///
////////////////////////////////////////////////////////////
void ApplicationSetStatus(uinteger State);

////////////////////////////////////////////////////////////
/// Get the status of the application
///
////////////////////////////////////////////////////////////
void ApplicationGetStatus(uinteger * State);

#endif // APPLICATION_H
