/****************************************************************************

  IqeB_GUI_Main.cpp

  04.01.2015 RR: First editon of this file.
                 Base was the 'OpenGL Example with Widgets' from
                 'Erco's FLTK Cheat Page'. http://seriss.com/people/erco/fltk/

*****************************************************************************
*/

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Gl_Window.H>
#include <FL/gl.h>
#include <FL/Fl_Browser.H>
#include <FL/Fl_File_Browser.H>
#include <FL/Fl_ask.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Radio_Round_Button.H>
#include <FL/Fl_File_Chooser.H>
#include <FL/Fl_Native_File_Chooser.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Menu_Button.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Preferences.H>
#include <FL/x.H>
#include <FL/fl_draw.H>

/************************************************************************************
 * Variables for GUI (widget pointers, state variables, ...)
 * Part 1.
 */

static char GUI_StartupFile[ MAX_FILENAME_LEN];       // file to load after startup
static unsigned int GUI_BackgroundColor;              // Remember for GUI call

/************************************************************************************
 * Hold info about registered callbacks.
 * This is called from tool windows just opened.
 *
 */

#define PREFERENCE_MAX_GROUPS 512       // max preference groups managed or max callbacks managed

typedef struct {
  void **ppMyToolWin;         // If != NULL, pointer to Fl_Window pointer of a tool window
  void (*pUpdateGUI)(int);    // point to update GUI function
  int UpdateFlags;            // 0, update all else specific bit
  int MouseMode;              // if > 0, a mouse edit mode
  void (*pCloseToolWin)(void *w, void *pValueArg); // If != NULL, close this toolwindow
} T_GUI_RegisterCallback;

static T_GUI_RegisterCallback IqeB_RegisterCallback[ PREFERENCE_MAX_GROUPS];

static int IqeB_RegisterCallbackN = 0;  // number of callbacks registered

/************************************************************************************
 * IqeB_GUI_RegisterHasChanged
 *
 * Register callbacks has been changed.
 * Update some data.
 *
 */
static void IqeB_GUI_RegisterHasChanged()
{
  int i;
  int IqeB_DispStyle_MouseMode_Backup;
  T_GUI_RegisterCallback *pRegisterCallback;

  // ...

  IqeB_DispStyle_MouseMode_Backup = IqeB_DispStyle_MouseMode;  // get copy
  IqeB_DispStyle_MouseMode = IQE_GUI_MOUSE_MODE_NONE;  // Preset to default

  // ...

  pRegisterCallback = IqeB_RegisterCallback;

  for( i = 0; i < IqeB_RegisterCallbackN; i++, pRegisterCallback++) {

    // Catch first MouseMode != IQE_GUI_MOUSE_MODE_NONE
    if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_NONE &&
        pRegisterCallback->MouseMode != IQE_GUI_MOUSE_MODE_NONE) {

      IqeB_DispStyle_MouseMode = pRegisterCallback->MouseMode;
    }
  }

  // has someting hanged

  if( IqeB_DispStyle_MouseMode_Backup != IqeB_DispStyle_MouseMode) {  // this has changed

    if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_SKEL_EDIT || // Mouse mode: Skeleton edit
        IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_JOINT_SEL) { // Mouse mode: Skeleton joint select

      if( ! IqeB_DispStyle_doskeleton) {   // Show skelteon checkbox should be set

        IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doskeleton, 1); // Update variable and GUI
      }

      if( IqeB_DispStyle_doAnimations) {   // Do animations checkbox should be reset

        IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doAnimations, 0); // Update variable and GUI
      }
    } else if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_ANIM_EDIT) { // Mouse mode: Animation edit

      if( ! IqeB_DispStyle_doskeleton) {   // Show skelteon checkbox should be set

         IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doskeleton, 1); // Update variable and GUI
      }

      if( !IqeB_DispStyle_doAnimations) {   // Do animations checkbox should be set

        IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doAnimations, 1); // Update variable and GUI
      }
    }

    IqeB_GUI_UpdateWidgets();            // Also update the GUI
  }
}

/************************************************************************************
 * IqeB_GUI_RegisterCallback
 *
 * Register callbacks
 *
 * pUpdateGUI      Pointer to call from IqeB_GUI_UpdateWidgets() to update the GUI
 * UpdateFlags  0  Call pUpdateGUI function is called each time from IqeB_GUI_UpdateWidgets().
 *             xx  Bits here and for IqeB_GUI_UpdateWidgets() must be set both to call pUpdateGUI function.
 *                 See IQE_GUI_UPDATE_xxx defines.
 */

void IqeB_GUI_RegisterCallback( void **ppMyToolWin,       // Pointer to hold pointer to tool win data
                                void (*pUpdateGUI)(int),  // Pointer to call from IqeB_GUI_UpdateWidgets() to update the GUI
                                int UpdateFlags,          // Flags for calling pUpdateGUI
                                int MouseMode,            // if != 0, needed edit mode for this ...
                                void (*pCloseToolWin)(void *w, void *pValueArg)) // Close this toolwindow
{

  if( IqeB_RegisterCallbackN >= PREFERENCE_MAX_GROUPS) {   // out of space

    return;   // simply return
  }

  // and register it

  IqeB_RegisterCallback[ IqeB_RegisterCallbackN].ppMyToolWin   = ppMyToolWin;
  IqeB_RegisterCallback[ IqeB_RegisterCallbackN].pUpdateGUI    = pUpdateGUI;
  IqeB_RegisterCallback[ IqeB_RegisterCallbackN].UpdateFlags   = UpdateFlags;
  IqeB_RegisterCallback[ IqeB_RegisterCallbackN].MouseMode     = MouseMode;
  IqeB_RegisterCallback[ IqeB_RegisterCallbackN].pCloseToolWin = pCloseToolWin;

  IqeB_RegisterCallbackN += 1;  // have on more

  IqeB_GUI_RegisterHasChanged();  // registered callbacks has changed
}

/************************************************************************************
 * IqeB_GUI_CloseToolWindow
 *
 * * Updates tool windows positions to preferences cache
 * * Close tool window dialog
 * * Resets pointer to tool window dialog to NULL
 * * Unregister callbacks
 */

void IqeB_GUI_CloseToolWindow( void **ppMyToolWin)
{
  Fl_Window *pThisWin;
  T_GUI_RegisterCallback *pRegisterCallback;
  int i;

  // Update preferences, this also stores window postions
  // to the preferences cache

  IqeB_PreferencesUpdateChanges();  // update preferences database and save to to file

  // Close tool window dialog

  pThisWin = (Fl_Window *)*ppMyToolWin; // get pointer to tool

  delete pThisWin;

  // * Resets pointer to tool window dialog to NULL

  *ppMyToolWin = NULL;

  // Unregister callbacks

  pRegisterCallback = IqeB_RegisterCallback;

  for( i = 0; i < IqeB_RegisterCallbackN; i++, pRegisterCallback++) {

    if( pRegisterCallback->ppMyToolWin == ppMyToolWin) { // Got it

      int SizeToMove;

      SizeToMove = (IqeB_RegisterCallbackN - 1 - i) * sizeof( T_GUI_RegisterCallback);

      if( SizeToMove > 0) {

        memcpy( pRegisterCallback, pRegisterCallback + 1, SizeToMove);
      }

      IqeB_RegisterCallbackN -= 1;  // have on less
      break;
    }
  }

  IqeB_GUI_RegisterHasChanged();  // registered callbacks has changed
}

/************************************************************************************
 * IqeB_GUI_ToolWindowTestFreeEditMode
 *
 * Test to have a free an unused edit mode
 * Bring up an error box if not free.
 *
 * return: 0:  OK, go on
 *         1: Mouse edit mode is in use, don't open the tool window.
 */

int  IqeB_GUI_ToolWindowTestFreeEditMode( int MouseMode)
{
  char *pWhatToClose;
  int i;
  T_GUI_RegisterCallback *pRegisterCallback;

  if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_NONE ||      // Edit mode not in use
      IqeB_DispStyle_MouseMode == MouseMode) {                    // Already same as requested

    return( 0);   // return OK
  }

  // try to close other window with holds the mousemode

  pRegisterCallback = IqeB_RegisterCallback;

  for( i = 0; i < IqeB_RegisterCallbackN; i++, pRegisterCallback++) {

    // Catch first MouseMode != IQE_GUI_MOUSE_MODE_NONE
    if( pRegisterCallback->MouseMode != IQE_GUI_MOUSE_MODE_NONE) {  // This must hold the mosue mode

      if( pRegisterCallback->pCloseToolWin != NULL) {    // has a close function

        pRegisterCallback->pCloseToolWin( *(pRegisterCallback->ppMyToolWin), NULL);  // Close the window

        if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_NONE) {      // Edit mode not in use now

          return( 0);   // return OK
        }
      }
    }
  }

  // Pop up messagbox with problem

  switch( IqeB_DispStyle_MouseMode) {
  case IQE_GUI_MOUSE_MODE_SKEL_EDIT:   // Mouse mode: Skeleton edit
    pWhatToClose = (char *)"the 'Skeleton/Editor' window";
    break;
  case IQE_GUI_MOUSE_MODE_JOINT_SEL:   // Mouse mode: Skeleton joint select
    pWhatToClose = (char *)"the 'Skin binding' window";
    break;
  case IQE_GUI_MOUSE_MODE_ANIM_EDIT:   // Mouse mode: Skeleton joint select
    pWhatToClose = (char *)"the 'Animation/Editor' window";
    break;
  default:
    pWhatToClose = (char *)"any mouse edit tool window";
    break;
  }    // end switch

  // pop up an error message box

  fl_alert( "Can't open this tool window.\nPlease close %s first!", pWhatToClose);

  return( 1);   // return mouse edit mode not free
}

/************************************************************************************
 *
 * File browser with drag receiver
 */

class DropFileBrowser : public Fl_File_Browser {
public:
  // Ctor
  DropFileBrowser(int x,int y,int w,int h, const char *l = 0) : Fl_File_Browser(x,y,w,h,l) {
  }

  // Receiver event handler
  int handle( int event) {
    int ret;

    ret = Fl_File_Browser::handle(event);

    if (event == FL_KEYBOARD && type() >= FL_HOLD_BROWSER) {

      switch (Fl::event_key()) {
      case FL_Enter:
      case FL_KP_Enter:

        if( value() > 0) {    // have a selection

          do_callback();      // do the callback
        }
        return 1;
        break;

      case FL_Home:

        if( size() > 0) {    // have items

          select( 1);        // select first item
        }
        return 1;
        break;

      case FL_End:

        if( size() > 0) {    // have items

          select( size());  // select last item
        }
        return 1;
        break;

      case FL_BackSpace:      // backspace --> up one directory level
        if( size() > 0) {    // have items

          select( 1);         // select first item (is the directory up
          do_callback();      // do the callback
        }
        return 1;
        break;
      } // end switch
    }

    return(ret);
  }

    // overwrite load
    int		load(const char *directory, Fl_File_Sort_F *sort = fl_numericsort) {

      int ret, DoInsertDirUp;
      const char *p;

      // first fill the list
      ret = Fl_File_Browser::load( directory, sort);

      // test to have a directory up string '../' as first line

      p = this->text( 1);   // get pointer to first line

      DoInsertDirUp = false;

      if( p == NULL) {      // have NO first line

        // no 'directory up', insert it

        DoInsertDirUp = true;

      } else {              // have a first line

        if(  p[ 0] == '.' && p[ 1] == '.' &&       // Test for 'directory up'
            (p[ 2] == '/' || p[ 2] == '\\') &&
             p[ 3] == '\0') {

          // have 'directory up', nothing to do

        } else if(  (p[ 0] >= 'A' && p[ 0] <= 'Z') && p[ 1] == ':' && // Test for drive, something like 'C:/'
            (p[ 2] == '/' || p[ 2] == '\\') &&
             p[ 3] == '\0') {

          // have 'directory up', nothing to do

        } else {

          // no 'directory up', insert it

          DoInsertDirUp = true;
        }
      }

      if( DoInsertDirUp) {

        insert( 1, "../");     // insert directory up
      }

      return( ret);
    }

};

/************************************************************************************
 * OpenGL window.
 * NOTE: Base was a code 'OpenGL App With FLTK Widgets' from 'erco 11/08/06'.
 */

class MyGlWindow : public Fl_Gl_Window {

    // DRAW METHOD
    void draw() {

      if( !valid()) {                 // first time? init
         valid(1);
         IqeB_DispSetViewportSize( w(), h());
       }

       IqeB_DispDisplay( GUI_BackgroundColor);   // Draw

       // Load startup file once after the first IqeB_DispDisplay() call.
       // This is needed to allow IqeB_DispDisplay() some OnpenGL specfic
       // initalisations after it's first call

       if( GUI_StartupFile[ 0] != '\0' ) {                        // have a startup file

         IqeB_DispPrepareNewModel( GUI_StartupFile, NULL); // load this file

         GUI_StartupFile[ 0] = '\0';                              // reset flag
       }
    }

    // HANDLE WINDOW RESIZING
    void resize(int X,int Y,int W,int H) {
        Fl_Gl_Window::resize(X,Y,W,H);
        IqeB_DispSetViewportSize( W,H);
        redraw();
    }

    // overwrite event handler

    int handle(int event) {

      switch( event) {
      case FL_ENTER:      // The mouse has been moved to point at this widget.
      case FL_LEAVE:      // The mouse has moved out of the widget.
      case FL_MOVE:       // The mouse has moved without any mouse buttons held down.
      case FL_DRAG:       // The mouse has moved with a button held down.
      case FL_MOUSEWHEEL: // The user has moved the mouse wheel.
      case FL_PUSH:       // A mouse button has gone down
      case FL_RELEASE:    // A mouse button has been released.

        IqeB_DispMouseEventAction( event);

        return( 1);
      }

      return( Fl_Gl_Window::handle( event));     // do parent handle function
    }

public:
    // OPENGL WINDOW CONSTRUCTOR
    MyGlWindow(int X,int Y,int W,int H,const char*L=0) : Fl_Gl_Window(X,Y,W,H,L) {
      end();
    }
};

/************************************************************************************
 * Variables for GUI (widget pointers, state variables, ...)
 * Part 2.
 */

// define for window sizes
#define MYWIN_SIZE_X_MIN       964
#define MYWIN_SIZE_X_MAX      2048
#define MYWIN_SIZE_X_DEFAULT   MYWIN_SIZE_X_MIN

#define MYWIN_SIZE_Y_MIN       720
#define MYWIN_SIZE_Y_MAX      2048
#define MYWIN_SIZE_Y_DEFAULT   MYWIN_SIZE_Y_MIN

// Main window

static Fl_Window *pGUI_Main;
static int MyWinPosX = IQE_GUI_NO_WINPOS_X, MyWinPosY = IQE_GUI_NO_WINPOS_Y; // last window position
static int MyWinSizeX = MYWIN_SIZE_X_DEFAULT, MyWinSizeY = MYWIN_SIZE_Y_DEFAULT; // last window size
#ifdef WIN32
static HANDLE hHandleMultipleRunning; // Protect against multiple running of this application
#endif

// file browser

static char GUI_CurrentDirectory[ MAX_FILENAME_LEN];  // path of current directory for file browser
static char GUI_FileExtensionList[ 4096];             // file extensions for the file browser

// GUI elements
static Fl_Group *pGUI_GroupLeftSide;        // left side of OpenGL window

static DropFileBrowser *pGUI_FileBrowser;   // file browser
static Fl_Button *pGUI_FileChoose;          // Choose a file

static MyGlWindow *pGUI_WinOpenGL;          // opengl window

// Save file things
static Fl_Button *pGUI_But_SaveFile;        // save file button
static Fl_Check_Button *pGUI_CBox_SaveFile_CopyTex;               // Also copy image files (textures)
static Fl_Check_Button *pGUI_CBox_SaveFile_Tangent;               // Generate texture tangent data
static Fl_Check_Button *pGUI_CBox_SaveFile_Adjacency;             // Generate triangle adjacency data
static Fl_Check_Button *pGUI_CBox_SaveFile_Model;                 // Export meshes including joints and base frame
static Fl_Check_Button *pGUI_CBox_SaveFile_Poses;                 // Export animations including all poses
static Fl_Choice *pGUI_SaveFile_Type;                             // What type of file to save

static int IqeB_SaveFile_CopyTex   = (IQE_EXPORT_OPTION_DEFAULT & IQE_EXPORT_OPTION_COPY_TEX) != 0;  // Also copy image files (textures)
static int IqeB_SaveFile_Tangent   = (IQE_EXPORT_OPTION_DEFAULT & IQE_EXPORT_OPTION_TANGENT) != 0;    // Generate texture tangent data
static int IqeB_SaveFile_Adjacency = (IQE_EXPORT_OPTION_DEFAULT & IQE_EXPORT_OPTION_ADJACENCY) != 0; // Generate triangle adjacency data
static int IqeB_SaveFile_Model     = (IQE_EXPORT_OPTION_DEFAULT & IQE_EXPORT_OPTION_MODEL) != 0; // Export meshes including joints and base frame
static int IqeB_SaveFile_Poses     = (IQE_EXPORT_OPTION_DEFAULT & IQE_EXPORT_OPTION_POSES) != 0; // Export animations including all poses
static int IqeB_SaveFile_Type      =  IQE_EXPORT_FILE_TYPE_IQM; // Save file type, 0 = is IQM, 1 = is IQM

// OpenGL window display state variables
static Fl_Check_Button *pGUI_CBox_DispStyle_doplane;              // draw gound plane
static Fl_Check_Button *pGUI_CBox_DispStyle_dowire;               // draw model wireframe
static Fl_Check_Button *pGUI_CBox_DispStyle_donormals;            // draw normals
static Fl_Check_Button *pGUI_CBox_DispStyle_dotexture;            // draw model with textures
static Fl_Check_Button *pGUI_CBox_DispStyle_dobackface;           // draw model with NO backface culling
static Fl_Check_Button *pGUI_CBox_DispStyle_doperspective;        // orthogonal/perspective camera
static Fl_Check_Button *pGUI_CBox_DispStyle_doskeleton;           // draw skeleton
static Fl_Check_Button *pGUI_CBox_DispStyle_doJointNames;         // draw the joint names of a skeleton
static Fl_Check_Button *pGUI_CBox_DispStyle_doBlendWeightColors;  // draw model with blend weight colors

static Fl_Button *pGUI_But_View_Reset;                                // View reset button

// OpenGL window animation things
static Fl_Check_Button *pGUI_CBox_Anim_Enable;                    // animation enable
static Fl_Choice *pGUI_Choice_Anim_Select;                        // Select an animation
static Fl_Check_Button *pGUI_CBox_Anim_PlayOnce;                  // animation 'play once' only
static Fl_Button *pGUI_But_Anim_Play;                             // Play/Pause animation
static Fl_Button *pGUI_But_Anim_Next;                             // Next animation
static Fl_Button *pGUI_But_Anim_Prev;                             // Previous animation
static Fl_Button *pGUI_But_Anim_NextF;                            // Next animation frame
static Fl_Button *pGUI_But_Anim_PrevF;                            // Previous animation frame
static Fl_Button *pGUI_But_Anim_FirstF;                           // First animation frame

/************************************************************************************
 * IqeB_GUI_CBox_SetValue_Callback
 */

static void IqeB_GUI_CBox_SetValue_Callback( Fl_Widget *w, void *pValueArg)
{
  int *pValue;
  Fl_Check_Button *pThis;

  IqeB_DispMessage( DRAW_MSG_COL_OK, NULL, NULL, NULL);  // Clear message area

  // ...

  pThis  = (Fl_Check_Button *)w;
  pValue = (int *)pValueArg;             // get pointer to associated variable

  *pValue = pThis->value();               // update the variable
}

/************************************************************************************
 * IqeB_GUI_But_Tool_Copy_Callback
 */

static void IqeB_GUI_But_Tool_Copy_Callback( Fl_Widget *w, void *pData)
{

  // test for model in view

  if( pDrawModel == NULL || DrawModelSelName[ 0] == '\0') {

    IqeB_DispMessage( DRAW_MSG_COL_ERR,
                      NULL, NULL, NULL);

    return;
  }

  // enshure clipboard model is freed

  if( pClipBoardModel != NULL) {

    IqeB_ModelFreeData( &pClipBoardModel, IQE_MODEL_FREE_ALL);
  }

  ClipBoardModelName[ 0] = '\0';       // empty string
  ClipBoardModelFullName[ 0] = '\0';   // empty string

  // copy

  strcpy( ClipBoardModelName, DrawModelSelName);
  strcpy( ClipBoardModelFullName, DrawModelSelFullName);
  IqeB_ModelCopy( &pDrawModel, &pClipBoardModel, IQE_MODEL_COPY_NEW_ALL);

  // ...

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    (char *)"Copy done", NULL, NULL);

  IqeB_GUI_UpdateWidgets();            // Also update the GUI
}

/************************************************************************************
 * IqeB_GUI_But_Tool_Paste_Callback
 * NOTE: loads the model to the file selection (so it can be added)
 *       and displays it.
 */

static void IqeB_GUI_But_Tool_Paste_Callback( Fl_Widget *w, void *pData)
{

  // test for a model in the clipboard

  if( pClipBoardModel == NULL || ClipBoardModelName[ 0] == '\0' || ClipBoardModelFullName[ 0] == '\0') {

    IqeB_DispMessage( DRAW_MSG_COL_ERR,
                      NULL, NULL, NULL);

    return;
  }

  // load the model to the file selection

  IqeB_DispPrepareNewModel( ClipBoardModelFullName, pClipBoardModel);  // Prepare some variables

  // ...

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    (char *)"Paste done", NULL, NULL);

  IqeB_GUI_UpdateWidgets();            // Also update the GUI
}

/************************************************************************************
 * IqeB_GUI_But_Tool_OpenWin_Callback
 */

static void IqeB_GUI_But_Tool_OpenWin_Callback( Fl_Widget *w, void *pValueArg)
{

  if( pGUI_Main == NULL) {     // security test, no main windwo

    return;
  }

  IqeB_DispMessage( DRAW_MSG_COL_OK, NULL, NULL, NULL);  // Clear message area

  if( pValueArg != NULL) {     // have a function

    void (*pFunc)( int, int, int, int);

    pFunc = (void (*)(int, int, int, int))pValueArg;   // convert to pointer to function

    pFunc( pGUI_Main->x_root(), pGUI_Main->x_root() + pGUI_Main->decorated_w(),
           pGUI_Main->y_root(), pGUI_Main->y_root() + pGUI_Main->decorated_h());             // call the function
  }

  // ...

}

/************************************************************************************
 * IqeB_DispStyle_ViewMode_Callback
 */

static void IqeB_DispStyle_ViewMode_Callback( Fl_Widget *w, void *data)
{
  int Value;

  IqeB_DispMessage( DRAW_MSG_COL_OK, NULL, NULL, NULL);  // Clear message area

  // ...

  Value = (int)data;             // get value to set

  if( Value < IQE_VIEWMODE_FIRST) Value = IQE_VIEWMODE_FIRST;   // security test
  if( Value > IQE_VIEWMODE_LAST)  Value = IQE_VIEWMODE_LAST;    // security test

  IqeB_DispStyle_ViewMode = Value;

  // Save changed preference settings

  IqeB_PreferencesUpdateChanges();  // update preferences database and save to to file
}

/************************************************************************************
 * IqeB_DispStyle_NumViewPorts_Callback
 */

static void IqeB_DispStyle_NumViewPorts_Callback( Fl_Widget *w, void *data)
{
  int Value;

  IqeB_DispMessage( DRAW_MSG_COL_OK, NULL, NULL, NULL);  // Clear message area

  // ...

  Value = (int)data;             // get value to set

  if( Value < IQE_VIEW_PORT_NUM_1) Value = IQE_VIEW_PORT_NUM_1;   // security test
  if( Value > IQE_VIEW_PORT_NUM_4) Value = IQE_VIEW_PORT_NUM_4;    // security test

  IqeB_DispStyle_NumViewPorts = Value;

  // Save changed preference settings

  IqeB_PreferencesUpdateChanges();  // update preferences database and save to to file
}

/************************************************************************************
 * IqeB_DispStyle_View_Reset_Callback
 */

static void IqeB_DispStyle_View_Reset_Callback( Fl_Widget *w, void *data)
{

  IqeB_DispMessage( DRAW_MSG_COL_OK, NULL, NULL, NULL);  // Clear message area

  // ...

  IqeB_DispViewReset();
}

/************************************************************************************
 * Callback, the file choose button is pressed
 */

static void ButtonFileChooseCallback(Fl_Widget *w, void *data)
{
  char CurrDirStr[ MAX_FILENAME_LEN];  // path of current directory for file browser
  char FileExtensionList[ 4096];
  int slen;
  const char *pFileName;

  // prepare directory
  strncpy( CurrDirStr, GUI_CurrentDirectory, sizeof( CurrDirStr) - 1);  // get copy of diretory path

  // remove trailing path delimiter
  slen = strlen( CurrDirStr);
  if( slen > 0 && ( CurrDirStr[ slen - 1] == '/' || CurrDirStr[ slen - 1] == '\\')) {

    CurrDirStr[ slen - 1] = '\0';
  }

  // prepare file extensions

  strcpy( FileExtensionList, "3D Models (*.{");   // search pattern header
  IqeB_ImportFileExtensions( FileExtensionList + strlen( FileExtensionList),
                                  sizeof( FileExtensionList) - strlen( FileExtensionList) - 3); // get file extensions we support
  strcat( FileExtensionList, "})");    // search pattern footer

  // Create the file chooser, and show it
  Fl_File_Chooser chooser( CurrDirStr,                 // directory
                           FileExtensionList,          // filter
                           Fl_File_Chooser::SINGLE,    // chooser type
                           "Choose a file");           // title

  chooser.sort = fl_casenumericsort;  // set our filename sort function
  chooser.preview( 0);            // disable preview
  chooser.previewButton->hide();  // hide the preview button
  chooser.newButton->hide();      // hide the new button
  chooser.show();

  // Block until user picks something.
  //     (The other way to do this is to use a callback())
  //
  while(chooser.shown()) {

    Fl::wait();
  }

  // User hit cancel?
  if( chooser.value() == NULL || chooser.count() < 1) {

    return;
  }

  // set directory

  pFileName = chooser.directory();

  if( pFileName != NULL && pFileName[ 0]) {

    memset( GUI_CurrentDirectory, 0, sizeof( GUI_CurrentDirectory)); // zero all
    strncpy( GUI_CurrentDirectory, pFileName, sizeof( GUI_CurrentDirectory) - 256);

    // enshure trailing path delimiter
    slen = strlen( GUI_CurrentDirectory);
    if( slen > 0) {
      if( GUI_CurrentDirectory[ slen - 1] != '/' && GUI_CurrentDirectory[ slen - 1] != '\\') {

        GUI_CurrentDirectory[ slen] = '/';
        GUI_CurrentDirectory[ slen + 1] = '\0';
      }

      // load the selected directory
      pGUI_FileBrowser->load( GUI_CurrentDirectory, fl_casenumericsort);  // load directory listing of current directory
    }
  }

  // selected filename

  pFileName = chooser.value();

  if( pFileName != NULL && pFileName[ 0]) {

    // get pointer after last path delimiter

    if( (pFileName = strrchr( chooser.value(), '\\')) != NULL) {

       pFileName++;

    } if( (pFileName = strrchr( chooser.value(), '/')) != NULL) {

       pFileName++;
    } else {

      pFileName = chooser.value();
    }

    // select the file

    for( slen = 1; slen <= pGUI_FileBrowser->size(); slen++) {

      if( strcasecmp( pFileName, pGUI_FileBrowser->text( slen)) == 0) {   // got it

        // deslect the last selection
        if( pGUI_FileBrowser->value() > 0) { // got a selected item

          pGUI_FileBrowser->select( pGUI_FileBrowser->value(), 0); // deselect
        }

        // select this
        pGUI_FileBrowser->select( slen); // select
        break;
      }
    }

    // Give focus to file browswer, cath the keyboard events

    pGUI_FileBrowser->take_focus();

    // load and show file

    IqeB_DispPrepareNewModel( (char *)chooser.value(), NULL);

    if( pGUI_WinOpenGL) {
      pGUI_WinOpenGL->redraw();    // Redraw the OpenGL window
    }
  }
}

/************************************************************************************
 * Callback, file browser has selected a file
 */

static void FileBrowserCallback(Fl_Widget *w, void *data)
{

  int isKeyboardEvent, isKeyDoDirectory;
  DropFileBrowser *pFileBrowserThis = (DropFileBrowser*)w;

  isKeyboardEvent = Fl::event() == FL_KEYBOARD;     // is a keyboard event
  isKeyDoDirectory = isKeyboardEvent &&
                     ( Fl::event_key() == FL_Enter ||    // is return key
                       Fl::event_key() == FL_KP_Enter ||
                       Fl::event_key() == FL_BackSpace); // or backspace key

  int index = pFileBrowserThis->value();  // get index of selected item
  if ( index > 0 ) {                      // valid item?

    int  TextLen;
    const char *pItemText;

    pItemText = pFileBrowserThis->text(index);   // get text of selected item
    TextLen = strlen( pItemText);                // length of item text

    if( TextLen > 0 &&                           // test for directory
        (pItemText[ TextLen - 1] == '/' || pItemText[ TextLen - 1] == '\\')) {

      if( !isKeyboardEvent || isKeyDoDirectory) {  // not from keyboard or do directory key

        // test for directory up
        if( TextLen == 3 && pItemText[ 0] == '.' && pItemText[ 1] == '.') {

          TextLen = strlen( GUI_CurrentDirectory);             // length of item text

          if( TextLen > 0) {

            TextLen -= 1;    // skip last path caracter

            while( TextLen > 0 && GUI_CurrentDirectory[ TextLen - 1] != '/' && GUI_CurrentDirectory[ TextLen - 1] != '\\') {

              TextLen -= 1;
            }

            GUI_CurrentDirectory[ TextLen] = '\0';    // set end of string (after the path character)
          }

          pFileBrowserThis->load( GUI_CurrentDirectory, fl_casenumericsort);       // load directory listing of current directory

        } else {   // must be directory name

          if( TextLen + strlen( GUI_CurrentDirectory) + 1 < sizeof( GUI_CurrentDirectory)) {  // nave space to append

            strcat( GUI_CurrentDirectory, pItemText);          // append selected directory
            pFileBrowserThis->load( GUI_CurrentDirectory, fl_casenumericsort);     // load directory listing of current directory
          }
        }
      }
    } else {

      // (try to) show the file as 3D model

      char file_name[ MAX_FILENAME_LEN];

      if( strlen( GUI_CurrentDirectory) + strlen( pItemText) + 2 < sizeof( file_name)) {  // strcat is OK

        strcpy( file_name, GUI_CurrentDirectory);
        strcat( file_name, pItemText);

        fl_cursor( FL_CURSOR_WAIT);       // A sometime time cosuming operation follows, show wait cursor

        IqeB_DispPrepareNewModel( file_name, NULL);

        fl_cursor( FL_CURSOR_DEFAULT);    // Reset cursor

        if( pGUI_WinOpenGL) {
          pGUI_WinOpenGL->redraw();    // Redraw the OpenGL window
        }
      }
    }
  }
}

/************************************************************************************
 * Callback, button 'Save' pressed
 * Saves the model visible in the OpenGL/glut window
 */

static void ButtonSaveFileCallback(Fl_Widget *w, void *data)
{
  static char CurrDirStr[ MAX_FILENAME_LEN];  // must be static, path of last used directory
  static char TempFileName[ MAX_FILENAME_LEN];  // must be static, path of last used directory
  char FileExtensionList[ 256];
  int slen, retval, ExportOptionFlags;
  const char *pFileName;
  char *p;

  // test for data to save

  if( pDrawModel == NULL || DrawModelSelName[ 0] == '\0') {

    IqeB_DispMessage( DRAW_MSG_COL_ERR,
                      NULL, NULL, NULL);

    return;
  }

  // prepare directory

  if( CurrDirStr[ 0] == '\0') {   // no directory set until now

    strncpy( CurrDirStr, GUI_CurrentDirectory, sizeof( CurrDirStr) - 1);  // get copy of diretory path

    // remove trailing path delimiter
    slen = strlen( CurrDirStr);
    if( slen > 0 && ( CurrDirStr[ slen - 1] == '/' || CurrDirStr[ slen - 1] == '\\')) {

      CurrDirStr[ slen - 1] = '\0';
    }

    IqeB_FileNormalizePathChars( CurrDirStr); // Normalize path slasches
  }

  // prepare preset filename

  strcpy( TempFileName, CurrDirStr);
  strcat( TempFileName, "/");
  strcat( TempFileName, DrawModelSelName);

  // Set windows path slasches
  p = TempFileName;
  while( *p != '\0') {

#ifdef _WIN32
      if( *p == '/') *p = '\\';
#else
      if( *p == '\\') *p = '/';
#endif
    p++;
  }

  // set prober extension
  if( strchr( TempFileName, '.') != NULL) {       // have point

    if( IqeB_SaveFile_Type == IQE_EXPORT_FILE_TYPE_IQM) {
      fl_filename_setext( TempFileName, sizeof( TempFileName), ".iqm");
    } else {  // must be .iqe
      fl_filename_setext( TempFileName, sizeof( TempFileName), ".iqe");
    }
  }

  // ...

  strcpy( FileExtensionList, "IQM 'binary' files (*.iqm)\tIQE 'text' files (*.iqe)");   // search pattern header

  // Create the file chooser, and show it
  Fl_File_Chooser chooser( TempFileName, //CurrDirStr,                 // directory
                           FileExtensionList,          // filter
                           Fl_File_Chooser::CREATE,    // chooser type
                           "Save to file");            // title

  chooser.filter_value( IqeB_SaveFile_Type); // last choosen file tpype
  chooser.sort = fl_casenumericsort;         // set our filename sort function
  chooser.preview( 0);                       // disable preview
  chooser.previewButton->hide();             // hide the preview button
  chooser.show();

  // Block until user picks something.
  //     (The other way to do this is to use a callback())
  //
  while(chooser.shown()) {

    Fl::wait();
  }

  // User hit cancel?
  if( chooser.value() == NULL || chooser.count() < 1) {

    return;
  }

  // get back last used directory and save for next time open

  pFileName = chooser.value();

  if( pFileName != NULL && pFileName[ 0]) {

    memset( CurrDirStr, 0, sizeof( CurrDirStr)); // zero all
    strncpy( CurrDirStr, pFileName, sizeof( CurrDirStr));

    // cat at last path delimter

    p = strrchr( CurrDirStr, '/');
    if( p == NULL) {
      p = strrchr( CurrDirStr, '\\');
    }
    if( p != NULL) {
      *p = '\0';
    }

    IqeB_FileNormalizePathChars( CurrDirStr); // Normalize path slasches
  } else {

    CurrDirStr[ 0] = '\0';   // empty string
  }

  // selected filename

  pFileName = chooser.value();

  memset( TempFileName, 0, sizeof( TempFileName));
  strncpy( TempFileName, pFileName, sizeof( TempFileName) - 4);

  // export options

  ExportOptionFlags = 0;
  if(   IqeB_SaveFile_CopyTex) ExportOptionFlags |= IQE_EXPORT_OPTION_COPY_TEX;
  if(   IqeB_SaveFile_Tangent) ExportOptionFlags |= IQE_EXPORT_OPTION_TANGENT;
  if( IqeB_SaveFile_Adjacency) ExportOptionFlags |= IQE_EXPORT_OPTION_ADJACENCY;
  if(     IqeB_SaveFile_Model) ExportOptionFlags |= IQE_EXPORT_OPTION_MODEL;
  if(     IqeB_SaveFile_Poses) ExportOptionFlags |= IQE_EXPORT_OPTION_POSES;

  switch( chooser.filter_value()) {

  case IQE_EXPORT_FILE_TYPE_IQM: // IQM file

    IqeB_SaveFile_Type = chooser.filter_value();  // remember choosen file tpype

    // set prober extension
    fl_filename_setext( TempFileName, sizeof( TempFileName), ".iqm");

    retval = IqeB_Export_File_IQM_IQE( pDrawModel, TempFileName, CurrDirStr, IqeB_SaveFile_Type, ExportOptionFlags);

    break;

  case IQE_EXPORT_FILE_TYPE_IQE: // IQE file

    IqeB_SaveFile_Type = chooser.filter_value();  // remember choosen file tpype

    // set prober extension
    fl_filename_setext( TempFileName, sizeof( TempFileName), ".iqe");

    retval = IqeB_Export_File_IQM_IQE( pDrawModel, TempFileName, CurrDirStr, IqeB_SaveFile_Type, ExportOptionFlags);

    break;

  default:

    IqeB_DispMessage( DRAW_MSG_COL_ERR,
                      (char *)"Select .iqm or .iqe file", (char *)"to save the model", NULL);

    return;
  }

  if( retval == 0) {   // Save was OK

    IqeB_DispMessage( DRAW_MSG_COL_OK,
                        (char *)"Save done", NULL, NULL);

  } else {

    IqeB_DispMessage( DRAW_MSG_COL_ERR,
                      (char *)"Save failed", NULL, NULL);
  }
}

/************************************************************************************
 * Callback, for animation buttons or checkboxes
 */

static void SaveFileTypeGUICallback(Fl_Widget *w, void *pValueArg)
{

  IqeB_SaveFile_Type = pGUI_SaveFile_Type->value();

  IqeB_GUI_UpdateWidgets();                        // Also update the GUI
}

/************************************************************************************
 * Callback, for animation buttons or checkboxes
 */

static void AnimationGUICallback(Fl_Widget *w, void *pValueArg)
{
  int AnimationAction, ActionArgument;

  IqeB_DispMessage( DRAW_MSG_COL_OK, NULL, NULL, NULL);  // Clear message area

  if( w == pGUI_CBox_Anim_Enable) {   // animation checbox

    int *pValue;
    Fl_Check_Button *pThis;

    pThis  = (Fl_Check_Button *)w;
    pValue = (int *)pValueArg;             // get pointer to associated variable

    if( pValueArg != NULL) {

      *pValue = pThis->value();            // update the variable
    }

    IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI

    return;
  }

  if( w == pGUI_CBox_Anim_PlayOnce) {   // animation 'play once' checbox

    int *pValue;
    Fl_Check_Button *pThis;

    pThis  = (Fl_Check_Button *)w;
    pValue = (int *)pValueArg;             // get pointer to associated variable

    if( pValueArg != NULL) {

      *pValue = pThis->value();            // update the variable
    }

    //x/IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI

    return;
  }

  // animation actions

  AnimationAction = IQE_ANIMATE_ACTION_NONE;           // Preset no animation action
  ActionArgument  = 0;                                 // Preset

  if( w == pGUI_But_Anim_FirstF) {                      // First animation frame
    AnimationAction = IQE_ANIMATE_ACTION_FRAME_FIRST;
  } else if( w == pGUI_But_Anim_PrevF) {               // Previous animation frame
    AnimationAction = IQE_ANIMATE_ACTION_FRAME_PREV;
  } else if( w == pGUI_But_Anim_NextF) {               // Next animation frame
    AnimationAction = IQE_ANIMATE_ACTION_FRAME_NEXT;
  } else if( w == pGUI_But_Anim_Prev) {                // Previous animation
    AnimationAction = IQE_ANIMATE_ACTION_PREV;
  } else if( w == pGUI_But_Anim_Next) {                // Next animation
    AnimationAction = IQE_ANIMATE_ACTION_NEXT;
  } else if( w == pGUI_Choice_Anim_Select) {           // Select animation
    ActionArgument  = pGUI_Choice_Anim_Select->value();
    AnimationAction = IQE_ANIMATE_ACTION_SET;
  }

  if( AnimationAction != IQE_ANIMATE_ACTION_NONE) {    // do animation action

    IqeB_AnimAction( AnimationAction, ActionArgument);

    IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
    return;
  }

  // play/pause button

  if( w == pGUI_But_Anim_Play) {   // play/pause

    IqeB_DispStyle_doplay = ! IqeB_DispStyle_doplay;   // toggle variable
    w->label( IqeB_DispStyle_doplay ? "@>" : "@+3||");

    IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
  }
}

/************************************************************************************
 * IqeB_GUI_MainEndWorks
 *
 * Do work on shutting down the program.
 */

static void IqeB_GUI_MainEndWorks()
{

  IqeB_PreferencesUpdateChanges();  // update preferences database and save to to file
}

/************************************************************************************
 * Callback exit program
 */

static void MainExitCallback( Fl_Widget *,void *)
{

  IqeB_GUI_MainEndWorks();      // Do work on shutting down the program.

#ifdef WIN32
  // Protect against multiple running of this application

  if( hHandleMultipleRunning != NULL) {

    ReleaseMutex( hHandleMultipleRunning ); // Explicitly release mutex
    CloseHandle( hHandleMultipleRunning ); // close handle before terminating
  }
#endif

    // Destroy gracefully all windows
    while (Fl::first_window())
    {
        delete Fl::first_window();
    }

   exit(0);
}

/************************************************************************************
 * Callback, open about dialog
 */

static void MainHideWinCallback( Fl_Return_Button* o, void*)
{

  ((Fl_Window*)(o->parent()))->hide();
}

static void MainAboutCallback( Fl_Widget *,void *)
{
  Fl_Window *about_panel;
  char TempStringTitle[ 256];
  char TempStringVersion[ 256];
  int y, TimeToBreakLoop;
  char *pCreditText = NULL;

  sprintf( TempStringTitle, "About %s", WIN_PROG_NAME);
  about_panel = new Fl_Window( 340, 100, TempStringTitle);

  // version

  y = 10;

  sprintf( TempStringVersion, "%s\nVersion %s", WIN_PROG_NAME, WIN_PROG_VERSION);
  { Fl_Box* o = new Fl_Box( 16, y, 300, 54, TempStringVersion);
    o->labelfont(1);
    o->labelsize(20);
    //o->labelcolor( fl_rgb_color( 103, 255, 142));
    o->labelcolor( fl_rgb_color( 104, 72, 35));
    o->align(Fl_Align(FL_ALIGN_TOP_LEFT|FL_ALIGN_INSIDE));
  } // Fl_Box* o

  y += 54;

  { Fl_Box* o = new Fl_Box( 16, y, 300, 16, "Credits:");
    o->align(Fl_Align(FL_ALIGN_TOP_LEFT|FL_ALIGN_INSIDE));
    o->labelsize(16);
    o->labelcolor( fl_rgb_color( 128, 0, 0));
  } // Fl_Box* o

  y += 24;

  // prepare credits text

  pCreditText = (char *)malloc( strlen( IQE_CREDITS_TEXT) + 1);   // Allocate temp buffer

  if( pCreditText != NULL) {  // Allocation OK

    strcpy( pCreditText, IQE_CREDITS_TEXT);

    char *pThis, *pFirst;

    pFirst = pCreditText;
    pThis  = pFirst;

    TimeToBreakLoop = false;
    for( ; ;) {

      if( *pThis == '\0' || *pThis == '\n') {  // Is end of line

        if( *pThis == '\n') {       // Is line feed

          *pThis = '\0';            // set end of string
        } else {                    // was end of string

          TimeToBreakLoop = true;   // break loop after this line is outputed
        }

        { Fl_Box* o = new Fl_Box( 16, y, 300, 16, pFirst);
          o->align(Fl_Align(FL_ALIGN_TOP_LEFT|FL_ALIGN_INSIDE));

          if( *pFirst == ' ') {   // line starts with blanks
            // A line starting with a blank is a internet address
            // (color it blue) and is the last line of a credits block.

            o->labelsize( 12);
            o->labelcolor( fl_rgb_color( 0, 0, 255));

            y += 20;
          } else {

            o->labelsize( 14);
            y += 16;
          }
        } // Fl_Box* o

        pFirst = pThis + 1;    // Begin of next line (if any)
      }

      if( TimeToBreakLoop) {  // Was end of string

        break;
      }

      pThis++;
    }
  }

  // button

  y += 32;

  { Fl_Return_Button* o = new Fl_Return_Button( about_panel->w() - 92, y - 32, 83, 25, "Close");
    o->callback((Fl_Callback*)MainHideWinCallback);
  } // Fl_Return_Button* o

  about_panel->size( about_panel->w(), y);  // final size of window

  about_panel->set_modal();
  about_panel->end();

  // show dialog

  about_panel->show();
  while( about_panel->shown()) {

    Fl::wait();
  }

  delete about_panel;

  if( pCreditText != NULL) {

    free( pCreditText);
  }
}

/************************************************************************************
 * Main dialog menues
 */

#define GUI_MENUHEIGHT       24   // Leight of menu bar
#define GUI_LEFT_SIDE_WIDTH 240   // Left side group with

static Fl_Menu_Bar *main_menubar;
static Fl_Menu_Item *main_menu_save_item = 0L;
static Fl_Menu_Item *main_menu_copy_item = 0L;
static Fl_Menu_Item *main_menu_paste_item = 0L;

static Fl_Menu_Item Main_Menu[] = {
{"&File",0,0,0,FL_SUBMENU},
  {"&Open...",        FL_COMMAND+'o', ButtonFileChooseCallback, 0},
  {"&Save...",        FL_COMMAND+'s', ButtonSaveFileCallback, 0, FL_MENU_DIVIDER},
  {"&Quit",           FL_COMMAND+'q', MainExitCallback},
  {0},
{"&Edit",0,0,0,FL_SUBMENU},
  {"&Copy",           FL_COMMAND+'c', IqeB_GUI_But_Tool_Copy_Callback},
  {"&Paste",          FL_COMMAND+'v', IqeB_GUI_But_Tool_Paste_Callback},
  {0},
{"&Tools",0,0,0,FL_SUBMENU},
  {"Display",0,0,0,FL_SUBMENU},
    {"Bo&x",          FL_COMMAND+'x', IqeB_GUI_But_Tool_OpenWin_Callback, (void *)IqeB_GUI_ToolsDispBoxWin},
    {"Colors",                     0, IqeB_GUI_But_Tool_OpenWin_Callback, (void *)IqeB_GUI_ToolsDispColorsWin},
    {0},
  {"Mesh",0,0,0,FL_SUBMENU},
    {"&Transform",    FL_COMMAND+'t', IqeB_GUI_But_Tool_OpenWin_Callback, (void *)IqeB_GUI_ToolsMeshTransformWin},
    {"&Parts",        FL_COMMAND+'p', IqeB_GUI_But_Tool_OpenWin_Callback, (void *)IqeB_GUI_ToolsMeshPartsWin},
    {"&Workbench",    FL_COMMAND+'w', IqeB_GUI_But_Tool_OpenWin_Callback, (void *)IqeB_GUI_ToolsMeshWorkbenchWin},
    {0},
  {"Skeleton",0,0,0,FL_SUBMENU},
    {"Files + utils",              0, IqeB_GUI_But_Tool_OpenWin_Callback, (void *)IqeB_GUI_ToolsSkelNewWin},
    {"Edit &joints",  FL_COMMAND+'j', IqeB_GUI_But_Tool_OpenWin_Callback, (void *)IqeB_GUI_ToolsSkelEditWin},
    {"Skin &binding", FL_COMMAND+'b', IqeB_GUI_But_Tool_OpenWin_Callback, (void *)IqeB_GUI_ToolsSkinBindWin},
    {0},
  {"Animation",0,0,0,FL_SUBMENU},
    {"Manager",                    0, IqeB_GUI_But_Tool_OpenWin_Callback, (void *)IqeB_GUI_ToolsAnimManagerWin},
    {"&Kinect",       FL_COMMAND+'k', IqeB_GUI_But_Tool_OpenWin_Callback, (void *)IqeB_GUI_ToolsAnimKinectWin},
    {"Editor",        FL_COMMAND+'a', IqeB_GUI_But_Tool_OpenWin_Callback, (void *)IqeB_GUI_ToolsAnimEditWin},
    {0},
  {0},
{"&Help",0,0,0,FL_SUBMENU},
  {"&About IqeBrowser...", 0, MainAboutCallback},
  {0},
{0}};

/************************************************************************************
 * Preferences data
 */

// hold preferences data

static Fl_Preferences IqeB_PreferencesData( Fl_Preferences::USER, "ya3dag.de", "IeqBrowser");

// remeber picked up preference settings, used to save a program close

typedef struct {
  const char *pGroupName;               // Name of this group
  Fl_Preferences *pGroup;               // Group for this preference
  T_GUI_PreferenceEntry *pSettings;     // pointer to seting table
  int nSettings;                        // Number of settings for this groupd
  void **ppMyToolWin;                   // If != NULL, pointer to Fl_Window pointer of a tool window
  int *pWinPosX, *pWinPosY;             // point to variable for windows postion
  int LWinPosX, LWinPosY;               // last position of tool window
} T_GUI_PreferenceGroup;

static T_GUI_PreferenceGroup IqeB_PreferenceGroup[ PREFERENCE_MAX_GROUPS];

static int IqeB_PreferenceGroupN = 0;  // number of preference groups

/************************************************************************************
 * IqeB_PreferencesAddGroup
 *
 * Add a group of preferences. Call this only once per group.
 *
 * NOTE: pGroupName must point to a constant string.
 */

static void IqeB_PreferencesAddGroup( const char *pGroupName, T_GUI_PreferenceEntry *pSettings, int nSettings,
                                      void **ppMyToolWin, int *pWinPosX, int *pWinPosY)
{
  int iSetting;
  T_GUI_PreferenceEntry *pSetting;

  if( IqeB_PreferenceGroupN >= PREFERENCE_MAX_GROUPS) {   // secuirty test, table overflows

    return;
  }

  // add to table of preference groups

  IqeB_PreferenceGroup[ IqeB_PreferenceGroupN].pGroupName  = pGroupName;
  IqeB_PreferenceGroup[ IqeB_PreferenceGroupN].pGroup      = NULL;         // is set later
  IqeB_PreferenceGroup[ IqeB_PreferenceGroupN].pSettings   = pSettings;
  IqeB_PreferenceGroup[ IqeB_PreferenceGroupN].nSettings   = nSettings;

  IqeB_PreferenceGroup[ IqeB_PreferenceGroupN].ppMyToolWin = ppMyToolWin;
  IqeB_PreferenceGroup[ IqeB_PreferenceGroupN].pWinPosX    = pWinPosX;
  IqeB_PreferenceGroup[ IqeB_PreferenceGroupN].pWinPosY    = pWinPosY;

  IqeB_PreferenceGroupN += 1;   // one more

  // get default value for this settings

  pSetting = pSettings;

  for( iSetting = 0; iSetting < nSettings; iSetting++, pSetting++) {  // loop over all settings

    switch( pSetting->Type) {

    case PREF_T_INT:

      pSetting->DVal_Int = atoi( pSetting->pDefaultValue);
      break;

    case PREF_T_FLOAT:

      pSetting->DVal_Float = atof( pSetting->pDefaultValue);
      break;

    } // end switch
  } // for( iSetting

  return;
}

/************************************************************************************
 * IqeB_PreferencesUpdateChanges
 *
 * Update changed settings to preferenes and
 * flush the preferences database.
 * Is called on program close and maybe on value changes.
 */

static unsigned int IqeB_PreferencesUpdateChanges_TimeLastCalled;  // rember time last called

void IqeB_PreferencesUpdateChanges()
{
  int iGroup, iSetting, AnyValueChanged;
  T_GUI_PreferenceGroup *pSettingGroup;
  T_GUI_PreferenceEntry *pSetting;

  AnyValueChanged = false;
  IqeB_PreferencesUpdateChanges_TimeLastCalled = IqeB_Disp_GetTickCount();  // rember time last called

  // get window sizes of main dialog

  MyWinSizeX = pGUI_Main->w();  // update window size
  MyWinSizeY = pGUI_Main->h();  // update window size

  //

  pSettingGroup = IqeB_PreferenceGroup;

  for( iGroup = 0; iGroup < IqeB_PreferenceGroupN; iGroup++, pSettingGroup++) {  // loop over all groups

    if( pSettingGroup->pGroup == NULL) {  // Security test

      continue;
    }

    // update window postions

    if( pSettingGroup->ppMyToolWin != NULL &&   // have pointer to pointer
        *pSettingGroup->ppMyToolWin != NULL) {  // and have pointer

      Fl_Window *pThisWin;

      pThisWin = (Fl_Window *)*pSettingGroup->ppMyToolWin; // get pointer to tool window

      if( pSettingGroup->pWinPosX != NULL) {   // X position

        *pSettingGroup->pWinPosX = pThisWin->x_root();  // update window position

        if( pSettingGroup->LWinPosX != *pSettingGroup->pWinPosX) {   // Value has changed

          AnyValueChanged = true;

          pSettingGroup->LWinPosX = *pSettingGroup->pWinPosX;

          pSettingGroup->pGroup->set( "WinPosX", *pSettingGroup->pWinPosX);
        }
      }

      if( pSettingGroup->pWinPosY != NULL) {   // Y position

        *pSettingGroup->pWinPosY = pThisWin->y_root();  // update window position

        if( pSettingGroup->LWinPosY != *pSettingGroup->pWinPosY) {   // Value has changed

          AnyValueChanged = true;

          pSettingGroup->LWinPosY = *pSettingGroup->pWinPosY;

          pSettingGroup->pGroup->set( "WinPosY", *pSettingGroup->pWinPosY);
        }
      }
    }

   // update other settings

    pSetting = pSettingGroup->pSettings;

    for( iSetting = 0; iSetting < pSettingGroup->nSettings; iSetting++, pSetting++) {  // loop over all settings

      switch( pSetting->Type) {

      case PREF_T_INT:
        { int *pValue;

          pValue = (int *)pSetting->pValue;

          if( pSetting->LVal_Int != *pValue) {   // Value has changed

            AnyValueChanged = true;

            pSetting->LVal_Int = *pValue;

            pSettingGroup->pGroup->set( pSetting->pName, *pValue);
          }
        }
        break;

      case PREF_T_FLOAT:
        { float *pValue;

          pValue = (float *)pSetting->pValue;

          if( pSetting->LVal_Float != *pValue) {   // Value has changed

            AnyValueChanged = true;

            pSetting->LVal_Float = *pValue;

            pSettingGroup->pGroup->set( pSetting->pName, *pValue);
          }
        }
        break;

      case PREF_T_STRING:
        { char *pValue;
          int CRC_Value, i, ThisLen;

          pValue = (char *)pSetting->pValue;

          // Calculate CRC

          CRC_Value = 4711;

          ThisLen = strlen( pValue);

          for( i = 0; i < ThisLen; i++) {

            CRC_Value ^= (pValue[ i] & 0xff) << ((i & 3) * 8);
          }

          if( pSetting->LVal_Int != CRC_Value) {   // Value has changed

            AnyValueChanged = true;

            pSetting->LVal_Float = *pValue;

            pSettingGroup->pGroup->set( pSetting->pName, pValue);
          }
        }
        break;

      } // end switch
    } // for( iSetting
  } // for( iGroup

  if( AnyValueChanged) {           // Any value has changed ?

    IqeB_PreferencesData.flush();  // Flush the data base
  }

  return;
}

/************************************************************************************
 * IqeB_PreferencesGetFromFile
 *
 * Get the preferences from the file and store to the vaiables.
 * Only call this one at program start.
 * All preference groups must have been added.
 */

void IqeB_PreferencesGetFromFile()
{
  int iGroup, iSetting;
  T_GUI_PreferenceGroup *pSettingGroup;
  T_GUI_PreferenceEntry *pSetting;

  pSettingGroup = IqeB_PreferenceGroup;

  for( iGroup = 0; iGroup < IqeB_PreferenceGroupN; iGroup++, pSettingGroup++) {  // loop over all groups

    if( pSettingGroup->pGroup == NULL) {

      pSettingGroup->pGroup = new Fl_Preferences( IqeB_PreferencesData, pSettingGroup->pGroupName);

      if( pSettingGroup->pGroup == NULL) {   // secuirty test

        continue;
      }
    }

    // get window postions

    if( pSettingGroup->pWinPosX != NULL) {   // X position
      int ThisValue;

      pSettingGroup->pGroup->get( "WinPosX", ThisValue, IQE_GUI_NO_WINPOS_X);

      pSettingGroup->LWinPosX  = ThisValue;
      *pSettingGroup->pWinPosX = ThisValue;
    }

    if( pSettingGroup->pWinPosY != NULL) {   // Y position
      int ThisValue;

      pSettingGroup->pGroup->get( "WinPosY", ThisValue, IQE_GUI_NO_WINPOS_Y);

      pSettingGroup->LWinPosY  = ThisValue;
      *pSettingGroup->pWinPosY = ThisValue;
    }

    // get other settings

    pSetting = pSettingGroup->pSettings;

    for( iSetting = 0; iSetting < pSettingGroup->nSettings; iSetting++, pSetting++) {  // loop over all settings

      switch( pSetting->Type) {

      case PREF_T_INT:
        { int *pValue, ThisValue;

          pValue = (int *)pSetting->pValue;

          pSettingGroup->pGroup->get( pSetting->pName, ThisValue, pSetting->DVal_Int);

          pSetting->LVal_Int = ThisValue;
          *pValue = ThisValue;
        }
        break;

      case PREF_T_FLOAT:
        { float *pValue, ThisValue;

          pValue = (float *)pSetting->pValue;

          pSettingGroup->pGroup->get( pSetting->pName, ThisValue, pSetting->DVal_Float);

          pSetting->LVal_Float = ThisValue;
          *pValue = ThisValue;
        }
        break;

      case PREF_T_STRING:
        { char *pValue;
          char *pDefault;
          int defaultSize, maxSize, i, ThisLen;

          pValue   = (char *)pSetting->pValue;
          pDefault = (char *)pSetting->pDefaultValue;
          if( pDefault == NULL) {

            pDefault = (char *)"";
          }

          defaultSize = strlen( pDefault) + 1;
          maxSize = pSetting->SizeOfString;

          if( maxSize > 0 && defaultSize > maxSize) { // Test default size not OK

            pDefault = (char *)"";
            defaultSize = strlen( pDefault) + 1;
          }

          if( maxSize > 0) {   // Need enough space for string

            pSettingGroup->pGroup->get( pSetting->pName, pValue, pDefault, maxSize);
          }

          // Calulate a CRC-Sum to test for Changes

          pSetting->LVal_Int = 4711;

          ThisLen = strlen( pValue);

          for( i = 0; i < ThisLen; i++) {

            pSetting->LVal_Int ^= (pValue[ i] & 0xff) << ((i & 3) * 8);
          }
        }
        break;

      } // end switch
    } // for( iSetting
  } // for( iGroup

  return;
}

/************************************************************************************
 * Presets for the Main_GUI
 *
 * Main Window GUI
 */

 static T_GUI_PreferenceEntry MyPreferences[] =
 {

  // Window size

  { PREF_T_INT, "WinSizeX", "100", &MyWinSizeX},  // NOTE: values will be clipped against MYWIN_SIZE_X_MIN / MYWIN_SIZE_Y_MIN
  { PREF_T_INT, "WinSizeY", "100", &MyWinSizeY},

  // File brower group

  { PREF_T_STRING, "FileBrowserLastDir",  "",  GUI_CurrentDirectory, sizeof( GUI_CurrentDirectory) - 1},

  // Save file group

  { PREF_T_INT, "SaveFileCBoxCopyTex",    "1", &IqeB_SaveFile_CopyTex},
  { PREF_T_INT, "SaveFileCBoxTangent",    "1", &IqeB_SaveFile_Tangent},
  { PREF_T_INT, "SaveFileCBoxAdjacency",  "0", &IqeB_SaveFile_Adjacency},
  { PREF_T_INT, "SaveFileCBoxModel",      "1", &IqeB_SaveFile_Model},
  { PREF_T_INT, "SaveFileCBoxPoses",      "1", &IqeB_SaveFile_Poses},
  { PREF_T_INT, "SaveFileTypeToSave",     "0", &IqeB_SaveFile_Type},          // Default: IQE_EXPORT_FILE_TYPE_IQM

  // Display style settings

  { PREF_T_INT, "DispStyle_ViewMode",     "7",  &IqeB_DispStyle_ViewMode},    // Default: IQE_VIEWMODE_PERSP
  { PREF_T_INT, "DispStyle_NumViewPorts", "0", &IqeB_DispStyle_NumViewPorts}, // Default: IQE_VIEW_PORT_NUM_1
  { PREF_T_INT, "DispStyle_doplane",      "1", &IqeB_DispStyle_doplane},
  { PREF_T_INT, "DispStyle_dowire",       "0", &IqeB_DispStyle_dowire},
  { PREF_T_INT, "DispStyle_donormals",    "0", &IqeB_DispStyle_donormals},
  { PREF_T_INT, "DispStyle_donormals",    "1", &IqeB_DispStyle_donormals},
  { PREF_T_INT, "DispStyle_dobackface",   "1", &IqeB_DispStyle_dobackface},
  { PREF_T_INT, "DispStyle_doskeleton",   "1", &IqeB_DispStyle_doskeleton},
  { PREF_T_INT, "DispStyle_doJointNames", "0", &IqeB_DispStyle_doJointNames},
  { PREF_T_INT, "DispStyle_doBlendWCols", "0", &IqeB_DispStyle_doBlendWeightColors},
  { PREF_T_INT, "DispStyle_doAnimations", "1", &IqeB_DispStyle_doAnimations},
  { PREF_T_INT, "DispStyle_doPlay",       "1", &IqeB_DispStyle_doplay},
  { PREF_T_INT, "DispStyle_PlayOnce",     "0", &IqeB_DispStyle_doPlayOnce},
};

// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "GUI_Main", MyPreferences, sizeof( MyPreferences) / sizeof( T_GUI_PreferenceEntry),
                                               (void **)(&pGUI_Main), &MyWinPosX, &MyWinPosY);

// Helper class IqeB_PreferencesGroup
// Automatic add preference settings at startup of the program.

/**
  The constructor adds a preference setting group.
*/
IqeB_PreferencesGroup::IqeB_PreferencesGroup( const char *pGroupName, T_GUI_PreferenceEntry *pSettings, int nSettings,
                                              void **ppMyToolWin, int *pWinPosX, int *pWinPosY)
{

  IqeB_PreferencesAddGroup( pGroupName, pSettings, nSettings, ppMyToolWin, pWinPosX, pWinPosY);
}

/**
  The destructor has nothing to do.
*/
IqeB_PreferencesGroup::~IqeB_PreferencesGroup()
{
}

/************************************************************************************
 * IqeB_MainWindow_GUI_Setup
 *
 * Main Window GUI
 */

static void  IqeB_MainWindow_GUI_Setup( Fl_Window *pWin)
{
  int x, x1, x2, x3, y, yy, xx, xx2, hWin, wWin;
  int WinOpenGL_x, WinOpenGL_y, WinOpenGL_xx, WinOpenGL_yy;
  Fl_Box *pBoxTemp;
  Fl_Group *pGroupTemp;
  Fl_Radio_Round_Button *pRadioButTemp;

  //

  hWin = pWin->h();
  wWin = pWin->w();

  //
  // main menu
  //

  main_menubar = new Fl_Menu_Bar( 4, 0, GUI_LEFT_SIDE_WIDTH, GUI_MENUHEIGHT);
  main_menubar->menu( Main_Menu);
  main_menubar->selection_color( Fl::get_color( FL_SELECTION_COLOR));

  main_menu_save_item  = (Fl_Menu_Item*)main_menubar->find_item( ButtonSaveFileCallback);
  main_menu_copy_item  = (Fl_Menu_Item*)main_menubar->find_item( IqeB_GUI_But_Tool_Copy_Callback);
  main_menu_paste_item = (Fl_Menu_Item*)main_menubar->find_item( IqeB_GUI_But_Tool_Paste_Callback);

  //
  // Group, left side op OpenGL window
  //

  x = 4;

  xx = GUI_LEFT_SIDE_WIDTH;

  pGUI_GroupLeftSide = new Fl_Group( x, GUI_MENUHEIGHT, xx, hWin);

  //
  // file browser
  //

#ifdef use_again
  yy = (hWin - GUI_MENUHEIGHT) / 2;
#else
  yy = 366;
#endif

  pGUI_FileBrowser = new DropFileBrowser( x, GUI_MENUHEIGHT + 20, xx, yy - 26, "File browser");

  pGUI_FileBrowser->align(FL_ALIGN_TOP_LEFT);     // align for label

  pGUI_FileBrowser->type(FL_HOLD_BROWSER);        // use for single selection
  //pGUI_FileBrowser.type(FL_MULTI_BROWSER);      // use for multiple selection
  pGUI_FileBrowser->callback( FileBrowserCallback, (void*)pWin);

  // contruct and set the file filter

  strcpy( GUI_FileExtensionList, "*.{");   // search pattern header
  IqeB_ImportFileExtensions( GUI_FileExtensionList + strlen( GUI_FileExtensionList),
  sizeof( GUI_FileExtensionList) - strlen( GUI_FileExtensionList) - 2); // get file extensions we support
  strcat( GUI_FileExtensionList, "}");    // search pattern footer

  pGUI_FileBrowser->filter( GUI_FileExtensionList);  // load directory listing of current directory

  // load from the current directory
  pGUI_FileBrowser->load( GUI_CurrentDirectory, fl_casenumericsort);  // load directory listing of current directory

  // File choose booton

  pGUI_FileChoose = new Fl_Button( x + xx - 30, GUI_MENUHEIGHT + 3, 30, 16, "...");
  pGUI_FileChoose->callback( ButtonFileChooseCallback);
  pGUI_FileChoose->tooltip( "Choose a file");

  //
  // Display render options
  //

  y = pGUI_FileBrowser->y() + pGUI_FileBrowser->h();

  y += 20;

  yy = 16;

  pBoxTemp = new Fl_Box( x, y, xx, yy * 4 + 6, "Render options");
  pBoxTemp->box( FL_DOWN_FRAME);
  pBoxTemp->align(FL_ALIGN_TOP_LEFT);     // align for label

  y += 4;

  xx2 = (xx - 8) / 2;
  x1 = x + 4;
  x2 = x + xx / 2;

  pGUI_CBox_DispStyle_dowire = new Fl_Check_Button( x1, y, xx2, yy, "Wireframe");
  pGUI_CBox_DispStyle_dowire->tooltip("Draw wireframe");
  pGUI_CBox_DispStyle_dowire->value( IqeB_DispStyle_dowire);
  pGUI_CBox_DispStyle_dowire->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_DispStyle_dowire);

  pGUI_CBox_DispStyle_doplane = new Fl_Check_Button( x2, y, xx2, yy, "Ground plane");
  pGUI_CBox_DispStyle_doplane->tooltip("Draw ground plane");
  pGUI_CBox_DispStyle_doplane->value( IqeB_DispStyle_doplane);
  pGUI_CBox_DispStyle_doplane->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_DispStyle_doplane);

  y += yy;

  pGUI_CBox_DispStyle_dotexture = new Fl_Check_Button( x1, y, xx2, yy, "Texture");
  pGUI_CBox_DispStyle_dotexture->tooltip("Draw with textures");
  pGUI_CBox_DispStyle_dotexture->value( IqeB_DispStyle_dotexture);
  pGUI_CBox_DispStyle_dotexture->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_DispStyle_dotexture);

  pGUI_CBox_DispStyle_doskeleton = new Fl_Check_Button( x2, y, xx2, yy, "Skeleton");
  pGUI_CBox_DispStyle_doskeleton->tooltip("Draw skeleton (if there is one)");
  pGUI_CBox_DispStyle_doskeleton->value( IqeB_DispStyle_doskeleton);
  pGUI_CBox_DispStyle_doskeleton->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_DispStyle_doskeleton);

  y += yy;

  pGUI_CBox_DispStyle_dobackface = new Fl_Check_Button( x1, y, xx2, yy, "Both faces");
  pGUI_CBox_DispStyle_dobackface->tooltip("Draw both faches of a triangle.\nNo backface culling");
  pGUI_CBox_DispStyle_dobackface->value( IqeB_DispStyle_dobackface);
  pGUI_CBox_DispStyle_dobackface->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_DispStyle_dobackface);

  pGUI_CBox_DispStyle_doJointNames = new Fl_Check_Button( x2, y, xx2, yy, "Joint names");
  pGUI_CBox_DispStyle_doJointNames->tooltip("Draw joint names");
  pGUI_CBox_DispStyle_doJointNames->value( IqeB_DispStyle_doJointNames);
  pGUI_CBox_DispStyle_doJointNames->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_DispStyle_doJointNames);

  y += yy;

  pGUI_CBox_DispStyle_donormals = new Fl_Check_Button( x1, y, xx2, yy, "Normals");
  pGUI_CBox_DispStyle_donormals->tooltip("Draw normals");
  pGUI_CBox_DispStyle_donormals->value( IqeB_DispStyle_donormals);
  pGUI_CBox_DispStyle_donormals->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_DispStyle_donormals);

  pGUI_CBox_DispStyle_doBlendWeightColors = new Fl_Check_Button( x2, y, xx2, yy, "Blend weight");
  pGUI_CBox_DispStyle_doBlendWeightColors->tooltip("Visualize blend weights.\nOverwrites 'texture' checkbox setting.");
  pGUI_CBox_DispStyle_doBlendWeightColors->value( IqeB_DispStyle_doBlendWeightColors);
  pGUI_CBox_DispStyle_doBlendWeightColors->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_DispStyle_doBlendWeightColors);

  y = pBoxTemp->y() + pBoxTemp->h();

  //
  // Display viewport type
  //

  y += 20;

  yy = 16;

  pBoxTemp = new Fl_Box( x, y, xx, yy * 4 + 12, "Viewport");
  pBoxTemp->box( FL_DOWN_FRAME);
  pBoxTemp->align(FL_ALIGN_TOP_LEFT);     // align for label

  y += 4;

  xx2 = (xx - 12) / 3;
  x1 = x + 4;
  x2 = x1 + xx2 + 4;
  x3 = x + xx - xx2 - 4;

  pRadioButTemp = new Fl_Radio_Round_Button( x1, y, xx2, yy, "&Front");
  pRadioButTemp->tooltip("2D camera front view\nY/Z plane");
  pRadioButTemp->callback( IqeB_DispStyle_ViewMode_Callback, (void *)IQE_VIEWMODE_YZ_FRONT);
  if( IqeB_DispStyle_ViewMode == IQE_VIEWMODE_YZ_FRONT) pRadioButTemp->value( 1);

  pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx2, yy, "Back");
  pRadioButTemp->tooltip("2D camera back view\nY/Z plane");
  pRadioButTemp->callback( IqeB_DispStyle_ViewMode_Callback, (void *)IQE_VIEWMODE_YZ_BACK);
  if( IqeB_DispStyle_ViewMode == IQE_VIEWMODE_YZ_BACK) pRadioButTemp->value( 1);

  pRadioButTemp = new Fl_Radio_Round_Button( x3, y, xx2, yy, "Ortho.");
  pRadioButTemp->tooltip("3D camera view\northogonal camera");
  pRadioButTemp->callback( IqeB_DispStyle_ViewMode_Callback, (void *)IQE_VIEWMODE_ORTHO);
  if( IqeB_DispStyle_ViewMode == IQE_VIEWMODE_ORTHO) pRadioButTemp->value( 1);

  y += yy;

  pRadioButTemp = new Fl_Radio_Round_Button( x1, y, xx2, yy, "&Right");
  pRadioButTemp->tooltip("2D camera right view\nZ/X plane");
  pRadioButTemp->callback( IqeB_DispStyle_ViewMode_Callback, (void *)IQE_VIEWMODE_ZX_RIGHT);
  if( IqeB_DispStyle_ViewMode == IQE_VIEWMODE_ZX_RIGHT) pRadioButTemp->value( 1);

  pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx2, yy, "Left");
  pRadioButTemp->tooltip("2D camera left view\nZ/X plane");
  pRadioButTemp->callback( IqeB_DispStyle_ViewMode_Callback, (void *)IQE_VIEWMODE_ZX_LEFT);
  if( IqeB_DispStyle_ViewMode == IQE_VIEWMODE_ZX_LEFT) pRadioButTemp->value( 1);

  pRadioButTemp = new Fl_Radio_Round_Button( x3, y, xx2, yy, "&Persp.");
  pRadioButTemp->tooltip("3D camera view\nperspective camera");
  pRadioButTemp->callback( IqeB_DispStyle_ViewMode_Callback, (void *)IQE_VIEWMODE_PERSP);
  if( IqeB_DispStyle_ViewMode == IQE_VIEWMODE_PERSP) pRadioButTemp->value( 1);

  y += yy;

  pRadioButTemp = new Fl_Radio_Round_Button( x1, y, xx2, yy, "&Top");
  pRadioButTemp->tooltip("2D camera top view\nX/Y plane");
  pRadioButTemp->callback( IqeB_DispStyle_ViewMode_Callback, (void *)IQE_VIEWMODE_XY_TOP);
  if( IqeB_DispStyle_ViewMode == IQE_VIEWMODE_XY_TOP) pRadioButTemp->value( 1);

  pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx2, yy, "Bottom");
  pRadioButTemp->tooltip("2D camera bottom view\nX/Y plane");
  pRadioButTemp->callback( IqeB_DispStyle_ViewMode_Callback, (void *)IQE_VIEWMODE_XY_BOTTOM);
  if( IqeB_DispStyle_ViewMode == IQE_VIEWMODE_XY_BOTTOM) pRadioButTemp->value( 1);

  y += yy;
  y += 4;

  pGroupTemp = new Fl_Group( x1, y, x3 - x1, yy);  // Group around this radio buttons

  pRadioButTemp = new Fl_Radio_Round_Button( x1, y, xx2, yy, "&1 View");
  pRadioButTemp->tooltip("The display area shows 1 model view");
  pRadioButTemp->callback( IqeB_DispStyle_NumViewPorts_Callback, (void *)IQE_VIEW_PORT_NUM_1);
  if( IqeB_DispStyle_NumViewPorts == IQE_VIEW_PORT_NUM_1) pRadioButTemp->value( 1);

  pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx2, yy, "&4 Views");
  pRadioButTemp->tooltip("The display area shows 4 model views");
  pRadioButTemp->callback( IqeB_DispStyle_NumViewPorts_Callback, (void *)IQE_VIEW_PORT_NUM_4);
  if( IqeB_DispStyle_NumViewPorts == IQE_VIEW_PORT_NUM_4) pRadioButTemp->value( 1);

  pGroupTemp->end();

  // ...

  pGUI_But_View_Reset = new Fl_Button( x3 + 2, pBoxTemp->y() + pBoxTemp->h() - 26, xx2 - 2, 22, "Reset");
  pGUI_But_View_Reset->callback( IqeB_DispStyle_View_Reset_Callback, NULL);
  pGUI_But_View_Reset->tooltip( "Reset view");

  y = pBoxTemp->y() + pBoxTemp->h();

  //
  // Animation
  //

  y += 20;

  yy = 16;

  pBoxTemp = new Fl_Box( x, y, xx, yy * 3 + 6, "Animation");
  pBoxTemp->box( FL_DOWN_FRAME);
  pBoxTemp->align(FL_ALIGN_TOP_LEFT);     // align for label

  y += 4;

  xx2 = (xx - 4 - 16) / 7;
  x1 = x + 4;

  pGUI_CBox_Anim_Enable = new Fl_Check_Button( x1, y, 70, yy, "Animate");
  pGUI_CBox_Anim_Enable->tooltip("Play animation(s)");
  pGUI_CBox_Anim_Enable->value( IqeB_DispStyle_doAnimations);
  pGUI_CBox_Anim_Enable->callback( AnimationGUICallback, &IqeB_DispStyle_doAnimations);

  x2 = pGUI_CBox_Anim_Enable->x() + pGUI_CBox_Anim_Enable->w() + 2;
  pGUI_Choice_Anim_Select = new Fl_Choice( x2, y, pGUI_CBox_Anim_Enable->x() + xx - x2 - 8, 22);
  pGUI_Choice_Anim_Select->callback( AnimationGUICallback, NULL);
  pGUI_Choice_Anim_Select->tooltip( "Select animation");

  pGUI_But_Anim_Play = new Fl_Button( x1 + (xx2 + 4) * 0, pBoxTemp->y() + pBoxTemp->h() - 32, xx2, 28,
  IqeB_DispStyle_doplay ? "@>" : "@+3||");
  pGUI_But_Anim_Play->callback( AnimationGUICallback, NULL);
  pGUI_But_Anim_Play->tooltip( "Play/Pause");

  pGUI_CBox_Anim_PlayOnce = new Fl_Check_Button( x1 + (xx2 + 2) * 1, pBoxTemp->y() + pBoxTemp->h() - 26, xx2, 22, "1");
  pGUI_CBox_Anim_PlayOnce->tooltip("Play animation once");
  pGUI_CBox_Anim_PlayOnce->value( IqeB_DispStyle_doPlayOnce);
  pGUI_CBox_Anim_PlayOnce->callback( AnimationGUICallback, &IqeB_DispStyle_doPlayOnce);

  pGUI_But_Anim_FirstF = new Fl_Button( x1 + (xx2 + 2) * 2, pBoxTemp->y() + pBoxTemp->h() - 26, xx2, 22, "@$>|");
  pGUI_But_Anim_FirstF->callback( AnimationGUICallback, NULL);
  pGUI_But_Anim_FirstF->tooltip( "First animation frame");

  pGUI_But_Anim_PrevF = new Fl_Button( x1 + (xx2 + 2) * 3, pBoxTemp->y() + pBoxTemp->h() - 26, xx2, 22, "@<");
  pGUI_But_Anim_PrevF->callback( AnimationGUICallback, NULL);
  pGUI_But_Anim_PrevF->tooltip( "Previous animation frame");

  pGUI_But_Anim_NextF = new Fl_Button( x1 + (xx2 + 2) * 4, pBoxTemp->y() + pBoxTemp->h() - 26, xx2, 22, "@>");
  pGUI_But_Anim_NextF->callback( AnimationGUICallback, NULL);
  pGUI_But_Anim_NextF->tooltip( "Next animation frame");

  pGUI_But_Anim_Prev = new Fl_Button( x1 + (xx2 + 2) * 5, pBoxTemp->y() + pBoxTemp->h() - 26, xx2, 22, "@<<");
  pGUI_But_Anim_Prev->callback( AnimationGUICallback, NULL);
  pGUI_But_Anim_Prev->tooltip( "Previous animation");

  pGUI_But_Anim_Next = new Fl_Button( x1 + (xx2 + 2) * 6, pBoxTemp->y() + pBoxTemp->h() - 26, xx2, 22, "@>>");
  pGUI_But_Anim_Next->callback( AnimationGUICallback, NULL);
  pGUI_But_Anim_Next->tooltip( "Next Animation");

  y = pBoxTemp->y() + pBoxTemp->h();

  //
  // Save to file
  //

  y += 20;

  yy = 16;

  pBoxTemp = new Fl_Box( x, y, xx, yy * 5 + 6, "Save to file");
  pBoxTemp->box( FL_DOWN_FRAME);
  pBoxTemp->align(FL_ALIGN_TOP_LEFT);     // align for label

  y += 4;

  xx2 = (xx - 12) / 3;
  x1 = x + 4;
  x2 = x1 + xx2 - 6;
  x3 = x + xx - xx2 - 12;

  // Texture export
  pGUI_CBox_SaveFile_CopyTex = new Fl_Check_Button( x1, y, xx2, yy, "Texture");
  pGUI_CBox_SaveFile_CopyTex->tooltip("The texture/image files are copied and\nthe material reference is adapted.");
  pGUI_CBox_SaveFile_CopyTex->value( IqeB_SaveFile_CopyTex);
  pGUI_CBox_SaveFile_CopyTex->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_SaveFile_CopyTex);

  // Tangent export
  pGUI_CBox_SaveFile_Tangent = new Fl_Check_Button( x2, y, xx2, yy, "Tangent");
  pGUI_CBox_SaveFile_Tangent->tooltip("Generate texture tangent data.\nIncreases the file size.");
  pGUI_CBox_SaveFile_Tangent->value( IqeB_SaveFile_Tangent);
  pGUI_CBox_SaveFile_Tangent->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_SaveFile_Tangent);

  // Adjacency export
  pGUI_CBox_SaveFile_Adjacency = new Fl_Check_Button( x3, y, xx2, yy, "Adjacency");
  pGUI_CBox_SaveFile_Adjacency->tooltip("Generate triangle adjacency data.\nIncreases the file size.");
  pGUI_CBox_SaveFile_Adjacency->value( IqeB_SaveFile_Adjacency);
  pGUI_CBox_SaveFile_Adjacency->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_SaveFile_Adjacency);

  y += 20;

  // Model export
  pGUI_CBox_SaveFile_Model = new Fl_Check_Button( x1, y, xx2, yy, "Model");
  pGUI_CBox_SaveFile_Model->tooltip("Store all meshes with joints and base frame.");
  pGUI_CBox_SaveFile_Model->value( IqeB_SaveFile_Model);
  pGUI_CBox_SaveFile_Model->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_SaveFile_Model);

  // Poses export
  pGUI_CBox_SaveFile_Poses = new Fl_Check_Button( x2, y, xx2, yy, "Poses");
  pGUI_CBox_SaveFile_Poses->tooltip("Store animations with all poses.");
  pGUI_CBox_SaveFile_Poses->value( IqeB_SaveFile_Poses);
  pGUI_CBox_SaveFile_Poses->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_SaveFile_Poses);

  y += yy;

  pGUI_But_SaveFile = new Fl_Button( x3 + 2, pBoxTemp->y() + pBoxTemp->h() - 26, xx2 - 2, 22, "&Save");
  pGUI_But_SaveFile->callback( ButtonSaveFileCallback);
  pGUI_But_SaveFile->tooltip( "Save displayed model to file");

  pGUI_SaveFile_Type = new Fl_Choice( x2, pBoxTemp->y() + pBoxTemp->h() - 26, x3 - x2 - 4, 22);
  pGUI_SaveFile_Type->callback( SaveFileTypeGUICallback, NULL);
  pGUI_SaveFile_Type->tooltip( "Type of file to save");

  pGUI_SaveFile_Type->add( ".iqm");
  pGUI_SaveFile_Type->add( ".iqe");
  pGUI_SaveFile_Type->value( IqeB_SaveFile_Type);   // select this

  y = pBoxTemp->y() + pBoxTemp->h();

  //
  // ...
  //

  pGUI_GroupLeftSide->end();             // end this group
  pGUI_GroupLeftSide->resizable( 0);     // nothing in this group is resiable

  //
  // OpenGL window
  //

  x = pGUI_GroupLeftSide->x() + pGUI_GroupLeftSide->w() + 4;

  // OpenGL window on screen on this place, will be created later
  WinOpenGL_x = x;
  WinOpenGL_y = 5;
  WinOpenGL_xx = wWin - x - 5;
  WinOpenGL_yy = hWin - 10;

  // frame around the OpenGL Window
  pBoxTemp = new Fl_Box( WinOpenGL_x - 3, WinOpenGL_y - 3, WinOpenGL_xx + 6, WinOpenGL_yy + 6);
  pBoxTemp->box( FL_DOWN_FRAME);
  pBoxTemp->align(FL_ALIGN_TOP_LEFT);     // align for label

  // OpenGL window is created now

  pGUI_WinOpenGL = new MyGlWindow( WinOpenGL_x, WinOpenGL_y, WinOpenGL_xx, WinOpenGL_yy);

  //
  // Layout end work
  //

  pWin->end();

  pWin->resizable( pGUI_WinOpenGL);                                  // the OpenGL window is resiable
  pWin->size_range( MYWIN_SIZE_X_MIN, MYWIN_SIZE_Y_MIN, MYWIN_SIZE_X_MAX, MYWIN_SIZE_Y_MAX); // minimum window size
}

/************************************************************************************
 * main
 */

int main(int argc, char **argv)
{
  int  TextLen;
  char *pStartupFile;

#ifdef WIN32
  // Protect against multiple running of this application

  hHandleMultipleRunning = CreateMutex( NULL, TRUE, WIN_DEFAULT_TITLE);
  if( ERROR_ALREADY_EXISTS == GetLastError() ) {

    // Program already running somewhere
    return(1); // Exit program
  }
#endif

	Swap_Init();                              // setup byte swap things

	//Fl::scheme( "plastic");    // Sets the current widget scheme.
	//Fl::scheme( "gtk+");
	//Fl::scheme( "gleam");

  Fl::get_system_colors();
  Fl::foreground(  185, 236, 255);   // adapt color scheme

  Fl::background(   96, 160, 255);
  //Fl::background2( 255, 255, 255);
  Fl::background2( 255, 255, 200);
  Fl::set_color( FL_SELECTION_COLOR, 97, 255, 140);
  //Fl::set_color( FL_INACTIVE_COLOR, 0, 64, 0);

  // Pick background color, is used by OpenGL view.
  GUI_BackgroundColor = Fl::get_color( FL_BACKGROUND_COLOR);

  // load preference data from file

  IqeB_PreferencesGetFromFile();

  // get current directroy for the file browser

  if( GUI_CurrentDirectory[ 0] == '\0') {   // Current directory not set

    fl_getcwd( GUI_CurrentDirectory, sizeof( GUI_CurrentDirectory) - 256);
  }

  pStartupFile = NULL;    // prest no startup file

	if (argc > 1) {

    pStartupFile = argv[1];
  } else {

#ifdef use_again
#ifdef _DEBUG
  	pStartupFile = (char *)"Test/MD5/Bob.md5mesh";
  	//pStartupFile = (char *)"Test/tris.md2";
#endif
#endif
  }

  if( pStartupFile != NULL) {

    char *p, SaveC;

    fl_filename_absolute( GUI_StartupFile, sizeof( GUI_StartupFile) - 256, pStartupFile);

    pStartupFile = GUI_StartupFile;   // set pointer to absolute filename

    p = strrchr( GUI_StartupFile, '/');
    if( !p) p = strrchr( GUI_StartupFile, '\\');
    if( p) {     // point to last path slash

      SaveC = *p;              // save this char
      *p = '\0';               // set end of string

      strcpy( GUI_CurrentDirectory, GUI_StartupFile);

      *p = SaveC;              // restore
    }
  }

  // enshure we have a path character at the end of the string

  TextLen = strlen( GUI_CurrentDirectory);
  if( TextLen > 0 && GUI_CurrentDirectory[ TextLen - 1] != '/' && GUI_CurrentDirectory[ TextLen - 1] != '\\') {

    strcat( GUI_CurrentDirectory, "/");   // add it
  }

  // construct GUI

  // clip window sizes

  if( MyWinSizeX < MYWIN_SIZE_X_MIN) MyWinSizeX = MYWIN_SIZE_X_MIN;
  if( MyWinSizeX > MYWIN_SIZE_X_MAX) MyWinSizeX = MYWIN_SIZE_X_MAX;

  if( MyWinSizeY < MYWIN_SIZE_Y_MIN) MyWinSizeY = MYWIN_SIZE_Y_MIN;
  if( MyWinSizeY > MYWIN_SIZE_Y_MAX) MyWinSizeY = MYWIN_SIZE_Y_MAX;

 // create the window

  if( MyWinPosX != IQE_GUI_NO_WINPOS_X && MyWinPosY != IQE_GUI_NO_WINPOS_Y) { // have last window position

    pGUI_Main = new Fl_Window( MyWinPosX, MyWinPosY, MyWinSizeX, MyWinSizeY, WIN_DEFAULT_TITLE);
  } else {

    pGUI_Main = new Fl_Window( MyWinSizeX, MyWinSizeY, WIN_DEFAULT_TITLE);
  }

  if( pGUI_Main == NULL) {  // security test

    return( 0);
  }

  IqeB_MainWindow_GUI_Setup( pGUI_Main);   // Main Window GUI

	IqeB_TextureInitChecker();

  Fl::add_idle( IqeB_GUI_OpenGLIdleAction);      // Redraw OpenGL window during idle

  pGUI_Main->callback( MainExitCallback);        // Press the close button --> close program

  // Set icon for window
#ifdef WIN32
  pGUI_Main->icon((char *)LoadIcon( fl_display, MAKEINTRESOURCE( 101)));
#endif

  pGUI_Main->show();

  IqeB_DispPrepareNewModel( NULL, NULL);  // Prepare some variables

  return Fl::run();
}

/************************************************************************************
 * IqeB_GUI_OpenGLUpdateGUISettingVariable
 *
 *  Update variable and GUI element
 */

void IqeB_GUI_OpenGLUpdateGUISettingVariable( int *pValue, int NewValue)
{
  Fl_Check_Button *pCBoxThis;

  // pGUI_CBox_DispStyle_doskeleton

  pCBoxThis  = pGUI_CBox_DispStyle_doskeleton;
  if( pCBoxThis != NULL && pCBoxThis->user_data() == pValue) { // is different

    if( pCBoxThis->value() != NewValue) {
      *pValue = NewValue;
      pCBoxThis->value( NewValue);
    }
    return;
  }

  // pGUI_CBox_DispStyle_doperspective

  pCBoxThis  = pGUI_CBox_DispStyle_doperspective;
  if( pCBoxThis != NULL && pCBoxThis->user_data() == pValue) { // is different

    if( pCBoxThis->value() != NewValue) {
      *pValue = NewValue;
      pCBoxThis->value( NewValue);
    }
    return;
  }

  // pGUI_CBox_Anim_Enable

  pCBoxThis  = pGUI_CBox_Anim_Enable;
  if( pCBoxThis != NULL && pCBoxThis->user_data() == pValue) { // is different

    if( pCBoxThis->value() != NewValue) {
      *pValue = NewValue;
      pCBoxThis->value( NewValue);
    }
    return;
  }

  // pGUI_CBox_Anim_PlayOnce

  pCBoxThis  = pGUI_CBox_Anim_PlayOnce;
  if( pCBoxThis != NULL && pCBoxThis->user_data() == pValue) { // is different

    if( pCBoxThis->value() != NewValue) {
      *pValue = NewValue;
      pCBoxThis->value( NewValue);
    }
    return;
  }

  // pGUI_CBox_DispStyle_doBlendWeightColors

  pCBoxThis  = pGUI_CBox_DispStyle_doBlendWeightColors;
  if( pCBoxThis != NULL && pCBoxThis->user_data() == pValue) { // is different

    if( pCBoxThis->value() != NewValue) {
      *pValue = NewValue;
      pCBoxThis->value( NewValue);
    }
    return;
  }

  // Play animation button

  if( &IqeB_DispStyle_doplay == pValue) {

    if( IqeB_DispStyle_doplay != NewValue) {
      IqeB_DispStyle_doplay = NewValue;
      pGUI_But_Anim_Play->label( IqeB_DispStyle_doplay ? "@>" : "@+3||");
      IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
    }
    return;
  }

  // no GUI element found, upddate anyway

  *pValue = NewValue;

}

/************************************************************************************
 * IqeB_GUI_OpenGLIdleAction
 */

void IqeB_GUI_OpenGLIdleAction( void *)
{

  // update preferences all 10 seconds

  unsigned int TimeStamp;

  TimeStamp = IqeB_Disp_GetTickCount();

  if( TimeStamp - IqeB_PreferencesUpdateChanges_TimeLastCalled >= 10000) {  // more than 10 sconds gone

    IqeB_PreferencesUpdateChanges();  // update preferences
  }

  // have to redraw OpenGL window

  if( IqeB_DispTestRedraw() ) {  // have to redraw

    if( pGUI_WinOpenGL) {
      pGUI_WinOpenGL->redraw();    // Redraw the OpenGL window
    }
  }
}

/************************************************************************************
 * IqeB_GUI_SetWindowTitle
 */

void IqeB_GUI_SetWindowTitle( char *pTitle)
{

  pGUI_Main->label( pTitle);
}

/************************************************************************************
 * IqeB_GUI_UpdateWidgets
 *
 */

void IqeB_GUI_WidgetActivate( void *wArg, int ActivateIt)
{
  Fl_Widget *w;

  w = (Fl_Widget *)wArg;

  if( w == NULL) {   // security test

    return;
  }

  if( ActivateIt) {
    if( ! w->active()) {
      w->activate();
    }
  } else {
    if( w->active()) {
      w->deactivate();
    }
  }
}

/************************************************************************************
 * IqeB_GUI_ItemActivate
 *
 */

static void IqeB_GUI_ItemActivate( void *wArg, int ActivateIt)
{
  Fl_Menu_Item *w;

  w = (Fl_Menu_Item *)wArg;

  if( w == NULL) {   // security test

    return;
  }

  if( ActivateIt) {
    if( ! w->active()) {
      w->activate();
    }
  } else {
    if( w->active()) {
      w->deactivate();
    }
  }
}

void IqeB_GUI_UpdateWidgets( int UpdateFlags)
{

  // Update some display things

  // Don't change 'skeleton' checkbox for edit mode skeleton
  IqeB_GUI_WidgetActivate( pGUI_CBox_DispStyle_doskeleton, IqeB_DispStyle_MouseMode != IQE_GUI_MOUSE_MODE_SKEL_EDIT &&
                                                           IqeB_DispStyle_MouseMode != IQE_GUI_MOUSE_MODE_JOINT_SEL &&
                                                           IqeB_DispStyle_MouseMode != IQE_GUI_MOUSE_MODE_ANIM_EDIT);

  // Update animaton things

  int i, doAnim, NumAnims;

  // No animation for edit mode skeleton
  doAnim = IqeB_DispStyle_doAnimations && IqeB_DispStyle_MouseMode != IQE_GUI_MOUSE_MODE_SKEL_EDIT &&
                                          IqeB_DispStyle_MouseMode != IQE_GUI_MOUSE_MODE_JOINT_SEL;

  NumAnims = 0;
  if( pDrawModel && pDrawModel->anim_count > 0) {
    NumAnims = pDrawModel->anim_count;
  }

  // Don't change 'Animate' checkbox for edit mode skeleton and animation
  IqeB_GUI_WidgetActivate( pGUI_CBox_Anim_Enable, IqeB_DispStyle_MouseMode != IQE_GUI_MOUSE_MODE_SKEL_EDIT &&
                                                  IqeB_DispStyle_MouseMode != IQE_GUI_MOUSE_MODE_JOINT_SEL &&
                                                  IqeB_DispStyle_MouseMode != IQE_GUI_MOUSE_MODE_ANIM_EDIT);

  IqeB_GUI_WidgetActivate( pGUI_Choice_Anim_Select, doAnim && NumAnims > 1);
  IqeB_GUI_WidgetActivate( pGUI_But_Anim_Play,      doAnim && NumAnims > 0);
  IqeB_GUI_WidgetActivate( pGUI_CBox_Anim_PlayOnce, doAnim && NumAnims > 0);
  IqeB_GUI_WidgetActivate( pGUI_But_Anim_FirstF,    doAnim && NumAnims > 0 && ! IqeB_DispStyle_doplay);
  IqeB_GUI_WidgetActivate( pGUI_But_Anim_PrevF,     doAnim && NumAnims > 0 && ! IqeB_DispStyle_doplay);
  IqeB_GUI_WidgetActivate( pGUI_But_Anim_NextF,     doAnim && NumAnims > 0 && ! IqeB_DispStyle_doplay);
  IqeB_GUI_WidgetActivate( pGUI_But_Anim_Prev ,     doAnim && NumAnims > 1);
  IqeB_GUI_WidgetActivate( pGUI_But_Anim_Next ,     doAnim && NumAnims > 1);

  // Update save file things

  IqeB_GUI_WidgetActivate( pGUI_But_SaveFile , pDrawModel != NULL && DrawModelSelName[ 0] != '\0');
  IqeB_GUI_ItemActivate( main_menu_save_item, pGUI_But_SaveFile->active());

  IqeB_GUI_WidgetActivate( pGUI_CBox_SaveFile_Tangent  , IqeB_SaveFile_Type == IQE_EXPORT_FILE_TYPE_IQM);
  IqeB_GUI_WidgetActivate( pGUI_CBox_SaveFile_Adjacency, IqeB_SaveFile_Type == IQE_EXPORT_FILE_TYPE_IQM);

  // update animation selection

  if( pGUI_Choice_Anim_Select != NULL) {

    // Model or animation has changed
    if( UpdateFlags & (IQE_GUI_UPDATE_MODEL_CHANGE | IQE_GUI_UPDATE_ANIMATION)) {

      int HasToUpdate;

      // Test count is different. NOTE: siez() returns one more because of "terminator" item at the end
      HasToUpdate = NumAnims != pGUI_Choice_Anim_Select->size() - 1;

      if( ! HasToUpdate) {   // Make sense to test more

        for( i = 0; i < NumAnims; i++) {

          if( strcmp( pDrawModel->anim_data[ i]->name, pGUI_Choice_Anim_Select->text( i)) != 0) {   // Name is different

            HasToUpdate = true;
            break;
          }
        }
      }

      if( ! HasToUpdate) {   // Make sense to test more

        // OK so far, animationas are equal. Test selected animation

        for( i = 0; i < NumAnims; i++) {

          if( pDrawModel->anim_data[ i] == pDrawModelCurAnim) {

            if( pGUI_Choice_Anim_Select->value() != i) {   // Other one is selected

              pGUI_Choice_Anim_Select->value( i);   // select this
            }
          }
        }
      }

      if( HasToUpdate) {                             // Has to update the

        pGUI_Choice_Anim_Select->clear();            // empty the list

        for( i = 0; i < NumAnims; i++) {

          pGUI_Choice_Anim_Select->add( pDrawModel->anim_data[ i]->name);

          if( pDrawModel->anim_data[ i] == pDrawModelCurAnim) {

            pGUI_Choice_Anim_Select->value( i);   // select this
          }
        }

        pGUI_Choice_Anim_Select->redraw();           // redraw
      }
    } else {

      for( i = 0; i < NumAnims; i++) {

        if( pDrawModel->anim_data[ i] == pDrawModelCurAnim) {

          pGUI_Choice_Anim_Select->value( i);   // select this
        }
      }
    }
  }

  // Update clipboard button

  IqeB_GUI_ItemActivate( main_menu_copy_item, pDrawModel != NULL && DrawModelSelName[ 0] != '\0');

  IqeB_GUI_ItemActivate( main_menu_paste_item, pClipBoardModel != NULL && ClipBoardModelName[ 0] != '\0');

  // check for registed update functions

  T_GUI_RegisterCallback *pRegisterCallback;

  pRegisterCallback = IqeB_RegisterCallback;

  for( i = 0; i < IqeB_RegisterCallbackN; i++, pRegisterCallback++) {

    if( pRegisterCallback->pUpdateGUI != NULL &&                   // have function pointer
        ( pRegisterCallback->UpdateFlags == 0 ||                   // like everything
          (pRegisterCallback->UpdateFlags & UpdateFlags) != 0)) {  // like this

      pRegisterCallback->pUpdateGUI( UpdateFlags);  // call it
    }
  }

  // Save changed preference settings

  IqeB_PreferencesUpdateChanges();  // update preferences database and save to to file
}

/********************************** End Of File **********************************/
