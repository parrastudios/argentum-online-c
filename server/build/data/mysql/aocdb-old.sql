-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'Character'
-- 
-- ---

DROP TABLE IF EXISTS `Character`;

CREATE TABLE `Character` (
`id` INT NOT NULL AUTO_INCREMENT,
`algin` INT NOT NULL DEFAULT 0,
PRIMARY KEY (`id`)
);

-- ---
-- Table 'CharacterAparence'
-- 
-- ---

DROP TABLE IF EXISTS `CharacterAparence`;

CREATE TABLE `CharacterAparence` (
`id` INT NOT NULL,
`head` INT NULL DEFAULT NULL,
`body` INT NULL DEFAULT NULL,
PRIMARY KEY (`id`)
);

-- ---
-- Table 'City'
-- 
-- ---

DROP TABLE IF EXISTS `City`;

CREATE TABLE `City` (
`id` INT NOT NULL AUTO_INCREMENT,
`name` CHAR(32) NOT NULL,
`desc` CHAR(255) NOT NULL,
PRIMARY KEY (`id`)
);

-- ---
-- Table 'Class'
-- 
-- ---

DROP TABLE IF EXISTS `Class`;

CREATE TABLE `Class` (
`id` INT NOT NULL AUTO_INCREMENT,
`name` CHAR(32) NOT NULL,
`desc` CHAR(255) NOT NULL,
PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `Attribute`;

CREATE TABLE `Attribute` (
`id` INT NOT NULL AUTO_INCREMENT,
`strength` INT NOT NULL,
`agility` INT NOT NULL,
`inteligence` INT NOT NULL,
`charisma` INT NOT NULL,
`constitution` INT NOT NULL,
PRIMARY KEY (`id`)
);

-- ---
-- Table 'Race'
-- 
-- ---

DROP TABLE IF EXISTS `Race`;

CREATE TABLE `Race` (
`id` INT NOT NULL AUTO_INCREMENT,
`name` CHAR(32) NOT NULL,
`desc` CHAR(255) NOT NULL,
`attribute_modifiers` INT NOT NULL, -- fk: Attribute 1:1
PRIMARY KEY (`id`)
);


-- ---
-- Table 'Player'
-- 
-- ---

DROP TABLE IF EXISTS `Player`;

CREATE TABLE `Player` (
`id` INT NOT NULL,
`account_id` INT NOT NULL,
`online` TINYINT(1) NOT NULL DEFAULT 0,
`name` CHAR(32) NOT NULL,
`desc` CHAR(255) NULL DEFAULT NULL,
`attributes` INT NOT NULL, -- fk: Attribute 1:1


PRIMARY KEY (`id`)
);

-- ---
-- Table 'Account'
-- 
-- ---

DROP TABLE IF EXISTS `Account`;

CREATE TABLE `Account` (
`id` INT NOT NULL AUTO_INCREMENT,
`name` CHAR(64) NOT NULL,
`password` CHAR(64) NOT NULL,
`email` CHAR(255) NOT NULL,
`players` INT NOT NULL, -- constrait: [max 6 players]
`ip` CHAR(255) NOT NULL DEFAULT 0,
`ban` TINYINT(1) NOT NULL DEFAULT 0,
`online` TINYINT(1) NOT NULL DEFAULT 0,
`active` TINYINT(1) NOT NULL,
`code_active` CHAR(128) NOT NULL,
PRIMARY KEY (`id`)
);

-- ---
-- Table 'Npc'
-- 
-- ---

DROP TABLE IF EXISTS `Npc`;

CREATE TABLE `Npc` (
`id` INT NOT NULL,
PRIMARY KEY (`id`)
);

-- ---
-- Table 'PlayerBasicInfo'
-- 
-- ---

DROP TABLE IF EXISTS `PlayerBasicInfo`;

CREATE TABLE `PlayerBasicInfo` (
`id` INT NOT NULL,
`class` INT NOT NULL, -- fk: class
`race` INT NOT NULL, -- fk: race
`genere` TINYINT(1) NOT NULL DEFAULT 0, -- fk: genere
`city` INT NOT NULL, -- fk: city
PRIMARY KEY (`id`)
);

-- ---
-- Table 'CharacterFlags'
-- 
-- ---

DROP TABLE IF EXISTS `CharacterFlags`;

CREATE TABLE `CharacterFlags` (
`id` INT NOT NULL,
`dead` TINYINT(1) NOT NULL DEFAULT 0,
`poisoned` TINYINT(1) NOT NULL DEFAULT 0,
`hidden` TINYINT(1) NOT NULL DEFAULT 0,
`trading` TINYINT(1) NOT NULL DEFAULT 0,
PRIMARY KEY (`id`)
);

-- ---
-- Table 'CharacterInventory'
-- 
-- ---

DROP TABLE IF EXISTS `CharacterInventory`;

CREATE TABLE `CharacterInventory` (
`id` INT NOT NULL,
`helmet` INT NULL DEFAULT NULL,
`weapon` INT NULL DEFAULT NULL,
`shield` INT NULL DEFAULT NULL,
`footwear` INT NULL DEFAULT NULL,
PRIMARY KEY (`id`)
);

-- ---
-- Table 'InventoryItem'
-- 
-- ---

DROP TABLE IF EXISTS `InventoryItem`;

CREATE TABLE `InventoryItem` (
`id` INT NOT NULL,
`amount` INT NOT NULL DEFAULT 1,
`equipped` TINYINT(1) NOT NULL DEFAULT 0,
`owner` TINYINT NULL DEFAULT NULL,
`type` INT NOT NULL,
PRIMARY KEY (`id`)
);

-- ---
-- Table 'Item'
-- 
-- ---

DROP TABLE IF EXISTS `Item`;

CREATE TABLE `Item` (
`id` INT NOT NULL AUTO_INCREMENT,
`material` INT NOT NULL,
`weight` FLOAT NOT NULL DEFAULT 1.0,
PRIMARY KEY (`id`)
);

-- ---
-- Table 'Helmet'
-- 
-- ---

DROP TABLE IF EXISTS `Helmet`;

CREATE TABLE `Helmet` (
`id` INT NOT NULL,
PRIMARY KEY (`id`)
);

-- ---
-- Table 'WeaponType'
-- 
-- ---

DROP TABLE IF EXISTS `WeaponType`;

CREATE TABLE `WeaponType` (
`id` INT NOT NULL AUTO_INCREMENT,
`name` CHAR(32) NOT NULL,
PRIMARY KEY (`id`)
);

-- ---
-- Table 'Weapon'
-- 
-- ---

DROP TABLE IF EXISTS `Weapon`;

CREATE TABLE `Weapon` (
`id` INT NOT NULL,
`type` double DEFAULT NULL,
`name` CHAR(64) NOT NULL,
`min_hit` FLOAT NOT NULL,
`max_hit` FLOAT NOT NULL,
`hands` TINYINT(1) NOT NULL DEFAULT 1,
`burn` TINYINT(1) NOT NULL DEFAULT 0,
`disarm` TINYINT(1) NOT NULL DEFAULT 0,
`magic_defense` FLOAT NOT NULL DEFAULT 0.0,
`immobilize` TINYINT(1) NOT NULL DEFAULT 0,
`velocity` FLOAT NOT NULL DEFAULT 0,
`aoe` FLOAT NOT NULL DEFAULT 1.0,
`stab` TINYINT(1) NOT NULL DEFAULT 0,
PRIMARY KEY (`id`)
);

-- ---
-- Table 'Shield'
-- 
-- ---

DROP TABLE IF EXISTS `Shield`;

CREATE TABLE `Shield` (
`id` INT NOT NULL,
PRIMARY KEY (`id`)
);

-- ---
-- Foreign Keys
-- ---

-- ALTER TABLE `Character` ADD FOREIGN KEY (id) REFERENCES `Npc` (`id`);
-- ALTER TABLE `Character` ADD FOREIGN KEY (id) REFERENCES `CharacterFlags` (`id`);
-- ALTER TABLE `Character` ADD FOREIGN KEY (id) REFERENCES `CharacterInventory` (`id`);
-- ALTER TABLE `CharacterAparence` ADD FOREIGN KEY (id) REFERENCES `Character` (`id`);
-- ALTER TABLE `Player` ADD FOREIGN KEY (id) REFERENCES `Character` (`id`);
-- ALTER TABLE `Player` ADD FOREIGN KEY (account_id) REFERENCES `Account` (`id`);
-- ALTER TABLE `PlayerBasicInfo` ADD FOREIGN KEY (id) REFERENCES `Player` (`id`);
-- ALTER TABLE `InventoryItem` ADD FOREIGN KEY (owner) REFERENCES `CharacterInventory` (`id`);
-- ALTER TABLE `InventoryItem` ADD FOREIGN KEY (type) REFERENCES `Item` (`id`);
-- ALTER TABLE `Item` ADD FOREIGN KEY (id) REFERENCES `Weapon` (`id`);
-- ALTER TABLE `Item` ADD FOREIGN KEY (id) REFERENCES `Helmet` (`id`);
-- ALTER TABLE `Weapon` ADD FOREIGN KEY (type) REFERENCES `WeaponType` (`id`);


-- ---
-- Table Properties
-- ---

-- ALTER TABLE `Character` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `CharacterAparence` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Player` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Account` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Npc` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `PlayerBasicInfo` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `CharacterFlags` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `CharacterInventory` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `InventoryItem` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Item` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Helmet` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `Weapon` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `Character` (`id`,`algin`) VALUES
-- ('','');
-- INSERT INTO `CharacterAparence` (`id`,`head`,`body`) VALUES
-- ('','','');
-- INSERT INTO `Player` (`id`,`account_id`,`online`,`name`,`desc`) VALUES
-- ('','','','','');
-- INSERT INTO `Account` (`id`,`name`,`password`,`email`,`ban`,`log`,`active`,`code_active`,`online`) VALUES
-- ('','','','','','','','','');
-- INSERT INTO `Npc` (`id`) VALUES
-- ('');
-- INSERT INTO `PlayerBasicInfo` (`id`,`class`,`race`,`genere`,`city`) VALUES
-- ('','','','','');
-- INSERT INTO `CharacterFlags` (`id`,`dead`,`poisoned`,`hidden`,`trading`) VALUES
-- ('','','','','');
-- INSERT INTO `CharacterInventory` (`id`,`helmet`,`weapon`,`shield`,`footwear`) VALUES
-- ('','','','','');
-- INSERT INTO `InventoryItem` (`id`,`amount`,`equipped`,`owner`,`type`) VALUES
-- ('','','','','');
-- INSERT INTO `Item` (`id`) VALUES
-- ('');
-- INSERT INTO `Helmet` (`id`) VALUES
-- ('');
-- INSERT INTO `Weapon` (`id`) VALUES
-- ('');


-- insert data

-- INSERT INTO `WeaponType` (`id`,`name`) VALUES (1,'Westering'), (2,'Melee'), (3,'Missile Bow'), (4,'Long Weapon'), (5,'Projectile Weapon');
