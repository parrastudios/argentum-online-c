/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Window/Keyboard.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct KeyRegisterType KeyRegisterArray[KeyCodeCount] = {{ KEY_EVENT_NONE }};	///< Array to check pressed keys

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Clears the event raised
////////////////////////////////////////////////////////////
void KeyboardClearEventRegister(uint32 Code)
{
	KeyRegisterArray[Code].Previous = KeyRegisterArray[Code].Current;
}

////////////////////////////////////////////////////////////
/// Clears all events raised
////////////////////////////////////////////////////////////
void KeyboardClearEvents()
{
	uint32 Code;

	for (Code = 0; Code < KeyCodeCount; ++Code)
	{
		KeyRegisterArray[Code].Current = KEY_EVENT_NONE;

		KeyboardClearEventRegister(Code);
	}
}

////////////////////////////////////////////////////////////
/// Set the pressed status of a key
////////////////////////////////////////////////////////////
void KeyboardSetKeyPressed(uint32 Code)
{
	KeyboardClearEventRegister(Code);

	KeyRegisterArray[Code].Current = KEY_EVENT_PRESSED;
}

////////////////////////////////////////////////////////////
/// Set the released status of a key
////////////////////////////////////////////////////////////
void KeyboardSetKeyReleased(uint32 Code)
{
	KeyboardClearEventRegister(Code);

	KeyRegisterArray[Code].Current = KEY_EVENT_RELEASED;
}

////////////////////////////////////////////////////////////
/// Get the pressed status of a key
////////////////////////////////////////////////////////////
bool KeyboardIsKeyPressed(uint32 Code)
{
	return (KeyRegisterArray[Code].Current == KEY_EVENT_PRESSED);
}

////////////////////////////////////////////////////////////
/// Get the released status of a key
////////////////////////////////////////////////////////////
bool KeyboardIsKeyReleased(uint32 Code)
{

	return (KeyRegisterArray[Code].Current == KEY_EVENT_RELEASED);
}

////////////////////////////////////////////////////////////
/// Get the pressed status of a key (as event handler)
////////////////////////////////////////////////////////////
bool KeyboardOnKeyPressed(uint32 Code)
{
	if ((KeyRegisterArray[Code].Previous == KEY_EVENT_RELEASED && KeyRegisterArray[Code].Current == KEY_EVENT_PRESSED))
	{
		// Clears the event (only can be fired one time)
		KeyboardClearEventRegister(Code);

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Get the released status of a key (as event handler)
////////////////////////////////////////////////////////////
bool KeyboardOnKeyReleased(uint32 Code)
{
	if ((KeyRegisterArray[Code].Previous == KEY_EVENT_PRESSED && KeyRegisterArray[Code].Current == KEY_EVENT_RELEASED))
	{
		// Clears the event (only can be fired one time)
		KeyboardClearEventRegister(Code);

		return true;
	}

	return false;
}
