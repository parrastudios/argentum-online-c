/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef NETWORK_PROTOCOL_DEFINITIONS_H
#define NETWORK_PROTOCOL_DEFINITIONS_H

/////////////////////////////////////////////////////////////////////////////////
/// Protocol Documentation (todo)
///
/// Data structure
///
/// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
/// | Preamble[0] = 0x73 |  Type[1] | Code[2] | Size[3] | ~Size[4] | Data[<255] |
/// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///
/// Direction	Type					Code					Data
/// C -> S		NETWORK_TYPE_LOGIN		NETWORK_CODE_REQUEST	struct PacketRequest
/// S -> C		NETWORK_TYPE_LOGIN		NETWORK_CODE_RESULT		struct PacketBool
/// C -> S		NETWORK_TYPE_LOGIN		NETWORK_CODE_LOGIN		struct PacketLogin
///
/// S -> C		NETWORK_TYPE_ERROR		NETWORK_CODE_ERROR		struct PacketError
///
///
///
/// Packet Alignment (union _To_PacketData)
///
///		The packet is aligned in order to avoid
///		padding between Header (5 bytes) and Packet.
///		The compiler adds 3 padding bytes after header
///		in order to align it.
///
///		This problem can be solved with a more generic
///		and cross-platform solution, changing the design.
///		It's possible to put the Header outside of its
///		struct and declaring that data in the inner unnamed struct
///		of union _To_PacketData.
///
///		Anyway, with packing this works fine with different platforms
///		because when the buffer is packed in the same way in sender
///		or receptor.
///
/////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Config.h>						///< Config includes

#include <Portability/Type.h>			///< Portability includes

#include <Network/Socket.h>				///< Network includes

#include <System/IOHelper.h>			///< System includes
#include <System/Error.h>
#include <System/Thread.h>
#include <System/Mutex.h>
#include <Memory/Alignment.h>

#include <Math/Geometry/Vector.h>		///< Math includes
#include <Math/Geometry/Quaternion.h>

#include <Game/Account.h>				///< Game includes
#include <Game/Character.h>
#include <Game/Movement.h>

////////////////////////////////////////////////////////////
// Protocol connection definitions
////////////////////////////////////////////////////////////
#define NETWORK_CONNECTION_PORT				0xA0C		///< Default server port
#define NETWORK_CONNECTION_SOCKET_MAX_SIZE	0x1000		///< Default socket buffer lenght

////////////////////////////////////////////////////////////
// Protocol parser definitions
////////////////////////////////////////////////////////////
#define NETWORK_PARSER_PREAMBLE				0x73

#define NETWORK_PARSER_WAITING_PREAMBLE		0x00
#define NETWORK_PARSER_WAITING_TYPE			0x01
#define NETWORK_PARSER_WAITING_CODE			0x02
#define NETWORK_PARSER_WAITING_LENGTH		0x03
#define NETWORK_PARSER_WAITING_CLENGTH		0x04
#define NETWORK_PARSER_WAITING_DATA			0x05

////////////////////////////////////////////////////////////
// Protocol data definitions
////////////////////////////////////////////////////////////
#define NETWORK_PACKET_LENGTH				0xFF
#define NETWORK_PACKET_HEADER_LENGTH		0x05
#define NETWORK_PACKET_DATA_LENGTH			NETWORK_PACKET_LENGTH - NETWORK_PACKET_HEADER_LENGTH
#define NETWORK_PACKET_DATA_EMPTY			0x00
#define NETWORK_PACKET_INVALID_SIZE			0xFFFFFFFF

////////////////////////////////////////////////////////////
// Protocol packet data constraits
////////////////////////////////////////////////////////////
#define NETWORK_TYPE_LOGIN_REQUEST_LENGTH	0x10	///< Size of MD5 code
#define NETWORK_TYPE_WORLD_CREATE_LENGTH	0x07	///< Max number of create entities per packet
#define NETWORK_TYPE_WORLD_UPDATE_LENGTH	0x07	///< Max number of update entities per packet
#define NETWORK_TYPE_WORLD_DESTROY_LENGTH	0x07	///< Max number of destroy entities per packet
#define NETWORK_TYPE_WORLD_MODIFY_LENGTH	0x07	///< Max number of modify entities per packet
#define NETWORK_TYPE_WORLD_DOWNLOAD_LENGTH	0x80	///< Size of the url of remote http download server

////////////////////////////////////////////////////////////
// Protocol packet definitions
////////////////////////////////////////////////////////////
#define NETWORK_TYPE_ERROR					0x00	///< Error packet (0)
#	define NETWORK_CODE_ERR_TYPE_INVALID	0x00		///< Generic invalid type error
#	define NETWORK_CODE_ERR_CODE_INVALID	0x01		///< Generic invalid code error

#	define NETWORK_CODE_ERR_REQ_INVALID		0x02		///< Request (User Login Error)

#	define NETWORK_CODE_ERR_ACC_INVALID		0x03		///< Account (Account Login Error)
#	define NETWORK_CODE_ERR_ACC_LOGGED		0x04
#	define NETWORK_CODE_ERR_ACC_BAN			0x05
#	define NETWORK_CODE_ERR_ACC_PASSW		0x07
#	define NETWORK_CODE_ERR_ACC_NOT_EXIT	0x08		///< Account (Account Logout Error)

#	define NETWORK_CODE_ERR_PLY_INVALID		0x09		///< Player (Player Login Error)
#	define NETWORK_CODE_ERR_PLY_LOGGED		0x0A
#	define NETWORK_CODE_ERR_PLY_BAN			0x0B
#	define NETWORK_CODE_ERR_PLY_NOT_EXIT	0x0C		///< Player (Player Logout Error)

#	define NETWORK_CODE_ERR_WALK_INVALID	0x0D		///< Invalid movement

#define NETWORK_TYPE_LOG_IN					0x01	///< Login packet (1) - (todo: convert to centralized login)
#	define NETWORK_CODE_LOG_IN_REQUEST		0x00		///< Request (check client integrity)
#	define NETWORK_CODE_LOG_IN_ACCOUNT		0x01		///< Account Login
#	define NETWORK_CODE_LOG_IN_PLAYER		0x02		///< Player Login

#define NETWORK_TYPE_LOG_OUT				0x02	///< Logout packet (2)
#	define NETWORK_CODE_LOG_OUT_PLAYER		0x00		///< Player Logout Request
#	define NETWORK_CODE_LOG_OUT_ACCOUNT		0x01		///< Account Logout Request

#define NETWORK_TYPE_WORLD					0x03	///< World packet (3)
#	define NETWORK_CODE_WORLD_CHECK			0x00		///< World Integrity Check
#	define NETWORK_CODE_WORLD_PRELOAD		0x02		///< World Preloading (Check Ack)
#	define NETWORK_CODE_WORLD_LOAD			0x03		///< World Loading (Related to Update)
#	define NETWORK_CODE_WORLD_DOWNLOAD		0x04		///< World Donwloading (Check Error Response)
#	define NETWORK_CODE_WORLD_CREATE		0x05		///< World Entity(ies) Create
#	define NETWORK_CODE_WORLD_DESTROY		0x06		///< World Entity(ies) Destroy
#	define NETWORK_CODE_WORLD_UPDATE		0x07		///< World Entity(ies) Update Position
#	define NETWORK_CODE_WORLD_MODIFY		0x08		///< World Entity(ies) Modify Properties

#define NETWORK_TYPE_MOVEMENT				0x04	///< Movement packet (4)
#	define NETWORK_CODE_MOVEMENT_WALK		0x00		///< Movement Walk
#	define NETWORK_CODE_MOVEMENT_RUN		0x01		///< Movement Run
#	define NETWORK_CODE_MOVEMENT_JUMP		0x02		///< Movement Jump
#	define NETWORK_CODE_MOVEMENT_FLY		0x03		///< Movement Fly
#	define NETWORK_CODE_MOVEMENT_RIDE		0x04		///< Movement Ride a Pet

#define NETWORK_TYPE_TALK					0x05	///< Talk packet (5) - (todo: convert to udp)
#	define NETWORK_CODE_TALK_PUBLIC			0x00		///< Talk to Public (area)
#	define NETWORK_CODE_TALK_PLAYER			0x01		///< Talk to Player (private player)
#	define NETWORK_CODE_TALK_CLAN			0x02		///< Talk to Clan (private clan)
#	define NETWORK_CODE_TALK_BROADCAST		0x03		///< Talk to All (console broadcast)
#	define NETWORK_CODE_TALK_GMS			0x04		///< Talk to GMs only (private GMs)

#define NETWORK_TYPE_CHARACTER				0x06	///< Character packet (6) - Npc & Player
#	define NETWORK_CODE_CHARACTER_CREATE	0x00		///< Create Character(s)
#	define NETWORK_CODE_CHARACTER_DESTROY	0x01		///< Destroy Character(s)
#	define NETWORK_CODE_CHARACTER_APARENCE	0x02		///< Update Character(s) Appearence
#	define NETWORK_CODE_CHARACTER_UPDATE	0x03		///< Update Character(s) Position
#	define NETWORK_CODE_CHARACTER_ATTACK	0x04		///< Character(s) Attack
#	define NETWORK_CODE_CHARACTER_DIE		0x05		///< Character(s) Die

#define NETWORK_TYPE_PLAYER					0x07	///< Player packet (7)
#	define NETWORK_CODE_PLAYER_HP			0x00		///< Update Player Hp
#	define NETWORK_CODE_PLAYER_MANA			0x01		///< Update Player Mana
#	define NETWORK_CODE_PLAYER_STAMINA		0x02		///< Update Player Stamina
#	define NETWORK_CODE_PLAYER_HUNGER		0x03		///< Update Player Hunger
#	define NETWORK_CODE_PLAYER_THIRST		0x04		///< Update Player Thirst
#	define NETWORK_CODE_PLAYER_MEDITATE		0x05		///< Make a Player Meditate

#define NETWORK_TYPE_NPC					0x08	///< Npc packet (8)
#	define NETWORK_CODE_NPC_ACTION			0x00		///< Perform an Action

#define NETWORK_TYPE_ENVIRONMENT			0x09	///< Environment packet (9)
#	define NETWORK_CODE_ENVIRONMENT_SOUND	0x00		///< Play/Stop Environment Sound
#	define NETWORK_CODE_ENVIRONMENT_AUDIO	0x01		///< Play/Stop Environment Audio Theme
#	define NETWORK_CODE_ENVIRONMENT_FOG		0x02		///< Enable/Disable Environment Fog

////////////////////////////////////////////////////////////
// Packet data structures
////////////////////////////////////////////////////////////
struct PacketLoginRequest		// Client -> Server
{
    uint8 MD5[NETWORK_TYPE_LOGIN_REQUEST_LENGTH];

	struct
	{
		uint8 Major, Minor, Revision;
	} Version;
};

struct PacketLoginAccount		// Client -> Server
{
    char Name[ACCOUNT_NAME_MAX_SIZE];
    char Password[ACCOUNT_PASS_MAX_SIZE];
};

struct PacketLoginAccountAck	// Server -> Client
{
	struct AccountPlayerInfo Players[ACCOUNT_PLAYER_MAX_SIZE];
	uint32 Count;
};

struct PacketLoginPlayer		// Client -> Server
{
	uint8 Index;				//	Index of the player to log in [0 to ACC_MAX_PLY]
};

struct PacketInventoryPlayer	// Server -> Client
{
	struct
	{
		uint32	Index;
		uint16	Amount;
	} Objects[PLAYER_INV_MAX_SLOTS] MEMORY_PACK;

	uint32 Count;
};

struct PacketEquippedPlayer		// Server -> Client
{
	uint32	WeaponEqpIndex;
	uint16	WeaponEqpSlot;

	uint32	ArmorEqpIndex;
	uint16	ArmorEqpSlot;

	uint32	ShieldEqpIndex;
	uint16	ShieldEqpSlot;

	uint32	HelmetEqpIndex;
	uint16	HelmetEqpSlot;

	uint32	FootwearEqpIndex;
	uint16	FootwearEqpSlot;

	uint32	ShipEqpIndex;
	uint16	ShipEqpSlot;
};

struct PacketSpellsPlayer		// Server -> Client
{
	struct
	{
		uint32 Index;
		// ..
	} Spells[PLAYER_SPELLS_MAX_SLOTS];

	uint32 Count;
};

struct PacketStatsPlayer		// Server -> Client
{
	uint16	Hp[PLAYER_STATS_COUNT];
	uint16	Mana[PLAYER_STATS_COUNT];
	uint16	Stamina[PLAYER_STATS_COUNT];
	uint16	Hunger[PLAYER_STATS_COUNT];
	uint16	Thirst[PLAYER_STATS_COUNT];
};

struct PacketLevelPlayer	// Server -> Client
{
	uint32 Value;
};

struct PacketExpPlayer		// Server -> Client
{
	uint32 Value;
};

struct PacketGoldPlayer			// Server -> Client
{
	uint32 Value;
};

struct PacketLoginPlayerAck		// Server -> Client
{
	struct
	{
		uint32			Index;
		// ...
	} Player;

	uint8 Motd[1];				//	MOTD string [0 < PACKET_DATA_LENGTH - (sizeof(struct PacketLoginPlayer) - 1)]
};

struct PacketWorldCheck			// Client -> Server
{
	uint32	Index;
	uint8	CheckSum[0x10];
};

struct PacketWorldPreload		// Server -> Client
{
	uint32			Index;
	// ...
};

struct PacketWorldLoad			// Server -> Client
{
	uint32			Index;
	// ...
};

struct PacketWorldDownload		// Server -> Client
{
	uint32			Index;
	char			RemoteServer[NETWORK_TYPE_WORLD_DOWNLOAD_LENGTH];
};

struct PacketWorldCreate		// Server -> Client
{
	uint8 EntityCount;

	// In case of structure modification, check size constraint
	struct
	{
		uint32 Index;
		struct Vector3f		Position;
		struct Quaternion	Rotation;
		// ...
	} EntityList[NETWORK_TYPE_WORLD_CREATE_LENGTH];
};

struct PacketWorldUpdate		// Server -> Client
{
	uint8 EntityCount;

	// In case of structure modification, check size constraint
	struct
	{
		uint32				Index;
		struct Vector3f		Position;
		struct Quaternion	Rotation;
	} EntityList[NETWORK_TYPE_WORLD_UPDATE_LENGTH];
};

struct PacketWorldDestroy		// Server -> Client
{
	uint8 EntityCount;

	// In case of structure modification, check size constraint
	struct
	{
		uint32 Index;
	} EntityList[NETWORK_TYPE_WORLD_DESTROY_LENGTH];
};

struct PacketWorldModify		// Server -> Client
{
	uint8 EntityCount;

	struct
	{
		uint32				Index;
		// ..
	} EntityPropertyList[NETWORK_TYPE_WORLD_MODIFY_LENGTH];
};

struct PacketMovementWalk		// Client -> Server
{
	uint8	Key;
	bool	Pressed;
};

struct PacketMovementWalkAck	// Client <- Server
{
	uint8 Movement;
};

struct PacketTalkDefault		// Client <-> Server
{
	uint8 Text[1];	// Payload
};

struct PacketTalkPlayer			// Client -> Server
{
	uint32 PlayerId;
	struct PacketTalkDefault Message;
};

struct PacketEnvironmentAudio	// Server -> Client
{
	uint32	Id;		// ENVIRONMENT_AUDIO_ALL for act over all audios
	uint8	State;	// PLAY | PAUSE | STOP | LOOP
};

struct PacketEnvironmentSound	// Server -> Client
{
	uint32			Id;			// ENVIRONMENT_SOUND_ALL for act over all sounds
	uint8			State;		// PLAY | PAUSE | STOP | LOOP
	struct Vector3f	Position;
};

struct PacketEnvironmentFog
{
	bool Activated;

	struct Fog
	{
		uint8	Color[4];
		float	Near;
		float	Far;
	} Properties;
};

////////////////////////////////////////////////////////////
// Packet definition
////////////////////////////////////////////////////////////
struct PacketHeader
{
	uint8 Preamble;
	uint8 Type;
	uint8 Code;
	uint8 DataSize;
	uint8 Checksum;
};

cassert(sizeof(struct PacketHeader) == NETWORK_PACKET_HEADER_LENGTH);

union ClientToServerPacket
{
	//struct PacketError			Error;

	struct PacketLoginRequest		Request;
	struct PacketLoginAccount		LoginAccount;
	struct PacketLoginPlayer		LoginPlayer;

	struct PacketWorldCheck			WorldCheck;

	struct PacketMovementWalk		MovementWalk;

	struct PacketTalkDefault		TalkDefault;
	struct PacketTalkPlayer			TalkPlayer;
};

// todo: fails in linux, solve it
// cassert(sizeof(union ClientToServerPacket) <= NETWORK_PACKET_DATA_LENGTH + 1);

union ClientToServerPacketData
{
	MEMORY_PACK_ENABLE(1)

	struct
	{
		struct PacketHeader			Header;
		union ClientToServerPacket	Packet;
	} MEMORY_PACK;

	MEMORY_PACK_DISABLE()

    uint8 							Buffer[NETWORK_PACKET_LENGTH];
};


// todo: fails in linux, solve it
// cassert(sizeof(union ClientToServerPacketData) == NETWORK_PACKET_LENGTH);

union ServerToClientPacket
{
	//struct PacketError				Error;

	struct PacketLoginAccountAck	LoginAccountAck;
	struct PacketLoginPlayerAck		LoginPlayerAck;

	struct PacketStatsPlayer		Stats;
	struct PacketInventoryPlayer	Inventory;
	struct PacketSpellsPlayer		Spells;
	struct PacketEquippedPlayer		Equipped;

	struct PacketWorldPreload		WorldPreload;
	struct PacketWorldLoad			WorldLoad;
	struct PacketWorldCreate		WorldCreate;
	struct PacketWorldUpdate		WorldUpdate;
	struct PacketWorldModify		WorldModify;
	struct PacketWorldDestroy		WorldDestroy;

	struct PacketMovementWalkAck	MovementWalkAck;

	struct PacketTalkDefault		TalkDefault;

	struct PacketEnvironmentAudio	EnvironmentAudio;
	struct PacketEnvironmentSound	EnvironmentSound;
	struct PacketEnvironmentFog		EnvironmentFog;

};

// todo: fails in linux, solve it
// cassert(sizeof(union ServerToClientPacket) <= NETWORK_PACKET_DATA_LENGTH + 1);

union ServerToClientPacketData
{
	MEMORY_PACK_ENABLE(1)

	struct
	{
		struct PacketHeader			Header;
		union ServerToClientPacket	Packet;
	} MEMORY_PACK;

	MEMORY_PACK_DISABLE()

    uint8 							Buffer[NETWORK_PACKET_LENGTH];
};

// todo: fails in linux, solve it
// cassert(sizeof(union ServerToClientPacketData) == NETWORK_PACKET_LENGTH);

#endif // NETWORK_PROTOCOL_DEFINITIONS_H
