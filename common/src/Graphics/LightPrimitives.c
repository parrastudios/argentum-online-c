/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/LightPrimitives.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

void LightBaseInitialize(struct LightBase * Light)
{
	if (Light)
	{
		Vector3fSetNull(&Light->Color);
		Light->AmbientIntensity = 0.0f;
		Light->DiffuseIntensity = 0.0f;
	}
}

void LightDirectionalInitialize(struct LightDirectional * Light)
{
	if (Light)
	{
		LightBaseInitialize(&Light->Base);
		Vector3fSetNull(&Light->Direction);
	}
}

void LightPointInitialize(struct LightPoint * Light)
{
	if (Light)
	{
		LightBaseInitialize(&Light->Base);
		Vector3fSetNull(&Light->Position);
		Light->Attenuation.Constant = 1.0f;
		Light->Attenuation.Linear = 0.0f;
		Light->Attenuation.Exp = 0.0f;
	}
}

void LightSpotInitialize(struct LightSpot * Light)
{
	if (Light)
	{
		LightPointInitialize(&Light->Point);
		Vector3fSetNull(&Light->Direction);
		Light->Cutoff = 0.0f;
	}
}
