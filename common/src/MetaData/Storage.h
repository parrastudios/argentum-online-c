/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef METADATA_STORAGE_H
#define METADATA_STORAGE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/Set.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define METADATA_STORAGE_INDEX_INVALID			SET_HASH_INVALID
#define METADATA_STORAGE_GLOBAL_NAME			"metadata_storage_global"

enum MetadataStorageResourceInfo
{
	METADATA_STORAGE_RESOURCE_VALUE				= 0x00,
	METADATA_STORAGE_RESOURCE_REFERENCE			= 0x01,
	METADATA_STORAGE_RESOURCE_PARAMETER_LIST	= 0x02,
	METADATA_STORAGE_RESOURCE_FUNCTION			= 0x03,
	METADATA_STORAGE_RESOURCE_OBJECT			= 0x04,

	METADATA_STORAGE_RESOURCE_SIZE,

	METADATA_STORAGE_RESOURCE_INVALID			= 0xFFFFFFFFUL
};

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef uinteger								MetadataStorageResourceType;
typedef struct MetadataStorageType *			MetadataStorageType;
typedef SetHashType								MetadataStorageIndexType;
typedef uinteger								MetadataStorageDataSize;
typedef char *									MetadataStorageName;

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Allocate resource macros
////////////////////////////////////////////////////////////
#define MetadataStorageAllocateValue(Storage, Size) \
	MetadataStorageAllocateResource(Storage, METADATA_STORAGE_RESOURCE_VALUE, Size)

#define MetadataStorageAllocateReference(Storage, Size) \
	MetadataStorageAllocateResource(Storage, METADATA_STORAGE_RESOURCE_REFERENCE, Size)

#define MetadataStorageAllocateParameterList(Storage, Size) \
	MetadataStorageAllocateResource(Storage, METADATA_STORAGE_RESOURCE_PARAMETER_LIST, Size)

#define MetadataStorageAllocateFunction(Storage, Size) \
	MetadataStorageAllocateResource(Storage, METADATA_STORAGE_RESOURCE_FUNCTION, Size)

#define MetadataStorageAllocateObject(Storage, Size) \
	MetadataStorageAllocateResource(Storage, METADATA_STORAGE_RESOURCE_OBJECT, Size)

////////////////////////////////////////////////////////////
/// Access resource macros
////////////////////////////////////////////////////////////
#define MetadataStorageGetImpl(Storage, ResourceTypeCast, ResourceType, Index) \
	(ResourceTypeCast)MetadataStorageGetResource(Storage, ResourceType, Index)

#define MetadataStorageGetValue(Storage, Index) \
	MetadataStorageGetImpl(Storage, MetadataValueTypeImpl, METADATA_STORAGE_RESOURCE_VALUE, Index)

#define MetadataStorageGetReference(Storage, Index) \
	MetadataStorageGetImpl(Storage, MetadataReferenceTypeImpl, METADATA_STORAGE_RESOURCE_REFERENCE, Index)

#define MetadataStorageGetParameterList(Storage, Index) \
	MetadataStorageGetImpl(Storage, MetadataParameterListTypeImpl, METADATA_STORAGE_RESOURCE_PARAMETER_LIST, Index)

#define MetadataStorageGetFunction(Storage, Index) \
	MetadataStorageGetImpl(Storage, MetadataFunctionTypeImpl, METADATA_STORAGE_RESOURCE_FUNCTION, Index)

#define MetadataStorageGetObject(Storage, Index) \
	MetadataStorageGetImpl(Storage, MetadataObjectTypeImpl, METADATA_STORAGE_RESOURCE_OBJECT, Index)

////////////////////////////////////////////////////////////
/// Deallocate resource macros
////////////////////////////////////////////////////////////
#define MetadataStorageDeallocateImpl(Storage, ResourceType, Index) \
	do { \
		MetadataStorageDeallocateResource(Storage, ResourceType, Index); \
		Index = METADATA_STORAGE_INDEX_INVALID; \
	} while (0)

#define MetadataStorageDeallocateValue(Storage, Index) \
	MetadataStorageDeallocateImpl(Storage, METADATA_STORAGE_RESOURCE_VALUE, Index)

#define MetadataStorageDeallocateReference(Storage, Index) \
	MetadataStorageDeallocateImpl(Storage, METADATA_STORAGE_RESOURCE_REFERENCE, Index)

#define MetadataStorageDeallocateParameterList(Storage, Index) \
	MetadataStorageDeallocateImpl(Storage, METADATA_STORAGE_RESOURCE_PARAMETER_LIST, Index)

#define MetadataStorageDeallocateFunction(Storage, Index) \
	MetadataStorageDeallocateImpl(Storage, METADATA_STORAGE_RESOURCE_FUNCTION, Index)

#define MetadataStorageDeallocateObject(Storage, Index) \
	MetadataStorageDeallocateImpl(Storage, METADATA_STORAGE_RESOURCE_OBJECT, Index)

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get global storage instance
////////////////////////////////////////////////////////////
MetadataStorageType MetadataStorageGlobalInstance();

////////////////////////////////////////////////////////////
/// Create a storage manager
////////////////////////////////////////////////////////////
MetadataStorageType MetadataStorageCreate();

////////////////////////////////////////////////////////////
/// Allocate a resource in the storage manager
////////////////////////////////////////////////////////////
MetadataStorageIndexType MetadataStorageAllocateResource(MetadataStorageType Storage, MetadataStorageResourceType ResourceType, MetadataStorageDataSize Size);

////////////////////////////////////////////////////////////
/// Get a resource from the storage manager
////////////////////////////////////////////////////////////
void * MetadataStorageGetResource(MetadataStorageType Storage, MetadataStorageResourceType ResourceType, MetadataStorageIndexType Index);

////////////////////////////////////////////////////////////
/// Deallocate a resource in the storage manager
////////////////////////////////////////////////////////////
bool MetadataStorageDeallocateResource(MetadataStorageType Storage, MetadataStorageResourceType ResourceType, MetadataStorageIndexType Index);

////////////////////////////////////////////////////////////
/// Clear a storage manager
////////////////////////////////////////////////////////////
void MetadataStorageClear(MetadataStorageType Storage);

////////////////////////////////////////////////////////////
/// Destroy a storage manager
////////////////////////////////////////////////////////////
void MetadataStorageDestroy(MetadataStorageType Storage);

#endif // METADATA_STORAGE_H
