/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_SHADER_H
#define GRAPHICS_SHADER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <Graphics/Device.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
/*
#ifdef RENDER_SYSTEM_OPENGL
#	if PIPELINE_SYSTEM_TYPE == RENDER_PIPELINE_PROGRAMMABLE
#		define SHADER_TYPE_VERTEX		GL_VERTEX_SHADER_ARB
#		define SHADER_TYPE_PIXEL		GL_FRAGMENT_SHADER_ARB
#		define SHADER_TYPE_GEOMETRY		GL_GEOMETRY_SHADER_EXT
#		define SHADER_TYPE_TESSELATION	GL_NOT_SUPPORTED_YET

#		if RENDER_SYSTEM_TYPE == RENDER_SYSTEM_GLES2
#			define SHADER_TYPE_VERTEX_HEADER	"#version 110               \n"
#			define SHADER_TYPE_PIXEL_HEADER		"#version 110               \n"
#		else
#			define SHADER_TYPE_VERTEX_HEADER
#			define SHADER_TYPE_PIXEL_HEADER		"precision mediump float;   \n"
#		endif
#	endif
#endif
*/

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct ShaderType
{
	bool		 Compiled;
	GLenum		 Type;
	GLuint		 Id;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

bool ShaderCreate(struct ShaderType * Shader, GLenum Type, const char * Path);

void ShaderInitialize(struct ShaderType * Shader, GLenum Type);

void ShaderSetSource(struct ShaderType * Shader, const char * Source, uinteger Length);

bool ShaderCheckError(struct ShaderType * Shader, GLenum State);

bool ShaderCompile(struct ShaderType * Shader);

void ShaderDestroy(struct ShaderType * Shader);

#endif // GRAPHICS_SHADER_H
