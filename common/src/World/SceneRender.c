/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Scene.h>
#include <World/Camera.h>
#include <World/Terrain.h>
#include <World/Model.h>

#include <Window/Window.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Render a scene
////////////////////////////////////////////////////////////
void SceneRender(struct SceneType * Scene, struct FrustumType * Frustum)
{
	// Render octree
	OctreeRender(Scene->Octree, Frustum);
}

////////////////////////////////////////////////////////////
/// Create a terrain for the scene render
////////////////////////////////////////////////////////////
void SceneTerrainCreateRender(struct SceneType * Scene, EntityIdType EntityId)
{
	if (Scene && EntityId != ENTITY_INDEX_INVALID)
	{
		// Default render properties
		#if (SCENE_DEFAULT_QUALITY == SCENE_QUALITY_LOW)
		uinteger TextureSize = 512;
		uinteger TextureTile = 1;
		#elif (SCENE_DEFAULT_QUALITY == SCENE_QUALITY_MEDIUM)
		uinteger TextureSize = 1024;
		uinteger TextureTile = 1;
		#elif (SCENE_DEFAULT_QUALITY == SCENE_QUALITY_HIGH)
		uinteger TextureSize = 2048;
		uinteger TextureTile = 1;
		#endif
		uinteger BaseTextureTile = 64;
		uinteger PatchVertexCacheSize = 4 * 1024;
		uinteger PatchIndexCacheSize = 1024;

		struct EntityType * Entity = EntityGet(ENTITY_TYPE_TERRAIN, EntityId);

		// Obtain render component data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderType * RenderType = ComponentTypeRenderDerived(Render, struct TerrainComponentRenderType);

		// Initialize terrain render camera target
		RenderType->SetCameraTarget(Entity, Scene->CameraId);

		// Initialize terrain
		RenderType->Initialize(Entity, WindowHeight, TextureSize, TextureTile, BaseTextureTile, PatchVertexCacheSize, PatchIndexCacheSize);
	}
}