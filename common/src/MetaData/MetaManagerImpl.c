/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/MetaManagerImpl.h>
#include <MetaData/Scope.h>
#include <MetaData/Storage.h>
#include <DataType/HashMap.h>

#include <System/IOHelper.h> // todo: refactor

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define METADATA_MANAGER_STORAGE_NAME_SIZE			0xFFUL

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef char MetadataManagerStorageNameImpl[METADATA_MANAGER_STORAGE_NAME_SIZE];

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef struct MetadataManagerStorageBucketType
{
	MetadataManagerStorageNameImpl	Name;		//< Storage name key value
	MetadataStorageType				Storage;	//< Storage reference

} * MetadataManagerStorageBucketType;

struct MetadataManagerImplType
{
	HashMap							DataBase;	//< Map holding all instances of storage

	struct MetadataScopeTypeImpl
	{
		HashMap						Naming;		//< Naming table pointing to the tree (todo: make real references to tree (vector contigously allocated))
		MetadataScopeType			Root;		//< Root scope of the system (todo: refactor into a tree datatype)
		MetadataScopeType			Current;	//< Current scope selected (todo: refactor into a stack datatype)
	} Scope;

};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Compare two buckets containing storage reference
////////////////////////////////////////////////////////////
OperatorType MetadataManagerStorageCompare(void * Left, void * Right)
{
	if (Left && Right)
	{
		MetadataManagerStorageBucketType LeftBucket = (MetadataManagerStorageBucketType)Left;
		MetadataManagerStorageBucketType RightBucket = (MetadataManagerStorageBucketType)Right;

		integer Result = strcmp(LeftBucket->Name, RightBucket->Name);

		if (Result == 0)
		{
			// If they are equal, is it valid to compare against pointers?
			return (LeftBucket->Storage == RightBucket->Storage) ? OPERATOR_EQUAL : OPERATOR_NOT_EQUAL; // warning: not equal or exception?
		}
		else if (Result > 0)
		{
			return OPERATOR_GREATER;
		}
		else if (Result < 0)
		{
			return OPERATOR_LESS;
		}
	}

	return OPERATOR_NOT_EQUAL;
}

////////////////////////////////////////////////////////////
/// Initialize a metadata manager implementation
////////////////////////////////////////////////////////////
bool MetadataManagerInitializeImpl(MetadataManagerImplType MetadataManagerImpl)
{
	if (MetadataManagerImpl)
	{
		if (MetadataManagerImpl->DataBase == NULL &&
			MetadataManagerImpl->Scope.Root == NULL &&
			MetadataManagerImpl->Scope.Current == NULL)
		{
			// Initialize global scope
			MetadataManagerImpl->Scope.Root = MetadataManagerImpl->Scope.Current = MetadataScopeGlobalInstance();

			if (MetadataManagerImpl->Scope.Root)
			{
				// Initialize storage database
				MetadataManagerImpl->DataBase = HashMapCreate(HashGetCallbackStringDefault(), CompareGetCallback(COMPARE_FUNC_CSTRING));

				if (MetadataManagerImpl->DataBase)
				{
					// Insert global storage into database
					if (MetadataManagerStorageCreateImpl(MetadataManagerImpl, METADATA_STORAGE_GLOBAL_NAME, MetadataStorageGlobalInstance()))
					{
						return true;
					}
					else
					{
						// Destroy on error
						MetadataManagerDestroyImpl(MetadataManagerImpl);
					}
				}
			}

			// Invalid scope
			return false;
		}

		// Already initialized
		return true;
	}

	// Invalid metadata manager impl instance
	return false;
}

////////////////////////////////////////////////////////////
/// Create a scope instance associated to a name
////////////////////////////////////////////////////////////
bool MetadataManagerScopeCreateImpl(MetadataManagerImplType MetadataManagerImpl, MetadataScopeName ScopeName, MetadataScopeType Scope, MetadataStorageName StorageName)
{
	if (MetadataManagerImpl && ScopeName && Scope && StorageName)
	{
		// todo
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Get a scope instance associated to a name
////////////////////////////////////////////////////////////
MetadataScopeType MetadataManagerScopeGetImpl(MetadataManagerImplType MetadataManagerImpl, MetadataScopeName Name)
{
	if (MetadataManagerImpl && Name)
	{
		// todo
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Get current scope instance
////////////////////////////////////////////////////////////
MetadataScopeType MetadataManagerScopeCurrentImpl(MetadataManagerImplType MetadataManagerImpl)
{
	// todo: refactor into stack->front()
	return MetadataManagerImpl->Scope.Current;
}


////////////////////////////////////////////////////////////
/// Begin a scope associated to a name
////////////////////////////////////////////////////////////
bool MetadataManagerScopeBeginImpl(MetadataManagerImplType MetadataManagerImpl, MetadataScopeName Name)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// End a scope associated to a name
////////////////////////////////////////////////////////////
bool MetadataManagerScopeEndImpl(MetadataManagerImplType MetadataManagerImpl, MetadataScopeName Name)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Destroy a scope instance associated to a name
////////////////////////////////////////////////////////////
bool MetadataManagerScopeDestroyImpl(MetadataManagerImplType MetadataManagerImpl, MetadataScopeName Name)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Creates a new storage instance associated to a name
////////////////////////////////////////////////////////////
bool MetadataManagerStorageCreateImpl(MetadataManagerImplType MetadataManagerImpl, MetadataStorageName Name, MetadataStorageType Storage)
{
	if (MetadataManagerImpl && Name && Storage)
	{
		// Create a new bucket
		MetadataManagerStorageBucketType Bucket = (MetadataManagerStorageBucketType)MemoryAllocate(sizeof(struct MetadataManagerStorageBucketType));

		if (Bucket)
		{
			// Copy name of storage
			strcpy(Bucket->Name, Name);

			// Set storage pointer
			Bucket->Storage = Storage;

			// Insert bucket into database
			if (HashMapInsert(MetadataManagerImpl->DataBase, Bucket->Name, Bucket))
			{

				return true;
			}
			else
			{
				// Clear allocated bucket
				MemoryDeallocate(Bucket);
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Get a storage instance associated to a name
////////////////////////////////////////////////////////////
MetadataStorageType MetadataManagerStorageGetImpl(MetadataManagerImplType MetadataManagerImpl, MetadataStorageName Name)
{
	if (MetadataManagerImpl && Name)
	{
		MetadataManagerStorageBucketType Bucket = (MetadataManagerStorageBucketType)HashMapGet(MetadataManagerImpl->DataBase, (HashKeyData)Name);

		if (Bucket)
		{
			return Bucket->Storage;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a storage instance associated to a name
////////////////////////////////////////////////////////////
bool MetadataManagerStorageDestroyImpl(MetadataManagerImplType MetadataManagerImpl, MetadataStorageName Name)
{
	if (MetadataManagerImpl && Name)
	{
		MetadataManagerStorageBucketType Bucket = (MetadataManagerStorageBucketType)HashMapGet(MetadataManagerImpl->DataBase, (HashKeyData)Name);

		if (Bucket)
		{
			// Remove bucket from hash map
			bool Result = HashMapRemove(MetadataManagerImpl->DataBase, (HashKeyData)Name);

			// Deallocate bucket memory
			MemoryDeallocate(Bucket);

			return Result;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Destroy a metadata manager implementation
////////////////////////////////////////////////////////////
void MetadataManagerDestroyImpl(MetadataManagerImplType MetadataManagerImpl)
{
	if (MetadataManagerImpl)
	{
		// Destroy database
		if (MetadataManagerImpl->DataBase != NULL)
		{
			// Iterate through database map and delete each bucket
			HashMapIterator Iterator;

			if (HashMapIteratorBegin(&Iterator, MetadataManagerImpl->DataBase))
			{
				do
				{
					MetadataManagerStorageBucketType Data = (MetadataManagerStorageBucketType)HashMapIteratorData(&Iterator);

					if (Data)
					{
						// Destroy storage
						MetadataStorageDestroy(Data->Storage);

						// Clear bucket memory
						MemoryDeallocate(Data);
					}

				} while (!HashMapIteratorEnd(&Iterator) && HashMapIteratorNext(&Iterator));
			}


			// Destroy hash map
			HashMapDestroy(MetadataManagerImpl->DataBase);
		}


		if (MetadataManagerImpl->Scope.Root != NULL)
		{
			// Clear root scope
			MetadataScopeDestroy(MetadataManagerImpl->Scope.Root);

			// Set current to null
			MetadataManagerImpl->Scope.Root = MetadataManagerImpl->Scope.Current = NULL;
		}

	}
}

////////////////////////////////////////////////////////////
/// Get global metadata manager instance implementation
////////////////////////////////////////////////////////////
MetadataManagerImplType MetadataManagerGlobalInstanceImpl()
{
	static struct MetadataManagerImplType GlobalMetadataManagerInstanceImpl =
	{
		TypeInit(DataBase, NULL),
		ExprInit(Scope,
			TypeInit(Root, NULL),
			TypeInit(Current, NULL)
		)
	};

	// Static initialization of implementation
	MetadataManagerInitializeImpl(&GlobalMetadataManagerInstanceImpl);

	return &GlobalMetadataManagerInstanceImpl;
}
