/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WINDOW_SETTINGS_H
#define WINDOW_SETTINGS_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Window/VideoModeSupport.h>

////////////////////////////////////////////////////////////
/// Structure defining the creation settings of window
////////////////////////////////////////////////////////////
struct WindowSettings
{
    uint32 DepthBits;         ///< Bits of the depth buffer		(24 by default)
    uint32 StencilBits;       ///< Bits of the stencil buffer	(8  by default)
    uint32 AntialiasingLevel; ///< Level of antialiasing		(0  by default)
};

////////////////////////////////////////////////////////////
/// Evaluate a pixel format configuration.
/// This functions can be used by implementations that have
/// several valid formats and want to get the best one
////////////////////////////////////////////////////////////
int32 SettingsEvaluateConfig(struct VideoMode * Mode, struct WindowSettings * Settings,
							 int32 ColorBits, int32 DepthBits, int32 StencilBits, int32 Antialiasing);

#endif // WINDOW_SETTINGS_H
