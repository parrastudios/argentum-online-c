/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/EntityPhysics.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
///	Set mass of the an entity
////////////////////////////////////////////////////////////
void EntityPhysicsMass(struct EntityType * Entity, float Mass);

////////////////////////////////////////////////////////////
/// Generate physics of the entity
////////////////////////////////////////////////////////////
void EntityPhysicsGenerate(struct EntityType * Entity, float ElapsedTime);

////////////////////////////////////////////////////////////
/// Set velocity of the entity
////////////////////////////////////////////////////////////
void EntityPhysicsVelocity(struct EntityType * Entity, struct Vector3f * Velocity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a physics component
////////////////////////////////////////////////////////////
struct EntityType * EntityPhysicsCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Physics = EntityComponentPhysics(Entity);
		struct ComponentPhysicsData * PhysicsData = ComponentDataPhysics(Physics);

		// Initialize physics data
		Vector3fSetNull(&PhysicsData->Velocity);

		Vector3fSetNull(&PhysicsData->Acceleration);

		Vector3fSetNull(&PhysicsData->Force);

		PhysicsData->Mass = 1.0f;

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a physics component
////////////////////////////////////////////////////////////
void EntityPhysicsDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Physics = EntityComponentPhysics(Entity);
		struct ComponentPhysicsData * PhysicsData = ComponentDataPhysics(Physics);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the entity physics implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentPhysicsType * EntityComponentPhysicsAttr()
{
	static const struct ComponentPhysicsType ComponentPhysicsAttr = {
		TypeInit(Mass, &EntityPhysicsMass),
		TypeInit(Generate, &EntityPhysicsGenerate),
		TypeInit(Velocity, &EntityPhysicsVelocity)
	};

	return (const struct ComponentPhysicsType *)&ComponentPhysicsAttr;
}

////////////////////////////////////////////////////////////
/// Return the entity physics implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentPhysicsImpl()
{
	static const struct ComponentImplType EntityComponentPhysicsTypeImpl = {
		ExprInit(Base, TypeInit(Create, &EntityPhysicsCreate), TypeInit(Destroy, &EntityPhysicsDestroy)),
		TypeInit(Type, COMPONENT_TYPE_PHYSICS),
		UnionInit(Attributes, Physics, &EntityComponentPhysicsAttr)
	};

	return &EntityComponentPhysicsTypeImpl;
}

////////////////////////////////////////////////////////////
///	Set mass of the an entity
////////////////////////////////////////////////////////////
void EntityPhysicsMass(struct EntityType * Entity, float Mass)
{
	if (Entity)
	{
		// Create physics component data reference
		struct ComponentType * Physics = EntityComponentPhysics(Entity);
		struct ComponentPhysicsData * PhysicsData = ComponentDataPhysics(Physics);

		if (Mass < 0.0f)
		{
			// Set defaut physics values
			PhysicsData->Mass = ENTITY_PHYSICS_MASS_DEFAULT;
		}
		else
		{
			// Set physics values
			PhysicsData->Mass = Mass;
		}
	}
}

////////////////////////////////////////////////////////////
/// Generate physics of the entity
////////////////////////////////////////////////////////////
void EntityPhysicsGenerate(struct EntityType * Entity, float ElapsedTime)
{
	if (Entity)
	{
		struct ComponentType * Physics = EntityComponentPhysics(Entity);
		struct ComponentPhysicsData * PhysicsData = ComponentDataPhysics(Physics);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Set velocity of the entity
////////////////////////////////////////////////////////////
void EntityPhysicsVelocity(struct EntityType * Entity, struct Vector3f * Velocity)
{
	if (Entity)
	{
		struct ComponentType * Physics = EntityComponentPhysics(Entity);
		struct ComponentPhysicsData * PhysicsData = ComponentDataPhysics(Physics);

		Vector3fCopy(&PhysicsData->Velocity, Velocity);
	}
}
