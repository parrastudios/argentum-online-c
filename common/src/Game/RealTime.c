/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/RealTime.h>

////////////////////////////////////////////////////////////
/// Initializes real time from path
////////////////////////////////////////////////////////////
bool RealTimeInitialize(struct RealTimeData * RealTime, char * Path)
{
	if (Path != NULL)
	{
		// todo: load from binary file

		return true;
	}
	
	// Initialize data by default otherwise
	RealTime->Milliseconds = RealTime->Year = 0;
	RealTime->Seconds = RealTime->Minutes = RealTime->Hour = RealTime->Day = RealTime->Month = 0;
	RealTime->MultiplyFactor = 1;

	return false;
}


////////////////////////////////////////////////////////////
/// Update real time from given adder
////////////////////////////////////////////////////////////
void RealTimeUpdate(struct RealTimeData * RealTime, uint32 TimeAdder)
{
	RealTime->Milliseconds	+= (TimeAdder * RealTime->MultiplyFactor);

	RealTime->Seconds		+= RealTime->Milliseconds / 1000;
	RealTime->Minutes		+= RealTime->Seconds / 60;
	RealTime->Hour			+= RealTime->Seconds / 60;
	RealTime->Day			+= RealTime->Hour / 24;
	RealTime->Month			+= RealTime->Day / 30;
	RealTime->Year			+= RealTime->Month / 12;

	RealTime->Milliseconds	%= 1000;
	RealTime->Seconds		%= 60;
	RealTime->Minutes		%= 60;
	RealTime->Hour			%= 24;
	RealTime->Day			%= 30;
	RealTime->Month			%= 12;
}

////////////////////////////////////////////////////////////
/// Destroy real time (if valid path it's stored in file)
////////////////////////////////////////////////////////////
bool RealTimeDestroy(struct RealTimeData * RealTime, char * Path)
{
	if (Path != NULL)
	{
		// todo: store into binary file

		return true;
	}

	return false;
}