/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Scene creator
//	Defines the format of the scene, can generate a default
//	(or custom) chuncks of data
//
// Documentation
//  file - scene: Contains information related to a portion
//                of the world map (name, description,
//                number of entities).
//
//  file - entities: Holds the list with information of the
//                   entities contained in the scene.
//                   Just information, not always the entity
//                   by itself.
//
//  file - terrain: Contains the information of the entity
//                  terrain associated to a scene.
//
//  file - model:   Holds all information about model meshes.
//  file - anim:    Holds all information about model bone animation.
//
//  file - particles: Contains all particles in a list.
//
//
//  ...
//
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define application_name			"Argentum Online C Scene Creator"
#define application_version			"0.1.0b"
#define application_major_version	0x00
#define application_minor_version	0x01
#define application_micro_version 	0x0b
#define application_version_hex		((application_major_version << 16) | \
									(application_minor_version << 8) | \
									(application_micro_version))

#ifndef null
#	define null (void*)0
#endif

#ifndef null_char
#   define null_char '\0'
#endif

#ifndef bool
    typedef unsigned char bool;
#   define false 0
#   define true 1
#endif

enum entity_type {
	entity_type_terrain = 0x00,
	entity_type_trigger,
	entity_type_model,
	entity_type_light,
	entity_type_particle,
	entity_type_count
};

enum weather_type {
    weather_type_sunny		= 0x01 << 0x00,
    weather_type_cloudy		= 0x01 << 0x01,
    weather_type_rainy		= 0x01 << 0x02,
    weather_type_stormy		= 0x01 << 0x03,
    weather_type_windy		= 0x01 << 0x04,
    weather_type_snowy		= 0x01 << 0x05,
    weather_type_foggy		= 0x01 << 0x06,
    weather_type_custom     = 0x01 << 0x07
};

enum scene_flags_type {
    scene_flags_safe_zone           = 0x01 << 0x00,
    scene_flags_magic_disabled      = 0x01 << 0x01,
    scene_flags_invisible_disabled  = 0x01 << 0x02,
    scene_flags_resurrect_disabled  = 0x01 << 0x03,
    scene_flags_npc_theft_enabled   = 0x01 << 0x04,
    scene_flags_backup              = 0x01 << 0x05
};

enum model_material_type {
	material_type_texture		= 0x01 << 0x00,
	material_type_diffuse		= 0x01 << 0x01,
	material_type_specular		= 0x01 << 0x02,
	material_type_normal		= 0x01 << 0x03,
	material_type_height		= 0x01 << 0x04,
	material_type_bump			= 0x01 << 0x05,
	material_type_count			= 6
};

enum light_type {
	light_spot = 0x00,
	light_point,
	light_type_count
};

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct header_type {
	unsigned int version;		// file version
	char description[0xff];		// file description
};

struct scene_descriptor { // scene.descriptor
	unsigned int width;
	unsigned int height;
	unsigned int *buffer; // world map table
};

struct entity_header_type {
	unsigned int terrain_index; // index of terrain entity (just one terrain by scene)
	unsigned int trigger_count;
	unsigned int model_count;
	unsigned int light_count;
	unsigned int particle_count;
	// ... ?
};

struct entity_chunck {
	float position[3];
	float scale[3];
	float force[3];
	float rotation[3];
	float orientation[4];
	float mass;

	//unsigned int type;		// just to allow dynamic union
	unsigned char payload;
};

struct terrain_chunk {
	unsigned int size;
	unsigned int patch_count;
	unsigned int frame_variance;
	unsigned int desired_tris;
	unsigned int pool_size;
	unsigned int layer_count;
	unsigned int texture_count;

	float water_height;

	unsigned char *heightmap;
	unsigned int *textures;
	unsigned char *patch_texture_count; // as a maximum 255 textures by patch
	unsigned int **patch_textures;		// patch_textures[patch_id] = texture_id
										// textures[texture_id] = %.png

	float *zone_uv;					// vector2f
	float *color_buffer;			// vector4f (mask used for multitexturing)
};

struct trigger_chunk {
	// region
};

struct model_chunk {
	unsigned int mesh_index;

	unsigned int animations_count;
	unsigned int *animations_index;

	unsigned int material_list[material_type_count];
};

struct light_chunk {
	unsigned int type; // spot, point

	float ambient[4];
	float diffuse[4];
	float specular[4];
	float attenuation[3]; // 0 : constant, 1 : linear, 2 : quadratic

	float radius;
};

struct particle_chunk {
	unsigned int texture_count;
	unsigned int *textures;

	// todo
	// ..
};

struct entity_resource {
    struct entity_header_type entity_info; // %d.entities
    struct entity_chunck *entity_list[entity_type_count];
};

struct scene_resource {
	char name[0x20];
	char description[0xff];

	unsigned int weather;
	unsigned int flags;

	unsigned int entity_resource_index; // %d.entities
	// ..
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

// generic file methods

#define file_get_path(path, index, format) "data/" path "/" #index "." format

#define file_buffer_cast_impl(buffer, position, type_name) ((type_name*)&buffer[position])

#define file_buffer_access_impl(buffer, position, type_name) *file_buffer_cast_impl(buffer, position, type_name)

#define file_buffer_increment(counter, type_name) counter += sizeof(type_name)

#define file_buffer_access(type_name) file_buffer_access_impl(buffer, count, type_name)

#define file_buffer_read_impl(buffer, counter, type_name) file_buffer_access_impl(buffer, counter, type_name); \
                                                            file_buffer_increment(counter, type_name)

#define file_buffer_read(type_name) file_buffer_read_impl(buffer, count, type_name)

#define file_buffer_read_array(array, size, type_name, i)  do { \
                                                            for (i = 0; i < size; ++i) { \
                                                                array[i] = file_buffer_read(type_name); \
                                                            } \
                                                        } while (0)

#define file_buffer_read_string(string, i) do { \
                                            for (i = 0; buffer[count] != null_char; ++i) { \
                                                string[i] = file_buffer_read(unsigned char); \
                                            } \
                                        } while (0)

#define file_buffer_read_header(header) header->version = file_buffer_read(unsigned int); \
                                        do { \
                                            unsigned int j; \
                                            file_buffer_read_string(header->description, j); \
                                        } while (0)

#define file_buffer_cast(type_name) file_buffer_cast_impl(buffer, count, type_name)

#define file_buffer_validate(buffer, header) (buffer != null && file_header_check_version(header))

#define file_buffer_begin(resource_type, index, format) \
        struct header_type header; \
		unsigned int file_size = 0; \
		unsigned int count = 0; \
		unsigned char *buffer = file_read(resource_type##_get_path(index, format), &file_size); \
		\
		file_buffer_read_header((&header)); \
		if (!file_buffer_validate(buffer, (&header))) \
            file_buffer_end(null)


#define file_buffer_end(resource)   free(resource); \
            do { if (count == file_size) { \
                return resource; \
            } else { \
                return null; \
            } } while (0)

bool file_header_check_version(struct header_type *header) {
	return (header->version == application_version_hex);
}

unsigned char * file_read(char *path, unsigned int *size) {
	FILE *file;
	unsigned char *buffer;
	unsigned int result;

	// open file
	file = fopen(path, "rb");

	if (file == null) {
		return null;
	}

	// obtain file size
	fseek(file, 0, SEEK_END);

	*size = ftell(file);

	// allocate buffer
	buffer = (unsigned char*)malloc((*size) * sizeof(unsigned char));

	if (buffer == null) {
		fclose(file);
		return null;
	}

	// read file
	rewind(file);

	result = fread(buffer, 1, *size, file);

	// close file
	fclose(file);

	if (result != *size) {
		return null;
	}

	return buffer;
}

// entity

#define entity_get_path(index, format) file_get_path("entities", index, format)

#define entity_sizeof(entity_type) (sizeof(struct entity_chunck) - 1 + sizeof(entity_type))

#define entity_allocate_impl(entity_type) (struct entity_chunck*)malloc(entity_sizeof(entity_type))

#define entity_calloc_impl(entity_type, size) (struct entity_chunck*)malloc(entity_sizeof(entity_type) * size)

#define entity_type_const(entity_name) entity_type_##entity_name

#define entity_create(entity, entity_name)	entity = entity_allocate_impl(struct entity_name##_chunk)//; \
											//entity->type = entity_type_const(entity_name)

#define entity_resource_list(resource, entity_name) resource->entity_list[entity_type_##entity_name]

#define entity_create_list(list, entity_name, size) list = entity_calloc_impl(struct entity_name##_chunk, size)

#define entity_create_resource(resource, entity_name)   entity_create( \
                                                        entity_resource_list(resource, entity_name), \
                                                        entity_name)

#define entity_create_resource_list(resource, entity_name)  entity_create_list( \
                                                            entity_resource_list(resource, entity_name), \
                                                            entity_name, \
                                                            resource->entity_info.entity_name##_count)

#define entity_access_impl(entity, entity_type) (entity_type*)(&((entity)->payload))

#define entity_attr(entity, entity_name) entity_access_impl(entity, struct entity_name##_chunk)

#define entity_attr_list_id(resource, entity_name, id) entity_attr(&entity_resource_list(resource, entity_name)[id], entity_name)

#define entity_attr_list(resource, entity_name) entity_attr_list_id(resource, entity_name, 0)


struct entity_resource *entity_read(struct entity_resource *resource, unsigned int index) {

    if (resource != null) {
        unsigned int i;

        // read entities file
        file_buffer_begin(entity, index, "entities");


        // read entity header info
		resource->entity_info.terrain_index     = file_buffer_read(unsigned int);
		resource->entity_info.trigger_count     = file_buffer_read(unsigned int);
		resource->entity_info.model_count       = file_buffer_read(unsigned int);
		resource->entity_info.light_count       = file_buffer_read(unsigned int);
		resource->entity_info.particle_count    = file_buffer_read(unsigned int);

        // create the entity list
        entity_create_resource(resource, terrain); // just one terrain by scene!
        entity_create_resource_list(resource, trigger);
        entity_create_resource_list(resource, model);
        entity_create_resource_list(resource, light);
        entity_create_resource_list(resource, particle);

        // store all entities into list

        // (terrain avoided)

        // read triggers
        for (i = 0; i < resource->entity_info.trigger_count; ++i) {
            struct trigger_chunk *trigger_ptr = entity_attr_list_id(resource, trigger, i);

            // todo
        }

        // read models
        for (i = 0; i < resource->entity_info.model_count; ++i) {
            unsigned int j;
            struct model_chunk *model_ptr = entity_attr_list_id(resource, model, i);

            // mesh index (contains the group of meshes)
            model_ptr->mesh_index = file_buffer_read(unsigned int);

            // animation count
            model_ptr->animations_count = file_buffer_read(unsigned int);

            // read animation list
            if (model_ptr->animations_count > 0) {
                model_ptr->animations_index = (unsigned int*)malloc(sizeof(unsigned int) * model_ptr->animations_count);

                for (j = 0; j < model_ptr->animations_count; ++j) {
                    model_ptr->animations_index[j] = file_buffer_read(unsigned int);
                }
            } else {
                model_ptr->animations_index = null;
            }

            // read material list
            for (j = 0; j < material_type_count; ++j) {
                model_ptr->material_list[j] = file_buffer_read(unsigned int);
            }
        }

        // read lights
        for (i = 0; i < resource->entity_info.light_count; ++i) {
            struct light_chunk *light_ptr = entity_attr_list_id(resource, light, i);

            // read light type
            light_ptr->type = file_buffer_read(unsigned int);

            // ambient component
            light_ptr->ambient[0] = file_buffer_read(float);
            light_ptr->ambient[1] = file_buffer_read(float);
            light_ptr->ambient[2] = file_buffer_read(float);
            light_ptr->ambient[3] = file_buffer_read(float);

            // diffuse component
            light_ptr->diffuse[0] = file_buffer_read(float);
            light_ptr->diffuse[1] = file_buffer_read(float);
            light_ptr->diffuse[2] = file_buffer_read(float);
            light_ptr->diffuse[3] = file_buffer_read(float);

            // specular component
            light_ptr->specular[0] = file_buffer_read(float);
            light_ptr->specular[1] = file_buffer_read(float);
            light_ptr->specular[2] = file_buffer_read(float);
            light_ptr->specular[3] = file_buffer_read(float);

            // attenuation component
            light_ptr->attenuation[0] = file_buffer_read(float);
            light_ptr->attenuation[1] = file_buffer_read(float);
            light_ptr->attenuation[2] = file_buffer_read(float);

            // read light radius
            light_ptr->radius = file_buffer_read(float);
        }

        // read particles
        for (i = 0; i < resource->entity_info.particle_count; ++i) {
            struct particle_chunk *particle_ptr = entity_attr_list_id(resource, particle, i);

            // todo
        }


        // finish reading
        file_buffer_end(resource);
    }

    return null;
}


// terrain

#define terrain_get_path(index, format) file_get_path("terrain", index, format)

struct terrain_chunk *terrain_read(struct terrain_chunk *terrain, unsigned int index) {

	if (terrain != null) {
	    unsigned int i;

        file_buffer_begin(terrain, index, "terrain");

		// parse file
		terrain->size = file_buffer_read(unsigned int);
		terrain->patch_count = file_buffer_read(unsigned int);
		terrain->frame_variance = file_buffer_read(unsigned int);
		terrain->desired_tris = file_buffer_read(unsigned int);
		terrain->pool_size = file_buffer_read(unsigned int);
		terrain->layer_count = file_buffer_read(unsigned int);

		terrain->water_height = file_buffer_read(float);

		terrain->heightmap = (unsigned char*)malloc(sizeof(unsigned char) * terrain->size * terrain->size);

		for (i = 0; i < terrain->size * terrain->size; ++i) {
			terrain->heightmap[i] = file_buffer_read(unsigned char);
		}

		terrain->patch_texture_count = (unsigned char*)malloc(sizeof(unsigned char) * terrain->patch_count * terrain->patch_count);
		terrain->patch_textures = (unsigned int**)malloc(sizeof(unsigned int*) * terrain->patch_count * terrain->patch_count);

		for (i = 0; i < terrain->patch_count * terrain->patch_count; ++i) {
			terrain->patch_texture_count[i] = file_buffer_read(unsigned char);
			terrain->patch_textures[i] = (unsigned int*)malloc(sizeof(unsigned int) * terrain->patch_texture_count[i]);
		}

		for (i = 0; i < terrain->patch_count * terrain->patch_count; ++i) {
			unsigned int j;

			for (j = 0; j < terrain->patch_texture_count[i]; ++j) {
				terrain->patch_textures[i][j] = file_buffer_read(unsigned int);
			}
		}


		// todo: uv, colorbuffer
        /*
        float *zone_uv;					// vector2f
        float *color_buffer;			// vector4f (mask used for multitexturing)
        */

		file_buffer_end(terrain);
	}

	return null;
}

// trigger




// light
/*
#define light_get_path(index, format) file_get_path("light", index, format)

struct light_chunk *light_read(struct light_chunk *light, unsigned int index) {

	if (light != null) {
		unsigned int file_size = 0;
		unsigned int count = 0;
		unsigned int i;
		unsigned char *buffer = file_read(light_get_path(index, "light"), &file_size);



		free(buffer);

		// assert
		if (count == file_size) {
			return light;
		}
	}

	return null;
}*/


// scene

#define scene_get_path(index, format) file_get_path("scene", index, format)

struct scene_resource *scene_read(struct scene_resource *scene, unsigned int index) {

	if (scene != null) {
	    unsigned int i;

        file_buffer_begin(scene, index, "scene");

		// parse file
		file_buffer_read_string(scene->name, i);
		file_buffer_read_string(scene->description, i);

		scene->weather  = file_buffer_read(unsigned int);
		scene->flags    = file_buffer_read(unsigned int);

		scene->entity_resource_index = file_buffer_read(unsigned int);

		file_buffer_end(scene);
	}

	return null;
}

struct scene_resource *scene_create(unsigned int index) {
    struct scene_resource *scene = (struct scene_resource*)malloc(sizeof(struct scene_resource));

    if (scene) {

        // read scene file
        if (!scene_read(scene, index)) {
            free(scene);
            return null;
        }

        // read entity file
        entities = entity_read(entities, index);


    }

    return null;


///////////
/*
    // read scene


    // read entity file
    entity_read()

    // read terrain file (one by scene)
    terrain_read(entity_attr_list(resource, terrain), resource->entity_info.terrain_index);

    // read models and animations files

    // read particle list file
*/
///////////
}

// application entry point

int main(int argc, char *argv[]) {
    struct terrain_chunk terrain;

    terrain_read(&terrain, 0);

	return 0;
}
