/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2016 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/Player.h>

////////////////////////////////////////////////////////////
/// Load all players from database
////////////////////////////////////////////////////////////
bool DBPlayerLoadList(Vector DBPlayerList)
{

	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Reload all players from database
////////////////////////////////////////////////////////////
bool DBPlayerReloadList(Vector DBPlayerList)
{

	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Store players to database
////////////////////////////////////////////////////////////
bool DBPlayerStoreList(Vector DBPlayerList)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Load the player from database
////////////////////////////////////////////////////////////
bool DBPlayerLoadById(struct DBCharacterData * Character, uinteger Index)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Load the player from row of database
////////////////////////////////////////////////////////////
bool DBPlayerLoadByRow(QueryRowType Row, struct DBCharacterData * Character)
{
	if (Character)
	{
		Character->Id = 0;
		strcpy(Character->Attributes.Player.Name, "Parra");
		Character->Attributes.Player.AccountId = 0;
		Character->Attributes.Player.Active = false;
		Character->Attributes.Player.Ban = false;

		Character->Attributes.Player.Class = 0;
		Character->Attributes.Player.Gender = 0;
		Character->Attributes.Player.Race = 0;

		Character->Attributes.Player.Home = 0;
		Character->Algin = 0;
		
		Character->Attributes.Player.Head = 0;
		Character->Attributes.Player.Body = 0;
		
		Character->Attributes.Player.Strength = 18;
		Character->Attributes.Player.Inteligence = 18;
		Character->Attributes.Player.Charisma = 18;
		Character->Attributes.Player.Constitution = 18;
		
		Character->Attributes.Player.Level = 32;
		Character->Attributes.Player.Exp = 123456789;
		Character->Attributes.Player.Gold = 50000000;
		
		Character->Attributes.Player.Weapon = 0;
		Character->Attributes.Player.Shield = 0;
		Character->Attributes.Player.Helmet = 0;
		Character->Attributes.Player.Footwear = 0;

		Character->Attributes.Player.MaxHp = 480;
		Character->Attributes.Player.MinHp = 480;
		Character->Attributes.Player.MaxSta = 390;
		Character->Attributes.Player.MinSta = 390;
		Character->Attributes.Player.MaxMan = 2000;
		Character->Attributes.Player.MinMan = 2000;
		Character->Attributes.Player.MaxHun = 100;
		Character->Attributes.Player.MinHun = 100;
		Character->Attributes.Player.MaxThi = 100;
		Character->Attributes.Player.MinThi = 100;

		Character->Attributes.Player.World = 0;

		Vector3fSet(&Character->Attributes.Player.Position, 0.0f, 0.0f, 0.0f);

		return true;
	}

	return false;
}

/*
////////////////////////////////////////////////////////////
/// Load the player from database
////////////////////////////////////////////////////////////
bool DBPlayerGetData(struct DBCharacterData * Character, Id)
{
	// todo

	void * Result = Query("SELECT * FROM `%s` WHERE name='%s'", DB_PLAYER_TABLE_DEFAULT, Name);

	if (Result)
	{
		void * Row = QueryGetRow(Result);

		if (Row)
		{
			// Get player data
			QueryGetField(Row, , , &Account->Active);

			// Release query
			QueryRelease(Result);

			return true;
		}
		else
		{
			// Release query
			QueryRelease(Result);

			return false;
		}

	}

	return false;
}
*/

////////////////////////////////////////////////////////////
/// Disconnect the player (store it in database)
////////////////////////////////////////////////////////////
bool DBPlayerStore(struct DBCharacterData * Character)
{
	// todo

	return true;
}

////////////////////////////////////////////////////////////
/// Set activate flag in database
////////////////////////////////////////////////////////////
void DBPlayerActive(struct DBCharacterData * Character)
{
	// Nothing
}

////////////////////////////////////////////////////////////
/// Check if a player exists in the database
////////////////////////////////////////////////////////////
bool DBPlayerExist(struct DBCharacterData * Character)
{
	if (Character)
	{
		return true;
	}

	return false;
}

bool DBPlayerCreate(struct DBCharacterData * Character)
{
	// todo: creates a new player and saves de values from input player (User)

	return true;
}

bool DBPlayerDelete(struct DBCharacterData * Character)
{
	// todo: removes a player from database

	return true;
}

