/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Memory/General.h>
#include <System/IOHelper.h>
#include <System/Error.h>
#include <Graphics/Device.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct DeviceType DeviceDefault;

struct DeviceType * DeviceGet()
{
	return &DeviceDefault;
}

struct DeviceInfoType * DeviceGetInfo()
{
	return &DeviceDefault.Info;
}

struct DeviceExtensionType * DeviceGetExt()
{
	return &DeviceDefault.Extensions;
}

DeviceTypeImpl * DeviceGetImpl()
{
	return &DeviceDefault.Impl;
}

////////////////////////////////////////////////////////////
/// Initialize current device (loads extensions and context)
////////////////////////////////////////////////////////////
void DeviceCreate(struct DeviceType * Device, struct VideoMode * Mode, struct WindowSettings * Params)
{
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
	int BestFormat, Color, Score;
	PIXELFORMATDESCRIPTOR ActualFormat;
	GLContextType CurrentContext;

    // Let's find a suitable pixel format -- first try with antialiasing
    BestFormat = 0;

    if (Params->AntialiasingLevel > 0)
    {
        // Define the basic attributes we want for our window
        int IntAttributes[] =
        {
            WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
		    WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		    WGL_ACCELERATION_ARB,   WGL_FULL_ACCELERATION_ARB,
		    WGL_DOUBLE_BUFFER_ARB,  GL_TRUE,
            WGL_SAMPLE_BUFFERS_ARB, (Params->AntialiasingLevel ? GL_TRUE : GL_FALSE),
		    WGL_SAMPLES_ARB,        Params->AntialiasingLevel,
		    0,                      0
        };

        // Let's check how many formats are supporting our requirements
        int   Formats[128];
	    UINT  NbFormats;
	    float FloatAttributes[] = {0, 0};
	    bool  IsValid = Device->Extensions.wglChoosePixelFormatARB(Device->Context, IntAttributes, FloatAttributes, sizeof(Formats) / sizeof(*Formats), Formats, &NbFormats) != 0;
        if (!IsValid || (NbFormats == 0))
        {
            if (Params->AntialiasingLevel > 2)
            {
                // No format matching our needs : reduce the multisampling level
				MessageError("CreateContext", "Failed to find a pixel format supporting %u antialiasing levels ; trying with 2 levels.", Params->AntialiasingLevel);

                Params->AntialiasingLevel = IntAttributes[1] = 2;

				IsValid = Device->Extensions.wglChoosePixelFormatARB(Device->Context, IntAttributes, FloatAttributes, sizeof(Formats) / sizeof(*Formats), Formats, &NbFormats) != 0;
            }

            if (!IsValid || (NbFormats == 0))
            {
                // Cannot find any pixel format supporting multisampling ; disabling antialiasing
                MessageError("CreateContext", "Failed to find a pixel format supporting antialiasing ; antialiasing will be disabled.");
                Params->AntialiasingLevel = 0;
            }
        }

        // Get the best format among the returned ones
        if (IsValid && (NbFormats > 0))
        {
            int  BestScore = 0xFFFF;
            UINT i		   = 0;
            for (i = 0; i < NbFormats; ++i)
            {
                // Get the current format's attributes
                PIXELFORMATDESCRIPTOR Attribs;
                Attribs.nSize    = sizeof(PIXELFORMATDESCRIPTOR);
                Attribs.nVersion = 1;
				DescribePixelFormat(Device->Context, Formats[i], sizeof(PIXELFORMATDESCRIPTOR), &Attribs);

                // Evaluate the current configuration
                Color = Attribs.cRedBits + Attribs.cGreenBits + Attribs.cBlueBits + Attribs.cAlphaBits;
                Score = SettingsEvaluateConfig(Mode, Params, Color,
											   Attribs.cDepthBits, Attribs.cStencilBits, Params->AntialiasingLevel);

                // Keep it if it's better than the current best
                if (Score < BestScore)
                {
                    BestScore  = Score;
                    BestFormat = Formats[i];
                }
            }
        }
    }

    // Find a pixel format with no antialiasing, if not needed or not supported
    if (BestFormat == 0)
    {
        // Setup a pixel format descriptor from the rendering settings
        PIXELFORMATDESCRIPTOR PixelDescriptor;
        ZeroMemory(&PixelDescriptor, sizeof(PIXELFORMATDESCRIPTOR));
        PixelDescriptor.nSize        = sizeof(PIXELFORMATDESCRIPTOR);
        PixelDescriptor.nVersion     = 1;
        PixelDescriptor.iLayerType   = PFD_MAIN_PLANE;
        PixelDescriptor.dwFlags      = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
        PixelDescriptor.iPixelType   = PFD_TYPE_RGBA;
        PixelDescriptor.cColorBits   = (BYTE)(Mode->BitsPerPixel);
        PixelDescriptor.cDepthBits   = (BYTE)(Params->DepthBits);
        PixelDescriptor.cStencilBits = (BYTE)(Params->StencilBits);

        // Get the pixel format that best matches our requirements
		BestFormat = ChoosePixelFormat(Device->Context, &PixelDescriptor);
        if (BestFormat == 0)
        {
            MessageError("CreateContext", "Failed to find a suitable pixel format for device context -- cannot create OpenGL context.");
            return;
        }
    }

    // Extract the depth and stencil bits from the chosen format
    ActualFormat.nSize    = sizeof(PIXELFORMATDESCRIPTOR);
    ActualFormat.nVersion = 1;
	DescribePixelFormat(Device->Context, BestFormat, sizeof(PIXELFORMATDESCRIPTOR), &ActualFormat);
    Params->DepthBits   = ActualFormat.cDepthBits;
    Params->StencilBits = ActualFormat.cStencilBits;

    // Set the chosen pixel format
	if (!SetPixelFormat(Device->Context, BestFormat, &ActualFormat))
    {
        MessageError("CreateContext", "Failed to set pixel format for device context -- cannot create OpenGL context.");
        return;
    }

    // Create the OpenGL context from the device context
	Device->GLContext = wglCreateContext(Device->Context);

	if (Device->GLContext == NULL)
    {
        MessageError("CreateContext", "Failed to create an OpenGL context for this window.");
        return;
    }

    // Share display lists with other contexts
    CurrentContext = wglGetCurrentContext();

    if (CurrentContext)
		wglShareLists(CurrentContext, Device->GLContext);

    // Activate the context
    DeviceSetRenderTarget(Device);

    // Enable multisampling
    if (Params->AntialiasingLevel > 0)
        glEnable(GL_MULTISAMPLE_ARB);

	// Update video mode
	Device->CurrentVideoMode.Width = Mode->Width;
	Device->CurrentVideoMode.Height = Mode->Height;
	Device->CurrentVideoMode.BitsPerPixel = Mode->BitsPerPixel;

#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD

#else

#endif

	// Initialize device
	DeviceInitialize(Device);
}

////////////////////////////////////////////////////////////
/// Destroy current device (delete extensions and context)
////////////////////////////////////////////////////////////
void DeviceDestroy(struct DeviceType * Device)
{
	// Destroy render stack
	StackDestroy(Device->MatrixRenderStack);

#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
	// Destroy device context
	if (Device->GLContext)
	{
		wglDeleteContext(Device->GLContext);
		Device->GLContext = NULL;
	}

#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD

#else

#endif
}

////////////////////////////////////////////////////////////
/// Reset current device
////////////////////////////////////////////////////////////
void DeviceReset(struct DeviceType * Device)
{
	// Reset the current view port
    glViewport(0, 0, Device->CurrentVideoMode.Width, Device->CurrentVideoMode.Height);
}

////////////////////////////////////////////////////////////
/// Configure device states for rendering
////////////////////////////////////////////////////////////
bool DeviceInitialize(struct DeviceType * Device)
{
	// Get library and device information
	Device->Info.Features.Vendor = (char*)glGetString(GL_VENDOR);
	Device->Info.Features.Renderer = (char*)glGetString(GL_RENDERER);
	Device->Info.Features.Version = (char*)glGetString(GL_VERSION);
	Device->Info.Features.ShaderVersion = (char*)glGetString(GL_SHADING_LANGUAGE_VERSION);

	// Load device extensions
	DeviceLoadExtensions(Device);

	// Get extension strings
	DeviceGetExtensionString(Device);

	if (!Device->Info.Features.Vendor || !Device->Info.Features.Renderer ||
		!Device->Info.Features.Version || !Device->Info.Features.ShaderVersion ||
		!Device->Info.Extensions)
	{
		// Error
		return false;
	}

	// Get bits info
	glGetIntegerv(GL_RED_BITS, &Device->Info.Bits.Red);
	glGetIntegerv(GL_GREEN_BITS, &Device->Info.Bits.Green);
	glGetIntegerv(GL_BLUE_BITS, &Device->Info.Bits.Blue);
	glGetIntegerv(GL_ALPHA_BITS, &Device->Info.Bits.Alpha);
	glGetIntegerv(GL_DEPTH_BITS, &Device->Info.Bits.Depth);
	glGetIntegerv(GL_STENCIL_BITS, &Device->Info.Bits.Stencil);

	// Get max number of clipping planes
	glGetIntegerv(GL_MAX_CLIP_PLANES, &Device->Info.Stack.ClipPlanes);

	// Get max modelview and projection matrix stacks
	glGetIntegerv(GL_MAX_MODELVIEW_STACK_DEPTH, &Device->Info.Stack.ModelView);
	glGetIntegerv(GL_MAX_PROJECTION_STACK_DEPTH, &Device->Info.Stack.Projection);
	glGetIntegerv(GL_MAX_ATTRIB_STACK_DEPTH, &Device->Info.Stack.Attrib);

	// Get max texture stacks
	glGetIntegerv(GL_MAX_TEXTURE_STACK_DEPTH, &Device->Info.Stack.Texture);

	// Get max number of lights, texture units, texture size and tessellation info
	glGetIntegerv(GL_MAX_LIGHTS, &Device->Info.Limits.Lights);
	glGetIntegerv(GL_MAX_TEXTURES_UNITS_ARB, &Device->Info.Limits.Units);
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &Device->Info.Limits.Texture);
	glGetIntegerv(GL_MAX_TESS_GEN_LEVEL, &Device->Info.Limits.Tessellation);
	glGetIntegerv(GL_MAX_PATCH_VERTICES, &Device->Info.Limits.PatchVertices);

	// Shade model
	glShadeModel(GL_SMOOTH); // GL_FLAT

	// Set perspective calculations
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);

	// Print features
#if COMPILE_TYPE == DEBUG_MODE
	DevicePrintFeatures(Device);
#endif

	// Initialize states manager
	StatesManagerInitialize(&Device->StatesManager);

	// Initialize matrix stack
	if (!Device->MatrixRenderStack)
	{
		Device->MatrixRenderStack = StackCreate(MATRIX_STACK_DEFAULT_SIZE);
	}

	return true;
}

////////////////////////////////////////////////////////////
/// Clear current device
////////////////////////////////////////////////////////////
void DeviceClear(struct DeviceType * Device, bool Depth, bool Stencil)
{
	// Reset device first
	DeviceReset(Device);

	// Clear device
	if (Depth == true)
	{
		if (Stencil == true)
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		}
		else
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}
	}
	else
	{
		glClear(GL_COLOR_BUFFER_BIT);
	}
}

////////////////////////////////////////////////////////////
/// Check if the current context is created
////////////////////////////////////////////////////////////
bool DeviceContextValid(struct DeviceType * Device)
{
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
	return (Device->GLContext != NULL);
#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD

#else
    return false;
#endif
}

////////////////////////////////////////////////////////////
/// Check if there's an active context on the current thread
////////////////////////////////////////////////////////////
bool DeviceContextActive(struct DeviceType * Device)
{
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
    return (wglGetCurrentContext() != NULL);
#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD

#else
    return false;
#endif
}

////////////////////////////////////////////////////////////
/// Enable / disable vertical synchronization
////////////////////////////////////////////////////////////
void DeviceUseVerticalSync(struct DeviceType * Device, bool Enabled)
{
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
	Device->Extensions.wglSwapIntervalEXT(Enabled ? 1 : 0);
#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD

#else

#endif
}

////////////////////////////////////////////////////////////
/// Set the render context associated to window context
////////////////////////////////////////////////////////////
void DeviceSetRenderTarget(struct DeviceType * Device)
{
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
	if (Device->Context != NULL)
	{
		if (wglGetCurrentContext() != Device->GLContext)
		{
			wglMakeCurrent(Device->Context, Device->GLContext);
		}
	}
	else
	{
		if (wglGetCurrentContext() == Device->GLContext)
            wglMakeCurrent(NULL, NULL);
	}
#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD

#else

#endif
}

////////////////////////////////////////////////////////////
// Return binding constant associated to the texture target
////////////////////////////////////////////////////////////
uint32 DeviceBindingConst(uint32 Target)
{
	switch (Target)
	{
		case GL_TEXTURE_1D :
			return GL_TEXTURE_BINDING_1D;

		case GL_TEXTURE_2D :
			return GL_TEXTURE_BINDING_2D;

		case GL_TEXTURE_3D :
			return GL_TEXTURE_BINDING_3D;

		case GL_TEXTURE_CUBE_MAP:
			return GL_TEXTURE_BINDING_CUBE_MAP;

#if 0 // not supported already
		case GL_TEXTURE_1D_ARRAY :
			return GL_TEXTURE_BINDING_1D_ARRAY;

		case GL_TEXTURE_2D_ARRAY :
			return GL_TEXTURE_BINDING_2D_ARRAY;

		case GL_TEXTURE_RECTANGLE :
			return GL_TEXTURE_BINDING_RECTANGLE;

		case GL_TEXTURE_BUFFER :
			return GL_TEXTURE_BINDING_BUFFER;

		case GL_TEXTURE_CUBE_MAP_ARRAY :
			return GL_TEXTURE_BINDING_CUBE_MAP_ARRAY;

		case GL_TEXTURE_2D_MULTISAMPLE :
			return GL_TEXTURE_BINDING_2D_MULTISAMPLE;

		case GL_TEXTURE_2D_MULTISAMPLE_ARRAY :
			return GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY;
#endif
	}

	return 0;
}

////////////////////////////////////////////////////////////
/// Check the last device error
////////////////////////////////////////////////////////////
void DeviceCheckError(char * File, uint32 Line)
{
    // Get the last error
    GLenum ErrorCode = glGetError();

    if (ErrorCode != GL_NO_ERROR)
    {
        char * Error = "unknown error";
        char * Desc  = "no description";

        // Decode the error code
        switch (ErrorCode)
        {
            case GL_INVALID_ENUM :
            {
                Error = "GL_INVALID_ENUM";
                Desc  = "an unacceptable value has been specified for an enumerated argument";
                break;
            }

            case GL_INVALID_VALUE :
            {
                Error = "GL_INVALID_VALUE";
                Desc  = "a numeric argument is out of range";
                break;
            }

            case GL_INVALID_OPERATION :
            {
                Error = "GL_INVALID_OPERATION";
                Desc  = "the specified operation is not allowed in the current state";
                break;
            }

            case GL_STACK_OVERFLOW :
            {
                Error = "GL_STACK_OVERFLOW";
                Desc  = "this command would cause a stack overflow";
                break;
            }

            case GL_STACK_UNDERFLOW :
            {
                Error = "GL_STACK_UNDERFLOW";
                Desc  = "this command would cause a stack underflow";
                break;
            }

            case GL_OUT_OF_MEMORY :
            {
                Error = "GL_OUT_OF_MEMORY";
                Desc  = "there is not enough memory left to execute the command";
                break;
            }

            case GL_INVALID_FRAMEBUFFER_OPERATION_EXT :
            {
                Error = "GL_INVALID_FRAMEBUFFER_OPERATION_EXT";
                Desc  = "the object bound to FRAMEBUFFER_BINDING_EXT is not \"framebuffer complete\"";
                break;
            }
        }

        // Log the error
		MessageError("GLCheckError", "An internal OpenGL call failed in %s ( %d ) : %s, %s.", File, Line, Error, Desc);
    }
}

////////////////////////////////////////////////////////////
// Prints graphic device and OpenGL features
////////////////////////////////////////////////////////////
void DevicePrintFeatures(struct DeviceType * Device)
{
	printf("Vendor:         %s\nRenderer: 	%s\nOpenGL Version: %s\nGLSL Version:	%s\nExtensions:	%s\n",
		Device->Info.Features.Vendor, Device->Info.Features.Renderer, Device->Info.Features.Version,
			Device->Info.Features.ShaderVersion, Device->Info.Extensions);
}

void DeviceExtensionStringAppend(struct DeviceType * Device, char * Extensions)
{
	uinteger CurrentLength = strlen(Device->Info.Extensions);
	uinteger AppendLength = strlen(Extensions);
	uinteger TotalLength = CurrentLength + AppendLength;

	if (TotalLength < DEVICE_EXTENSION_STRING_SIZE)
	{
		strncpy(&Device->Info.Extensions[CurrentLength], Extensions, AppendLength);

		Device->Info.Extensions[TotalLength] = NULLCHAR;
	}
}

////////////////////////////////////////////////////////////
/// Get string containing all API extensions
////////////////////////////////////////////////////////////
void DeviceGetExtensionString(struct DeviceType * Device)
{
	Device->Info.Extensions[0] = NULLCHAR;

	// Check render system
#	if DEVICE_SYSTEM_TYPE == DEVICE_SYSTEM_OPENGL

		// Get render system extensions depending on version
#		if RENDER_SYSTEM_TYPE == RENDER_SYSTEM_GL1 || RENDER_SYSTEM_TYPE == RENDER_SYSTEM_GL2
			DeviceExtensionStringAppend(Device, (char*)glGetString(GL_EXTENSIONS));
#		elif RENDER_SYSTEM_TYPE == RENDER_SYSTEM_GL3 || RENDER_SYSTEM_TYPE == RENDER_SYSTEM_GL4
			uint32 ExtensionSize = glGetIntegerv(GL__NUM_EXTENSIONS);
			uint32 i;

			for (i = 0; i < ExtensionSize; ++i)
			{
				DeviceExtensionStringAppend(Device, (char*)glGetStringi(GL_EXTENSIONS, i));
			}
#		endif

		// Get platform specific extensions
#		if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
			{
				PFNWGLGETEXTENSIONSSTRINGEXTPROC wglGetExtensionsStringEXT = (PFNWGLGETEXTENSIONSSTRINGEXTPROC)DeviceGetExtension(Device, "wglGetExtensionsStringEXT");

				if (wglGetExtensionsStringEXT)
				{
					DeviceExtensionStringAppend(Device, (char *)wglGetExtensionsStringEXT());
				}
			}
#		elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_BSD
			DeviceExtensionStringAppend(Device, (char*)glXQueryExtensionsString(Device->Context, Device->Impl.Screen));
#		endif

#	endif

}

bool DeviceLoadLibrary(struct DeviceType * Device)
{
#	if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
	Device->LibraryHandle = LoadLibraryA("opengl32.dll");
#	elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD
	Device->LibraryHandle = dlopen("libGL.so.1", RTLD_LAZY | RTLD_GLOBAL);
#	elif PLATFORM_TYPE == PLATFORM_MACOS
	Device->LibraryHandle.BundleURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault,
		CFSTR("/System/Library/Frameworks/OpenGL.framework"),
		kCFURLPOSIXPathStyle, true);
	Device->LibraryHandle.Bundle = CFBundleCreate(kCFAllocatorDefault, Device->LibraryHandle.BundleURL);
#	endif

	return (Device->LibraryHandle != NULL);
}

void DeviceDestroyLibrary(struct DeviceType * Device)
{
#	if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
	FreeLibrary(Device->LibraryHandle);
#	elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD
	dlclose(Device->LibraryHandle);
#	elif PLATFORM_TYPE == PLATFORM_MACOS
	CFRelease(Device->LibraryHandle.Bundle);
	CFRelease(Device->LibraryHandle.BundleURL);
#	endif
}

////////////////////////////////////////////////////////////
/// Load the device extension pointers
////////////////////////////////////////////////////////////
void DeviceLoadExtensions(struct DeviceType * Device)
{
	// Get supported extensions
	Device->Info.Support.TextureNPOT		= DeviceCheckExtension(Device, "GL_ARB_texture_non_power_of_two");
	Device->Info.Support.VBO				= DeviceCheckExtension(Device, "GL_ARB_vertex_buffer_object");
	Device->Info.Support.EdgeClamp			= DeviceCheckExtension(Device, "GL_EXT_texture_edge_clamp");
	Device->Info.Support.TextureCombine		= DeviceCheckExtension(Device, "GL_ARB_texture_env_combine");
	Device->Info.Support.CVA				= DeviceCheckExtension(Device, "GL_EXT_compiled_vertex_array");
	Device->Info.Support.VARFence			= DeviceCheckExtension(Device, "GL_NV_vertex_array_range") && DeviceCheckExtension(Device, "GL_NV_Fence");
	Device->Info.Support.PBuffer			= DeviceCheckExtension(Device, "WGL_ARB_pixel_format") && DeviceCheckExtension(Device, "WGL_ARB_pbuffer");
	Device->Info.Support.MakeCurrentRead	= DeviceCheckExtension(Device, "WGL_ARB_make_current_read");
	Device->Info.Support.Shaders			= DeviceCheckExtension(Device, "GL_ARB_shader_objects") && DeviceCheckExtension(Device, "GL_ARB_vertex_shader")
												&& DeviceCheckExtension(Device, "GL_ARB_fragment_shader");

	// Load library
	if (DeviceLoadLibrary(Device))
	{
		// Initialize extension pointers

		Device->Extensions.glAttachShader = DeviceGetExtension(Device, "glAttachShader");
		Device->Extensions.glBindAttribLocation = DeviceGetExtension(Device, "glBindAttribLocation");
		Device->Extensions.glBlendEquationSeparate = DeviceGetExtension(Device, "glBlendEquationSeparate");
		Device->Extensions.glCompileShader = DeviceGetExtension(Device, "glCompileShader");
		Device->Extensions.glCreateProgram = DeviceGetExtension(Device, "glCreateProgram");
		Device->Extensions.glCreateShader = DeviceGetExtension(Device, "glCreateShader");
		Device->Extensions.glDeleteProgram = DeviceGetExtension(Device, "glDeleteProgram");
		Device->Extensions.glDeleteShader = DeviceGetExtension(Device, "glDeleteShader");
		Device->Extensions.glDetachShader = DeviceGetExtension(Device, "glDetachShader");
		Device->Extensions.glDisableVertexAttribArray = DeviceGetExtension(Device, "glDisableVertexAttribArray");
		Device->Extensions.glDrawBuffers = DeviceGetExtension(Device, "glDrawBuffers");
		Device->Extensions.glEnableVertexAttribArray = DeviceGetExtension(Device, "glEnableVertexAttribArray");
		Device->Extensions.glGetActiveAttrib = DeviceGetExtension(Device, "glGetActiveAttrib");
		Device->Extensions.glGetActiveUniform = DeviceGetExtension(Device, "glGetActiveUniform");
		Device->Extensions.glGetAttachedShaders = DeviceGetExtension(Device, "glGetAttachedShaders");
		Device->Extensions.glGetAttribLocation = DeviceGetExtension(Device, "glGetAttribLocation");
		Device->Extensions.glGetProgramInfoLog = DeviceGetExtension(Device, "glGetProgramInfoLog");
		Device->Extensions.glGetProgramiv = DeviceGetExtension(Device, "glGetProgramiv");
		Device->Extensions.glGetShaderInfoLog = DeviceGetExtension(Device, "glGetShaderInfoLog");
		Device->Extensions.glGetShaderSource = DeviceGetExtension(Device, "glGetShaderSource");
		Device->Extensions.glGetShaderiv = DeviceGetExtension(Device, "glGetShaderiv");
		Device->Extensions.glGetUniformLocation = DeviceGetExtension(Device, "glGetUniformLocation");
		Device->Extensions.glGetUniformfv = DeviceGetExtension(Device, "glGetUniformfv");
		Device->Extensions.glGetUniformiv = DeviceGetExtension(Device, "glGetUniformiv");
		Device->Extensions.glGetVertexAttribPointerv = DeviceGetExtension(Device, "glGetVertexAttribPointerv");
		Device->Extensions.glGetVertexAttribdv = DeviceGetExtension(Device, "glGetVertexAttribdv");
		Device->Extensions.glGetVertexAttribfv = DeviceGetExtension(Device, "glGetVertexAttribfv");
		Device->Extensions.glGetVertexAttribiv = DeviceGetExtension(Device, "glGetVertexAttribiv");
		Device->Extensions.glIsProgram = DeviceGetExtension(Device, "glIsProgram");
		Device->Extensions.glIsShader = DeviceGetExtension(Device, "glIsShader");
		Device->Extensions.glLinkProgram = DeviceGetExtension(Device, "glLinkProgram");
		Device->Extensions.glShaderSource = DeviceGetExtension(Device, "glShaderSource");
		Device->Extensions.glStencilFuncSeparate = DeviceGetExtension(Device, "glStencilFuncSeparate");
		Device->Extensions.glStencilMaskSeparate = DeviceGetExtension(Device, "glStencilMaskSeparate");
		Device->Extensions.glStencilOpSeparate = DeviceGetExtension(Device, "glStencilOpSeparate");
		Device->Extensions.glUniform1f = DeviceGetExtension(Device, "glUniform1f");
		Device->Extensions.glUniform1fv = DeviceGetExtension(Device, "glUniform1fv");
		Device->Extensions.glUniform1i = DeviceGetExtension(Device, "glUniform1i");
		Device->Extensions.glUniform1iv = DeviceGetExtension(Device, "glUniform1iv");
		Device->Extensions.glUniform2f = DeviceGetExtension(Device, "glUniform2f");
		Device->Extensions.glUniform2fv = DeviceGetExtension(Device, "glUniform2fv");
		Device->Extensions.glUniform2i = DeviceGetExtension(Device, "glUniform2i");
		Device->Extensions.glUniform2iv = DeviceGetExtension(Device, "glUniform2iv");
		Device->Extensions.glUniform3f = DeviceGetExtension(Device, "glUniform3f");
		Device->Extensions.glUniform3fv = DeviceGetExtension(Device, "glUniform3fv");
		Device->Extensions.glUniform3i = DeviceGetExtension(Device, "glUniform3i");
		Device->Extensions.glUniform3iv = DeviceGetExtension(Device, "glUniform3iv");
		Device->Extensions.glUniform4f = DeviceGetExtension(Device, "glUniform4f");
		Device->Extensions.glUniform4fv = DeviceGetExtension(Device, "glUniform4fv");
		Device->Extensions.glUniform4i = DeviceGetExtension(Device, "glUniform4i");
		Device->Extensions.glUniform4iv = DeviceGetExtension(Device, "glUniform4iv");
		Device->Extensions.glUniformMatrix2fv = DeviceGetExtension(Device, "glUniformMatrix2fv");
		Device->Extensions.glUniformMatrix3fv = DeviceGetExtension(Device, "glUniformMatrix3fv");
		Device->Extensions.glUniformMatrix4fv = DeviceGetExtension(Device, "glUniformMatrix4fv");
		Device->Extensions.glUseProgram = DeviceGetExtension(Device, "glUseProgram");
		Device->Extensions.glValidateProgram = DeviceGetExtension(Device, "glValidateProgram");
		Device->Extensions.glVertexAttrib1d = DeviceGetExtension(Device, "glVertexAttrib1d");
		Device->Extensions.glVertexAttrib1dv = DeviceGetExtension(Device, "glVertexAttrib1dv");
		Device->Extensions.glVertexAttrib1f = DeviceGetExtension(Device, "glVertexAttrib1f");
		Device->Extensions.glVertexAttrib1fv = DeviceGetExtension(Device, "glVertexAttrib1fv");
		Device->Extensions.glVertexAttrib1s = DeviceGetExtension(Device, "glVertexAttrib1s");
		Device->Extensions.glVertexAttrib1sv = DeviceGetExtension(Device, "glVertexAttrib1sv");
		Device->Extensions.glVertexAttrib2d = DeviceGetExtension(Device, "glVertexAttrib2d");
		Device->Extensions.glVertexAttrib2dv = DeviceGetExtension(Device, "glVertexAttrib2dv");
		Device->Extensions.glVertexAttrib2f = DeviceGetExtension(Device, "glVertexAttrib2f");
		Device->Extensions.glVertexAttrib2fv = DeviceGetExtension(Device, "glVertexAttrib2fv");
		Device->Extensions.glVertexAttrib2s = DeviceGetExtension(Device, "glVertexAttrib2s");
		Device->Extensions.glVertexAttrib2sv = DeviceGetExtension(Device, "glVertexAttrib2sv");
		Device->Extensions.glVertexAttrib3d = DeviceGetExtension(Device, "glVertexAttrib3d");
		Device->Extensions.glVertexAttrib3dv = DeviceGetExtension(Device, "glVertexAttrib3dv");
		Device->Extensions.glVertexAttrib3f = DeviceGetExtension(Device, "glVertexAttrib3f");
		Device->Extensions.glVertexAttrib3fv = DeviceGetExtension(Device, "glVertexAttrib3fv");
		Device->Extensions.glVertexAttrib3s = DeviceGetExtension(Device, "glVertexAttrib3s");
		Device->Extensions.glVertexAttrib3sv = DeviceGetExtension(Device, "glVertexAttrib3sv");
		Device->Extensions.glVertexAttrib4Nbv = DeviceGetExtension(Device, "glVertexAttrib4Nbv");
		Device->Extensions.glVertexAttrib4Niv = DeviceGetExtension(Device, "glVertexAttrib4Niv");
		Device->Extensions.glVertexAttrib4Nsv = DeviceGetExtension(Device, "glVertexAttrib4Nsv");
		Device->Extensions.glVertexAttrib4Nub = DeviceGetExtension(Device, "glVertexAttrib4Nub");
		Device->Extensions.glVertexAttrib4Nubv = DeviceGetExtension(Device, "glVertexAttrib4Nubv");
		Device->Extensions.glVertexAttrib4Nuiv = DeviceGetExtension(Device, "glVertexAttrib4Nuiv");
		Device->Extensions.glVertexAttrib4Nusv = DeviceGetExtension(Device, "glVertexAttrib4Nusv");
		Device->Extensions.glVertexAttrib4bv = DeviceGetExtension(Device, "glVertexAttrib4bv");
		Device->Extensions.glVertexAttrib4d = DeviceGetExtension(Device, "glVertexAttrib4d");
		Device->Extensions.glVertexAttrib4dv = DeviceGetExtension(Device, "glVertexAttrib4dv");
		Device->Extensions.glVertexAttrib4f = DeviceGetExtension(Device, "glVertexAttrib4f");
		Device->Extensions.glVertexAttrib4fv = DeviceGetExtension(Device, "glVertexAttrib4fv");
		Device->Extensions.glVertexAttrib4iv = DeviceGetExtension(Device, "glVertexAttrib4iv");
		Device->Extensions.glVertexAttrib4s = DeviceGetExtension(Device, "glVertexAttrib4s");
		Device->Extensions.glVertexAttrib4sv = DeviceGetExtension(Device, "glVertexAttrib4sv");
		Device->Extensions.glVertexAttrib4ubv = DeviceGetExtension(Device, "glVertexAttrib4ubv");
		Device->Extensions.glVertexAttrib4uiv = DeviceGetExtension(Device, "glVertexAttrib4uiv");
		Device->Extensions.glVertexAttrib4usv = DeviceGetExtension(Device, "glVertexAttrib4usv");
		Device->Extensions.glVertexAttribPointer = DeviceGetExtension(Device, "glVertexAttribPointer");

		Device->Extensions.glLockArraysEXT = DeviceGetExtension(Device, "glLockArraysEXT");
		Device->Extensions.glUnlockArraysEXT = DeviceGetExtension(Device, "glUnlockArraysEXT");

		Device->Extensions.glMultiTexCoord2fARB = DeviceGetExtension(Device, "glMultiTexCoord2fARB");
		Device->Extensions.glActiveTextureARB = DeviceGetExtension(Device, "glActiveTextureARB");
		Device->Extensions.glClientActiveTextureARB = DeviceGetExtension(Device, "glClientActiveTextureARB");

		Device->Extensions.glGenBuffersARB = DeviceGetExtension(Device, "glGenBuffersARB");
		Device->Extensions.glBindBufferARB = DeviceGetExtension(Device, "glBindBufferARB");
		Device->Extensions.glBufferDataARB = DeviceGetExtension(Device, "glBufferDataARB");
		Device->Extensions.glBufferSubDataARB = DeviceGetExtension(Device, "glBufferSubDataARB");
		Device->Extensions.glDeleteBuffersARB = DeviceGetExtension(Device, "glDeleteBuffersARB");
		Device->Extensions.glMapBufferARB = DeviceGetExtension(Device, "glMapBufferARB");
		Device->Extensions.glUnmapBufferARB = DeviceGetExtension(Device, "glUnmapBufferARB");
		Device->Extensions.glVertexAttribPointerARB = DeviceGetExtension(Device, "glVertexAttribPointerARB");

		Device->Extensions.glGenVertexArray = DeviceGetExtension(Device, "glGenVertexArray");
		Device->Extensions.glBindVertexArray = DeviceGetExtension(Device, "glBindVertexArray");

#	if PLATFORM_TYPE == PLATFORM_WINDOWS_NT

		Device->Extensions.wglSwapIntervalEXT = DeviceGetExtension(Device, "wglSwapIntervalEXT");
		Device->Extensions.wglGetSwapIntervalEXT = DeviceGetExtension(Device, "wglGetSwapIntervalEXT");

		Device->Extensions.wglGetPixelFormatAttribivARB = DeviceGetExtension(Device, "wglGetPixelFormatAttribivARB");
		Device->Extensions.wglGetPixelFormatAttribfvARB = DeviceGetExtension(Device, "wglGetPixelFormatAttribfvARB");
		Device->Extensions.wglChoosePixelFormatARB = DeviceGetExtension(Device, "wglChoosePixelFormatARB");

		Device->Extensions.wglMakeContextCurrentARB = DeviceGetExtension(Device, "wglMakeContextCurrentARB");
		Device->Extensions.wglGetCurrentReadDCARB = DeviceGetExtension(Device, "wglGetCurrentReadDCARB");

		Device->Extensions.wglCreatePbufferARB = DeviceGetExtension(Device, "wglCreatePbufferARB");
		Device->Extensions.wglGetPbufferDCARB = DeviceGetExtension(Device, "wglGetPbufferDCARB");
		Device->Extensions.wglReleasePbufferDCARB = DeviceGetExtension(Device, "wglReleasePbufferDCARB");
		Device->Extensions.wglDestroyPbufferARB = DeviceGetExtension(Device, "wglDestroyPbufferARB");
		Device->Extensions.wglQueryPbufferARB = DeviceGetExtension(Device, "wglQueryPbufferARB");

		Device->Extensions.glFlushVertexArrayRangeNV = DeviceGetExtension(Device, "glFlushVertexArrayRangeNV");
		Device->Extensions.glVertexArrayRangeNV = DeviceGetExtension(Device, "glVertexArrayRangeNV");
		Device->Extensions.wglAllocateMemoryNV = DeviceGetExtension(Device, "wglAllocateMemoryNV");
		Device->Extensions.wglFreeMemoryNV = DeviceGetExtension(Device, "wglFreeMemoryNV");

		Device->Extensions.glDeleteFencesNV = DeviceGetExtension(Device, "glDeleteFencesNV");
		Device->Extensions.glGenFencesNV = DeviceGetExtension(Device, "glGenFencesNV");
		Device->Extensions.glIsFenceNV = DeviceGetExtension(Device, "glIsFenceNV");
		Device->Extensions.glTestFenceNV = DeviceGetExtension(Device, "glTestFenceNV");
		Device->Extensions.glGetFenceivNV = DeviceGetExtension(Device, "glGetFenceivNV");
		Device->Extensions.glFinishFenceNV = DeviceGetExtension(Device, "glFinishFenceNV");
		Device->Extensions.glSetFenceNV = DeviceGetExtension(Device, "glSetFenceNV");

#	elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD

#	elif PLATFORM_TYPE == PLATFORM_MACOS

#	endif

		// Destroy library
		DeviceDestroyLibrary(Device);
	}
}

////////////////////////////////////////////////////////////
/// Check the OpenGL extensions
////////////////////////////////////////////////////////////
bool DeviceCheckExtension(struct DeviceType * Device, char * Extension)
{
	char * ExtensionsItr = &Device->Info.Extensions[0];
	char * ExtensionsEnd = ExtensionsItr + strlen(ExtensionsItr);
	uint32 ExtensionLength = strlen(Extension);

	while (ExtensionsItr < ExtensionsEnd)
	{
		uint32 Length = strcspn(ExtensionsItr, " ");

		if ((ExtensionLength == Length) && (strncmp(Extension, ExtensionsItr, Length) == 0))
			return true;

		ExtensionsItr += (Length + 1);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Get the process address from an extension string
////////////////////////////////////////////////////////////
void * DeviceGetExtension(struct DeviceType * Device, char * Extension)
{
	void * ExtensionPtr = NULL;

#	if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
		ExtensionPtr = wglGetProcAddress(Extension);

		if (ExtensionPtr == NULL || ExtensionPtr == (void*)0x1 || ExtensionPtr == (void*)0x2 ||
			ExtensionPtr == (void*)0x3 || ExtensionPtr == (void*)-1)
		{
			ExtensionPtr = GetProcAddress(Device->LibraryHandle, Extension);
		}
#	elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD
		ExtensionPtr = glXGetProcAddress((const GLubyte*)Extension);

		if (!ExtensionPtr)
		{
			ExtensionPtr = dlsym(Device->LibraryHandle, Extension);
		}
#	elif PLATFORM_TYPE == PLATFORM_MACOS
		CFStringRef ExtString = CFStringCreateWithCString(kCFAllocatorDefault, Extension, kCFStringEncodingASCII);
		ExtensionPtr = CFBundleGetFunctionPointerForName(Device->LibraryHandle.Bundle, ExtString);
		CFRelease(ExtString);
#	endif

		return ExtensionPtr;
}

void DeviceSetState(struct DeviceType * Device, uint32 Mode)
{
	if (Mode & DEVICE_RENDERING_2D)
	{
		// Set states
		StatesManagerSet(&Device->StatesManager, STATES_LIGHTING_MODE,	STATES_LIGHTING_DISABLED);
		StatesManagerSet(&Device->StatesManager, STATES_DEPTH_MODE, STATES_DEPTH_DISABLED);
		StatesManagerSet(&Device->StatesManager, STATES_DITHER_MODE, STATES_DITHER_DISABLED);
		StatesManagerSet(&Device->StatesManager, STATES_CULLFACE_MODE, STATES_CULLFACE_DISABLED);
		StatesManagerSet(&Device->StatesManager, STATES_LIGHTING_MODE, STATES_LIGHTING_DISABLED);
		StatesManagerSet(&Device->StatesManager, STATES_TEXTURING_MODE, STATES_TEXTURING_2D);
		StatesManagerSet(&Device->StatesManager, STATES_POLYGON_MODE, STATES_POLYGON_FILL);
		StatesManagerSet(&Device->StatesManager, STATES_COLOR_MODE, STATES_COLOR_ENABLED);

		// Set alpha
		if (Mode & DEVICE_RENDERING_ALPHA)
		{
			StatesManagerSet(&Device->StatesManager, STATES_BLEND_MODE, STATES_BLEND_ALPHA);
		}
		else
		{
			StatesManagerSet(&Device->StatesManager, STATES_BLEND_MODE, STATES_BLEND_NORMAL);
		}

		// Apply states
		StatesManagerApply(&Device->StatesManager);
	}
	else if (Mode & DEVICE_RENDERING_3D)
	{
		// Set states
		StatesManagerSet(&Device->StatesManager, STATES_DEPTH_MODE, STATES_DEPTH_ENABLED);
		StatesManagerSet(&Device->StatesManager, STATES_DITHER_MODE, STATES_DITHER_ENABLED);
		StatesManagerSet(&Device->StatesManager, STATES_CULLFACE_MODE, STATES_CULLFACE_ENABLED);
		StatesManagerSet(&Device->StatesManager, STATES_LIGHTING_MODE, STATES_LIGHTING_ENABLED);
		StatesManagerSet(&Device->StatesManager, STATES_TEXTURING_MODE, STATES_TEXTURING_2D); // STATES_TEXTURING_DISABLED
		StatesManagerSet(&Device->StatesManager, STATES_POLYGON_MODE, STATES_POLYGON_FILL); // STATES_POLYGON_WIREFRAME
		StatesManagerSet(&Device->StatesManager, STATES_COLOR_MODE, STATES_COLOR_ENABLED);

		// Set alpha (todo)
		if (Mode & DEVICE_RENDERING_ALPHA)
		{
			//glEnable(GL_DEPTH_ALPHA_TEST);
			//glAlphaFunc(GL_GREATER, 0.1f);
		}
		else
		{
			//glDisable(GL_DEPTH_ALPHA_TEST);
		}

		StatesManagerSet(&Device->StatesManager, STATES_BLEND_MODE, STATES_BLEND_NORMAL);

		// Apply states
		StatesManagerApply(&Device->StatesManager);
	}
}

////////////////////////////////////////////////////////////
/// Set current matrix
////////////////////////////////////////////////////////////
void DeviceSetMatrix(struct DeviceType * Device, struct Matrix4f * Matrix)
{
	// Set current matrix
	StackLoadMatrix(Device->MatrixRenderStack, Matrix);
}

////////////////////////////////////////////////////////////
/// Set current matrix to orthogonal ones
////////////////////////////////////////////////////////////
void DeviceSetMatrixOrthogonal(struct DeviceType * Device)
{
	struct Matrix4f OrthoMatrix;
	GLfloat Viewport[4];

	// Get view port
	glGetFloatv(GL_VIEWPORT, Viewport);

	// Calculate the projection	from viewport
	Matrix4fLoadOrtho(&OrthoMatrix, Viewport[0], Viewport[0] + Viewport[2], Viewport[1] + Viewport[3], Viewport[1], -1.0f, 1.0f);

	// Apply orthographic projection
	StackLoadMatrix(Device->MatrixRenderStack, &OrthoMatrix);
}

////////////////////////////////////////////////////////////
/// Apply current matrix
////////////////////////////////////////////////////////////
//void DeviceApplyMatrix(struct DeviceType * Device)
//{
//	// Apply the current matrix
//	glLoadMatrixf((GLfloat*)&StackGetMatrix(Device->MatrixRenderStack)->v[0]);
//}

void DeviceBegin(struct DeviceType * Device)
{
	GLfloat * GLViewProjMatrix = (GLfloat*)&StackGetMatrix(Device->MatrixRenderStack)->v[0];

	// Save OpenGL render states
	glPushAttrib(GL_ALL_ATTRIB_BITS);

	// Save current state
	StatesManagerSave(&Device->StatesManager);

	// Apply projection to OpenGL
    glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	glLoadMatrixf(GLViewProjMatrix);

	StackPush(Device->MatrixRenderStack);

	// Enable modelview to OpenGL
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	// Set identity by default
	glLoadIdentity();
}

void DeviceEnd(struct DeviceType * Device)
{
	// Restore state
	StatesManagerRestore(&Device->StatesManager);

	// Restore render states
	glPopAttrib();

	// Restore model view
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	// Restore projection
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	// Restore projection in the stack
	StackPop(Device->MatrixRenderStack);
}
