/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Plugin/Connector.h>
#include <Plugin/Plugin.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Retreive the name symbol of connector functor
////////////////////////////////////////////////////////////
const char * ConnectorName()
{
	return PLUGIN_CONNECTOR_NAME_STR;
}

////////////////////////////////////////////////////////////
/// Bind address from plugin handle to the object
////////////////////////////////////////////////////////////
bool ConnectorBind(struct PluginType * Plugin, ConnectorAddress Address)
{
	struct PluginConnectorType * Connector = &Plugin->Connector;
	ConnectorRegisterHandler Register;

	// The native connector, autodefined in all plugins
	Connector->Address = Address;
	Register = Connector->Address;

	if (Register)
	{
		Register(&Plugin->Connector.Info); // todo: improve this

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Invoke the connector functor of the plugin
////////////////////////////////////////////////////////////
bool ConnectorInvoke(PluginType * Plugin)
{
	struct PluginConnectorInfoType * Info = Plugin->Connector.Info;

	// The custom connector, defined by the user to register the plugin interface
	if (Info && Info->Name != NULL && Info->Register != NULL)
	{
		// Execute custom register
		uint32 RetCode = Info->Register();

		return (RetCode > 0);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Initialize a connector type
////////////////////////////////////////////////////////////
PluginConnectorType * ConnectorInitialize(PluginConnectorType * Connector)
{
	if (Connector)
	{
		// Initialize connector functions
		Connector->Name = &ConnectorName;
		Connector->Bind = &ConnectorBind;
		Connector->Invoke = &ConnectorInvoke;

		// Initialize connector address to null
		Connector->Address = NULL;

		// Initialize connector info to null
		Connector->Info = NULL;

		// Create hash map of address of plugin
		Connector->HookMap = HashMapCreate(HashGetCallbackStringDefault(), CompareGetCallback(COMPARE_FUNC_CSTRING));
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Release a connector type
////////////////////////////////////////////////////////////
void ConnectorDestroy(PluginConnectorType * Connector)
{
	if (Connector)
	{
		// Clear connector functions
		Connector->Name = NULL;
		Connector->Bind = NULL;
		Connector->Invoke = NULL;

		// Set connector address to null
		Connector->Address = NULL;

		// Set connector info to null
		Connector->Info = NULL;

		// Clear address hash map
		HashMapDestroy(Connector->HookMap);
	}
}
