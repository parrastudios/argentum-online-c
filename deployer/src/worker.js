// imports
var zmq			= require('zmq'),
	requester	= zmq.socket('req'),
	responder	= zmq.socket('rep'),
	q     	 	= require('q'),
	utils	 	= require('./utils.js');
		  
// recive arguments from command line
var remote_url		= process.argv[2] || 'tcp://localhost:8889';
var register_text	= process.argv[3] || 'register';
var message_text		= process.argv[4] || 'done';
var port_status 	= process.argv[6] || 9000;
var id 			= utils.randString();



		responder.bind('tcp://*:'+port_status,function(err) {
			if(err) 
			{
				console.log(err);
			} else {
				console.log("listening on port: "+port_status);
			}
		});
		responder.on('message',function(){
				console.log("Message received status:");
				var load = getLoad();
				console.log("Sending load: "+load);
				responder.send(load);

		});
		var connect_callback = function() {
			var d = q.defer();

			if (requester.connect(url)) {
				d.resolve('Connected to broker');
			} else {
				d.reject('Unnable to connect to broker');
			}

			return d.promise;
		};

		requester.on('message', function() {
			
			var d = q.defer();
				
			d.resolve(Array.apply(null, arguments));
			d.promise.then(

			function(args) {
				 	utils.debug_message(args[1],'Message received from broker: client_id(', args[0], ') client_text(', args[2], ')');
		    
		    // set requester id
			requester['identity'] = id;
		  	
		  	// send new packet back to broker
		  	requester.send([args[0], '', text_done]); 
			}, function(err) {
				// print error
				utils.debug_message(err);
				requester.close();
			});
		});


		var close_callback = function() {
			var d = q.defer();

			process.on('SIGINT', function() {
				d.resolve('Close signal recived properly');
			});	

			setTimeout(function() {
				d.reject('Timeout, close signal lost');
			}, 3600000); // 1 hour

			return d.promise;
		};

		// ser verbose mode
		utils.debug_set_verbose(process.argv[5]);

		utils.debug_message('url : ', url);	

		// connect to the broker
		connect_callback().then(function(result) {
			// show result
			utils.debug_message("Result: "+result);

			// set requester id
			requester['identity'] = id;

			// send text to register
			requester.send([text_ava,'',port_status,'',0]);
		}, function(err) {
			// print error
			console.log('nice error');
			utils.debug_message(err);
			requester.close();
		});

		close_callback().then(function(result) {
			// print result
			utils.debug_message(result);

			// close socket
			requester.close();

			// exit process
			process.exit(0);
		}, function(error) {
			// print error
			utils.debug_message(error);

			// close socket
			requester.close();

			// exit process
			process.exit(1);
		});


	function getLoad() {
	  var fs     = require('fs')//Este código está tal cual. 
	  , data   = fs.readFileSync("/proc/loadavg") //version sincrona
	  , tokens = data.toString().split(' ')
	  , min1   = parseFloat(tokens[0])+0.01
	  , min5   = parseFloat(tokens[1])+0.01
	  , min15  = parseFloat(tokens[2])+0.01
	  , m      = min1*10 + min5*2 + min15;
	  return m;
	}
