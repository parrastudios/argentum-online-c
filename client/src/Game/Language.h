/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

#ifndef GAME_LANGUAGE_H
#define GAME_LANGUAGE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define LANGUAGE_SPANISH			0x00
#define LANGUAGE_ENGLISH			0x01

#define LANGUAGE_SIZE				0x02	///< Number of languages availables
#define LANGUAGE_MESSAGE_LENGHT		0xFF	///< Max size of each message
#define LANGUAGE_MESSAGE_SIZE		0x0A	///< Number of messages

#define MSG_USER					0x00
#define MSG_PASSWORD				0x01
#define MSG_CONNECT					0x02
#define MSG_EXIT					0x03
#define MSG_ACCOUNT					0x04
#define MSG_LEVEL					0x05
#define MSG_GOLD					0x06
#define MSG_DEAD					0x07
#define MSG_YES						0x08
#define MSG_NO						0x09

#define LANGUAGE_MSG_ERROR			0xFFFF	///< Error

////////////////////////////////////////////////////////////
/// Lenguage initializer
////////////////////////////////////////////////////////////
bool LanguageInitialize();

////////////////////////////////////////////////////////////
/// Message loader
////////////////////////////////////////////////////////////
bool LanguageLoad(int32 Idiom);

////////////////////////////////////////////////////////////
/// Message method
////////////////////////////////////////////////////////////
char * LanguageMessage(uint32 MessageIndex);

////////////////////////////////////////////////////////////
/// Get the message id with a message text
////////////////////////////////////////////////////////////
uint32 LanguageMessageText(char * MessageType);

////////////////////////////////////////////////////////////
/// Simplification method
////////////////////////////////////////////////////////////
char * LanguageMessageGet(char * Type);

#endif // GAME_LANGUAGE_H
