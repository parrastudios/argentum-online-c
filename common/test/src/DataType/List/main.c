/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/DataTypes/List.h>
#include <System/IOHelper.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct TestData
{
	uint32	IntegerData;
	uint32	IntegerDataDouble;
	char *	StringTag;
};

////////////////////////////////////////////////////////////
// Macro definitions
////////////////////////////////////////////////////////////
#define Stringfy(Value)			#Value
#define DeclareTestData(Name, Value)	struct TestData Name = { Value, Value * 2, "Test <" Stringfy(Value) ">" }
#define SetTestData(DataPtr, Value)	do { (DataPtr)->IntegerData = Value; (DataPtr)->IntegerDataDouble = Value * 2; (DataPtr)->StringTag = "Test <" Stringfy(Value) ">"; } while (0)
#define PrintTestData(DataPtr)		printf("Name: " Stringfy(DataPtr) " - Integer: %d - IntegerDouble: %d - Tag: %s\n", (DataPtr)->IntegerData, (DataPtr)->IntegerDataDouble, (DataPtr)->StringTag)

////////////////////////////////////////////////////////////
/// Program entry point
////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
	List DataByCopyList = ListNew(sizeof(struct TestData));
	List DataByRefList = ListNew(sizeof(struct TestData *));
	ListIterator Iterator;

	// Initialize data
	DeclareTestData(Zero, 0);
	DeclareTestData(One, 1);
	DeclareTestData(Two, 2);

	struct TestData * RefZero = &Zero, * RefOne = &One, * RefTwo = &Two;

	// Push back to the copy list the data
	ListPushBack(DataByCopyList, (void *)&Zero);
	ListPushBack(DataByCopyList, (void *)&One);
	ListPushBack(DataByCopyList, (void *)&Two);

	// Push back to the reference list the data
	ListPushBack(DataByRefList, (void *)&RefZero);
	ListPushBack(DataByRefList, (void *)&RefOne);
	ListPushBack(DataByRefList, (void *)&RefTwo);

	printf("Copy List\n");

	// Print copy list
	ListForEach(DataByCopyList, Iterator)
	{
		// Obtain the reference to the copy
		struct TestData * Ref = &(ListItData(Iterator, struct TestData));

		if (Ref)
		{
			// Print the structure
			PrintTestData(Ref);
		}
	}

	printf("Ref List\n");

	// Print reference list
	ListForEach(DataByRefList, Iterator)
	{
		// Obtain the reference to the copy
		struct TestData * Ref = ListItData(Iterator, struct TestData *);

		if (Ref)
		{
			// Print the structure
			PrintTestData(Ref);
		}
	}

	// Modify the original data
	SetTestData(&Zero, 5);
	SetTestData(&One, 6);
	SetTestData(&Two, 7);

	printf("Copy List\n");

	// Print copy list
	ListForEach(DataByCopyList, Iterator)
	{
		// Obtain the reference to the copy
		struct TestData * Ref = &(ListItData(Iterator, struct TestData));

		if (Ref)
		{
			// Print the structure
			PrintTestData(Ref);
		}
	}

	printf("Ref List\n");

	// Print reference list
	ListForEach(DataByRefList, Iterator)
	{
		// Obtain the reference to the copy
		struct TestData * Ref = ListItData(Iterator, struct TestData *);

		if (Ref)
		{
			// Print the structure
			PrintTestData(Ref);
		}
	}

	// Output:
	//
	// ...

	// Destroy lists
	ListDestroy(DataByCopyList);
	ListDestroy(DataByRefList);

    return 0;
}
