/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/Color.h>

////////////////////////////////////////////////////////////
// Internal macros
////////////////////////////////////////////////////////////
#define ByteToFloat(Value)	( ((float)(Value)) / 256.0f )
#define FloatToByte(Value)	( (uint8)((Value) * 255.0f) )

////////////////////////////////////////////////////////////
// Internal data
////////////////////////////////////////////////////////////
struct Color4ub Color4ubGetInstance = { 0x00, 0x00, 0x00, 0x00 };
struct Color4f	Color4fGetInstance	= { 0.0f, 0.0f, 0.0f, 0.0f };

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

void Color4ubCopy(struct Color4ub * Dest, struct Color4ub * Source)
{
	if (Dest && Source)
	{
		Dest->a = Source->a;
		Dest->r = Source->r;
		Dest->g = Source->g;
		Dest->b = Source->b;
	}
}

void Color4ubSet(struct Color4ub * Color, uint8 r, uint8 g, uint8 b, uint8 a)
{
	if (Color)
	{
		Color->a = a;
		Color->r = r;
		Color->g = g;
		Color->b = b;
	}
}

struct Color4ub * Color4ubGet(uint8 r, uint8 g, uint8 b, uint8 a)
{
	Color4ubSet(&Color4ubGetInstance, r, g, b, a);
	
	return &Color4ubGetInstance;
}

uint32 Color4ubPack(struct Color4ub * Color)
{
	return (Color->a << 24) | (Color->b << 16) | (Color->g << 8) | (Color->r << 0);
}

void Color4ubToFloat(struct Color4ub * Color, struct Color4f * Result)
{
	Color4fSet(Result, ByteToFloat(Color->r), ByteToFloat(Color->g), ByteToFloat(Color->b), ByteToFloat(Color->a));
}

void Color4fCopy(struct Color4f * Dest, struct Color4f * Source)
{
	if (Dest && Source)
	{
		Dest->a = Source->a;
		Dest->r = Source->r;
		Dest->g = Source->g;
		Dest->b = Source->b;
	}
}

void Color4fSet(struct Color4f * Color, float r, float g, float b, float a)
{
	if (Color)
	{
		Color->a = a;
		Color->r = r;
		Color->g = g;
		Color->b = b;
	}
}

struct Color4f * Color4fGet(float r, float g, float b, float a)
{
	Color4fSet(&Color4fGetInstance, r, g, b, a);
	
	return &Color4fGetInstance;
}

uint32 Color4fPack(struct Color4f * Color)
{
	return ((uint8)(Color->a * 255.0f) << 24) |
		   ((uint8)(Color->b * 255.0f) << 16) |
		   ((uint8)(Color->g * 255.0f) << 8)  |
		   ((uint8)(Color->r * 255.0f) << 0);
}

void Color4fToByte(struct Color4f * Color, struct Color4ub * Result)
{
	Color4ubSet(Result, FloatToByte(Color->r), FloatToByte(Color->g), FloatToByte(Color->b), FloatToByte(Color->a));
}