/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Memory/General.h>
#include <Graphics/Fog.h>
#include <Graphics/Device.h>

/*
#ifdef RENDER_SYSTEM_OPENGL
#	if PIPELINE_SYSTEM_TYPE == RENDER_PIPELINE_FIXED
		uint32 glFogMode[] = { GL_EXP, GL_EXP2, GL_LINEAR };
#	else

#	endif
#else
#	error Not fog implementation for this render system
#endif
*/

struct FogType * FogCreate(uint32 Mode, struct Color4f * Color, float Density, float Near, float Far)
{
	struct FogType * Fog = (struct FogType*)MemoryAllocate(sizeof(struct FogType));

	if (Fog == NULL)
		return NULL;

	FogSet(Fog, Mode, Color, Density, Near, Far);

	return Fog;
}

void FogDestroy(struct FogType * Fog)
{
	if (Fog != NULL)
	{
		MemoryDeallocate(Fog);
	}
}

void FogSet(struct FogType * Fog, uint32 Type, struct Color4f * Color, float Density, float Near, float Far)
{
	Fog->Mode		= Type;
	Fog->Density	= Density;
	Fog->Near		= Near;
	Fog->Far		= Far;
	Color4fCopy(&Fog->Color, Color);
}

void FogEnable(struct FogType * Fog)
{
#ifdef RENDER_SYSTEM_OPENGL
#	if PIPELINE_SYSTEM_TYPE == RENDER_PIPELINE_FIXED
		glEnable(GL_FOG);
		glFogi(GL_FOG_MODE, glFogMode[Fog->Mode]);
		glFogfv(GL_FOG_COLOR, (const GLfloat*)&Fog->Color);
		glFogf(GL_FOG_DENSITY, Fog->Density);
		glHint(GL_FOG_HINT, GL_DONT_CARE);
		glFogf(GL_FOG_START, Fog->Near);
		glFogf(GL_FOG_END, Fog->Far);
#	else

#	endif
#endif
}

void FogDisable(struct FogType * Fog)
{
#ifdef RENDER_SYSTEM_OPENGL
#	if PIPELINE_SYSTEM_TYPE == RENDER_PIPELINE_FIXED
		glDisable(GL_FOG);
#	else

#	endif
#endif
}
