/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Memory/General.h>
#include <System/IOHelper.h>

#include <Graphics/Font.h>
#include <Graphics/TextureRender.h>

#include <GUI/Widget.h>

////////////////////////////////////////////////////////////
/// Create a console
////////////////////////////////////////////////////////////
struct GuiWidget * GuiConsoleCreate(int32 X, int32 Y, uint32 Width, uint32 Height, uint32 FontSize)
{
	struct GuiWidget * Widget;

	// Create
	Widget = GuiWidgetCreate(GUI_WIDGET_CONSOLE, X, Y, FontSize, NULL);

	if (Widget)
	{
		// Set the rect
		uint32 LineHeight = (Widget->FontSize + 2);
		uint32 ExtraFontSize = Width % FontSize;
		uint32 ExtraLineSize = Height % LineHeight;

		if (ExtraFontSize > 0)
		{
			Width += (FontSize - ExtraFontSize);
			Widget->Length = (Width / FontSize) + 1;
		}
		else
		{
			Widget->Length = (Width / FontSize);
		}

		if (ExtraLineSize > 0)
		{
			Height += (LineHeight - ExtraLineSize);
		}

		Widget->Width = Width;
		Widget->Height = Height;

		// Set the attributtes
		QueueFixedClear(Widget->Attributes.Console.Queue);
		Widget->Attributes.Console.LineSize = Widget->Height / LineHeight;
		Widget->Attributes.Console.LineCount = Widget->Attributes.Console.LineStart = 0;

		// Check for overflow
		if (Widget->Length > GUI_CONSOLE_LINE_LENGTH)
		{
			Widget->Length = GUI_CONSOLE_LINE_LENGTH;
			Widget->Width = GUI_CONSOLE_LINE_LENGTH * FontSize;
		}

		if (Widget->Attributes.Console.LineSize > GUI_CONSOLE_LINE_SIZE)
		{
			Widget->Attributes.Console.LineSize = GUI_CONSOLE_LINE_SIZE;
			Widget->Height = GUI_CONSOLE_LINE_SIZE * LineHeight;
		}

		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Create a console by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiConsoleCreateDefault()
{
	// TODO: Remove this, temporal patch
	return GuiConsoleCreate(0, 0, 425, 149, 8);

	/*
	// Create
	struct GuiWidget * Widget = GuiWidgetCreateDefault(GUI_WIDGET_CONSOLE);

	if (Widget)
	{
		// Set the rect
		Widget->Width = 300;
		Widget->Height = 120;

		// Set the attributts
		QueueFixedClear(Widget->Attributes.Console.Queue);
		Widget->Attributes.Console.LineSize = 0;
		Widget->Attributes.Console.LineCount = 0;
		Widget->Attributes.Console.LineStart = 0;

		return Widget;
	}

	return NULL;
	*/
}

////////////////////////////////////////////////////////////
/// Add a line of text
////////////////////////////////////////////////////////////
void GuiConsoleAddLine(struct GuiWidget * Widget, char * Line)
{
	struct GuiConsole * Console = &Widget->Attributes.Console;
	
	// If full consume old lines
	if (QueueFixedFull(Console->Queue, GUI_CONSOLE_LINE_SIZE))
	{
		QueueFixedPop(Console->Queue, GUI_CONSOLE_LINE_SIZE);
	}

	// Push into the circular buffer
	QueueFixedPush(Console->Queue, Line, ConsoleLineType, GUI_CONSOLE_LINE_SIZE);
	
	// Increment visible line counter
	if (Console->LineCount < Console->LineSize)
	{
		++Console->LineCount;
	}
	else
	{
		++Console->LineStart;
	}
}

////////////////////////////////////////////////////////////
/// Scroll text of the console
////////////////////////////////////////////////////////////
void GuiConsoleScroll(struct GuiWidget * Widget, int32 Position)
{
	struct GuiConsole * Console = &Widget->Attributes.Console;

	if (Position < 0)
	{
		// Position is a negative number
		Console->LineStart += Position;

		if (Console->LineStart < 0)
		{
			Console->LineStart = GUI_CONSOLE_LINE_SIZE + Position - 1;
		}
	}
	else
	{
		// Position is a positive number
		Console->LineStart += Position;

		if (Console->LineStart > GUI_CONSOLE_LINE_SIZE)
		{
			Console->LineStart %= GUI_CONSOLE_LINE_SIZE;
		}
	}
}

////////////////////////////////////////////////////////////
/// Render a console widget
////////////////////////////////////////////////////////////
void GuiConsoleRender(struct GuiWidget * Form, struct GuiWidget * Widget, struct Color4ub * BackgroundColor, struct Color4ub * FontColor)
{
	struct GuiConsole * Console = &Widget->Attributes.Console;
	uint32 Count;
	void * Iterator;

	// Render the background repeating it
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_BACKGROUND,
								Form->X + Widget->X, Form->Y + Widget->Y,
								Form->X + Widget->X + Widget->Width, Form->Y + Widget->Y + Widget->Height,
								BackgroundColor);

	// Render the borders repeating it
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_VBORDER,
								Form->X + Widget->X - IndexGetGrh(GUI_TEXTBOX_VBORDER)->PixelWidth, Form->Y + Widget->Y,
								Form->X + Widget->X, Form->Y + Widget->Y + Widget->Height,
								BackgroundColor);
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_VBORDER + 0x01,
								Form->X + Widget->X + Widget->Width, Form->Y + Widget->Y,
								Form->X + Widget->X + Widget->Width + IndexGetGrh(GUI_TEXTBOX_VBORDER + 0x01)->PixelWidth, Form->Y + Widget->Y + Widget->Height,
								BackgroundColor);
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_HBORDER,
								Form->X + Widget->X, Form->Y + Widget->Y - IndexGetGrh(GUI_TEXTBOX_HBORDER)->PixelHeight,
								Form->X + Widget->X + Widget->Width, Form->Y + Widget->Y,
								BackgroundColor);
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_HBORDER + 0x01,
								Form->X + Widget->X, Form->Y + Widget->Y + Widget->Height,
								Form->X + Widget->X + Widget->Width, Form->Y + Widget->Y + Widget->Height + IndexGetGrh(GUI_TEXTBOX_HBORDER + 0x01)->PixelHeight,
								BackgroundColor);

	// Render the edges
	TextureRenderGrhColor(GUI_TEXTBOX_EDGE, Form->X + Widget->X - IndexGetGrh(GUI_TEXTBOX_EDGE)->PixelWidth, Form->Y + Widget->Y - IndexGetGrh(GUI_TEXTBOX_EDGE)->PixelHeight, BackgroundColor);
	TextureRenderGrhColor(GUI_TEXTBOX_EDGE + 0x01, Form->X + Widget->X + Widget->Width, Form->Y + Widget->Y - IndexGetGrh(GUI_TEXTBOX_EDGE + 0x01)->PixelHeight, BackgroundColor);
	TextureRenderGrhColor(GUI_TEXTBOX_EDGE + 0x02, Form->X + Widget->X - IndexGetGrh(GUI_TEXTBOX_EDGE + 0x02)->PixelWidth, Form->Y + Widget->Y + Widget->Height, BackgroundColor);
	TextureRenderGrhColor(GUI_TEXTBOX_EDGE + 0x03, Form->X + Widget->X + Widget->Width, Form->Y + Widget->Y + Widget->Height, BackgroundColor);

	// Find first line
	for (Count = 0, Iterator = QueueFixedGetHead(Console->Queue, ConsoleLineType);
		 Count < Console->LineStart;
		 ++Count, QueueFixedIteratorNext(Console->Queue, Iterator, ConsoleLineType, GUI_CONSOLE_LINE_SIZE));

	// Render text
	for (Count = 0;
		 Count < Console->LineCount && Iterator != QueueFixedGetTail(Console->Queue, ConsoleLineType, GUI_CONSOLE_LINE_SIZE);
		 ++Count, QueueFixedIteratorNext(Console->Queue, Iterator, ConsoleLineType, GUI_CONSOLE_LINE_SIZE))
	{
		FontRenderTextColor((float)(Widget->X + Form->X + 1),
							(float)(Widget->Y + Form->Y + 3) + (Count * (Widget->FontSize + 2)),
							(char*)Iterator,
							FontColor);
	}
}
