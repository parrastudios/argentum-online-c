/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Application.h>

#include <Network/Protocol.h>		///< Network includes (must be first because of Win32)

#include <System/Platform.h>		///< Platform SO includes
#include <System/Thread.h>
#include <System/Mutex.h>
#include <System/CPUInfo.h>

#include <Math/General.h>			///< Math includes

#include <Game/User.h>				///< Game includes

#include <World/SceneManager.h>		///< World includes

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
Mutex	AppMutex;		///< Application mutex
bool	AppRun;			///< Application switch

////////////////////////////////////////////////////////////
/// Initialize the application
////////////////////////////////////////////////////////////
bool ApplicationInitialize()
{
	// Initialize mutex
	MutexInitialize(&AppMutex);

	// Start it
	AppSetRun(false);

	// Initialize math library
	MathInitialize();

	// Platform Initialize
	#if PLATFORM_TYPE == PLATFORM_MACOS
		InitializeWorkingDirectory(); // Specific in MacOS
	#endif
		
	// Get the CPU features
	CPUInitialize();

	// Initialize database
	if (!DataBaseInitialize())
		return false;

	// Initialize timer manager (1ms of interval by default)
	if (!TimerManagerInitialize(1.0f))
		return false;

	// Initialize users
	if (!UsersInitialize())
		return false;

	// Initialize protocol
	if (!ProtocolInitialize(NETWORK_CONNECTION_PORT))
		return false;

	// Initialize console
	if (!ConsoleInitialize())
		return false;

	// Initialize players
	PlayerManagerInitialize();

	// World initialization block
	{
		// Create entity system
		EntitySystemInitialize();

		// Initialize scene
		if (!SceneManagerCreate(0))
			return false;
	}

	// Run it
	AppSetRun(true);

	return true;
}

////////////////////////////////////////////////////////////
/// Run the application
////////////////////////////////////////////////////////////
void ApplicationRun()
{
	while (AppRun)
	{

		#if (COMPILE_TYPE == DEBUG_MODE)
			SystemSleep(10.0f);
		#endif
	}
}

////////////////////////////////////////////////////////////
/// Destroy the application
////////////////////////////////////////////////////////////
bool ApplicationDestroy()
{
	// World destruction block
	{
		// Destroy scene
		SceneManagerDestroy(0);

		// ...

		// Destroy entity system
		EntitySystemDestroy();
	}

	// Destroy players
	PlayerManagerDestroy();

	// Destroy console
	ConsoleDestroy();

	// Destroy protocol
	ProtocolDestroy();

	// Destroy users
	if (!UsersDestroy())
		return false;

	// Destroy timer manager
	TimerManagerDestroy();

	// Destroy database
	if (!DataBaseDestroy())
		return false;

	// Destroy cpu queries
	CPUDestroy();

	// Destroy mutex
	MutexDestroy(&AppMutex);

	return true;
}

////////////////////////////////////////////////////////////
/// Set the state of the application
////////////////////////////////////////////////////////////
void AppSetRun(bool value)
{
	MutexLock(&AppMutex);

	AppRun = value;

	MutexUnlock(&AppMutex);
}
