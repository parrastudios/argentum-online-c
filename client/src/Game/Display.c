/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Display.h>
#include <Graphics/Font.h>
#include <System/Timer.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define ELAPSED_TIME_UPDATE_INTERVAL	300.0f
#define DISPLAY_TEXT_LENGTH             64

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct Timer	TimerData;			///< Timer
uint32			FrameCounter;		///< Frames per second counter
char			DisplayText[DISPLAY_TEXT_LENGTH];	///< Char pointer to display text

////////////////////////////////////////////////////////////
/// Initialize user display info
////////////////////////////////////////////////////////////
void DisplayInitialize()
{
	// Reset timer
	TimerReset(&TimerData);

	FrameCounter = 0;

	DisplayText[0] = NULLCHAR;
}

////////////////////////////////////////////////////////////
/// Render user display info
////////////////////////////////////////////////////////////
void DisplayUpdate()
{
	float ElapsedTime = TimerGetTime(&TimerData);

	// Update
	FrameCounter++;

	if (ElapsedTime > ELAPSED_TIME_UPDATE_INTERVAL)
	{
	    float FramesPerSecond = (((float)FrameCounter) * 1000.0f) / ElapsedTime;

		// Format string
		sprintf(DisplayText, "FPS: %6.8f\nElapsed Time: %6.4f", FramesPerSecond, ElapsedTime);

		// Reset frame counter
		FrameCounter = 0;

		// Restart timer
		TimerReset(&TimerData);
	}

	// Render
	FontRenderText(5, 5, DisplayText);
}
