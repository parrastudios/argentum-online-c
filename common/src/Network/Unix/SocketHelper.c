/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Network/SocketHelper.h>
#include <errno.h>
#include <fcntl.h>


////////////////////////////////////////////////////////////
/// In Unix this procediment not performs
////////////////////////////////////////////////////////////
void SocketHelperInitialize()
{
	// Nothing..
}

////////////////////////////////////////////////////////////
/// Clean...
////////////////////////////////////////////////////////////
void SocketHelperCleanup()
{
	// Nothing..
}

////////////////////////////////////////////////////////////
/// Return the value of the invalid socket
////////////////////////////////////////////////////////////
SocketType InvalidSocket()
{
    return -1;
}


////////////////////////////////////////////////////////////
/// Close / destroy a socket
////////////////////////////////////////////////////////////
bool SocketHelperClose(SocketType Socket)
{
    return (close(Socket) != -1);
}


////////////////////////////////////////////////////////////
/// Set a socket as blocking or non-blocking
////////////////////////////////////////////////////////////
void SocketHelperSetBlocking(SocketType Socket, bool Block)
{
    int32 Status = fcntl(Socket, F_GETFL);

    if (Block)
        fcntl(Socket, F_SETFL, Status & ~O_NONBLOCK);
    else
        fcntl(Socket, F_SETFL, Status | O_NONBLOCK);
}


////////////////////////////////////////////////////////////
/// Get the last socket error status
////////////////////////////////////////////////////////////
enum SocketStatus SocketHelperGetErrorStatus()
{
    // The followings are sometimes equal to EWOULDBLOCK,
    // so we have to make a special case for them in order
    // to avoid having double values in the switch case
    if ((errno == EAGAIN) || (errno == EINPROGRESS))
        return NotReady;

    switch (errno)
    {
        case EWOULDBLOCK :  return NotReady;
        case ECONNABORTED : return Disconnected;
        case ECONNRESET :   return Disconnected;
        case ETIMEDOUT :    return Disconnected;
        case ENETRESET :    return Disconnected;
        case ENOTCONN :     return Disconnected;
        default :           return Error;
    }
}
