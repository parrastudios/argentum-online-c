/****************************************************************************

  IqeB_GUI_ToolsDispColors.cpp

  05.02.2015 RR: First editon of this file.


*****************************************************************************
*/

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Box.H>
#include <FL/fl_show_colormap.H>

/************************************************************************************
 * Statics
 */

static  Fl_Window *pMyToolWin;
static int MyWinPosX = IQE_GUI_NO_WINPOS_X, MyWinPosY = IQE_GUI_NO_WINPOS_Y; // last window position

/************************************************************************************
 * IqeB_GUI_CBox_SetValue_Callback
 */

static void IqeB_GUI_But_SetValue_Callback( Fl_Widget *w, void *v)
{
  Fl_Button *b = (Fl_Button*)w;
  unsigned int *pColor = (unsigned int *)v;

  // ...

  *pColor = fl_show_colormap( *pColor);
  b->color( *pColor);
  b->parent()->redraw();

}

/************************************************************************************
 * close_cb, close this window
 *
 * pValueArg is a pointer to the widget. Set this pointer to NULL on deletion.
 */

static void close_cb( Fl_Widget *w, void *pValueArg)
{

  IqeB_GUI_CloseToolWindow( (void **)&pMyToolWin);
}

/************************************************************************************
 * Presets for this tools window
 *
 */

static T_GUI_PreferenceEntry MyPreferences[] =
 {

  // OpenGL view clors

  { PREF_T_INT,   "DispCol_BACKGROUND",   " 97", /*  */               &IqeB_DispCol_BACKGROUND},
  { PREF_T_INT,   "DispCol_VERSION",      " 88", /* FL_RED */         &IqeB_DispCol_VERSION},
  { PREF_T_INT,   "DispCol_INFOS",        "255", /* FL_WHITE */       &IqeB_DispCol_INFOS},
  { PREF_T_INT,   "DispCol_NORMALS",      "248", /* FL_MAGENTA */     &IqeB_DispCol_NORMALS},

  { PREF_T_INT,   "DispCol_GROUND_PLANE", " 18", /* FL_DARK1 */       &IqeB_DispCol_GROUND_PLANE},
  { PREF_T_INT,   "DispCol_AXIS",         "223", /* FL_CYAN */        &IqeB_DispCol_AXIS},
  { PREF_T_INT,   "DispCol_BOX",          " 63", /* FL_GREEN */       &IqeB_DispCol_BOX},
  { PREF_T_INT,   "DispCol_BOX_GROUND",   " 59", /*  */               &IqeB_DispCol_BOX_GROUND},

  { PREF_T_INT,   "DispCol_SKELETON",     " 95", /* FL_YELLOW */      &IqeB_DispCol_SKELETON},
  { PREF_T_INT,   "DispCol_JOINTS",       " 95", /* FL_YELLOW */      &IqeB_DispCol_JOINTS},
  { PREF_T_INT,   "DispCol_JOINTS_SEL",   " 88", /* FL_RED */         &IqeB_DispCol_JOINTS_SEL},
  { PREF_T_INT,   "DispCol_JOINT_NAMES",  " 91", /*   */              &IqeB_DispCol_JOINT_NAMES}

};

// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "ToolsDispColors", MyPreferences, sizeof( MyPreferences) / sizeof( T_GUI_PreferenceEntry),
                                               (void **)(&pMyToolWin), &MyWinPosX, &MyWinPosY);

/************************************************************************************
 * IqeB_GUI_ToolsDisplayWin
 *
 * Open dialog
 */

void IqeB_GUI_ToolsDispColorsWin( int xLeft, int xRight, int yTop, int yBotton)
{

  //
  // winodow already created --> show it
  //

  if( pMyToolWin != NULL) {         // already have tool window

    // Show invisble window or bring visible window to foreground

    pMyToolWin->show();          // show it

    return;
  }

  //
  // creation of window on first call
  //

  if( pMyToolWin == NULL) {  // no tool window until now

    int xPos, yPos;

    xPos = xRight;
    yPos = yTop;

    if( MyWinPosX != IQE_GUI_NO_WINPOS_X && MyWinPosY != IQE_GUI_NO_WINPOS_Y) { // have last window position

      xPos = MyWinPosX;
      yPos = MyWinPosY;
    }

    pMyToolWin = new Fl_Window( xPos, yPos, IQE_GUI_TOOLS_STD_WITDH, 400, "Display/Colors");

    if( pMyToolWin == NULL) {  // security test

      return;
    }
  }

  //
  //  GUI things
  //

  int x1, x2, xx2, y1, y, yy;

  Fl_Button *pTempButton;
  Fl_Box *pFrame_Box;
  Fl_Box *pTemp_Box;

  x1  = 8;
  x2  = (2 * pMyToolWin->w()) / 3;
  xx2 = pMyToolWin->w() - x2 - 14;
  yy  = 22;

  y1 = 8;
  y = y1;

  // frame it

  pFrame_Box = new Fl_Box( x1 - 4, y1 - 4, pMyToolWin->w() - 8, pMyToolWin->h() - 8, "");
  pFrame_Box->align( FL_ALIGN_TOP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
  pFrame_Box->box( FL_DOWN_FRAME);

  // Colors

  y += 4;

  pTemp_Box = new Fl_Box( x2, y, 1, yy - 4, "Background");
  pTemp_Box->align( FL_ALIGN_LEFT);
  pTemp_Box->box( FL_NO_BOX);

  pTempButton = new Fl_Button( x2 + 4, y, xx2, yy - 4, "");
  pTempButton->color( IqeB_DispCol_BACKGROUND);
  pTempButton->callback( IqeB_GUI_But_SetValue_Callback, &IqeB_DispCol_BACKGROUND);

  y += yy;

  pTemp_Box = new Fl_Box( x2, y, 1, yy - 4, "Version text");
  pTemp_Box->align( FL_ALIGN_LEFT);
  pTemp_Box->box( FL_NO_BOX);

  pTempButton = new Fl_Button( x2 + 4, y, xx2, yy - 4, "");
  pTempButton->color( IqeB_DispCol_VERSION);
  pTempButton->callback( IqeB_GUI_But_SetValue_Callback, &IqeB_DispCol_VERSION);

  y += yy;

  pTemp_Box = new Fl_Box( x2, y, 1, yy - 4, "Other text output");
  pTemp_Box->align( FL_ALIGN_LEFT);
  pTemp_Box->box( FL_NO_BOX);

  pTempButton = new Fl_Button( x2 + 4, y, xx2, yy - 4, "");
  pTempButton->color( IqeB_DispCol_INFOS);
  pTempButton->callback( IqeB_GUI_But_SetValue_Callback, &IqeB_DispCol_INFOS);

  y += yy;

  pTemp_Box = new Fl_Box( x2, y, 1, yy - 4, "Normals");
  pTemp_Box->align( FL_ALIGN_LEFT);
  pTemp_Box->box( FL_NO_BOX);

  pTempButton = new Fl_Button( x2 + 4, y, xx2, yy - 4, "");
  pTempButton->color( IqeB_DispCol_NORMALS);
  pTempButton->callback( IqeB_GUI_But_SetValue_Callback, &IqeB_DispCol_NORMALS);

  y += yy;
  y += 8;

  pTemp_Box = new Fl_Box( x2, y, 1, yy - 4, "Ground grid");
  pTemp_Box->align( FL_ALIGN_LEFT);
  pTemp_Box->box( FL_NO_BOX);

  pTempButton = new Fl_Button( x2 + 4, y, xx2, yy - 4, "");
  pTempButton->color( IqeB_DispCol_GROUND_PLANE);
  pTempButton->callback( IqeB_GUI_But_SetValue_Callback, &IqeB_DispCol_GROUND_PLANE);

  y += yy;

  pTemp_Box = new Fl_Box( x2, y, 1, yy - 4, "Axis");
  pTemp_Box->align( FL_ALIGN_LEFT);
  pTemp_Box->box( FL_NO_BOX);

  pTempButton = new Fl_Button( x2 + 4, y, xx2, yy - 4, "");
  pTempButton->color( IqeB_DispCol_AXIS);
  pTempButton->callback( IqeB_GUI_But_SetValue_Callback, &IqeB_DispCol_AXIS);

  y += yy;

  pTemp_Box = new Fl_Box( x2, y, 1, yy - 4, "Box");
  pTemp_Box->align( FL_ALIGN_LEFT);
  pTemp_Box->box( FL_NO_BOX);

  pTempButton = new Fl_Button( x2 + 4, y, xx2, yy - 4, "");
  pTempButton->color( IqeB_DispCol_BOX);
  pTempButton->callback( IqeB_GUI_But_SetValue_Callback, &IqeB_DispCol_BOX);

  y += yy;

  pTemp_Box = new Fl_Box( x2, y, 1, yy - 4, "Ground level of box");
  pTemp_Box->align( FL_ALIGN_LEFT);
  pTemp_Box->box( FL_NO_BOX);

  pTempButton = new Fl_Button( x2 + 4, y, xx2, yy - 4, "");
  pTempButton->color( IqeB_DispCol_BOX_GROUND);
  pTempButton->callback( IqeB_GUI_But_SetValue_Callback, &IqeB_DispCol_BOX_GROUND);

  y += yy;
  y += 8;

  pTemp_Box = new Fl_Box( x2, y, 1, yy - 4, "Skeleton");
  pTemp_Box->align( FL_ALIGN_LEFT);
  pTemp_Box->box( FL_NO_BOX);

  pTempButton = new Fl_Button( x2 + 4, y, xx2, yy - 4, "");
  pTempButton->color( IqeB_DispCol_SKELETON);
  pTempButton->callback( IqeB_GUI_But_SetValue_Callback, &IqeB_DispCol_SKELETON);

  y += yy;

  pTemp_Box = new Fl_Box( x2, y, 1, yy - 4, "Joints");
  pTemp_Box->align( FL_ALIGN_LEFT);
  pTemp_Box->box( FL_NO_BOX);

  pTempButton = new Fl_Button( x2 + 4, y, xx2, yy - 4, "");
  pTempButton->color( IqeB_DispCol_JOINTS);
  pTempButton->callback( IqeB_GUI_But_SetValue_Callback, &IqeB_DispCol_JOINTS);

  y += yy;

  pTemp_Box = new Fl_Box( x2, y, 1, yy - 4, "Selected joint");
  pTemp_Box->align( FL_ALIGN_LEFT);
  pTemp_Box->box( FL_NO_BOX);

  pTempButton = new Fl_Button( x2 + 4, y, xx2, yy - 4, "");
  pTempButton->color( IqeB_DispCol_JOINTS_SEL);
  pTempButton->callback( IqeB_GUI_But_SetValue_Callback, &IqeB_DispCol_JOINTS_SEL);

  y += yy;

  pTemp_Box = new Fl_Box( x2, y, 1, yy - 4, "Joint names");
  pTemp_Box->align( FL_ALIGN_LEFT);
  pTemp_Box->box( FL_NO_BOX);

  pTempButton = new Fl_Button( x2 + 4, y, xx2, yy - 4, "");
  pTempButton->color( IqeB_DispCol_JOINT_NAMES);
  pTempButton->callback( IqeB_GUI_But_SetValue_Callback, &IqeB_DispCol_JOINT_NAMES);

  y += yy;

  // update frame height

  pFrame_Box->size( pFrame_Box->w(), y - y1 + 6);

  // finish up

  pMyToolWin->size( pMyToolWin->w(), pFrame_Box->y() + pFrame_Box->h() + 4);  // final size of window

  pMyToolWin->end();
  pMyToolWin->set_non_modal();
  pMyToolWin->callback( close_cb, &pMyToolWin);
  pMyToolWin->show();
}

/********************************** End Of File **********************************/
