/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_MODEL_LOADER_H
#define WORLD_MODEL_LOADER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/Vector.h>
#include <World/Entity.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct ModelResourceType;

struct ModelLoadCallbackType;

struct ModelLoadBuildType;

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
//typedef struct ModelResourceType * ModelResource;					//< Opaque model resource handler type definition

typedef struct ModelLoadCallbackType * ModelLoadCallback;			//< On load callback block type (promise)

typedef bool (*ModelLoadCallbackFunc)(ModelLoadCallback);			//< On load callback function type (callback)

typedef struct ModelLoadBuildType * ModelLoadBuild;					//< Builder pattern for register callbacks

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct ModelMeshType
{
	uinteger IndexSize;
	uinteger IndexOffset;
	uinteger VertexSize;
	uinteger VertexOffset;
	uinteger Material;
};

struct ModelJointType
{
	integer Parent;
	struct Vector3f Position;
	struct Quaternion Rotation;
	struct Vector3f Scale;
};

struct ModelResourceType
{
	const char * Name;										//< Resource name
	bool Animated;											//< Check if the model is animated
	Vector Meshes;											//< Mesh list <T = ModelMeshType>
	Vector Triangles;										//< Triangle list <T = Vector3ui>
	Vector Positions;										//< Vertex positions <T = Vector3f>
	Vector Normals;											//< Vertex normals <T = Vector3f>
	Vector Tangents;										//< Vertex tangents <T = Vector4f>
	Vector TextureCoords;									//< Texture coordinates <T = Vector2f>
	Vector BlendIndexes;									//< Blend indexes <T = Vector4ub>
	Vector BlendWeights;									//< Blend weights <T = Vector4ub>
	Vector Colors;											//< Colors <T = Color4ub>
	Vector Textures;										//< Texture list <T = uint8 *>
	Vector Joints;											//< Skeleton joints <T = ModelJointType>
	Vector BaseFrame;										//< Base frame <T = Matrix34f>
	Vector InverseBaseFrame;								//< Inverse base frame <T = Matrix34f>
	Vector Frames;											//< Frame list <T = Matrix34f>
	uinteger FrameSize;										//< Size of frame list
	uinteger PoseSize;										//< Size of poses for each frame
};

struct ModelLoadCallbackType
{
	struct ModelResourceType * Resource;					//< Resource handle
	ModelLoadCallbackFunc Func;								//< Callback pointer to itself
	EntityIdType EntityId;									//< Id of the entity
};

struct ModelLoadBuildType
{
	ModelLoadBuild (*Build)(ModelLoadCallbackFunc);			//< Build callback
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Register on load callback
////////////////////////////////////////////////////////////
ModelLoadBuild ModelLoadRegisterCallback(ModelLoadCallbackFunc Callback);

////////////////////////////////////////////////////////////
/// Unregister on load callback
////////////////////////////////////////////////////////////
void ModelLoadUnregisterCallback(ModelLoadCallbackFunc Callback);

////////////////////////////////////////////////////////////
/// Load model from file
////////////////////////////////////////////////////////////
bool ModelLoadFromFile(const char * Path, const char * Name, EntityIdType EntityId);

#endif // WORLD_MODEL_LOADER_H
