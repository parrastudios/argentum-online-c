/*
***********************************************************************************
 IqeB_Import_FileIQM.cpp

 Import a .iqm file.

14.12.2014 RR: * First editon of this file.

*/

#include "IqeBrowser.h"
#include "iqm.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

/************************************************************************************
 * IqeB_ImportFileIQM()
 *
 * return:   NULL  error on loading
 *           else  pointer to loaded model data
 */

struct IQE_model *IqeB_ImportFileIQM( char *filename)
{
  char basedir[ 1024];
	FILE *fp = NULL;
	char *p;
  unsigned char *buf = NULL;
  iqmheader hdr;
	struct skel *skel = NULL;
	struct mesh *mesh = NULL;
	struct anim *anim = NULL;
	struct IQE_model *model = NULL;
	char *texts;
	iqmvertexarray *vas;

	//x/fprintf( stderr, "loading iqm model '%s'\n", filename);

  IqeB_ImportArraysReset();  // reset temporary arrays used on loading files

  // get base directory (path to filename)

	strcpy(basedir, filename);
	p = strrchr(basedir, '/');
	if (!p) p = strrchr(basedir, '\\');
	if (!p) {
    strcpy(basedir, "");    // empty directory
    p = filename;          // point to begin of file name
  } else {
    *p = 0;          // overwrite slash
    p += 1;          // point to begin of file name
  }

	// try to open file

	fp = fopen( filename, "rb");
	if (!fp) {

		//x/fprintf( stderr, "ERROR: cannot open file '%s'\n", filename);

		return( NULL);
	}

  if( fread( &hdr, 1, sizeof(hdr), fp) != sizeof(hdr)) {
    goto error;
  }
  if( memcmp(hdr.magic, IQM_MAGIC, sizeof(hdr.magic))) {
    goto error;
  }
  hdr.version = LittleLong( hdr.version );
  if(hdr.version != IQM_VERSION) {
      goto error;
  }

	// byteswap header
	hdr.filesize = LittleLong(hdr.filesize);
	hdr.flags = LittleLong(hdr.flags);
	hdr.num_text = LittleLong(hdr.num_text);
	hdr.ofs_text = LittleLong(hdr.ofs_text);
	hdr.num_meshes = LittleLong(hdr.num_meshes);
	hdr.ofs_meshes = LittleLong(hdr.ofs_meshes);
	hdr.num_vertexarrays = LittleLong(hdr.num_vertexarrays);
	hdr.num_vertexes = LittleLong(hdr.num_vertexes);
	hdr.ofs_vertexarrays = LittleLong(hdr.ofs_vertexarrays);
	hdr.num_triangles = LittleLong(hdr.num_triangles);
	hdr.ofs_triangles = LittleLong(hdr.ofs_triangles);
	hdr.ofs_adjacency = LittleLong(hdr.ofs_adjacency);
	hdr.num_joints = LittleLong(hdr.num_joints);
	hdr.ofs_joints = LittleLong(hdr.ofs_joints);
	hdr.num_poses = LittleLong(hdr.num_poses);
	hdr.ofs_poses = LittleLong(hdr.ofs_poses);
	hdr.num_anims = LittleLong(hdr.num_anims);
	hdr.ofs_anims = LittleLong(hdr.ofs_anims);
	hdr.num_frames = LittleLong(hdr.num_frames);
	hdr.num_framechannels = LittleLong(hdr.num_framechannels);
	hdr.ofs_frames = LittleLong(hdr.ofs_frames);
	hdr.ofs_bounds = LittleLong(hdr.ofs_bounds);
	hdr.num_comment = LittleLong(hdr.num_comment);
	hdr.ofs_comment = LittleLong(hdr.ofs_comment);
	hdr.num_extensions = LittleLong(hdr.num_extensions);
	hdr.ofs_extensions = LittleLong(hdr.ofs_extensions);

  if(hdr.filesize > (16<<20)) {
    goto error; // sanity check... don't load files bigger than 16 MB
  }

  buf = (unsigned char *)malloc( hdr.filesize);
  if( fread(buf + sizeof(hdr), 1, hdr.filesize - sizeof(hdr), fp) != hdr.filesize - sizeof(hdr)) {
    goto error;
  }

  // pointer to string data

  texts = hdr.ofs_text ? (char *)&buf[hdr.ofs_text] : (char *)"";

	// allocated an empty model

	model = IqeB_ModelCreateEmpty();    // get empty model
	if( model == NULL) {                                   // memory allocation failed

    return( NULL);
	}

  // for less writing, some short cuts

	skel = model->skel;
	mesh = model->mesh;
	anim = NULL;

  // ...

	skel->joint_count = 0;

	// load the data ...

	unsigned int iLoop;
	float *pInFloat;
	unsigned char *pUByte;
	int *pInInt;

  vas = (iqmvertexarray *)&buf[hdr.ofs_vertexarrays];
  for( int iVarray = 0; iVarray < (int)hdr.num_vertexarrays; iVarray++)
  {
    iqmvertexarray &va = vas[ iVarray];

		va.type = LittleLong( va.type );
  	va.flags = LittleLong( va.flags );
	  va.format = LittleLong( va.format );
	  va.size = LittleLong( va.size );
	  va.offset = LittleLong( va.offset );

    switch(va.type)
    {
    case IQM_POSITION:
      if(va.format != IQM_FLOAT || va.size != 3) {
        goto error;
      }
      pInFloat = (float *)( buf + va.offset);
	    for( iLoop = 0; iLoop < hdr.num_vertexes; iLoop++ ) {
        pInFloat[0] = LittleFloat( pInFloat[0] );
        pInFloat[1] = LittleFloat( pInFloat[1] );
        pInFloat[2] = LittleFloat( pInFloat[2] );
    		addposition( pInFloat[0], pInFloat[1], pInFloat[2]);
	      pInFloat += 3;
	    }
      break;
    case IQM_NORMAL:
      if(va.format != IQM_FLOAT || va.size != 3) {
        goto error;
      }
      pInFloat = (float *)( buf + va.offset);
	    for( iLoop = 0; iLoop < hdr.num_vertexes; iLoop++ ) {
        pInFloat[0] = LittleFloat( pInFloat[0] );
        pInFloat[1] = LittleFloat( pInFloat[1] );
        pInFloat[2] = LittleFloat( pInFloat[2] );
  			addnormal( pInFloat[0], pInFloat[1], pInFloat[2]);
		    pInFloat += 3;
	    }
      break;
    case IQM_TANGENT:
      if(va.format != IQM_FLOAT || va.size != 4) {
        goto error;
      }
      // is not used
      break;
    case IQM_TEXCOORD:
      if(va.format != IQM_FLOAT || va.size != 2) {
        goto error;
      }
      pInFloat = (float *)( buf + va.offset);
	    for( iLoop = 0; iLoop < hdr.num_vertexes; iLoop++ ) {
        pInFloat[0] = LittleFloat( pInFloat[0] );
        pInFloat[1] = LittleFloat( pInFloat[1] );
  	  	addtexcoord( pInFloat[0], pInFloat[1]);
		    pInFloat += 2;
      }
      break;
    case IQM_BLENDINDEXES:
      if(va.format != IQM_UBYTE || va.size != 4) {
        goto error;
      }
      pUByte = (unsigned char *)( buf + va.offset);
	    for( iLoop = 0; iLoop < hdr.num_vertexes; iLoop++ ) {
      	pushint(&IqeB_ImportArray_blendindex, pUByte[0]);
       	pushint(&IqeB_ImportArray_blendindex, pUByte[1]);
       	pushint(&IqeB_ImportArray_blendindex, pUByte[2]);
       	pushint(&IqeB_ImportArray_blendindex, pUByte[3]);
		    pUByte += 4;
	    }
      break;
    case IQM_BLENDWEIGHTS:
      if(va.format != IQM_UBYTE || va.size != 4) {
        goto error;
      }
      pUByte = (unsigned char *)( buf + va.offset);
	    for( iLoop = 0; iLoop < hdr.num_vertexes; iLoop++ ) {

        float total = pUByte[0] + pUByte[1] + pUByte[2] + pUByte[3];

      	pushfloat(&IqeB_ImportArray_blendweight, pUByte[0] / total);
       	pushfloat(&IqeB_ImportArray_blendweight, pUByte[1] / total);
       	pushfloat(&IqeB_ImportArray_blendweight, pUByte[2] / total);
       	pushfloat(&IqeB_ImportArray_blendweight, pUByte[3] / total);
		    pUByte += 4;
	    }
      break;
    case IQM_COLOR:
      if(va.format != IQM_UBYTE || va.size != 4) {
        goto error;
      }
      pUByte = (unsigned char *)( buf + va.offset);
	    for( iLoop = 0; iLoop < hdr.num_vertexes; iLoop++ ) {
  	  	addcolor( pUByte[0] / 255.0, pUByte[1] / 255.0, pUByte[2] / 255.0, pUByte[3] / 255.0);
        pUByte += 4;
      }
      break;
    }
  }

	// load triangles

  pInInt = (int *)( buf + hdr.ofs_triangles);
	for( iLoop = 0; iLoop < hdr.num_triangles; iLoop++ ) {
    pInInt[0] = LittleLong( pInInt[0] );
    pInInt[1] = LittleLong( pInInt[1] );
    pInInt[2] = LittleLong( pInInt[2] );
    addtriangleFlipWinding( pInInt[0], pInInt[1], pInInt[2]);
    pInInt += 3;
  }

	// load meshes

  iqmmesh *inmesh;
  inmesh = (iqmmesh *)( buf + hdr.ofs_meshes);

	for( iLoop = 0; iLoop < hdr.num_meshes; iLoop++ ) {

		inmesh[iLoop].name = LittleLong( inmesh[iLoop].name );
		inmesh[iLoop].material = LittleLong( inmesh[iLoop].material );
		inmesh[iLoop].first_vertex = LittleLong( inmesh[iLoop].first_vertex );
		inmesh[iLoop].num_vertexes = LittleLong( inmesh[iLoop].num_vertexes );
		inmesh[iLoop].first_triangle = LittleLong( inmesh[iLoop].first_triangle );
		inmesh[iLoop].num_triangles = LittleLong( inmesh[iLoop].num_triangles );

		//x/poutmodel->meshes[iLoop].name = texts + inmesh[Loopi].name;
		//x/pSkinNameOriginal = texts + inmesh[iLoop].material;

		pushpart( &IqeB_ImportArray_partbuf, inmesh[iLoop].first_triangle * 3, (inmesh[iLoop].first_triangle + inmesh[iLoop].num_triangles) * 3,
              texts + inmesh[iLoop].name, texts + inmesh[iLoop].material, basedir);

	}

  // load joints (the skeleton)

	iqmjoint *joints;

  joints = (iqmjoint *)( buf + hdr.ofs_joints);

	for( iLoop = 0; iLoop < hdr.num_joints; iLoop++ ) {

    int jLoop;

		joints[iLoop].name = LittleLong( joints[iLoop].name );
		joints[iLoop].parent = LittleLong( joints[iLoop].parent );

		if( joints[iLoop].parent >= (int)iLoop) {
			//x/Com_Error( ERR_DROP, "%s bone[%i].parent(%i) >= %i", mod->name, iLoop, joints[iLoop].parent, iLoop);

			goto error;
		}

		if (skel->joint_count < MAXBONE) {
			skel->j[ skel->joint_count].name   = strdup( texts + joints[iLoop].name);
			skel->j[ skel->joint_count].parent = joints[iLoop].parent;

  		for( jLoop = 0; jLoop < 3; jLoop++ ) {
	  		joints[iLoop].translate[jLoop] = LittleFloat( joints[iLoop].translate[jLoop] );
 			  skel->pose[skel->joint_count].location[jLoop] = joints[iLoop].translate[jLoop];

			  joints[iLoop].scale[jLoop] = LittleFloat( joints[iLoop].scale[jLoop] );
 			  skel->pose[skel->joint_count].scale[jLoop] = joints[iLoop].scale[jLoop];
		  }

  		for( jLoop = 0; jLoop < 4; jLoop++ ) {
		  	joints[iLoop].rotate[jLoop] = LittleFloat( joints[iLoop].rotate[jLoop] );
 			  skel->pose[skel->joint_count].rotation[jLoop] = joints[iLoop].rotate[jLoop];
  		}

			skel->joint_count++;
		}
	}

	// load animations

  unsigned int nFrames;
  struct pose **FrameDataTable;

  FrameDataTable = NULL;
  nFrames = 0;
  if( hdr.num_frames > 0) {

	  FrameDataTable = (struct pose **)malloc( hdr.num_frames * sizeof(*FrameDataTable));
  }

  // ...

	struct iqmanim *anims;

	anims = ( iqmanim * )( buf + hdr.ofs_anims);

	for( iLoop = 0; iLoop < hdr.num_anims; iLoop++ ) {

    unsigned int jLoop;

		anims[iLoop].name = LittleLong( anims[iLoop].name);
		anims[iLoop].first_frame = LittleLong( anims[iLoop].first_frame);
		anims[iLoop].num_frames = LittleLong( anims[iLoop].num_frames);
		anims[iLoop].framerate = LittleFloat( anims[iLoop].framerate);
		anims[iLoop].flags = LittleLong( anims[iLoop].flags);

    anim = pushanim( model, texts + anims[iLoop].name);

    anim->len = anims[iLoop].num_frames;
    anim->framerate = anims[iLoop].framerate;
    if( anim->framerate <= 0.01) {  // security test
      anim->framerate = 10.0;
    }
    anim->looped = (anims[iLoop].flags & IQM_LOOP) != 0;

    // allocate memory for the animations

    if( anims[iLoop].num_frames > 0) {

		  anim->data = (struct pose **)malloc( anims[iLoop].num_frames * sizeof(*anim->data));
    }

    for( jLoop = 0; jLoop < anims[iLoop].num_frames; jLoop++) {

 	    anim->data[ jLoop] = (struct pose *)malloc( sizeof(struct pose) * hdr.num_poses);

      // where to store the loaded frame data
      if( nFrames < hdr.num_frames) {

        FrameDataTable[ nFrames] = anim->data[ jLoop];
        nFrames++;
      }
   }

    // NOTE: loop flag is not read in until know
  }

	// load frames

	struct iqmpose *poses;

	poses = ( iqmpose * )( buf + hdr.ofs_poses);

	for( iLoop = 0; iLoop < hdr.num_poses; iLoop++ ) {

    int jLoop;

		poses[iLoop].parent = LittleLong( poses[iLoop].parent );
		poses[iLoop].mask = LittleLong( poses[iLoop].mask );

		for( jLoop = 0; jLoop < 10; jLoop++ ) {
			poses[iLoop].channeloffset[jLoop] = LittleFloat( poses[iLoop].channeloffset[jLoop] );
			poses[iLoop].channelscale[jLoop] = LittleFloat( poses[iLoop].channelscale[jLoop] );
		}
	}

	unsigned short *framedata;

	framedata = ( unsigned short * )( buf + hdr.ofs_frames);

	for( iLoop = 0; iLoop < hdr.num_frames; iLoop++ ) {
    unsigned int j;
    struct pose *pOutPose, *pbp;


		pOutPose = FrameDataTable[ iLoop];

		for( j = 0, pbp = pOutPose; j < hdr.num_poses; j++, pbp++ ) {

			pbp->location[0] = poses[j].channeloffset[0]; if( poses[j].mask & 0x01 ) pbp->location[0] += *framedata++ * poses[j].channelscale[0];
			pbp->location[1] = poses[j].channeloffset[1]; if( poses[j].mask & 0x02 ) pbp->location[1] += *framedata++ * poses[j].channelscale[1];
			pbp->location[2] = poses[j].channeloffset[2]; if( poses[j].mask & 0x04 ) pbp->location[2] += *framedata++ * poses[j].channelscale[2];

			pbp->rotation[0] = poses[j].channeloffset[3]; if( poses[j].mask & 0x08 ) pbp->rotation[0] += *framedata++ * poses[j].channelscale[3];
			pbp->rotation[1] = poses[j].channeloffset[4]; if( poses[j].mask & 0x10 ) pbp->rotation[1] += *framedata++ * poses[j].channelscale[4];
			pbp->rotation[2] = poses[j].channeloffset[5]; if( poses[j].mask & 0x20 ) pbp->rotation[2] += *framedata++ * poses[j].channelscale[5];
			pbp->rotation[3] = poses[j].channeloffset[6]; if( poses[j].mask & 0x40 ) pbp->rotation[3] += *framedata++ * poses[j].channelscale[6];

			if( pbp->rotation[3] > 0 ) {
				Vector4Inverse( pbp->rotation);
			}
			Vector4Normalize( pbp->rotation);

#ifdef use_again
			// scale is unused
			if( poses[j].mask & 0x80  ) framedata++;
			if( poses[j].mask & 0x100 ) framedata++;
			if( poses[j].mask & 0x200 ) framedata++;
#else
			pbp->scale[0] = poses[j].channeloffset[7]; if( poses[j].mask & 0x080 ) pbp->scale[0] += *framedata++ * poses[j].channelscale[7];
			pbp->scale[1] = poses[j].channeloffset[8]; if( poses[j].mask & 0x100 ) pbp->scale[1] += *framedata++ * poses[j].channelscale[8];
			pbp->scale[2] = poses[j].channeloffset[9]; if( poses[j].mask & 0x200 ) pbp->scale[2] += *framedata++ * poses[j].channelscale[9];
#endif
		}
	}

  // finish

  free( buf);         // free data of model

  fclose( fp);
  fp = NULL;          // flag that fp is closed

  IqeB_ModelSkeletonMatrixCalc( model);          // update skeleton matrix
  IqeB_AnimPrepareAfterLoad( model);             // Prepare animations

	mesh->vertex_count = IqeB_ImportArray_position.len / 3;
	mesh->position = (float *)dupArray(IqeB_ImportArray_position.data, IqeB_ImportArray_position.len, sizeof(float));
	mesh->normal = (float *)dupArray(IqeB_ImportArray_normal.data, IqeB_ImportArray_normal.len, sizeof(float));
	mesh->texcoord = (float *)dupArray(IqeB_ImportArray_texcoord.data, IqeB_ImportArray_texcoord.len, sizeof(float));
	mesh->color = (float *)dupArray(IqeB_ImportArray_color.data, IqeB_ImportArray_color.len, sizeof(float));
	mesh->blendindex = (int *)dupArray(IqeB_ImportArray_blendindex.data, IqeB_ImportArray_blendindex.len, sizeof(int));
	mesh->blendweight = (float *)dupArray(IqeB_ImportArray_blendweight.data, IqeB_ImportArray_blendweight.len, sizeof(float));
	mesh->aposition = NULL;
	mesh->anormal = NULL;

	mesh->element_count = IqeB_ImportArray_element.len;
	mesh->element = (int *)dupArray(IqeB_ImportArray_element.data, IqeB_ImportArray_element.len, sizeof(int));

	mesh->part_count = IqeB_ImportArray_partbuf.len;
	mesh->part = (part *)dupArray(IqeB_ImportArray_partbuf.data, IqeB_ImportArray_partbuf.len, sizeof(struct part));

	//x/fprintf(stderr, "\t%d batches; %d vertices; %d triangles; %d bones\n",
	//x/		mesh->part_count, mesh->vertex_count, mesh->element_count / 3, skel->joint_count);

  //x/fprintf(stderr, "%s: Done loading\n", filename);

	return model;

error:
	//x/fprintf( stderr, "%s: ERROR while loading\n", filename);

  if( buf != NULL) {
    free( buf);
  }
  if( skel != NULL) {
    free( skel);
  }
  if( mesh != NULL) {
    free( mesh);
  }
  if( anim != NULL) {
    free( anim);
  }

  if( fp != NULL) {
    fclose( fp);
  }
  return NULL;
}

/****************************** End Of File ******************************/
