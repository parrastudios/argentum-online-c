// npm install mysql
var mysql = require('mysql');
var options = require('../Options');

/**
 * Main connection
 */
var connection = mysql.createConnection({
  host: 'localhost',
  user: options.username,
  password: options.password,
  database: options.database
});

connection.connect();

module.exports = connection, options;
