/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/List.h>

////////////////////////////////////////////////////////////
// Data structure
////////////////////////////////////////////////////////////
struct ListNodeType
{
    struct ListNodeType *   Prev;
    struct ListNodeType *   Next;
};

typedef struct ListNodeType * ListNode;

struct ListType
{
    uinteger    Tsize;
	uinteger    Size;
	ListNode    Begin;
    ListNode    End;
};

////////////////////////////////////////////////////////////
// Helper macros
////////////////////////////////////////////////////////////
#define ListTypeFatalError(Error) do{ /* ... */ }while(0)
#define ListNodeAlloc(l) ((ListNode) MemoryAllocate(sizeof(struct ListNodeType) + l->Tsize))
#define ListNodeFree(l, ln) MemoryDeallocate(ln)
#define ListNodeData(ln) ((void*)(ln+1))
#define ListItNodeInfo(li) (((ListNode)li)-1)

////////////////////////////////////////////////////////////
// Public functions
////////////////////////////////////////////////////////////

// Returns new List whose objects have size 'TypeSize'
List ListNew(uinteger TypeSize) 
{
    List l = (List)MemoryAllocate(sizeof(struct ListType));

    if(l == NULL) 
    {
        ListTypeFatalError("List allocation error");
        return NULL;
    }

    l->Tsize = TypeSize;
    l->Size = 0;
    l->Begin = NULL;
    l->End = NULL;

    return l;
}

// Free all the memory
//   After that te object is unusable
//   Don't apply the function twice over the same object
void ListDestroy(List l) 
{
    ListNode ln = l->Begin;

    while(ln != NULL) 
    {
        ListNode lnAux = ln;
        ln = ln->Next;
        ListNodeFree(l, lnAux);
    }

    MemoryDeallocate(l);
}

// Deletes all the objects and set size to 0
void ListClear(List l)
{
    ListNode ln = l->Begin;

    while(ln)
    {
        ListNode lnAux = ln;
        ln = ln->Next;
		ListNodeFree(l, lnAux);
    }

    l->Begin = NULL;
    l->End = NULL;
    l->Size = 0;
}

// Returns the amount of object in the list
uinteger ListSize(List l) 
{
    return l->Size;
}

// Returns true if the list if empty (Size is 0)
bool ListEmpty(List l) 
{
    return l->Size == 0;
}

// Returns a pointer to the first object (or NULL if is empty)
void* ListFront(List l) 
{
    if(l->Begin == NULL)
        return NULL;

    return ListNodeData(l->Begin);
}

// Returns a pointer to the last object (or NULL if is empty)
void* ListBack(List l) 
{
    if(l->End == NULL)
        return NULL;

    return ListNodeData(l->End);
}

// Returns a pointer to object (or NULL if the position don't exits)
void* ListAt(List l, uinteger Position) 
{
    if(Position < l->Size)
    {
        ListNode ln = l->Begin;

        while(Position--)
            ln = ln->Next;

        return ListNodeData(ln);
    }
    else
    {
        return NULL;
    }
}

// Sets the object in the that position to the same of the object pointed by 'Element'
void ListSet(List l, uinteger Position, void *Element) 
{
    void * lnData = ListAt(l, Position);

    if(lnData != NULL)
        MemoryCopy(lnData, Element, l->Tsize);
}

// Adds an object to the begin of the list
void ListPushFront(List l, void * Element) 
{
    ListNode ln = ListNodeAlloc(l);

	if (ln)
	{
		MemoryCopy(ListNodeData(ln), Element, l->Tsize);
		ln->Prev = NULL;

		if (l->Size == 0)
		{
			ln->Next = NULL;
			l->Begin = ln;
			l->End = ln;
		}
		else
		{
			ln->Next = l->Begin;
			l->Begin->Prev = ln;
			l->Begin = ln;
		}

		++l->Size;
	}
}

// Deletes the first object of the list
void ListPopFront(List l) 
{
    if(l->Size > 0)
    {
        ListNode ln = l->Begin;

        l->Begin = ln->Next;
		if (l->Begin)
		{
			l->Begin->Prev = NULL;
		}
		else
		{
			l->End = NULL;
		}
        ListNodeFree(l, ln);

        --l->Size;
    }
}

// Adds an object to the end of the list
void ListPushBack(List l, void * Element) 
{
    ListNode ln = ListNodeAlloc(l);

	if (ln)
	{
		MemoryCopy(ListNodeData(ln), Element, l->Tsize);
		ln->Next = NULL;

		if (l->Size == 0)
		{
			ln->Prev = NULL;
			l->Begin = ln;
			l->End = ln;
		}
		else
		{
			ln->Prev = l->End;
			l->End->Next = ln;
			l->End = ln;
		}

		++l->Size;
	}
}

// Deletes the last object of the list
void ListPopBack(List l) 
{
    if(l->Size > 0)
    {
        ListNode ln = l->End;

        l->End = ln->Prev;
		if (l->End)
		{
			l->End->Next = NULL;
		}
		else
		{
			l->Begin = NULL;
		}
        ListNodeFree(l, ln);

        --l->Size;
    }
}

// Adds an object in that position
//   If the position is less than 0, is inserted at begin
//   If the position is greater than the size, is inserted at end
void ListInsert(List l, uinteger Position, void *Element)
{
    if(Position <= 0)
    {
        ListPushFront(l, Element);
    }
    else if(Position >= l->Size)
    {
        ListPushBack(l, Element);
    }
    else
    {
        ListNode ln = l->Begin;
        ListNode lnNew = ListNodeAlloc(l);

		if (lnNew)
		{
			MemoryCopy(ListNodeData(lnNew), Element, l->Tsize);

			while (Position--)
				ln = ln->Next;

			lnNew->Prev = ln->Prev;
			lnNew->Next = ln;
			ln->Prev->Next = lnNew;
			ln->Prev = lnNew;

			++l->Size;
		}
		else
		{
			// Handle error
		}
    }
}

// Removes an object of the list
void ListErase(List l, uinteger Position)
{
    if(Position == 0)
    {
        ListPopFront(l);
    }
    else if(Position + 1 == l->Size)
    {
        ListPopBack(l);
    }
    else if(Position > 0 && Position + 1 < l->Size)
    {
        ListNode ln = l->Begin;

        while(Position--)
            ln = ln->Next;

        ln->Prev->Next = ln->Next;
        ln->Next->Prev = ln->Prev;
        ListNodeFree(l, ln);

        --l->Size;
    }
}

// Removes an object of the list with an iterator
// Remember to manage erasing manually when iterate
// In ListForEach, the next iteration will not be performed
void ListEraseIt(List ListData, ListIterator Iterator)
{
    ListNode Node = ListItNodeInfo(Iterator);
	
	if (Node->Prev)
	{
		Node->Prev->Next = Node->Next;
	}
	else
	{
		ListData->Begin = Node->Next;
	}

	if (Node->Next)
	{
		Node->Next->Prev = Node->Prev;
	}
	else
	{
		ListData->End = Node->Prev;
	}

	ListNodeFree(ListData, Node);
	
	--ListData->Size;
}

// Returns an interator to the beginning of the list
ListIterator ListBegin(List l)
{
    if(l->Size == 0)
        return NULL;
    else
        return ListNodeData(l->Begin);
}

// Returns an interator to the end of the list
ListIterator ListEnd(List l)
{
    if(l->Size == 0)
        return NULL;
    else
        return ListNodeData(l->End);
}

// Returns an iterator to the next element
ListIterator ListItNext(ListIterator li)
{
    ListNode ln = ListItNodeInfo(li)->Next;

    if(ln)
        return ListNodeData(ln);
    else 
        return NULL;
}

// Returns an iterator to the previous element
ListIterator ListItPrev(ListIterator li)
{
    ListNode ln = ListItNodeInfo(li)->Prev;

    if(ln)
        return ListNodeData(ln);
    else 
        return NULL;
}

// Returns true if have a next element
bool ListItHasNext(ListIterator li)
{
    return ListItNodeInfo(li)->Next != NULL;
}

// Returns true if have a previous element
bool ListItHasPrev(ListIterator li)
{
    return ListItNodeInfo(li)->Prev != NULL;
}

// Returns true if you reached the end of the list
//   If this returns true, you can't aplicate any 
//   other function to this iterator.
bool ListItDone(ListIterator li)
{
    return li == NULL;
}
