/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/Type.h>
#include <MetaData/Value.h>
#include <MetaData/Reference.h>
#include <MetaData/Function.h>
#include <MetaData/Object.h>
#include <MetaData/Storage.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define META_FLOAT_ARRAY_SIZE			0xFFUL

////////////////////////////////////////////////////////////
/// Application entry point
////////////////////////////////////////////////////////////
int main(int argc, char * argv[])
{
	unsigned int i;
	float32 j;

	// Value test definition
	MetadataStorageIndexType MetaFloatValue, MetaFloatArray;
	float32 * FloatValue, * FloatArray;
	
	MetadataStorageIndexType MetaStringValue;
	uint8 * StringValue;

	MetadataStorageIndexType MetaIntegerDefinition;
	uint32 * IntegerDefinition;

	// Reference test definition
	MetadataStorageIndexType MetaRefFloatArray;
	float32 * RefFloatArray;

	// Function test definition
	// ..

	// Object test definiton
	// ..

	// Create storage
	MetadataStorageType Storage = MetadataStorageCreate();


	// Test value storage
	MetaFloatValue = MetadataStorageAllocateValue(Storage, MetadataTypeSize(FLOAT));
	MetaFloatArray = MetadataStorageAllocateValueArray(Storage, MetadataTypeSize(FLOAT), META_FLOAT_ARRAY_SIZE);

	FloatValue = MetadataStorageGetValue(Storage, MetaFloatValue);

	*FloatValue = 123.0f;

	FloatArray = MetadataStorageGetValueArray(Storage, MetaFloatArray);

	for (i = 0, j = 0.0; i < META_FLOAT_ARRAY_SIZE; ++i, j += 1.0f)
	{
		FloatArray[i] = j;
	}

	// Test reference storage


	// Clear storage
	MetadataStorageClear(Storage);

	// Destroy storage
	MetadataStorageDestroy(Storage);

	return 0;
}
