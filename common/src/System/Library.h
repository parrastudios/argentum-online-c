/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_LIBRARY_H
#define SYSTEM_LIBRARY_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define LIBRARY_FLAGS_BIND_LAZY		(UINTEGER_SUFFIX(0x01) << UINTEGER_SUFFIX(0x00))
#define LIBRARY_FLAGS_BIND_LOCAL	(UINTEGER_SUFFIX(0x01) << UINTEGER_SUFFIX(0x01))
#define LIBRARY_FLAGS_BIND_GLOBAL	(UINTEGER_SUFFIX(0x01) << UINTEGER_SUFFIX(0x02))

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef void *                      LibraryImplType;
typedef const char *				LibraryNameType;
typedef uinteger					LibraryFlagsType;
typedef const char *				LibrarySymbolNameType;
typedef void *						LibrarySymbolType;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get the library extension for specified platform
////////////////////////////////////////////////////////////
const char * LibraryExtension();

////////////////////////////////////////////////////////////
/// Loads a library returning the handle to it
////////////////////////////////////////////////////////////
struct LibraryType * LibraryLoad(LibraryNameType Name, LibraryFlagsType Flags);

////////////////////////////////////////////////////////////
/// Unloads a library by its handle
////////////////////////////////////////////////////////////
void LibraryUnload(struct LibraryType * Library);

////////////////////////////////////////////////////////////
/// Get the library name
////////////////////////////////////////////////////////////
LibraryNameType LibraryName(struct LibraryType * Library);

////////////////////////////////////////////////////////////
/// Get a symbol from library
////////////////////////////////////////////////////////////
bool LibrarySymbol(struct LibraryType * Library, LibrarySymbolNameType Name, LibrarySymbolType * Address);

#endif // SYSTEM_LIBRARY_H
