/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Preprocessor/String.h>

#include <stdio.h>

// PREPROCESSOR_STRING_STATIC_DEFINE(HelloWorld, "hello world");

// typedef char assert_string_hash[(PREPROCESSOR_STRING_HASH_IMPL_1(HelloWorld, 0, 0) == 0) ? 1 : -1];


#define GET_THIRD_CHAR(Str) (uinteger)(*Str)

static char HelloWorld[0xFF] = "hello world";

enum
{
	HELLO_WORLD_HASH = GET_THIRD_CHAR("HelloWorld")
	//HELLO_WORLD_HASH = PREPROCESSOR_STRING_HASH_IMPL_1(HelloWorld, 0, 0)
};

////////////////////////////////////////////////////////////
/// Main entry point
////////////////////////////////////////////////////////////
int main(int argc, char * argv[])
{

	printf("Hash of '%s' : %d\n", HelloWorld, HELLO_WORLD_HASH);

	return 0;
}