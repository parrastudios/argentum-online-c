/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PORTABILITY_TYPE_H
#define PORTABILITY_TYPE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Architecture.h>
#include <Portability/Assertion.h>
#include <Portability/Language.h>

////////////////////////////////////////////////////////////
// Define portable fixed-size types
////////////////////////////////////////////////////////////
#include <limits.h>

// 8 bits integer types
#if UCHAR_MAX == 0xFF
#	define INT8_SUFFIX(Value)		Value
#	define UINT8_SUFFIX(Value)		Value ## U

	typedef signed   char			int8;
	typedef unsigned char			uint8;
#else
#	error No 8 bits integer type for this platform
#endif

#define INT8_MAX_RANGE				INT8_SUFFIX(0x7F)
#define INT8_MIN_RANGE				INT8_SUFFIX(0x80)
#define UINT8_MAX_RANGE				UINT8_SUFFIX(0xFF)
#define UINT8_MIN_RANGE				UINT8_SUFFIX(0x00)

cassert(sizeof(int8) == 1 && sizeof(uint8) == 1);

// 16 bits integer types
#if USHRT_MAX == 0xFFFF
#	define INT16_SUFFIX(Value)		Value
#	define UINT16_SUFFIX(Value)		Value ## U

    typedef signed short			int16;
    typedef unsigned short			uint16;
#elif UINT_MAX == 0xFFFF
#	define INT16_SUFFIX(Value)		Value
#	define UINT16_SUFFIX(Value)		Value ## U

	typedef signed int				int16;
	typedef unsigned int			uint16;
#elif ULONG_MAX == 0xFFFF
#	define INT16_SUFFIX(Value)		Value ## L
#	define UINT16_SUFFIX(Value)		Value ## UL

    typedef signed long				int16;
    typedef unsigned long			uint16;
#else
#	error No 16 bits integer type for this platform
#endif

#define INT16_MAX_RANGE				INT16_SUFFIX(0x7FFF)
#define INT16_MIN_RANGE				INT16_SUFFIX(0x8000)
#define UINT16_MAX_RANGE			UINT16_SUFFIX(0xFFFF)
#define UINT16_MIN_RANGE			UINT16_SUFFIX(0x0000)

cassert(sizeof(int16) == 2 && sizeof(uint16) == 2);

// 32 bits integer types
#if PLATFORM_TYPE == PLATFORM_PALMOS
#	define INT32_SUFFIX(Value)		Value ## L
#	define UINT32_SUFFIX(Value)		Value ## UL

	typedef signed long				int32;
	typedef unsigned long			uint32;
#else
#	if USHRT_MAX == 0xFFFFFFFF
#		define INT32_SUFFIX(Value)	Value
#		define UINT32_SUFFIX(Value)	Value ## U

		typedef signed short		int32;
		typedef unsigned short		uint32;
#	elif UINT_MAX == 0xFFFFFFFF
#		define INT32_SUFFIX(Value)	Value
#		define UINT32_SUFFIX(Value)	Value ## U

		typedef signed int			int32;
		typedef unsigned int		uint32;
#	elif ULONG_MAX == 0xFFFFFFFF
#		define INT32_SUFFIX(Value)	Value ## L
#		define UINT32_SUFFIX(Value)	Value ## UL

		typedef signed long			int32;
		typedef unsigned long		uint32;
#	else
#		error No 32 bits integer type for this platform
#	endif
#endif

#define INT32_MAX_RANGE				INT32_SUFFIX(0x7FFFFFFF)
#define INT32_MIN_RANGE				INT32_SUFFIX(0x80000000)
#define UINT32_MAX_RANGE			UINT32_SUFFIX(0xFFFFFFFF)
#define UINT32_MIN_RANGE			UINT32_SUFFIX(0x00000000)

cassert(sizeof(int32) == 4 && sizeof(uint32) == 4);

// 64 bits integer types
#if COMPILER_TYPE == COMPILER_MSVC || COMPILER_TYPE == COMPILER_WATCOM || COMPILER_TYPE == COMPILER_BORLAND
#	define INT64_SUFFIX(Value)		Value ## LL
#	define UINT64_SUFFIX(Value)		Value ## ULL

	typedef signed __int64			int64;
	typedef unsigned __int64		uint64;
#elif ARCH_FAMILY == ARCHITECTURE_64 || (COMPILER_TYPE == COMPILER_GNUC && defined(__LP64__))
#	define INT64_SUFFIX(Value)		Value ## L
#	define UINT64_SUFFIX(Value)		Value ## UL

	typedef signed long				int64;
	typedef unsigned long			uint64;
#else
#	define INT64_SUFFIX(Value)		Value ## LL
#	define UINT64_SUFFIX(Value)		Value ## ULL

	typedef signed long long		int64;
	typedef unsigned long long		uint64;
#endif

#define INT64_MAX_RANGE				INT64_SUFFIX(0x7FFFFFFFFFFFFFFF)
#define INT64_MIN_RANGE				INT64_SUFFIX(0x8000000000000000)
#define UINT64_MAX_RANGE			UINT64_SUFFIX(0xFFFFFFFFFFFFFFFF)
#define UINT64_MIN_RANGE			UINT64_SUFFIX(0x0000000000000000)

cassert(sizeof(int64) == 8 && sizeof(uint64) == 8);

// Generic pointer - integer types
#if ARCH_FAMILY == ARCHITECTURE_32
#	define INTEGER_SUFFIX(Value)	INT32_SUFFIX(Value)
#	define UINTEGER_SUFFIX(Value)	UINT32_SUFFIX(Value)

#   define INTEGER_MAX_RANGE		INT32_MAX_RANGE
#   define INTEGER_MIN_RANGE		INT32_MIN_RANGE
#   define UINTEGER_MAX_RANGE		UINT32_MAX_RANGE
#   define UINTEGER_MIN_RANGE		UINT32_MIN_RANGE

	typedef int32					integer;
	typedef int32					integerptr;
	typedef uint32					uinteger;
	typedef uint32					uintegerptr;
#elif ARCH_FAMILY == ARCHITECTURE_64
#	define INTEGER_SUFFIX(Value)	INT64_SUFFIX(Value)
#	define UINTEGER_SUFFIX(Value)	UINT64_SUFFIX(Value)

#   define INTEGER_MAX_RANGE		INT64_MAX_RANGE
#   define INTEGER_MIN_RANGE		INT64_MIN_RANGE
#   define UINTEGER_MAX_RANGE		UINT64_MAX_RANGE
#   define UINTEGER_MIN_RANGE		UINT64_MIN_RANGE

	typedef int64					integer;
	typedef int64					integerptr;
	typedef uint64					uinteger;
	typedef uint64					uintegerptr;
#else
#	error No integer type for this platform
#endif

cassert(sizeof(void *) == sizeof(integerptr) && sizeof(void *) == sizeof(uintegerptr));

////////////////////////////////////////////////////////////
// Define floating point type precision
////////////////////////////////////////////////////////////
#include <float.h>

#define SINGLE_PRECISION	0x00
#define DOUBLE_PRECISION	0x01

#ifndef PRECISION_TYPE
#	if ARCH_FAMILY == ARCHITECTURE_64
#		define PRECISION_TYPE DOUBLE_PRECISION
#	elif ARCH_FAMILY == ARCHITECTURE_32
#		define PRECISION_TYPE SINGLE_PRECISION
#	endif
#endif

// Floating point types
typedef float	float32;

cassert(sizeof(float32) == 4);

typedef double	float64;

cassert(sizeof(float64) == 8);

// Generic real type
#if PRECISION_TYPE == SINGLE_PRECISION
	typedef float32 real;
#elif PRECISION_TYPE == DOUBLE_PRECISION
	typedef float64 real;
#else
#	error No floating point type for this platform
#endif

#define FLOAT_SUFFIX(Value) Value ## f

#define DOUBLE_SUFFIX(Value) Value

////////////////////////////////////////////////////////////
// Generic boolean macros
////////////////////////////////////////////////////////////
#ifndef bool
#	define bool		uint8
#endif
#ifndef false
#	define false	UINT8_SUFFIX(0x00)
#endif
#ifndef true
#	define true		UINT8_SUFFIX(0x01)
#endif

////////////////////////////////////////////////////////////
// Generic string macros
////////////////////////////////////////////////////////////
#ifndef NULLCHAR
#	define NULLCHAR '\0'
#endif

////////////////////////////////////////////////////////////
// Generic pointer macros
////////////////////////////////////////////////////////////
#ifndef NULL
#	ifndef __cplusplus
#		define NULL	((void *)0)
#	else
#		define NULL (0L)
#	endif
#endif

////////////////////////////////////////////////////////////
// Generic offset evaluator macro
////////////////////////////////////////////////////////////
#ifndef OffsetOf
#	if (COMPILER_TYPE == COMPILER_GNUC && COMPILER_VERSION >= CompilerVersionBuild(4, 0, 0))
#		define OffsetOf(Type, Member) __builtin_offsetof(Type, Member)
#	else
#		define OffsetOf(Type, Member) ((integer) ((int8 *)&((Type *)0)->Member - (int8 *)((Type *)0)))
#	endif
#endif

#ifndef ContainerOf
#	if (COMPILER_TYPE == COMPILER_GNUC)
#		define ContainerOf(Ptr, Type, Member) ({ \
		const typeof(((Type*)0)->Member) * _Ptr = (Ptr); \
		(Type*)((int8*)_Ptr - OffsetOf(Type, Member)); })
#	else
#		define ContainerOf(Ptr, Type, Member) do { /* ... */ } while (0)
#	endif
#endif

////////////////////////////////////////////////////////////
// Generic function parameter evaluator macro (avoid warning)
////////////////////////////////////////////////////////////
#ifndef UnusedParam
#	define UnusedParam(arg) do { (void)(arg); } while (0)
#endif

////////////////////////////////////////////////////////////
// Generic struct member initializer macro
////////////////////////////////////////////////////////////
#if (COMPILER_TYPE != COMPILER_MSVC) && \
	(LANGUAGE_VERSION_TYPE == LANGUAGE_VERSION_C99) || (LANGUAGE_VERSION_TYPE == LANGUAGE_VERSION_C11) || \
	(LANGUAGE_VERSION_TYPE == LANGUAGE_VERSION_CPP11) || (LANGUAGE_VERSION_TYPE == LANGUAGE_VERSION_CPP14)

//	Initialization must be done in same order as
//	in structure definition to allow backward compatibility
#	ifndef TypeInit
#		define TypeInit(Member, Value)				.Member = Value
#	endif
#	ifndef ExprInit
#		define ExprInit(Member, ...)				.Member = { __VA_ARGS__ }
#	endif
#	ifndef UnionInit
#		define UnionInit(Name, Member, Value)		.Name.Member = Value
#	endif
#	ifndef ArrayInit
#		define ArrayInit(Position, Value)			[Position] = Value
#	endif
#	ifndef ArrayExprInit
#		define ArrayExprInit(Position, ...)			[Position] = { __VA_ARGS__ }
#	endif
#else
#	ifndef TypeInit
#		define TypeInit(Member, Value)				Value
#	endif
#	ifndef ExprInit
#		define ExprInit(Member, ...)				{ __VA_ARGS__ }
#	endif
#	ifndef UnionInit
#		define UnionInit(Name, Member, Value)		(void *)Value
#	endif
#	ifndef ArrayInit
#		define ArrayInit(Position, Value)			Value
#	endif
#	ifndef ArrayExprInit
#		define ArrayExprInit(Position, ...)			[Position] = { __VA_ARGS__ }
#	endif
#endif

#endif // PORTABILITY_TYPE_H
