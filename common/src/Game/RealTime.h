/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GAME_REALTIME_H
#define GAME_REALTIME_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Member Data
////////////////////////////////////////////////////////////
struct RealTimeData
{
	uint32	Milliseconds;	///< Time structure
	uint8	Seconds;
	uint8	Minutes;
	uint8	Hour;
	uint8	Day;
	uint8	Month;
	uint32	Year;

	uint32	MultiplyFactor;	///< Time speed
};

////////////////////////////////////////////////////////////
/// Initializes real time from path
////////////////////////////////////////////////////////////
bool RealTimeInitialize(struct RealTimeData * RealTime, char * Path);

////////////////////////////////////////////////////////////
/// Update real time from given adder
////////////////////////////////////////////////////////////
void RealTimeUpdate(struct RealTimeData * RealTime, uint32 TimeAdder);

////////////////////////////////////////////////////////////
/// Destroy real time (if valid path it's stored in file)
////////////////////////////////////////////////////////////
bool RealTimeDestroy(struct RealTimeData * RealTime, char * Path);

#endif // GAME_REALTIME_H
