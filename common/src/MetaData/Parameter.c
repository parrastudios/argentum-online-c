/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/Parameter.h>
#include <MetaData/Value.h>
#include <MetaData/Reference.h>
#include <MetaData/Function.h>
#include <MetaData/Object.h>
#include <MetaData/Copy.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef struct MetadataParameterType
{
	MetadataParameterTypeInfo		InfoType;		///< Type which parameter is passed to the function
	MetadataStorageResourceType		ResourceType;	///< Type of resource associated to the parameter

	union
	{
		MetadataStorageIndexType	Index;			///< Generic index
		MetadataValueType			Value;			///< Parameter as a value
		MetadataReferenceType		Reference;		///< Parameter as a reference
		MetadataFunctionType		Function;		///< Parameter as a function
		MetadataObjectType			Object;			///< Parameter as a object

	} Data;

} * MetadataParameterTypeImpl;

typedef struct MetadataParameterListType
{
	MetadataParameterListCountType	Count;			///< Number of parameters
	struct MetadataParameterType	List[1];		///< Contiguous allocated list of parameters

} * MetadataParameterListImpl;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
/// Set a parameter signature type
////////////////////////////////////////////////////////////
MetadataParameterSignature * MetadataParameterSignatureInitialize(MetadataParameterSignature * ParameterSignature, MetadataParameterTypeInfo InfoType, MetadataStorageResourceType ResourceType, MetadataStorageIndexType Signature)
{
	if (ParameterSignature)
	{
		ParameterSignature->InfoType = InfoType;
		ParameterSignature->ResourceType = ResourceType;
		ParameterSignature->Signature = Signature;
	}

	return ParameterSignature;
}

////////////////////////////////////////////////////////////
/// Create a parameter list
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataParameterListCreate(MetadataScopeType Scope, MetadataParameterListCountType Count)
{
	if (Count > 0 && Count < METADATA_PARAMETER_LIST_LENGTH)
	{
		// Calculate size
		MetadataStorageDataSize Size = OffsetOf(struct MetadataParameterListType, List) + (sizeof(struct MetadataParameterType) * Count);

		// Allocate memory for parameter list
		MetadataParameterListType ParameterList = MetadataStorageAllocateParameterList(MetadataScopeGetStorage(Scope), Size);

		// If allocation was successful
		if (ParameterList != METADATA_STORAGE_INDEX_INVALID)
		{
			MetadataParameterListImpl ParameterListImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), ParameterList);

			if (ParameterListImpl)
			{
				// Set properties
				ParameterListImpl->Count = Count;

				// Initialize parameter slots to null
				for (Count = 0; Count < ParameterListImpl->Count; ++Count)
				{
					MetadataParameterTypeImpl Parameter = &ParameterListImpl->List[Count];

					// Set parameter properties
					Parameter->InfoType = METADATA_PARAMETER_TYPE_INVALID;
					Parameter->ResourceType = METADATA_STORAGE_RESOURCE_INVALID;
					Parameter->Data.Index = METADATA_STORAGE_INDEX_INVALID;
				}
			}
			else
			{
				MetadataStorageDeallocateParameterList(MetadataScopeGetStorage(Scope), ParameterList);
			}
		}

		return ParameterList;
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Copy a parameter list
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataParameterListCopy(MetadataScopeType Scope, MetadataParameterListType ParameterList)
{
	MetadataParameterListImpl ParameterListImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), ParameterList);

	if (ParameterListImpl)
	{
		// Calculate size
		MetadataStorageDataSize Size = OffsetOf(struct MetadataParameterListType, List) + (sizeof(struct MetadataParameterType) * ParameterListImpl->Count);

		// Allocate memory for parameter list
		MetadataParameterListType ParameterListCopy = MetadataStorageAllocateParameterList(MetadataScopeGetStorage(Scope), Size);

		// If allocation was successful
		if (ParameterListCopy != METADATA_STORAGE_INDEX_INVALID)
		{
			MetadataParameterListImpl ParameterListCopyImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), ParameterListCopy);

			if (ParameterListCopyImpl)
			{
				MetadataParameterListCountType Count;

				// Set properties
				ParameterListCopyImpl->Count = ParameterListImpl->Count;

				// Initialize parameter slots to null
				for (Count = 0; Count < ParameterListCopyImpl->Count; ++Count)
				{
					MetadataParameterTypeImpl Parameter = &ParameterListImpl->List[Count];
					MetadataParameterTypeImpl ParameterCopy = &ParameterListCopyImpl->List[Count];

					// Set parameter properties
					ParameterCopy->InfoType = Parameter->InfoType;
					ParameterCopy->ResourceType = Parameter->ResourceType;
					ParameterCopy->Data.Index = Parameter->Data.Index;
				}
			}
			else
			{
				MetadataStorageDeallocateParameterList(MetadataScopeGetStorage(Scope), ParameterListCopy);
			}
		}

		return ParameterListCopy;
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Get size of parameter list
////////////////////////////////////////////////////////////
MetadataParameterListCountType MetadataParameterListSize(MetadataScopeType Scope, MetadataParameterListType ParameterList)
{
	if (ParameterList != METADATA_STORAGE_INDEX_INVALID)
	{
		MetadataParameterListTypeImpl ParameterListImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), ParameterList);

		if (ParameterListImpl)
		{
			return ParameterListImpl->Count;
		}
	}

	return 0;
}

////////////////////////////////////////////////////////////
/// Get the info type of specified parameter
////////////////////////////////////////////////////////////
MetadataParameterTypeInfo MetadataParameterListSignatureInfoType(MetadataScopeType Scope, MetadataParameterListType ParameterList, MetadataParameterType Parameter)
{
	if (ParameterList != METADATA_STORAGE_INDEX_INVALID)
	{
		MetadataParameterListTypeImpl ParameterListImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), ParameterList);

		if (ParameterListImpl && Parameter < ParameterListImpl->Count)
		{
			MetadataParameterTypeImpl ParameterImpl = &ParameterListImpl->List[Parameter];

			return ParameterImpl->InfoType;
		}
	}

	return METADATA_PARAMETER_TYPE_INVALID;
}

////////////////////////////////////////////////////////////
/// Get the resource type of specified parameter
////////////////////////////////////////////////////////////
MetadataStorageResourceType MetadataParameterListSignatureResourceType(MetadataScopeType Scope, MetadataParameterListType ParameterList, MetadataParameterType Parameter)
{
	if (ParameterList != METADATA_STORAGE_INDEX_INVALID)
	{
		MetadataParameterListTypeImpl ParameterListImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), ParameterList);

		if (ParameterListImpl && Parameter < ParameterListImpl->Count)
		{
			MetadataParameterTypeImpl ParameterImpl = &ParameterListImpl->List[Parameter];

			return ParameterImpl->ResourceType;
		}
	}

	return METADATA_STORAGE_RESOURCE_INVALID;
}

////////////////////////////////////////////////////////////
/// Get the index which parameter refers to
////////////////////////////////////////////////////////////
MetadataStorageIndexType MetadataParameterListGetParameterIndex(MetadataScopeType Scope, MetadataParameterListType ParameterList, MetadataParameterType Parameter)
{
	if (ParameterList != METADATA_STORAGE_INDEX_INVALID)
	{
		MetadataParameterListTypeImpl ParameterListImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), ParameterList);

		if (ParameterListImpl && Parameter < ParameterListImpl->Count)
		{
			MetadataParameterTypeImpl ParameterImpl = &ParameterListImpl->List[Parameter];

			return ParameterImpl->Data.Index;
		}
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Define a parameter as a signature
////////////////////////////////////////////////////////////
bool MetadataParameterListDefine(MetadataScopeType Scope, MetadataParameterListType Signature, MetadataParameterType Parameter, MetadataParameterTypeInfo InfoType, MetadataStorageResourceType ResourceType, MetadataStorageIndexType Index)
{
	if (Signature != METADATA_STORAGE_INDEX_INVALID)
	{
		MetadataParameterListTypeImpl SignatureImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), Signature);

		if (SignatureImpl && Parameter < SignatureImpl->Count)
		{
			MetadataParameterTypeImpl ParameterImpl = &SignatureImpl->List[Parameter];

			// Set parameter properties
			ParameterImpl->InfoType = InfoType;
			ParameterImpl->ResourceType = ResourceType;
			ParameterImpl->Data.Index = Index;

			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Destroy a parameter (slot is not removed from list)
////////////////////////////////////////////////////////////
bool MetadataParameterListUndefine(MetadataScopeType Scope, MetadataParameterListType Signature, MetadataParameterType Parameter)
{
	if (Signature != METADATA_STORAGE_INDEX_INVALID)
	{
		MetadataParameterListTypeImpl SignatureImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), Signature);

		if (SignatureImpl && Parameter < SignatureImpl->Count)
		{
			MetadataParameterTypeImpl ParameterImpl = &SignatureImpl->List[Parameter];

			// Reset parameter properties
			ParameterImpl->InfoType = METADATA_PARAMETER_TYPE_INVALID;
			ParameterImpl->ResourceType = METADATA_STORAGE_RESOURCE_INVALID;
			ParameterImpl->Data.Index = METADATA_STORAGE_INDEX_INVALID;

			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Creates a signature
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataParameterListBuildSignature(MetadataScopeType Scope, MetadataParameterListCountType Count, MetadataParameterSignature * ParameterSignatureList)
{
	if (ParameterSignatureList && (Count > 0 && Count < METADATA_PARAMETER_LIST_LENGTH))
	{
		// Calculate size
		MetadataStorageDataSize Size = OffsetOf(struct MetadataParameterListType, List) + (sizeof(struct MetadataParameterType) * Count);

		// Allocate memory for parameter list
		MetadataParameterListType Signature = MetadataStorageAllocateParameterList(MetadataScopeGetStorage(Scope), Size);

		if (Signature != METADATA_STORAGE_INDEX_INVALID)
		{
			MetadataParameterListTypeImpl SignatureImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), Signature);

			if (SignatureImpl)
			{
				// Set properties
				SignatureImpl->Count = Count;

				// Initialize parameter slots
				for (Count = 0; Count < SignatureImpl->Count; ++Count)
				{
					// Define parameter properties
					if (!MetadataParameterListDefine(Scope, Signature, Count, ParameterSignatureList[Count].InfoType, ParameterSignatureList[Count].ResourceType, ParameterSignatureList[Count].Signature))
					{
						// Deallocate resource on error
						MetadataStorageDeallocateParameterList(MetadataScopeGetStorage(Scope), Signature);

						return METADATA_STORAGE_INDEX_INVALID;
					}
				}
			}
			else
			{
				MetadataStorageDeallocateParameterList(MetadataScopeGetStorage(Scope), Signature);
			}
		}

		return Signature;
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Copy a parameter list filled by given parameters
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataParameterListGenerate(MetadataScopeType Scope, MetadataParameterListType Signature, VariableArgumentList * Arguments)
{
	if (Signature != METADATA_STORAGE_INDEX_INVALID)
	{
		// Get signature implementation
		MetadataParameterListTypeImpl SignatureImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), Signature);

		if (SignatureImpl)
		{
			// Create a new parameter list
			MetadataParameterListType ParameterList = MetadataParameterListCreate(Scope, SignatureImpl->Count);

			if (ParameterList != METADATA_STORAGE_INDEX_INVALID)
			{
				// Get parameter list implementation
				MetadataParameterListTypeImpl ParameterListImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), ParameterList);

				if (ParameterListImpl)
				{
					MetadataParameterType Parameter;

					// Iterate through signature and build new parameter list
					for (Parameter = 0; Parameter < SignatureImpl->Count; ++Parameter)
					{
						MetadataParameterTypeImpl ParameterSignature = &SignatureImpl->List[Parameter];
						MetadataParameterTypeImpl ParameterImpl = &ParameterListImpl->List[Parameter];

						MetadataStorageIndexType Index = METADATA_STORAGE_INDEX_INVALID;

						// Check how argument is passed to the function
						if (ParameterSignature->InfoType == METADATA_PARAMETER_TYPE_NULL)
						{
							// Invalid index
							Index = METADATA_STORAGE_INDEX_INVALID;
						}
						else if (ParameterSignature->InfoType == METADATA_PARAMETER_TYPE_BY_VALUE)
						{
							// ...

							if (ParameterSignature->ResourceType == METADATA_STORAGE_RESOURCE_VALUE)
							{
								// Create a value passing data pointer and signature properties
								Index = MetadataValueCreate(Scope,
									MetadataValueTypeOf(Scope, ParameterSignature->Data.Value),
									MetadataValueCount(Scope, ParameterSignature->Data.Value),
									NULL);

								if (Index != METADATA_STORAGE_INDEX_INVALID)
								{
									// Copy argument data
									MetadataValueArgument(Scope, Index, Arguments);
								}
							}
							/*
							else if (...)
							{
								...
							}
							*/
						}
						/*
						else if (ParameterSignature->InfoType == METADATA_PARAMETER_TYPE_BY_REFERENCE)
						{
							// Copy the reference
							Index = ResourceIndexList[Count];
						}
						*/

						// Copy values from signature to real parameter
						ParameterImpl->InfoType = ParameterSignature->InfoType;
						ParameterImpl->ResourceType = ParameterSignature->ResourceType;
						ParameterImpl->Data.Index = Index;
					}

					// Finally return constructed parameter list
					return ParameterList;
				}
				else
				{
					// On parameter list creation error, destroy the instance
					MetadataStorageDeallocateParameterList(MetadataScopeGetStorage(Scope), ParameterList);

					return METADATA_STORAGE_INDEX_INVALID;
				}
			}
		}
	}

	return METADATA_STORAGE_INDEX_INVALID;



	/*
	// Iterate through each argument
	while (Count > 0)
	{
	// Retreive parameter info type
	MetadataParameterTypeInfo InfoType = MetadataParameterListSignatureInfoType(Scope, Signature, Count);

	if (Info)

	// MetadataParameterSignature * ParameterSignature = va_arg(SignatureList, MetadataParameterSignature *);

	// ...

	--Count;
	}
	*/


	/*
	if (Signature != METADATA_STORAGE_INDEX_INVALID)
	{
		// Copy signature to a new parameter list
		//MetadataParameterListType ParameterList = MetadataParameterListCopy(Scope, Signature);

		if (ParameterList != METADATA_STORAGE_INDEX_INVALID)
		{
			MetadataParameterListTypeImpl ParameterListImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), ParameterList);

			if (ParameterListImpl)
			{
				MetadataParameterListCountType Count;

				for (Count = 0; Count < ParameterListImpl->Count; ++Count)
				{
					MetadataParameterTypeImpl Parameter = &ParameterListImpl->List[Count];

					MetadataStorageIndexType Index = METADATA_STORAGE_INDEX_INVALID;

					if (Parameter->InfoType == METADATA_PARAMETER_TYPE_NULL)
					{
						// Invalid index
						Index = METADATA_STORAGE_INDEX_INVALID;
					}
					else if (Parameter->InfoType == METADATA_PARAMETER_TYPE_BY_VALUE)
					{
						// Copy whole resource
						Index = MetadataCopyResource(Scope, Parameter->ResourceType, ResourceIndexList[Count]);
					}
					else if (Parameter->InfoType == METADATA_PARAMETER_TYPE_BY_REFERENCE)
					{
						// Copy the reference
						Index = ResourceIndexList[Count];
					}

					Parameter->Data.Index = Index;
				}
			}
			else
			{
				MetadataStorageDeallocateParameterList(MetadataScopeGetStorage(Scope), ParameterList);
			}
		}

		return ParameterList;
	}

	return METADATA_STORAGE_INDEX_INVALID;
	*/
}

////////////////////////////////////////////////////////////
/// Destroy a parameter list
////////////////////////////////////////////////////////////
void MetadataParameterListDestroy(MetadataScopeType Scope, MetadataParameterListType ParameterList)
{
	MetadataParameterListTypeImpl ParameterListImpl = MetadataStorageGetParameterList(MetadataScopeGetStorage(Scope), ParameterList);

	if (ParameterListImpl)
	{
		MetadataParameterListCountType Count;

		// Set parameter slots to null
		for (Count = 0; Count < ParameterListImpl->Count; ++Count)
		{
			MetadataParameterTypeImpl Parameter = &ParameterListImpl->List[Count];

			// Set parameter properties
			Parameter->InfoType = METADATA_PARAMETER_TYPE_INVALID;
			Parameter->ResourceType = METADATA_STORAGE_RESOURCE_INVALID;
			Parameter->Data.Index = METADATA_STORAGE_INDEX_INVALID;
		}

		// Reset counter
		ParameterListImpl->Count = 0;
	}

	MetadataStorageDeallocateParameterList(MetadataScopeGetStorage(Scope), ParameterList);
}
