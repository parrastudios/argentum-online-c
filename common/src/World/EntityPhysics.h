/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_ENTITY_PHYSICS_H
#define WORLD_ENTITY_PHYSICS_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Vector.h>

#include <World/Component.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define ENTITY_PHYSICS_MASS_DEFAULT 1.0f

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct EntityType;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Physics derivable component base data
////////////////////////////////////////////////////////////
struct ComponentPhysicsData
{
	struct Vector3f				Velocity;
	struct Vector3f				Acceleration;
	struct Vector3f				Force;
	float						Mass;
	uint8						Impl[1];
};

////////////////////////////////////////////////////////////
/// Physics base component type
////////////////////////////////////////////////////////////
struct ComponentPhysicsType
{
	//< Set mass of the an entity
	void (*Mass)(struct EntityType * Entity, float Mass);

	//< Simulate physics of an entity
	void (*Generate)(struct EntityType * Entity, float ElapsedTime);

	//< Change velocity
	void (*Velocity)(struct EntityType * Entity, struct Vector3f * Velocity);
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a physics component
////////////////////////////////////////////////////////////
struct EntityType * EntityPhysicsCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a physics component
////////////////////////////////////////////////////////////
void EntityPhysicsDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Return the entity physics implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentPhysicsType * EntityComponentPhysicsAttr();

////////////////////////////////////////////////////////////
/// Return the entity physics implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentPhysicsImpl();

#endif // WORLD_ENTITY_PHYSICS_H
