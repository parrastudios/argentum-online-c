/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	WORLD_TERRAIN_BASE_H
#define WORLD_TERRAIN_BASE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Component.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define TERRAIN_BASE_NODES_SIZE_INVALID			UINTEGER_MAX_RANGE

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Implicit quadtree related macros
////////////////////////////////////////////////////////////
#define TerrainQuadtreeBottomLeft(Node)				(((Node) << 2) + 1)
#define TerrainQuadtreeBottomRight(Node)			(((Node) << 2) + 2)
#define TerrainQuadtreeTopLeft(Node)				(((Node) << 2) + 3)
#define TerrainQuadtreeTopRight(Node)				(((Node) << 2) + 4)

#define TerrainQuadtreeGetClipFlags(Node)			((Node) & UINTEGER_SUFFIX(0x000000FF))
#define TerrainQuadtreeGetMinZ(Node)				(((Node) >> 8) & UINTEGER_SUFFIX(0x000000FF))
#define TerrainQuadtreeGetMaxZ(Node)				(((Node) >> 16) & UINTEGER_SUFFIX(0x000000FF))

#define TerrainQuadtreeSetClipFlags(Node, Flags)	(((Node) & UINTEGER_SUFFIX(0xFFFFFF00)) | (Flags))
#define TerrainQuadtreeSetMinZ(Node, MinZ)			(((Node) & UINTEGER_SUFFIX(0xFFFF00FF)) | (MinZ) << 8)
#define TerrainQuadtreeSetMaxZ(Node, MaxZ)			(((Node) & UINTEGER_SUFFIX(0xFF00FFFF)) | (MaxZ) << 16)

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct TerrainComponentBaseData
{
	uinteger                        Index;                  //< Index of the current terrain
	uint8 *							HeightMap;				//< Height map data
	uinteger						HeightMapSize;			//< Height map size (2 ^ n)
	uinteger						HeightMapTile;			//< Tiles in xy height map
	struct Vector3f					Scale;					//< Scale vector
	uinteger						PatchSize;				//< Patch size (2 ^ p)
	uinteger						PatchTile;				//< Patches per heightmap side
};

struct TerrainComponentBaseType
{
	//< Initialize base terrain properties
	struct EntityType * (*Initialize)(struct EntityType * Entity, uinteger Index, uinteger HeightMapSize, uinteger HeightMapTile, uinteger PatchSize, struct Vector3f * Scale);

	//< Calculate quadtree nodes for a given patch size
	uinteger (*PatchNodes)(struct EntityType * Entity, uinteger Size);

	//< Return number of quadtree nodes for the heightmap
	uinteger (*HeightMapNodesSize)(struct EntityType * Entity);

	//< Return number of quadtree nodes for the terrain
	uinteger (*NodesSize)(struct EntityType * Entity);

	//< Get the height at desired position
	float (*GetHeightAt)(struct EntityType * Entity, float X, float Y);
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize base components
////////////////////////////////////////////////////////////
bool TerrainFactoryInitializeBase();

////////////////////////////////////////////////////////////
/// Destroy base component pool of terrain factory
////////////////////////////////////////////////////////////
void TerrainFactoryDestroyBase();

////////////////////////////////////////////////////////////
/// Return the terrain base implementation attributes
////////////////////////////////////////////////////////////
const void * TerrainComponentBaseAttr();

////////////////////////////////////////////////////////////
/// Return the terrain base implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * TerrainComponentBaseImpl();

////////////////////////////////////////////////////////////
/// Return the terrain base derived data size
////////////////////////////////////////////////////////////
uinteger TerrainComponentBaseSize();

#endif // WORLD_TERRAIN_BASE_H
