/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <GUI/EventHandler.h>

#include <System/IOHelper.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct EventNode
{
	struct EventPtr		Data;
	struct EventNode *	Next;
};

struct EventNode * First = NULL;
struct EventNode * Last = NULL;

////////////////////////////////////////////////////////////
/// Initialize all events
////////////////////////////////////////////////////////////
void GuiEventHandlerInitialize()
{

}

////////////////////////////////////////////////////////////
// Common Methods
////////////////////////////////////////////////////////////


void VoidHandler_ButtonReleased(struct MouseButtonEvent * ButtonEvent)
{
	// Do stuff
	printf("Void Function\n");
}


////////////////////////////////////////////////////////////
// Create an event for the list
////////////////////////////////////////////////////////////
struct EventNode * GuiEventHandlerCreate()
{
	struct EventNode * Event = (struct EventNode*)MemoryAllocate(sizeof(struct EventNode));

	if (!Event)
		return NULL;

	// Event->Data.Ptr = NULL;
	Event->Data.Type = GUI_EVENT_ERROR;
	Event->Data.ID[0] = NULLCHAR;

	return Event;
}

////////////////////////////////////////////////////////////
/// Return the event type
////////////////////////////////////////////////////////////
uint32 GuiEventHandlerType(char * EventType)
{
	if (strcmp(EventType, "KeyPressed") == 0)
		return GUI_EVENT_KEY_PRESSED;
	else if (strcmp(EventType, "KeyReleased") == 0)
		return GUI_EVENT_KEY_RELEASED;
	else if (strcmp(EventType, "TextEntered") == 0)
		return GUI_EVENT_TEXT_ENTERED;
	else if (strcmp(EventType, "ButtonPressed") == 0)
		return GUI_EVENT_BUTTON_PRESSED;
	else if (strcmp(EventType, "ButtonReleased") == 0)
		return GUI_EVENT_BUTTON_RELEASED;
	else if (strcmp(EventType, "MouseMove") == 0)
		return GUI_EVENT_MOUSE_MOVE;
	else if (strcmp(EventType, "Open") == 0)
		return GUI_EVENT_OPEN;
	else if (strcmp(EventType, "Close") == 0)
		return GUI_EVENT_CLOSE;
	else
		return GUI_EVENT_ERROR;
}

////////////////////////////////////////////////////////////
/// Get the event type of the function id
////////////////////////////////////////////////////////////
uint32 GuiEventHandlerGetType(char * FunctionID)
{
	char EventTypeStr[0xFF];

	// Get the type
	sreadfield(EventTypeStr, FunctionID, '_', 1);

	// Check for the event
	if (GuiEventHandlerType(EventTypeStr) == GUI_EVENT_ERROR)
        //@TODO: warn that this handler is not defined and is using default handler instead.
        return GUI_EVENT_BUTTON_RELEASED;
    else
        return GuiEventHandlerType(EventTypeStr);
}

////////////////////////////////////////////////////////////
// Search an event by id
////////////////////////////////////////////////////////////
struct EventNode * GuiEventHandlerSearch(char * ID)
{
	struct EventNode * Iterator;

	for (Iterator = First; Iterator != NULL; Iterator = Iterator->Next)
	{
		if (strcmp(ID, Iterator->Data.ID) == 0)
		{
			return Iterator;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Add an event to the list
////////////////////////////////////////////////////////////
struct EventNode * GuiEventHandlerAdd(char * ID, void * Func)
{
	struct EventNode * Node = GuiEventHandlerCreate();

	if (Node)
	{
		// Set the type
		Node->Data.Type = GuiEventHandlerGetType(ID);

        // Set the function pointer
        switch (Node->Data.Type)
        {
            case GUI_EVENT_KEY_PRESSED :
                Node->Data.Ptr.KeyPressed = (void (*)(struct KeyEvent*))Func;
                break;

            case GUI_EVENT_KEY_RELEASED :
                Node->Data.Ptr.KeyReleased = (void (*)(struct KeyEvent*))Func;
                break;

            case GUI_EVENT_TEXT_ENTERED :
                Node->Data.Ptr.TextEntered = (void (*)(struct TextEvent*))Func;
                break;

            case GUI_EVENT_BUTTON_PRESSED :
                Node->Data.Ptr.ButtonPressed = (void (*)(struct MouseButtonEvent*))Func;
                break;

            case GUI_EVENT_BUTTON_RELEASED :
                Node->Data.Ptr.ButtonReleased = (void (*)(struct MouseButtonEvent*))Func;
                break;

            case GUI_EVENT_MOUSE_MOVE :
                Node->Data.Ptr.MouseMove = (void (*)(struct MouseMoveEvent*))Func;
                break;

            case GUI_EVENT_OPEN :
                Node->Data.Ptr.Open = (void (*)())Func;
                break;

            case GUI_EVENT_CLOSE :
                Node->Data.Ptr.Close = (void (*)())Func;
                break;

            default :       // By default, no add the function
				MemoryDeallocate(Node);
                return NULL;
        }

        // Copy the function ID
        strcpy(Node->Data.ID, ID);

		// Add to the list
		if (First == NULL)
		{
			// If it is not initialized
			First = Last = Node;
		}
		else
		{
			Last->Next = Node;
			Node->Next = NULL;
			Last = Node;
		}

		return Node;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Remove an event to the list by id
////////////////////////////////////////////////////////////
void GuiEventHandlerRemove(char * ID)
{
	// Search for the event
	struct EventNode * Node = GuiEventHandlerSearch(ID);

	if (Node)
	{
		struct EventNode * Temp, * Prev;

		// Set to null the function ptr
		// Node->Data.Ptr = NULL;

		// Remove from the list
		Temp = Node; Prev = First;

		if (Temp == Prev)
		{
			First = First->Next;

			if (Last == Temp)
			{
				Last = Last->Next;
			}
		}
		else
		{
			while (Prev->Next != Temp)
			{
				Prev = Prev->Next;
			}

			Prev->Next = Temp->Next;

			if (Last == Temp)
				Last = Prev;
		}

		// Destroy it
		MemoryDeallocate(Temp);
	}
}

////////////////////////////////////////////////////////////
// Get an event by id
////////////////////////////////////////////////////////////
struct EventPtr * GuiEventHandlerGet(char * ID)
{
	// Return the event
	if (GuiEventHandlerSearch(ID) != NULL)
	{
        return &GuiEventHandlerSearch(ID)->Data;
    }
    else
    {
        return &GuiEventHandlerAdd(ID, (void*)&VoidHandler_ButtonReleased)->Data;
    }
}

////////////////////////////////////////////////////////////
// Destroy the list
////////////////////////////////////////////////////////////
void GuiEventHandlerDestroy()
{
	if (First == Last)
	{
		MemoryDeallocate(First);

		First = Last = NULL;
	}
	else
	{
		struct EventNode * Iterator = First;

		// Delete all one by one
		while (Iterator != NULL)
		{
			struct EventNode * Temp = Iterator->Next;

			// Set to null the function ptr
			// Iterator->Data.Ptr = NULL;

			// Delete it
			MemoryDeallocate(Iterator);

			Iterator = Temp;
		}
	}
}
