/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/StatesManager.h>
#include <Graphics/Device.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

bool StatesManagerIsModified(struct StatesStack * States, uint32 State)
{
	return (bool)( (States->Pointer > 0) &&
				   (States->Data[States->Pointer * STATES_MODE_SIZE + State] !=
				    States->Data[(States->Pointer - 1) * STATES_MODE_SIZE + State])
				 );
}

void StatesManagerInitialize(struct StatesStack * States)
{
	// Reset states
	StatesManagerReset(States);

	// Default initialization
	StatesManagerSet(States, STATES_DITHER_MODE,	STATES_DITHER_ENABLED);
	StatesManagerSet(States, STATES_TEXTURING_MODE,	STATES_TEXTURING_2D);
	StatesManagerSet(States, STATES_DEPTH_MODE,		STATES_DEPTH_ENABLED);
	StatesManagerSet(States, STATES_LIGHTING_MODE,	STATES_LIGHTING_ENABLED);
	StatesManagerSet(States, STATES_BLEND_MODE,		STATES_BLEND_NORMAL);
	StatesManagerSet(States, STATES_POLYGON_MODE,	STATES_POLYGON_FILL); // STATES_POLYGON_WIREFRAME
	StatesManagerSet(States, STATES_COLOR_MODE,		STATES_COLOR_ENABLED);
	StatesManagerSet(States, STATES_CULLFACE_MODE,	STATES_CULLFACE_ENABLED);

	// Apply default states
	StatesManagerApply(States);
}

void StatesManagerReset(struct StatesStack * States)
{
	uint32 i;

	for (i = 0; i < STATES_MODE_SIZE * STATES_STACK_SIZE; i++)
	{
		States->Data[i] = 0x00;
	}

	States->Pointer = 0;
}

void StatesManagerSave(struct StatesStack * States)
{
	if (States->Pointer < STATES_STACK_SIZE)
	{
		States->Pointer++;
	}
}

void StatesManagerRestore(struct StatesStack * States)
{
	if (States->Pointer > 0)
	{
		States->Pointer--;
	}
}

void StatesManagerClear(struct StatesStack * States)
{
	uint32 i;
		
	for (i = 0; i < STATES_MODE_SIZE; i++)
	{
		States->Data[States->Pointer * STATES_MODE_SIZE + i] = 0x00;
	}
}

void StatesManagerApply(struct StatesStack * States)
{
	// Blend mode
	if (StatesManagerIsModified(States, STATES_BLEND_MODE))
	{
		if (StatesManagerGet(States, STATES_BLEND_MODE) == STATES_BLEND_NONE)
		{
			glBlendFunc(GL_ONE, GL_ZERO);
			glDisable(GL_BLEND);
		}
		else
		{
			glEnable(GL_BLEND);

			if (StatesManagerGet(States, STATES_BLEND_MODE) == STATES_BLEND_NORMAL)
			{
				glBlendFunc(GL_SRC_ALPHA, GL_ONE);
			}
			else if (StatesManagerGet(States, STATES_BLEND_MODE) == STATES_BLEND_ALPHA)
			{
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			}
			else if (StatesManagerGet(States, STATES_BLEND_MODE) == STATES_BLEND_ADD)
			{
				glBlendFunc(GL_ONE, GL_ONE);
				//glBlendEquation(GL_FUNC_ADD);
			}
			else if (StatesManagerGet(States, STATES_BLEND_MODE) == STATES_BLEND_SUBTRACT)
			{
				glBlendFunc(GL_ONE, GL_ONE);
				//glBlendEquation(GL_FUNC_SUBTRACT);
			}
			else if (StatesManagerGet(States, STATES_BLEND_MODE) == STATES_BLEND_MULTIPLY)
			{
				glBlendFunc(GL_DST_COLOR, GL_ZERO);
			}
			else if (StatesManagerGet(States, STATES_BLEND_MODE) == STATES_BLEND_MULTITEXTURE)
			{
				glBlendFunc(GL_ONE, GL_SRC_COLOR);
			}

			glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
			glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
		}
	}


	// Polygon mode
	if (StatesManagerIsModified(States, STATES_POLYGON_MODE))
	{
		if (StatesManagerGet(States, STATES_POLYGON_MODE) == STATES_POLYGON_WIREFRAME)
		{
			// Set the width of the lines
			glLineWidth(1.0f);

			// Polygon mode line
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		}
		else if (StatesManagerGet(States, STATES_POLYGON_MODE) == STATES_POLYGON_FILL)
		{
			// Polygon mode fill
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}
	}

	// Color mode
	if (StatesManagerIsModified(States, STATES_COLOR_MODE))
	{
		if (StatesManagerGet(States, STATES_COLOR_MODE) == STATES_COLOR_DISABLED)
		{
			glDisable(GL_COLOR_MATERIAL);
		}
		else if (StatesManagerGet(States, STATES_COLOR_MODE) == STATES_COLOR_ENABLED)
		{
			glEnable(GL_COLOR_MATERIAL);
			glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
		}
	}

	// Depth mode
	if (StatesManagerIsModified(States, STATES_DEPTH_MODE))
	{
		if (StatesManagerGet(States, STATES_DEPTH_MODE) == STATES_DEPTH_DISABLED)
		{
			glDisable(GL_DEPTH_TEST);
		}
		else if (StatesManagerGet(States, STATES_DEPTH_MODE) == STATES_DEPTH_ENABLED)
		{
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LEQUAL);
		}
	}

	// Lighting mode
	if (StatesManagerIsModified(States, STATES_LIGHTING_MODE))
	{
		if (StatesManagerGet(States, STATES_LIGHTING_MODE) == STATES_LIGHTING_DISABLED)
		{
			glDisable(GL_LIGHTING);
		}
		else if (StatesManagerGet(States, STATES_LIGHTING_MODE) == STATES_LIGHTING_ENABLED)
		{
			glEnable(GL_LIGHTING);
		}
	}

	// Texturing mode
	if (StatesManagerIsModified(States, STATES_TEXTURING_MODE))
	{
		if (StatesManagerGet(States, STATES_TEXTURING_MODE) == STATES_TEXTURING_DISABLED)
		{
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_TEXTURE_3D);
		}
		else if (StatesManagerGet(States, STATES_TEXTURING_MODE) == STATES_TEXTURING_2D)
		{
			glEnable(GL_TEXTURE_2D);
		}
		else if (StatesManagerGet(States, STATES_TEXTURING_MODE) == STATES_TEXTURING_3D)
		{
			glEnable(GL_TEXTURE_3D);
		}
	}

	// Dither mode
	if (StatesManagerIsModified(States, STATES_DITHER_MODE))
	{
		if (StatesManagerGet(States, STATES_DITHER_MODE) == STATES_DITHER_DISABLED)
		{
			glDisable(GL_DITHER);
		}
		else if (StatesManagerGet(States, STATES_DITHER_MODE) == STATES_DITHER_ENABLED)
		{
			glEnable(GL_DITHER);
		}
	}

	// Cull face mode
	if (StatesManagerIsModified(States, STATES_CULLFACE_MODE))
	{
		if (StatesManagerGet(States, STATES_CULLFACE_MODE) == STATES_CULLFACE_DISABLED)
		{
			glDisable(GL_CULL_FACE);
		}
		else if (StatesManagerGet(States, STATES_CULLFACE_MODE) == STATES_CULLFACE_ENABLED)
		{
			glEnable(GL_CULL_FACE);
		}
	}
}

void StatesManagerSet(struct StatesStack * States, uint32 State, uint8 Value)
{
	States->Data[States->Pointer * STATES_MODE_SIZE + State] = Value;
}

uint8 StatesManagerGet(struct StatesStack * States, uint32 State)
{
	return States->Data[States->Pointer * STATES_MODE_SIZE + State];
}
