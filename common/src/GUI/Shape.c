/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/TextureRender.h>
#include <Graphics/Font.h>
#include <System/IOHelper.h>

#include <GUI/Widget.h>

////////////////////////////////////////////////////////////
// Create a shape
////////////////////////////////////////////////////////////
struct GuiWidget * GuiShapeCreate(int32 X, int32 Y, uint32 Width, uint32 Height, uint32 FontSize)
{
	// Create
	struct GuiWidget * Widget = GuiWidgetCreate(GUI_WIDGET_SHAPE, X, Y, FontSize, NULL);

	if (Widget)
	{
		// Set style type
		GuiShapeSetStyleType(&Widget->Attributes.Shape, GUI_SHAPE_TYPE_SQUARE);

		// Set displacement direction
		GuiShapeSetDisplacementDirection(&Widget->Attributes.Shape, GUI_SHAPE_DIRECTION_TOP_TO_BOTTOM);

		// Set the Desplacement
		GuiShapeSetPercent(&Widget->Attributes.Shape, 1.0f);

		// Set default texture
		GuiShapeSetTexture(&Widget->Attributes.Shape, GUI_SHAPE_NORMAL_COLOR);

		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Create a shape by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiShapeCreateDefault()
{
	// Create
	struct GuiWidget * Widget = GuiWidgetCreateDefault(GUI_WIDGET_SHAPE);

	if (Widget)
	{
		// Set style type
		GuiShapeSetStyleType(&Widget->Attributes.Shape, GUI_SHAPE_TYPE_SQUARE);

		// Set displacement direction
		GuiShapeSetDisplacementDirection(&Widget->Attributes.Shape, GUI_SHAPE_DIRECTION_TOP_TO_BOTTOM);

		// Set the status
		GuiShapeSetPercent(&Widget->Attributes.Shape, 1.0f);

		// Set default texture
		GuiShapeSetTexture(&Widget->Attributes.Shape, GUI_SHAPE_NORMAL_COLOR);

		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Set style of the shape
////////////////////////////////////////////////////////////
void GuiShapeSetStyleType(struct GuiShape * Shape, uinteger StyleType)
{
	Shape->StyleType = StyleType;
}

////////////////////////////////////////////////////////////
// Set displacement direction of the shape
////////////////////////////////////////////////////////////
void GuiShapeSetDisplacementDirection(struct GuiShape * Shape, uinteger Direction)
{
    Shape->DisplacementDirection = Direction;
}

////////////////////////////////////////////////////////////
// Set percent of the shape
////////////////////////////////////////////////////////////
void GuiShapeSetPercent(struct GuiShape * Shape, float Percent)
{
	if (Percent >= 0.0f && Percent <= 1.0f)
    {
        Shape->Percentage = Percent;
    }
}

////////////////////////////////////////////////////////////
// Append percent to the shape
////////////////////////////////////////////////////////////
void GuiShapeAppendPercent(struct GuiShape * Shape, float Percent)
{
    float Result = Shape->Percentage + Percent;

    if (Result < 0.0f)
    {
        Shape->Percentage = 0.0f;
    }
	else if (Result > 1.0f)
    {
        Shape->Percentage = 1.0f;
    }
    else
    {
        Shape->Percentage = Result;
    }
}

////////////////////////////////////////////////////////////
// Set texture to be rendered on shape
////////////////////////////////////////////////////////////
void GuiShapeSetTexture(struct GuiShape * Shape, integer Tex)
{
	Shape->TextureFill = Tex;
}

////////////////////////////////////////////////////////////
// Render a shape
////////////////////////////////////////////////////////////
void GuiShapeRender(struct GuiWidget * Form, struct GuiWidget * Widget, struct Color4ub * BackgroundColor, struct Color4ub * FontColor)
{
	struct GuiShape *	Shape = &Widget->Attributes.Shape;
	integer 			Texture = Shape->TextureFill; // texture HardCoded
	float				Percent = Shape->Percentage;
    int32				TextX, TextY;

	// Render depends on style property on script
	if (Shape->StyleType == GUI_SHAPE_TYPE_CIRCUMFERENCE)
	{
		if (Shape->DisplacementDirection == GUI_SHAPE_DIRECTION_RIGTH_TO_LEFT)
		{
			TextureRenderGrhRound(Texture, Form->X + Widget->X + Widget->Width / 2, Form->Y + Widget->Y + Widget->Height / 2, Widget->Width / 2, UINTEGER_SUFFIX(20), 0.0f, 180.0f * Percent, BackgroundColor);
		}
		else if (Shape->DisplacementDirection == GUI_SHAPE_DIRECTION_LEFT_TO_RIGTH)
		{
			TextureRenderGrhRound(Texture, Form->X + Widget->X + Widget->Width / 2, Form->Y + Widget->Y + Widget->Height / 2, Widget->Width / 2, UINTEGER_SUFFIX(20), 180.0f, 180.0f * Percent, BackgroundColor);
		}
	}
	else if (Shape->StyleType == GUI_SHAPE_TYPE_SQUARE)
	{
		// Render square
		switch (Shape->DisplacementDirection)
		{
            case GUI_SHAPE_DIRECTION_TOP_TO_BOTTOM:
                // fill from top>bottom
                TextureRenderGrhColorRepeat(Texture,
                                        Form->X + Widget->X,
                                        Form->Y + Widget->Y,
                                        Form->X + Widget->X + Widget->Width,
                                        Form->Y + Widget->Y + (int32)(Widget->Height * Percent),
                                        BackgroundColor);
                break;
            case GUI_SHAPE_DIRECTION_BOTTOM_TO_TOP:
                TextureRenderGrhColorRepeat(Texture,
                                        Form->X + Widget->X,
                                        Form->Y + Widget->Y + Widget->Height - (int32)(Widget->Height * Percent),
                                        Form->X + Widget->X + Widget->Width,
                                        Form->Y + Widget->Y + Widget->Height,
                                        BackgroundColor);
                break;
            case GUI_SHAPE_DIRECTION_RIGTH_TO_LEFT:
                TextureRenderGrhColorRepeat(Texture,
                                        Form->X + Widget->X + Widget->Width - (int32)(Widget->Width * Percent),
                                        Form->Y + Widget->Y,
                                        Form->X + Widget->X + Widget->Width,
                                        Form->Y + Widget->Y + Widget->Height,
                                        BackgroundColor);
                break;
            case GUI_SHAPE_DIRECTION_LEFT_TO_RIGTH:
                TextureRenderGrhColorRepeat(Texture,
                                        Form->X + Widget->X,
                                        Form->Y + Widget->Y,
                                        Form->X + Widget->X + (int32)(Widget->Width * Percent),
                                        Form->Y + Widget->Y + Widget->Height,
                                        BackgroundColor);
                break;
            default:
                TextureRenderGrhColorRepeat(Texture,
                                        Form->X + Widget->X,
                                        Form->Y + Widget->Y,
                                        Form->X + Widget->X + (int32)(Widget->Width * Percent),
                                        Form->Y + Widget->Y + Widget->Height,
                                        BackgroundColor);
                break;
		}
	}


	if (Widget->Text[0] == NULLCHAR)
		return;

	// Calculate center position
	TextX = (Widget->Width / 2) - (Widget->FontSize * (Widget->Length / 2));
	TextY = (Widget->Height / 2) - 4;

	FontRenderTextColor((float)(TextX + Widget->X + Form->X), (float)(TextY + Widget->Y + Form->Y), Widget->Text, FontColor);

}
