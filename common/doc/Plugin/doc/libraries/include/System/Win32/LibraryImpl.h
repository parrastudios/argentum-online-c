/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_WIN32_LIBRARY_IMPL_H
#define SYSTEM_WIN32_LIBRARY_IMPL_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Types.h>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
typedef struct LibraryImplType		LibraryImplType;
typedef const char *				LibraryNameType;
typedef uinteger					LibraryFlagsType;
typedef const char *				LibrarySymbolNameType;
typedef void *						LibrarySymbolType;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct LibraryImplType
{
	HANDLE Handle;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get the library extension for Win32
////////////////////////////////////////////////////////////
const char * LibraryExtensionImpl();

////////////////////////////////////////////////////////////
/// Loads a library returning the handle to it in Win32 platform
////////////////////////////////////////////////////////////
bool LibraryLoadImpl(LibraryImplType * Impl, LibraryNameType Name, LibraryFlagsType Flags);

////////////////////////////////////////////////////////////
/// Unloads a library by its handle in Win32 platform
////////////////////////////////////////////////////////////
bool LibraryUnloadImpl(LibraryImplType * Impl);

////////////////////////////////////////////////////////////
/// Get a symbol from library in Win32 platform
////////////////////////////////////////////////////////////
bool LibrarySymbolImpl(LibraryImplType * Impl, LibrarySymbolNameType Name, LibrarySymbolType * Address);

#endif // SYSTEM_WIN32_LIBRARY_IMPL_H
