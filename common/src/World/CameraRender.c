/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/CameraRender.h>
#include <Graphics/Device.h>

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create render component in a entity camera
////////////////////////////////////////////////////////////
struct EntityType * CameraRenderCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Set camera render color
////////////////////////////////////////////////////////////
void CameraRenderColor(struct EntityType * Entity, struct Color4ub * Color);

////////////////////////////////////////////////////////////
/// Set camera render line width
////////////////////////////////////////////////////////////
void CameraRenderLineWidth(struct EntityType * Entity, float Width);

////////////////////////////////////////////////////////////
/// Destroy a camera render component
////////////////////////////////////////////////////////////
void CameraRenderDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Render a camera
////////////////////////////////////////////////////////////
void CameraRender(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the camera render implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentRenderType * CameraComponentRenderAttr()
{
	static const struct CameraComponentRenderType CameraComponentRenderImplAttr = {
	    TypeInit(Color, &CameraRenderColor),
	    TypeInit(LineWidth, &CameraRenderLineWidth)
	};

	static const struct ComponentRenderType CameraComponentRenderAttr = {
		TypeInit(Draw, &CameraRender),
		TypeInit(TypeImpl, (const void *)(&CameraComponentRenderImplAttr))
	};

	return (const void *)&CameraComponentRenderAttr;
}

////////////////////////////////////////////////////////////
/// Return the camera render implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * CameraComponentRenderImpl()
{
	static const struct ComponentImplType CameraComponentRenderTypeImpl = {
		ExprInit(Base, TypeInit(Create, &CameraRenderCreate), TypeInit(Destroy, &CameraRenderDestroy)),
		TypeInit(Type, COMPONENT_TYPE_RENDER),
		UnionInit(Attributes, Render, &CameraComponentRenderAttr)
	};

	return &CameraComponentRenderTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create render component in a entity camera
////////////////////////////////////////////////////////////
struct EntityType * CameraRenderCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain render component data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct CameraComponentRenderData * RenderData = ComponentDataDerived(Render, struct CameraComponentRenderData);

		// Set default render color
		Color4ubSet(&RenderData->Color, CAMERA_RENDER_COLOR_DEFAULT);

		// Set default line width
		RenderData->Width = CAMERA_RENDER_WIDTH_DEFAULT;

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set camera render color
////////////////////////////////////////////////////////////
void CameraRenderColor(struct EntityType * Entity, struct Color4ub * Color)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct CameraComponentRenderData * RenderData = ComponentDataDerived(Render, struct CameraComponentRenderData);

		// Set color
		if (Color != NULL)
		{
			Color4ubCopy(&RenderData->Color, Color);
		}
		else
		{
			Color4ubSet(&RenderData->Color, CAMERA_RENDER_COLOR_DEFAULT);
		}
	}
}

////////////////////////////////////////////////////////////
/// Set camera render line width
////////////////////////////////////////////////////////////
void CameraRenderLineWidth(struct EntityType * Entity, float Width)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct CameraComponentRenderData * RenderData = ComponentDataDerived(Render, struct CameraComponentRenderData);

		// Set line width
		RenderData->Width = clamp(Width, CAMERA_RENDER_WIDTH_DEFAULT, CAMERA_RENDER_WIDTH_LIMIT);
	}
}

////////////////////////////////////////////////////////////
/// Destroy a camera render component
////////////////////////////////////////////////////////////
void CameraRenderDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get render component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct CameraComponentRenderData * RenderData = ComponentDataDerived(Render, struct CameraComponentRenderData);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Render a camera
////////////////////////////////////////////////////////////
void CameraRender(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct CameraComponentBaseData * BaseData = ComponentDataDerived(Base, struct CameraComponentBaseData);

		// Get render component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct CameraComponentRenderData * RenderData = ComponentDataDerived(Render, struct CameraComponentRenderData);

		// Get collision sphere component and obtain data reference
		struct ComponentType * Collision = EntityComponentCollisionFrustum(Entity);
		struct ComponentCollisionFrustumData * CollisionData = ComponentDataCollisionFrustum(Collision);

		// Render color declaration
		GLubyte Color[4] = { RenderData->Color.r, RenderData->Color.g, RenderData->Color.b, RenderData->Color.a };

		// Set line width
		/*
		glLineWidth((GLfloat)RenderData->Width);
		*/

		// Render camera with specified color
		/*
		glBegin(GL_POINTS);
			glColor4ubv((GLubyte*)Color);
			glVertex3fv((GLfloat*)&TransformData->Position.v[0]);
		glEnd();
		*/
	}
}

////////////////////////////////////////////////////////////
/// Return the camera render derived data size
////////////////////////////////////////////////////////////
uinteger CameraComponentRenderSize()
{
    return sizeof(struct CameraComponentRenderData);
}
