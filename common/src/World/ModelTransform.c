/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/ModelTransform.h>

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a transform component of a model
////////////////////////////////////////////////////////////
struct EntityType * ModelTransformCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a model transform component
////////////////////////////////////////////////////////////
void ModelTransformDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the model transform implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * ModelComponentTransformImpl()
{
	static const struct ComponentImplType ModelComponentTransformTypeImpl = {
		ExprInit(Base, TypeInit(Create, &ModelTransformCreate), TypeInit(Destroy, &ModelTransformDestroy)),
		TypeInit(Type, COMPONENT_TYPE_TRANSFORM),
		UnionInit(Attributes, Transform, &EntityComponentTransformAttr)
	};

	return &ModelComponentTransformTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a transform component of a model
////////////////////////////////////////////////////////////
struct EntityType * ModelTransformCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Default range
		const float DefaultRange = 2048.0f;
		const float DefaultRangeHalf = DefaultRange / 2.0f;

		// Obtain base component data reference
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

		// Randomize position
#define FloatRandom(Range, Half)	((((float)rand() / (float)(RAND_MAX)) * Range) - Half)

		TransformData->Position.x = FloatRandom(DefaultRange, DefaultRangeHalf);
		TransformData->Position.y = FloatRandom(DefaultRange, DefaultRangeHalf);
		TransformData->Position.z = FloatRandom(DefaultRange, DefaultRangeHalf);

#undef FloatRandom

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a model transform component
////////////////////////////////////////////////////////////
void ModelTransformDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get transform component
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the model transform derived data size
////////////////////////////////////////////////////////////
uinteger ModelComponentTransformSize()
{
	return COMPONENT_DATA_LENGTH_DEFAULT;
}
