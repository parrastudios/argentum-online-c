/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Input.h>
#include <Game/Movement.h>
#include <Game/User.h>
#include <Network/Protocol.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Update user input keys
////////////////////////////////////////////////////////////
uint32 InputUpdateKey(bool * Pressed)
{
	uint32 Key = MOVEMENT_KEY_NONE;

	// Pressed
	if (KeyboardOnKeyPressed(W) || KeyboardOnKeyPressed(Up))
	{
		Key = MOVEMENT_KEY_UP;
		*Pressed = true;
	}

	if (KeyboardOnKeyPressed(D) || KeyboardOnKeyPressed(Right))
	{
		Key = MOVEMENT_KEY_RIGHT;
		*Pressed = true;
	}

	if (KeyboardOnKeyPressed(S) || KeyboardOnKeyPressed(Down))
	{
		Key = MOVEMENT_KEY_DOWN;
		*Pressed = true;
	}

	if (KeyboardOnKeyPressed(A) || KeyboardOnKeyPressed(Left))
	{
		Key = MOVEMENT_KEY_LEFT;
		*Pressed = true;
	}

	#if 0
	{
		if (KeyboardOnKeyPressed(N))
		{
			Key = MOVEMENT_KEY_NEAR;
			*Pressed = true;
		}

		if (KeyboardOnKeyPressed(F))
		{
			Key = MOVEMENT_KEY_FAR;
			*Pressed = true;
		}
	}
	#endif

	// Released
	if (KeyboardOnKeyReleased(W) || KeyboardOnKeyReleased(Up))
	{
		Key = MOVEMENT_KEY_UP;
		*Pressed = false;
	}

	if (KeyboardOnKeyReleased(D) || KeyboardOnKeyReleased(Right))
	{
		Key = MOVEMENT_KEY_RIGHT;
		*Pressed = false;
	}

	if (KeyboardOnKeyReleased(S) || KeyboardOnKeyReleased(Down))
	{
		Key = MOVEMENT_KEY_DOWN;
		*Pressed = false;
	}

	if (KeyboardOnKeyReleased(A) || KeyboardOnKeyReleased(Left))
	{
		Key = MOVEMENT_KEY_LEFT;
		*Pressed = false;
	}

	#if 0
	{
		if (KeyboardOnKeyReleased(N))
		{
			Key = MOVEMENT_KEY_NEAR;
			*Pressed = false;
		}

		if (KeyboardOnKeyReleased(F))
		{
			Key = MOVEMENT_KEY_FAR;
			*Pressed = false;
		}
	}
	#endif

	return Key;
}

////////////////////////////////////////////////////////////
/// Update movement from user input
////////////////////////////////////////////////////////////
bool InputUpdateMovement()
{

	struct UserType *	User = UserGetData();
	bool				Pressed;
	uint32				Key = InputUpdateKey(&Pressed);
	bool				Updated;

	// Update and send movement if need
	//if (Key != MOVEMENT_KEY_NONE || (Key == MOVEMENT_KEY_NONE && User->Character.Movement != MoveNot))
	//{
		if (UserCharacterMove(User, Key, Pressed, &Updated))
		{
			if (Updated)
			{
				// Send to the server
				PacketSendMovementWalk(Key, Pressed);
			}

			return true;
		}

		//return true;
	//}

	return false;
}
