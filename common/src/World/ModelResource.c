/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/ModelResource.h>
#include <Portability/Endianness.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define MODEL_RESOURCE_BODY_MAX_SIZE			0x1000000	//< 16 MBytes

////////////////////////////////////////////////////////////
// Enumerations
////////////////////////////////////////////////////////////
enum
{
	MODEL_RESOURCE_ATTRIBUTE_POSITION = 0x00,
	MODEL_RESOURCE_ATTRIBUTE_TEXCOORD = 0x01,
	MODEL_RESOURCE_ATTRIBUTE_NORMAL = 0x02,
	MODEL_RESOURCE_ATTRIBUTE_TANGENT = 0x03,
	MODEL_RESOURCE_ATTRIBUTE_BLENDINDEXES = 0x04,
	MODEL_RESOURCE_ATTRIBUTE_BLENDWEIGHTS = 0x05,
	MODEL_RESOURCE_ATTRIBUTE_COLOR = 0x06,

	// ..

	MODEL_RESOURCE_ATTRIBUTE_CUSTOM = 0x10,

	// ..

	MODEL_RESOURCE_ATTRIBUTE_SIZE
};

enum
{
	MODEL_RESOURCE_TYPE_BYTE					= 0x00,
	MODEL_RESOURCE_TYPE_UBYTE					= 0x01,
	MODEL_RESOURCE_TYPE_SHORT					= 0x02,
	MODEL_RESOURCE_TYPE_USHORT					= 0x03,
	MODEL_RESOURCE_TYPE_INT						= 0x04,
	MODEL_RESOURCE_TYPE_UINT					= 0x05,
	MODEL_RESOURCE_TYPE_HALF					= 0x06,
	MODEL_RESOURCE_TYPE_FLOAT					= 0x07,
	MODEL_RESOURCE_TYPE_DOUBLE					= 0x08
};

enum
{
	MODEL_RESOURCE_ANIMATION_FLAG_LOOP			= 1 << 0
};

////////////////////////////////////////////////////////////
// Constants
////////////////////////////////////////////////////////////
static const uint8 ModelHeaderIdentifier[MODEL_RESOURCE_HEADER_ID_SIZE] =
{
    0x49, 0x4E, 0x54, 0x45,
    0x52, 0x51, 0x55, 0x41,
    0x4B, 0x45, 0x4D, 0x4F,
    0x44, 0x45, 0x4C, 0x00
};

static const uinteger ModelHeaderVersionIdentifier = 2;

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define ModelResourceEndiannessFormatValue(Type, Value) \
	Value = EndiannessToHostType(Type, Value, ENDIAN_LITTLE)

#define ModelResourceEndiannessFormatArray(Type, Buffer, Offset, Count, Size) \
	EndiannessForEachToHost(Type, (Type *)&Buffer[Offset], (Count * Size), ENDIAN_LITTLE)

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Reset a model resource header
////////////////////////////////////////////////////////////
void ModelResourceHeaderReset(struct ModelResourceHeaderType * ModelResourceHeader)
{
	if (ModelResourceHeader)
	{
		uinteger i;

		for (i = 0; i < MODEL_RESOURCE_HEADER_ID_SIZE; ++i)
		{
			ModelResourceHeader->HeaderIdentifier[i] = 0x00;
		}

		ModelResourceHeader->Version = 0;
		ModelResourceHeader->FileSize = 0;
		ModelResourceHeader->Flags = 0;
		ModelResourceHeader->TextSize = ModelResourceHeader->TextOffset = 0;
		ModelResourceHeader->MeshSize = ModelResourceHeader->MeshOffset = 0;
		ModelResourceHeader->VertexArraysSize = ModelResourceHeader->VertexSize = ModelResourceHeader->VertexArraysOffset = 0;
		ModelResourceHeader->TriangleSize = ModelResourceHeader->TriangleOffset = ModelResourceHeader->AdjacencyOffset = 0;
		ModelResourceHeader->JointSize = ModelResourceHeader->JointOffset = 0;
		ModelResourceHeader->PoseSize = ModelResourceHeader->PoseOffset = 0;
		ModelResourceHeader->AnimSize = ModelResourceHeader->AnimOffset = 0;
		ModelResourceHeader->FrameSize = ModelResourceHeader->FrameChannelSize = ModelResourceHeader->FrameOffset = ModelResourceHeader->BoundOffset = 0;
		ModelResourceHeader->CommentSize = ModelResourceHeader->CommentOffset = 0;
		ModelResourceHeader->ExtensionSize = ModelResourceHeader->ExtensionOffset = 0;
	}
}

////////////////////////////////////////////////////////////
/// Check model resource header integrity
////////////////////////////////////////////////////////////
bool ModelResourceHeaderCheck(struct ModelResourceHeaderType * ModelResourceHeader)
{
	if (ModelResourceHeader)
	{
		uinteger i;

		// Check model format string identifier
		for (i = 0; i < MODEL_RESOURCE_HEADER_ID_SIZE; ++i)
		{
			if (ModelResourceHeader->HeaderIdentifier[i] != ModelHeaderIdentifier[i])
			{
				return false;
			}
		}

		// Check for correct version
		if (ModelResourceHeader->Version != ModelHeaderVersionIdentifier)
		{
			return false;
		}

		// Check for max body size
		if (ModelResourceHeader->FileSize > MODEL_RESOURCE_BODY_MAX_SIZE)
		{
			return false;
		}

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Convert model header to proper endian
////////////////////////////////////////////////////////////
void ModelResourceHeaderFormat(struct ModelResourceHeaderType * ModelResourceHeader)
{
	if (ModelResourceHeader)
	{
		// TODO: Introspection / reflection need here
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->Version);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->FileSize);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->Flags);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->TextSize);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->TextOffset);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->MeshSize);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->MeshOffset);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->VertexArraysSize);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->VertexSize);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->VertexArraysOffset);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->TriangleSize);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->TriangleOffset);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->AdjacencyOffset);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->JointSize);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->JointOffset);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->PoseSize);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->PoseOffset);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->AnimSize);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->AnimOffset);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->FrameSize);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->FrameChannelSize);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->FrameOffset);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->BoundOffset);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->CommentSize);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->CommentOffset);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->ExtensionSize);
		ModelResourceEndiannessFormatValue(uint32, ModelResourceHeader->ExtensionOffset);
	}
}

////////////////////////////////////////////////////////////
/// Parse model resource header
////////////////////////////////////////////////////////////
bool ModelResourceHeaderRead(struct ModelResourceHeaderType * ModelResourceHeader, FILE * File)
{
	if (ModelResourceHeader && File)
	{
		// Read model header
		if (fread(ModelResourceHeader, 1, sizeof(struct ModelResourceHeaderType), File) == sizeof(struct ModelResourceHeaderType))
		{
			// Convert header to correct endianness
			ModelResourceHeaderFormat(ModelResourceHeader);

			// Check model format integrity
			return ModelResourceHeaderCheck(ModelResourceHeader);
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Create a model resource representation
////////////////////////////////////////////////////////////
void ModelResourceCreate(struct ModelResourceImplType * ModelResource)
{
	if (ModelResource)
	{
		// Allocate header in the buffer
		ModelResource->Buffer = (uint8 *)MemoryAllocate(sizeof(struct ModelResourceHeaderType));

		if (ModelResource->Buffer)
		{
			// Clear model resource
			ModelResourceClear(ModelResource);

			// Set header pointer to buffer
			ModelResource->Header = (struct ModelResourceHeaderType *)ModelResource->Buffer;

			// Reset header
			ModelResourceHeaderReset(ModelResource->Header);
		}
	}
}

////////////////////////////////////////////////////////////
/// Clear a model resource representation
////////////////////////////////////////////////////////////
void ModelResourceClear(struct ModelResourceImplType * ModelResource)
{
	if (ModelResource)
	{
		ModelResource->Header = NULL;
		ModelResource->Text = NULL;
		ModelResource->Meshes = NULL;
		ModelResource->Triangles = NULL;
		ModelResource->Neighbors = NULL;
		ModelResource->VertexArrays = NULL;
		ModelResource->Animations = NULL;
		ModelResource->Joints = NULL;
		ModelResource->Poses = NULL;
		ModelResource->Frames = NULL;
		ModelResource->Bounds = NULL;

		ModelResource->Vertex.Positions = NULL;
		ModelResource->Vertex.Normals = NULL;
		ModelResource->Vertex.Tangents = NULL;
		ModelResource->Vertex.TextureCoords = NULL;
		ModelResource->Vertex.BlendIndexes = NULL;
		ModelResource->Vertex.BlendWeights = NULL;
		ModelResource->Vertex.Colors = NULL;
	}
}

////////////////////////////////////////////////////////////
/// Parse model resource (on success, file handle is closed)
////////////////////////////////////////////////////////////
bool ModelResourceRead(struct ModelResourceImplType * ModelResource, FILE * File)
{
	if (ModelResource && File)
	{
		// Initialize model resource
		ModelResourceCreate(ModelResource);

		// Read model header
		if (ModelResourceHeaderRead(ModelResource->Header, File))
		{
			uinteger BodySize = ModelResource->Header->FileSize - sizeof(struct ModelResourceHeaderType);

			if (BodySize > 0)
			{
				// Rellocate model buffer to fit the body
				ModelResource->Buffer = (uint8 *)MemoryReallocate(ModelResource->Buffer, ModelResource->Header->FileSize * sizeof(uint8));

				// Set header pointer to new buffer block
				ModelResource->Header = (struct ModelResourceHeaderType *)ModelResource->Buffer;

				// Read model body
				if (fread(ModelResource->Buffer + sizeof(struct ModelResourceHeaderType), 1, BodySize, File) == BodySize)
				{
					// Close file handle
					fclose(File);

					return true;
				}
			}
			else
			{
				// Close file handle
				fclose(File);

				return true;
			}
		}

		// On error, clear model resource
		ModelResourceClear(ModelResource);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Destroy a model resource representation
////////////////////////////////////////////////////////////
void ModelResourceDestroy(struct ModelResourceImplType * ModelResource)
{
	if (ModelResource)
	{
		ModelResourceClear(ModelResource);

		MemoryDeallocate(ModelResource->Buffer);
	}
}

////////////////////////////////////////////////////////////
///	Check vertex array format
////////////////////////////////////////////////////////////
static bool ModelResourceLoadCheckVertexArray(struct ModelResourceVertexArrayType * VertexArray)
{
	if (VertexArray && VertexArray->Type < MODEL_RESOURCE_ATTRIBUTE_SIZE)
	{
		static struct VertexTypeToFormatMapping {
			uint32 Format;
			uint32 Size;
		} FormatMap[MODEL_RESOURCE_ATTRIBUTE_SIZE] = {
			ArrayExprInit(MODEL_RESOURCE_ATTRIBUTE_POSITION,
				TypeInit(Format, MODEL_RESOURCE_TYPE_FLOAT),
				TypeInit(Size, 3)),
			ArrayExprInit(MODEL_RESOURCE_ATTRIBUTE_TEXCOORD,
				TypeInit(Format, MODEL_RESOURCE_TYPE_FLOAT),
				TypeInit(Size, 2)),
			ArrayExprInit(MODEL_RESOURCE_ATTRIBUTE_NORMAL,
				TypeInit(Format, MODEL_RESOURCE_TYPE_FLOAT),
				TypeInit(Size, 3)),
			ArrayExprInit(MODEL_RESOURCE_ATTRIBUTE_TANGENT,
				TypeInit(Format, MODEL_RESOURCE_TYPE_FLOAT),
				TypeInit(Size, 4)),
			ArrayExprInit(MODEL_RESOURCE_ATTRIBUTE_BLENDINDEXES,
				TypeInit(Format, MODEL_RESOURCE_TYPE_UBYTE),
				TypeInit(Size, 4)),
			ArrayExprInit(MODEL_RESOURCE_ATTRIBUTE_BLENDWEIGHTS,
				TypeInit(Format, MODEL_RESOURCE_TYPE_UBYTE),
				TypeInit(Size, 4)),
			ArrayExprInit(MODEL_RESOURCE_ATTRIBUTE_COLOR,
				TypeInit(Format, MODEL_RESOURCE_TYPE_UBYTE),
				TypeInit(Size, 4))

			// ...

		};

		return (bool)(FormatMap[VertexArray->Type].Format == VertexArray->Format &&
			FormatMap[VertexArray->Type].Size == VertexArray->Size);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Load model resource meshes from buffer
////////////////////////////////////////////////////////////
bool ModelResourceLoadMeshes(struct ModelResourceImplType * ModelResource)
{
	if (ModelResource && ModelResource->Buffer)
	{
		uinteger i;

		// Set text pointer
		if (ModelResource->Header->TextOffset > 0)
		{
			ModelResource->Text = (uint8 *)&ModelResource->Buffer[ModelResource->Header->TextOffset];
		}

		// Set mesh pointer
		ModelResource->Meshes = (struct ModelResourceMeshType *)&ModelResource->Buffer[ModelResource->Header->MeshOffset];

		for (i = 0; i < ModelResource->Header->MeshSize; ++i)
		{
			struct ModelResourceMeshType * Mesh = &ModelResource->Meshes[i];

			// TODO: Introspection / reflection need here
			ModelResourceEndiannessFormatValue(uint32, Mesh->Name);
			ModelResourceEndiannessFormatValue(uint32, Mesh->Material);
			ModelResourceEndiannessFormatValue(uint32, Mesh->VertexOffset);
			ModelResourceEndiannessFormatValue(uint32, Mesh->VertexSize);
			ModelResourceEndiannessFormatValue(uint32, Mesh->TriangleOffset);
			ModelResourceEndiannessFormatValue(uint32, Mesh->TriangleSize);
		}

		// Set vertex arrays pointer
		ModelResource->VertexArrays = (struct ModelResourceVertexArrayType *)&ModelResource->Buffer[ModelResource->Header->VertexArraysOffset];
		
		// Set up vertex arrays
		for (i = 0; i < ModelResource->Header->VertexArraysSize; ++i)
		{
			struct ModelResourceVertexArrayType * VertexArray = &ModelResource->VertexArrays[i];

			// TODO: Introspection / reflection need here
			ModelResourceEndiannessFormatValue(uint32, VertexArray->Type);
			ModelResourceEndiannessFormatValue(uint32, VertexArray->Flags);
			ModelResourceEndiannessFormatValue(uint32, VertexArray->Format);
			ModelResourceEndiannessFormatValue(uint32, VertexArray->Size);
			ModelResourceEndiannessFormatValue(uint32, VertexArray->Offset);

			// Check vertex array integrity
			if (ModelResourceLoadCheckVertexArray(VertexArray))
			{
				// TODO: Introspection / reflection need here & mapping from type to callback
				if (VertexArray->Format == MODEL_RESOURCE_TYPE_FLOAT)
				{
					ModelResourceEndiannessFormatArray(float32, ModelResource->Buffer, VertexArray->Offset, ModelResource->Header->VertexSize, VertexArray->Size);
				}
				else if (VertexArray->Format == MODEL_RESOURCE_TYPE_DOUBLE)
				{
					ModelResourceEndiannessFormatArray(float64, ModelResource->Buffer, VertexArray->Offset, ModelResource->Header->VertexSize, VertexArray->Size);
				}
				else if (VertexArray->Format == MODEL_RESOURCE_TYPE_SHORT)
				{
					ModelResourceEndiannessFormatArray(int16, ModelResource->Buffer, VertexArray->Offset, ModelResource->Header->VertexSize, VertexArray->Size);
				}
				else if (VertexArray->Format == MODEL_RESOURCE_TYPE_USHORT)
				{
					ModelResourceEndiannessFormatArray(uint16, ModelResource->Buffer, VertexArray->Offset, ModelResource->Header->VertexSize, VertexArray->Size);
				}
				else if (VertexArray->Format == MODEL_RESOURCE_TYPE_INT)
				{
					ModelResourceEndiannessFormatArray(int32, ModelResource->Buffer, VertexArray->Offset, ModelResource->Header->VertexSize, VertexArray->Size);
				}
				else if (VertexArray->Format == MODEL_RESOURCE_TYPE_UINT)
				{
					ModelResourceEndiannessFormatArray(uint32, ModelResource->Buffer, VertexArray->Offset, ModelResource->Header->VertexSize, VertexArray->Size);
				}
				else if (VertexArray->Format == MODEL_RESOURCE_TYPE_BYTE || VertexArray->Format == MODEL_RESOURCE_TYPE_UBYTE)
				{
					// Nothing to do
				}
				else
				{
					return false;
				}

				// Save intermediate pointers to vertex arrays
				if (VertexArray->Type == MODEL_RESOURCE_ATTRIBUTE_POSITION)
				{
					ModelResource->Vertex.Positions = (float32 *)&ModelResource->Buffer[VertexArray->Offset];
				}
				else if (VertexArray->Type == MODEL_RESOURCE_ATTRIBUTE_TEXCOORD)
				{
					ModelResource->Vertex.TextureCoords = (float32 *)&ModelResource->Buffer[VertexArray->Offset];
				}
				else if (VertexArray->Type == MODEL_RESOURCE_ATTRIBUTE_NORMAL)
				{
					ModelResource->Vertex.Normals = (float32 *)&ModelResource->Buffer[VertexArray->Offset];
				}
				else if (VertexArray->Type == MODEL_RESOURCE_ATTRIBUTE_TANGENT)
				{
					ModelResource->Vertex.Tangents = (float32 *)&ModelResource->Buffer[VertexArray->Offset];
				}
				else if (VertexArray->Type == MODEL_RESOURCE_ATTRIBUTE_BLENDINDEXES)
				{
					ModelResource->Vertex.BlendIndexes = (uint8 *)&ModelResource->Buffer[VertexArray->Offset];
				}
				else if (VertexArray->Type == MODEL_RESOURCE_ATTRIBUTE_BLENDWEIGHTS)
				{
					ModelResource->Vertex.BlendWeights = (uint8 *)&ModelResource->Buffer[VertexArray->Offset];
				}
				else if (VertexArray->Type == MODEL_RESOURCE_ATTRIBUTE_COLOR)
				{
					ModelResource->Vertex.Colors = (uint8 *)&ModelResource->Buffer[VertexArray->Offset];
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}

		// Set triangle pointer
		ModelResource->Triangles = (struct ModelResourceTriangleType *)&ModelResource->Buffer[ModelResource->Header->TriangleOffset];

		// TODO: Introspection / reflection need here
		ModelResourceEndiannessFormatArray(uint32, ModelResource->Buffer, ModelResource->Header->TriangleOffset, ModelResource->Header->TriangleSize, 3);

		// Set neighbors pointer (adjacency)
		ModelResource->Neighbors = (struct ModelResourceAdjacencyType *)&ModelResource->Buffer[ModelResource->Header->AdjacencyOffset];

		// TODO: Introspection / reflection need here
		ModelResourceEndiannessFormatArray(uint32, ModelResource->Buffer, ModelResource->Header->AdjacencyOffset, ModelResource->Header->TriangleSize, 3);
		
		// Set joint pointer
		ModelResource->Joints = (struct ModelResourceJointType *)&ModelResource->Buffer[ModelResource->Header->JointOffset];

		for (i = 0; i < ModelResource->Header->JointSize; ++i)
		{
			struct ModelResourceJointType * Joint = &ModelResource->Joints[i];

			// TODO: Introspection / reflection need here
			ModelResourceEndiannessFormatValue(uint32, Joint->Name);
			ModelResourceEndiannessFormatValue(int32, Joint->Parent);
			EndiannessForEachToHost(float32, Joint->Translate, 3, ENDIAN_LITTLE);
			EndiannessForEachToHost(float32, Joint->Rotate, 4, ENDIAN_LITTLE);
			EndiannessForEachToHost(float32, Joint->Scale, 3, ENDIAN_LITTLE);
		}

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Load model resource animations from buffer
////////////////////////////////////////////////////////////
bool ModelResourceLoadAnims(struct ModelResourceImplType * ModelResource)
{
	if (ModelResource && ModelResource->Buffer)
	{
		uinteger i;

		// Set poses pointer
		ModelResource->Poses = (struct ModelResourcePoseType *)&ModelResource->Buffer[ModelResource->Header->PoseOffset];

		for (i = 0; i < ModelResource->Header->PoseSize; ++i)
		{
			struct ModelResourcePoseType * Pose = &ModelResource->Poses[i];

			// TODO: Introspection / reflection need here
			ModelResourceEndiannessFormatValue(int32, Pose->Parent);
			ModelResourceEndiannessFormatValue(uint32, Pose->Mask);
			EndiannessForEachToHost(float32, Pose->ChannelOffset, 10, ENDIAN_LITTLE);
			EndiannessForEachToHost(float32, Pose->ChannelScale, 10, ENDIAN_LITTLE);
		}

		// Set animations pointer
		ModelResource->Animations = (struct ModelResourceAnimationType *)&ModelResource->Buffer[ModelResource->Header->AnimOffset];

		for (i = 0; i < ModelResource->Header->AnimSize; ++i)
		{
			struct ModelResourceAnimationType * Animation = &ModelResource->Animations[i];

			// TODO: Introspection / reflection need here
			ModelResourceEndiannessFormatValue(uint32, Animation->Name);
			ModelResourceEndiannessFormatValue(uint32, Animation->FrameOffset);
			ModelResourceEndiannessFormatValue(uint32, Animation->FrameSize);
			ModelResourceEndiannessFormatValue(float32, Animation->FrameRate);
			ModelResourceEndiannessFormatValue(uint32, Animation->Flags);
		}

		// Create frame list
		ModelResource->Frames = (uint16 *)&ModelResource->Buffer[ModelResource->Header->FrameOffset];

		// TODO: Introspection / reflection need here
		ModelResourceEndiannessFormatArray(uint16, ModelResource->Buffer, ModelResource->Header->FrameOffset, ModelResource->Header->FrameSize, ModelResource->Header->FrameChannelSize);

		// Set bounds pointer
		ModelResource->Bounds = (struct ModelResourceBoundsType *)&ModelResource->Buffer[ModelResource->Header->BoundOffset];

		for (i = 0; i < ModelResource->Header->FrameSize; ++i)
		{
			struct ModelResourceBoundsType * Bound = &ModelResource->Bounds[i];

			// TODO: Introspection / reflection need here
			EndiannessForEachToHost(float32, Bound->BoundingBoxMin, 3, ENDIAN_LITTLE);
			EndiannessForEachToHost(float32, Bound->BoundingBoxMax, 3, ENDIAN_LITTLE);
			ModelResourceEndiannessFormatValue(float32, Bound->XYRadius);
			ModelResourceEndiannessFormatValue(float32, Bound->Radius);
		}

		return true;
	}

	return false;
}
