/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATATYPE_STRING_STRING_H
#define DATATYPE_STRING_STRING_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Types.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// NARG
////////////////////////////////////////////////////////////
#define PP_NARG(...) \
         PP_NARG_(__VA_ARGS__,PP_RSEQ_N())

#define PP_NARG_(...) \
         PP_ARG_N(__VA_ARGS__)

#define PP_ARG_N( \
          _1, _2, _3, _4, _5, _6, _7, _8, _9,_10, \
         _11,_12,_13,_14,_15,_16,_17,_18,_19,_20, \
         _21,_22,_23,_24,_25,_26,_27,_28,_29,_30, \
         _31,_32,_33,_34,_35,_36,_37,_38,_39,_40, \
         _41,_42,_43,_44,_45,_46,_47,_48,_49,_50, \
         _51,_52,_53,_54,_55,_56,_57,_58,_59,_60, \
         _61,_62,_63,N,...) N
#define PP_RSEQ_N() \
         63,62,61,60,                   \
         59,58,57,56,55,54,53,52,51,50, \
         49,48,47,46,45,44,43,42,41,40, \
         39,38,37,36,35,34,33,32,31,30, \
         29,28,27,26,25,24,23,22,21,20, \
         19,18,17,16,15,14,13,12,11,10, \
         9,8,7,6,5,4,3,2,1,0

////////////////////////////////////////////////////////////
// EXPR_S
////////////////////////////////////////////////////////////
#define CAT(a, ...) PRIMITIVE_CAT(a, __VA_ARGS__)
#define PRIMITIVE_CAT(a, ...) a ## __VA_ARGS__

#define INC(x) PRIMITIVE_CAT(INC_, x)
#define INC_0 1
#define INC_1 2
#define INC_2 3
#define INC_3 4
#define INC_4 5
#define INC_5 6
#define INC_6 7
#define INC_7 8
#define INC_8 9
#define INC_9 9

#define DEC(x) PRIMITIVE_CAT(DEC_, x)
#define DEC_0 0
#define DEC_1 0
#define DEC_2 1
#define DEC_3 2
#define DEC_4 3
#define DEC_5 4
#define DEC_6 5
#define DEC_7 6
#define DEC_8 7
#define DEC_9 8

#define EXPR_S(s) PRIMITIVE_CAT(EXPR_, s)
#define EXPR_0(...) __VA_ARGS__
#define EXPR_1(...) __VA_ARGS__
#define EXPR_2(...) __VA_ARGS__
#define EXPR_3(...) __VA_ARGS__
#define EXPR_4(...) __VA_ARGS__
#define EXPR_5(...) __VA_ARGS__
#define EXPR_6(...) __VA_ARGS__
#define EXPR_7(...) __VA_ARGS__
#define EXPR_8(...) __VA_ARGS__
#define EXPR_9(...) __VA_ARGS__

#define CHECK_N(x, n, ...) n
#define CHECK(...) CHECK_N(__VA_ARGS__, 0,)

#define NOT(x) CHECK(PRIMITIVE_CAT(NOT_, x))
#define NOT_0 ~, 1,

#define COMPL(b) PRIMITIVE_CAT(COMPL_, b)
#define COMPL_0 1
#define COMPL_1 0

#define BOOL(x) COMPL(NOT(x))

#define IIF(c) PRIMITIVE_CAT(IIF_, c)
#define IIF_0(t, ...) __VA_ARGS__
#define IIF_1(t, ...) t

#define IF(c) IIF(BOOL(c))

#define EAT(...)
#define EXPAND(...) __VA_ARGS__
#define WHEN(c) IF(c)(EXPAND, EAT)

#define EMPTY()
#define DEFER(id) id EMPTY()
#define OBSTRUCT(id) id DEFER(EMPTY)()
     
//#define REPEAT_S(s, n, m, ...) \
//        IF(n)(REPEAT_I, EAT)(OBSTRUCT(), INC(s), DEC(n), m, __VA_ARGS__)
//        
//#define REPEAT_INDIRECT() REPEAT_S
//#define REPEAT_I(_, s, n, m, ...) \
//        EXPR_S _(s)( \
//            REPEAT_INDIRECT _()(s, n, m, __VA_ARGS__) \
//        )\
//        m _(s, n, __VA_ARGS__)
        
#define REPEAT_S(s, n, m, ...) \
        REPEAT_I(OBSTRUCT(), INC(s), n, m, __VA_ARGS__)
        
#define REPEAT_INDIRECT() REPEAT_I
#define REPEAT_I(_, s, n, m, ...) \
        WHEN _(n)(EXPR_S _(s)( \
            REPEAT_INDIRECT _()(OBSTRUCT _(), INC _(s), DEC _(n), m, __VA_ARGS__) \
        ))\
        m _(s, n, __VA_ARGS__)

#define COMMA() ,

#define COMMA_IF(n) IF(n)(COMMA, EAT)()

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define STRING_TYPE_UINT8		0x01 << 0x00
#define STRING_TYPE_UINT16		0x01 << 0x01
#define STRING_TYPE_UINT32		0x01 << 0x02

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
#define StringAttributesDef() \
	uint32	Size;	\
	uint32	Length;	\
	uint8	Type

struct StringData
{
	StringAttributesDef();
};

#define string_data_size() (sizeof(uint32) * 2 + sizeof(uint8))

#define string_type_impl(Type) (sizeof(Type) == STRING_TYPE_UINT8) ? STRING_TYPE_UINT8 : \
							  ((sizeof(Type) == STRING_TYPE_UINT16) ? STRING_TYPE_UINT16 : STRING_TYPE_UINT32)

#define string_fixed_static_initializer(Type, Name, Size) Name = { Size, 0, string_type_impl(Type) }

#define string_fixed_def(Type, Size) \
	union \
	{ \
		struct \
		{ \
			StringAttributesDef(); \
		}; \
		Type				Buffer[(Size * sizeof(Type)) + string_data_size() + 1]; \
	}

#define string_def(Type, Size, ...) (PP_NARG(__VA_ARGS__, 0) - 1) // todo

/*
#define STRING_TYPE_IMPL_1 uint8*
#define STRING_TYPE_IMPL_2 uint16*
#define STRING_TYPE_IMPL_4 uint32*

#define string_type(Type) STRING_TYPE_IMPL_##(sizeof(Type) == STRING_TYPE_UINT8) ? STRING_TYPE_UINT8 : \
((sizeof(Type) == STRING_TYPE_UINT16) ? STRING_TYPE_UINT16 : STRING_TYPE_UINT32)

#define string_type_impl(Type) (Type == STRING_TYPE_UINT8) ? STRING_TYPE_UINT8 : \
((Type == STRING_TYPE_UINT16) ? STRING_TYPE_UINT16 : STRING_TYPE_UINT32)

#define string_fixed_ptr(string) (string_type(string.Data.Type))((&string) + sizeof(struct StringData))
*/

#define string_ptr(string) ((&string) + string_data_size())

#define string_allocate(Type, Size) \
	(struct StringData*)malloc(sizeof(struct StringData) + sizeof(Type) * Size)

#define string_initialize(TypeOf, SizeOf, Name) \
	Name->Size = SizeOf; \
	Name->Length = 0; \
	Name->Type = sizeof(TypeOf)

#define string_create(Type, Size, Name) \
	struct StringData * Name = string_allocate(Type, Size)

#define string_destroy(String) \
	free(String)

#define string_set(String, Literal) \
	do \
	{ \
		char * StringIterator = (char*)string_ptr(String); \
		char * LiteralIterator = (char*)Literal; \
		String->Length = 0; \
		do \
		{ \
			*(StringIterator++) = (char)*(LiteralIterator++); \
			++String->Length; \
		} while (*LiteralIterator != '\0'); \
	} while (0)

#define string_dbg(String) printf(#String ## " { Size : %d, Len : %d, Type : %c, Content : %s}\n", String->Size, String->Length, String->Type, string_ptr(String))

#define string_cpy(Dst, Src) \
	do \
	{ \
		if (Src->Type == Dst->Type && Src->Length <= Dst->Size) \
		{ \
			char * SrcIterator = (char*)string_ptr(Src); \
			char * DstIterator = (char*)string_ptr(Dst); \
			for (Dst->Length = 0; Dst->Length < Src->Length; ++Dst->Length) \
			{ \
				*(DstIterator++) = *(SrcIterator++); \
			} \
			*DstIterator = '\0'; \
		} \
	} while (0)

#endif // DATATYPE_STRING_STRING_H
