/////////////////////////////////////////////////////////////////////////////
//	Preprocessor Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A cross-platform macro based library to give support
//	for preprocessor metaprogramming.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PREPROCESSOR_ARGUMENTS_H
#define PREPROCESSOR_ARGUMENTS_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Compiler.h>
#include <Preprocessor/Empty.h>
#include <Preprocessor/Concatenation.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PREPROCESSOR_ARGS_COUNT_SIZE	64

#define PREPROCESSOR_ARGS_COUNT_SEQ_IMPL() \
	63, 62, 61, 60, \
	59, 58, 57, 56, 55, 54, 53, 52, 51, 50, \
	49, 48, 47, 46, 45, 44, 43, 42, 41, 40, \
	39, 38, 37, 36, 35, 34, 33, 32, 31, 30, \
	29, 28, 27, 26, 25, 24, 23, 22, 21, 20, \
	19, 18, 17, 16, 15, 14, 13, 12, 11, 10, \
	9, 8, 7, 6, 5, 4, 3, 2, 1, 0

#define PREPROCESSOR_ARGS_COMMA_SEQ_IMPL() \
	1, 1, 1, 1, 1, 1, 1, 1, \
	1, 1, 1, 1, 1, 1, 1, 1, \
	1, 1, 1, 1, 1, 1, 1, 1, \
	1, 1, 1, 1, 1, 1, 1, 1, \
	1, 1, 1, 1, 1, 1, 1, 1, \
	1, 1, 1, 1, 1, 1, 1, 1, \
	1, 1, 1, 1, 1, 1, 1, 1, \
	1, 1, 1, 1, 1, 1, 0


////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define PREPROCESSOR_ARGS_FIRST(First, ...) First

#define PREPROCESSOR_ARGS_FIRST_REMOVE(First, ...) __VA_ARGS__

#define PREPROCESSOR_ARGS_SECOND(First, Second, ...) Second

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Preprocessor/Boolean.h>
#include <Preprocessor/If.h>
#include <Preprocessor/Tuple.h>

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define PREPROCESSOR_ARGS_FIRST_OR_EMPTY_IMPL(...) \
	PREPROCESSOR_IF(PREPROCESSOR_ARGS_NOT_EMPTY(__VA_ARGS__), \
		PREPROCESSOR_ARGS_FIRST(__VA_ARGS__, PREPROCESSOR_EMPTY_SYMBOL()), \
		PREPROCESSOR_EMPTY_SYMBOL())

#define PREPROCESSOR_ARGS_FIRST_OR_EMPTY(...) \
	PREPROCESSOR_IF(PREPROCESSOR_ARGS_NOT_EMPTY(__VA_ARGS__), \
		PREPROCESSOR_ARGS_FIRST_OR_EMPTY_IMPL(__VA_ARGS__), \
		PREPROCESSOR_EMPTY_SYMBOL())

#if COMPILER_TYPE == COMPILER_GNUC

#	define PREPROCESSOR_ARGS_SECOND_OR_EMPTY_IMPL(...) \
		PREPROCESSOR_IF(PREPROCESSOR_ARGS_NOT_EMPTY(__VA_ARGS__), \
			PREPROCESSOR_ARGS_FIRST(__VA_ARGS__), \
		    PREPROCESSOR_EMPTY_SYMBOL())

#	define PREPROCESSOR_ARGS_SECOND_OR_EMPTY(...) \
		PREPROCESSOR_IF(PREPROCESSOR_ARGS_AT_LEAST_TWO(__VA_ARGS__), \
			PREPROCESSOR_ARGS_SECOND_OR_EMPTY_IMPL(PREPROCESSOR_ARGS_FIRST_REMOVE(__VA_ARGS__)), \
			PREPROCESSOR_EMPTY_SYMBOL())

#	define PREPROCESSOR_ARGS_AT_LEAST_TWO_IMPL_I(First, ...) \
		PREPROCESSOR_ARGS_NOT_EMPTY(__VA_ARGS__)

#	define PREPROCESSOR_ARGS_AT_LEAST_TWO_IMPL(...) \
		PREPROCESSOR_ARGS_AT_LEAST_TWO_IMPL_I(__VA_ARGS__)

#	define PREPROCESSOR_ARGS_AT_LEAST_TWO(...) \
		PREPROCESSOR_IF(PREPROCESSOR_ARGS_NOT_EMPTY(__VA_ARGS__), \
			PREPROCESSOR_ARGS_AT_LEAST_TWO_IMPL(__VA_ARGS__), \
			0)

#elif COMPILER_TYPE == COMPILER_MSVC

#	define PREPROCESSOR_ARGS_SECOND_OR_EMPTY_IMPL(...) \
		PREPROCESSOR_IF(PREPROCESSOR_ARGS_NOT_EMPTY(__VA_ARGS__), \
		PREPROCESSOR_ARGS_FIRST(__VA_ARGS__, PREPROCESSOR_EMPTY_SYMBOL()), \
		PREPROCESSOR_EMPTY_SYMBOL())

#	define PREPROCESSOR_ARGS_SECOND_OR_EMPTY(...) \
		PREPROCESSOR_IF(PREPROCESSOR_ARGS_AT_LEAST_TWO(__VA_ARGS__), \
		PREPROCESSOR_ARGS_SECOND_OR_EMPTY_IMPL( \
			PREPROCESSOR_ARGS_FIRST_REMOVE(__VA_ARGS__, PREPROCESSOR_EMPTY_SYMBOL()) \
		), \
		PREPROCESSOR_EMPTY_SYMBOL())

#	define PREPROCESSOR_ARGS_AT_LEAST_TWO_IMPL_I(First, ...) \
		PREPROCESSOR_ARGS_NOT_EMPTY(__VA_ARGS__)

#	define PREPROCESSOR_ARGS_AT_LEAST_TWO_IMPL(...) \
		PREPROCESSOR_ARGS_AT_LEAST_TWO_IMPL_I(\
			PREPROCESSOR_IF(PREPROCESSOR_ARGS_NOT_EMPTY(__VA_ARGS__), \
				__VA_ARGS__, \
				PREPROCESSOR_TUPLE_PREPEND(~, __VA_ARGS__) \
			) \
		)

#	define PREPROCESSOR_ARGS_AT_LEAST_TWO(...) \
		PREPROCESSOR_IF(PREPROCESSOR_ARGS_NOT_EMPTY(__VA_ARGS__), \
			PREPROCESSOR_ARGS_AT_LEAST_TWO_IMPL((__VA_ARGS__)), \
			0)

#endif

#define PREPROCESSOR_ARGS_NOT_EMPTY_IMPL(...) \
	PREPROCESSOR_ARGS_EMPTY(__VA_ARGS__)

#define PREPROCESSOR_ARGS_NOT_EMPTY(...) \
	PREPROCESSOR_NOT(PREPROCESSOR_ARGS_EMPTY(__VA_ARGS__))

#define PREPROCESSOR_ARGS_EMPTY_CASE_IMPL_0001 ,

#define PREPROCESSOR_ARGS_EMPTY_CASE(_0, _1, _2, _3, _4) \
	_0 ## _1 ## _2 ## _3 ## _4

#define PREPROCESSOR_ARGS_EMPTY_IMPL_I(_0, _1, _2, _3) \
	PREPROCESSOR_ARGS_COMMA(PREPROCESSOR_ARGS_EMPTY_CASE(PREPROCESSOR_ARGS_EMPTY_CASE_IMPL_, _0, _1, _2, _3))

#define PREPROCESSOR_ARGS_EMPTY_IMPL(_0, _1, _2, _3) \
	PREPROCESSOR_ARGS_EMPTY_IMPL_I(_0, _1, _2, _3)

#define PREPROCESSOR_ARGS_EMPTY(...) \
	PREPROCESSOR_ARGS_EMPTY_IMPL( \
		PREPROCESSOR_ARGS_COMMA(__VA_ARGS__), \
		PREPROCESSOR_ARGS_COMMA(PREPROCESSOR_COMMA_VARIDIC __VA_ARGS__), \
		PREPROCESSOR_ARGS_COMMA(__VA_ARGS__ ()), \
		PREPROCESSOR_ARGS_COMMA(PREPROCESSOR_COMMA_VARIDIC __VA_ARGS__ ()) \
	)

#define PREPROCESSOR_ARGS_END_TOKEN() 0

#define PREPROCESSOR_ARG_N_IMPL( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
	_61, _62, _63, N, ...) N

#if COMPILER_TYPE == COMPILER_GNUC

#	define PREPROCESSOR_ARGS_COUNT_IMPL(...) \
		PREPROCESSOR_ARG_N_IMPL(__VA_ARGS__)

#	define PREPROCESSOR_ARGS_STRINGIFY_EMPTY_IMPL(...) # __VA_ARGS__

#	define PREPROCESSOR_ARGS_STRINGIFY_EMPTY(...) PREPROCESSOR_ARGS_STRINGIFY_EMPTY_IMPL(__VA_ARGS__)

#	define PREPROCESSOR_ARGS_COUNT(...) \
		(PREPROCESSOR_ARGS_COUNT_IMPL(__VA_ARGS__, PREPROCESSOR_ARGS_COUNT_SEQ_IMPL()) - \
		(sizeof(PREPROCESSOR_ARGS_STRINGIFY_EMPTY(__VA_ARGS__)) == 1))

#	define PREPROCESSOR_ARGS_COMMA(...) \
		PREPROCESSOR_ARGS_COUNT_IMPL(__VA_ARGS__, PREPROCESSOR_ARGS_COMMA_SEQ_IMPL())

#elif COMPILER_TYPE == COMPILER_MSVC

#	define PREPROCESSOR_ARGS_COUNT_PREFIX__PREPROCESSOR_ARGS_COUNT_POSTFIX ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,0

#	define PREPROCESSOR_ARGS_COUNT_IMPL(Args) PREPROCESSOR_ARG_N_IMPL Args

#	define PREPROCESSOR_ARGS_COUNT(...) \
		PREPROCESSOR_ARGS_COUNT_IMPL((PREPROCESSOR_ARGS_COUNT_PREFIX_ ## __VA_ARGS__ ## _PREPROCESSOR_ARGS_COUNT_POSTFIX, PREPROCESSOR_ARGS_COUNT_SEQ_IMPL()))

#	define PREPROCESSOR_ARGS_COMMA(...) \
		PREPROCESSOR_ARGS_COUNT_IMPL((PREPROCESSOR_ARGS_COUNT_PREFIX_ ## __VA_ARGS__ ## _PREPROCESSOR_ARGS_COUNT_POSTFIX, PREPROCESSOR_ARGS_COMMA_SEQ_IMPL()))

#endif

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PREPROCESSOR_ARGS_LAST_SEQ_IMPL() \
	PREPROCESSOR_ARGS_LAST_IMPL_63, PREPROCESSOR_ARGS_LAST_IMPL_62, PREPROCESSOR_ARGS_LAST_IMPL_61, PREPROCESSOR_ARGS_LAST_IMPL_60, \
	PREPROCESSOR_ARGS_LAST_IMPL_59, PREPROCESSOR_ARGS_LAST_IMPL_58, PREPROCESSOR_ARGS_LAST_IMPL_57, PREPROCESSOR_ARGS_LAST_IMPL_56, \
	PREPROCESSOR_ARGS_LAST_IMPL_55, PREPROCESSOR_ARGS_LAST_IMPL_54, PREPROCESSOR_ARGS_LAST_IMPL_53, PREPROCESSOR_ARGS_LAST_IMPL_52, \
	PREPROCESSOR_ARGS_LAST_IMPL_51, PREPROCESSOR_ARGS_LAST_IMPL_50, PREPROCESSOR_ARGS_LAST_IMPL_49, PREPROCESSOR_ARGS_LAST_IMPL_48, \
	PREPROCESSOR_ARGS_LAST_IMPL_47, PREPROCESSOR_ARGS_LAST_IMPL_46, PREPROCESSOR_ARGS_LAST_IMPL_45, PREPROCESSOR_ARGS_LAST_IMPL_44, \
	PREPROCESSOR_ARGS_LAST_IMPL_43, PREPROCESSOR_ARGS_LAST_IMPL_42, PREPROCESSOR_ARGS_LAST_IMPL_41, PREPROCESSOR_ARGS_LAST_IMPL_40, \
	PREPROCESSOR_ARGS_LAST_IMPL_39, PREPROCESSOR_ARGS_LAST_IMPL_38, PREPROCESSOR_ARGS_LAST_IMPL_37, PREPROCESSOR_ARGS_LAST_IMPL_36, \
	PREPROCESSOR_ARGS_LAST_IMPL_35, PREPROCESSOR_ARGS_LAST_IMPL_34, PREPROCESSOR_ARGS_LAST_IMPL_33, PREPROCESSOR_ARGS_LAST_IMPL_32, \
	PREPROCESSOR_ARGS_LAST_IMPL_31, PREPROCESSOR_ARGS_LAST_IMPL_30, PREPROCESSOR_ARGS_LAST_IMPL_29, PREPROCESSOR_ARGS_LAST_IMPL_28, \
	PREPROCESSOR_ARGS_LAST_IMPL_27, PREPROCESSOR_ARGS_LAST_IMPL_26, PREPROCESSOR_ARGS_LAST_IMPL_25, PREPROCESSOR_ARGS_LAST_IMPL_24, \
	PREPROCESSOR_ARGS_LAST_IMPL_23, PREPROCESSOR_ARGS_LAST_IMPL_22, PREPROCESSOR_ARGS_LAST_IMPL_21, PREPROCESSOR_ARGS_LAST_IMPL_20, \
	PREPROCESSOR_ARGS_LAST_IMPL_19, PREPROCESSOR_ARGS_LAST_IMPL_18, PREPROCESSOR_ARGS_LAST_IMPL_17, PREPROCESSOR_ARGS_LAST_IMPL_16, \
	PREPROCESSOR_ARGS_LAST_IMPL_15, PREPROCESSOR_ARGS_LAST_IMPL_14, PREPROCESSOR_ARGS_LAST_IMPL_13, PREPROCESSOR_ARGS_LAST_IMPL_12, \
	PREPROCESSOR_ARGS_LAST_IMPL_11, PREPROCESSOR_ARGS_LAST_IMPL_10, PREPROCESSOR_ARGS_LAST_IMPL_9, PREPROCESSOR_ARGS_LAST_IMPL_8, \
	PREPROCESSOR_ARGS_LAST_IMPL_7, PREPROCESSOR_ARGS_LAST_IMPL_6, PREPROCESSOR_ARGS_LAST_IMPL_5, PREPROCESSOR_ARGS_LAST_IMPL_4, \
	PREPROCESSOR_ARGS_LAST_IMPL_3, PREPROCESSOR_ARGS_LAST_IMPL_2, PREPROCESSOR_ARGS_LAST_IMPL_1, PREPROCESSOR_ARGS_LAST_IMPL_0

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define PREPROCESSOR_ARGS_LAST_IMPL_0()

#define PREPROCESSOR_ARGS_LAST_IMPL_1( \
	_1) _1

#define PREPROCESSOR_ARGS_LAST_IMPL_2( \
	_1, _2) _2

#define PREPROCESSOR_ARGS_LAST_IMPL_3( \
	_1, _2, _3) _3

#define PREPROCESSOR_ARGS_LAST_IMPL_4( \
	_1, _2, _3, _4) _4

#define PREPROCESSOR_ARGS_LAST_IMPL_5( \
	_1, _2, _3, _4, _5) _5

#define PREPROCESSOR_ARGS_LAST_IMPL_6( \
	_1, _2, _3, _4, _5, _6) _6

#define PREPROCESSOR_ARGS_LAST_IMPL_7( \
	_1, _2, _3, _4, _5, _6, _7) _7

#define PREPROCESSOR_ARGS_LAST_IMPL_8( \
	_1, _2, _3, _4, _5, _6, _7, _8) _8

#define PREPROCESSOR_ARGS_LAST_IMPL_9( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9) _9

#define PREPROCESSOR_ARGS_LAST_IMPL_10( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10) _10

#define PREPROCESSOR_ARGS_LAST_IMPL_11( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11) _11

#define PREPROCESSOR_ARGS_LAST_IMPL_12( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12) _12

#define PREPROCESSOR_ARGS_LAST_IMPL_13( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13) _13

#define PREPROCESSOR_ARGS_LAST_IMPL_14( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14) _14

#define PREPROCESSOR_ARGS_LAST_IMPL_15( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15) _15

#define PREPROCESSOR_ARGS_LAST_IMPL_16( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16) _16

#define PREPROCESSOR_ARGS_LAST_IMPL_17( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17) _17

#define PREPROCESSOR_ARGS_LAST_IMPL_18( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18) _18

#define PREPROCESSOR_ARGS_LAST_IMPL_19( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19) _19

#define PREPROCESSOR_ARGS_LAST_IMPL_20( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20) _20

#define PREPROCESSOR_ARGS_LAST_IMPL_21( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21) _21

#define PREPROCESSOR_ARGS_LAST_IMPL_22( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22) _22

#define PREPROCESSOR_ARGS_LAST_IMPL_23( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23) _23

#define PREPROCESSOR_ARGS_LAST_IMPL_24( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24) _24

#define PREPROCESSOR_ARGS_LAST_IMPL_25( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25) _25

#define PREPROCESSOR_ARGS_LAST_IMPL_26( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26) _26

#define PREPROCESSOR_ARGS_LAST_IMPL_27( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27) _27

#define PREPROCESSOR_ARGS_LAST_IMPL_28( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28) _28

#define PREPROCESSOR_ARGS_LAST_IMPL_29( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29) _29

#define PREPROCESSOR_ARGS_LAST_IMPL_30( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30) _30

#define PREPROCESSOR_ARGS_LAST_IMPL_31( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31) _31

#define PREPROCESSOR_ARGS_LAST_IMPL_32( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32) _32

#define PREPROCESSOR_ARGS_LAST_IMPL_33( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33) _33

#define PREPROCESSOR_ARGS_LAST_IMPL_34( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34) _34

#define PREPROCESSOR_ARGS_LAST_IMPL_35( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35) _35

#define PREPROCESSOR_ARGS_LAST_IMPL_36( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36) _36

#define PREPROCESSOR_ARGS_LAST_IMPL_37( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37) _37

#define PREPROCESSOR_ARGS_LAST_IMPL_38( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38) _38

#define PREPROCESSOR_ARGS_LAST_IMPL_39( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39) _39

#define PREPROCESSOR_ARGS_LAST_IMPL_40( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40) _40

#define PREPROCESSOR_ARGS_LAST_IMPL_41( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41) _41

#define PREPROCESSOR_ARGS_LAST_IMPL_42( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42) _42

#define PREPROCESSOR_ARGS_LAST_IMPL_43( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43) _43

#define PREPROCESSOR_ARGS_LAST_IMPL_44( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44) _44

#define PREPROCESSOR_ARGS_LAST_IMPL_45( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45) _45

#define PREPROCESSOR_ARGS_LAST_IMPL_46( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46) _46

#define PREPROCESSOR_ARGS_LAST_IMPL_47( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47) _47

#define PREPROCESSOR_ARGS_LAST_IMPL_48( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48) _48

#define PREPROCESSOR_ARGS_LAST_IMPL_49( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49) _49

#define PREPROCESSOR_ARGS_LAST_IMPL_50( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50) _50

#define PREPROCESSOR_ARGS_LAST_IMPL_51( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51) _51

#define PREPROCESSOR_ARGS_LAST_IMPL_52( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52) _52

#define PREPROCESSOR_ARGS_LAST_IMPL_53( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53) _53

#define PREPROCESSOR_ARGS_LAST_IMPL_54( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54) _54

#define PREPROCESSOR_ARGS_LAST_IMPL_55( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55) _55

#define PREPROCESSOR_ARGS_LAST_IMPL_56( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56) _56

#define PREPROCESSOR_ARGS_LAST_IMPL_57( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57) _57

#define PREPROCESSOR_ARGS_LAST_IMPL_58( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58) _58

#define PREPROCESSOR_ARGS_LAST_IMPL_59( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59) _59

#define PREPROCESSOR_ARGS_LAST_IMPL_60( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60) _60

#define PREPROCESSOR_ARGS_LAST_IMPL_61( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
	_61) _61

#define PREPROCESSOR_ARGS_LAST_IMPL_62( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
	_61, _62) _62

#define PREPROCESSOR_ARGS_LAST_IMPL_63( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
	_61, _62, _63) _63

#define PREPROCESSOR_ARGS_LAST_IMPL_64( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
	_61, _62, _63, _64) _64

#if COMPILER_TYPE == COMPILER_GNUC

#	define PREPROCESSOR_ARGS_LAST_IMPL(...) \
		PREPROCESSOR_ARG_N_IMPL(__VA_ARGS__)

#	define PREPROCESSOR_ARGS_LAST(...) \
		PREPROCESSOR_ARGS_LAST_IMPL(__VA_ARGS__, PREPROCESSOR_ARGS_LAST_SEQ_IMPL())(__VA_ARGS__)

#elif COMPILER_TYPE == COMPILER_MSVC

#	define PREPROCESSOR_ARGS_LAST_PREFIX__PREPROCESSOR_ARGS_LAST_POSTFIX ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,PREPROCESSOR_EMPTY_SYMBOL

#	define PREPROCESSOR_ARGS_LAST_IMPL(Args) PREPROCESSOR_ARG_N_IMPL Args

#	define PREPROCESSOR_ARGS_LAST(...) \
		PREPROCESSOR_ARGS_LAST_IMPL((PREPROCESSOR_ARGS_LAST_PREFIX_ ## __VA_ARGS__ ## _PREPROCESSOR_ARGS_LAST_POSTFIX, PREPROCESSOR_ARGS_LAST_SEQ_IMPL()))(__VA_ARGS__)

#endif

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PREPROCESSOR_ARGS_LAST_REMOVE_SEQ_IMPL() \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_63, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_62, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_61, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_60, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_59, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_58, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_57, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_56, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_55, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_54, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_53, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_52, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_51, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_50, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_49, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_48, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_47, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_46, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_45, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_44, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_43, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_42, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_41, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_40, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_39, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_38, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_37, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_36, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_35, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_34, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_33, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_32, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_31, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_30, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_29, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_28, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_27, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_26, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_25, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_24, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_23, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_22, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_21, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_20, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_19, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_18, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_17, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_16, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_15, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_14, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_13, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_12, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_11, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_10, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_9, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_8, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_7, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_6, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_5, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_4, \
	PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_3, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_2, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_1, PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_0

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_0()

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_1(_1)

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_2( \
	_1, _2) \
	_1

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_3( \
	_1, _2, _3) \
	_1, _2

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_4( \
	_1, _2, _3, _4) \
	_1, _2, _3

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_5( \
	_1, _2, _3, _4, _5) \
	_1, _2, _3, _4

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_6( \
	_1, _2, _3, _4, _5, _6) \
	_1, _2, _3, _4, _5

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_7( \
	_1, _2, _3, _4, _5, _6, _7) \
	_1, _2, _3, _4, _5, _6

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_8( \
	_1, _2, _3, _4, _5, _6, _7, _8) \
	_1, _2, _3, _4, _5, _6, _7

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_9( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9) \
	_1, _2, _3, _4, _5, _6, _7, _8

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_10( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_11( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
    _11) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10


#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_12( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_13( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_14( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_15( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_16( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_17( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_18( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_19( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_20( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_21( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
    _21) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20


#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_22( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_23( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_24( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_25( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_26( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_27( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_28( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_29( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_30( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_31( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
    _31) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30


#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_32( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_33( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_34( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_35( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_36( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_37( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_38( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_39( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_40( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_41( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
    _41) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_42( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_43( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_44( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_45( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_46( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_47( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_48( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_49( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_50( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_51( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
    _51) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50


#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_52( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_53( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_54( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_55( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_56( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_57( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_58( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_59( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_60( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_61( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
	_61) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60


#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_62( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
	_61, _62) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
	_61

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_63( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
	_61, _62, _63) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
	_61, _62

#define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_64( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
	_61, _62, _63, _64) \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
	_61, _62, _63

#if COMPILER_TYPE == COMPILER_GNUC

#	define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL(...) \
		PREPROCESSOR_ARG_N_IMPL(__VA_ARGS__)

#	define PREPROCESSOR_ARGS_LAST_REMOVE(...) \
		PREPROCESSOR_ARGS_LAST_REMOVE_IMPL(__VA_ARGS__, PREPROCESSOR_ARGS_LAST_REMOVE_SEQ_IMPL())(__VA_ARGS__)

#elif COMPILER_TYPE == COMPILER_MSVC

#	define PREPROCESSOR_ARGS_LAST_REMOVE_PREFIX__PREPROCESSOR_ARGS_LAST_REMOVE_POSTFIX ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,PREPROCESSOR_EMPTY_SYMBOL


#	define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL(Args) PREPROCESSOR_ARG_N_IMPL Args

#	define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_II(...) \
		PREPROCESSOR_ARGS_LAST_REMOVE_IMPL((PREPROCESSOR_ARGS_LAST_REMOVE_PREFIX_ ## __VA_ARGS__ ## _PREPROCESSOR_ARGS_LAST_REMOVE_POSTFIX, PREPROCESSOR_ARGS_LAST_REMOVE_SEQ_IMPL()))(__VA_ARGS__)

#	define PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_I(Expr) \
		PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_II Expr

#	define PREPROCESSOR_ARGS_LAST_REMOVE(...) \
		PREPROCESSOR_ARGS_LAST_REMOVE_IMPL_I((__VA_ARGS__))

#endif

#endif // PREPROCESSOR_ARGUMENTS_H
