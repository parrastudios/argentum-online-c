/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_MD5_H
#define SYSTEM_MD5_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// MD5 Context struct
////////////////////////////////////////////////////////////
struct MD5Context
{
  uint32 State[4];		///< State (ABCD)
  uint32 Count[2];		///< Number of bits, modulo 2^64 (lsb first)
  uint8  Buffer[64];	///< Input buffer
};

////////////////////////////////////////////////////////////
/// MD5 initialization.
/// Begins an MD5 operation, writing a new context
////////////////////////////////////////////////////////////
void MD5Init(struct MD5Context * Context);

////////////////////////////////////////////////////////////
/// MD5 block update operation. Continues an MD5 message-digest
/// operation, processing another message block, and updating the
/// context
////////////////////////////////////////////////////////////
void MD5Update(struct MD5Context * Context, unsigned char * Input, uinteger Len);

////////////////////////////////////////////////////////////
/// MD5 finalization. Ends an MD5 message-digest operation, writing the
/// the message digest and zeroizing the context
////////////////////////////////////////////////////////////
void MD5Final(unsigned char Digest[16], struct MD5Context * Context);

////////////////////////////////////////////////////////////
/// Computes the message digest for a specified file
////////////////////////////////////////////////////////////
void MD5File(char * File, unsigned char Digest[16]);

////////////////////////////////////////////////////////////
/// Digests a string and returns the result
////////////////////////////////////////////////////////////
void MD5String(unsigned char * String, unsigned char Digest[16]);

#endif // SYSTEM_MD5_H
