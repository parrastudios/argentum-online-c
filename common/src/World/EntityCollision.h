/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_ENTITY_COLLISION_H
#define WORLD_ENTITY_COLLISION_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Bounding/Box.h>
#include <Math/Geometry/Bounding/Sphere.h>
#include <Math/Geometry/Bounding/Frustum.h>

#include <World/Component.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define COMPONENT_TYPE_COLLISION_IMPL_BOX			0x00
#define COMPONENT_TYPE_COLLISION_IMPL_SPHERE		0x01
#define COMPONENT_TYPE_COLLISION_IMPL_FRUSTUM		0x02

#define COMPONENT_TYPE_COLLISION_IMPL_SIZE			0x03

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define ComponentCollisionTypeGetImpl(DataType)		DataType - COMPONENT_DATA_COLLISION - 1

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct EntityType;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Collision component derivable base data interface
////////////////////////////////////////////////////////////
struct ComponentCollisionData
{
	uint8						Impl[1];
	// todo: implement a callback for oncollide passing the list of entities ?
};

////////////////////////////////////////////////////////////
/// Collision box component data
////////////////////////////////////////////////////////////
struct ComponentCollisionBoxData
{
	struct BoundingBox			Box;
};

////////////////////////////////////////////////////////////
/// Collision sphere component data
////////////////////////////////////////////////////////////
struct ComponentCollisionSphereData
{
	struct BoundingSphere 		Sphere;
};

////////////////////////////////////////////////////////////
/// Collision frustum component data
////////////////////////////////////////////////////////////
struct ComponentCollisionFrustumData
{
	struct FrustumType			Frustum;
};

////////////////////////////////////////////////////////////
/// Collision box component type
////////////////////////////////////////////////////////////
struct ComponentCollisionBoxType
{
	//< Set the data of a bounding box
	void (*SetBounds)(struct EntityType * Entity, struct Vector3f * Min, struct Vector3f * Max);

	//< Set the data of a bounding box by its center and radius
	void (*SetRadius)(struct EntityType * Entity, struct Vector3f * Center, float Radius);

	//< Create a bounding box from a list of vertices
	void (*Generate)(struct EntityType * Entity, struct Vector3f * VertList, uinteger Size);
};

////////////////////////////////////////////////////////////
/// Collision sphere component type
////////////////////////////////////////////////////////////
struct ComponentCollisionSphereType
{
	//< Set the data of a bounding sphere
	void (*SetBounds)(struct EntityType * Entity, struct Vector3f * Center, float Radius);

	//< Set the radius of a bounding sphere
	void (*SetRadius)(struct EntityType * Entity, float Radius);

	//< Set the center of a bounding sphere
	void (*SetCenter)(struct EntityType * Entity, struct Vector3f * Center);
};

////////////////////////////////////////////////////////////
/// Collision frustum component type
////////////////////////////////////////////////////////////
struct ComponentCollisionFrustumType
{
	//< Update frustum using a view projection matrix
	void (*Update)(struct EntityType * Entity, struct Matrix4f * ViewProj);

	//< Update frustum using a view projection matrix extended
	void (*UpdateProjection)(struct EntityType * Entity, struct Matrix4f * View, struct Matrix4f * Proj, struct Matrix4f * ViewProj, float Depth);
};

////////////////////////////////////////////////////////////
/// Collision base component type
////////////////////////////////////////////////////////////
struct ComponentCollisionType
{
	//< Check if collides with another component
	bool (*Collide)(struct ComponentType * Collider, struct ComponentType * Target);

	//< Double dispatch for holding methods over primitives
	struct ComponentCollisionDoubleDispatchType
	{
		//< Check if collides with another component (double dispatch pattern)
		bool (*Collide[COMPONENT_TYPE_COLLISION_IMPL_SIZE])(struct ComponentType * Collider, struct ComponentCollisionData * Target);
	} DispatchImpl;

	//< Type implementation (primitives and derived tables)
	union ComponentCollisionTypeImpl
	{
		const struct ComponentCollisionBoxType *		Box;
		const struct ComponentCollisionSphereType *		Sphere;
		const struct ComponentCollisionFrustumType *	Frustum;
		const void *									Derived;
	} TypeImpl;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the entity collision generic implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentCollisionType * EntityComponentCollisionAttr();

////////////////////////////////////////////////////////////
/// Return the entity collision generic implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentCollisionImpl();

////////////////////////////////////////////////////////////
/// Create a bounding box component
////////////////////////////////////////////////////////////
struct EntityType * EntityCollisionBoxCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a bounding box component
////////////////////////////////////////////////////////////
void EntityCollisionBoxDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Return the entity collision box implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentCollisionType * EntityComponentCollisionBoxAttr();

////////////////////////////////////////////////////////////
/// Return the entity collision box implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentCollisionBoxImpl();

////////////////////////////////////////////////////////////
/// Create a bounding sphere component
////////////////////////////////////////////////////////////
struct EntityType * EntityCollisionSphereCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a bounding sphere component
////////////////////////////////////////////////////////////
void EntityCollisionSphereDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Return the entity collision sphere implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentCollisionType * EntityComponentCollisionSphereAttr();

////////////////////////////////////////////////////////////
/// Return the entity collision sphere implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentCollisionSphereImpl();

////////////////////////////////////////////////////////////
/// Create a frustum component
////////////////////////////////////////////////////////////
struct EntityType * EntityCollisionFrustumCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a frustum component
////////////////////////////////////////////////////////////
void EntityCollisionFrustumDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Return the entity collision frustum implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentCollisionType * EntityComponentCollisionFrustumAttr();

////////////////////////////////////////////////////////////
/// Return the entity collision frustum implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentCollisionFrustumImpl();

////////////////////////////////////////////////////////////
/// Check if entity collides with a collision data impl
////////////////////////////////////////////////////////////
bool EntityCollisionComponentCollide(struct EntityType * Entity, struct ComponentCollisionData * Collider, uint32 DataType);

// todo: what happens with EntityCollisionComponentCollide?

#endif // WORLD_ENTITY_COLLISION_H
