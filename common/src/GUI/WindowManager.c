/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <GUI/WindowManager.h>
#include <GUI/Clipboard.h>
#include <GUI/WindowParser.h>

#include <Audio/SoundManager.h>

#include <Math/Geometry/Rect.h>

#include <System/IOHelper.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct GuiWindowList	WindowList;					///< Window list
struct GuiWidget *		WidgetPressed = NULL;		///< Temp widget
bool					GuiKeyboardBlock = false;	///< Check if user is introducing input

////////////////////////////////////////////////////////////
/// Initialize the windows
////////////////////////////////////////////////////////////
void GuiInitialize(uinteger Template, char * Path)
{
	GuiEventHandlerInitialize();
	GuiParserInitialize(Template, Path);
}

////////////////////////////////////////////////////////////
/// Check if keyboard is being used on GUI
////////////////////////////////////////////////////////////
bool GuiKeyboardAvailable()
{
	return (GuiKeyboardBlock == false) && (WindowList.Focused == NULL);
}

////////////////////////////////////////////////////////////
// Add window to the list
////////////////////////////////////////////////////////////
void GuiAddWindow(struct GuiWindow * Window)
{
	if (WindowList.First == NULL)
	{
		WindowList.First = WindowList.Focused = Window;
		Window->Previous = NULL;
	}
	else
	{
		WindowList.Last->Next = Window;
		Window->Previous = WindowList.Last;
	}

	WindowList.Last = Window;
	Window->Next = NULL;
}

////////////////////////////////////////////////////////////
// Remove window from the list
////////////////////////////////////////////////////////////
void GuiRemoveWindow(struct GuiWindow * Window)
{
	if (Window->Previous == NULL)
		WindowList.First = Window->Next;
	else
		Window->Previous->Next = Window->Next;

	if (Window->Next == NULL)
		WindowList.Last = Window->Previous;
	else
		Window->Next->Previous = Window->Previous;

	GuiWindowDestroy(Window);
}

////////////////////////////////////////////////////////////
/// Get a window pointer by id
////////////////////////////////////////////////////////////
struct GuiWindow * GuiGetWindow(char * WindowID)
{
	struct GuiWindow * Iterator;

	for (Iterator = WindowList.First; Iterator != NULL; Iterator = Iterator->Next)
	{
		if (Iterator->First->Type == GUI_WIDGET_FORM)
		{
		    char * Name = Iterator->First->Name;

			if (strncmp(Name, WindowID, strlen(WindowID)) == 0)
			{
				return Iterator;
			}
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Open the window
////////////////////////////////////////////////////////////
void GuiOpenWindow(struct GuiWindow * Window)
{
	if (Window)
	{
		if (Window->hStatus != GUI_WINDOW_OPENED)
		{
			// Play sound
			SoundBufferPlay(GUI_FORM_SOUND_OPEN, false, 0.0f, 0.0f, 0.0f);

			// Add to the list
			if (Window->hStatus != GUI_WINDOW_CLOSED)
                GuiAddWindow(Window);

			// Open the window
			Window->hStatus = GUI_WINDOW_OPENED;

			// Set as focused
			WindowList.Focused = Window;

			if (Window->First)
			{
				if (Window->First->Open)
					Window->First->Open();
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Load the window on the list
////////////////////////////////////////////////////////////
void GuiLoadWindow(struct GuiWindow * Window)
{
    if (Window)
    {
        // Set the status to hide it
		Window->hStatus = GUI_WINDOW_CLOSED;

		// Add to the list
        GuiAddWindow(Window);
    }
}

////////////////////////////////////////////////////////////
/// Close the window
////////////////////////////////////////////////////////////
void GuiCloseWindow(struct GuiWindow * Window)
{
	if (Window)
	{
		if (Window->hStatus != GUI_WINDOW_CLOSED)
		{
			// Close the window
			Window->hStatus = GUI_WINDOW_CLOSED;

			// Play sound
			SoundBufferPlay(GUI_FORM_SOUND_CLOSE, false, 0.0f, 0.0f, 0.0f);

			if (Window->First)
			{
				if (Window->First->Close)
					Window->First->Close();
			}

			// Unfocus the window
			GuiLostFocusWindow(Window);
		}
	}
}

////////////////////////////////////////////////////////////
/// Lost focus of a window
////////////////////////////////////////////////////////////
void GuiLostFocusWindow(struct GuiWindow * Window)
{
	if (Window == WindowList.Focused)
	{
		struct GuiWindow * Iterator;

		// Set window focused
		for (Iterator = WindowList.First; Iterator != NULL; Iterator = Iterator->Next)
		{
			if (Iterator->hStatus == GUI_WINDOW_OPENED && Iterator != WindowList.Focused)
			{
				WindowList.Focused = Iterator;
				return;
			}
		}

		GuiLostFocusAll();
	}
}

////////////////////////////////////////////////////////////
/// Lost focus of all window
////////////////////////////////////////////////////////////
void GuiLostFocusAll()
{
	WindowList.Focused = NULL;
	GuiKeyboardBlock = false;
}


////////////////////////////////////////////////////////////
// Render all gui (renderer)
////////////////////////////////////////////////////////////
void GuiRender()
{
	if (WindowList.First)
	{
		struct GuiWindow * Iterator;

		// Render all forms
		for (Iterator = WindowList.First; Iterator != NULL; Iterator = Iterator->Next)
		{
			if (Iterator == WindowList.Focused)
			{
				Iterator = Iterator->Next;

				if (!Iterator)
					break;
			}

			if (Iterator->hStatus == GUI_WINDOW_OPENED && Iterator != WindowList.Focused)
				GuiWindowRender(Iterator, false);
		}

		// Render focused window
		if (WindowList.Focused && WindowList.Focused->hStatus == GUI_WINDOW_OPENED)
			GuiWindowRender(WindowList.Focused, true);
	}
}

////////////////////////////////////////////////////////////
// Update all gui (event handler)
////////////////////////////////////////////////////////////
void GuiUpdate(struct Event * EventData)
{
	struct GuiWidget * Iterator;
	bool EventComputed = false;

	// If there are any window
	if (WindowList.First && WindowList.Focused && WindowList.Focused->hStatus == GUI_WINDOW_OPENED)
	{
		// Check events
		if (EventData->Type)
		{
			switch (EventData->Type)
			{
				// Mouse events
				case MouseMoved :
					if (WidgetPressed)
					{
						if (WidgetPressed->Type == GUI_WIDGET_TEXTBOX)
						{
							GuiTextBoxSetSelection(WidgetPressed, EventData->MouseMove.X - WindowList.Focused->First->X, false);
						}
					}
					else
					{
						if ( RectContainsEx(WindowList.Focused->First->X,
											WindowList.Focused->First->Y,
											WindowList.Focused->First->X + WindowList.Focused->First->Width,
											WindowList.Focused->First->Y + WindowList.Focused->First->Height,
											EventData->MouseMove.X, EventData->MouseMove.Y) )
						{
							for (Iterator = WindowList.Focused->Last; Iterator != NULL; Iterator = Iterator->Previous)
							{
								if (Iterator->Type == GUI_WIDGET_BUTTON)
								{
									if (Iterator->Attributes.Button.ActionStatus != GUI_BUTTON_PRESSED)
										GuiButtonSetState(&Iterator->Attributes.Button, GUI_BUTTON_NORMAL);
								}

								if (EventComputed == false)
								{
									if ( RectContainsEx(WindowList.Focused->First->X + Iterator->X,
														WindowList.Focused->First->Y + Iterator->Y,
														WindowList.Focused->First->X + Iterator->X + Iterator->Width,
														WindowList.Focused->First->Y + Iterator->Y + Iterator->Height,
														EventData->MouseMove.X, EventData->MouseMove.Y) )
									{
										// Do the widget event
										if (Iterator->Type == GUI_WIDGET_BUTTON)
										{
											if (Iterator->Attributes.Button.ActionStatus != GUI_BUTTON_PRESSED)
												GuiButtonSetState(&Iterator->Attributes.Button, GUI_BUTTON_HOVERED);
										}

										if (Iterator->MouseMove)
											Iterator->MouseMove(&EventData->MouseMove);

										EventComputed = true;
									}
								}
							}
						}
					}

					return;

				case MouseButtonPressed :
					if ( RectContainsEx(WindowList.Focused->First->X,
										WindowList.Focused->First->Y,
										WindowList.Focused->First->X + WindowList.Focused->First->Width,
										WindowList.Focused->First->Y + WindowList.Focused->First->Height,
									    EventData->MouseButton.X, EventData->MouseButton.Y) )
					{
						for (Iterator = WindowList.Focused->Last; Iterator != NULL; Iterator = Iterator->Previous)
						{
							if ( RectContainsEx(WindowList.Focused->First->X + Iterator->X,
												WindowList.Focused->First->Y + Iterator->Y,
												WindowList.Focused->First->X + Iterator->X + Iterator->Width,
												WindowList.Focused->First->Y + Iterator->Y + Iterator->Height,
												EventData->MouseButton.X, EventData->MouseButton.Y) )
							{
								// Do the widget event
								switch (Iterator->Type)
								{
									case GUI_WIDGET_BUTTON :
										SoundBufferPlay(GUI_BUTTON_SOUND_1, false, 0.0f, 0.0f, 0.0f);
										GuiButtonSetState(&Iterator->Attributes.Button, GUI_BUTTON_PRESSED);
										GuiKeyboardBlock = false;
										break;

                                    case GUI_WIDGET_TEXTBOX :
                                        GuiTextBoxSetCurrentPos(Iterator, (EventData->MouseButton.X - WindowList.Focused->First->X));
                                        GuiKeyboardBlock = true;
										break;

									case GUI_WIDGET_LIST :
										GuiListSetItemSelected(Iterator,
											EventData->MouseButton.Y - WindowList.Focused->First->Y - Iterator->Y);

										if (GuiListItemChanged(Iterator))
										{
											if (Iterator->ButtonReleased)
												Iterator->ButtonReleased(&EventData->MouseButton);
										}

										GuiKeyboardBlock = true;

										break;

									default :
										GuiKeyboardBlock = false;
										break;
								}

								if (Iterator->ButtonPressed)
									Iterator->ButtonPressed(&EventData->MouseButton);

								WindowList.Focused->FocusWidget = WidgetPressed = Iterator;

								return;
							}
						}
					}
					else
					{
						struct GuiWindow * WindowItr;

						for (WindowItr = WindowList.First; WindowItr != NULL; WindowItr = WindowItr->Next)
						{
							if ( WindowItr->hStatus == GUI_WINDOW_OPENED &&
								 RectContainsEx(WindowItr->First->X,
												WindowItr->First->Y,
												WindowItr->First->X + WindowItr->First->Width,
												WindowItr->First->Y + WindowItr->First->Height,
												EventData->MouseButton.X, EventData->MouseButton.Y) )
							{
								if (WindowItr->First->Enabled)
								{
									WindowList.Focused = WindowItr;

									// Play sound
									SoundBufferPlay(GUI_FORM_SOUND_FOCUS, false, 0.0f, 0.0f, 0.0f);
									return;
								}
							}
						}
					}

					return;

				case MouseButtonReleased :
					if (WidgetPressed && WidgetPressed->Enabled)
					{
						// Do the widget event
						if (WidgetPressed->Type == GUI_WIDGET_BUTTON)
						{
							GuiButtonSetState(&WidgetPressed->Attributes.Button, GUI_BUTTON_HOVERED);
						}
						else if (WidgetPressed->Type == GUI_WIDGET_TEXTBOX)
						{
							GuiKeyboardBlock = true;
							GuiTextBoxSetSelection(WidgetPressed, EventData->MouseButton.X - WindowList.Focused->First->X, true);
						}

						if (WidgetPressed->ButtonReleased)
							WidgetPressed->ButtonReleased(&EventData->MouseButton);

						WidgetPressed = NULL;

						return;
					}

					return;


				// Keyboard events
				case TextEntered :
					if (EventData->Key.Code == Tab)
					{
						struct GuiWidget * Iterator;

						if (WindowList.Focused->FocusWidget->Type == GUI_WIDGET_TEXTBOX)
						{
							if (WindowList.Focused->FocusWidget->Next != NULL)
							{
								WindowList.Focused->FocusWidget = WindowList.Focused->FocusWidget->Next;
							}
						}

						Iterator = WindowList.Focused->FocusWidget;

						for (WindowList.Focused->FocusWidget = NULL; Iterator != NULL; Iterator = Iterator->Next)
						{
							if (Iterator->Type == GUI_WIDGET_TEXTBOX && Iterator->Enabled)
							{
								WindowList.Focused->FocusWidget = Iterator;
								GuiKeyboardBlock = true;
								break;
							}
						}

						if (WindowList.Focused->FocusWidget == NULL)
						{
							WindowList.Focused->FocusWidget = WindowList.Focused->First;
						}
					}

					// Send the event
					if (WindowList.Focused->FocusWidget->TextEntered)
						WindowList.Focused->FocusWidget->TextEntered(&EventData->Text);

					// Auto focus
					if (WindowList.Focused->FocusWidget->Type == GUI_WIDGET_FORM)
					{
						struct GuiWidget * Iterator;

						for (Iterator = WindowList.Focused->First->Next; Iterator != NULL; Iterator = Iterator->Next)
						{
							if (Iterator->Type == GUI_WIDGET_TEXTBOX)
							{
								WindowList.Focused->FocusWidget = Iterator;
								GuiKeyboardBlock = true;
								break;
							}
						}
					}

					if (WindowList.Focused->FocusWidget->Type == GUI_WIDGET_TEXTBOX)
					{
						GuiKeyboardBlock = true;

						// Update the text
						if (EventData->Text.Unicode >= 0x20 && EventData->Text.Unicode < 0x7F)
						{
							if (WindowList.Focused->FocusWidget->Attributes.TextBox.SelectionPos != GUI_TEXTBOX_NOT_SELECTED)
							{
								GuiTextBoxEraseSelection(WindowList.Focused->FocusWidget);
							}

							GuiTextBoxAddChar(WindowList.Focused->FocusWidget, EventData->Text.Unicode);
							return;
						}

						if (EventData->Text.Unicode == 0x08)
						{
							GuiTextBoxRemoveChar(WindowList.Focused->FocusWidget);
							return;
						}
					}

					return;

				case KeyPressed :
					// Send the event
					if (WindowList.Focused->FocusWidget->KeyPressed)
						WindowList.Focused->FocusWidget->KeyPressed(&EventData->Key);

					if (WindowList.Focused->FocusWidget->Type == GUI_WIDGET_TEXTBOX)
					{
						struct GuiWidget * Widget = WindowList.Focused->FocusWidget;
						struct GuiTextBox * TextBox = &Widget->Attributes.TextBox;

						if (EventData->Key.Code == Left)
						{
							if (EventData->Key.Shift)
							{
								if (TextBox->SelectionPos == GUI_TEXTBOX_NOT_SELECTED)
								{
									TextBox->SelectionPos = TextBox->CurrentPos;
								}

								if (TextBox->CurrentPos > 0)
								{
									TextBox->CurrentPos--;
								}
							}
							else
							{
								if (TextBox->SelectionPos != GUI_TEXTBOX_NOT_SELECTED)
								{
									TextBox->SelectionPos =	GUI_TEXTBOX_NOT_SELECTED;
								}
								else
								{
									if (TextBox->CurrentPos > 0)
									{
										TextBox->CurrentPos--;
									}
								}
							}
						}
						else if (EventData->Key.Code == Right)
						{
							if (EventData->Key.Shift)
							{
								if (TextBox->SelectionPos == GUI_TEXTBOX_NOT_SELECTED)
								{
									TextBox->SelectionPos = TextBox->CurrentPos;
								}

								if (TextBox->CurrentPos != Widget->Length)
								{
									TextBox->CurrentPos++;
								}
							}
							else
							{
								if (TextBox->SelectionPos != GUI_TEXTBOX_NOT_SELECTED)
								{
									TextBox->SelectionPos =	GUI_TEXTBOX_NOT_SELECTED;
								}
								else
								{
									if (TextBox->CurrentPos != Widget->Length)
									{
										TextBox->CurrentPos++;
									}
								}
							}
						}
					}

					return;

				case KeyReleased :
					// Send the event
					if (WindowList.Focused->FocusWidget->KeyReleased)
						WindowList.Focused->FocusWidget->KeyReleased(&EventData->Key);

					// Clipboard events
					if (EventData->Key.Control)
					{
						if (EventData->Key.Code == C)
						{
							switch (WindowList.Focused->FocusWidget->Type)
							{
								case GUI_WIDGET_TEXTBOX :
									GuiTextBoxCopySelection(WindowList.Focused->FocusWidget);
									return;

								case GUI_WIDGET_LIST :
									GuiClipboardSet(GuiListGetItemSelected(&WindowList.Focused->FocusWidget->Attributes.List),
													strlen(GuiListGetItemSelected(&WindowList.Focused->FocusWidget->Attributes.List)));
									return;

								default :
									return;
							}
						}

						if (EventData->Key.Code == X)
						{
							switch (WindowList.Focused->FocusWidget->Type)
							{
								case GUI_WIDGET_TEXTBOX :
									GuiTextBoxCutSelection(WindowList.Focused->FocusWidget);
									return;

								default :
									return;
							}
						}

						if (EventData->Key.Code == V)
						{
							switch (WindowList.Focused->FocusWidget->Type)
							{
								case GUI_WIDGET_TEXTBOX :
									GuiTextBoxPasteSelection(WindowList.Focused->FocusWidget);
									return;

								default :
									return;
							}
						}
					}

					return;

				// Another else
				default : return;
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Destroy all windows
////////////////////////////////////////////////////////////
void GuiDestroy()
{
	struct GuiWindow * Iterator = WindowList.First;

	while (Iterator != NULL)
	{
		struct GuiWindow * Next = Iterator->Next;

		// Destroy it
		GuiWindowDestroy(Iterator);

		// Go to the next
		Iterator = Next;
	}

	// Set focused to null
	WindowList.Focused = NULL;

	// Destroy all events
	GuiEventHandlerDestroy();

	// Destroy the parser
	GuiParserDestroy();
}
