/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Projection.h>
#include <Graphics/Device.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get device values for picking
////////////////////////////////////////////////////////////
void PickingGetDeviceValues(struct Rect * ViewPortRect, struct Matrix4f * ModelViewMatrix, struct Matrix4f * ProjectionMatrix)
{
	if (ViewPortRect && ModelViewMatrix && ProjectionMatrix)
	{
		uinteger i;
		GLint ViewPort[4];
		GLfloat ModelView[16], Projection[16];

		// Get viewport and matrix OpenGL values
		glGetIntegerv(GL_VIEWPORT, ViewPort);
		glGetFloatv(GL_MODELVIEW_MATRIX, ModelView);
		glGetFloatv(GL_PROJECTION_MATRIX, Projection);

		// Copy into math representation
		ViewPortRect->Left = ViewPort[0];
		ViewPortRect->Top = ViewPort[1];
		ViewPortRect->Right = ViewPort[2];
		ViewPortRect->Bottom = ViewPort[3];

		for (i = 0; i < 16; ++i)
		{
			ModelViewMatrix->v[i] = ModelView[i];
			ProjectionMatrix->v[i] = Projection[i];
		}
	}
}

////////////////////////////////////////////////////////////
/// Project a point from 3D space into 2D view port as a pixel
////////////////////////////////////////////////////////////
void PickingGetPixelFromPosition(struct Vector3f * Position, struct Vector2i * Pixel)
{
	if (Position && Pixel)
	{
		struct Rect ViewPortRect;
		struct Matrix4f ModelViewMatrix, ProjectionMatrix;
		struct Vector3f Result;

		// Get viewport and matrix OpenGL values
		PickingGetDeviceValues(&ViewPortRect, &ModelViewMatrix, &ProjectionMatrix);

		// Project the point
		if (ProjectionProjectPoint(Position, &ModelViewMatrix, &ProjectionMatrix, &ViewPortRect, &Result))
		{
			// Reverse y since OpenGL has y origin at bottom, but window has y origin at top
			Pixel->x = (integer)Result.x - ViewPortRect.Left;
			Pixel->y = ViewPortRect.Top + ViewPortRect.Bottom - (integer)Result.y;
		}
		else
		{
			Vector2iSetNull(Pixel);
		}
	}
}

////////////////////////////////////////////////////////////
/// Unproject a pixel from 2D view port into 3D space as a point
////////////////////////////////////////////////////////////
void PickingGetPositionFromPixel(struct Vector2i * Pixel, struct Vector3f * Position)
{
	if (Pixel && Position)
	{
		struct Rect ViewPortRect;
		struct Matrix4f ModelViewMatrix, ProjectionMatrix;
		struct Vector3f PixelPosition;
		struct Vector2i ViewPortPixel;

		// Get viewport and matrix OpenGL values
		PickingGetDeviceValues(&ViewPortRect, &ModelViewMatrix, &ProjectionMatrix);

		// Calculate viewport pixel
		ViewPortPixel.x = Pixel->x;
		ViewPortPixel.y = ViewPortRect.Bottom - Pixel->y;

		// Retreive the pixel position
		PixelPosition.x = (float)ViewPortPixel.x;
		PixelPosition.y = (float)ViewPortPixel.y;
		PixelPosition.z = 0.0f;

		// Read depth buffer
		glReadPixels(ViewPortPixel.x, ViewPortPixel.y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &PixelPosition.z);

		// Unproject the point
		if (!ProjectionUnProjectPoint(&PixelPosition, &ModelViewMatrix, &ProjectionMatrix, &ViewPortRect, Position))
		{
			Vector3fSetNull(Position);
		}
	}
}
