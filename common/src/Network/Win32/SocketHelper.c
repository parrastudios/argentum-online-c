/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Network/SocketHelper.h>

////////////////////////////////////////////////////////////
/// Windows needs some initialization and cleanup to get
/// sockets working properly..
////////////////////////////////////////////////////////////
void SocketHelperInitialize()
{
	WSADATA InitData;
	WSAStartup(MAKEWORD(2,2), &InitData);
}

////////////////////////////////////////////////////////////
/// Clean...
////////////////////////////////////////////////////////////
void SocketHelperCleanup()
{
	WSACleanup();
}

////////////////////////////////////////////////////////////
/// Return the value of the invalid socket
////////////////////////////////////////////////////////////
SocketType InvalidSocket()
{
    return INVALID_SOCKET;
}


////////////////////////////////////////////////////////////
/// Close / destroy a socket
////////////////////////////////////////////////////////////
bool SocketHelperClose(SocketType Socket)
{
    return (closesocket(Socket) != -1);
}


////////////////////////////////////////////////////////////
/// Set a socket as blocking or non-blocking
////////////////////////////////////////////////////////////
void SocketHelperSetBlocking(SocketType Socket, bool Block)
{
    unsigned long Blocking = (Block ? 0 : 1);
    ioctlsocket(Socket, FIONBIO, &Blocking);
}


////////////////////////////////////////////////////////////
/// Get the last socket error status
////////////////////////////////////////////////////////////
enum SocketStatus SocketHelperGetErrorStatus()
{
    switch (WSAGetLastError())
    {
        case WSAEWOULDBLOCK :	return NotReady;
        case WSAECONNABORTED :	return Disconnected;
        case WSAECONNRESET :	return Disconnected;
        case WSAETIMEDOUT :		return Disconnected;
        case WSAENETRESET :		return Disconnected;
        case WSAENOTCONN :		return Disconnected;
        default :				return Error;
    }
}
