/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PORTABILITY_GENERAL_H
#define PORTABILITY_GENERAL_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Debug.h>
#include <Portability/Assertion.h>
#include <Portability/Compiler.h>
#include <Portability/Platform.h>
#include <Portability/Language.h>
#include <Portability/Architecture.h>
#include <Portability/Assembly.h>
#include <Portability/CallingConvention.h>
#include <Portability/VariableArguments.h>
#include <Portability/Qualifier.h>
#include <Portability/Thread.h>
#include <Portability/Type.h>
#include <Portability/Endianness.h>
#include <Portability/Casting.h>

#endif // PORTABILITY_GENERAL_H
