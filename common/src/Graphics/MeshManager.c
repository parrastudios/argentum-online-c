/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2016 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/MeshManager.h>
#include <Graphics/Mesh.h>
#include <Graphics/ShaderProgram.h>
#include <Graphics/ShaderManager.h>
#include <Graphics/Texture.h>
#include <Graphics/TextureLoader.h>
#include <Graphics/Device.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define MESH_MANAGER_INDEX_INVALID SET_HASH_INVALID

////////////////////////////////////////////////////////////
// Enumerations
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Generate a hash for a mesh
////////////////////////////////////////////////////////////
MeshIdType MeshManagerHash(void * MeshPtr)
{
	if (MeshPtr)
	{
		struct MeshType * Mesh = MeshPtr;

		return (MeshIdType)Mesh->Index;
	}

	return MESH_MANAGER_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Compare a pair of meshes
////////////////////////////////////////////////////////////
OperatorType MeshManagerCompare(void * Left, void * Right)
{
	struct MeshType * LeftMesh = Left;
	struct MeshType * RightMesh = Right;

	return (LeftMesh->Index == RightMesh->Index) ? OPERATOR_EQUAL : OPERATOR_NOT_EQUAL;
}

////////////////////////////////////////////////////////////
/// Initialize mesh manager
////////////////////////////////////////////////////////////
bool MeshManagerInitialize(struct MeshManagerType * MeshManager)
{
	if (MeshManager)
	{
		// Create free slot list
		MeshManager->EmptySlots = ListNewT(uinteger);

		// Create mesh list
		MeshManager->MeshList = SetCreate(sizeof(struct MeshType), &MeshManagerHash, &MeshManagerCompare);

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Insert a mesh in the manager
////////////////////////////////////////////////////////////
MeshIdType MeshManagerInsert(struct MeshManagerType * MeshManager, struct MeshType * Mesh)
{
	MeshIdType MeshIndex = MESH_MANAGER_INDEX_INVALID;

	if (MeshManager && Mesh)
	{
		bool Cached = false;

		// Retrieve a valid id
		if (!ListEmpty(MeshManager->EmptySlots))
		{
			MeshIndex = ListFrontT(MeshManager->EmptySlots, MeshIdType);

			Cached = true;
		}
		else
		{
			MeshIndex = SetGetSize(MeshManager->MeshList);
		}

		// Set mesh index
		Mesh->Index = MeshIndex;

		// Insert into the set
		if (SetInsert(MeshManager->MeshList, (void *)Mesh))
		{
			// Remove cached index
			if (Cached)
			{
				ListPopFront(MeshManager->EmptySlots);
			}
		}
		else
		{
			Mesh->Index = MeshIndex = MESH_MANAGER_INDEX_INVALID;
		}
	}

	return MeshIndex;
}

////////////////////////////////////////////////////////////
/// Get a mesh from the manager
////////////////////////////////////////////////////////////
struct MeshType * MeshManagerGet(struct MeshManagerType * MeshManager, MeshIdType MeshId)
{
	if (MeshManager)
	{
		// Get mesh from list
		return SetGetValueByHash(MeshManager->MeshList, MeshId);
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Iterate through a mesh list of the manager
////////////////////////////////////////////////////////////
void MeshManagerForEach(struct MeshManagerType * MeshManager, MeshManagerIteratorCallback Callback)
{
	if (MeshManager && Callback)
	{
		struct SetIteratorType Iterator;

		// Iterate through all meshes
		SetForEach(MeshManager->MeshList, &Iterator)
		{
			struct MeshType * Mesh = (struct MeshType *)SetIteratorValue(&Iterator);

			// Execute callback
			if (Mesh)
			{
				Callback(Mesh);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Iterate through a mesh id list in the manager
////////////////////////////////////////////////////////////
void MeshManagerForEachList(struct MeshManagerType * MeshManager, MeshManagerIteratorCallback Callback, Vector MeshIdList)
{
	if (MeshManager && Callback && MeshIdList)
	{
		uinteger i;

		for (i = 0; i < VectorSize(MeshIdList); ++i)
		{
			// Retrieve iterator data
			uinteger Index = VectorAtT(MeshIdList, i, uinteger);

			// Retrieve mesh
			struct MeshType * Mesh = MeshManagerGet(MeshManager, Index);

			// Execute callback
			if (Mesh)
			{
				Callback(Mesh);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Remove a mesh from the manager
////////////////////////////////////////////////////////////
void MeshManagerRemove(struct MeshManagerType * MeshManager, MeshIdType MeshId)
{
	if (MeshManager)
	{
		// Remove mesh from the list
		SetRemoveByHash(MeshManager->MeshList, MeshId);

		// Insert index into the free list
		ListPushBack(MeshManager->EmptySlots, (void *)&MeshId);
	}
}

////////////////////////////////////////////////////////////
/// Destroy mesh manager
////////////////////////////////////////////////////////////
void MeshManagerDestroy(struct MeshManagerType * MeshManager)
{
	if (MeshManager)
	{
		// Destroy free slot list
		ListDestroy(MeshManager->EmptySlots);

		// Destroy mesh list
		SetDestroy(MeshManager->MeshList);
	}
}
