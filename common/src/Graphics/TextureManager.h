/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_TEXTURE_MANAGER_H
#define GRAPHICS_TEXTURE_MANAGER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <Graphics/Color.h>
#include <Math/Geometry/Rect.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define TEXTURE_CACHE_SIZE	0xFFFF	///< Texture control max number
#define TEXTURE_LOAD_SIZE	0xFF	///< Texture memory max size

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct TextureCache
{
	int32 	ID;			///< Texture ID
	float 	LastUsed;	///< Texture last used
	bool	Remove;		///< If the texture can be deleted or not
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Returns the texture loaded into memory
////////////////////////////////////////////////////////////
bool TextureLoad(int32 Index, uint8 * Data, bool StaticTexture, struct Rect * SubTexture);

////////////////////////////////////////////////////////////
/// Returns the texture data
////////////////////////////////////////////////////////////
struct Texture * TextureGetData(int32 Index);

////////////////////////////////////////////////////////////
/// Render a texture
////////////////////////////////////////////////////////////
void TextureRender(int32 Id, int32 X, int32 Y, struct Rect * RectData, struct Color4ub * Color);

////////////////////////////////////////////////////////////
/// Delete all textures
////////////////////////////////////////////////////////////
void TextureManagerDestroy();

#endif // GRAPHICS_TEXTURE_MANAGER_H
