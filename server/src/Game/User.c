/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/User.h>
#include <Game/Movement.h>
#include <Memory/General.h>

// todo: poor design, bottleneck
//			remove thread by user management
//			and use async i/o (kqueue, i/o completion ports)

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct UserListData		UserList;	///< User list

////////////////////////////////////////////////////////////
/// Create a user
////////////////////////////////////////////////////////////
struct UserData * UserCreate()
{
	struct UserData * User = (struct UserData*)MemoryAllocate(sizeof(struct UserData));

	if (User)
	{
		// Initialize account data
		AccountInitialize(&User->Account);

		// Set node list pointers
		User->Next = User->Previous = NULL;

		// Set character pointer
		User->Character = NULL;

		// Initialize mutex
		MutexInitialize(&User->MutexData);

		return User;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// User protocol parser
////////////////////////////////////////////////////////////
void UserParseData(void * User)
{
	// Socket incoming buffer
	uint8 Buffer[NETWORK_CONNECTION_SOCKET_MAX_SIZE];

	// Socket incoming data size
	uint32 SizeRecived = 0;

	// User
	struct UserData * UserPtr = (struct UserData*)User;

	// Socket receive loop
	do
	{
		// Receive data
		if (SocketReceive(&UserPtr->Connection.Socket, Buffer, NETWORK_CONNECTION_SOCKET_MAX_SIZE, &SizeRecived) == Done)
		{
			// Lock until the user hasn't been processed (todo: this is correct?)
			//MutexLock(&UserPtr->MutexData);

			if (!ProtocolParser(UserPtr, (uint8*)Buffer, SizeRecived))
			{
				//MutexUnlock(&UserPtr->MutexData);

				// Remove it
				UsersRemove(UserPtr);

				return;
			}
		
			//MutexUnlock(&UserPtr->MutexData);
		}

	} while (SizeRecived > 0);

	// Remove it
	UsersRemove(UserPtr);
}

////////////////////////////////////////////////////////////
/// Get character from user
////////////////////////////////////////////////////////////
struct CharacterData * UserGetCharacter(struct UserData * User)
{
	if (User->Character == NULL)
	{
		User->Character = AccountGetLoggedPlayerData(&User->Account);
	}

	return User->Character;
}

////////////////////////////////////////////////////////////
/// Initialize users data
////////////////////////////////////////////////////////////
bool UsersInitialize()
{
	// Initialize list
	UserList.First = UserList.Last = NULL;
	UserList.Count = 0;

	// Initialize mutex
	MutexInitialize(&UserList.MutexData);

	return true;
}

////////////////////////////////////////////////////////////
/// Destroy all users
////////////////////////////////////////////////////////////
bool UsersDestroy()
{
	UsersClear();

	// Destroy list mutex
	MutexDestroy(&UserList.MutexData);

	return true;
}

////////////////////////////////////////////////////////////
/// Clear all users
////////////////////////////////////////////////////////////
void UsersClear()
{
	struct UserData * Iterator;
	
	MutexLock(&UserList.MutexData);
	
	Iterator = UserList.First;

	// Remove all users
	while (Iterator != NULL)
	{
		struct UserData * TempUser = Iterator;
		
		// Lock user mutex
		MutexLock(&TempUser->MutexData);

		// Advance iterator
		Iterator = TempUser->Next;

		// Close socket (automatically exits from thread)
		SocketClose(&TempUser->Connection.Socket);

		// Unlock mutex
		MutexUnlock(&TempUser->MutexData);

		// Wait until user thread is finished
		ThreadWait(&TempUser->ThreadData);

		// Destroy the mutex
		MutexDestroy(&TempUser->MutexData);

		// Free it
		MemoryDeallocate(TempUser);

		TempUser = NULL;
	}

	// Clear list
	UserList.First = UserList.Last = NULL;
	UserList.Count = 0;

	MutexUnlock(&UserList.MutexData);
}

////////////////////////////////////////////////////////////
/// Add user in the list
////////////////////////////////////////////////////////////
void UsersAdd(struct UserData * User)
{
	// Lock mutex
	MutexLock(&UserList.MutexData);

	// Add to list
	if (UserList.Last)
	{
		// Put the user at the end
        UserList.Last->Next = User;
		User->Previous = UserList.Last;
        UserList.Last = User;
    }
    else
	{
		// If list is empty, put the user at first
		UserList.First = User;
        UserList.Last = User;
    }

	// Increment counter
    UserList.Count++;

	// Unlock mutex
	MutexUnlock(&UserList.MutexData);
}

////////////////////////////////////////////////////////////
/// Remove user from the list
////////////////////////////////////////////////////////////
void UsersRemove(struct UserData * User)
{
    struct UserData * Prev;
    struct UserData * Next;

	// Lock user list mutex
	MutexLock(&UserList.MutexData);

	Prev = User->Previous;
	Next = User->Next;

	// Remove from list
	if (Prev)
	{
		if (Next)
		{
			Prev->Next = Next;
			Next->Previous = Prev;
		}
		else
		{
			Prev->Next = NULL;
			UserList.Last = Prev;
		}
	}
	else
	{
		if (Next)
		{
			Next->Previous = NULL;
			UserList.First = Next;
		}
		else
		{
			UserList.First = UserList.Last = NULL;
		}
	}

	// Decrement counter
	UserList.Count--;

	// Unlock user list mutex
	MutexUnlock(&UserList.MutexData);

	// Lock user mutex
	MutexLock(&User->MutexData);

	// Close user socket
	SocketClose(&User->Connection.Socket);

	// (destroy other resources...)

	// Unlock user mutex
	MutexUnlock(&User->MutexData);

	// Destroy user mutex
	MutexDestroy(&User->MutexData);

	// Delete user
	MemoryDeallocate(User);
}

////////////////////////////////////////////////////////////
/// Check if the user is on the list
////////////////////////////////////////////////////////////
bool UsersContain(struct UserData * User)
{
	if (User != NULL)
	{
		struct UserData * Iterator;

		MutexLock(&UserList.MutexData);

		Iterator = UserList.First;

		while (Iterator != NULL)
		{
			if (Iterator == User)
			{
				MutexUnlock(&UserList.MutexData);
				return true;
			}

			Iterator = Iterator->Next;
		}
	
		MutexUnlock(&UserList.MutexData);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Wait the thread until the user isn't removed
////////////////////////////////////////////////////////////
void UsersWaitUntilRemoved(struct UserData * User)
{
	while (UsersContain(User))
	{
		ThreadWait(&User->ThreadData);
	}
}


////////////////////////////////////////////////////////////
/// Broadcast a message to all users
////////////////////////////////////////////////////////////
void UsersBroadcastMessage(struct PacketHeader * Packet)
{
	if (Packet != NULL)
	{
		struct UserData * Iterator;

		MutexLock(&UserList.MutexData);

		Iterator = UserList.First;

		while (Iterator != NULL)
		{
			Packet->Checksum = ~Packet->DataSize;

			// Send data to user
			SocketSend(&Iterator->Connection.Socket,
					   (uint8*)Packet,
					   Packet->DataSize + NETWORK_PACKET_HEADER_LENGTH);

			Iterator = Iterator->Next;
		}
	
		MutexUnlock(&UserList.MutexData);
	}
}

////////////////////////////////////////////////////////////
/// Move the character of user
////////////////////////////////////////////////////////////
bool UserCharacterMove(struct UserData * User, uint8 Key, bool Pressed)
{
	uint32 NextMovement = CharacterMove(User->Character->Movement, Key, Pressed);

	if (NextMovement != MoveInvalid)
	{
		// Apply movement
		if (NextMovement != User->Character->Movement)
		{
			// todo:

			//SceneManagerMove(Self)
			//{
				// CharacterMoveApply(NextMovement, , , );

				// Update forward with elapsed time

				// Clip position to terrain

				// Apply movement to user entity
			//}

			return true;
		}
	}

	return false;
}
