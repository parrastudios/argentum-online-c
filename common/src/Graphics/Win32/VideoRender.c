/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Compiler.h>
#include <Portability/Assembly.h>
#include <Portability/Qualifier.h>
#include <Graphics/VideoRender.h>
#include <Graphics/Device.h>
#include <Graphics/Texture.h>
#include <Graphics/TextureManager.h>
#include <Math/General.h>
#include <System/Error.h>

////////////////////////////////////////////////////////////
// Declarations
////////////////////////////////////////////////////////////
static bool Initialized = false;		///< Check if the library is initialized

////////////////////////////////////////////////////////////
// Swap red color to blue color (OGL Compability)
////////////////////////////////////////////////////////////
inline static uint32 VideoSwapBytes(void * Buffer, uint32 Size)
{
	register uint32 TextureSize = Size;

    #if COMPILER_TYPE == COMPILER_MSVC
        __asm
        {
            mov ecx, TextureSize
            mov ebx, Buffer
            label:
                mov al, [ebx+0]
                mov ah, [ebx+2]
                mov [ebx+2], al
                mov [ebx+0], ah

                add ebx, 3
                dec ecx
                jnz label
        }

        //jmp [ebx+3], 0 // Color key
    #elif COMPILER_TYPE == COMPILER_GNUC || COMPILER_TYPE == COMPILER_MINGW
        __asm__
        (
            "movl TextureSize, %ecx\n\t"
            "movl Buffer, %ebx\n\t"
            "label:\n\t"
                "movl (%ebx), %al\n\t"

                "movl 2(%ebx), %ah\n\t"

                "movl %al, 2(%ebx)\n\t"
                "movl %ah, (%ebx)\n\t"

                "addl $0x03, %ebx\n\t"
                "decl %ecx\n\t"
                "jnz label"
        );
    #endif
}

////////////////////////////////////////////////////////////
// Error handler
////////////////////////////////////////////////////////////
void VideoHandleError(HRESULT hr, char * FilePath)
{
	char Error[0xFF];

	switch (hr)
	{
		case AVIERR_BADFORMAT:
			strcpy(Error, "The file couldn't be read, indicating a corrupt file or an unrecognized format");
			break;

		case AVIERR_MEMORY:
			strcpy(Error, "The file could not be opened because of insufficient memory");
			break;

		case AVIERR_FILEREAD:
			strcpy(Error, "A disk error occurred while reading the file");
			break;

		case AVIERR_FILEOPEN:
			strcpy(Error, "A disk error occurred while opening the file");
			break;

		case REGDB_E_CLASSNOTREG:
			strcpy(Error, "According to the registry, the type of file specified in AVIFileOpen does not have a handler to process it");
			break;

		default :
			strcpy(Error, "Unknown");
			break;
	}

	MessageError("VideoLoad", "Failed to open the AVI frame: %s. Reason: %s.", FilePath, Error);
}

////////////////////////////////////////////////////////////
// Load a video
////////////////////////////////////////////////////////////
void VideoLoad(struct Video * VideoData, char * FilePath)
{
	HRESULT hr;

	if (!Initialized)
	{
		AVIFileInit();
		Initialized = true;
	}

	// Opens the AVI stream
	if ((hr = AVIStreamOpenFromFile(&VideoData->Object.Stream, (LPCSTR)(&FilePath[0]),
									streamtypeVIDEO, 0, OF_READ, NULL)) != AVIERR_OK)
	{
		// An error occurred opening the stream
		VideoHandleError(hr, FilePath);
		VideoData->Loaded = false;
		return;
	}

	AVIStreamInfo(VideoData->Object.Stream, &VideoData->Object.StreamInfo, sizeof(VideoData->Object.StreamInfo));

	// Set information
	VideoData->Width		= VideoData->Object.StreamInfo.rcFrame.right  - VideoData->Object.StreamInfo.rcFrame.left;
	VideoData->Height		= VideoData->Object.StreamInfo.rcFrame.bottom - VideoData->Object.StreamInfo.rcFrame.top;
	VideoData->LastFrame	= AVIStreamLength(VideoData->Object.Stream);
	VideoData->MsPerFrame	= AVIStreamSampleToTime(VideoData->Object.Stream, VideoData->LastFrame) / VideoData->LastFrame;
	VideoData->Object.File	= AVIStreamGetFrameOpen(VideoData->Object.Stream, NULL);
	VideoData->Finished		= false;
	VideoData->Started		= false;
	VideoData->CurrentFrame = 0;

	if (VideoData->Object.File	== NULL)
	{
		// An error occurred opening the frame
		MessageError("VideoLoad", "Failed to open the AVI frame: %s", FilePath);
		VideoData->Loaded = false;
		return;
	}

	VideoData->Loaded = true;
}

////////////////////////////////////////////////////////////
// Render a frame of the video
////////////////////////////////////////////////////////////
void VideoRender(struct Video * VideoData, bool Loop, uint32 X, uint32 Y, uint32 Width, uint32 Height)
{
	if (VideoData->Loaded && !VideoData->Finished)
	{
		LPBITMAPINFOHEADER BitmapHeader;
		uint8 *	Pixels;
		uint32	TextureWidth, TextureHeight;
		GLint	PreviousTexture;
		GLuint	Texture;

		// Load the frame
		BitmapHeader = (LPBITMAPINFOHEADER)AVIStreamGetFrame(VideoData->Object.File, VideoData->CurrentFrame);
		Pixels = (uint8 *)BitmapHeader + BitmapHeader->biSize + BitmapHeader->biClrUsed * sizeof(RGBQUAD);

		// Check if it is loaded
		if (!Pixels)
			return;

		// Swap the red and blue bytes (OGL Compatability)
		// VideoSwapBytes((void *)Pixels, 128*128);

		// Adjust internal texture dimensions depending on NPOT textures support
		TextureWidth  = TextureValidSize(VideoData->Width);
		TextureHeight = TextureValidSize(VideoData->Height);

		// Check the maximum texture size
		if ((TextureWidth > (uint32)(DeviceGetInfo()->Limits.Texture)) || (TextureHeight > (uint32)(DeviceGetInfo()->Limits.Texture)))
		{
			MessageError("VideoRender", "Failed to create image, its internal size is too high (%dx%d).", TextureWidth, TextureHeight);
			return;
		}

		// Bind the texture
		PreviousTexture = 0;
		Texture = 0;

		glEnable(GL_TEXTURE_2D);

		glGetIntegerv(GL_TEXTURE_BINDING_2D, &PreviousTexture);

		glGenTextures(1, &Texture);
		glBindTexture(Texture, Texture);

		if (DeviceGetInfo()->Support.EdgeClamp)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		}
		else
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		}

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		if (DeviceGetInfo()->Support.TextureNPOT)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, 3, VideoData->Width, VideoData->Height,
											0, GL_BGR, GL_UNSIGNED_BYTE, &Pixels[0]);
		}
		else
		{
			glTexImage2D(GL_TEXTURE_2D, 0, 3, TextureWidth, TextureHeight,
											0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, VideoData->Width, VideoData->Height, GL_BGR,
														GL_UNSIGNED_BYTE, &Pixels[0]);
		}

		// Render
		glBegin(GL_TRIANGLE_STRIP);
			glTexCoord2f(1.0f, 1.0f); glVertex2i(X + Width, Y);
			glTexCoord2f(0.0f, 1.0f); glVertex2i(X, Y);
			glTexCoord2f(1.0f, 0.0f); glVertex2i(X + Width, Y + Height);
			glTexCoord2f(0.0f, 0.0f); glVertex2i(X, Y + Height);
		glEnd();

		// Delete Texture
		glDeleteTextures(1, &Texture);

		// Bind previous
		glBindTexture(GL_TEXTURE_2D, PreviousTexture);

		// Update
		if (VideoData->Started == false)
		{
			// Reset the timer, initialize it
			TimerReset(&VideoData->TimerData);
			VideoData->Started = true;
		}

		VideoData->CurrentFrame = (uint32)(TimerGetTime(&VideoData->TimerData) / VideoData->MsPerFrame);

		if (VideoData->CurrentFrame >= VideoData->LastFrame)
		{
			// Set data
			if (Loop)
			{
				// Reset
				TimerReset(&VideoData->TimerData);
				VideoData->CurrentFrame = 0;
			}
			else
			{
				// Finish it
				VideoData->Finished = true;
				VideoData->Started = false;
			}
		}
	}
}

////////////////////////////////////////////////////////////
// Destroy a video
////////////////////////////////////////////////////////////
void VideoDelete(struct Video * VideoData)
{
	// Deallocates the GetFrame resources
	AVIStreamGetFrameClose(VideoData->Object.File);

	// Release the stream
	AVIStreamRelease(VideoData->Object.Stream);
}

////////////////////////////////////////////////////////////
// Destroy the video library
////////////////////////////////////////////////////////////
void VideoDestroy()
{
	// Release the library
	if (Initialized)
		AVIFileExit();
}
