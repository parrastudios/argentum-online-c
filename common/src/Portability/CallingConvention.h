/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PORTABILITY_CALLING_CONVENTION_H
#define PORTABILITY_CALLING_CONVENTION_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Compiler.h>

////////////////////////////////////////////////////////////
// Detection
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Function calling convention declarations
////////////////////////////////////////////////////////////
#if COMPILER_TYPE == COMPILER_MSVC
#	ifndef cdecl
#		define cdecl __cdecl
#	endif
#	ifndef stdcall
#		define stdcall __stdcall
#	endif
#	ifndef fastcall
#		define fastcall __fastcall
#	endif
#elif COMPILER_TYPE == COMPILER_GNUC
#	ifndef cdecl
#		define cdecl __attribute__((cdecl))
#	endif
#	ifndef stdcall
#		define stdcall __attribute__((stdcall))
#	endif
#	ifndef fastcall
#		define fastcall __attribute__((fastcall))
#	endif
#endif

#ifndef cdecl
#	define cdecl
#endif

#ifndef stdcall
#	define stdcall
#endif

#ifndef fastcall
#	define fastcall
#endif

#endif // PORTABILITY_CALLING_CONVENTION_H
