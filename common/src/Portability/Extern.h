/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// X Macros
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Warp an include over extern C X-Macro
///
/// Usage:
///		#define EXTERN_C_INCLUDE <MyCHeader.h>
///		#include <Portability/Extern.h>
////////////////////////////////////////////////////////////
#include <Portability/ExternBegin.h>

#include EXTERN_C_INCLUDE

#include <Portability/ExternEnd.h>

#undef EXTERN_C_INCLUDE
