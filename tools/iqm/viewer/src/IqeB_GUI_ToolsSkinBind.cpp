/****************************************************************************

  IqeB_GUI_ToolsSkinBind.cpp

  06.04.2015 RR: First editon of this file.


*****************************************************************************
*/

#include <math.h>

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Radio_Round_Button.H>
#include <FL/fl_draw.H>

/************************************************************************************
 * Globals
 *
 */

#define IQE_SKINBIND_BIND_NEAREST_JOINT   0  // Bind to nearest joint
#define IQE_SKINBIND_BIND_NEAREST_BONE    1  // Bind to nearest bone
#define IQE_SKINBIND_BIND_VOXEL_SKIN      2  // Bind 'nearest bone' skin using voxels
#define IQE_SKINBIND_BIND_VOXEL_BONES     3  // Bind by bone heat diffusion using voxels

static int   IqeB_ToolsSkinBind_AssignType = IQE_SKINBIND_ASSIGN_ALL_VERT;    //
static int   IqeB_ToolsSkinBind_JointSel   = IQE_SKINBIND_JOINT_ALL;          //
static int   IqeB_ToolsSkinBind_BindMode   = IQE_SKINBIND_BIND_NEAREST_JOINT; //
static int   IqeB_ToolsSkinBind_BindSmooth = 20;                              // Skin binding smooth

/************************************************************************************
 * Static variables
 */

static  Fl_Window *pMyToolWin;
static int MyWinPosX = IQE_GUI_NO_WINPOS_X, MyWinPosY = IQE_GUI_NO_WINPOS_Y; // last window position

static Fl_Button *pGUI_ButJointSelReset;    // Reset all joint selections
static Fl_Button *pGUI_ButBindReset;        // Reset all skin bindings
static Fl_Button *pGUI_ButBindDo;           // Do skin binding
static Fl_Int_Input *pInt_InputSmooth;      // Input field for skin smooth

/************************************************************************************
 * IqeB_GUI_ToolsSkinBindD
 *
 * Do the skin binding for the displayed model.
 *
 * UseSmooth: true  = use the bind smooth % from the GUI
 *            false = no bind smooth (use a smoot of 0%)
 */

void IqeB_GUI_ToolsSkinBindDo( int UseSmooth)
{
  int BindSmoothUse;
  char ErrorString[ 1024];
  float FillFactor;       // Fill factor = '# all voxels' / '# inside voxels'

  // What bind smooth to use

  if( UseSmooth) {   // Do the smooth

    BindSmoothUse = IqeB_ToolsSkinBind_BindSmooth;

  } else {           // Reset bind smooth

    BindSmoothUse = 0;
  }

  // Prepareations

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  ErrorString[ 0] = '\0';    //  Empty errorstring

  // Set the 'blend weights' checkbox --> display skin bindings
  IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doBlendWeightColors, 1); // Update variable and GUI

  //IqeB_SkinBindToBones( pDrawModel, IqeB_ToolsSkinBind_AssignType, IqeB_ToolsSkinBind_JointSel);
  switch( IqeB_ToolsSkinBind_BindMode) {
  default:
  case IQE_SKINBIND_BIND_NEAREST_JOINT:

    IqeB_SkinBindNearestJoint( pDrawModel, IqeB_ToolsSkinBind_AssignType, IqeB_ToolsSkinBind_JointSel);
    break;

  case IQE_SKINBIND_BIND_NEAREST_BONE:

    IqeB_SkinBindNearestBone( pDrawModel, IqeB_ToolsSkinBind_AssignType, IqeB_ToolsSkinBind_JointSel);
    break;

  case IQE_SKINBIND_BIND_VOXEL_SKIN:

    fl_cursor( FL_CURSOR_WAIT);       // A time cosuming operation follows, show wait cursor

    IqeB_SkinBindHeadDiffusionSkin( pDrawModel, IqeB_ToolsSkinBind_AssignType, IqeB_ToolsSkinBind_JointSel,
                                    BindSmoothUse, ErrorString, &FillFactor);

    fl_cursor( FL_CURSOR_DEFAULT);    // Reset cursor

    if( ErrorString[ 0] != '\0') {    //  Got errorstring

      IqeB_DispMessage( DRAW_MSG_COL_ERR,
                        (char *)"Nearest bone + Voxels:",
                        ErrorString, NULL);

    } else if( FillFactor <= IQE_SKINBIND_LOW_FILLFACTOR) {

      sprintf( ErrorString, "Bind done: %.1f%% filled", FillFactor * 100.0);

      IqeB_DispMessage( DRAW_MSG_COL_WARNING,
                        ErrorString,
                        (char *)"The model may have holes.",
                        (char *)"Please check the model.");
    } else {

      sprintf( ErrorString, "Bind done: %.1f%% filled", FillFactor * 100.0);
      IqeB_DispMessage( DRAW_MSG_COL_OK,
                        ErrorString,
                        NULL, NULL);
    }
    break;

  case IQE_SKINBIND_BIND_VOXEL_BONES:

    fl_cursor( FL_CURSOR_WAIT);       // A time cosuming operation follows, show wait cursor

    IqeB_SkinBindHeadDiffusionBones( pDrawModel, IqeB_ToolsSkinBind_AssignType, IqeB_ToolsSkinBind_JointSel,
                                     BindSmoothUse, ErrorString, &FillFactor);

    fl_cursor( FL_CURSOR_DEFAULT);    // Reset cursor

    pDrawModel->mesh->blendColorsRebuild = 1;   // Flag rebuild of 'blendColors'

    if( ErrorString[ 0] != '\0') {    //  Got errorstring

      IqeB_DispMessage( DRAW_MSG_COL_ERR,
                        (char *)"Bone heat diffusion + Voxels:",
                        ErrorString, NULL);

    } else if( FillFactor <= IQE_SKINBIND_LOW_FILLFACTOR) {

      sprintf( ErrorString, "Bind done: %.1f%% filled", FillFactor * 100.0);

      IqeB_DispMessage( DRAW_MSG_COL_WARNING,
                        ErrorString,
                        (char *)"The model may have holes.",
                        (char *)"Please check the model.");
    } else {

      sprintf( ErrorString, "Bind done: %.1f%% filled", FillFactor * 100.0);
      IqeB_DispMessage( DRAW_MSG_COL_OK,
                        ErrorString,
                        NULL, NULL);
    }
    break;
  } // switch

  pDrawModel->mesh->blendColorsRebuild = 1;   // Flag rebuild of 'blendColors'
}

/************************************************************************************
 * update GUI of this tool window
 */

static void MyWinUpdate()
{

  IqeB_GUI_WidgetActivate( pGUI_ButJointSelReset, pDrawModel != NULL &&               // Have a model displayed
                                                  pDrawModel->skel != NULL &&
                                                  pDrawModel->skel->joint_count > 0); // Have a skeleton

  IqeB_GUI_WidgetActivate( pGUI_ButBindReset,     pDrawModel != NULL &&               // Have a model displayed
                                                  pDrawModel->skel != NULL &&
                                                  pDrawModel->skel->joint_count > 0); // Have a skeleton

  IqeB_GUI_WidgetActivate( pGUI_ButBindDo,  pDrawModel != NULL &&               // Have a model displayed
                                            pDrawModel->skel != NULL &&
                                            pDrawModel->skel->joint_count > 0); // Have a skeleton

  IqeB_GUI_WidgetActivate( pInt_InputSmooth, IqeB_ToolsSkinBind_BindMode == IQE_SKINBIND_BIND_VOXEL_SKIN ||  // Only for this modes
                                             IqeB_ToolsSkinBind_BindMode == IQE_SKINBIND_BIND_VOXEL_BONES);
}

/************************************************************************************
 * IqeB_GUI_Button_Callback
 */

#define ACTION_CODE_JOINT_SEL_RESET      0  // Reset all joint selections
#define ACTION_CODE_BIND_CLEAR           1  // Clear skin bindings
#define ACTION_CODE_BIND_DO              2  // Do skin binding

static void IqeB_GUI_Button_Callback( Fl_Widget *w, void *ActionCodeArg)
{
  int ActionCode;

  if( pDrawModel == NULL) {              // have no draw model

    return;                              // nothing to do
  }

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  ActionCode = (int)ActionCodeArg;       // get action code

  switch( ActionCode) {
  case ACTION_CODE_JOINT_SEL_RESET:      // Reset all joint selections

    if( pDrawModel->skel != NULL) {      // have a skeleton

      int iJoint;

  	  for( iJoint = 0; iJoint < MAXBONE; iJoint++) {

        pDrawModel->skel->j[ iJoint].isSelected = 0;
  	  }
    }

    break;

  case ACTION_CODE_BIND_CLEAR:           // Clear skin bindings

    // Enable 'blend weights' checkbox
    IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doBlendWeightColors, 1); // Update variable and GUI

    IqeB_SkinBindClear( pDrawModel, IqeB_ToolsSkinBind_AssignType);

    pDrawModel->mesh->blendColorsRebuild = 1;   // Flag rebuild of 'blendColors'

    break;

  case ACTION_CODE_BIND_DO:   // Do skin binding

    IqeB_GUI_ToolsSkinBindDo( true);

    break;

  } // switch
}

/************************************************************************************
 * IqeB_SkinBind_AssignType_Callback
 */

static void IqeB_SkinBind_AssignType_Callback( Fl_Widget *w, void *data)
{

  IqeB_ToolsSkinBind_AssignType = (int)data;    // Set radio button value
}

/************************************************************************************
 * IqeB_SkinBind_JointSel_Callback
 */

static void IqeB_SkinBind_JointSel_Callback( Fl_Widget *w, void *data)
{

  IqeB_ToolsSkinBind_JointSel = (int)data;    // Set radio button value
}

/************************************************************************************
 * IqeB_SkinBind_BindMode_Callback
 */

static void IqeB_SkinBind_BindMode_Callback( Fl_Widget *w, void *data)
{

  IqeB_ToolsSkinBind_BindMode = (int)data;    // Set radio button value

  MyWinUpdate();               // Update the GUI
}

/************************************************************************************
 * IqeB_GUI_CBox_SetValue_Callback
 */

#ifdef use_again
static void IqeB_GUI_Float_SetValue_Callback( Fl_Widget *w, void *pValueArg)
{
  float *pValue;
  Fl_Float_Input *pThis;

  // ...

  pThis  = (Fl_Float_Input *)w;
  pValue = (float *)pValueArg;             // get pointer to associated variable

  *pValue = atof( pThis->value());         // update the variable
}
#endif

/************************************************************************************
 * IqeB_GUI_Int_SetValue_Callback
 */

static void IqeB_GUI_Int_SetValue_Callback( Fl_Widget *w, void *pValueArg)
{
  int Value, *pValue;
  Fl_Int_Input *pThis;
  char TempBuffer[ 256];

  // ...

  pThis  = (Fl_Int_Input *)w;
  pValue = (int *)pValueArg;               // get pointer to associated variable

  Value = atoi( pThis->value());           // get the value

  if( pValue == &IqeB_ToolsSkinBind_BindSmooth) {   // Range check for skin bind smooth

    if( Value < 0) {       //  Clip to percent range

      Value = 0;

      sprintf( TempBuffer, "%d", Value);    // Updat on GUI
      pThis->value( TempBuffer);
    }

    if( Value > 100) {       //  Clip to percent range

      Value = 100;
      sprintf( TempBuffer, "%d", Value);    // Updat on GUI
      pThis->value( TempBuffer);
    }
  }

  *pValue = Value;         // update the variable
}

/************************************************************************************
 * IqeB_GUI_UpdateCallback
 *
 * Is called from IqeB_GUI_UpdateWidgets() if something has changed
 * what needs an update (new model displayed, ...).
 */

static void IqeB_GUI_UpdateCallback( int UpdateFlags)
{

  if( pMyToolWin == NULL) {  // security test, has main window

    return;
  }

  MyWinUpdate();               // Update the GUI
}

/************************************************************************************
 * close_cb, close this window
 *
 * pValueArg is a pointer to the widget. Set this pointer to NULL on deletion.
 */

static void close_cb( Fl_Widget *w, void *pValueArg)
{

  IqeB_GUI_CloseToolWindow( (void **)&pMyToolWin);
}

/************************************************************************************
 * Presets for this tools window
 *
 */

 static T_GUI_PreferenceEntry MyPreferences[] =
 {

  // Cube display

  { PREF_T_INT,   "AssignType",          "0", &IqeB_ToolsSkinBind_AssignType},
  { PREF_T_INT,   "JointSelection",      "0", &IqeB_ToolsSkinBind_JointSel},
  { PREF_T_INT,   "BindMode",            "0", &IqeB_ToolsSkinBind_BindMode},
  { PREF_T_INT,   "BindSmooth",         "20", &IqeB_ToolsSkinBind_BindSmooth},
};

// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "ToolsSkinBind", MyPreferences, sizeof( MyPreferences) / sizeof( T_GUI_PreferenceEntry),
                                               (void **)(&pMyToolWin), &MyWinPosX, &MyWinPosY);

/************************************************************************************
 * IqeB_GUI_ToolsMeshTransformWin
 *
 * Open dialog
 */

void IqeB_GUI_ToolsSkinBindWin( int xLeft, int xRight, int yTop, int yBotton)
{

  //
  // winodow already created --> show it
  //

  if( pMyToolWin != NULL) {         // already have tool window

    // Show invisble window or bring visible window to foreground

    pMyToolWin->show();          // show it

    return;
  }

  //
  // creation of window on first call
  //

  if( pMyToolWin == NULL) {  // no tool window until now

    int xPos, yPos, MouseMode;

    xPos = xRight;
    yPos = yTop;

    MouseMode = IQE_GUI_MOUSE_MODE_JOINT_SEL;     // Mosue mode i wand to set

    if( IqeB_GUI_ToolWindowTestFreeEditMode( MouseMode)) {  // Test to have a free edit mode

      return;
    }

    if( MyWinPosX != IQE_GUI_NO_WINPOS_X && MyWinPosY != IQE_GUI_NO_WINPOS_Y) { // have last window position

      xPos = MyWinPosX;
      yPos = MyWinPosY;
    }

    pMyToolWin = new Fl_Window( xPos, yPos, IQE_GUI_TOOLS_STD_WITDH, 235, "Skin binding");

    if( pMyToolWin == NULL) {  // security test

      return;
    }

    IqeB_GUI_RegisterCallback( (void **)&pMyToolWin, IqeB_GUI_UpdateCallback,
                               IQE_GUI_UPDATE_MODEL_CHANGE | IQE_GUI_UPDATE_JOINT_SELECTED | IQE_GUI_UPDATE_JOINT_CHANGED,
                               MouseMode, (void(*)(void *, void *))close_cb);
  }

  //
  //  GUI things
  //

  int x1, x2, xc, xx1, xx2, xxTemp, y, yy, yGroup;

  //x/Fl_Check_Button *pTemp_Check_Button;
  //Fl_Float_Input  *pTemp_Float_Input;
  Fl_Box          *pTemp_Box;
  Fl_Tabs         *pTemp_Tabs;
  Fl_Group        *pTemp_Group, *pTemp_GroupRadioButton;
  //x/Fl_Button       *pTemp_Button;
  Fl_Radio_Round_Button *pRadioButTemp;
  char TempBuffer[ 256];

  x1  = 4;
  x2  = 8;
  xx1 = pMyToolWin->w() - 16;
  xx2 = xx1 / 2;
  xc  = pMyToolWin->w() / 2;          // x center
  yy  = 24;

  y = 4;

  //
  // Tabs
  //

  pTemp_Tabs = new Fl_Tabs( x1, y, pMyToolWin->w() - x1 - 4, pMyToolWin->h() - y - 4);
  pTemp_Tabs->selection_color( Fl::get_color( FL_SELECTION_COLOR));

  y += 26;

  //
  // Settings
  //

  yGroup = y;

  pTemp_Group = new Fl_Group( x1, y, pMyToolWin->w() - x1 - 4, pMyToolWin->h() - y - 4, "Settings");

    y += 4;

    // -- Assign type selection


    y += 4;

    pTemp_Box = new Fl_Box( x2, y, xx1, 2, "Assign bindings to vertices");
    pTemp_Box->align( FL_ALIGN_TOP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
    pTemp_Box->box( FL_DOWN_FRAME);

    y += 20;

    pTemp_GroupRadioButton = new Fl_Group( x2, y, xx1, 16 * 3);  // Group around this radio buttons

    pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx1, 16, "All vertices");
    pRadioButTemp->tooltip("Assign skin binding to all vertices");
    pRadioButTemp->callback( IqeB_SkinBind_AssignType_Callback, (void *)IQE_SKINBIND_ASSIGN_ALL_VERT);
    if( IqeB_ToolsSkinBind_AssignType == IQE_SKINBIND_ASSIGN_ALL_VERT) pRadioButTemp->value( 1);

    y += 16;

    pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx1, 16, "Selected mesh");
    pRadioButTemp->tooltip("Assign skin binding to vertices of selected mesh\n"
                           "Use the Mesh/Parts tool to select a mesh\n");
    pRadioButTemp->callback( IqeB_SkinBind_AssignType_Callback, (void *)IQE_SKINBIND_ASSIGN_SELECTED_MESH);
    if( IqeB_ToolsSkinBind_AssignType == IQE_SKINBIND_ASSIGN_SELECTED_MESH) pRadioButTemp->value( 1);

    y += 16;

    pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx1, 16, "Unassigned vertices");
    pRadioButTemp->tooltip("Assign skin binding to all unassigned vertices");
    pRadioButTemp->callback( IqeB_SkinBind_AssignType_Callback, (void *)IQE_SKINBIND_ASSIGN_UNASSIGNED_VERT);
    if( IqeB_ToolsSkinBind_AssignType == IQE_SKINBIND_ASSIGN_UNASSIGNED_VERT) pRadioButTemp->value( 1);

    y += 16;

    pTemp_GroupRadioButton->end();
    //pTemp_GroupRadioButton->size( pTemp_GroupRadioButton->w(), y - pTemp_GroupRadioButton->y());

    // -- Assign joint selection

    y += 4;

    pTemp_Box = new Fl_Box( x2, y, xx1, 2, "Assign from joints");
    pTemp_Box->align( FL_ALIGN_TOP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
    pTemp_Box->box( FL_DOWN_FRAME);

    y += 20;

    pTemp_GroupRadioButton = new Fl_Group( x2, y, xx1, 16 * 3);  // Group around this radio buttons

    pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx1 - 60, 16, "All joints");
    pRadioButTemp->tooltip("");
    pRadioButTemp->callback( IqeB_SkinBind_JointSel_Callback, (void *)IQE_SKINBIND_JOINT_ALL);
    if( IqeB_ToolsSkinBind_JointSel == IQE_SKINBIND_JOINT_ALL) pRadioButTemp->value( 1);

    y += 16;

    pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx1 - 60, 16, "Selected joints");
    pRadioButTemp->tooltip("");
    pRadioButTemp->callback( IqeB_SkinBind_JointSel_Callback, (void *)IQE_SKINBIND_JOINT_SELECTED);
    if( IqeB_ToolsSkinBind_JointSel == IQE_SKINBIND_JOINT_SELECTED) pRadioButTemp->value( 1);

    y += 16;

    pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx1 - 60, 16, "Unselected joints");
    pRadioButTemp->tooltip("");
    pRadioButTemp->callback( IqeB_SkinBind_JointSel_Callback, (void *)IQE_SKINBIND_JOINT_NOT_SEL);
    if( IqeB_ToolsSkinBind_JointSel == IQE_SKINBIND_JOINT_NOT_SEL) pRadioButTemp->value( 1);

    pGUI_ButJointSelReset = new Fl_Button( x2 + xx1 - 52, y - yy + 16, 52, yy, "Reset");
    pGUI_ButJointSelReset->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_JOINT_SEL_RESET));
    pGUI_ButJointSelReset->tooltip( "Reset all joint selections");

    y += yy;
    //x/y += 16;

    pTemp_GroupRadioButton->end();
    //pTemp_GroupRadioButton->size( pTemp_GroupRadioButton->w(), y - pTemp_GroupRadioButton->y());

    // Finish things for this group

    //pTemp_Group->size( pTemp_Group->w(), y - pTemp_Group->y());
    pTemp_Group->end();

  //
  // Group bind
  //

  y = yGroup;

  pTemp_Group = new Fl_Group( x1, y, pMyToolWin->w() - x1 - 4, pMyToolWin->h() - y - 4, "Bind");

    // translate to min, center or max side

    xxTemp = xx2;

    y += 4;

    // -- Reset all skin bindings

    y += 4;

    pTemp_Box = new Fl_Box( x2, y, xx1, 2, "Clear skin bindings");
    pTemp_Box->align( FL_ALIGN_TOP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
    pTemp_Box->box( FL_DOWN_FRAME);

    y += 20;

    pGUI_ButBindReset = new Fl_Button( xc, y, xxTemp, yy, "Do it");
    pGUI_ButBindReset->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_BIND_CLEAR));
    pGUI_ButBindReset->tooltip( "Clear skin bindings.\n"
                           "Depents from 'Assign bindings to vertices' setting:\n"
                           "  All vertices:  clear bindings of all vertices\n"
                           "  Selected mesh: clear bindings of selected mesh part\n"
                           "NOTE: Model need a skeleton to see the skin weights.\n");

    y += yy;

    // -- Bind skin to nearest bone

    y += 4;

    pTemp_Box = new Fl_Box( x2, y, xx1, 2, "Skin binding");
    pTemp_Box->align( FL_ALIGN_TOP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
    pTemp_Box->box( FL_DOWN_FRAME);

    y += 20;

    pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx1 - 60, 16, "Nearest joint");
    pRadioButTemp->tooltip("");
    pRadioButTemp->callback( IqeB_SkinBind_BindMode_Callback, (void *)IQE_SKINBIND_BIND_NEAREST_JOINT);
    if( IqeB_ToolsSkinBind_BindMode == IQE_SKINBIND_BIND_NEAREST_JOINT) pRadioButTemp->value( 1);
    pRadioButTemp->tooltip( "Skin bind to nearest joint.");

    y += 16;

    pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx1 - 60, 16, "Nearest bone");
    pRadioButTemp->tooltip("");
    pRadioButTemp->callback( IqeB_SkinBind_BindMode_Callback, (void *)IQE_SKINBIND_BIND_NEAREST_BONE);
    if( IqeB_ToolsSkinBind_BindMode == IQE_SKINBIND_BIND_NEAREST_BONE) pRadioButTemp->value( 1);
    pRadioButTemp->tooltip( "Skin bind to nearest bone.");

    y += 16;

    pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx1 - 60, 16, "Nearest bone + Voxels");
    pRadioButTemp->tooltip("");
    pRadioButTemp->callback( IqeB_SkinBind_BindMode_Callback, (void *)IQE_SKINBIND_BIND_VOXEL_SKIN);
    if( IqeB_ToolsSkinBind_BindMode == IQE_SKINBIND_BIND_VOXEL_SKIN) pRadioButTemp->value( 1);
    pRadioButTemp->tooltip( "Skin bind to nearest bone.\n"
                            "Use a voxel approach to test visibility\n"
                            "and to smooth the bindings.\n");

    y += 16;

    pRadioButTemp = new Fl_Radio_Round_Button( x2, y, xx1 - 60, 16, "Bone heat diffusion + Voxels");
    pRadioButTemp->tooltip("");
    pRadioButTemp->callback( IqeB_SkinBind_BindMode_Callback, (void *)IQE_SKINBIND_BIND_VOXEL_BONES);
    if( IqeB_ToolsSkinBind_BindMode == IQE_SKINBIND_BIND_VOXEL_BONES) pRadioButTemp->value( 1);
    pRadioButTemp->tooltip( "Heat diffusion of bones.\n"
                            "Use a voxel approach for heat diffusion\n"
                            "and to smooth the bindings.\n");

    y += 20;

    y += 8;

    pGUI_ButBindDo = new Fl_Button( xc, y, xxTemp, yy, "Do it");
    pGUI_ButBindDo->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_BIND_DO));
    pGUI_ButBindDo->tooltip( "Do the skin binding.");

    pInt_InputSmooth = new Fl_Int_Input( x2 + xx1 / 12, y + 2, xx1 / 3, yy - 2, "Smooth %");
    pInt_InputSmooth->align( FL_ALIGN_TOP_LEFT);     // align for label
    pInt_InputSmooth->labelsize( 10);
    sprintf( TempBuffer, "%d", IqeB_ToolsSkinBind_BindSmooth);
    pInt_InputSmooth->value( TempBuffer);
    pInt_InputSmooth->callback( IqeB_GUI_Int_SetValue_Callback, &IqeB_ToolsSkinBind_BindSmooth);
    pInt_InputSmooth->tooltip( "Smooth of skin bindings (in % of object size).\n"
                               "  0 = off.\n"
                               "NOTE: Computing time increase with higher values.");

    y += yy;

    // Finish things for this group

    //pTemp_Group->size( pTemp_Group->w(), y - pTemp_Group->y());
    pTemp_Group->end();

  // finish up

  pTemp_Tabs->end();
  //pMyToolWin->size( pMyToolWin->w(), pTemp_Tabs->y() + pTemp_Tabs->h() + 4);  // final size of window

  pMyToolWin->end();
  pMyToolWin->set_non_modal();
  pMyToolWin->callback( close_cb, &pMyToolWin);

  MyWinUpdate(); // Update the GUI

  pMyToolWin->show();
}

/********************************** End Of File **********************************/
