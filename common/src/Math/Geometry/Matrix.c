/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Matrix.h>

void Matrix3fIdentity(struct Matrix3f * Matrix)
{
    if (Matrix)
    {
        Matrix->m[0][0] = 1.0f; Matrix->m[0][1] = 0.0f; Matrix->m[0][2] = 0.0f;
        Matrix->m[1][0] = 0.0f; Matrix->m[1][1] = 1.0f; Matrix->m[1][2] = 0.0f;
        Matrix->m[2][0] = 0.0f; Matrix->m[2][1] = 0.0f; Matrix->m[2][2] = 1.0f;
    }
}

void Matrix3fFromQuaternion(struct Matrix3f * Matrix, struct Quaternion * Quat)
{
	if (Matrix && Quat)
	{
		float tx = 2 * Quat->x, ty = 2 * Quat->y, tz = 2 * Quat->z;
		float txx = tx * Quat->x, tyy = ty * Quat->y, tzz = tz * Quat->z;
		float txy = tx * Quat->y, txz = tx * Quat->z, tyz = ty * Quat->z;
		float twx = Quat->w * tx, twy = Quat->w * ty, twz = Quat->w * tz;

		Matrix->m[0][0] = 1.0f - (tyy + tzz);
		Matrix->m[0][1] = txy - twz;
		Matrix->m[0][2] = txz + twy;

		Matrix->m[1][0] = txy + twz;
		Matrix->m[1][1] = 1.0f - (txx + tzz);
		Matrix->m[1][2] = tyz - twx;

		Matrix->m[2][0] = txz - twy;
		Matrix->m[2][1] = tyz + twx;
		Matrix->m[2][2] = 1.0f - (txx + tyy);
	}
}

void Matrix3fFromQuaternionScale(struct Matrix3f * Matrix, struct Quaternion * Quat, struct Vector3f * Scale)
{
	if (Matrix && Quat && Scale)
	{
		Matrix3fFromQuaternion(Matrix, Quat);

		Matrix->m[0][0] *= Scale->x;
		Matrix->m[0][1] *= Scale->y;
		Matrix->m[0][2] *= Scale->z;

		Matrix->m[1][0] *= Scale->x;
		Matrix->m[1][1] *= Scale->y;
		Matrix->m[1][2] *= Scale->z;

		Matrix->m[2][0] *= Scale->x;
		Matrix->m[2][1] *= Scale->y;
		Matrix->m[2][2] *= Scale->z;
	}
}

void Matrix3fFromAdjointTranspose(struct Matrix3f * Dest, struct Matrix34f * Src)
{
	if (Dest && Src)
	{
		Dest->m[0][0] = Src->m[1][1] * Src->m[2][2] - Src->m[1][2] * Src->m[2][1];
		Dest->m[0][1] = Src->m[1][2] * Src->m[2][0] - Src->m[1][0] * Src->m[2][2];
		Dest->m[0][2] = Src->m[1][0] * Src->m[2][1] - Src->m[1][1] * Src->m[2][0];

		Dest->m[1][0] = Src->m[2][1] * Src->m[0][2] - Src->m[2][2] * Src->m[0][1];
		Dest->m[1][1] = Src->m[2][2] * Src->m[0][0] - Src->m[2][0] * Src->m[0][2];
		Dest->m[1][2] = Src->m[2][0] * Src->m[0][1] - Src->m[2][1] * Src->m[0][0];

		Dest->m[2][0] = Src->m[0][1] * Src->m[1][2] - Src->m[0][2] * Src->m[1][1];
		Dest->m[2][1] = Src->m[0][2] * Src->m[1][0] - Src->m[0][0] * Src->m[1][2];
		Dest->m[2][2] = Src->m[0][0] * Src->m[1][1] - Src->m[0][1] * Src->m[1][0];
	}
}

float Matrix3fDeterminant(struct Matrix3f * Matrix)
{
    if (Matrix)
    {
        return (Matrix->m[0][0] * Matrix->m[1][1] * Matrix->m[2][2]) +
               (Matrix->m[1][0] * Matrix->m[2][1] * Matrix->m[0][2]) +
               (Matrix->m[0][1] * Matrix->m[1][2] * Matrix->m[2][0]) -
               (Matrix->m[2][0] * Matrix->m[1][1] * Matrix->m[0][2]) -
               (Matrix->m[1][0] * Matrix->m[0][1] * Matrix->m[2][2]) -
               (Matrix->m[2][1] * Matrix->m[1][2] * Matrix->m[0][0]);
    }

    return 0.0f;
};

void Matrix3fRotationVector(struct Matrix3f * Matrix, struct Vector3f * Vector, float Radians)
{
	float InvLength, Sin, Cos, InvCos;
	float vx, vy, vz, xx, yy, zz, xy, yz, zx, xs, ys, zs;

	Sin = sinf(Radians);
	Cos = cosf(Radians);

	InvLength = 1.0f / Vector3fLength(Vector);

	vx = Vector->x * InvLength;
	vy = Vector->y * InvLength;
	vz = Vector->z * InvLength;
	xx = vx * vx;
	yy = vy * vy;
	zz = vz * vz;
	xy = vx * vy;
	yz = vy * vz;
	zx = vz * vx;
	xs = vx * Sin;
	ys = vy * Sin;
	zs = vz * Sin;
	InvCos = 1.0f - Cos;

	Matrix->m[0][0] = (InvCos * xx) + Cos;
	Matrix->m[1][0] = (InvCos * xy) - zs;
	Matrix->m[2][0] = (InvCos * zx) + ys;

	Matrix->m[0][1] = (InvCos * xy) + zs;
	Matrix->m[1][1] = (InvCos * yy) + Cos;
	Matrix->m[2][1] = (InvCos * yz) - xs;

	Matrix->m[0][2] = (InvCos * zx) - ys;
	Matrix->m[1][2] = (InvCos * yz) + xs;
	Matrix->m[2][2] = (InvCos * zz) + Cos;
}

void Matrix3fTransform(struct Matrix3f * Matrix, struct Vector3f * Vector, struct Vector3f * Result)
{
	if (Matrix && Vector && Result)
	{
		uinteger i, j;

		// Reset result
		Vector3fSetNull(Result);

		// Calculate transform
		for (i = 0; i < 3; ++i)
		{
			for (j = 0; j < 3; ++j)
			{
				Result->v[i] += (Matrix->m[i][j] * Vector->v[j]);
			}
		}
	}
}

void Matrix3fTransposedTransform(struct Matrix3f * Matrix, struct Vector3f * Vector, struct Vector3f * Result)
{
	if (Matrix && Vector && Result)
	{
		uinteger i, j;

		// Reset result
		Vector3fSetNull(Result);

		for (i = 0; i < 3; ++i)
		{
			for (j = 0; j < 3; ++j)
			{
				Result->v[i] += Matrix->m[j][i] * Vector->v[j];
			}
		}
	}
}

void Matrix34fCopy(struct Matrix34f * Dest, struct Matrix34f * Source)
{
	if (Dest && Source)
	{
		uinteger i, j;

		for (i = 0; i < 3; ++i)
		{
			for (j = 0; j < 4; ++j)
			{
				Dest->m[i][j] = Source->m[i][j];
			}
		}
	}
}

void Matrix34fFromQuaternionRotation(struct Matrix34f * Matrix, struct Quaternion * Rotation, struct Vector3f * Translation, struct Vector3f * Scale)
{
	if (Matrix && Rotation && Translation && Scale)
	{
		struct Matrix3f RotationScaleMatrix;

		Matrix3fFromQuaternionScale(&RotationScaleMatrix, Rotation, Scale);

		Matrix34fFromMatrixRotation(Matrix, &RotationScaleMatrix, Translation);
	}
}

void Matrix34fFromMatrixRotation(struct Matrix34f * Matrix, struct Matrix3f * Rotation, struct Vector3f * Translation)
{
	if (Matrix && Rotation && Translation)
	{
		Matrix->m[0][0] = Rotation->m[0][0];
		Matrix->m[0][1] = Rotation->m[0][1];
		Matrix->m[0][2] = Rotation->m[0][2];
		Matrix->m[0][3] = Translation->x;

		Matrix->m[1][0] = Rotation->m[1][0];
		Matrix->m[1][1] = Rotation->m[1][1];
		Matrix->m[1][2] = Rotation->m[1][2];
		Matrix->m[1][3] = Translation->y;

		Matrix->m[2][0] = Rotation->m[2][0];
		Matrix->m[2][1] = Rotation->m[2][1];
		Matrix->m[2][2] = Rotation->m[2][2];
		Matrix->m[2][3] = Translation->z;
	}
}

void Matrix34fAdd(struct Matrix34f * Matrix, struct Matrix34f * A, struct Matrix34f * B)
{
	if (Matrix && A && B)
	{
		Matrix->m[0][0] = A->m[0][0] + B->m[0][0];
		Matrix->m[0][1] = A->m[0][1] + B->m[0][1];
		Matrix->m[0][2] = A->m[0][2] + B->m[0][2];
		Matrix->m[0][3] = A->m[0][3] + B->m[0][3];

		Matrix->m[1][0] = A->m[1][0] + B->m[1][0];
		Matrix->m[1][1] = A->m[1][1] + B->m[1][1];
		Matrix->m[1][2] = A->m[1][2] + B->m[1][2];
		Matrix->m[1][3] = A->m[1][3] + B->m[1][3];

		Matrix->m[2][0] = A->m[2][0] + B->m[2][0];
		Matrix->m[2][1] = A->m[2][1] + B->m[2][1];
		Matrix->m[2][2] = A->m[2][2] + B->m[2][2];
		Matrix->m[2][3] = A->m[2][3] + B->m[2][3];
	}
}

void Matrix34fScale(struct Matrix34f * Dest, struct Matrix34f * Src, float Scale)
{
	if (Dest && Src && Scale != 0.0f)
	{
		Dest->m[0][0] = Src->m[0][0] * Scale;
		Dest->m[0][1] = Src->m[0][1] * Scale;
		Dest->m[0][2] = Src->m[0][2] * Scale;
		Dest->m[0][3] = Src->m[0][3] * Scale;

		Dest->m[1][0] = Src->m[1][0] * Scale;
		Dest->m[1][1] = Src->m[1][1] * Scale;
		Dest->m[1][2] = Src->m[1][2] * Scale;
		Dest->m[1][3] = Src->m[1][3] * Scale;

		Dest->m[2][0] = Src->m[2][0] * Scale;
		Dest->m[2][1] = Src->m[2][1] * Scale;
		Dest->m[2][2] = Src->m[2][2] * Scale;
		Dest->m[2][3] = Src->m[2][3] * Scale;
	}
}

void Matrix34fMultiply(struct Matrix34f * Matrix, struct Matrix34f * A, struct Matrix34f * B)
{
	if (Matrix && A && B)
	{
		uinteger i, j, k;

		for (i = 0; i < 3; ++i)
		{
			for (j = 0; j < 4; ++j)
			{
				for (k = 0, Matrix->m[i][j] = 0.0f; k < 3; ++k)
				{
					Matrix->m[i][j] += A->m[i][k] * B->m[k][j];
				}
			}

			Matrix->m[i][3] += A->m[i][3];
		}
	}
}

void Matrix34fInvert(struct Matrix34f * Result, struct Matrix34f * Matrix)
{
	if (Result && Matrix)
	{
		uinteger i;
		struct Matrix3f InverseRotation;
		struct Vector3f Translation;

		// Calculate inverse rotation matrix
		for (i = 0; i < 3; ++i)
		{
			struct Vector3f InverseRotationVector;

			Vector3fSet(&InverseRotationVector, Matrix->m[0][i], Matrix->m[1][i], Matrix->m[2][i]);

			Vector3fDivide(&InverseRotationVector, Vector3fLengthSquared(&InverseRotationVector));

			InverseRotation.m[i][0] = InverseRotationVector.x;
			InverseRotation.m[i][1] = InverseRotationVector.y;
			InverseRotation.m[i][2] = InverseRotationVector.z;

		}

		// Apply translation
		Vector3fSet(&Translation, Matrix->m[0][3], Matrix->m[1][3], Matrix->m[2][3]);

		// Calculate final matrix
		for (i = 0; i < 3; ++i)
		{
			struct Vector3f InverseRotationVector;

			Vector3fSet(&InverseRotationVector, InverseRotation.m[i][0], InverseRotation.m[i][1], InverseRotation.m[i][2]);

			Result->m[i][0] = InverseRotationVector.x;
			Result->m[i][1] = InverseRotationVector.y;
			Result->m[i][2] = InverseRotationVector.z;
			Result->m[i][3] = -Vector3fDot(&InverseRotationVector, &Translation);
		}
	}
}

void Matrix34fTransform(struct Matrix34f * Matrix, struct Vector3f * Vector, struct Vector3f * Result)
{
	if (Matrix && Vector && Result)
	{
		uinteger i, j;

		// Set result
		Vector3fSet(Result, Matrix->m[0][3], Matrix->m[1][3], Matrix->m[2][3]);

		// Calculate transform
		for (i = 0; i < 3; ++i)
		{
			for (j = 0; j < 3; ++j)
			{
				Result->v[i] += (Matrix->m[i][j] * Vector->v[j]);
			}
		}
	}
}

void Matrix34fTransformNormal(struct Matrix34f * Matrix, struct Vector3f * Vector, struct Vector3f * Result)
{
	if (Matrix && Vector && Result)
	{
		uinteger i, j;

		// Reset result
		Vector3fSetNull(Result);

		// Calculate transform
		for (i = 0; i < 3; ++i)
		{
			for (j = 0; j < 3; ++j)
			{
				Result->v[i] += (Matrix->m[i][j] * Vector->v[j]);
			}
		}
	}
}

void Matrix4fIdentity(struct Matrix4f * Matrix)
{
    if (Matrix)
    {
        Matrix->m[0][0] = 1.0f; Matrix->m[0][1] = 0.0f; Matrix->m[0][2] = 0.0f; Matrix->m[0][3] = 0.0f;
        Matrix->m[1][0] = 0.0f; Matrix->m[1][1] = 1.0f; Matrix->m[1][2] = 0.0f; Matrix->m[1][3] = 0.0f;
        Matrix->m[2][0] = 0.0f; Matrix->m[2][1] = 0.0f; Matrix->m[2][2] = 1.0f; Matrix->m[2][3] = 0.0f;
        Matrix->m[3][0] = 0.0f; Matrix->m[3][1] = 0.0f; Matrix->m[3][2] = 0.0f; Matrix->m[3][3] = 1.0f;
    }
}

void Matrix4fCopy(struct Matrix4f * Dest, struct Matrix4f * Source)
{
	if (Source && Dest)
	{
		Dest->m[0][0] = Source->m[0][0];
		Dest->m[0][1] = Source->m[0][1];
		Dest->m[0][2] = Source->m[0][2];
		Dest->m[0][3] = Source->m[0][3];

		Dest->m[1][0] = Source->m[1][0];
		Dest->m[1][1] = Source->m[1][1];
		Dest->m[1][2] = Source->m[1][2];
		Dest->m[1][3] = Source->m[1][3];

		Dest->m[2][0] = Source->m[2][0];
		Dest->m[2][1] = Source->m[2][1];
		Dest->m[2][2] = Source->m[2][2];
		Dest->m[2][3] = Source->m[2][3];

		Dest->m[3][0] = Source->m[3][0];
		Dest->m[3][1] = Source->m[3][1];
		Dest->m[3][2] = Source->m[3][2];
		Dest->m[3][3] = Source->m[3][3];
	}
}

void Matrix4fMultiply(struct Matrix4f * Matrix, struct Matrix4f * A, struct Matrix4f * B)
{
    if (Matrix && A && B)
    {
        uinteger i, j, k;

        for (i = 0; i < 4; ++i)
        {
            for (j = 0; j < 4; ++j)
            {
                for (k = 0, Matrix->m[i][j] = 0.0f; k < 4; ++k)
                {
                    Matrix->m[i][j] += A->m[i][k] * B->m[k][j];
                }
            }
        }
    }
}

void Matrix4fVertorMultiply(struct Vector4f * Result, struct Matrix4f * Matrix, struct Vector4f * Vector)
{
    if (Result && Matrix && Vector)
    {
		uinteger i, j;

		for (i = 0; i < 4; ++i)
		{
			for (j = 0, Result->v[i] = 0.0f; j < 4; ++j)
			{
				Result->v[i] += Vector->v[j] * Matrix->m[j][i];
			}
		}
    }
}

void Matrix4fRotateAngle(struct Matrix4f * Matrix, float Angle, uinteger Axis)
{
    if (Matrix && Axis >= MATRIX_AXIS_X && Axis <= MATRIX_AXIS_Z)
    {
        float AngleCos = (float)cos(Angle);
        float AngleSin = (float)sin(Angle);

        Matrix4fIdentity(Matrix);

        switch (Axis)
        {
            case MATRIX_AXIS_X :
            {
                Matrix->m[1][1] = Matrix->m[2][2] = AngleCos;
                Matrix->m[2][1] = -AngleSin;
                Matrix->m[1][2] = AngleSin;
                break;
            }

            case MATRIX_AXIS_Y :
            {
                Matrix->m[0][0] = Matrix->m[2][2] = AngleCos;
                Matrix->m[0][2] = -AngleSin;
                Matrix->m[2][0] = AngleSin;
                break;
            }

            case MATRIX_AXIS_Z :
            {
                Matrix->m[0][0] = Matrix->m[1][1] = AngleCos;
                Matrix->m[1][0] = -AngleSin;
                Matrix->m[0][1] = AngleSin;
                break;
            }

            default :
                break;
        }
    }
}

void Matrix4fRotateVector(struct Matrix4f * Matrix, struct Vector3f * Vector)
{
    if (Matrix && Vector)
    {
        Matrix4fIdentity(Matrix);

        if (Vector->x != 0.0f)
        {
            float AngleCos = (float)cos(Vector->x);
            float AngleSin = (float)sin(Vector->x);

            Matrix->m[1][1] = Matrix->m[2][2] = AngleCos;
            Matrix->m[2][1] = -AngleSin;
            Matrix->m[1][2] = AngleSin;
        }

        if (Vector->y != 0.0f)
        {
            float AngleCos = (float)cos(Vector->y);
            float AngleSin = (float)sin(Vector->y);

            Matrix->m[0][0] = Matrix->m[2][2] = AngleCos;
            Matrix->m[0][2] = -AngleSin;
            Matrix->m[2][0] = AngleSin;
        }

        if (Vector->z != 0.0f)
        {
            float AngleCos = (float)cos(Vector->z);
            float AngleSin = (float)sin(Vector->z);

            Matrix->m[0][0] = Matrix->m[1][1] = AngleCos;
            Matrix->m[1][0] = -AngleSin;
            Matrix->m[0][1] = AngleSin;
        }
    }
}

void Matrix4fRotateQuaternion(struct Matrix4f * Matrix, struct Quaternion * Quat)
{
    if (Matrix && Quat)
    {
        float xy = Quat->x * Quat->y;  float xz = Quat->x * Quat->z;  float xw = Quat->x * Quat->w;
        float yz = Quat->y * Quat->z;  float yw = Quat->y * Quat->w;
        float zw = Quat->z * Quat->w;

        Matrix->m[3][3] = 1.0f;

        Matrix->m[0][0] = (1-(2*( (Quat->y * Quat->y) + (Quat->z * Quat->z))));
        Matrix->m[1][0] = 2*(xy-zw);
        Matrix->m[2][0] = 2*(yw+xz);

        Matrix->m[0][1] = 2*(xz+zw);
        Matrix->m[1][1] = (1-(2*( (Quat->x * Quat->x) + (Quat->z * Quat->z))));
        Matrix->m[2][1] = 2*(yz-xw);

        Matrix->m[0][2] = 2*(xz-yw);
        Matrix->m[1][2] = 2*(yz+xw);
        Matrix->m[2][2] = (1-(2*( (Quat->x * Quat->x) + (Quat->y * Quat->y))));
    }
}

void Matrix4fScale(struct Matrix4f * Matrix, float x, float y, float z)
{
    if (Matrix)
    {
        Matrix4fIdentity(Matrix);

        Matrix->m[0][0] = x;
        Matrix->m[1][1] = y;
        Matrix->m[2][2] = z;
    }
}

void Matrix4fScaleVector(struct Matrix4f * Matrix, struct Vector3f * Vector)
{
    if (Matrix && Vector)
    {
        Matrix4fIdentity(Matrix);

        Matrix->m[0][0] = Vector->x;
        Matrix->m[1][1] = Vector->y;
        Matrix->m[2][2] = Vector->z;
    }
}

struct Matrix4f * Matrix4fTranslate(struct Matrix4f * Matrix, float x, float y, float z)
{
    if (Matrix)
    {
        Matrix->v[12] = Matrix->v[0] * x + Matrix->v[4] * y + Matrix->v[8] * z + Matrix->v[12];
        Matrix->v[13] = Matrix->v[1] * x + Matrix->v[5] * y + Matrix->v[9] * z + Matrix->v[13];
        Matrix->v[14] = Matrix->v[2] * x + Matrix->v[6] * y + Matrix->v[10] * z + Matrix->v[14];
        Matrix->v[15] = Matrix->v[3] * x + Matrix->v[7] * y + Matrix->v[11] * z + Matrix->v[15];

        return Matrix;
    }

    return NULL;
}

void Matrix4fTransform(struct Matrix4f * Matrix, float x, float y, float z)
{
    if (Matrix)
    {
        Matrix4fIdentity(Matrix);

        Matrix->m[3][0] = x;
        Matrix->m[3][1] = y;
        Matrix->m[3][2] = z;
    }
}

void Matrix4fTransformVector(struct Matrix4f * Matrix, struct Vector3f * Vector)
{
    if (Matrix && Vector)
    {
        Matrix4fIdentity(Matrix);

        Matrix->m[3][0] = Vector->x;
        Matrix->m[3][1] = Vector->y;
        Matrix->m[3][2] = Vector->z;
    }
}

struct Matrix4f * Matrix4fLoadOrtho(struct Matrix4f * Matrix, float Left, float Right, float Bottom, float Top, float Near, float Far)
{
    if (Matrix)
    {
        Matrix->m[0][0] = 2.0f / (Right - Left);
        Matrix->m[0][1] = 0.0f;
        Matrix->m[0][2] = 0.0f;
        Matrix->m[0][3] = 0.0f;

        Matrix->m[1][0] = 0.0f;
        Matrix->m[1][1] = 2.0f / (Top - Bottom);
        Matrix->m[1][2] = 0.0f;
        Matrix->m[1][3] = 0.0f;

        Matrix->m[2][0] = 0.0f;
        Matrix->m[2][1] = 0.0f;
        Matrix->m[2][2] = -2.0f / (Far - Near);
        Matrix->m[2][3] = 0.0f;

        Matrix->m[3][0] = -((Right + Left) / (Right - Left));
        Matrix->m[3][1] = -((Top + Bottom) / (Top - Bottom));
        Matrix->m[3][2] = -((Far + Near) / (Far - Near));
        Matrix->m[3][3] = 1.0f;

        return Matrix;
    }

    return NULL;
}

struct Matrix4f * Matrix4fLoadView(struct Matrix4f * Matrix, struct Vector3f * Front, struct Vector3f * Up, struct Vector3f * Side)
{
    if (Matrix && Front && Up && Side)
    {
        Matrix->m[0][0] = Side->x;
        Matrix->m[0][1] = Up->x;
        Matrix->m[0][2] = -Front->x;
        Matrix->m[0][3] = 0.0f;

        Matrix->m[1][0] = Side->y;
        Matrix->m[1][1] = Up->y;
        Matrix->m[1][2] = -Front->y;
        Matrix->m[1][3] = 0.0f;

        Matrix->m[2][0] = Side->z;
        Matrix->m[2][1] = Up->z;
        Matrix->m[2][2] = -Front->z;
        Matrix->m[2][3] = 0.0f;

        Matrix->m[3][0] = 0.0f;
        Matrix->m[3][1] = 0.0f;
        Matrix->m[3][2] = 0.0f;
        Matrix->m[3][3] = 1.0f;

        return Matrix;
    }

    return NULL;
}

bool Matrix4fInverse(struct Matrix4f * Inverse, struct Matrix4f * Matrix)
{
	if (Inverse && Matrix)
	{
		struct Matrix4f InverseMatrix;
		float Determinant;

		InverseMatrix.m[0][0] = Matrix->m[1][2] * Matrix->m[2][2] * Matrix->m[3][3] -
			Matrix->m[1][2] * Matrix->m[2][3] * Matrix->m[3][2] -
			Matrix->m[2][1] * Matrix->m[1][2] * Matrix->m[3][3] +
			Matrix->m[2][1] * Matrix->m[1][3] * Matrix->m[3][2] +
			Matrix->m[3][1] * Matrix->m[1][2] * Matrix->m[2][3] -
			Matrix->m[3][1] * Matrix->m[1][3] * Matrix->m[2][2];

		InverseMatrix.m[1][0] = -Matrix->m[1][1] * Matrix->m[2][2] * Matrix->m[3][3] +
			Matrix->m[1][1] * Matrix->m[2][3] * Matrix->m[3][2] +
			Matrix->m[2][0] * Matrix->m[1][2] * Matrix->m[3][3] -
			Matrix->m[2][0] * Matrix->m[1][3] * Matrix->m[3][2] -
			Matrix->m[3][0] * Matrix->m[1][2] * Matrix->m[2][3] +
			Matrix->m[3][0] * Matrix->m[1][3] * Matrix->m[2][2];

		InverseMatrix.m[2][0] = Matrix->m[1][1] * Matrix->m[2][1] * Matrix->m[3][3] -
			Matrix->m[1][1] * Matrix->m[2][3] * Matrix->m[3][1] -
			Matrix->m[2][0] * Matrix->m[1][2] * Matrix->m[3][3] +
			Matrix->m[2][0] * Matrix->m[1][3] * Matrix->m[3][1] +
			Matrix->m[3][0] * Matrix->m[1][2] * Matrix->m[2][3] -
			Matrix->m[3][0] * Matrix->m[1][3] * Matrix->m[2][1];

		InverseMatrix.m[3][0] = -Matrix->m[1][1] * Matrix->m[2][1] * Matrix->m[3][2] +
			Matrix->m[1][1] * Matrix->m[2][2] * Matrix->m[3][1] +
			Matrix->m[2][0] * Matrix->m[1][2] * Matrix->m[3][2] -
			Matrix->m[2][0] * Matrix->m[1][2] * Matrix->m[3][1] -
			Matrix->m[3][0] * Matrix->m[1][2] * Matrix->m[2][2] +
			Matrix->m[3][0] * Matrix->m[1][2] * Matrix->m[2][1];

		Determinant = Matrix->m[0][0] * InverseMatrix.m[0][0] + Matrix->m[0][1] * InverseMatrix.m[1][0] + Matrix->m[0][2] * InverseMatrix.m[2][0] + Matrix->m[0][3] * InverseMatrix.m[3][0];

		if (Determinant != 0.0f)
		{
			uinteger i;

			InverseMatrix.m[0][1] = -Matrix->m[0][1] * Matrix->m[2][2] * Matrix->m[3][3] +
				Matrix->m[0][1] * Matrix->m[2][3] * Matrix->m[3][2] +
				Matrix->m[2][1] * Matrix->m[0][2] * Matrix->m[3][3] -
				Matrix->m[2][1] * Matrix->m[0][3] * Matrix->m[3][2] -
				Matrix->m[3][1] * Matrix->m[0][2] * Matrix->m[2][3] +
				Matrix->m[3][1] * Matrix->m[0][3] * Matrix->m[2][2];

			InverseMatrix.m[1][1] = Matrix->m[0][0] * Matrix->m[2][2] * Matrix->m[3][3] -
				Matrix->m[0][0] * Matrix->m[2][3] * Matrix->m[3][2] -
				Matrix->m[2][0] * Matrix->m[0][2] * Matrix->m[3][3] +
				Matrix->m[2][0] * Matrix->m[0][3] * Matrix->m[3][2] +
				Matrix->m[3][0] * Matrix->m[0][2] * Matrix->m[2][3] -
				Matrix->m[3][0] * Matrix->m[0][3] * Matrix->m[2][2];

			InverseMatrix.m[2][1] = -Matrix->m[0][0] * Matrix->m[2][1] * Matrix->m[3][3] +
				Matrix->m[0][0] * Matrix->m[2][3] * Matrix->m[3][1] +
				Matrix->m[2][0] * Matrix->m[0][1] * Matrix->m[3][3] -
				Matrix->m[2][0] * Matrix->m[0][3] * Matrix->m[3][1] -
				Matrix->m[3][0] * Matrix->m[0][1] * Matrix->m[2][3] +
				Matrix->m[3][0] * Matrix->m[0][3] * Matrix->m[2][1];

			InverseMatrix.m[3][1] = Matrix->m[0][0] * Matrix->m[2][1] * Matrix->m[3][2] -
				Matrix->m[0][0] * Matrix->m[2][2] * Matrix->m[3][1] -
				Matrix->m[2][0] * Matrix->m[0][1] * Matrix->m[3][2] +
				Matrix->m[2][0] * Matrix->m[0][2] * Matrix->m[3][1] +
				Matrix->m[3][0] * Matrix->m[0][1] * Matrix->m[2][2] -
				Matrix->m[3][0] * Matrix->m[0][2] * Matrix->m[2][1];

			InverseMatrix.m[0][2] = Matrix->m[0][1] * Matrix->m[1][2] * Matrix->m[3][3] -
				Matrix->m[0][1] * Matrix->m[1][3] * Matrix->m[3][2] -
				Matrix->m[1][2] * Matrix->m[0][2] * Matrix->m[3][3] +
				Matrix->m[1][2] * Matrix->m[0][3] * Matrix->m[3][2] +
				Matrix->m[3][1] * Matrix->m[0][2] * Matrix->m[1][3] -
				Matrix->m[3][1] * Matrix->m[0][3] * Matrix->m[1][2];

			InverseMatrix.m[1][2] = -Matrix->m[0][0] * Matrix->m[1][2] * Matrix->m[3][3] +
				Matrix->m[0][0] * Matrix->m[1][3] * Matrix->m[3][2] +
				Matrix->m[1][1] * Matrix->m[0][2] * Matrix->m[3][3] -
				Matrix->m[1][1] * Matrix->m[0][3] * Matrix->m[3][2] -
				Matrix->m[3][0] * Matrix->m[0][2] * Matrix->m[1][3] +
				Matrix->m[3][0] * Matrix->m[0][3] * Matrix->m[1][2];

			InverseMatrix.m[2][2] = Matrix->m[0][0] * Matrix->m[1][2] * Matrix->m[3][3] -
				Matrix->m[0][0] * Matrix->m[1][3] * Matrix->m[3][1] -
				Matrix->m[1][1] * Matrix->m[0][1] * Matrix->m[3][3] +
				Matrix->m[1][1] * Matrix->m[0][3] * Matrix->m[3][1] +
				Matrix->m[3][0] * Matrix->m[0][1] * Matrix->m[1][3] -
				Matrix->m[3][0] * Matrix->m[0][3] * Matrix->m[1][2];

			InverseMatrix.m[3][2] = -Matrix->m[0][0] * Matrix->m[1][2] * Matrix->m[3][2] +
				Matrix->m[0][0] * Matrix->m[1][2] * Matrix->m[3][1] +
				Matrix->m[1][1] * Matrix->m[0][1] * Matrix->m[3][2] -
				Matrix->m[1][1] * Matrix->m[0][2] * Matrix->m[3][1] -
				Matrix->m[3][0] * Matrix->m[0][1] * Matrix->m[1][2] +
				Matrix->m[3][0] * Matrix->m[0][2] * Matrix->m[1][2];

			InverseMatrix.m[0][3] = -Matrix->m[0][1] * Matrix->m[1][2] * Matrix->m[2][3] +
				Matrix->m[0][1] * Matrix->m[1][3] * Matrix->m[2][2] +
				Matrix->m[1][2] * Matrix->m[0][2] * Matrix->m[2][3] -
				Matrix->m[1][2] * Matrix->m[0][3] * Matrix->m[2][2] -
				Matrix->m[2][1] * Matrix->m[0][2] * Matrix->m[1][3] +
				Matrix->m[2][1] * Matrix->m[0][3] * Matrix->m[1][2];

			InverseMatrix.m[1][3] = Matrix->m[0][0] * Matrix->m[1][2] * Matrix->m[2][3] -
				Matrix->m[0][0] * Matrix->m[1][3] * Matrix->m[2][2] -
				Matrix->m[1][1] * Matrix->m[0][2] * Matrix->m[2][3] +
				Matrix->m[1][1] * Matrix->m[0][3] * Matrix->m[2][2] +
				Matrix->m[2][0] * Matrix->m[0][2] * Matrix->m[1][3] -
				Matrix->m[2][0] * Matrix->m[0][3] * Matrix->m[1][2];

			InverseMatrix.m[2][3] = -Matrix->m[0][0] * Matrix->m[1][2] * Matrix->m[2][3] +
				Matrix->m[0][0] * Matrix->m[1][3] * Matrix->m[2][1] +
				Matrix->m[1][1] * Matrix->m[0][1] * Matrix->m[2][3] -
				Matrix->m[1][1] * Matrix->m[0][3] * Matrix->m[2][1] -
				Matrix->m[2][0] * Matrix->m[0][1] * Matrix->m[1][3] +
				Matrix->m[2][0] * Matrix->m[0][3] * Matrix->m[1][2];

			InverseMatrix.m[3][3] = Matrix->m[0][0] * Matrix->m[1][2] * Matrix->m[2][2] -
				Matrix->m[0][0] * Matrix->m[1][2] * Matrix->m[2][1] -
				Matrix->m[1][1] * Matrix->m[0][1] * Matrix->m[2][2] +
				Matrix->m[1][1] * Matrix->m[0][2] * Matrix->m[2][1] +
				Matrix->m[2][0] * Matrix->m[0][1] * Matrix->m[1][2] -
				Matrix->m[2][0] * Matrix->m[0][2] * Matrix->m[1][2];

			// Calculate the determinant
			Determinant = 1.0f / Determinant;

			for (i = 0; i < 16; ++i)
			{
				Inverse->v[i] = InverseMatrix.v[i] * Determinant;
			}

			return true;
		}
	}

	return false;
}
