/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
//	Terrain texturing and lighting functions based on
//	Adrian Welbourn geomipmaps implementation.
//
//	TODO: this module should be reimplemented from strach
//			because of the poor implementation desing and
//			extended in order to allow triplanar texture
//			mapping and pixel shader effects.
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/TerrainBase.h>
#include <World/TerrainRender.h>
#include <World/TerrainTexture.h>

#include <Math/General.h>
#include <Math/Geometry/Vector.h>

#include <Memory/General.h>

#include <Graphics/Device.h>
#include <Graphics/Texture.h>
#include <Graphics/TextureLoader.h>

#include <System/IOHelper.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Put a pixel value into the light map
////////////////////////////////////////////////////////////
void TerrainTextureLightMapPut(uint8 * LightMap, uinteger Size, uinteger X, uinteger Y, uint8 Value)
{
	uinteger Mask = Size - 1;

	// Assume size is a power of two
	X = X & Mask;
	Y = Y & Mask;

	LightMap[X + Y * Size] = Value;
}

////////////////////////////////////////////////////////////
/// Get a pixel value from the light map
////////////////////////////////////////////////////////////
uint8 TerrainTextureLightMapGet(uint8 * LightMap, uinteger Size, uinteger X, uinteger Y)
{
	uinteger Mask = Size - 1;

	// Assume size is a power of two
	X = X & Mask;
	Y = Y & Mask;

	return LightMap[X + Y * Size];
}

////////////////////////////////////////////////////////////
/// Get a normal from the normal map
////////////////////////////////////////////////////////////
int8 * TerrainTextureNormalMapGet(int8 * NormalMap, uinteger Size, uinteger X, uinteger Y)
{
	uinteger Mask = Size - 1;

	// Assume size is a power of two
	X = X & Mask;
	Y = Y & Mask;

	return &NormalMap[3 * (X + Y * Size)];
}

////////////////////////////////////////////////////////////
/// Helper function for drawing into the shadow map.
/// Used when casting a ray from a heightmap point.
/// If the height of the heightmap at point x, y is less
/// than the height z of the ray at point x, y then a full
/// darkness shadow pixel is drawn.
////////////////////////////////////////////////////////////
void TerrainTextureShadowMapPut(uint8 * ShadowMap, uint8 * HeightMap, uinteger Size, uinteger X, uinteger Y, float Z)
{
	uinteger Mask = Size - 1;

	// Assume size is a power of two
	X = X & Mask;
	Y = Y & Mask;

	if (HeightMap[X + Y * (Size + 1)] <= Z)
	{
		ShadowMap[X + Y * Size] = 255;
	}
}

////////////////////////////////////////////////////////////
/// Bump map calculation that is used to apply some noise
/// to the normal map. This isn't a bump map in the usual
/// graphics sense, its just random noise that is used to
/// vary the texture applied to flat areas of the map as if
/// the terrain were more bumpy.
////////////////////////////////////////////////////////////
uint8 * TerrainTextureBumpMapGenerate(uinteger Size)
{
	uint8 * BumpMap = (uint8 *)MemoryAllocate(sizeof(uint8) * sqr(Size));

	if (BumpMap)
	{
		uinteger X, Y;

		for (Y = 0; Y < Size; ++Y)
		{
			for (X = 0; X < Size; ++X)
			{
				float Noise = (float)rand() / (float)RAND_MAX;

				if (Noise > 1.0f)
				{
					Noise = 1.0f;
				}

				BumpMap[Y * Size + X] = (uint8)(255.0f * Noise);
			}
		}

		return BumpMap;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Calculate the bump map intensity for a given point
////////////////////////////////////////////////////////////
float TerrainTextureBumpMapIntensity(float X, float Y, uint8 * BumpMap, uinteger BumpMapSize)
{
	struct Vector2i LowerBound, UpperBound;

	Vector2iSet(&LowerBound, (integer)floorf(X), (integer)floorf(Y));
	Vector2iSet(&UpperBound, (integer)ceilf(X), (integer)ceilf(Y));

	if (LowerBound.x == UpperBound.x && LowerBound.y == UpperBound.y)
	{
		// Heightmap point corresponds to an exact bump map point
		return (float)TerrainTextureLightMapGet(BumpMap, BumpMapSize, LowerBound.x, LowerBound.y);
	}
	else
	{
		float Bump, LerpScale;

		// Heightmap point falls between bump map points, interpolate
		float b00 = TerrainTextureLightMapGet(BumpMap, BumpMapSize, LowerBound.x, LowerBound.y);
		float b01 = TerrainTextureLightMapGet(BumpMap, BumpMapSize, UpperBound.x, LowerBound.y);
		float b10 = TerrainTextureLightMapGet(BumpMap, BumpMapSize, LowerBound.x, UpperBound.y);
		float b11 = TerrainTextureLightMapGet(BumpMap, BumpMapSize, UpperBound.x, UpperBound.y);
		float b0 = b00;
		float b1 = b10;

		if (LowerBound.x != UpperBound.x)
		{
			// Lerp x
			LerpScale = (float)UpperBound.x - X;
			b0 *= LerpScale;
			b1 *= LerpScale;

			LerpScale = X - (float)LowerBound.x;
			b0 += LerpScale * b01;
			b1 += LerpScale * b11;
		}

		Bump = b0;

		if (LowerBound.y != UpperBound.y)
		{
			// Lerp y
			LerpScale = (float)UpperBound.y - Y;
			Bump *= LerpScale;

			LerpScale = Y - (float)LowerBound.y;
			Bump += LerpScale * b1;
		}

		return Bump;
	}
}

////////////////////////////////////////////////////////////
/// Apply noise and re-normalize. Assuming z is near 1 and
///	x, y and noise b are small compared to z, we have to add
///	something to x & y otherwise when we re-normalize,
/// z returns to near its previous value. The something
///	added to x & y should tend to keep the normal's length
///	unchanged so that when we re-normalize, z retains
///	its adjusted value.
///
/// Since
///
///	x^2 + y^2 + z'^2  approx=  x^2 + y^2 + z^2 - 2b
///
///	if we add sqrt(b) to x and y (where b == Bump) we get:
///
///	x'^2 + y'^2 + z'^2  approx=  x^2 + b + y^2 + b + z^2 - 2b
///
/// which is equivalent to:
///
///	x^2 + y^2 + z^2
///
////////////////////////////////////////////////////////////
void TerrainTextureBumpMapNoise(float Bump, float BumpMapScale, struct Vector3f * Normal)
{
	Bump *= BumpMapScale;

	Normal->z -= Bump;

	Bump = sqrtf(Bump);

	Normal->x += Bump;
	Normal->y += Bump;

	if (Normal->z < 0.0f)
	{
		Normal->z = 0.0f;
	}

	Vector3fNormalize(Normal);
}

////////////////////////////////////////////////////////////
/// Apply some noise from the bump map if normal z
/// is above the threshold
////////////////////////////////////////////////////////////
void TerrainTextureBumpMapNoiseNormal(float X, float Y, uint8 * BumpMap, uinteger BumpMapSize, float BumpMapScale, struct Vector3f * Normal)
{
	float Bump = TerrainTextureBumpMapIntensity(X, Y, BumpMap, BumpMapSize);

	// Apply noise and re-normalize
	TerrainTextureBumpMapNoise(Bump, BumpMapScale, Normal);
}

////////////////////////////////////////////////////////////
/// Generates a normal for each heightmap vertex by
/// calculating a normal for each of the eight triangles
/// sharing the vertex and averaging. The three components
/// of the normal are scaled to the range +-127 and
/// stored as signed int8 values.
///
/// If NoiseScale and Frequency are both greater than zero,
/// a bump map (above) is generated with one point for
/// every frequency points of the normal map, e.g. if the
/// normal map is n x n and frequency is 4, the bump map will
/// be n / 4 x n / 4 and will then be interpolated across
/// the normal map, smoothing the noise out.
/// The CutOff parameter is the lower limit to Normal.z above
/// which noise is applied. This allows noise to only be
/// applied to the flattest parts of the map if required.
///
/// The bump map is deleted on completion.
////////////////////////////////////////////////////////////
int8 * TerrainTextureNormalMapGenerate(uint8 * HeightMap, uinteger Size, struct Vector3f * Scale, float NoiseScale, float CutOff, uinteger Frequency)
{
	uint8 * BumpMap = NULL;
	uinteger BumpMapSize;
	float ScaleBump = NoiseScale / 255.0f;
	float InvFrequency = 1.0f;
	int8 * NormalMap;
	uinteger X, Y;
	uinteger i, j;
	struct Vector3f Normal;

	// Eight vectors connecting a point in the heightmap to each of its neighbors
	float v[24] = { -1, -1,  0,
					 0, -1,  0,
					 1, -1,  0,
					 1,  0,  0,
					 1,  1,  0,
					 0,  1,  0,
					-1,  1,  0,
					-1,  0,  0 };

	// Calculate the bumpmap
	if (NoiseScale > 0.0f && Frequency > 0)
	{
		if (Frequency > Size)
		{
			Frequency = Size;
		}

		BumpMapSize = Size / Frequency;

		BumpMap = TerrainTextureBumpMapGenerate(BumpMapSize);

		InvFrequency = 1.0f / Frequency;
	}

	// Allocate space for the normal map
	NormalMap = (int8 *)MemoryAllocate(3 * sqr(Size));

	if (!NormalMap)
	{
		MemoryDeallocate(BumpMap);
		return NormalMap;
	}

	// For each point in the heightmap
	for (Y = 0; Y < Size; ++Y)
	{
		for (X = 0; X < Size; ++X)
		{
			// Get the eight vectors connecting this point to its neighbors
			for (i = 0; i < 8; ++i)
			{
				int x, y, z;

				// Assume Size is a power of two
				x = (X + (int)(v[3*i + 0])) & (Size - 1);
				y = (Y + (int)(v[3*i + 1])) & (Size - 1);
				z = (int)HeightMap[x + y * (Size + 1)] - (int)HeightMap[X + Y * (Size + 1)];

				v[3*i + 2] = (float)z;
			}

			// Calculate the eight cross products and average
			Vector3fSetNull(&Normal);

			for (i = 0, j = 1; i < 8; ++i, ++j)
			{
				struct Vector3f EdgeVector, temp, temp0, temp1;

				if (j >= 8)
				{
					j = 0;
				}

				Vector3fSet(&EdgeVector, v[i * 3], v[i * 3 + 1], v[i * 3 + 2]);
				Vector3fMultiplyVectorEx(&temp0, &EdgeVector, Scale);

				Vector3fSet(&EdgeVector, v[j * 3], v[j * 3 + 1], v[j * 3 + 2]);
				Vector3fMultiplyVectorEx(&temp1, &EdgeVector, Scale);

				Vector3fCross(&temp0, &temp1, &temp);

				// Make sure the normal points up
				if (temp.z < 0.0f)
				{
					Vector3fMultiply(&temp, -1.0f);
				}

				Vector3fAdd(&Normal, &temp);
			}

			Vector3fNormalize(&Normal);

			// If we have a bump map and Normal.z is above the threshold, apply some noise
			if (Normal.z >= CutOff && BumpMap)
			{
				float fbX = (float)X * InvFrequency;
				float fbY = (float)Y * InvFrequency;

				TerrainTextureBumpMapNoiseNormal(fbX, fbY, BumpMap, BumpMapSize, ScaleBump, &Normal);
			}

			// Save the normal in the normal map
			NormalMap[3 * (X + Y * Size) + 0] = (int8)(127.0f * Normal.x);
			NormalMap[3 * (X + Y * Size) + 1] = (int8)(127.0f * Normal.y);
			NormalMap[3 * (X + Y * Size) + 2] = (int8)(127.0f * Normal.z);
		}
	}

	// Delete the bump map
	MemoryDeallocate(BumpMap);

	return NormalMap;
}

////////////////////////////////////////////////////////////
///	For each point in the heightmap we cast a ray in the
///	light direction. Any points intersected in x-y which
///	are below the ray in z are deemed to be in shadow.
////////////////////////////////////////////////////////////
uint8 * TerrainTextureShadowsGenerate(uint8 * HeightMap, uinteger Size, struct Vector3f * Scale, struct Vector3f * Direction)
{
	const float ScaleNormal = 1.0f / 127.0f;
	uint8 * ShadowMap;
	struct Vector3f LightDirection;
	uinteger X, Y;

	// Allocate space for the shadow map
	ShadowMap = (unsigned char*)MemoryAllocate(sqr(Size));

	if (!ShadowMap)
    {
		return ShadowMap;
	}

	MemorySet(ShadowMap, 0, sqr(Size));

	// Make sure the light source is normalized
	Vector3fCopy(&LightDirection, Direction);
	Vector3fDivideVectorEx(&LightDirection, &LightDirection, Scale);
	Vector3fNormalize(&LightDirection);

	if (LightDirection.z == 0.0f)
    {
		// Pathological case
		return ShadowMap;
	}

	// For each heightmap vertex
	for (Y = 0; Y < Size; ++Y)
	{
		for (X = 0; X < Size; ++X)
		{
			float Height;

			// If vertex already in shadow ignore it
			if (ShadowMap[X + Y * Size])
				continue;

			// Step along a line through the vertex in the direction of light
			Height = (float)HeightMap[X + Y * (Size + 1)];

			if (fabsf(LightDirection.x) < fabsf(LightDirection.y))
            {
				float incx = LightDirection.x / LightDirection.y;
				float incz = LightDirection.z / LightDirection.y;
				integer y, incy = 1;
				float x;

				if (LightDirection.y < 0.0f)
                {
					incx = -incx; incy = -incy, incz = -incz;
				}

				x = X + incx;
				Height += incz;

				for (y = Y + incy; Height >= 0.0f; x += incx, y += incy, Height += incz)
				{
					TerrainTextureShadowMapPut(ShadowMap, HeightMap, Size, (uinteger)x, y, Height);
				}
			}
			else
			{
				float incy = LightDirection.y / LightDirection.x;
				float incz = LightDirection.z / LightDirection.x;
				integer x, incx = 1;
				float y;

				if (LightDirection.x < 0.0f)
                {
					incx = -incx; incy = -incy, incz = -incz;
				}

				y = Y + incy;
				Height += incz;

				for (x = X + incx; Height >= 0.0f; x += incx, y += incy, Height += incz)
				{
					TerrainTextureShadowMapPut(ShadowMap, HeightMap, Size, x, (uinteger)y, Height);
				}
			}
		}
	}

	return ShadowMap;
}

////////////////////////////////////////////////////////////
/// Applies a simple blurring filter to the light map.
/// The blurring filter is first weighted according
/// to the light direction.
////////////////////////////////////////////////////////////
void TerrainTextureLightMapBlur(unsigned char *lightMap, int Size, struct Vector3f * Direction)
{
	char *lightMap2;
	int blurMap[9] = { 64,  64,  64,
					   64, 255,  64,
					   64,  64,  64 };
	int X, Y, i, divisor = 0;

	lightMap2 = (unsigned char*)MemoryAllocate(sqr(Size));
	if (!lightMap2) {
		//err_report("lsc_blurLightMap: cannot allocate %d bytes", sqr(Size));
		return;
	}

	MemorySet(lightMap2, 0, sqr(Size));

	if (Direction->x > 0.6f) {
		blurMap[2] = blurMap[5] = blurMap[8] = 128;
	}
	else if (Direction->x < 0.6f) {
		blurMap[0] = blurMap[3] = blurMap[6] = 128;
	}
	if (Direction->y > 0.6f) {
		blurMap[0] = blurMap[1] = blurMap[2] = 128;
	}
	else if (Direction->y < 0.6f) {
		blurMap[6] = blurMap[7] = blurMap[8] = 128;
	}

	for (i = 0; i < 9; i++)
		divisor += blurMap[i];

	// For each heightmap vertex
	for (Y = 0; Y < Size; Y++) {
		for (X = 0; X < Size; X++) {
			int x, y, accum = 0;
			for (y = 0; y < 3; y++) {
				for (x = 0; x < 3; x++) {
					accum += blurMap[x + 3*y]
						* TerrainTextureLightMapGet(lightMap, Size, X+x-1, Y+y-1);
				}
			}
			accum /= divisor;
			if (accum > 255)
				accum = 255;
			TerrainTextureLightMapPut(lightMap2, Size, X, Y, (unsigned char)accum);
		}
	}

	MemoryCopy(lightMap, lightMap2, sqr(Size));

	MemoryDeallocate(lightMap2);
}

////////////////////////////////////////////////////////////
/// Light intensity is calculated for each point in the
/// heightmap, scaled to range [0, 255] and
/// stored in lightmap
////////////////////////////////////////////////////////////
void TerrainTextureLightMapGenerateIntensity(uint8 * LightMap, int8 * NormalMap, uint8 * ShadowMap, uinteger Size,
	struct Vector3f * LightDirection, float Ambient, float Diffuse)
{
	const float ScaleNormal = 1.0f / 127.0f;
	const float ScaleShadow = 1.0f / 255.0f;

	uinteger X, Y;

	// For each point
	for (Y = 0; Y < Size; ++Y)
	{
		for (X = 0; X < Size; ++X)
		{
		    struct Vector3f Normal;
			float Light, Shadow, Distance;

            uinteger LightMapIndex = (X + Y * Size);
			uinteger NormalMapIndex = 3 * LightMapIndex;

			// Get the dot product with the light source direction
			Normal.x = NormalMap[NormalMapIndex];
			Normal.y = NormalMap[NormalMapIndex + 1];
			Normal.z = NormalMap[NormalMapIndex + 2];

			Vector3fMultiply(&Normal, ScaleNormal);

			Distance = Vector3fDot(LightDirection, &Normal);

			if (Distance < 0.0f)
            {
				Distance = 0.0f;
            }

			// Calculate illumination model (ambient plus diffuse)
			if (ShadowMap)
            {
				Shadow = 1.0f - ScaleShadow * ShadowMap[X + Y * Size];
            }
            else
            {
                Shadow = 1.0f;
            }

			Light = Ambient + Shadow * Distance * Diffuse;

			// Save in light map
			LightMap[LightMapIndex] = (uint8)(clamp(Light, 0.0f, 1.0f) * 255.0f);
		}
	}
}

////////////////////////////////////////////////////////////
/// Calculate light map from normal and shadow map and
/// then blurring filter to soften the shadow edges
////////////////////////////////////////////////////////////
uint8 * TerrainTextureLightMapGenerate(int8 * NormalMap, uint8 * ShadowMap, uinteger Size,
	struct Vector3f * Scale, float Ambient, float Diffuse, struct Vector3f * Direction)
{
	// Allocate space for the light map
	uint8 * LightMap = (uint8 *)MemoryAllocate(sqr(Size));

	if (LightMap)
    {
        struct Vector3f LightDirection;

        // Make sure the light source is normalized
        Vector3fCopy(&LightDirection, Direction);
        Vector3fDivideVectorEx(&LightDirection, &LightDirection, Scale);
        Vector3fNormalize(&LightDirection);
        Vector3fMultiply(&LightDirection, -1.0f);

        TerrainTextureLightMapGenerateIntensity(LightMap, NormalMap, ShadowMap, Size, &LightDirection, Ambient, Diffuse);

        // Blur the lightmap
        TerrainTextureLightMapBlur(LightMap, Size, &LightDirection);
        TerrainTextureLightMapBlur(LightMap, Size, &LightDirection);
    }

	return LightMap;
}

/*
  calcTextures()

  mode == 0:
  ---------

  This calculates one big texture map for the entire landscape, referred to as
  the colourmap.

  Up to ten texture tga files are loaded and scaled in size by the factor
  texScale. The files must be named colourn.tga, where n = 0 through 9. The
  file with the lowest n is taken to represent the flattest terrain. The file
  with the highest n is taken to represent the steepest terrain. E.g.

    colour0    grass
    colour1	   mud
    colour2	   sand
    colour3	   rock

  One big texture map, size (texPerSide x texSize)^2 is allocated in memory.
  For each point in the texture map a corresponding point in the heightmap
  is found and the values of normal's z and light are read from the normal and
  light maps (or more accurately, because the texture map point may lie
  between heightmap points, the values of normal's z & light are linearly
  interpolated from the surrounding 4 heightmap points). The normal's z is
  converted to a gradient which is then scaled into the range 0 to N-1, where
  N is the number of texture maps read. The scaled gradient is then used as an
  index to the required texture file from which a texel is copied to the
  heightmap texture (or if the scaled gradient is non-integer, two texels are
  read from different texture files and the final texel is interpolated). If
  the scaled up texture files are smaller than the colourmap they will be
  tiled. Finally the texel value may be scaled by the light value (can be
  enabled / disabled with the prelit flag).

  The colourmap is created in GL_RGB format (24bpp).


  mode == 1:
  ---------

  This calculates the blend maps used for alpha blended texture splatting.

  Several blend maps are allocated sequentially in memory, each size
  hmSize x hmSize. The number of blend maps created depends on the textures to
  be used in texture splatting. The texture files must be named texturen.tga,
  where n = 0 through 9. The file with the lowest n is taken to represent the
  flattest terrain. The file with the highest n is taken to represent the
  steepest terrain. E.g.

    texture0    grass
    texture1    mud
    texture2    sand
    texture3    rock

  One blend map is created for each texture map, plus one blend map for the
  light map (e.g. 4 textures produces one light map plus 4 blend maps).

  The blend maps can be created in one of two formats, GL_ALPHA (8bpp) or
  GL_RGBA (32bpp), depending on the value of the colourmap argument. If
  colourmap is null, GL_ALPHA format is used. If colourmap is non-null, it
  is interpreted as a pointer to the colourmap created by mode 0 (above). In
  this case the blend maps are created with GL_RGBA format and the colourmap
  is copied into the RGB channels of each blend map.

*/
static unsigned char *lsc_calcTextures(int mode, bool prelit,
									   uint8 * heightMap,
									   int8 * NormalMap,
									   uint8 * lightMap,
									   uint8 * colourMap,
									   int hmSize,
									   int texPerSide, int texSize,
									   float texScale, int *numTextures)
{
	const float ScaleNormal = 1.0f / 127.0f;
	const float ScaleLights = 1.0f / 255.0f;

	char filename[256];
	int countLoaded = 0;
	int Size[10];
	unsigned char *data[10];
	unsigned char *textureMap;
	int textureMapSize, i, X, Y;
	float invTexScale = 1.0f / texScale;

	// Number of texture maps created
	*numTextures = 0;

	// Load up to ten texture files, must be square
	for (i = 0; i < 3; i++)
	{
		struct Texture TextureData;

		if (mode == 0)
		{
			// If colour%d.tga found
			sprintf(filename, "data/geomipmaps/test/color%d.tga", i);
		}
		else
		{	// If texture%d.tga found and is square
			sprintf(filename, "data/geomipmaps/hq/texture%d.tga", i);
		}

		if (TextureLoadFromFile(filename, &TextureData) && TextureData.Width == TextureData.Height)
		{
			if (mode == 0) {
				if (TextureData.Channels == 4)
				{
					uinteger i;

					for (i = 0; i < TextureData.Width * TextureData.Height; ++i)
					{
						TextureData.Pixels[i * 3] = TextureData.Pixels[i * 4];
						TextureData.Pixels[i * 3 + 1] = TextureData.Pixels[i * 4 + 1];
						TextureData.Pixels[i * 3 + 2] = TextureData.Pixels[i * 4 + 2];
					}

					TextureData.Channels = 3;
				}

				// Copy into memory, scaling if necessary
				Size[countLoaded] = (int)(TextureData.Width * texScale);
				data[countLoaded] = (unsigned char*)MemoryAllocate(TextureData.Channels * sqr(Size[countLoaded]));

				if (!data[countLoaded]) {
					//err_report("lsc_calcTextures: cannot allocate %d bytes", 3*sqr(Size[countLoaded]));
					continue;
				}

				if (Size[countLoaded] == (int)TextureData.Width)
				{
					// Copy texture to memory
					MemoryCopy(data[countLoaded], TextureData.Pixels, TextureData.Channels * sqr(Size[countLoaded]));
				}
				else {
					// Copy texture into memory, scaling by texScale
					int j, k;
					for (k = 0; k < Size[countLoaded]; k++) {
						float fsk = k * invTexScale;
						int sk0 = (int)floor(fsk);
						int sk1 = (int)ceil(fsk);

						if (sk0 >= (int)TextureData.Width)
							sk0 = (int)TextureData.Width - 1;
						if (sk1 >= (int)TextureData.Width)
							sk1 = (int)TextureData.Width - 1;

						for (j = 0; j < Size[countLoaded]; j++) {
							float fsj = j * invTexScale;
							int sj0 = (int)floor(fsj);
							int sj1 = (int)ceil(fsj);

							if (sj0 >= (int)TextureData.Width)
								sj0 = (int)TextureData.Width - 1;
							if (sj1 >= (int)TextureData.Width)
								sj1 = (int)TextureData.Width - 1;

							if (sk0 == sk1 && sj0 == sj1) {
								MemoryCopy(
									&data[countLoaded][TextureData.Channels*(k*Size[countLoaded] + j)],
									&(TextureData.Pixels[TextureData.Channels * (sk0*TextureData.Width + sj0)]),
									TextureData.Channels);
							}
							else {
								// Lerp texels
								unsigned char ctex[3];
								float Scale;
								uinteger l;
								float tex00[3], tex01[3], tex10[3], tex11[3];
								float tex0[3], tex1[3];
								float tex[3];
								MemoryCopy(tex00, &(TextureData.Pixels[TextureData.Channels*(sk0*TextureData.Width + sj0)]), TextureData.Channels);
								MemoryCopy(tex01, &(TextureData.Pixels[TextureData.Channels*(sk0*TextureData.Width + sj1)]), TextureData.Channels);
								MemoryCopy(tex10, &(TextureData.Pixels[TextureData.Channels*(sk1*TextureData.Width + sj0)]), TextureData.Channels);
								MemoryCopy(tex11, &(TextureData.Pixels[TextureData.Channels*(sk1*TextureData.Width + sj1)]), TextureData.Channels);
								MemoryCopy(tex0, tex00, TextureData.Channels);
								MemoryCopy(tex1, tex10, TextureData.Channels);
								if (sj0 != sj1) {
									// Lerp j
									Scale = (float)sj1 - fsj;
									for (l = 0; l < TextureData.Channels; l++) {
										tex0[l] *= Scale;
										tex1[l] *= Scale;
									}
									Scale = fsj - (float)sj0;
									for (l = 0; l < TextureData.Channels; l++) {
										tex0[l] += Scale * tex01[l];
										tex1[l] += Scale * tex11[l];
									}
								}
								MemoryCopy(tex, tex0, TextureData.Channels);
								if (sk0 != sk1) {
									// Lerp k
									Scale = (float)sk1 - fsk;
									for (l = 0; l < TextureData.Channels; l++)
										tex[l] *= Scale;
									Scale = fsk - (float)sk0;
									for (l = 0; l < 3; l++)
										tex[l] += Scale * tex1[l];
								}
								for (l = 0; l < TextureData.Channels; l++) {
									if (tex[l] < 0.0f)
										tex[l] = 0.0f;
									else if (tex[l] > 255.0f)
										tex[l] = 255.0f;
									ctex[l] = (unsigned char)(tex[l]);
								}
								MemoryCopy(&data[countLoaded][TextureData.Channels*(k*Size[countLoaded] + j)],
									ctex, 3);
							}
						}
					}
				}
			}

			countLoaded++;
		}

		MemoryDeallocate(TextureData.Pixels);
	}

	if (mode == 0) {

		//printf("\nMerge textures");
		//fflush(stdout);

		// Now allocate memory for the colourmap
		textureMapSize = texPerSide * texSize;
		textureMap = (unsigned char*)MemoryAllocate(3*sqr(textureMapSize));
		if (!textureMap) {
			//err_report("lsc_calcTextures: cannot allocate %d bytes", 3*sqr(textureMapSize));
			for (i = 0; i < countLoaded; i++)
				MemoryDeallocate(data[i]);
			return textureMap;
		}

		// Number of texture maps created
		*numTextures = 1;
	}
	else {

		// mode == 1

		// AW TODO: currently the engine can only handle three splat textures
		if (countLoaded > 3)
			countLoaded = 3;

		// One to one for blend maps
		textureMapSize = hmSize;
		if (colourMap) {
			textureMap = (unsigned char*)MemoryAllocate(4*(countLoaded+1)*sqr(textureMapSize));
			if (!textureMap) {
				//err_report("lsc_calcTextures: cannot allocate %d bytes", 4*countLoaded*sqr(textureMapSize));
				return textureMap;
			}
		}
		else {
			textureMap = (unsigned char*)MemoryAllocate((countLoaded+1)*sqr(textureMapSize));
			if (!textureMap) {
				//err_report("lsc_calcTextures: cannot allocate %d bytes", countLoaded*sqr(textureMapSize));
				return textureMap;
			}
		}

		// Number of texture maps created
		*numTextures = countLoaded + 1;
	}

	// Tile the source textures into the colourmap based on height map
	// gradient, i.e. the size of the height map normal's z component
	for (Y = 0; Y < textureMapSize; Y++) {

		// Scale y into the height map
		float fhY = (float)Y * (float)hmSize / (float)textureMapSize;
		int hY0 = (int)floor(fhY);
		int hY1 = (int)ceil(fhY);

		for (X = 0; X < textureMapSize; X++) {
			float r, g, b;

			// Scale x into the height map
			float fhX = (float)X * (float)hmSize / (float)textureMapSize;
			int hX0 = (int)floor(fhX);
			int hX1 = (int)ceil(fhX);

			// Get gradient & light, lerping if necessary
			float grad;
			float light;
			if (hX0 == hX1 && hY0 == hY1) {
				grad = TerrainTextureNormalMapGet(NormalMap, hmSize, hX0, hY0)[2];
				light = TerrainTextureLightMapGet(lightMap, hmSize, hX0, hY0);
			}
			else {
				float Scale;
				float grad00 = TerrainTextureNormalMapGet(NormalMap, hmSize, hX0, hY0)[2];
				float grad01 = TerrainTextureNormalMapGet(NormalMap, hmSize, hX1, hY0)[2];
				float grad10 = TerrainTextureNormalMapGet(NormalMap, hmSize, hX0, hY1)[2];
				float grad11 = TerrainTextureNormalMapGet(NormalMap, hmSize, hX1, hY1)[2];
				float light00 = TerrainTextureLightMapGet(lightMap, hmSize, hX0, hY0);
				float light01 = TerrainTextureLightMapGet(lightMap, hmSize, hX1, hY0);
				float light10 = TerrainTextureLightMapGet(lightMap, hmSize, hX0, hY1);
				float light11 = TerrainTextureLightMapGet(lightMap, hmSize, hX1, hY1);
				float grad0 = grad00;
				float grad1 = grad10;
				float light0 = light00;
				float light1 = light10;
				if (hX0 != hX1) {
					// Lerp x
					Scale = (float)hX1 - fhX;
					grad0 *= Scale;
					grad1 *= Scale;
					light0 *= Scale;
					light1 *= Scale;
					Scale = fhX - (float)hX0;
					grad0 += Scale * grad01;
					grad1 += Scale * grad11;
					light0 += Scale * light01;
					light1 += Scale * light11;
				}
				grad = grad0;
				light = light0;
				if (hY0 != hY1) {
					// Lerp y
					Scale = (float)hY1 - fhY;
					grad *= Scale;
					light *= Scale;
					Scale = fhY - (float)hY0;
					grad += Scale * grad1;
					light += Scale * light1;
				}
			}
			// Scale grad to range 0 to 1
			grad *= ScaleNormal;

			// Convert grad from normal's z to true gradient in range 0 to 1
			grad = 2.0f * acosf(grad) / MATH_PI;
			if (grad < 0.0f)
				grad = 0.0f;
			else if (grad > 1.0f)
				grad = 1.0f;

			// Scale light to range 0 to 1
			light *= ScaleLights;
			if (light < 0.0f)
				light = 0.0f;
			else if (light > 1.0f)
				light = 1.0f;

			if (mode == 0) {

				// Pixel colour if no textures loaded
				r = 200.0;
				g =  25.0;
				b = 200.0;

				// Use the gradient to get indexes to the source textures
				if (countLoaded) {
					float fIndex = grad * (float)(countLoaded - 1);
					int Index0 = (int)floor(fIndex);
					int Index1 = (int)ceil(fIndex);
					int sourceSize = Size[Index0];
					int sourceX = X%sourceSize;
					int sourceY = Y%sourceSize;

					r = data[Index0][3*(sourceY*sourceSize + sourceX) + 0];
					g = data[Index0][3*(sourceY*sourceSize + sourceX) + 1];
					b = data[Index0][3*(sourceY*sourceSize + sourceX) + 2];

					if (Index0 != Index1) {

						// Lerp the texture between Index0 and Index1
						float r2, g2, b2;
						float Scale = (float)Index1 - fIndex;
						r *= Scale;
						g *= Scale;
						b *= Scale;

						sourceSize = Size[Index1];
						sourceX = X%sourceSize;
						sourceY = Y%sourceSize;

						r2 = data[Index1][3*(sourceY*sourceSize + sourceX) + 0];
						g2 = data[Index1][3*(sourceY*sourceSize + sourceX) + 1];
						b2 = data[Index1][3*(sourceY*sourceSize + sourceX) + 2];

						Scale = fIndex - (float)Index0;

						r += r2 * Scale;
						g += g2 * Scale;
						b += b2 * Scale;
					}
				}

				if (prelit) {
					// Apply lighting model
					r *= light;
					g *= light;
					b *= light;
				}

				// Range check
				if (r < 0.0f)
					r = 0.0f;
				else if (r > 255.0f)
					r = 255.0f;
				if (g < 0.0f)
					g = 0.0f;
				else if (g > 255.0f)
					g = 255.0f;
				if (b < 0.0f)
					b = 0.0f;
				else if (b > 255.0f)
					b = 255.0f;

				// Write to colourmap
				textureMap[3*(X + Y*textureMapSize) + 0] = (unsigned char)r;
				textureMap[3*(X + Y*textureMapSize) + 1] = (unsigned char)g;
				textureMap[3*(X + Y*textureMapSize) + 2] = (unsigned char)b;
			}
			else {

				// mode == 1

				// Use the gradient to get indexes to the source textures
				if (countLoaded) {
					float fIndex = grad * (float)(countLoaded - 1);
					unsigned char* map = textureMap;
					int Index0, Index1;
					float Scale0, Scale1;

					Index0 = (int)floor(fIndex);
					Index1 = (int)ceil(fIndex);
					if (Index1 == Index0)
						Index1++;
					Scale0 = (float)Index1 - fIndex;
					Scale1 = fIndex - (float)Index0;

					// Write to light map
					if (colourMap) {
						map[4 * (X + Y*textureMapSize) + 0] =
							colourMap[3 * (X + Y*textureMapSize) + 0];
						map[4 * (X + Y*textureMapSize) + 1] =
							colourMap[3 * (X + Y*textureMapSize) + 1];
						map[4 * (X + Y*textureMapSize) + 2] =
							colourMap[3 * (X + Y*textureMapSize) + 2];
						map[4 * (X + Y*textureMapSize) + 3] =
									(unsigned char)(light * 255.0f);
					}
					else {
						map[X + Y*textureMapSize] =
									(unsigned char)(light * 255.0f);
					}

					// Write to blend maps
					if (colourMap)
						map += 4*sqr(textureMapSize);
					else
						map += sqr(textureMapSize);
					for (i = 0; i < countLoaded; i++) {
						if (colourMap) {
							map[4 * (X + Y*textureMapSize) + 0] =
								colourMap[3 * (X + Y*textureMapSize) + 0];
							map[4 * (X + Y*textureMapSize) + 1] =
								colourMap[3 * (X + Y*textureMapSize) + 1];
							map[4 * (X + Y*textureMapSize) + 2] =
								colourMap[3 * (X + Y*textureMapSize) + 2];
							if (i == Index0)
								map[4 * (X + Y*textureMapSize) + 3] =
											(unsigned char)(Scale0 * light * 255.0f);
							else if (i == Index1)
								map[4 * (X + Y*textureMapSize) + 3] =
											(unsigned char)(Scale1 * light * 255.0f);
							else
								map[4 * (X + Y*textureMapSize) + 3] = 0;
						}
						else {
							if (i == Index0)
								map[X + Y*textureMapSize] =
											(unsigned char)(Scale0 * light * 255.0f);
							else if (i == Index1)
								map[X + Y*textureMapSize] =
											(unsigned char)(Scale1 * light * 255.0f);
							else
								map[X + Y*textureMapSize] = 0;
						}

						if (colourMap)
							map += 4*sqr(textureMapSize);
						else
							map += sqr(textureMapSize);
					}
				}
			}
		}
	}

	if (mode == 0) {

		// Free up temporary storage
		for (i = 0; i < countLoaded; i++)
			MemoryDeallocate(data[i]);
	}

	//printf("\n");
	//fflush(stdout);

	return textureMap;
}

/*
  createColourMapTexObjs()

  Create OpenGL texture object(s) from the colourmap.

  Once the colourmap has been calculated, it is cut up into texPerSide
  x texPerSide tiles, each of size texSize x texSize, and each tile is used
  to create an OpenGL texture object. Once this has been done, the colourmap
  is deleted.

  Lastly, another texture object is created based on the texture file
  terrain.tga, to be used as the detail texture for the landscape in
  multitexture rendering. The texture coordinates for the base texture are set
  up so that the baseTexture will occupy baseTexScale of a heightmap patch,
  e.g. baseTexScale = 0.25 means that the base texture tiles into a patch 4
  times in x and 4 times in y.

  Nuff said.
*/
//bool lsc_createColourMapTexObjs(lsc l, unsigned char *hm, float texScale)
bool TerrainTextureColorMapCreate(struct EntityType * Entity, float texScale)
{
	// Get base component and obtain data reference
	struct ComponentType * Base = EntityComponentBase(Entity);
	struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

	// Get render component and obtain data reference
	struct ComponentType * Render = EntityComponentRender(Entity);
	struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);
	struct TerrainComponentRenderType * RenderType = ComponentTypeRenderDerived(Render, struct TerrainComponentRenderType);

	int texPerSide = RenderData->TextureTile;
	int texSize = RenderData->TextureSize;
	int hmSize = BaseData->HeightMapSize;
	struct Vector3f * Scale = &BaseData->Scale;
	int px, py, tx, ty, pcount = BaseData->PatchTile * BaseData->HeightMapTile;
	int patchesPerTextureObj = BaseData->PatchTile / texPerSide;
	struct Vector3f LightDirection;
	int textureMapSize = texPerSide * texSize;
	int numTextureMaps;
	int numTextures = sqr(texPerSide);
	int *textures = NULL;
	int8 *NormalMap  = NULL;
	uint8 *ShadowMap  = NULL;
	uint8 *lightMap   = NULL;
	uint8 *textureMap = NULL;
	struct Texture BaseTextureData;
	char fileName[256];
	FILE *f;
	bool readFromFile = false;

	Vector3fSet(&LightDirection, 1.0f, 0.0f, -1.0f);

	// Malloc space for texture object ids
	textures = (int*)MemoryAllocate(numTextures * sizeof(int));
	if (!textures) {
		//err_report("lsc_createColourMapTexObjs: cannot allocate %d bytes", numTextures * sizeof(int));
		return false;
	}

	// Try reading colourmap from file
	sprintf(fileName, "data/geomipmaps/test/color%d.raw", hmSize);
	if (fopen(&f, fileName, "rb")) {
		textureMap = (unsigned char*)MemoryAllocate(3*sqr(textureMapSize));
		if (!textureMap) {
			//err_report("lsc_createColourMapTexObjs: cannot allocate %d bytes", 3*sqr(textureMapSize));
			MemoryDeallocate(textures);
			return false;
		}
		fread(textureMap, 1, 3*sqr(textureMapSize), f);
		fclose(f);
		readFromFile = true;
	}
	else {
		//err_report("lsc_createColourMapTexObjs: cannot open %s", fileName);

		// Create colourmap

		// AW TODO: calculate best values automatically
		// This was tuned to a 2048 map single texture map
		NormalMap = TerrainTextureNormalMapGenerate(BaseData->HeightMap, hmSize, Scale, 0.1f, 0.75f, 32);
		// 512:
		//NormalMap = TerrainTextureNormalMapGenerate(BaseData->HeightMap, hmSize, Scale, 0.1f, 0.75f, 8);
		ShadowMap = TerrainTextureShadowsGenerate(BaseData->HeightMap, hmSize, Scale, &LightDirection);

		if (NormalMap && ShadowMap)
			lightMap  = TerrainTextureLightMapGenerate(NormalMap, ShadowMap, hmSize, Scale, 0.5f, 0.5f, &LightDirection);

		if (lightMap)
			textureMap = lsc_calcTextures(0, true, BaseData->HeightMap, NormalMap, lightMap,
				NULL, hmSize, texPerSide, texSize, texScale, &numTextureMaps);
		if (NormalMap)
			MemoryDeallocate(NormalMap);
		if (ShadowMap)
			MemoryDeallocate(ShadowMap);
		if (lightMap)
			MemoryDeallocate(lightMap);

		if (!textureMap) {
			MemoryDeallocate(textures);
			return false;
		}
	}

	if (numTextures == 1) {
		struct Texture TextureMapData;

		// Make sure the texture object tiles seamlessly
		int X, Y;
		for (Y = 0; Y < (texSize-1); Y++) {
			textureMap[3*((Y+1)*texSize - 1) + 0] =
					textureMap[3*(Y*textureMapSize) + 0];
			textureMap[3*((Y+1)*texSize - 1) + 1] =
					textureMap[3*(Y*textureMapSize) + 1];
			textureMap[3*((Y+1)*texSize - 1) + 2] =
					textureMap[3*(Y*textureMapSize) + 2];
		}
		for (X = 0; X < texSize; X++) {
			textureMap[3*(Y*texSize + X) + 0] = textureMap[3*X + 0];
			textureMap[3*(Y*texSize + X) + 1] = textureMap[3*X + 1];
			textureMap[3*(Y*texSize + X) + 2] = textureMap[3*X + 2];
		}

		// Set texture info
		TextureMapData.Pixels = textureMap;
		TextureMapData.Channels = 3;
		TextureMapData.Width = TextureMapData.Height = texSize;

		// Make an OpenGL texture object (edge clamped)
		if (TextureCreate(&TextureMapData, true, false))
		{
			textures[0] = TextureMapData.Id;
		}
	}
	else {
		// Chop up the colourmap into texSize x texSize chunks
		int X, Y, i, j;
		unsigned char *source;
		unsigned char *littleTexture = (unsigned char*)MemoryAllocate(
											3*texSize*texSize);
		if (!littleTexture) {
			//err_report("lsc_createColourMapTexObjs: cannot allocate %d bytes", 3*texSize*texSize);
			MemoryDeallocate(textureMap);
			MemoryDeallocate(textures);
			return false;
		}
		for (j = 0; j < texPerSide; j++) {
			for (i = 0; i < texPerSide; i++) {
				struct Texture LittleTextureData;

				for (Y = 0; Y < (texSize-1); Y++) {
					source = &textureMap[3*texSize*(j*textureMapSize + i)];
					for (X = 0; X < (texSize-1); X++) {
						littleTexture[3*(Y*texSize + X) + 0] =
							source[3*(Y*textureMapSize + X) + 0];
						littleTexture[3*(Y*texSize + X) + 1] =
							source[3*(Y*textureMapSize + X) + 1];
						littleTexture[3*(Y*texSize + X) + 2] =
							source[3*(Y*textureMapSize + X) + 2];
					}
					// Make sure each texture object tiles seamlessly with its neighbours
					if (i == (texPerSide-1))
						source = &textureMap[3*texSize*(j*textureMapSize - 1)];
					littleTexture[3*((Y+1)*texSize - 1) + 0] =
							source[3*(Y*textureMapSize + texSize) + 0];
					littleTexture[3*((Y+1)*texSize - 1) + 1] =
							source[3*(Y*textureMapSize + texSize) + 1];
					littleTexture[3*((Y+1)*texSize - 1) + 2] =
							source[3*(Y*textureMapSize + texSize) + 2];
				}
				// Make sure each texture object tiles seamlessly with its neighbours
				if (j == (texPerSide-1))
					source = &textureMap[3*texSize*(i - textureMapSize)];
				else
					source = &textureMap[3*texSize*(j*textureMapSize + i)];
				for (X = 0; X < texSize; X++) {
					littleTexture[3*(Y*texSize + X) + 0] =
						source[3*((Y+1)*textureMapSize + X) + 0];
					littleTexture[3*(Y*texSize + X) + 1] =
						source[3*((Y+1)*textureMapSize + X) + 1];
					littleTexture[3*(Y*texSize + X) + 2] =
						source[3*((Y+1)*textureMapSize + X) + 2];
				}

				// Make an OpenGL texture object (edge clamped)
				LittleTextureData.Pixels = littleTexture;
				LittleTextureData.Channels = 3;
				LittleTextureData.Width = LittleTextureData.Height = texSize;

				// Make an OpenGL texture object (edge clamped)
				if (TextureCreate(&LittleTextureData, true, false))
				{
					textures[j*texPerSide + i] = LittleTextureData.Id;
				}
			}
		}

		// Free up temporary storage
		MemoryDeallocate(littleTexture);
	}

	if (!readFromFile) {
		// Save the colourmap for next time before freeing
		sprintf(fileName, "data/geomipmaps/test/color%d.raw", hmSize);

		if (!fopen(&f, fileName, "wb")) {
			//err_report("lsc_createColourMapTexObjs: cannot open %s", fileName);
		}
		else {
			fwrite(textureMap, 1, 3*sqr(textureMapSize), f);
			fclose(f);
		}
	}

	MemoryDeallocate(textureMap);

	// Base texture
	if (TextureLoadFromFile("data/geomipmaps/test/terrain.tga", &BaseTextureData))
	{
		if (BaseTextureData.Width == BaseTextureData.Height)
		{
			if (BaseTextureData.Channels == 4)
			{
				uinteger i;

				for (i = 0; i < BaseTextureData.Width * BaseTextureData.Height; ++i)
				{
					BaseTextureData.Pixels[i * 3] = BaseTextureData.Pixels[i * 4];
					BaseTextureData.Pixels[i * 3 + 1] = BaseTextureData.Pixels[i * 4 + 1];
					BaseTextureData.Pixels[i * 3 + 2] = BaseTextureData.Pixels[i * 4 + 2];
				}

				BaseTextureData.Channels = 3;
			}

			// Make an OpenGL texture object (wrapped)
			if (TextureCreate(&BaseTextureData, true, false))
			{
				RenderData->BaseTexture = BaseTextureData.Id;
			}
		}

		MemoryDeallocate(BaseTextureData.Pixels);
	}

	// Assign texture ids to landscape patches
	for (py = 0; py < pcount; py++) {
		ty = (py % BaseData->PatchTile) / patchesPerTextureObj;
		for (px = 0; px < pcount; px++) {
			tx = (px % BaseData->PatchTile) / patchesPerTextureObj;
			RenderType->Patch(Entity, px, py)->ColourMapTexure = textures[ty*texPerSide + tx];
		}
	}

	MemoryDeallocate(textures);

	return true;
}

/*
  createTextureSplatTexObjs()

  Create OpenGL texture object(s) for texture splatting.

  Once the blend maps have been calculated, they are cut up into texPerSide
  x texPerSide tiles, each of size texSize x texSize, and each tile is used
  to create an OpenGL texture object. Once this has been done, the blend maps
  are deleted.
*/
bool TerrainTextureSplatCreate(struct EntityType * Entity, float texScale)
{
	// Get base component and obtain data reference
	struct ComponentType * Base = EntityComponentBase(Entity);
	struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

	// Get render component and obtain data reference
	struct ComponentType * Render = EntityComponentRender(Entity);
	struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);
	struct TerrainComponentRenderType * RenderType = ComponentTypeRenderDerived(Render, struct TerrainComponentRenderType);

	int alphaSize;
	int alphaMapSize;
	int alphaPerSide;
	int numAlphaMaps;
	int numAlphas;
	int hmSize = BaseData->HeightMapSize;
	struct Vector3f * Scale = &BaseData->Scale;
	int px, py, tx, ty, pcount = BaseData->PatchTile * BaseData->HeightMapTile;
	int patchesPerAlphaObj;
	struct Vector3f LightDirection;
	int *alphas = NULL;
	int8 *NormalMap  = NULL;
	uint8 *ShadowMap  = NULL;
	uint8 *lightMap   = NULL;
	uint8 *alphaMaps  = NULL;
	uint8 *alphaMap   = NULL;
	int textureMapSize = RenderData->TextureTile * RenderData->TextureSize;
	unsigned char *textureMap = NULL;
	int k, countLoaded;
	char fileName[256];
	FILE *f;
	bool readFromFile = false;

	Vector3fSet(&LightDirection, 1.0f, 0.0f, -1.0f);

	// Create blend maps
	alphaSize = RenderData->TextureSize;
	alphaMapSize = BaseData->HeightMapSize;
	if (alphaSize > alphaMapSize)
		alphaSize = alphaMapSize;
	alphaPerSide = alphaMapSize / alphaSize;
	numAlphas = sqr(alphaPerSide);
	patchesPerAlphaObj = BaseData->PatchTile / alphaPerSide;

	if (textureMapSize != alphaMapSize) {
		//err_report("lsc_createTextureSplatTexObjs: bad colour map size %d", textureMapSize);
		return false;
	}

	// Try reading unlit colourmap from file
	sprintf(fileName, "data/geomipmaps/test/unlit_color%d.raw", hmSize);
	if (fopen(&f, fileName, "rb")) {
		textureMap = (unsigned char*)MemoryAllocate(3*sqr(textureMapSize));
		if (!textureMap) {
			//err_report("lsc_createTextureSplatTexObjs: cannot allocate %d bytes", 3*sqr(textureMapSize));
			return false;
		}
		fread(textureMap, 1, 3*sqr(textureMapSize), f);
		fclose(f);
		readFromFile = true;
	}
	//else
	//	err_report("lsc_createTextureSplatTexObjs: cannot open %s", fileName);

	// AW TODO: calculate best values automatically
	// This was tuned to a 2048 map single texture map
	NormalMap = TerrainTextureNormalMapGenerate(BaseData->HeightMap, hmSize, Scale, 0.1f, 0.75f, 32);
	// 512:
	// NormalMap = TerrainTextureNormalMapGenerate(BaseData->HeightMap, hmSize, Scale, 0.1f, 0.75f, 8);
	ShadowMap = TerrainTextureShadowsGenerate(BaseData->HeightMap, hmSize, Scale, &LightDirection);

	if (NormalMap && ShadowMap)
		lightMap  = TerrainTextureLightMapGenerate(NormalMap, ShadowMap, hmSize, Scale, 0.5f, 0.5f, &LightDirection);

	if (lightMap && !readFromFile)
		textureMap = lsc_calcTextures(0, false, BaseData->HeightMap, NormalMap, lightMap,
				NULL, hmSize, alphaPerSide, alphaSize, texScale, &numAlphaMaps);

	if (lightMap && textureMap)
		alphaMaps = lsc_calcTextures(1, false, BaseData->HeightMap, NormalMap, lightMap, textureMap,
				hmSize, alphaPerSide, alphaSize, 1.0f, &numAlphaMaps);

	if (NormalMap)
		MemoryDeallocate(NormalMap);
	if (ShadowMap)
		MemoryDeallocate(ShadowMap);
	if (lightMap)
		MemoryDeallocate(lightMap);

	if (textureMap && !readFromFile) {
		// Save the colourmap for next time before freeing
		sprintf(fileName, "data/geomipmaps/test/unlit_color%d.raw", hmSize);
		if (!fopen(&f, fileName, "wb")) {
			//err_report("lsc_createTextureSplatTexObjs: cannot open %s", fileName);
		}
		else {
			fwrite(textureMap, 1, 3*sqr(textureMapSize), f);
			fclose(f);
		}
	}

	if (textureMap)
		MemoryDeallocate(textureMap);

	if (!alphaMaps)
		return false;

	// Malloc space for texture object ids
	// AW TODO: currently the engine can only handle three splat textures
	if (numAlphaMaps > 4)
		numAlphaMaps = 4;
	alphas = (int*)MemoryAllocate(numAlphas * numAlphaMaps * sizeof(int));
	if (!alphas) {
		//err_report("lsc_createTextureSplatTexObjs: cannot allocate %d bytes", numAlphas * numAlphaMaps * sizeof(int));
		MemoryDeallocate(alphaMaps);
		return false;
	}

	alphaMap = alphaMaps;
	for (k = 0; k < numAlphaMaps; k++) {

		if (numAlphas == 1) {
			struct Texture AlphaTextureData;
			int X, Y;

			// Make sure the texture object tiles seamlessly
			for (Y = 0; Y < (alphaSize-1); Y++) {
				alphaMap[4 * ((Y+1)*alphaSize - 1) + 0] =
							alphaMap[4 * (Y*alphaSize) + 0];
				alphaMap[4 * ((Y+1)*alphaSize - 1) + 1] =
							alphaMap[4 * (Y*alphaSize) + 1];
				alphaMap[4 * ((Y+1)*alphaSize - 1) + 2] =
							alphaMap[4 * (Y*alphaSize) + 2];
				alphaMap[4 * ((Y+1)*alphaSize - 1) + 3] =
							alphaMap[4 * (Y*alphaSize) + 3];
			}
			for (X = 0; X < alphaSize; X++) {
				alphaMap[4 * (Y*alphaSize + X) + 0] =
										alphaMap[4 * X + 0];
				alphaMap[4 * (Y*alphaSize + X) + 1] =
										alphaMap[4 * X + 1];
				alphaMap[4 * (Y*alphaSize + X) + 2] =
										alphaMap[4 * X + 2];
				alphaMap[4 * (Y*alphaSize + X) + 3] =
										alphaMap[4 * X + 3];
			}

			// Make an OpenGL texture object (edge clamped)
			AlphaTextureData.Pixels = alphaMap;
			AlphaTextureData.Channels = 4;
			AlphaTextureData.Width = AlphaTextureData.Height = alphaSize;

			if (TextureCreate(&AlphaTextureData, true, false))
			{
				alphas[k] = AlphaTextureData.Id;
			}
		}
		else {
			// Chop up into alphaSize x alphaSize chunks
			int X, Y, i, j;
			unsigned char *source;
			unsigned char *littleAlpha = (unsigned char*)MemoryAllocate(
												4*alphaSize*alphaSize);
			if (!littleAlpha) {
				//err_report(
				//"lsc_createTextureSplatTexObjs: cannot allocate %d bytes",
				//									4*alphaSize*alphaSize);
				MemoryDeallocate(alphaMaps);
				MemoryDeallocate(alphas);
				return false;
			}
			for (j = 0; j < alphaPerSide; j++) {
				for (i = 0; i < alphaPerSide; i++) {
					struct Texture LittleTextureData;
					for (Y = 0; Y < (alphaSize-1); Y++) {
						source = &alphaMap[4 * (alphaSize*(j*alphaMapSize + i))];
						for (X = 0; X < (alphaSize-1); X++) {
							littleAlpha[4 * (Y*alphaSize + X) + 0] =
								source[4 * (Y*alphaMapSize + X) + 0];
							littleAlpha[4 * (Y*alphaSize + X) + 1] =
								source[4 * (Y*alphaMapSize + X) + 1];
							littleAlpha[4 * (Y*alphaSize + X) + 2] =
								source[4 * (Y*alphaMapSize + X) + 2];
							littleAlpha[4 * (Y*alphaSize + X) + 3] =
								source[4 * (Y*alphaMapSize + X) + 3];
						}
						// Make sure each texture object tiles seamlessly with its neighbours
						if (i == (alphaPerSide-1))
							source = &alphaMap[4*(alphaSize*(j*alphaMapSize - 1))];
						littleAlpha[4 * ((Y+1)*alphaSize - 1) + 0] =
								source[4 * (Y*alphaMapSize + alphaSize) + 0];
						littleAlpha[4 * ((Y+1)*alphaSize - 1) + 1] =
								source[4 * (Y*alphaMapSize + alphaSize) + 1];
						littleAlpha[4 * ((Y+1)*alphaSize - 1) + 2] =
								source[4 * (Y*alphaMapSize + alphaSize) + 2];
						littleAlpha[4 * ((Y+1)*alphaSize - 1) + 3] =
								source[4 * (Y*alphaMapSize + alphaSize) + 3];
					}
					// Make sure each texture object tiles seamlessly with its neighbours
					if (j == (alphaPerSide-1))
						source = &alphaMap[4 * (alphaSize*(i - alphaMapSize))];
					else
						source = &alphaMap[4 * (alphaSize*(j*alphaMapSize + i))];
					for (X = 0; X < alphaSize; X++) {
						littleAlpha[4 * (Y*alphaSize + X) + 0] =
							source[4 * ((Y+1)*alphaMapSize + X) + 0];
						littleAlpha[4 * (Y*alphaSize + X) + 1] =
							source[4 * ((Y+1)*alphaMapSize + X) + 1];
						littleAlpha[4 * (Y*alphaSize + X) + 2] =
							source[4 * ((Y+1)*alphaMapSize + X) + 2];
						littleAlpha[4 * (Y*alphaSize + X) + 3] =
							source[4 * ((Y+1)*alphaMapSize + X) + 3];
					}

					// Make an OpenGL texture object (edge clamped)
					LittleTextureData.Pixels = littleAlpha;
					LittleTextureData.Channels = 4;
					LittleTextureData.Width = LittleTextureData.Height = alphaSize;

					if (TextureCreate(&LittleTextureData, true, false))
					{
						alphas[k*numAlphas + j*alphaPerSide + i] = LittleTextureData.Id;
					}
				}
			}

			// Free up temporary storage
			MemoryDeallocate(littleAlpha);
		}

		alphaMap += 4 * sqr(alphaMapSize);
	}

	MemoryDeallocate(alphaMaps);

	// Assign texture ids to landscape patches
	for (k = 0; k < numAlphaMaps; k++) {
		for (py = 0; py < pcount; py++) {
			ty = (py % BaseData->PatchTile) / patchesPerAlphaObj;
			for (px = 0; px < pcount; px++) {
				tx = (px % BaseData->PatchTile) / patchesPerAlphaObj;
				RenderType->Patch(Entity, px, py)->BlendMapTexture[k] = alphas[k*numAlphas + ty*alphaPerSide + tx];
			}
		}
	}

	MemoryDeallocate(alphas);

	// Load splat textures
	countLoaded = 0;
	for (k = 0; k < 3; k++) {
		struct Texture TextureData;
		char filename[256];

		// If texture%d.tga found and is square
		sprintf(filename, "data/geomipmaps/hq/texture%d.tga", k);

		if (TextureLoadFromFile(filename, &TextureData))
		{
			if (TextureData.Width == TextureData.Height)
			{
				if (TextureCreate(&TextureData, true, false))
				{
					RenderData->SplatTexture[countLoaded] = TextureData.Id;
					++countLoaded;
				}
			}

			MemoryDeallocate(TextureData.Pixels);
		}

		// AW TODO: currently the engine can only handle three splat textures
		if (countLoaded >= 3)
			break;
	}

	return true;
}
