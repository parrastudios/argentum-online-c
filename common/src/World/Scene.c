/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Scene.h>

#include <World/Camera.h>

#include <World/Terrain.h>

#include <World/Model.h>
#include <World/ModelLoader.h>

#include <Window/Window.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

// todo: check why entity system breaks when using more than 256 entities
//		 the problem appears making the entity number 255 as null
#define SCENE_DEFAULT_ENTITIES_SIZE			0xFE // 0x02FF // 0x11 // 0x1F

#if (SCENE_DEFAULT_QUALITY == SCENE_QUALITY_LOW)
#   define SCENE_DEFAULT_SIZE				512.0f
#elif (SCENE_DEFAULT_QUALITY == SCENE_QUALITY_MEDIUM)
#   define SCENE_DEFAULT_SIZE				1024.0f
#elif (SCENE_DEFAULT_QUALITY == SCENE_QUALITY_HIGH)
#   define SCENE_DEFAULT_SIZE				2048.0f
#endif

#define SCENE_DEFAULT_SIZE_HALF				SCENE_DEFAULT_SIZE / 2.0f

#define SCENE_DEFAULT_CAMERA_TYPE_BASE		0x00
#define SCENE_DEFAULT_CAMERA_TYPE_SPRING	0x01
#define SCENE_DEFAULT_CAMERA_TYPE			SCENE_DEFAULT_CAMERA_TYPE_SPRING

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Returns the scene min bound
////////////////////////////////////////////////////////////
struct Vector3f * SceneMinBound()
{
    static struct Vector3f MinBound = { -SCENE_DEFAULT_SIZE_HALF, -SCENE_DEFAULT_SIZE_HALF, -SCENE_DEFAULT_SIZE_HALF };

    return &MinBound;
}

////////////////////////////////////////////////////////////
/// Returns the scene max bound
////////////////////////////////////////////////////////////
struct Vector3f * SceneMaxBound()
{
    static struct Vector3f MaxBound = { SCENE_DEFAULT_SIZE_HALF, SCENE_DEFAULT_SIZE_HALF, SCENE_DEFAULT_SIZE_HALF };

    return &MaxBound;
}

////////////////////////////////////////////////////////////
/// Returns the scene scale
////////////////////////////////////////////////////////////
struct Vector3f * SceneScale()
{
    #if (SCENE_DEFAULT_QUALITY == SCENE_QUALITY_LOW)
    static struct Vector3f Scale = { 100.0f, 100.0f, 10.0f };
    #elif (SCENE_DEFAULT_QUALITY == SCENE_QUALITY_MEDIUM)
    static struct Vector3f Scale = { 50.0f, 50.0f, 5.0f };
    #elif (SCENE_DEFAULT_QUALITY == SCENE_QUALITY_HIGH)
    static struct Vector3f Scale = { 25.0f, 25.0f, 2.5f };
    #endif

    return &Scale;
}

////////////////////////////////////////////////////////////
/// Create an entity and insert it into the scene
////////////////////////////////////////////////////////////
EntityIdType SceneEntityCreate(struct SceneType * Scene, uinteger Type, bool Insert)
{
	if (Scene)
	{
		// Initialize entity
		EntityIdType EntityId = EntityCreate(Type);

		if (EntityId != ENTITY_INDEX_INVALID)
		{
			struct EntityIdPairType EntityIdPair;

			// Insert pair to identify the entity easily in octree
			EntityIdPair.Type = Type;
			EntityIdPair.Id = EntityId;

            if (Insert)
            {
                // Add entity to the octree
                OctreeAddEntity(Scene->Octree, &EntityIdPair);
            }

			return EntityId;
		}
	}

	return ENTITY_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Create a camera for the scene
////////////////////////////////////////////////////////////
void SceneCameraCreate(struct SceneType * Scene)
{
	if (Scene)
	{
		// Create camera entity
		EntityIdType EntityId = SceneEntityCreate(Scene, ENTITY_TYPE_CAMERA, false);

		// Initialize camera entity
		if (EntityId != ENTITY_INDEX_INVALID)
		{
			struct Vector3f Target = { 0.0f, 0.0f, 0.0f }; // todo: change by target position
			struct Vector3f Eye = { 0.0f, 1500.0f, 1500.0f }; // todo: change target Z by the height when target is updated
			struct Vector3f Up = { 0.0f, 0.0f, 1.0f };

			struct EntityType * Entity = EntityGet(ENTITY_TYPE_CAMERA, EntityId);

			// Obtain base component data reference
			struct ComponentType * Base = EntityComponentBase(Entity);
			struct CameraComponentBaseType * BaseType = ComponentTypeDerived(Base, struct CameraComponentBaseType);

			// Obtain transform component data reference
			struct ComponentType * Transform = EntityComponentTransform(Entity);
			struct CameraComponentTransformType * TransformType = ComponentTypeDerived(Transform, struct CameraComponentTransformType);

			// Initialize camera
			#if (SCENE_DEFAULT_CAMERA_TYPE == SCENE_DEFAULT_CAMERA_TYPE_BASE)
				BaseType->Initialize(Entity, "Scene Main Camera");
			#elif (SCENE_DEFAULT_CAMERA_TYPE == SCENE_DEFAULT_CAMERA_TYPE_SPRING)
				BaseType->InitializeSpring(Entity, "Scene Main Camera", CAMERA_BASE_SPRING_DEFAULT);
			#endif

			// Reset camera projection
			BaseType->Perspective(Entity, CAMERA_BASE_FOVX_DEFAULT, (float)(WindowWidth) / (float)(WindowHeight), CAMERA_BASE_ZNEAR_DEFAULT, CAMERA_BASE_ZFAR_DEFAULT);

			// Reset camera view
			BaseType->LookAt(Entity, &Eye, &Target, &Up);

			// First update
			TransformType->Update(Entity, 1.0f);
		}

		// Set current camera of the scene
		Scene->CameraId = EntityId;
	}
}

////////////////////////////////////////////////////////////
/// Get the camera of the scene
////////////////////////////////////////////////////////////
struct EntityType * SceneCameraGet(struct SceneType * Scene)
{
	if (Scene && Scene->CameraId != ENTITY_INDEX_INVALID)
	{
		return EntityGet(ENTITY_TYPE_CAMERA, Scene->CameraId);
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Update the camera of the scene
////////////////////////////////////////////////////////////
void SceneCameraUpdate(struct SceneType * Scene, EntityIdType TargetId, float ElapsedTime)
{
	if (Scene && TargetId != ENTITY_INDEX_INVALID)
	{
		// Get camera entity
		struct EntityType * Camera = SceneCameraGet(Scene);

		// Get camera transform type
		struct ComponentType * CameraTransform = EntityComponentTransform(Camera);
		struct CameraComponentTransformType * CameraTransformType = ComponentTypeDerived(CameraTransform, struct CameraComponentTransformType);

		// Get camera physics type
		struct ComponentType * CameraPhysics = EntityComponentPhysics(Camera);
		struct ComponentPhysicsType * CameraPhysicsType = ComponentTypePhysics(CameraPhysics);

		// Get target entity
		struct EntityType * Target = SceneModelGet(Scene, TargetId);

		// Get target transform data
		struct ComponentType * TargetTransform = EntityComponentTransform(Target);
		struct ComponentTransformData * TargetTransformData = ComponentDataTransform(TargetTransform);

		// Get target physics data
		struct ComponentType * TargetPhysics = EntityComponentPhysics(Target);
		struct ComponentPhysicsData * TargetPhysicsData = ComponentDataPhysics(TargetPhysics);

		// Set camera velocity
		CameraPhysicsType->Velocity(Camera, &TargetPhysicsData->Velocity);

		// Update phyisics velocity with elapsed time
		CameraPhysicsType->Generate(Camera, ElapsedTime);

		// Reset camera view
		CameraTransformType->Move(Camera, &TargetTransformData->Position);

		// Update camera
		CameraTransformType->Update(Camera, ElapsedTime);
	}
}

////////////////////////////////////////////////////////////
/// Create a terrain for the scene
////////////////////////////////////////////////////////////
void SceneTerrainCreate(struct SceneType * Scene)
{
	if (Scene)
	{
		// Create terrain entity
		EntityIdType EntityId = SceneEntityCreate(Scene, ENTITY_TYPE_TERRAIN, true);

		// Initialize terrain entity
		if (EntityId != ENTITY_INDEX_INVALID)
		{
			// Default base properties
			uinteger Index = 1;
			#if (SCENE_DEFAULT_QUALITY == SCENE_QUALITY_LOW)
			uinteger HeightMapSize = 512;
			uinteger HeightMapTile = 2;
			#elif (SCENE_DEFAULT_QUALITY == SCENE_QUALITY_MEDIUM)
			uinteger HeightMapSize = 1024;
			uinteger HeightMapTile = 1;
			#elif (SCENE_DEFAULT_QUALITY == SCENE_QUALITY_HIGH)
			uinteger HeightMapSize = 2048;
			uinteger HeightMapTile = 1;
			#endif
			uinteger PatchSize = 16;

			struct EntityType * Entity = EntityGet(ENTITY_TYPE_TERRAIN, EntityId);

			// Obtain base component data reference
			struct ComponentType * Base = EntityComponentBase(Entity);
			struct TerrainComponentBaseType * BaseType = ComponentTypeDerived(Base, struct TerrainComponentBaseType);

			// Obtain transform component data reference
			struct ComponentType * Transform = EntityComponentTransform(Entity);
			struct ComponentTransformType * TransformType = ComponentTypeTransform(Transform);

			// Initialize terrain transform
			TransformType->Scale(Entity, SceneScale());

			TransformType->Move(Entity, SceneMinBound());

			// Initialize terrain base
			BaseType->Initialize(Entity, Index, HeightMapSize, HeightMapTile, PatchSize, SceneScale());

			// Initialize terrain render
			#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_RENDER)
				SceneTerrainCreateRender(Scene, EntityId);
			#endif

			// Set secene terrain id
			Scene->TerrainId = EntityId;
		}
	}
}

////////////////////////////////////////////////////////////
/// Get the terrain in the scene
////////////////////////////////////////////////////////////
struct EntityType * SceneTerrainGet(struct SceneType * Scene)
{
	if (Scene && Scene->TerrainId != ENTITY_INDEX_INVALID)
	{
		return EntityGet(ENTITY_TYPE_TERRAIN, Scene->TerrainId);
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Get terrain height in the scene
////////////////////////////////////////////////////////////
float SceneTerrainGetHeight(struct SceneType * Scene, EntityIdType EntityId)
{
	if (Scene && EntityId != ENTITY_INDEX_INVALID)
	{
		// Get target entity
		struct EntityType * Target = SceneModelGet(Scene, EntityId);

		struct ComponentType * TargetTransform = EntityComponentTransform(Target);
		struct ComponentTransformData * TargetTransformData = ComponentDataTransform(TargetTransform);

		// Get terrain entity
		struct EntityType * Terrain = SceneTerrainGet(Scene);

		if (Terrain)
		{
			// Obtain base component data reference
			struct ComponentType * TerrainBase = EntityComponentBase(Terrain);
			struct TerrainComponentBaseType * TerrainBaseType = ComponentTypeDerived(TerrainBase, struct TerrainComponentBaseType);

			// Get the height
			return TerrainBaseType->GetHeightAt(Terrain, TargetTransformData->Position.x, TargetTransformData->Position.y);
		}
	}

	return 0.0f;
}

////////////////////////////////////////////////////////////
/// Create a model for the scene
////////////////////////////////////////////////////////////
void SceneModelCreate(struct SceneType * Scene, const char * Name, struct Vector3f * Position)
{
	if (Scene)
	{
		// Create terrain entity
		EntityIdType EntityId = SceneEntityCreate(Scene, ENTITY_TYPE_MODEL, true);

		// Initialize terrain entity
		if (EntityId != ENTITY_INDEX_INVALID)
		{
			if (ModelLoadFromFile("data/models/test/", Name, EntityId))
			{
				// Get entity
				struct EntityType * Entity = EntityGet(ENTITY_TYPE_MODEL, EntityId);

				// Get transform component
				struct ComponentType * Transform = EntityComponentTransform(Entity);
				struct ComponentTransformType * TransformType = ComponentTypeTransform(Transform);

				// Get collision component
				struct ComponentType * Collision = EntityComponentCollisionSphere(Entity);
				struct ComponentCollisionSphereType * CollisionType = ComponentTypeCollisionSphere(Collision);

				// struct Vector3f Position = { 0.0f, 0.0f, 0.0f };
				struct Vector3f Scale = { 50.0f, 50.0f, 50.0f };
				struct Vector3f Rotation = { 0.0f, 0.0f, -90.0f };
				struct Vector3f Orient = { 0.0f, 0.0f, 0.0f };

				// Set initial position
				TransformType->Move(Entity, Position);

				// Set scale
				TransformType->Scale(Entity, &Scale);

				// Set rotation
				TransformType->Rotate(Entity, &Rotation);

				// Set orientation
				TransformType->Orient(Entity, &Orient);

				// Update first frame
				TransformType->Update(Entity, 1.0f);

				// Initialize model collision component
				CollisionType->SetBounds(Entity, Position, 15.0f);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Get the model by id in the scene
////////////////////////////////////////////////////////////
struct EntityType * SceneModelGet(struct SceneType * Scene, EntityIdType EntityId)
{
	if (Scene && EntityId != ENTITY_INDEX_INVALID)
	{
		return EntityGet(ENTITY_TYPE_MODEL, EntityId);
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Get model height in the scene
////////////////////////////////////////////////////////////
float SceneModelGetHeight(struct SceneType * Scene, EntityIdType EntityId)
{
	if (Scene && EntityId != ENTITY_INDEX_INVALID)
	{
		// Get target entity
		struct EntityType * Target = SceneModelGet(Scene, EntityId);

		struct ComponentType * TargetTransform = EntityComponentTransform(Target);
		struct ComponentTransformData * TargetTransformData = ComponentDataTransform(TargetTransform);

		// Return height
		return TargetTransformData->Position.z;
	}

	return 0.0f;
}

////////////////////////////////////////////////////////////
/// Set model height in the scene
////////////////////////////////////////////////////////////
void SceneModelSetHeight(struct SceneType * Scene, EntityIdType EntityId, float Height)
{
	if (Scene && EntityId != ENTITY_INDEX_INVALID)
	{
		// Get target entity
		struct EntityType * Entity = SceneModelGet(Scene, EntityId);

		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);
		struct ComponentTransformType * TransformType = ComponentTypeTransform(Transform);

		struct Vector3f Position;

		// Set new position
		Vector3fSet(&Position, TransformData->Position.x, TransformData->Position.y, Height);

		// Move entity to desired height
		TransformType->Move(Entity, &Position);
	}
}

////////////////////////////////////////////////////////////
/// Update a model for the scene
////////////////////////////////////////////////////////////
void SceneModelUpdate(struct SceneType * Scene, EntityIdType EntityId, struct Vector3f * Velocity,
	struct Vector3f * Orientation, struct Vector3f * Rotation, float ElapsedTime)
{
	if (Scene && EntityId != ENTITY_INDEX_INVALID &&
		Velocity && Orientation && Rotation && ElapsedTime > 0.0f)
	{
		// Get entity
		struct EntityType * Entity = EntityGet(ENTITY_TYPE_MODEL, EntityId);

		// Get transform component data reference
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);
		struct ComponentTransformType * TransformType = ComponentTypeTransform(Transform);

		// Get physics component data reference
		struct ComponentType * Physics = EntityComponentPhysics(Entity);
		struct ComponentPhysicsData * PhysicsData = ComponentDataPhysics(Physics);
		struct ComponentPhysicsType * PhysicsType = ComponentTypePhysics(Physics);

		// Get collision component data reference
		struct ComponentType * Collision = EntityComponentCollisionSphere(Entity);
		struct ComponentCollisionSphereType * CollisionType = ComponentTypeCollisionSphere(Collision);

		// Calculate the terrain height
		float TerrainHeight = SceneTerrainGetHeight(Scene, EntityId);

		// Apply velocity to the component
		PhysicsType->Velocity(Entity, Velocity);

		// Update position
		if (!Vector3fIsNull(Velocity) && ElapsedTime > 0.0f)
		{
			// Remove from the octree
			SceneRemoveEntity(Scene, Entity);
		}

		// Update phyisics velocity with elapsed time
		PhysicsType->Generate(Entity, ElapsedTime);

		// Update orientation
		if (!Vector3fIsNull(Orientation))
		{
			TransformType->Orient(Entity, Orientation);
		}

		// Update rotation
		if (!Vector3fIsNull(Rotation))
		{
			TransformType->Rotate(Entity, Rotation);
		}

		// Calculate proper height
		SceneModelSetHeight(Scene, EntityId, TerrainHeight);

		// Update transform position with elapsed time
		TransformType->Update(Entity, ElapsedTime);

		// Update position
		if (!Vector3fIsNull(Velocity) && ElapsedTime > 0.0f)
		{
			// Update collision position
			CollisionType->SetCenter(Entity, &TransformData->Position);

			// Insert in the octree with the new position
			SceneInsertEntity(Scene, Entity);
		}
	}
}

////////////////////////////////////////////////////////////
/// Update all points for the scene
////////////////////////////////////////////////////////////
void ScenePointsUpdate(struct SceneType * Scene, struct Vector3f * Velocity,
	struct Vector3f * Orientation, struct Vector3f * Rotation, float ElapsedTime)
{
	if (Scene)
	{
		struct SetIteratorType Iterator;

		// All points follow the target
		EntityFactorySetForEach(ENTITY_TYPE_POINT, &Iterator)
		{
			struct EntityType * Entity = (struct EntityType *)SetIteratorValue(&Iterator);

			if (Entity && Entity->Index != ENTITY_INDEX_INVALID)
			{		// Get transform component data reference
				struct ComponentType * Transform = EntityComponentTransform(Entity);
				struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);
				struct ComponentTransformType * TransformType = ComponentTypeTransform(Transform);

				// Get physics component data reference
				struct ComponentType * Physics = EntityComponentPhysics(Entity);
				struct ComponentPhysicsData * PhysicsData = ComponentDataPhysics(Physics);
				struct ComponentPhysicsType * PhysicsType = ComponentTypePhysics(Physics);

				// Get collision component data reference
				struct ComponentType * Collision = EntityComponentCollisionSphere(Entity);
				struct ComponentCollisionSphereType * CollisionType = ComponentTypeCollisionSphere(Collision);

				// Apply velocity to the component
				PhysicsType->Velocity(Entity, Velocity);

				// Update position
				if (!Vector3fIsNull(Velocity) && ElapsedTime > 0.0f)
				{
					// Remove from the octree
					SceneRemoveEntity(Scene, Entity);
				}

				// Update phyisics velocity with elapsed time
				PhysicsType->Generate(Entity, ElapsedTime);

				// Update orientation
				if (!Vector3fIsNull(Orientation))
				{
					TransformType->Orient(Entity, Orientation);
				}

				// Update rotation
				if (!Vector3fIsNull(Rotation))
				{
					TransformType->Rotate(Entity, Rotation);
				}

				// Update transform position with elapsed time
				TransformType->Update(Entity, ElapsedTime);

				// Update position
				if (!Vector3fIsNull(Velocity) && ElapsedTime > 0.0f)
				{
					// Update collision position
					CollisionType->SetCenter(Entity, &TransformData->Position);

					// Insert in the octree with the new position
					SceneInsertEntity(Scene, Entity);
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Create a scene by default
////////////////////////////////////////////////////////////
bool SceneCreateDefault(struct SceneType * Scene)
{
	if (Scene)
	{	
		uinteger i;
		
		// Create camera entity
		SceneCameraCreate(Scene);

		// Create the terrain
		SceneTerrainCreate(Scene);

		for (i = 0; i < 1; ++i)
		{
			struct Vector3f Position;

			// Create a player
			Vector3fSet(&Position, -200.0f * (float)(i), -200.0f, 0.0f);

			SceneModelCreate(Scene, "test", &Position);
		}

		/*
		// Generate entities to the scene
		for (i = 0; i < SCENE_DEFAULT_ENTITIES_SIZE; ++i)
		{
			EntityIdType EntityId = SceneEntityCreate(Scene, ENTITY_TYPE_POINT, true);

            if (EntityId != ENTITY_INDEX_INVALID)
			{
			    struct EntityType * Entity = EntityGet(ENTITY_TYPE_POINT, EntityId);

                // Obtain base component data reference
                struct ComponentType * Transform = EntityComponentTransform(Entity);
                struct ComponentTransformType * TransformType = ComponentTypeTransform(Transform);

				// Create collision component data reference
				struct ComponentType * Collision = EntityComponentCollisionSphere(Entity);
				struct ComponentCollisionSphereType * CollisionType = ComponentTypeCollisionSphere(Collision);

                // Randomize position
                #define FloatRandom(Range, Half)	((((float)rand() / (float)(RAND_MAX)) * Range) - Half)

                struct Vector3f Position =
				{
                    FloatRandom(SCENE_DEFAULT_SIZE, SCENE_DEFAULT_SIZE_HALF),
                    FloatRandom(SCENE_DEFAULT_SIZE, SCENE_DEFAULT_SIZE_HALF),
                    FloatRandom(SCENE_DEFAULT_SIZE, SCENE_DEFAULT_SIZE_HALF)
                };

                #undef FloatRandom

				// Scale position
				//Vector3fMultiplyVectorEx(&Position, &Position, SceneScale());

				// Remove from the octree
				SceneRemoveEntity(Scene, Entity);

                // Set position
				TransformType->Move(Entity, &Position);

				// Set collision component
				CollisionType->SetCenter(Entity, &Position);

				// Remove from the octree
				SceneInsertEntity(Scene, Entity);

                // Scale
                TransformType->Scale(Entity, SceneScale());
			}
		}
		*/
		
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Load a scene from given index
////////////////////////////////////////////////////////////
bool SceneLoad(struct SceneType * Scene, uinteger Index)
{
	if (Scene)
	{
		struct BoundingBox OctreeBounds;
		struct Vector3f OctreeMinBound, OctreeMaxBound;

		// Copy index
		Scene->Index = Index;

		// Create octree
		Scene->Octree = OctreeCreate();

		// Initialize octree bounds
		Vector3fMultiplyVectorEx(&OctreeMinBound, SceneMinBound(), SceneScale());

		Vector3fMultiplyVectorEx(&OctreeMaxBound, SceneMaxBound(), SceneScale());

		BoundingBoxSet(&OctreeBounds, &OctreeMinBound, &OctreeMaxBound);

        // Initialize octree
		if (OctreeInitialize(Scene->Octree, 0, NULL, &OctreeBounds, 0, OCTREE_NODE_TYPE_ROOT))
		{
			// Initialize entity manager
			Scene->EntityManager = EntityManagerCreate();

			// Create a default scene
			return SceneCreateDefault(Scene);
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Add an entity to the scene (octree)
////////////////////////////////////////////////////////////
void SceneInsertEntity(struct SceneType * Scene, struct EntityType * Entity)
{
	if (Scene && Entity)
	{
		struct EntityIdPairType EntityIdPair;

		// Insert pair to identify the entity easily in octree
		EntityIdPair.Type = Entity->Factory->Type;
		EntityIdPair.Id = Entity->Index;

		OctreeAddEntity(Scene->Octree, &EntityIdPair);
	}
}

////////////////////////////////////////////////////////////
/// Remove an entity from scene (octree)
////////////////////////////////////////////////////////////
void SceneRemoveEntity(struct SceneType * Scene, struct EntityType * Entity)
{
	if (Scene && Entity)
	{
		struct EntityIdPairType EntityIdPair;

		// Insert pair to identify the entity easily in octree
		EntityIdPair.Type = Entity->Factory->Type;
		EntityIdPair.Id = Entity->Index;

		OctreeRemoveEntity(Scene->Octree, &EntityIdPair);
	}
}

////////////////////////////////////////////////////////////
/// Destroy a scene
////////////////////////////////////////////////////////////
void SceneDestroy(struct SceneType * Scene)
{
	if (Scene)
	{
		// Destroy entity manager
		EntityManagerDestroy(Scene->EntityManager);

		// Destroy the octree
		OctreeDestroy(Scene->Octree);
	}
}
