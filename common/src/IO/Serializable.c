/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <IO/Serializable.h>
#include <IO/StorageManager.h>

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define SERIALIZABLE_VALUE_VALID(Value) \
	(Value != NULL && Value->Type != SERIALIZABLE_VALUE_DATA_TYPE_INVALID)

#define SERIALIZABLE_VALUE_EXISTS(Value) \
	(Value != NULL && Value->Type != SERIALIZABLE_VALUE_DATA_TYPE_INVALID && Value->Count > 0)

#define SERIALIZABLE_VALUE_DEFINITION(Value) \
	(Value != NULL && Value->Type != SERIALIZABLE_VALUE_DATA_TYPE_INVALID && Value->Count == 0)

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define SERIALIZABLE_REFERENCE_TYPE_VALUE		0x00
#define SERIALIZABLE_REFERENCE_TYPE_OBJECT		0x01
#define SERIALIZABLE_REFERENCE_TYPE_FUNCTION	0x02

typedef uinteger SerializableReferenceTypeInfo;
typedef uinteger SerializableReferenceCountType;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct SerializableValueType
{
	SerializableIndexDataType			Index;		///< Reference to itself
	SerializableValueDataType			Type;		///< Type of value
	SerializableReferenceCountType		RefCount;	///< References to value
	SerializableValueDataCount			Count;		///< Amount of values contained (0 -> definition, 1 -> value, (>=2) -> array)

	union SerializableData
	{
		uint8	UInt8;
		int8	Int8;
		uint16	UInt16;
		int16	Int16;
		uint32	UInt32;
		int32	Int32;
		uint64	UInt64;
		int64	Int64;
		float32	Float32;
		float64	Float64;
		uint8	String[1];
	} Data;											///< Data implementation of the value

};

struct SerializableReferenceType
{
	SerializableIndexDataType			Index;		///< Index referencing to the data
	SerializableReferenceTypeInfo		Type;		///< Internal type of reference

	union
	{
		SerializableValueType			Value;
		SerializableObjectType			Object;
		SerializableFunctionType		Function;
	} Pointer;										///< Pointer to data implementation

};

union SerializableParameterType
{
	struct SerializableValueType		Value;		///< Parameter as a value
	struct SerializableReferenceType	Reference;	///< Parameter as a reference
};

struct SerializableParameterListType
{
	SerializableParameterListSize		Size;		///< Number of parameters
	SerializableParameterType			List[1];	///< Contiguous allocated list of parameters
};

struct SerializableFunctionType
{
	SerializableFunctionNameType		Name;		///< Name of the function
	SerializableValueType				RetValue;	///< Return value (definition)
	SerializableParameterListType		Parameters;	///< List of parameters (definition)
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a serial value
////////////////////////////////////////////////////////////
SerializableValueType SerializableValueCreate(SerializableValueDataType Type, SerializableValueDataCount Count, void * Data)
{
	SerializableIndexDataType Index;

	// Calculate the sizes
	uinteger DataSize = Count * SerializableValueDataSizeInfo(Type);
	uinteger ValueSize = OffsetOf(struct SerializableValueType, Data) + DataSize;

	// Alloc memory for value
	//SerializableValueType Value = (SerializableValueType)MemoryAllocate(ValueSize);
	SerializableValueType Value = (SerializableValueType)StorageValueAllocate(&Index, ValueSize);

	if (Value)
	{
		// Set values
		Value->Index = Index;
		Value->Type = Type;
		Value->Count = Count;

		// Copy data
		if (Value->Count > 0 && Data != NULL)
		{
			MemoryCopy(&Value->Data, Data, DataSize);
		}

		return Value;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Get the type of a value
////////////////////////////////////////////////////////////
SerializableValueDataType SerializableValueTypeOf(SerializableValueType Value)
{
	if (SERIALIZABLE_VALUE_VALID(Value))
	{
		return Value->Type;
	}

	return SERIALIZABLE_VALUE_DATA_TYPE_INVALID;
}

////////////////////////////////////////////////////////////
/// Get the amount of values that contains itself
////////////////////////////////////////////////////////////
SerializableValueDataCount SerializableValueCount(SerializableValueType Value)
{
	if (SERIALIZABLE_VALUE_VALID(Value))
	{
		return Value->Count;
	}

	return 0;
}

////////////////////////////////////////////////////////////
/// Get the data wraped by a value
////////////////////////////////////////////////////////////
void * SerializableValueGetData(SerializableValueType Value)
{
	if (SERIALIZABLE_VALUE_EXISTS(Value))
	{
		return (void *)&Value->Data;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set the internal data of a value
////////////////////////////////////////////////////////////
void SerializableValueSet(SerializableValueType Value, void * Data)
{
	if (SERIALIZABLE_VALUE_EXISTS(Value) && Data != NULL)
	{
		// Calculate size
		uinteger DataSize = Value->Count * SerializableValueDataSizeInfo(Value->Type);

		// Copy data
		MemoryCopy(&Value->Data, Data, DataSize);
	}
}

////////////////////////////////////////////////////////////
/// Destroy a value
////////////////////////////////////////////////////////////
void SerializableValueDestroy(SerializableValueType Value)
{
	if (Value)
	{
		// MemoryDeallocate(Value);
		StorageValueDeallocate(Value->Index);
	}
}

////////////////////////////////////////////////////////////
/// Create a serial reference
////////////////////////////////////////////////////////////
SerializableReferenceType SerializableReferenceCreate(SerializableValueType Value)
{


	return NULL;
}

////////////////////////////////////////////////////////////
/// Set a reference to a value
////////////////////////////////////////////////////////////
void SerializableReferenceSet(SerializableReferenceType Reference, SerializableValueType Value)
{

}

////////////////////////////////////////////////////////////
/// Get the data that reference is pointing to
////////////////////////////////////////////////////////////
void * SerializableReferenceGetValue(SerializableReferenceType Reference)
{


	return NULL;
}

////////////////////////////////////////////////////////////
/// Set the internal data of a value
////////////////////////////////////////////////////////////
void SerializableReferenceSetValue(SerializableReferenceType Reference, void * Data)
{

}

////////////////////////////////////////////////////////////
/// Destroy a reference
////////////////////////////////////////////////////////////
void SerializableReferenceDestroy(SerializableReferenceType Reference)
{

}
