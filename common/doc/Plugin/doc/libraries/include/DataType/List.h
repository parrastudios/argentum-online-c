/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATATYPE_LIST_H
#define DATATYPE_LIST_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Types.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Data structure
////////////////////////////////////////////////////////////

struct ListType;
typedef struct ListType * List;
typedef void * ListIterator;

////////////////////////////////////////////////////////////
// Function prototypes
////////////////////////////////////////////////////////////

List ListNew(uinteger TypeSize);
void ListDestroy(List l);
void ListClear(List l);
//macro: List ListNewT(typename type);

uinteger ListSize(List l);
bool ListEmpty(List l);

void * ListFront(List l);
void * ListBack(List l);
void * ListAt(List l, uinteger Position);
void ListSet(List l, uinteger Position, void * Element);

void ListPushFront(List l, void * Element);
void ListPopFront(List l);
//macro: void ListPushFrontV(List l, Type variable);
//macro: void ListPushFrontC(List l, const Type constant, typename Type);

void ListPushBack(List l, void * Element);
void ListPopBack(List l);
//macro: void ListPushBackV(List l, Type variable);
//macro: void ListPushBackC(List l, const Type constant, typename Type);

void ListInsert(List l, uinteger Position, void * Element);
void ListErase(List l, uinteger Position);
void ListEraseIt(List ListData, ListIterator Iterator);
//macro: void ListInsertV(List l, uinteger Position, Type variable);
//macro: void ListInsertC(List l, uinteger Position, const Type constant, typename Type);

ListIterator ListBegin(List l);
ListIterator ListEnd(List l);

ListIterator ListItNext(ListIterator li);
ListIterator ListItPrev(ListIterator li);
bool ListItHasNext(ListIterator li);
bool ListItHasPrev(ListIterator li);
bool ListItDone(ListIterator li);
//macro: Type& ListItData(ListIterator li, typename Type);

////////////////////////////////////////////////////////////
// Public macros
////////////////////////////////////////////////////////////

#define ListNewT(Type) \
    ListNew(sizeof(Type))

#define ListPushFrontV(l, Variable) \
    ListPushFront(l, &Variable)

#define ListPushFrontC(l, Constant, Type) \
    do{ Type cnst = Constant; \
        ListPushFront(l, &cnst); }while(0)

#define ListPushBackV(l, Variable) \
    ListPushBack(l, &Variable)

#define ListPushBackC(l, Constant, Type) \
    do{ Type cnst = Constant; \
        ListPushBack(l, &cnst); }while(0)

#define ListInsertV(l, Position, Variable) \
    ListInsert(l, Position, &Variable)

#define ListInsertC(l, Position, Constant, Type) \
    do{ Type cnst = Constant; \
        ListInsert(l, Position, &cnst); }while(0)

#define ListItData(li, Type) \
    (*((Type*) li))

#define ListItDataPtr(ListData, Type) \
	((Type *)ListData)

#define ListForEach(l, Iterator) \
	if (!ListEmpty(l)) \
		for (Iterator = ListBegin(l); !ListItDone(Iterator); Iterator = ListItNext(Iterator))

#endif // DATATYPE_LIST_H
