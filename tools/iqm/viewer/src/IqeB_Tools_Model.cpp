/*
***********************************************************************************
 IqeB_Tools_Model.cpp

 Tools for working with models.

13.12.2014 RR: * First editon of this file.

*/

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/gl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

/************************************************************************************
 * IqeB_ModelCreateEmpty()
 *
 * Create an empty model.
 *
 * Return:   NULL  memory allocation failed
 *           else  pointer to allocated model
 */

struct IQE_model *IqeB_ModelCreateEmpty()
{

	struct IQE_model *model = NULL;
	struct skel *skel = NULL;
	struct mesh *mesh = NULL;

  // allocated memory

	model = (struct IQE_model *)malloc(sizeof *model);
	skel  = (struct skel *)malloc(sizeof *skel);
	mesh  = (struct mesh *)malloc(sizeof *mesh);

	// test all memory is allocated

	if( model == NULL || skel == NULL || mesh == NULL) {  // not all allocated

    // free what we got until now
    if( model != NULL) free( model);
    if(  skel != NULL) free( skel);
    if(  mesh != NULL) free( mesh);

    return( NULL);                    // return nothing allocated
	}

  // inital clear all data

	memset( model, 0, (sizeof *model));
	memset(  skel, 0, (sizeof *skel));
	memset(  mesh, 0, (sizeof *mesh));

  // set pointer to model parts
	model->skel = skel;
	model->mesh = mesh;

	return( model);     // return what we have allocated
}

/************************************************************************************
 * IqeB_ModelFreeData()
 *
 * Free's all data allocated by a model.
 *
 * WhatToFree:
 *   #define IQE_MODEL_FREE_ALL            0    // Free all
 *   #define IQE_MODEL_FREE_SKELETON  0x0001    // Free skeleton (and animation)
 *   #define IQE_MODEL_FREE_ANIMATION 0x0002    // Free animation data
 *   #define IQE_MODEL_FREE_MESH      0x0004    // Free mesh data
 */

void IqeB_ModelFreeData( struct IQE_model **ppModel, int WhatToFree)
{
  int i, j;
  struct IQE_model *model;
  struct anim *anim;

  model = *ppModel;            // Get pointer to model pointer

  if( model == NULL) {         /* security test */

    return;
  }

  if( WhatToFree == IQE_MODEL_FREE_ALL) {   // Free all

    *ppModel = NULL;            // reset pointer to model pointer

    //

    if( pDrawModel == model) {   // this model was used for display

      pDrawModel = NULL;         // data is no valid so invalidate pointer
    }
  }

  // release mesh texture and strings

  if( WhatToFree == IQE_MODEL_FREE_ALL ||    // Free all
     (WhatToFree & IQE_MODEL_FREE_MESH)) {   // or free mesh

	  for( i = 0; i < model->mesh->part_count; i++) {

      if( model->mesh->part[i].material > 0) {       // openGL has loaded

        unsigned int TempMaterial;

        TempMaterial = model->mesh->part[i].material;

        glDeleteTextures( 1, &TempMaterial);
      }

      model->mesh->part[i].material = -1;            // invalidate loaded texture

      // ...

      free( model->mesh->part[ i].pMeshName);
      free( model->mesh->part[ i].pMaterialStr);
      free( model->mesh->part[ i].pMaterialTags);
      free( model->mesh->part[ i].pMaterialBasedir);
      if( model->mesh->part[ i].pMaterialLoadedFile) free( model->mesh->part[ i].pMaterialLoadedFile);
	  }

    // free mesh

	  if( model->mesh->position)    free( model->mesh->position);
	  if( model->mesh->normal)      free( model->mesh->normal);
	  if( model->mesh->texcoord)    free( model->mesh->texcoord);
	  if( model->mesh->color)       free( model->mesh->color);
	  if( model->mesh->blendindex)  free( model->mesh->blendindex);
	  if( model->mesh->blendweight) free( model->mesh->blendweight);
	  if( model->mesh->element)     free( model->mesh->element);
	  if( model->mesh->part)        free( model->mesh->part);
	  if( model->mesh->aposition)   free( model->mesh->aposition);
	  if( model->mesh->anormal)     free( model->mesh->anormal);
	  if( model->mesh->blendColors) free( model->mesh->blendColors);

	  if( model->mesh)   free( model->mesh);
    model->mesh = NULL;
  }

	// free animations

  if( WhatToFree == IQE_MODEL_FREE_ALL ||        // Free all
     (WhatToFree & IQE_MODEL_FREE_ANIMATION)) {  // or free animation

    for( i = 0; i < model->anim_count; i++) {

	    anim = model->anim_data[ i];

      free( anim->name);

      if( anim->data) {

        for( j = 0; j < anim->len; j++) {

          free( anim->data[ j]);
        }

        free( anim->data);
      }

      free( anim);
	  }

    if( model->anim_data != NULL) {

      free( model->anim_data);

      model->anim_data = NULL;
    }

    model->anim_poses = 0;                // number of poses in an animation frame
    model->anim_count = 0;                // number of animations used
    model->anim_cap = 0;                  // number of animations allocated
  }

	// free skeleton

  if( WhatToFree == IQE_MODEL_FREE_ALL ||        // Free all
     (WhatToFree & IQE_MODEL_FREE_SKELETON)) {   // or free skeleton

	  for( i = 0; i < model->skel->joint_count; i++) {

      free( model->skel->j[ i].name);
	  }

	  if( model->skel) {
      free( model->skel);
	  }
  }

	// ...

  if( WhatToFree == IQE_MODEL_FREE_ALL) {   // Free all

    free( model);
  }
}

/************************************************************************************
 * IqeB_ModelCopy()
 *
 * get a copy of a model
 *
 * CopyFlags:
 *   #define IQE_MODEL_COPY_NEW_ALL    0   // copy everything, distination model is allocated
 *   #define IQE_MODEL_COPY_SKELETON   1   // copy skeleton, distination must be allocated and have no skeleton
 *   #define IQE_MODEL_COPY_MESH       2   // copy skeleton, distination must be allocated and have no mesh, vertexes, trigangle
 *   #define IQE_MODEL_COPY_ANIMATION  4   // copy animation, distination must be allocated, append animation
 *   #define IQE_MODEL_COPY_ALL_PARTS ( IQE_MODEL_COPY_SKELETON | IQE_MODEL_COPY_MESH | IQE_MODEL_COPY_ANIMATION) // copy all parts, distination must be allocated
 */

void IqeB_ModelCopy( struct IQE_model **ppModelSrc, struct IQE_model **ppModelDst, int CopyFlags)
{
  int i, j;
  struct IQE_model *pModelSrc, *pModelDst;

  pModelSrc = *ppModelSrc;     // Get pointer to model pointer
  if( pModelSrc == NULL) {     // security test, we need a source

    return;
  }

  if( CopyFlags == IQE_MODEL_COPY_NEW_ALL) {  // allocatd complete destination model

    IqeB_ModelFreeData( ppModelDst, IQE_MODEL_FREE_ALL);  // enshure there is no destination model

	  pModelDst = (struct IQE_model *)malloc(sizeof *pModelDst);  // allocate

  	memset( pModelDst,       0, sizeof( struct IQE_model));

	  pModelDst->skel = (struct skel *)malloc(sizeof *pModelDst->skel);
	  memset( pModelDst->skel, 0, sizeof( struct skel));

	  pModelDst->mesh = (struct mesh *)malloc(sizeof *pModelDst->mesh);
	  memset( pModelDst->mesh, 0, sizeof( struct mesh));

    CopyFlags = IQE_MODEL_COPY_ALL_PARTS;      // continue copy all parts
  } else {

    pModelDst = *ppModelDst;              // Get pointer to model pointer
  }

  if( (CopyFlags & IQE_MODEL_COPY_MESH) != 0) {  // copy mesh

    int VertexCnt;

    VertexCnt = pModelSrc->mesh->vertex_count;
	  pModelDst->mesh->vertex_count = VertexCnt;
	  pModelDst->mesh->position     = (float *)dupArray( pModelSrc->mesh->position,    VertexCnt * 3, sizeof(float));
	  pModelDst->mesh->normal       = (float *)dupArray( pModelSrc->mesh->normal,      VertexCnt * 3, sizeof(float));
	  pModelDst->mesh->texcoord     = (float *)dupArray( pModelSrc->mesh->texcoord,    VertexCnt * 2, sizeof(float));
	  pModelDst->mesh->color        = (float *)dupArray( pModelSrc->mesh->color,       VertexCnt * 4, sizeof(float));
	  pModelDst->mesh->blendindex   =   (int *)dupArray( pModelSrc->mesh->blendindex,  VertexCnt * 4, sizeof(int));
	  pModelDst->mesh->blendweight  = (float *)dupArray( pModelSrc->mesh->blendweight, VertexCnt * 4, sizeof(float));
	  pModelDst->mesh->aposition    = NULL;
	  pModelDst->mesh->anormal      = NULL;
	  pModelDst->mesh->blendColors  = NULL;

	  pModelDst->mesh->element_count = pModelSrc->mesh->element_count;
	  pModelDst->mesh->element = (int *)dupArray( pModelSrc->mesh->element, pModelSrc->mesh->element_count, sizeof(int));

	  pModelDst->mesh->part_count = pModelSrc->mesh->part_count;
	  pModelDst->mesh->part = (part *)dupArray( pModelSrc->mesh->part, pModelSrc->mesh->part_count, sizeof(struct part));

    // handle partial mesh things

	  for( i = 0; i < pModelDst->mesh->part_count; i++) {

      pModelDst->mesh->part[i].material = -1;            // invalidate loaded texture
      pModelDst->mesh->part[ i].pMaterialLoadedFile = NULL;

      // ...

      pModelDst->mesh->part[ i].pMeshName        = strdup( pModelSrc->mesh->part[ i].pMeshName);
      pModelDst->mesh->part[ i].pMaterialStr     = strdup( pModelSrc->mesh->part[ i].pMaterialStr);
      pModelDst->mesh->part[ i].pMaterialTags    = strdup( pModelSrc->mesh->part[ i].pMaterialTags);
      pModelDst->mesh->part[ i].pMaterialBasedir = strdup( pModelSrc->mesh->part[ i].pMaterialBasedir);
	  }
  }

	// copy skeleton things

  if( (CopyFlags & IQE_MODEL_COPY_SKELETON) != 0) {  // copy skeleton

	  pModelDst->skel = (skel *)dupArray( pModelSrc->skel, 1, sizeof(struct skel));

	  for( i = 0; i < pModelDst->skel->joint_count; i++) {

      pModelDst->skel->j[ i].name = strdup( pModelSrc->skel->j[ i].name);
	  }

	  memcpy( pModelDst->mesh->abs_bind_matrix, pModelSrc->mesh->abs_bind_matrix, sizeof( pModelDst->mesh->abs_bind_matrix));
	  memcpy( pModelDst->mesh->inv_bind_matrix, pModelSrc->mesh->inv_bind_matrix, sizeof( pModelDst->mesh->inv_bind_matrix));
  }

  // copy animations

  if( (CopyFlags & IQE_MODEL_COPY_ANIMATION) != 0 &&  // copy/append animation
  	   pModelSrc->anim_count > 0 && pModelSrc->anim_poses > 0) {  // have animation and poses

	  struct anim *animSrc, *animDst;

    for( i = 0; i < pModelSrc->anim_count; i++) {

	    animSrc = pModelSrc->anim_data[ i];

      animDst = pushanim( pModelDst, animSrc->name);
      animDst->len = animSrc->len;
      animDst->framerate = animSrc->framerate;

      // allocate memory for the animations

      if( animDst->len > 0) {

		    animDst->data = (struct pose **)malloc( animDst->len * sizeof(*animDst->data));
      }

      for( j = 0; j < animDst->len; j++) {

 	      animDst->data[ j] = (struct pose *)dupArray( animSrc->data[ j], 1, sizeof(struct pose) * pModelSrc->anim_poses);
      }
    }

    if( pModelDst->anim_poses == 0) {

	    pModelDst->anim_poses = pModelSrc->anim_poses;
    }
  }

	// finish up

  *ppModelDst = pModelDst;    // set pointer to model

}

/************************************************************************************
 * IqeB_ModelMergeTo()
 *
 * Merge Src model to dst model.
 *
 * countDst   Amout of data we have.
 *
 * countAdd   Amout of data to add.
 *            An call, this is always > 0
 */

static void *addArray( void *dataDst, int countDst, void *dataAdd, int countAdd, int size)
{
  void *p;

	if( countAdd <= 0) {                        // nothing to add

		return dataDst;                           // can return what we have as input for destination
  }

  // have no destination, but some source data, --> copy it

  if( dataDst == NULL && countDst == 0 && dataAdd != NULL && countAdd > 0) {

	  p = malloc( countAdd * size);
    memcpy( p, dataAdd, countAdd * size);
	  return p;
  }

  // ...

  if( dataDst == NULL || dataAdd == NULL) {   // both must have pointers

    if( dataDst != NULL) {                    // data was allocated for this

      free( dataDst);                         // free it
    }

    return( NULL);                            // return no data
  }

  // If we come to here, dataDst and dataAdd both points to data
  // and countAdd indicates that we have to add data.
  // NOTE that dataDst points to allocated data.

	p = realloc( dataDst, (countDst + countAdd) * size); // reallocte for the data we add

	if( p == NULL) {      // security test, memory allocation failed

    return( NULL);
	}

	// copy the new data after the old data

	memcpy( (char *)p + (countDst * size), dataAdd, countAdd * size);

	return p;
}

void IqeB_ModelMergeTo( struct IQE_model *pModelDst, struct IQE_model *pModelSrc)
{
  int i, j;

  //
  // Merge meshes
  //

  int VertexCntDst, VertexCntSrc;
  int ElementCntDst, ElementCntSrc;
  int PartCntDst, PartCntSrc;

  VertexCntDst  = pModelDst->mesh->vertex_count;
  VertexCntSrc  = pModelSrc->mesh->vertex_count;

  ElementCntDst = pModelDst->mesh->element_count;
  ElementCntSrc = pModelSrc->mesh->element_count;

  PartCntDst    = pModelDst->mesh->part_count;
  PartCntSrc    = pModelSrc->mesh->part_count;

  if( VertexCntSrc > 0 && ElementCntSrc > 0 && PartCntSrc > 0) {  // have to add meshes

    // vertex

    pModelDst->mesh->vertex_count = VertexCntDst + VertexCntSrc;

    pModelDst->mesh->position    = (float *)addArray( pModelDst->mesh->position,    VertexCntDst * 3, pModelSrc->mesh->position,    VertexCntSrc * 3, sizeof(float));
    pModelDst->mesh->normal      = (float *)addArray( pModelDst->mesh->normal,      VertexCntDst * 3, pModelSrc->mesh->normal,      VertexCntSrc * 3, sizeof(float));
    pModelDst->mesh->texcoord    = (float *)addArray( pModelDst->mesh->texcoord,    VertexCntDst * 2, pModelSrc->mesh->texcoord,    VertexCntSrc * 2, sizeof(float));
    pModelDst->mesh->color       = (float *)addArray( pModelDst->mesh->color,       VertexCntDst * 4, pModelSrc->mesh->color,       VertexCntSrc * 4, sizeof(float));
    pModelDst->mesh->blendindex  =   (int *)addArray( pModelDst->mesh->blendindex,  VertexCntDst * 4, pModelSrc->mesh->blendindex,  VertexCntSrc * 4, sizeof(int));
    pModelDst->mesh->blendweight = (float *)addArray( pModelDst->mesh->blendweight, VertexCntDst * 4, pModelSrc->mesh->blendweight, VertexCntSrc * 4, sizeof(float));
    pModelDst->mesh->aposition   = NULL;
    pModelDst->mesh->anormal     = NULL;
	  pModelDst->mesh->blendColors  = NULL;

    // triangle elements

    pModelDst->mesh->element_count = ElementCntDst + ElementCntSrc;

    pModelDst->mesh->element = (int *)addArray( pModelDst->mesh->element, ElementCntDst, pModelSrc->mesh->element, ElementCntSrc, sizeof(int));

    if( pModelDst->mesh->element == NULL) {    // have no elements

      // have no elements add all, zero count
      ElementCntDst = 0;
      ElementCntSrc = 0;

      pModelDst->mesh->element_count = ElementCntDst + ElementCntSrc;

      // also reset model parts

      pModelDst->mesh->part_count = 0;

      if( pModelDst->mesh->part != NULL) {    // got some allocated data

        free( pModelDst->mesh->part);
      }

    } else {

      // have to add offset to triangle vertex references

      for( i = 0; i < ElementCntSrc; i++) {

        pModelDst->mesh->element[ ElementCntDst + i] += VertexCntDst;
      }

      // handle partial mesh things

	    pModelDst->mesh->part_count = PartCntDst + PartCntSrc;
	    pModelDst->mesh->part = (part *)addArray( pModelDst->mesh->part, PartCntDst, pModelSrc->mesh->part, PartCntSrc, sizeof(struct part));

	    for( i = PartCntDst; i < pModelDst->mesh->part_count; i++) {

        pModelDst->mesh->part[i].first += ElementCntDst;   // add triangle offset

        // for added model parts

        pModelDst->mesh->part[i].material = -1;    // invalidate loaded texture
        pModelDst->mesh->part[ i].pMaterialLoadedFile = NULL;

        // The string pointers was copied in addArray().
        // Must make a new allocation

        pModelDst->mesh->part[ i].pMeshName        = strdup( pModelDst->mesh->part[ i].pMeshName);
        pModelDst->mesh->part[ i].pMaterialStr     = strdup( pModelDst->mesh->part[ i].pMaterialStr);
        pModelDst->mesh->part[ i].pMaterialTags    = strdup( pModelDst->mesh->part[ i].pMaterialTags);
        pModelDst->mesh->part[ i].pMaterialBasedir = strdup( pModelDst->mesh->part[ i].pMaterialBasedir);
	    }
	  }
  }

  //
	// Merge skeletons
	// If Dst already has a skeleton, keep this skeleton, else
	// if Src has a skeleton, copy this to Dst.
	// ==> On successive merges, the first skeleton is latched.
	//

	if( (pModelDst->skel == NULL || pModelDst->skel->joint_count == 0) && // Dst hase no skeleton
       pModelSrc->skel != NULL && pModelSrc->skel->joint_count > 0) {   // Src has a skeleton

    if( pModelDst->skel == 0) {  // no data allocated until now

      // allocated memory and copy from source
	    pModelDst->skel = (skel *)dupArray( pModelSrc->skel, 1, sizeof(struct skel));
    } else {

      // copy from source

	    memcpy( pModelDst->skel, pModelSrc->skel, sizeof(struct skel));
    }

	  for( i = 0; i < pModelDst->skel->joint_count; i++) {

      // The string pointers was copied in dupArray().
      // Must make a new allocation

      pModelDst->skel->j[ i].name = strdup( pModelSrc->skel->j[ i].name);
	  }

	  memcpy( pModelDst->mesh->abs_bind_matrix, pModelSrc->mesh->abs_bind_matrix, sizeof( pModelDst->mesh->abs_bind_matrix));
	  memcpy( pModelDst->mesh->inv_bind_matrix, pModelSrc->mesh->inv_bind_matrix, sizeof( pModelDst->mesh->inv_bind_matrix));
  }

  //
  // Merge animations
  // Add animatins

 	if( pModelSrc->anim_count > 0 && pModelSrc->anim_poses > 0) {  // have animation and poses to merge

	  struct anim *animSrc, *animDst;

    for( i = 0; i < pModelSrc->anim_count; i++) {

	    animSrc = pModelSrc->anim_data[ i];

      animDst = pushanim( pModelDst, animSrc->name);
      animDst->len = animSrc->len;
      animDst->framerate = animSrc->framerate;

      // allocate memory for the animations

      if( animDst->len > 0) {

		    animDst->data = (struct pose **)malloc( animDst->len * sizeof(*animDst->data));
      }

      for( j = 0; j < animDst->len; j++) {

 	      animDst->data[ j] = (struct pose *)dupArray( animSrc->data[ j], 1, sizeof(struct pose) * pModelSrc->anim_poses);
      }
    }

    if( pModelDst->anim_poses == 0) {

	    pModelDst->anim_poses = pModelSrc->anim_poses;
    }
  }

	// done

}

/************************************************************************************
 * IqeB_ModelSkeletonMatrixCalc
 *
 * Update some matrix data for skeleton things.
 *
 */

void IqeB_ModelSkeletonMatrixCalc( struct IQE_model *pModel)
{

  mat4 loc_bind_matrix[MAXBONE];

 	if( pModel != NULL && pModel->mesh != NULL &&
      pModel->skel != NULL && pModel->skel->joint_count > 0) {    // have all we need

    // calc matrix things
		calc_matrix_from_pose( loc_bind_matrix, pModel->skel->pose, pModel->skel->joint_count);
		calc_abs_matrix( pModel->mesh->abs_bind_matrix, loc_bind_matrix, pModel->skel);
		calc_inv_matrix( pModel->mesh->inv_bind_matrix, pModel->mesh->abs_bind_matrix, pModel->skel->joint_count);

    // Update joint linking, hierarchy info and other things.
    IqeB_SkelUpdateHierarchy( pModel->skel);
	}

}

/************************************************************************************
 * IqeB_ModelBuildBlendColors()
 *
 * Build a color table (one element for each model vector)
 * to show the blend weights influence. Use this colors
 * to skin a model
 *
 *
 */

#ifdef use_hue_again
// Returns a color across the hue space from given parameter in [0,1]

static void GetHueIterplolated( vec3 pColorOut, float hue) // static method
{
	// usisng ease-in/ease-out to generate a more linear distributed hue (optional)
	hue = -(2.0f*(hue*hue*hue)) + (3.0f*(hue*hue));

	// Calculate RGB from HSV (Hue color space). added by David Huang
	float h = 1.0f - hue;
	if(h > 1.0f) h = 1.0f; // input Hue is bounded between 0 and 1
	if (h<0.0f) h=0.0f;
	//if(h < -gstiny) return GsColor::black; // this may generate interesting result

	float hsv_h, hsv_s, hsv_v, sat_r, sat_g, sat_b;

	//hsv_h = h*360.0f*0.65f - 6.0f; // re-mapping hue space
	hsv_h = h * 240.0f; // re-mapping hue space
	hsv_s = 1.0f; hsv_v = 1.0f;

	if(hsv_h < 0.0f)
		hsv_h += 360.0f;
	if(hsv_h > 360.0f)
		hsv_h -= 360.0f;

	if(hsv_h < 120.0f)
	{
		sat_r = (120.0f - hsv_h) / 60.0f;
		sat_g = hsv_h / 60.0f;
		sat_b = 0.0f;
	}
	else if(hsv_h < 240.0f)
	{
		sat_r = 0.0f;
		sat_g = (240.0f - hsv_h) / 60.0f;
		sat_b = (hsv_h - 120.0f) / 60.0f;
	}
	else
	{
		sat_r = (hsv_h - 240.0f) / 60.0f;
		sat_g = 0.0f;
		sat_b = (360.0f - hsv_h) / 60.0f;
	}
	if(sat_r > 1) sat_r = 1;
	if(sat_g > 1) sat_g = 1;
	if(sat_b > 1) sat_b = 1;

	pColorOut[ 0] = (1.0f - hsv_s + hsv_s * sat_r) * hsv_v;
	pColorOut[ 1] = (1.0f - hsv_s + hsv_s * sat_g) * hsv_v;
	pColorOut[ 2] = (1.0f - hsv_s + hsv_s * sat_b) * hsv_v;

	return;
}
#else

// Returns a color from a predefined color table

static float BlendColorTable[8][3] = {
  { 1.0, 0.0, 0.0},  // red
  { 1.0, 1.0, 0.0},  // yellow
  { 0.0, 1.0, 0.0},  // green
  { 0.0, 1.0, 1.0},  // cya
  { 0.0, 0.0, 1.0},  // blue
  { 1.0, 0.0, 1.0}   // mangenta
};

static void GetBlendColorFromTable( vec3 pColorOut, int i) // static method
{
  i = i % 6;    // index in color table

	pColorOut[ 0] = BlendColorTable[ i][ 0];
	pColorOut[ 1] = BlendColorTable[ i][ 1];
	pColorOut[ 2] = BlendColorTable[ i][ 2];
}

#endif

static void IqeB_ModelBuildBlendColorsHirarchy( struct skel *skel, int iParent, int ColorNumber)
{
  int iJoint;

  // Set color of parent

#ifdef use_hue_again
  // Repeat color after 8 vertexes
  GetHueIterplolated( skel->TempBoneColor[ iJoint], (ColorNumber % 8) / 8.0);
#else
  GetBlendColorFromTable( skel->j[ iParent].TempBoneColor, ColorNumber);
#endif

  ColorNumber += 1;

  // Set color of childs

  // Childs start only after iParent
  for( iJoint = iParent + 1; iJoint < skel->joint_count; iJoint++) {

    if( iParent == skel->j[ iJoint].parent) {    // Is child from parent joint above

      IqeB_ModelBuildBlendColorsHirarchy( skel, iJoint, ColorNumber);

      ColorNumber += 1;
    }
  }

}



// ...

void IqeB_ModelBuildBlendColors( struct IQE_model *pModel)
{
  int iJoint, i, n;
  vec3 TempColor;
	int *bi;
	float *bw, *bc;

  // security tests
  if( pModel == NULL || pModel->mesh == NULL) {  // security test, need this pointers

    return;   // exit to caller
  }

  if( pModel->skel == NULL ||              // security test, need a skeleton
      pModel->skel->joint_count <= 0 ||    // minimum need one joint
      pModel->mesh->vertex_count <= 0 ||   // minimum need one vertex
      pModel->mesh->blendindex == NULL ||  // need this
      pModel->mesh->blendweight == NULL) { // need this

    goto ErrorExit;
  }

  // allocate memory for blend colors.
  // If there is already memory reallocate it

  if( pModel->mesh->blendColors == NULL) {  // no data untilo now

    pModel->mesh->blendColors = (float *)malloc( pModel->mesh->vertex_count * 4 * sizeof(float));

  } else {  // already have data, relloacte

    pModel->mesh->blendColors = (float *)realloc( pModel->mesh->blendColors, pModel->mesh->vertex_count * 4 * sizeof(float));
  }

  // make color per joint

#ifdef use_again
  for( iJoint = 0; iJoint < pModel->skel->joint_count; iJoint++) {

#ifdef use_hue_again
    // Repeat color after 8 vertexes
    GetHueIterplolated( pModel->skel->TempBoneColor[ iJoint], (iJoint % 8) / 8.0);
#else
    GetBlendColorFromTable( pModel->skel->j[ iJoint].TempBoneColor, iJoint);
#endif
  }
#else

  int ColorNumber;

  ColorNumber = 0;

  for( iJoint = 0; iJoint < pModel->skel->joint_count; iJoint++) {

    if( pModel->skel->j[ iJoint].parent < 0) {   // Is a root joint

      IqeB_ModelBuildBlendColorsHirarchy( pModel->skel, iJoint, ColorNumber);

      ColorNumber += 1;
    }
  }
#endif

  // Merge the colors of the bones depenting on blend weights
  // NOTE: The sum of the weights is 1.0 (per definition)

	bi = pModel->mesh->blendindex;
	bw = pModel->mesh->blendweight;
	bc = pModel->mesh->blendColors;
	n  = pModel->mesh->vertex_count;

	while (n--) {                   // over all vertices

		vec_set( TempColor, 0.0, 0.0, 0.0);  // init color sum

		for (i = 0; i < 4; i++) {     // up to 4 blends per vertex

			if( bw[i] == 0.0) {         // end of list
        break;
			}

			vec_scale_add_to( TempColor, pModel->skel->j[ bi[ i]].TempBoneColor, bw[ i]);
		}

    if( i == 0) {      // There was no blend assigned

  		vec_set( TempColor, 0.4, 0.4, 0.4);  // Show a gray color
    }

		bc[ 0] = TempColor[ 0];
   	bc[ 1] = TempColor[ 1];
	  bc[ 2] = TempColor[ 2];
		bc[ 3] = 1.0;

		bi += 4; bw += 4;
		bc += 4;
	}

	// done

  pModel->mesh->blendColorsRebuild = 0;   // Reset flag rebuild of 'blendColors'

	return;  // return to caller

//
// Error exit
//

ErrorExit:

  // Got error, enshure blend color data is removed

  if( pModel->mesh->blendColors) {    // got blend colors from before

    free( pModel->mesh->blendColors); // free the data

    pModel->mesh->blendColors = NULL; // flag no data
  }

  pModel->mesh->blendColorsRebuild = 0;   // Reset flag rebuild of 'blendColors'

  return;
}

/************************************************************************************
 * IqeB_ModelSizeScale
 *
 * Scale the model by the given factor.
 *
 * Return:      0:   done
 *           else:   error
 */

int IqeB_ModelSizeScale( struct IQE_model *pModel, float Scale)
{
  int i, n;

  // security tests
  if( pModel == NULL) {  // security test, need this pointer

    return( -1);   // exit to caller
  }

  if( Scale > - 0.0001 && Scale < 0.0001) {  // avoid divsion by zero

    Scale = 0.0001;
  }

  //
  // scale the mesh
  //

  if( pModel->mesh != NULL && pModel->mesh->position) {  // have a mesh with postion

    float *pVertex;

    n = pModel->mesh->vertex_count;
    pVertex = pModel->mesh->position;

	  for( i = 0; i < n; i++, pVertex += 3) {

      pVertex[ 0] *= Scale;
      pVertex[ 1] *= Scale;
      pVertex[ 2] *= Scale;
	 }
  }

  //
  // scale the skeleton
  //

  if( pModel->skel != NULL && pModel->skel->joint_count > 0) {  // have a skeleton

    n = pModel->skel->joint_count;

    for( i = 0; i < n; i++) {

      pModel->skel->pose[ i].location[ 0] *= Scale;
      pModel->skel->pose[ i].location[ 1] *= Scale;
      pModel->skel->pose[ i].location[ 2] *= Scale;
    }

    IqeB_ModelSkeletonMatrixCalc( pModel);  // update skeleton matrix
  }

  //
  // Skale the animations
  //

  if( pModel->anim_data != NULL && pModel->anim_count >= 1) {

    int j, f;
    struct anim *pCurAnim;
    struct pose *pPose;

    n = pModel->skel->joint_count;

    for( j = 0; j < pModel->anim_count; j++) {

      pCurAnim = pModel->anim_data[ j];

      for( f = 0; f < pCurAnim->len; f++) {

        pPose = pCurAnim->data[ f];

        for( i = 0; i < n; i++) {

          pPose[ i].location[ 0] *= Scale;
          pPose[ i].location[ 1] *= Scale;
          pPose[ i].location[ 2] *= Scale;
        }
      }
    }

    IqeB_AnimPrepareAfterLoad( pModel);            // Prepare animations

	  if( pModel == pDrawModel && pModel->anim_count > 0) {  // if this is the model displayed

      IqeB_AnimAction( IQE_ANIMATE_ACTION_UPDATE, 0);  // also update current used animation data
	  }
  }

	return( 0);  // return OK
}

/************************************************************************************
 * IqeB_ModelTranslate
 *
 * Translate the model by
 *
 * Return:      0:   done
 *           else:   error
 */

int IqeB_ModelTranslate( struct IQE_model *pModel, vec3 Translate)
{
  int i, n;

  // security tests
  if( pModel == NULL) {  // security test, need this pointer

    return( -1);   // exit to caller
  }

  //
  // translate the mesh
  //

  if( pModel->mesh != NULL && pModel->mesh->position) {  // have a mesh with postion

    float *pVertex;

    n = pModel->mesh->vertex_count;
    pVertex = pModel->mesh->position;

	  for( i = 0; i < n; i++, pVertex += 3) {

      vec_add_to( pVertex, Translate);
	  }
  }

  //
  // translate the skeleton
  //

  if( pModel->skel != NULL && pModel->skel->joint_count > 0) {  // have a skeleton

    n = pModel->skel->joint_count;

    for( i = 0; i < n; i++) {

      if( pModel->skel->j[ i].parent < 0) {   // have NO parent

        vec_add_to( pModel->skel->pose[ i].location, Translate);
      }
    }

    IqeB_ModelSkeletonMatrixCalc( pModel);  // update skeleton matrix
  }

  //
  // translate the animations
  //

  if( pModel->anim_data != NULL && pModel->anim_count >= 1 &&   // have an animation
      pModel->skel != NULL && pModel->skel->joint_count > 0) {  // and have a skeleton

    int j, f;
    struct anim *pCurAnim;
    struct pose *pPose;

    n = pModel->skel->joint_count;

    for( j = 0; j < pModel->anim_count; j++) {

      pCurAnim = pModel->anim_data[ j];

      for( f = 0; f < pCurAnim->len; f++) {

        pPose = pCurAnim->data[ f];

        for( i = 0; i < n; i++) {

          if( pModel->skel->j[ i].parent < 0) {   // have NO parent

            vec_add_to( pPose[ i].location, Translate);
          }
        }
      }
    }

    IqeB_AnimPrepareAfterLoad( pModel);            // Prepare animations

	  if( pModel == pDrawModel && pModel->anim_count > 0) {  // if this is the model displayed

      IqeB_AnimAction( IQE_ANIMATE_ACTION_UPDATE, 0);  // also update current used animation data
	  }
  }

	return( 0);  // return OK
}

/************************************************************************************
 * IqeB_ModelRotate
 *
 * Rotate the model
 *
 * Return:      0:   done
 *           else:   error
 */

int  IqeB_ModelRotate( struct IQE_model *pModel, vec4 QuatRotate)
{
  int i, n;

  // security tests
  if( pModel == NULL) {  // security test, need this pointer

    return( -1);   // exit to caller
  }

  //
  // rotate the mesh
  //

  if( pModel->mesh != NULL && pModel->mesh->position) {  // have a mesh with postion

    float *pVertex;

    n = pModel->mesh->vertex_count;
    pVertex = pModel->mesh->position;

	  for( i = 0; i < n; i++, pVertex += 3) {

      quat_transfrom_vec3( QuatRotate, pVertex, pVertex);
	  }
  }

  if( pModel->mesh != NULL && pModel->mesh->normal) {  // have a mesh with normals

    float *pNormal;

    n = pModel->mesh->vertex_count;
    pNormal = pModel->mesh->normal;

	  for( i = 0; i < n; i++, pNormal += 3) {

      quat_transfrom_vec3( QuatRotate, pNormal, pNormal);
	  }
  }

  //
  // rotate the skeleton
  //

  if( pModel->skel != NULL && pModel->skel->joint_count > 0) {  // have a skeleton

    struct pose *pPose;

    n = pModel->skel->joint_count;

    for( i = 0; i < n; i++) {

      pPose = pModel->skel->pose;

      if( pModel->skel->j[ i].parent < 0) {   // have NO parent

        quat_mul( pPose[ i].rotation, QuatRotate, pPose[ i].rotation);
        quat_transfrom_vec3( QuatRotate, pPose[ i].location, pPose[ i].location);
      }
    }

    IqeB_ModelSkeletonMatrixCalc( pModel);  // update skeleton matrix
  }

  //
  // rotate the animations
  //

  if( pModel->anim_data != NULL && pModel->anim_count >= 1 &&   // have an animation
      pModel->skel != NULL && pModel->skel->joint_count > 0) {  // and have a skeleton

    int j, f;
    struct anim *pCurAnim;
    struct pose *pPose;

    n = pModel->skel->joint_count;

    for( j = 0; j < pModel->anim_count; j++) {

      pCurAnim = pModel->anim_data[ j];

      for( f = 0; f < pCurAnim->len; f++) {

        pPose = pCurAnim->data[ f];

        for( i = 0; i < n; i++) {

          if( pModel->skel->j[ i].parent < 0) {   // have NO parent

            quat_mul( pPose[ i].rotation, QuatRotate, pPose[ i].rotation);
            quat_transfrom_vec3( QuatRotate, pPose[ i].location, pPose[ i].location);
          }
        }
      }
    }

    IqeB_AnimPrepareAfterLoad( pModel);            // Prepare animations

	  if( pModel == pDrawModel && pModel->anim_count > 0) {  // if this is the model displayed

      IqeB_AnimAction( IQE_ANIMATE_ACTION_UPDATE, 0);  // also update current used animation data
	  }
  }

	return( 0);  // return OK
}

/************************************************************************************
 * IqeB_ModelSkelNormalize()
 *
 * Remap the joints location (use lacation, rotation, size as used in the skeleton).
 * Also resets the joints roation and size information.
 * NOTE: Need no precalucations. Onyl the skeleton data is used.
 *
 * NOTE 05.04.2015 RR: Works for a skeleton.
 *                     Missing: If there are animations, this anmations
 *                              must get the rotation information we remove
 *                              from the skeleton.
 *
 * NOTE 27.05.2015 RR: Reworked normalization.
 *                     The Rotation from the root bones is moved to it's childs.
 *
 * return:   0: Done OK
 *         < 0: error
 *          -1: No skeleton data
 */

#define SKEL_NORMALIZE_RESET_SCALE 1  // define this to also reset all scales

static void IqeB_ModelSkelNormalizeChildsRemoveRotation( struct IQE_model *pModel, int iParent, int DoAnimations)
{
  struct skel *pSkel;
  struct anim *pCurAnim;
  struct pose *pPose;
  vec4 PoseRotate;
#ifdef SKEL_NORMALIZE_RESET_SCALE  // Also reset all scales
  vec3 PoseScale;
#endif
  vec4 PoseRotateInv;
#ifdef SKEL_NORMALIZE_RESET_SCALE  // Also reset all scales
  vec3 PoseScaleInv;
#endif
  int iPose, DoPoseCalcs, nPoses;
  int iAnim, iFrame;

  // false: reset rotation and scale of all childs
  // true: poses with no childs keep their rotation and scle (the leave childs)
  int LeaveChildsKeepRotScale = false;


  pSkel = pModel->skel;

  // Test to have childs

  if( LeaveChildsKeepRotScale &&          // Leave childs keep rotation and scale
      pSkel->j[ iParent].nChilds <= 0) {  // Have no childs

    return;                               // Nothing to do
  }

  // Is there a rotation ?
  DoPoseCalcs = pSkel->pose[ iParent].scale[ 0] != 1.0 ||
                pSkel->pose[ iParent].scale[ 1] != 1.0 ||
                pSkel->pose[ iParent].scale[ 2] != 1.0 ||
                pSkel->pose[ iParent].rotation[ 0] != 0.0 ||
                pSkel->pose[ iParent].rotation[ 1] != 0.0 ||
                pSkel->pose[ iParent].rotation[ 2] != 0.0 ||
                (pSkel->pose[ iParent].rotation[ 3] != 1.0 && pSkel->pose[ iParent].rotation[ 3] != -1.0);

  // Get pose info of this parent skeleton

  quat_copy( PoseRotate, pSkel->pose[ iParent].rotation);
#ifdef SKEL_NORMALIZE_RESET_SCALE  // Also reset all scales
  vec_copy( PoseScale, pSkel->pose[ iParent].scale);
#endif

  // Inverse of rotation and scale

  quat_copy( PoseRotateInv, PoseRotate);
  quat_inverse( PoseRotateInv);

#ifdef SKEL_NORMALIZE_RESET_SCALE  // Also reset all scales
  vec_set( PoseScaleInv, 1.0 / PoseScale[ 0], 1.0 / PoseScale[ 1], 1.0 / PoseScale[ 2]);
#endif

  // Now reset the parents skeleton pose information

  quat_set_identity( pSkel->pose[ iParent].rotation);
#ifdef SKEL_NORMALIZE_RESET_SCALE  // Also reset all scales
  vec_set( pSkel->pose[ iParent].scale, 1.0, 1.0, 1.0);  // default scale
#endif

  //
  // Animation parent poses get's the inverse of the skeleton
  //

#ifndef use_again
  if( DoPoseCalcs && DoAnimations) {  // Do animations

    for( iAnim = 0; iAnim < pModel->anim_count; iAnim++) {

      pCurAnim = pModel->anim_data[ iAnim];

      for( iFrame = 0; iFrame < pCurAnim->len; iFrame++) {

        pPose = pCurAnim->data[ iFrame];    // point to this frame poses
        pPose += iParent;                   // Modify this pose

        // Now subtract from the parents skeleton pose information

        quat_mul( pPose->rotation, pPose->rotation, PoseRotateInv);

#ifdef SKEL_NORMALIZE_RESET_SCALE  // Also reset all scales
        vec_mul_to( pPose->location, PoseScaleInv);
        vec_mul_to( pPose->scale, PoseScaleInv);
#endif
      }
    }
  }
#endif

  //
  // 1. pass, correct childs for the rotation of it's parent.
  //

  nPoses = pSkel->joint_count;

  // Childs start only after iParent
  for( iPose = iParent + 1; iPose < nPoses; iPose++) {

    if( iParent == pSkel->j[ iPose].parent) {  // Is child from parent joint above

      if( DoPoseCalcs) {  // Do the pose calculations

        //
        // Corrections for the skeleton
        //

        // Transform child locations by parent rotation
        quat_transfrom_vec3( PoseRotate, pSkel->pose[ iPose].location, pSkel->pose[ iPose].location);

        // Add rotation of parent to it's direct child
        quat_mul( pSkel->pose[ iPose].rotation, PoseRotate, pSkel->pose[ iPose].rotation);

#ifdef SKEL_NORMALIZE_RESET_SCALE  // Also reset all scales
        vec_mul_to( pSkel->pose[ iPose].location, PoseScale);
        vec_mul_to( pSkel->pose[ iPose].scale, PoseScale);
#endif

        //
        // Corrections for the animations
        //
#ifndef use_again
        if( DoAnimations) {  // Do animations

          for( iAnim = 0; iAnim < pModel->anim_count; iAnim++) {

            pCurAnim = pModel->anim_data[ iAnim];

            for( iFrame = 0; iFrame < pCurAnim->len; iFrame++) {

              pPose = pCurAnim->data[ iFrame];    // point to this frame poses
              pPose += iPose;                     // Modify this pose

              // ...

              quat_transfrom_vec3( PoseRotate, pPose->location, pPose->location);

              // Add rotation of parent to it's direct child
              quat_mul( pPose->rotation, PoseRotate, pPose->rotation);

#ifdef SKEL_NORMALIZE_RESET_SCALE  // Also reset all scales
              vec_mul_to( pPose->location, PoseScale);
              vec_mul_to( pPose->scale, PoseScale);
#endif
            }
          }
        }
#endif
      }
    }
  }

  //
  // 2. pass, recursive go down the childs
  //

  // Childs start only after iParent
  for( iPose = iParent + 1; iPose < pSkel->joint_count; iPose++) {

    if( iParent == pSkel->j[ iPose].parent &&    // Is child from parent joint above
        (LeaveChildsKeepRotScale == false ||     // And NOT leave childs keep rotation and scale
         pSkel->j[ iPose].nChilds > 0)) {        // or this one has childs

      IqeB_ModelSkelNormalizeChildsRemoveRotation( pModel, iPose, DoAnimations);
    }
  }

}

int IqeB_ModelSkelNormalize( struct IQE_model *pModel)
{
#ifdef use_again // 27.05.2015 RR: ---
  struct skel *pSkel;
	int iJoint, iParent;
  mat4 loc_bind_matrix[MAXBONE];
	mat4 abs_bind_matrix[MAXBONE];

  // skeleton

  pSkel = pModel->skel;

	if( pSkel->joint_count <= 0) {  // no skeleton

    return( -1);
	}

  // do calculations ...

  calc_matrix_from_pose( loc_bind_matrix, pSkel->pose, pSkel->joint_count);
  calc_abs_matrix( abs_bind_matrix, loc_bind_matrix, pSkel);

  // ...

  for( iJoint = 0; iJoint < pSkel->joint_count; iJoint++) {

    iParent = pSkel->j[ iJoint].parent;

    if( iParent < 0 || iParent >= pSkel->joint_count) {     // parent index has a problem

      // Must be a root joint

      pSkel->j[ iJoint].parent = -1;     // Set parent

      vec_copy( pSkel->pose[ iJoint].location, abs_bind_matrix[ iJoint] + 12);

    } else {

      // difference to parent

      vec_copy( pSkel->pose[ iJoint].location, abs_bind_matrix[ iJoint] + 12);
      vec_sub_from( pSkel->pose[ iJoint].location, abs_bind_matrix[ iParent] + 12);
    }

    vec_set( pSkel->pose[ iJoint].scale, 1.0, 1.0, 1.0);  // default scale
    quat_set_identity( pSkel->pose[ iJoint].rotation);    // default rotation
	}

  return( 0);  // return OK

#else    // 27.05.2015 RR: ---
  struct skel *pSkel;
	int iJoint, iParent, DoAnimations;

  // skeleton

	if( pModel == NULL || pModel->skel == NULL || pModel->skel->joint_count <= 0) {  // no skeleton

    return( -1);
	}

  pSkel = pModel->skel;

	// Can recalc animations

  DoAnimations =  pModel->anim_data != NULL && pModel->anim_count >= 1 &&   // have an animation
                  pSkel != NULL && pSkel->joint_count > 0 &&                // and have a skeleton
                  pSkel->joint_count == pModel->anim_poses;                 // Poses and joint count must match

  //
  // Recursive remove the rotation from the childs and the animations
  //

  if( pSkel != NULL && pSkel->joint_count > 0) {  // have a skeleton

    for( iJoint = 0; iJoint < pSkel->joint_count; iJoint++) {

      iParent = pSkel->j[ iJoint].parent;

      if( iParent < 0 || iParent >= pSkel->joint_count) {     // parent index has a problem

        // Must be a root joint

        pSkel->j[ iJoint].parent = -1;     // Set parent

        // Make sense to remove the rotation

        IqeB_ModelSkelNormalizeChildsRemoveRotation( pModel, iJoint, DoAnimations);
      }
    }

    // and update ...

    IqeB_ModelSkeletonMatrixCalc( pModel);  // update skeleton matrix
	}

  // and update for animations

  if( DoAnimations) {  // Do animations

    IqeB_AnimPrepareAfterLoad( pModel);            // Prepare animations

	  if( pModel == pDrawModel && pModel->anim_count > 0) {  // if this is the model displayed

      IqeB_AnimAction( IQE_ANIMATE_ACTION_UPDATE, 0);  // also update current used animation data
	  }
  }

  return( 0);  // return OK

#endif   // 27.05.2015 RR: ---
}

/****************************** End Of File ******************************/
