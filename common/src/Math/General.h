/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef MATH_GENERAL_H
#define MATH_GENERAL_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <stdlib.h>		//< min, max
#include <math.h>		//< math functions

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#ifndef FLOAT_TOLERANCE
#	if PRECISION_TYPE == SINGLE_PRECISION
#		define FLOAT_TOLERANCE 1e-05
#	elif PRECISION_TYPE == DOUBLE_PRECISION
#		define FLOAT_TOLERANCE 1e-09
#	endif
#endif

#ifndef MATH_PI
#	define MATH_PI			3.14159265358979323846f
#endif

#ifndef MATH_EPSILON
#	define MATH_EPSILON		0.0000001f
#endif

#ifndef MATH_RADIAN
#	define MATH_RADIAN		3.14159265358979323846f / 180.0f
#endif

////////////////////////////////////////////////////////////
// Default macros
////////////////////////////////////////////////////////////
#define sqr(x)		((x) * (x))

#ifndef max
#	define max(a, b)	((a) > (b) ? (a) : (b))
#endif

#ifndef min
#	define min(a, b)	((a) < (b) ? (a) : (b))
#endif

#ifndef clamp // a : min, b : max
#	define clamp(Value, Min, Max) (((Value) > (Max)) ? (Max) : (((Value) < (Min) ) ? (Min) : (Value)))
#endif

#define degtorad(a)	(((a) * MATH_PI) / 180.0f)
#define radtodeg(a)	(((a) * 180.0f) / MATH_PI)

////////////////////////////////////////////////////////////
/// Standard definitons
////////////////////////////////////////////////////////////
/*#ifdef cos
	#undef cos
	#define cos		MathCos
#endif

#ifdef sin
	#undef sin
	#define sin		MathSin
#endif

#ifdef sqrt
	#undef sqrt
	#define sqrt	MathSqrt
#endif*/

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize the library
////////////////////////////////////////////////////////////
void MathInitialize();

////////////////////////////////////////////////////////////
/// Sine function
////////////////////////////////////////////////////////////
float MathSin(float Radians);

////////////////////////////////////////////////////////////
/// Cosine function
////////////////////////////////////////////////////////////
float MathCos(float Radians);

////////////////////////////////////////////////////////////
/// Square root function
////////////////////////////////////////////////////////////
float (*MathSqrt)(float Value);

////////////////////////////////////////////////////////////
/// Calculate the next power of two
////////////////////////////////////////////////////////////
uinteger MathPowerOfTwo(uinteger Value);

////////////////////////////////////////////////////////////
/// Determines if two floating-point are close enough
/// together that they can be considered equal
////////////////////////////////////////////////////////////
bool MathCloseEnough(float f1, float f2);

////////////////////////////////////////////////////////////
/// Returns a gradual transition of 'x' from 0 to 1 beginning at
/// threshold 'a' and ending at threshold 'b'
///
/// References:
///  [1] http://www.rendermanacademy.com/docs/smoothstep.htm
///  [2] http://www.brlcad.org/doxygen/d8/d33/noise_8c-source.html
///  [3] Natalya Tatarchuk, "Efficient High-Level Shader Development",
///      Game Developers Conference Europe, August 2003
////////////////////////////////////////////////////////////
float MathSmoothStep(float a, float b, float x);

////////////////////////////////////////////////////////////
/// Determines if a number is in the range [Min Max]
////////////////////////////////////////////////////////////
bool MathCheckBoundsi(int32 * Value, int32 Min, int32 Max);

bool MathCheckBoundsf(float * Value, float Min, float Max);

////////////////////////////////////////////////////////////
/// Discover the orientation of a triangle's points
////////////////////////////////////////////////////////////
int32 MathTriangleOrientation(int32 pX, int32 pY, int32 qX, int32 qY, int32 rX, int32 rY);

#endif // MATH_GENERAL_H
