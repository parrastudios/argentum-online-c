/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <GUI/Widget.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Alloc memory widget
////////////////////////////////////////////////////////////
struct GuiWidget * GuiWidgetCreate(uinteger Type, integer X, integer Y, uinteger FontSize, char * Text)
{
	struct GuiWidget * Widget = (struct GuiWidget *)MemoryAllocate(sizeof(struct GuiWidget));

	if (Widget)
	{
		Widget->Enabled = true;
		Widget->Visible = true;
		Widget->Border = true;
		Widget->FontSize = FontSize;
		Widget->X = X;
		Widget->Y = Y;
		Widget->Close = Widget->Open = NULL;
		Widget->KeyPressed = NULL;
		Widget->KeyReleased = NULL;
		Widget->TextEntered = NULL;
		Widget->MouseMove = NULL;
		Widget->ButtonPressed = Widget->ButtonReleased = NULL;
		Widget->Next = Widget->Previous = NULL;
		Widget->Type = Type;

		// Set the text
		if (Text != NULL)
		{
			strcpy(Widget->Text, Text);
			Widget->Length = strlen(Text);
		}
		else
		{
			Widget->Text[0] = NULLCHAR;
			Widget->Length = 0;
		}

		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Create widget without allocating memory by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiWidgetCreateDefault(uinteger Type)
{
	struct GuiWidget * Widget = (struct GuiWidget *)MemoryAllocate(sizeof(struct GuiWidget));

	if (Widget)
	{
		Widget->Enabled = true;
		Widget->Visible = true;
		Widget->Border = true;
		Widget->FontSize = 8;
		Widget->X = GUI_FORM_CENTERED;
		Widget->Y = GUI_FORM_CENTERED;
		Widget->Close = Widget->Open = NULL;
		Widget->KeyPressed = NULL;
		Widget->KeyReleased = NULL;
		Widget->TextEntered = NULL;
		Widget->MouseMove = NULL;
		Widget->ButtonPressed = Widget->ButtonReleased = NULL;
		Widget->Next = Widget->Previous = NULL;
		Widget->Type = Type;
		Widget->Text[0] = NULLCHAR;
		Widget->Length = 0;

		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Copy the events of the widget
////////////////////////////////////////////////////////////
void GuiWidgetCopyEvents(struct GuiWidget * Dest, struct GuiWidget * Src)
{
	Dest->KeyPressed		= Src->KeyPressed;
	Dest->KeyReleased		= Src->KeyReleased;
	Dest->TextEntered		= Src->TextEntered;
	Dest->ButtonPressed		= Src->ButtonPressed;
	Dest->ButtonReleased	= Src->ButtonReleased;
	Dest->MouseMove			= Src->MouseMove;
	Dest->Open				= Src->Open;
	Dest->Close				= Src->Close;
}

////////////////////////////////////////////////////////////
// Toggle visibility
////////////////////////////////////////////////////////////
struct GuiWidget * GuiWidgetToggleVisibility(struct GuiWidget * Widget)
{
    if (Widget)
    {
		Widget->Visible = !Widget->Visible;

		return Widget;
    }

    return NULL;
}

////////////////////////////////////////////////////////////
// Destroy widget and memory
////////////////////////////////////////////////////////////
void GuiWidgetDestroy(struct GuiWidget * Widget)
{
    // Destroy widget
    MemoryDeallocate(Widget);
}
