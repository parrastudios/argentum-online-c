/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_TIMER_H
#define SYSTEM_TIMER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct Timer
{
	float LastCounter;	///< Saves original absolute start time
	float ElapsedTime;	///< Saves last check of the timer (the absolute current time)
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Clears the timer
////////////////////////////////////////////////////////////
void TimerClear(struct Timer * TimerData);

////////////////////////////////////////////////////////////
// Reset the timer to 0
////////////////////////////////////////////////////////////
void TimerReset(struct Timer * TimerData);

////////////////////////////////////////////////////////////
// Check time
////////////////////////////////////////////////////////////
bool TimerCheck(struct Timer * TimerData, float Interval);

////////////////////////////////////////////////////////////
// Check time by specified elapsed time
////////////////////////////////////////////////////////////
bool TimerCheckEx(struct Timer * TimerData, float ElapsedTime, float Interval);

////////////////////////////////////////////////////////////
// Return relative time of timer
////////////////////////////////////////////////////////////
float TimerGetTime(struct Timer * TimerData);

#endif // SYSTEM_TIMER_H
