/**
 * Include files
 */
var promise	= require('./promise.js');	///< promise module
var rand 	= require('./rand.js');		///< random module
var debug 	= require('./debug.js');	///< debug module

// promise test

function my_func(value) {
	var p = new promise.Promise();

	p.defer();

	debug.log('my_func is called');

	if (value == true) {
		var m = 1.0;
		//setTimeout(function() {
			for (var i = 0; i < 10000000; ++i) {
				var a = 0.43;
				var b = 3242.4043242423432 / a;
				var c = ((c * a / b) / c / a / b) / 9.875656565 * m;

				m = c * 10.0;
			}
			debug.log('resolve is called');
			p.resolve();
		//}, 3000);
	} else {
		setTimeout(function() {
			debug.log('reject is called');
			p.reject();
		}, 3000);
	}

	return p;
}

var result_callback = (function(args) {
	debug.log('result - args:', args.get_value(0), args.get_value(1));
})

var error_callback = (function(args) {
	debug.log('error - args:', args.get_value(0), args.get_value(1));
});

/**
 *	Application main instance
 */
(function main() {
	// promise test

	// create promise module
	promise.Initializer('q');

	debug.log('get the promise');

	p = my_func(true); // with setTimeout == async, with direct call == sync

	debug.log('register result');

	p.register_result(new promise.Callback(result_callback, new promise.Arguments('a', 'b')));

	debug.log('register error');

	p.register_error(new promise.Callback(error_callback, new promise.Arguments('c', 'd')));

	debug.log('p.raise();');
	
	p.raise();

	// rand test

	debug.log('randomized string', rand.str());
})();
