/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GUI_WINDOW_H
#define GUI_WINDOW_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <GUI/Widget.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define GUI_WINDOW_NOTLOADED	0x00
#define GUI_WINDOW_OPENED		0x01
#define GUI_WINDOW_CLOSED		0x02

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct GuiWindow
{
	uint32 hStatus;					///< Window state

	struct GuiWidget * First;		///< Fisrt widget on window
	struct GuiWidget * Last;		///< Last widget on window
	struct GuiWidget * FocusWidget;	///< Widget that has the focus

	struct GuiWindow * Next;		///< Next node
	struct GuiWindow * Previous;	///< Previous node
};

////////////////////////////////////////////////////////////
// Add widget to window
////////////////////////////////////////////////////////////
void GuiWindowAddWidget(struct GuiWindow * Window, struct GuiWidget * Widget);

////////////////////////////////////////////////////////////
/// Remove widget to window
////////////////////////////////////////////////////////////
void GuiWindowRemoveWidget(struct GuiWindow * Window, struct GuiWidget * Widget);

////////////////////////////////////////////////////////////
/// Create the window
////////////////////////////////////////////////////////////
struct GuiWindow * GuiWindowCreate();

////////////////////////////////////////////////////////////
/// Destroy the window
////////////////////////////////////////////////////////////
void GuiWindowDestroy(struct GuiWindow * Window);

////////////////////////////////////////////////////////////
/// Render a form
////////////////////////////////////////////////////////////
void GuiWindowRender(struct GuiWindow * Window, bool Focused);

////////////////////////////////////////////////////////////
/// Return the state
////////////////////////////////////////////////////////////
uint32 GuiWindowStatus(struct GuiWindow * Window);

////////////////////////////////////////////////////////////
/// Get the specified widget by name
////////////////////////////////////////////////////////////
struct GuiWidget * GuiWindowGetWidget(struct GuiWindow * Window, char * Name);

////////////////////////////////////////////////////////////
/// Show a widget of a window (assumed that belongs to it)
////////////////////////////////////////////////////////////
void GuiWindowShowWidget(struct GuiWindow * Window,  struct GuiWidget * Widget, bool Show);

#endif // GUI_WINDOW_H
