/****************************************************************************

  IqeB_GUI_ToolsSkelEdit.cpp

  16.02.2015 RR: First editon of this file.


*****************************************************************************
*/

#include "IqeBrowser.h"

#include <math.h>

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Menu.H>

/************************************************************************************
 * Globals
 *
 */

int IqeB_ToolsSkelEdit_MoveSymmetricJoints = false;  // If enabled, move symmetric joints

/************************************************************************************
 * Statics
 */

static  Fl_Window  *pMyToolWin;
static int MyWinPosX = IQE_GUI_NO_WINPOS_X, MyWinPosY = IQE_GUI_NO_WINPOS_Y; // last window position

static Fl_Browser *pJointBrowser;      // Skeleton joints browser
static Fl_Input   *pInputName;

static Fl_Float_Input *pFloat_InputPX, *pFloat_InputPY, *pFloat_InputPZ; // Joint position
static Fl_Float_Input *pFloat_InputA0, *pFloat_InputA1, *pFloat_InputA2; // Joint angles
static Fl_Float_Input *pFloat_InputSX, *pFloat_InputSY, *pFloat_InputSZ; // Joint size

static Fl_Button *pGUI_ButBindDoN;  // Do skin binding, no smooth
static Fl_Button *pGUI_ButBindDoS;  // Do skin binding, with smooth

// ...

static struct pose *pPose_Data_Displayed;                                // Point to pose of displayed data

static int PoseLastDrawnPoseNr = -1;      // Last drawn for pose GUI elements

/************************************************************************************
 * IqeB_GUI_ToolsJointUpdate
 *
 * Updates the GUI things for single joint
 *
 * iJoint: >= 0  Update for this joint
 *         < 0   No joint to update
 */

static void IqeB_GUI_ToolsJointUpdate( int iJoint)
{
  char TempString[ 256];

  IqeB_GUI_WidgetActivate( pGUI_ButBindDoN,  pDrawModel != NULL &&               // Have a model displayed
                                             pDrawModel->skel != NULL &&
                                             pDrawModel->skel->joint_count > 0); // Have a skeleton

  IqeB_GUI_WidgetActivate( pGUI_ButBindDoS,  pDrawModel != NULL &&               // Have a model displayed
                                             pDrawModel->skel != NULL &&
                                             pDrawModel->skel->joint_count > 0); // Have a skeleton

  if( pDrawModel == NULL ||
      pDrawModel->skel  == NULL ||
      pDrawModel->skel->joint_count <= 0 ||
      iJoint < 0 || iJoint >= pDrawModel->skel->joint_count) {  // in range

    if( pInputName) {  // TEST, have GUI elements
      pInputName->value( "");
      IqeB_GUI_WidgetActivate( pInputName, false); // Set item activated/inactive

      pFloat_InputPX->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputPX, false); // Set item activated/inactive
      pFloat_InputPY->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputPY, false); // Set item activated/inactive
      pFloat_InputPZ->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputPZ, false); // Set item activated/inactive

      pFloat_InputA0->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputA0, false); // Set item activated/inactive
      pFloat_InputA1->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputA1, false); // Set item activated/inactive
      pFloat_InputA2->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputA2, false); // Set item activated/inactive

      pFloat_InputSX->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputSX, false); // Set item activated/inactive
      pFloat_InputSY->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputSY, false); // Set item activated/inactive
      pFloat_InputSZ->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputSZ, false); // Set item activated/inactive
    }

    pPose_Data_Displayed = NULL;       // Reset pointer to displayed data
    PoseLastDrawnPoseNr  = -1;

    IqeB_DispStyle_JointSelected = -1; // no joint selected

    return;
  }

  PoseLastDrawnPoseNr  = iJoint;

  if( pInputName) {  // TEST, have GUI elements

    pPose_Data_Displayed = pDrawModel->skel->pose + iJoint;  // Set pointer to displayed data

    pInputName->value(  pDrawModel->skel->j[ iJoint].name);
    IqeB_GUI_WidgetActivate( pInputName, true); // Set item activated/inactive

    // NOTE: Keep info of some element decativated

    sprintf( TempString, "%.3f", pDrawModel->skel->pose[ iJoint].location[ 0]);
    pFloat_InputPX->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputPX, true); // Set item activated/inactive
    sprintf( TempString, "%.3f", pDrawModel->skel->pose[ iJoint].location[ 1]);
    pFloat_InputPY->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputPY, true); // Set item activated/inactive
    sprintf( TempString, "%.3f", pDrawModel->skel->pose[ iJoint].location[ 2]);
    pFloat_InputPZ->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputPZ, true); // Set item activated/inactive

    sprintf( TempString, "%.3f", pDrawModel->skel->pose[ iJoint].angles[ 0]);
    pFloat_InputA0->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputA0, false); // Set item activated/inactive
    sprintf( TempString, "%.3f", pDrawModel->skel->pose[ iJoint].angles[ 1]);
    pFloat_InputA1->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputA1, false); // Set item activated/inactive
    sprintf( TempString, "%.3f", pDrawModel->skel->pose[ iJoint].angles[ 2]);
    pFloat_InputA2->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputA2, false); // Set item activated/inactive

    sprintf( TempString, "%.3f", pDrawModel->skel->pose[ iJoint].scale[ 0]);
    pFloat_InputSX->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputSX, false); // Set item activated/inactive
    sprintf( TempString, "%.3f", pDrawModel->skel->pose[ iJoint].scale[ 1]);
    pFloat_InputSY->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputSY, false); // Set item activated/inactive
    sprintf( TempString, "%.3f", pDrawModel->skel->pose[ iJoint].scale[ 2]);
    pFloat_InputSZ->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputSZ, false); // Set item activated/inactive
  }

  IqeB_DispStyle_JointSelected = iJoint; // set selected joint
}

/************************************************************************************
 * IqeB_GUI_ToolsJointsUpdate
 *
 * Updates the joints view
 */

static void IqeB_GUI_ToolsJointsUpdate( int KeepSelection)
{
  int i, j;
  int LastSelection;
  char TempString[ MAX_FILENAME_LEN + MAXBONE + MAXBONE + MAXBONE];
  char TempIndent[ MAXBONE + MAXBONE + MAXBONE + 1];

  if( pJointBrowser == NULL) {   // security test, need this pointer

    return;
  }

  LastSelection = pJointBrowser->value();  // get index of selected item

  pJointBrowser->clear();                  // empty the list box

  // test for joints parts to display

  if( pDrawModel == NULL ||
      pDrawModel->skel  == NULL ||
      pDrawModel->skel->joint_count <= 0) {

    IqeB_GUI_ToolsJointUpdate( -1);   // deselect
    pJointBrowser->redraw();          // Redraw it

    return;
  }

  // Add joints

  for( i = 0; i < pDrawModel->skel->joint_count; i++) {

#ifdef use_again
    sprintf( TempString, "%2d-%d-%d: %s", pDrawModel->skel->j[ i].nParentsChain, pDrawModel->skel->j[ i].nChilds, pDrawModel->skel->j[ i].nIndent, pDrawModel->skel->j[ i].name);
#else

    // Build Indent string
    for( j = 0; j < pDrawModel->skel->j[ i].nIndent * 3; j++) {
      TempIndent[ j] = ' ';
    }
    TempIndent[ j] = '\0';   // End of string

    sprintf( TempString, "%s%s", TempIndent, pDrawModel->skel->j[ i].name);
#endif

    pJointBrowser->add( TempString);
  }

  if( KeepSelection && LastSelection > 0 && LastSelection <= pJointBrowser->size()) {

    pJointBrowser->select( LastSelection);         // set to last selected

    pJointBrowser->middleline( LastSelection);

    IqeB_GUI_ToolsJointUpdate( LastSelection - 1);

  } else if( pDrawModel->skel->joint_count > 0) {                    // have any

    pJointBrowser->topline( 1);           // position to top line
    IqeB_GUI_ToolsJointUpdate( -1);       // deselect
  }

  pJointBrowser->redraw();     // Redraw it
}

/************************************************************************************
 * IqeB_GUI_UpdateCallback
 *
 * Is called from IqeB_GUI_UpdateWidgets() if something has changed
 * what needs an update (new model displayed, ...).
 */

static void IqeB_GUI_UpdateCallback( int UpdateFlags)
{
  int LastSelection;

  if( pMyToolWin == NULL) {  // security test, has main window

    return;
  }

  if( UpdateFlags & IQE_GUI_UPDATE_JOINT_SELECTED) {  // Joint selection has changed

    if( pJointBrowser != NULL &&   // need this pointer
        IqeB_DispStyle_JointSelected >= 0) {  // have a joint selection

      LastSelection = pJointBrowser->value() - 1;  // get index of selected item

      if( LastSelection != IqeB_DispStyle_JointSelected) {  // current selection is differend

        LastSelection = IqeB_DispStyle_JointSelected + 1;

        pJointBrowser->select( LastSelection);         // set to last selected
        pJointBrowser->middleline( LastSelection);
        IqeB_GUI_ToolsJointUpdate( LastSelection - 1);
      }
    }

    return;
  }

  if( UpdateFlags & IQE_GUI_UPDATE_JOINT_CHANGED) {  // Data of selected joint has changed

    if( pJointBrowser != NULL &&   // need this pointer
        IqeB_DispStyle_JointSelected >= 0) {  // have a joint selection

      LastSelection = pJointBrowser->value() - 1;  // get index of selected item

      if( LastSelection != IqeB_DispStyle_JointSelected) {  // current selection is differend

        LastSelection = IqeB_DispStyle_JointSelected + 1;

        pJointBrowser->select( LastSelection);         // set to last selected
        pJointBrowser->middleline( LastSelection);
      }

      IqeB_GUI_ToolsJointUpdate( IqeB_DispStyle_JointSelected);
    }

    return;
  }

  IqeB_GUI_ToolsJointsUpdate( false);
}

/************************************************************************************
 * PopupMenuJoints
 *
 * Right mouse button on selected joint or background.
 * Bring up a popup menu.
 */
static void PopupMenuJoints()
{
  Fl_Menu_Item menu_new_root[] = {
    { "Add root joint",          0, NULL, (void*)IQE_SKEL_JOINT_ADD_ROOT },
    { 0 }
  };

  Fl_Menu_Item menu_all[] = {
    { "Add child joint",          0, NULL, (void*)IQE_SKEL_JOINT_ADD_CHILD },
    { "Add root joint",           0, NULL, (void*)IQE_SKEL_JOINT_ADD_ROOT },
    { "Delete this",              0, NULL, (void*)IQE_SKEL_JOINT_DEL_THIS },
    { "Delete this and children", 0, NULL, (void*)IQE_SKEL_JOINT_DEL_CHILDS },
    { 0 }
  };

  const Fl_Menu_Item *m;
  int iJoint, iNewSel, ierr, Action, nJointsOld;

  // Security tests

  if( pDrawModel == NULL ||             // No model loaded
      pDrawModel->skel  == NULL) {

    return;
  }

  // Joint selected

  iJoint = IqeB_DispStyle_JointSelected;  // selected joint

  // pop up ...

  if( pDrawModel->skel->joint_count <= 0 ||                     // Model has no joints
      iJoint < 0 || iJoint >= pDrawModel->skel->joint_count) {  // or no joint selected

    m = menu_new_root->popup( Fl::event_x(), Fl::event_y(), 0, 0, 0);
  } else {

    m = menu_all->popup( Fl::event_x(), Fl::event_y(), 0, 0, 0);
  }

  if( m ) {

    Action = (int)m->user_data();

    nJointsOld = pDrawModel->skel->joint_count;   // remember the number of joints

    ierr = IqeB_SkelJointManipulate( pDrawModel->skel, Action, iJoint);

    iNewSel = iJoint;   // New joint to select

    // and update ...

    if( nJointsOld != pDrawModel->skel->joint_count) {   // number of joints has changed

      // Any animation is invalid now, so remove it
      IqeB_ModelFreeData( &pDrawModel, IQE_MODEL_FREE_ANIMATION);  // remove all animations
    }

    IqeB_ModelSkeletonMatrixCalc( pDrawModel);  // update skeleton matrix

    IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_MODEL_CHANGE); // Also update the GUI

    if( ierr >= 0 && ierr < pDrawModel->skel->joint_count) {

      iNewSel = ierr;
    }

    if( iNewSel < 0 || iNewSel >= pDrawModel->skel->joint_count) {  // out of range

      iNewSel = -1;  // no selection
    }

    IqeB_GUI_ToolsJointUpdate( iNewSel); // Select the new joint / deselect
    if( ierr >= 0) {
      pJointBrowser->select( iNewSel + 1);
      pJointBrowser->middleline( iNewSel + 1);
    }
    pJointBrowser->redraw();          // Redraw it
  }

  return;
}

/************************************************************************************
 * IqeB_JointBrowser_Callback
 */

static void IqeB_JointBrowser_Callback( Fl_Widget *w, void *data)
{
  int this_item;
  int MouseButton, event, is_click;

  if( pDrawModel == NULL ||
      pDrawModel->skel  == NULL) {

    IqeB_GUI_ToolsJointUpdate( -1);   // deselect

    return;
  }

  // Get mouse info about event calling this callback function

	event       = Fl::event();           // Last (this) event processed
	is_click    = Fl::event_is_click();  // Mouse is click
	MouseButton = Fl::event_button();    // mouse button from last event

  // Select item

  this_item = pJointBrowser->value();  // get index of selected item

  if( this_item > 0 && this_item <= pDrawModel->skel->joint_count) { // index is in range

    // Have a selection
    this_item -= 1;   // adapt index from 1 based to 0 based table access

    IqeB_GUI_ToolsJointUpdate( this_item);

    // Test for right mouse button to bring up a popuplist

    if( event == FL_RELEASE && is_click &&
        MouseButton == FL_RIGHT_MOUSE) {  // Right mouse button click

      // Popup menu

      PopupMenuJoints();
    }
  } else {

    // selected the background

    IqeB_GUI_ToolsJointUpdate( -1);   // deselect

    // Test for right mouse button to bring up a popuplist

    if( event == FL_RELEASE && is_click &&
        MouseButton == FL_RIGHT_MOUSE) {  // Right mouse button click

      // Popup menu

      PopupMenuJoints();
    }
  }
}

/************************************************************************************
 * IqeB_GUI_Input_SetValue_Callback
 */

static void IqeB_GUI_Input_SetValue_Callback( Fl_Widget *w, void *data)
{
  Fl_Input *pInput;
  int iJoint, j;
  char TempString[ MAX_FILENAME_LEN + MAXBONE + MAXBONE + MAXBONE];
  char TempIndent[ MAXBONE + MAXBONE + MAXBONE + 1];

  pInput = (Fl_Input *)w;

  if( pInput == NULL) {   // security test

    return;
  }

  iJoint = IqeB_DispStyle_JointSelected;  // selected joint

  if( pDrawModel == NULL ||
      pDrawModel->skel  == NULL ||
      pDrawModel->skel->joint_count <= 0 ||
      iJoint < 0 || iJoint >= pDrawModel->skel->joint_count) {  // in range

    return;
  }

  // ...

  if( pInput == pInputName) {

    if( strcmp( pDrawModel->skel->j[ iJoint].name, pInput->value()) != 0) { // string is different

      // set changed string

      if( pDrawModel->skel->j[ iJoint].name) {

        free( pDrawModel->skel->j[ iJoint].name);
      }

      pDrawModel->skel->j[ iJoint].name = strdup( pInput->value());

      // Also update browser

      // Build Indent string
      for( j = 0; j < pDrawModel->skel->j[ iJoint].nIndent * 3; j++) {
        TempIndent[ j] = ' ';
      }
      TempIndent[ j] = '\0';   // End of string

      sprintf( TempString, "%s%s", TempIndent, pDrawModel->skel->j[ iJoint].name);
      pJointBrowser->text( iJoint + 1, TempString);
    }

    return;
  }
}

/************************************************************************************
 * IqeB_GUI_Float_SetValue_Callback
 */

static void IqeB_GUI_Float_SetValue_Callback( Fl_Widget *w, void *pDummy)
{
  float Value;
  Fl_Float_Input *pThis;
  struct pose *pPose;
  int AnyChanged;

  // ...

  AnyChanged = false;

  pThis  = (Fl_Float_Input *)w;

  Value  = atof( pThis->value());         // update the variable

  if( pPose_Data_Displayed != NULL &&     // security test, haved data to displayed pose
      pDrawModel != NULL &&               // Have drawing model
      pDrawModel->skel != NULL) {         // Have a skeleton

    pPose = pPose_Data_Displayed;

    // Position
    if( pThis == pFloat_InputPX) {

      if( pPose->location[ 0] != Value) {

        AnyChanged = true;
        pPose->location[ 0] = Value;
      }

    } else if( pThis == pFloat_InputPY) {

      if( pPose->location[ 1] != Value) {

        AnyChanged = true;
        pPose->location[ 1] = Value;
      }

    } else if( pThis == pFloat_InputPZ) {

      if( pPose->location[ 2] != Value) {

        AnyChanged = true;
        pPose->location[ 2] = Value;
      }

    // angle
    } else if( pThis == pFloat_InputA0) {

      if( pPose->angles[ 0] != Value) {

        AnyChanged = true;
        pPose->angles[ 0] = Value;
        IqeB_AnimAnglesToRotPose( pPose);
      }

    } else if( pThis == pFloat_InputA1) {

      if( pPose->angles[ 1] != Value) {

        AnyChanged = true;
        pPose->angles[ 1] = Value;
        IqeB_AnimAnglesToRotPose( pPose);
      }

    } else if( pThis == pFloat_InputA2) {

      if( pPose->angles[ 2] != Value) {

        AnyChanged = true;
        pPose->angles[ 2] = Value;
        IqeB_AnimAnglesToRotPose( pPose);
      }

    // scale
    } else if( pThis == pFloat_InputSX) {

      if( Value < 0.001) Value = 0.001; // Clipping

      if( pPose->scale[ 0] != Value) {

        AnyChanged = true;
        pPose->scale[ 0] = Value;
      }

    } else if( pThis == pFloat_InputSY) {

      if( Value < 0.001) Value = 0.001; // Clipping

      if( pPose->scale[ 1] != Value) {

        AnyChanged = true;
        pPose->scale[ 1] = Value;
      }

    } else if( pThis == pFloat_InputSZ) {

      if( Value < 0.001) Value = 0.001; // Clipping

      if( pPose->scale[ 2] != Value) {

        AnyChanged = true;
        pPose->scale[ 2] = Value;
      }
    }

    // Support of symmetric joints modify

    if( pThis == pFloat_InputPX || pThis == pFloat_InputPY || pThis == pFloat_InputPZ) {  // Location modify

      if( IqeB_ToolsSkelEdit_MoveSymmetricJoints &&    // Also move symmetric joints
          pDrawModel->skel->j[ PoseLastDrawnPoseNr].iSymmetric >= 0) {  // This joint is smmetric

        int iOther;
        vec3 PosOther;

        iOther = pDrawModel->skel->j[ PoseLastDrawnPoseNr].iSymmetric;

        // Copy over the positon info
        vec_copy( PosOther, pPose->location);

        // Mirror Y-axis

        PosOther[ 1] = - pPose->location[ 1];

        // Set in

        if( pDrawModel->skel->pose[ iOther].location[ 0] != PosOther[ 0]) {

          AnyChanged = true;
          pDrawModel->skel->pose[ iOther].location[ 0] = PosOther[ 0];
        }

        if( pDrawModel->skel->pose[ iOther].location[ 1] != PosOther[ 1]) {

          AnyChanged = true;
          pDrawModel->skel->pose[ iOther].location[ 1] = PosOther[ 1];
        }

        if( pDrawModel->skel->pose[ iOther].location[ 2] != PosOther[ 2]) {

          AnyChanged = true;
          pDrawModel->skel->pose[ iOther].location[ 2] = PosOther[ 2];
        }
      }
    }

    if( AnyChanged) {

      // update ...

      IqeB_ModelSkeletonMatrixCalc( pDrawModel);  // update skeleton matrix

      // Data of selected joint has changed
      IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_JOINT_CHANGED); // Also update the GUI
    }
  }
}

/************************************************************************************
 * IqeB_GUI_CBox_SetValue_Callback
 */

static void IqeB_GUI_CBox_SetValue_Callback( Fl_Widget *w, void *pValueArg)
{
  int *pValue;
  Fl_Check_Button *pThis;

  // ...

  pThis  = (Fl_Check_Button *)w;
  pValue = (int *)pValueArg;             // get pointer to associated variable

  *pValue = pThis->value();              // update the variable
}

/************************************************************************************
 * IqeB_GUI_Button_Callback
 */

#define ACTION_CODE_BIND_NO_SMOOTH       0  // Do skin binding, no smooth
#define ACTION_CODE_BIND_WITH_SMOOTH     1  // Do skin binding, with smooth

static void IqeB_GUI_Button_Callback( Fl_Widget *w, void *ActionCodeArg)
{
  int ActionCode;

  if( pDrawModel == NULL) {              // have no draw model

    return;                              // nothing to do
  }

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  ActionCode = (int)ActionCodeArg;       // get action code

  switch( ActionCode) {
  case ACTION_CODE_BIND_NO_SMOOTH:   // Do skin binding, no smooth

    IqeB_GUI_ToolsSkinBindDo( false);

    break;

  case ACTION_CODE_BIND_WITH_SMOOTH:   // Do skin binding, with smooth

    IqeB_GUI_ToolsSkinBindDo( true);

    break;

  } // switch
}

/************************************************************************************
 * close_cb, close this window
 *
 * pValueArg is a pointer to the widget. Set this pointer to NULL on deletion.
 */

static void close_cb( Fl_Widget *w, void *pValueArg)
{

  IqeB_DispStyle_JointSelected = -1; // no joint selected

  IqeB_GUI_CloseToolWindow( (void **)&pMyToolWin);
}

/************************************************************************************
 * Presets for this tools window
 *
 */

 static T_GUI_PreferenceEntry MyPreferences[] =
 {

  // Move Symmetric Joints

  { PREF_T_INT,   "MoveSymmetricJoints",   "1", &IqeB_ToolsSkelEdit_MoveSymmetricJoints},
};

// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "ToolsSkelEdit", MyPreferences, sizeof( MyPreferences) / sizeof( T_GUI_PreferenceEntry),
                                               (void **)(&pMyToolWin), &MyWinPosX, &MyWinPosY);

/************************************************************************************
 * IqeB_GUI_ToolsSkelEditWin
 *
 * Open dialog
 */

void IqeB_GUI_ToolsSkelEditWin( int xLeft, int xRight, int yTop, int yBotton)
{

  //
  // winodow already created --> show it
  //

  if( pMyToolWin != NULL) {         // already have tool window

    // Show invisble window or bring visible window to foreground

    pMyToolWin->show();          // show it

    return;
  }

  //
  // creation of window on first call
  //

  if( pMyToolWin == NULL) {  // no tool window until now

    int xPos, yPos, MouseMode;

    xPos = xRight;
    yPos = yTop;

    MouseMode = IQE_GUI_MOUSE_MODE_SKEL_EDIT;     // Mosue mode i wand to set

    if( IqeB_GUI_ToolWindowTestFreeEditMode( MouseMode)) {  // Test to have a free edit mode

      return;
    }

    if( MyWinPosX != IQE_GUI_NO_WINPOS_X && MyWinPosY != IQE_GUI_NO_WINPOS_Y) { // have last window position

      xPos = MyWinPosX;
      yPos = MyWinPosY;
    }

    pMyToolWin = new Fl_Window( xPos, yPos, IQE_GUI_TOOLS_STD_WITDH, 400, "Skeleton/Editor");

    if( pMyToolWin == NULL) {  // security test

      return;
    }

    IqeB_GUI_RegisterCallback( (void **)&pMyToolWin, IqeB_GUI_UpdateCallback,
                               IQE_GUI_UPDATE_MODEL_CHANGE | IQE_GUI_UPDATE_JOINT_SELECTED | IQE_GUI_UPDATE_JOINT_CHANGED,
                               MouseMode, (void(*)(void *, void *))close_cb);
  }

  //
  //  GUI things
  //

  int x1, xx, xx2, xx3, xx4, y1, y, yy, yy2;

  Fl_Check_Button *pTemp_Check_Button;

  x1  = 4;
  xx  = pMyToolWin->w() - 8;
  yy  = 32;

  xx2 = (pMyToolWin->w() - 8 - x1 + 1) / 2;
  xx3 = (pMyToolWin->w() - 8 - x1 + 2) / 3;
  xx4 = xx2 / 2;

  y1 = 20;
  y = y1;

  // Joints browser

  yy2 = 340;

  pJointBrowser = new Fl_Browser( x1, y1, xx, yy2, "Skeleton joints");

  pJointBrowser->align( FL_ALIGN_TOP_LEFT);     // align for label

  pJointBrowser->type(FL_HOLD_BROWSER);        // use for single selection
  pJointBrowser->callback( IqeB_JointBrowser_Callback, NULL);
  pJointBrowser->tooltip( "Left mouse button: select joint\n"
                          "Right mouse button: popup menu");

  y += yy2;

  // Name

  y += 4;

  pInputName = new Fl_Input( x1, y + 12, xx, yy - 12, "Joint name");
  pInputName->align( FL_ALIGN_TOP_LEFT);     // align for label
  pInputName->labelsize( 10);
  pInputName->tooltip( "Name of joint");
  pInputName->callback( IqeB_GUI_Input_SetValue_Callback);

  y += yy;

  // joint position

  pFloat_InputPX = new Fl_Float_Input( x1 + xx3 * 0, y + 12, xx3, yy - 12, "Joint position");
  pFloat_InputPX->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputPX->labelsize( 10);
  pFloat_InputPX->tooltip( "Joint position X");
  pFloat_InputPX->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pFloat_InputPY = new Fl_Float_Input( x1 + xx3 * 1, y + 12, xx3, yy - 12);
  pFloat_InputPY->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputPY->labelsize( 10);
  pFloat_InputPY->tooltip( "Joint position Y");
  pFloat_InputPY->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pFloat_InputPZ = new Fl_Float_Input( x1 + xx3 * 2, y + 12, xx3, yy - 12);
  pFloat_InputPZ->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputPZ->labelsize( 10);
  pFloat_InputPZ->tooltip( "Joint position Z");
  pFloat_InputPZ->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  y += yy;

  // joint angles

  pFloat_InputA0 = new Fl_Float_Input( x1 + xx3 * 0, y + 12, xx3, yy - 12, "Joint angles (degree)");
  pFloat_InputA0->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputA0->labelsize( 10);
  pFloat_InputA0->tooltip( "Joint angle");
  pFloat_InputA0->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pFloat_InputA1 = new Fl_Float_Input( x1 + xx3 * 1, y + 12, xx3, yy - 12);
  pFloat_InputA1->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputA1->labelsize( 10);
  pFloat_InputA1->tooltip( "Joint angle");
  pFloat_InputA1->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pFloat_InputA2 = new Fl_Float_Input( x1 + xx3 * 2, y + 12, xx3, yy - 12);
  pFloat_InputA2->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputA2->labelsize( 10);
  pFloat_InputA2->tooltip( "Joint angle");
  pFloat_InputA2->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  y += yy;

  // joint size

  pFloat_InputSX = new Fl_Float_Input( x1 + xx3 * 0, y + 12, xx3, yy - 12, "Joint sizes");
  pFloat_InputSX->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputSX->labelsize( 10);
  pFloat_InputSX->tooltip( "Joint size X");
  pFloat_InputSX->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pFloat_InputSY = new Fl_Float_Input( x1 + xx3 * 1, y + 12, xx3, yy - 12);
  pFloat_InputSY->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputSY->labelsize( 10);
  pFloat_InputSY->tooltip( "Joint size Y");
  pFloat_InputSY->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pFloat_InputSZ = new Fl_Float_Input( x1 + xx3 * 2, y + 12, xx3, yy - 12);
  pFloat_InputSZ->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputSZ->labelsize( 10);
  pFloat_InputSZ->tooltip( "Joint size Z");
  pFloat_InputSZ->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  y += yy;

  // enable checkbox

  y += 4;
  yy  = 24;

  pTemp_Check_Button = new Fl_Check_Button( x1, y, xx, yy, "Sym. joints");
  pTemp_Check_Button->tooltip( "If moving a symmetric joint with the mouse\n"
                               "the symmtric joint gets a X-axis mirrored\n"
                               "copy of the position info.");
  pTemp_Check_Button->value( IqeB_ToolsSkelEdit_MoveSymmetricJoints);
  pTemp_Check_Button->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_ToolsSkelEdit_MoveSymmetricJoints);

  // Do skin binding

  pGUI_ButBindDoN = new Fl_Button( x1 + xx2, y, xx4, yy, "Bind");
  pGUI_ButBindDoN->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_BIND_NO_SMOOTH));
  pGUI_ButBindDoN->tooltip( "Do skin binding, no smooth.\n"
                            "--> A shortcut to the 'Skin binding' BIND button.\n"
                            "Does the skin binding as specified inside\n"
                            "the 'Skin binding' dialog (see there).\n"
                            "A 'smooth %' of zero is used to speed up\n"
                            "the 'move joint' and test cycle time.");

  pGUI_ButBindDoS = new Fl_Button( x1 + xx2 + xx4, y, xx4, yy, "Bind S");
  pGUI_ButBindDoS->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_BIND_WITH_SMOOTH));
  pGUI_ButBindDoS->tooltip( "Do skin binding, with smooth."
                            "--> A shortcut to the 'Skin binding' BIND button.\n"
                            "Does the skin binding as specified inside\n"
                            "the 'Skin binding' dialog (see there).");

  y += yy;

  // finish up

  pMyToolWin->size( pMyToolWin->w(), y + 4);  // final size of window

  pMyToolWin->end();
  pMyToolWin->set_non_modal();
  pMyToolWin->callback( close_cb, &pMyToolWin);

  PoseLastDrawnPoseNr  = -1;

  IqeB_GUI_ToolsJointsUpdate( false); // Update the GUI

  pMyToolWin->show();
}

/********************************** End Of File **********************************/
