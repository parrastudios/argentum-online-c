/**
 * Include files
 */
var fs		= require('fs');          ///< file system module
var zmq		= require('zmq');         ///< zmq network module
var q		= require('q');           ///< q promise module
var debug	= require('./debug.js');  ///< debug module


/**
 *  Broker
 *
 * @author Vicente Ferrer
 * @lastmodify 120214 by Vicente
 * @brief A module that runs deployer & autoupdater's broker middelware.
 */
var Broker = Broker || (function(frontend_port, backend_port) {
	/**
	 * Private
	 */
	var frontend_		= null;				///< Frontend socket listener
	var backend_		= null;				///< Backend socket listener
	var frontend_port_  = 0;				///< Remote port of clients
	var backend_port_   = 0;				///< Remote port of workers
	var client_list_    = new Array();		///< List of registered users
	var worker_list_    = new Array();		///< List of registered workers

	/**
	 * Private constants
	 */
	var default_remote_protocol_addr  = 'tcp://*';
	var default_remote_frontend_port  = 0xA0C + 0x0001;
	var default_remote_backend_port   = 0xA0C + 0x0002;

	/**
	 * Private methods
	 */
	 
	var handle_frontend_incoming_data = (function(data) {
		var d = q.defer();
		
		d.resolve('todo');
		
		return d.promise;
	});
	
	var handle_backend_incoming_data = (function(data) {
		var d = q.defer();
		
		d.resolve('todo');
		
		return d.promise;
	});
	
	var bind = (function() {
		// bind sockets to ports (sync)
		frontend_.bindSync(default_remote_protocol_addr + ':' + frontend_port_);
		backend_.bindSync(default_remote_protocol_addr + ':'+ backend_port_);

		// bind frontend recv callback
		frontend_.on('message', function() {
			handle_frontend_incoming_data(Array.apply(null, arguments)).then(function(result) {
			
			}, function(error) {
				// show error mesage
				debug.log(error);
				
				// close socket
				frontend_.close();
			});
		});

		// bind backend recv callback
		backend_.on('message' function() {
			handle_backend_incoming_data(Array.apply(null, arguments)).then(function(result) {
			
			}, function(error) {
				// show error mesage
				debug.log(error);
				
				// close socket
				backend_.close();
			});
		});
	});

///////////////



          frontend.on('message', function() {
            var d = q.defer();
            // separate message parts
            d.resolve(Array.apply(null, arguments));
            // frontend callback
            d.promise.then(function(args) {
          var worker = null;
          
          // debug incoming packet
          utils.debug_packet_data(args, 'from frontend');

          if (args.length > 1) { // client_id + text
          // if not exist register user
          if (!utils.check_if_exists(client_list, args[0])) {
            // push back client_id
            client_list.push(args[0]);
            utils.debug_message('New client registered:', args[0]);
            } else {
            utils.debug_message('Incoming existing client:', args[0]);
          }
          
          // find the less balanced worker, and send message to it
          var min_load = 10000;
          var min = -1;
          for (var i = 0; i < worker_list.length; ++i) {

            if (worker_list[i]['working'] == false && min_load > worker_list[i]['status']) {
              min = i;
              min_load = worker_list[i]['status'];
            }
          }
          worker = worker_list[min];

            // if exists a worker
            if (worker != null) {
            // insert delimiter and the worker id
            args = utils.insert_packet_data(args, worker['id'], args[1]);
            
            // debug packet
              utils.debug_packet_data(args, 'to backend');
              
              // pass array of strings/buffers to send multipart messages
              backend.send(args);
            } else {
              utils.debug_message('No worker available');
            }
          } else {
           utils.debug_message('Invalid message, you must specify client_id + text');
          }
        }, function(error)  {
          // show error message
          utils.debug_message(error);

          // close socket
          frontend.close();
        });  
      });


          

          backend.on('message', function() {
            // separate message parts
          var d = q.defer();
          d.resolve(Array.apply(null, arguments));

          // backend callback
          d.promise.then(function(args) {
          // debug incoming packet
          utils.debug_packet_data(args, 'from backend');

          // worker wants to register
          if (args.length == 3/*&& args[2] == 'register'*/) { // worker_id + text -> register
            console.log("Nada");
          } 
          // worker wants to register
          else if (args.length == 7 /*&& args[2] == 'register'*/) { // worker_id + text -> register
            var worker = null;
            
            // check if already is registered
            for (var i = 0; i < worker_list.length; ++i) {
              if (worker_list[i]['id'] == args[0]) {
                // already exits
                worker = worker_list[i];
              }
            }

            // check if worker exists
            if (worker == null) {
              // create a new worker
              worker = {'id' : args[0], 'working' : false, 'port_status' : args[4],'status':args[6], 'waiting': false};
              
              // append it to the list
              worker_list.push(worker);
              
              utils.debug_message('New worker registered', args[0]); 
            } else {
              utils.debug_message('Existing worker', args[0]); 
            }
          
          // worker wants to send a message to a client
          } else if (args.length == 5 /*&& args[4] == 'done'*/) { // worker_id + client_id + text
            //remove worker_id from args
            var worker_id = utils.retreive_packet_data(args);
            
              // debug packet
              utils.debug_packet_data(args, 'to frontend');
            
              // send content to the client
             frontend.send(args);
            
              utils.debug_message('Message sent from', worker_id, 'to', args[0]);   
            } else {
              utils.debug_message('Backend error, invalid number of args:', args.length);
            }
          }, function(error)  {
          // show error message
          utils.debug_message(error);

          // close socket
          backend.close();
        }); 
      }); 

function worker_timeout(id) {
		utils.debug_message('Worker timeout thrown:', id, worker_list[id]['waiting']);
	
		if (worker_list[id]['waiting'] == true) {
			utils.debug_message('Worker', id, 'lost');
			
			for (var it = id; it < worker_list.length - 1; ++it) {
				worker_list[it] = worker_list[it + 1];
			}
			
			worker_list.pop();
		}
}

setInterval( function() {
  for(var i in worker_list) {

      var d = q.defer();
      
      var r = zmq.socket( 'req' );

      r.connect( 'tcp://localhost:'+worker_list[ i ].port_status );

      r.worker_i = i;

      r.status_d = d;

	  // wait for response
	  worker_list[i]['waiting'] = true;
	  
	 // worker_list[i]['timeout_callback'] = setTimeout(function() {
	 //	  worker_timeout(i);
	 //	  utils.debug_message('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiddddddDD:', i);
	 // }, 4500); // avoid timeout overlapping

      r.send('Give your load');
      console.log("Sent load request to "+i);
      r.on('message',function(data) {
          this.status_d.resolve([this.worker_i,data]);
      })

      d.promise.then(function(args) {
        var i = args[0];
        var load_status = args[1];

        worker_list[i].status = load_status;
        worker_list[i]['waiting'] = false;
        console.log('Working '+worker_list[i].status);
      });
  }
  
  setTimeout(function() {
	  for (var i = 0; i < worker_list.length; ++i) {
		  worker_timeout(i);
	  }
  }, 4500); // avoid timeout overlapping
  
},5*1000);        
        
