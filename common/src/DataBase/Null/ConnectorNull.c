/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2016 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/Connector.h>
#include <DataBase/Null/General.h>
#include <Memory/General.h>

#include <string.h> // strcpy

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define CONNECTOR_DEFAULT_FLAGS		CLIENT_MULTI_STATEMENTS

////////////////////////////////////////////////////////////
/// Report an error in connector
////////////////////////////////////////////////////////////
bool DBConnectorCriticalError(struct DataBase * DB)
{
	MessageError("Connector Error", "NULL");

	return DATABASE_ERROR;
}

////////////////////////////////////////////////////////////
/// Create a connector
////////////////////////////////////////////////////////////
struct DataBase * DBConnectorCreate(char * Server, char * User, char * Password, char * Database, uint32 Port)
{
	struct DataBase * DB = (struct DataBase*)MemoryAllocate(sizeof(struct DataBase));

	if (DB)
	{
		strcpy(DB->Server, Server);
		strcpy(DB->User, User);
		strcpy(DB->Password, Password);
		strcpy(DB->Database, Database);
		DB->Port = Port;

		return DB;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Connect from MySQL database
////////////////////////////////////////////////////////////
bool DBConnectorInitialize(struct DataBase * DB)
{
	if (DB)
	{
		return DATABASE_SUCCESS;
	}

	return DATABASE_ERROR;
}

////////////////////////////////////////////////////////////
/// Reconnect from MySQL database
////////////////////////////////////////////////////////////
bool DBConnectorReset(struct DataBase * DB)
{
	if (DB)
	{
		return DATABASE_SUCCESS;
	}

	return DATABASE_ERROR;
}

////////////////////////////////////////////////////////////
/// Disconnect from MySQL database
////////////////////////////////////////////////////////////
bool DBConnectorDestroy(struct DataBase * DB)
{
	if (DB)
	{
		return DATABASE_SUCCESS;
	}

	MessageError("Connector Error", "Error when server tried to destroy the connector - (%s / %s / %s / %d)",
				 DB->Server, DB->User, DB->Database, DB->Port);

	return DATABASE_ERROR;
}

////////////////////////////////////////////////////////////
/// Get the MySQL version
////////////////////////////////////////////////////////////
const char * DBConnectorGetVersion(struct DataBase * DB)
{
	if (DB)
	{
		return "NULL";
	}
	else
	{
		return "unknown";
	}
}