/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_MODEL_FACTORY_H
#define WORLD_MODEL_FACTORY_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/EntityFactory.h>

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_BASE)
#	include <World/ModelBase.h>
#endif

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_TRANSFORM)
#	include <World/ModelTransform.h>
#endif

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_COLLISION)
#	include <World/ModelCollision.h>
#endif

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_PHYSICS)
#	include <World/ModelPhysics.h>
#endif

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_ANIMATION)
#   include <World/ModelAnimation.h>
#endif

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_RENDER)
#	include <World/ModelRender.h>
#endif

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize component pool of model factory
////////////////////////////////////////////////////////////
bool ModelFactoryInitialize();

////////////////////////////////////////////////////////////
/// Get model factory instance
////////////////////////////////////////////////////////////
struct EntityFactoryType * ModelFactoryGet();

////////////////////////////////////////////////////////////
/// Destroy component pool of model factory
////////////////////////////////////////////////////////////
void ModelFactoryDestroy();

#endif // WORLD_MODEL_FACTORY_H
