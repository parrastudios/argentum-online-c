/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Point.h>
#include <Graphics/Device.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define POINT_RENDER_RADIUS_LIMIT   		25.0f
#define POINT_RENDER_RADIUS_DEFAULT         1.0f

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create render component in a entity point
////////////////////////////////////////////////////////////
struct EntityType * PointRenderCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a point render component
////////////////////////////////////////////////////////////
void PointRenderDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Initialize point render properties
////////////////////////////////////////////////////////////
void PointRenderColor(struct EntityType * Entity, struct Color4ub * Color);

////////////////////////////////////////////////////////////
/// Render a point
////////////////////////////////////////////////////////////
void PointRender(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the point render implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentRenderType * PointComponentRenderAttr()
{
	static const struct PointComponentRenderType PointComponentRenderImplAttr = {
		TypeInit(Color, &PointRenderColor)
	};

	static const struct ComponentRenderType PointComponentRenderAttr = {
        TypeInit(Draw, &PointRender),
        TypeInit(TypeImpl, (const void *)(&PointComponentRenderImplAttr))
	};

	return (const void *)&PointComponentRenderAttr;
}

////////////////////////////////////////////////////////////
/// Return the point render implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * PointComponentRenderImpl()
{
	static const struct ComponentImplType PointComponentRenderTypeImpl = {
		ExprInit(Base, TypeInit(Create, &PointRenderCreate), TypeInit(Destroy, &PointRenderDestroy)),
		TypeInit(Type, COMPONENT_TYPE_RENDER),
		UnionInit(Attributes, Render, &PointComponentRenderAttr)
	};

	return &PointComponentRenderTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create render component in a entity point
////////////////////////////////////////////////////////////
struct EntityType * PointRenderCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain render component data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct PointComponentRenderData * RenderData = ComponentDataDerived(Render, struct PointComponentRenderData);

		// Set default render color
		Color4ubSet(&RenderData->Color, POINT_RENDER_COLOR_DEFAULT);

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set point render color
////////////////////////////////////////////////////////////
void PointRenderColor(struct EntityType * Entity, struct Color4ub * Color)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct PointComponentRenderData * RenderData = ComponentDataDerived(Render, struct PointComponentRenderData);

		if (Color != NULL)
		{
			Color4ubCopy(&RenderData->Color, Color);
		}
		else
		{
			Color4ubSet(&RenderData->Color, POINT_RENDER_COLOR_DEFAULT);
		}
	}
}

////////////////////////////////////////////////////////////
/// Destroy a point render component
////////////////////////////////////////////////////////////
void PointRenderDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct PointComponentRenderData * RenderData = ComponentDataDerived(Render, struct PointComponentRenderData);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Render a point
////////////////////////////////////////////////////////////
void PointRender(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct PointComponentBaseData * BaseData = ComponentDataDerived(Base, struct PointComponentBaseData);

		// Get render component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct PointComponentRenderData * RenderData = ComponentDataDerived(Render, struct PointComponentRenderData);

		// Get transform component and obtain data reference
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

		// Get collision sphere component and obtain data reference
		struct ComponentType * Collision = EntityComponentCollisionSphere(Entity);
		struct ComponentCollisionSphereData * CollisionData = ComponentDataCollisionSphere(Collision);

		// Render color declaration
		GLubyte Color[4] = { RenderData->Color.r, RenderData->Color.g, RenderData->Color.b, RenderData->Color.a };

        if (CollisionData->Sphere.Radius <= 0.0f || CollisionData->Sphere.Radius > POINT_RENDER_RADIUS_LIMIT)
        {
            // Set the radius
            glPointSize(POINT_RENDER_RADIUS_DEFAULT);
        }
        else
        {
            // Set the radius
            glPointSize(CollisionData->Sphere.Radius);
        }

		// Render point with specified color
		glBegin(GL_POINTS);
			glColor4ubv((GLubyte*)Color);
			glVertex3fv((GLfloat*)&TransformData->Position.v[0]);
		glEnd();

		// Render point name text
		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the model render data size
////////////////////////////////////////////////////////////
uinteger PointComponentRenderSize()
{
    return sizeof(struct PointComponentRenderData);
}
