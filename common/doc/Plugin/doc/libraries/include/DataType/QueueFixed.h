/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATATYPE_QUEUE_FIXED_H
#define DATATYPE_QUEUE_FIXED_H

////////////////////////////////////////////////////////////
// Doc
//
// QueueFixedDefine(uint32, 16);
//
// QueueFixedDefine(uint32, 64) QueueFixedCreate(QueueA),
//								QueueFixedCreate(QueueB);
//
// QueueFixedDeclare(uint32, 16) QueueFixedCreate(QueueC),
//								 QueueFixedCreate(QueueD),
//								 QueueE;
//
// QueueFixedClear(QueueE);
//
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Types.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct QueueFixedBase
{
	uint32	Current;
	uint32	Count;
	uint8	Buffer[1];
};

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define QueueFixedDefine(Type, Size) \
	struct QueueFixed##Type##Size \
	{ \
		uint32	Current; \
		uint32	Count; \
		Type	Buffer[Size]; \
	}

#define QueueFixedDeclare(Type, Size) struct QueueFixed##Type##Size

#define QueueFixedCreate(Queue) Queue = { 0, 0, { 0 }}

#define QueueFixedClear(Queue) QueueFixedClearType((struct QueueFixedBase*)&Queue)

#define QueueFixedGetHead(Queue, Type) QueueFixedGetHeadType((struct QueueFixedBase*)&Queue, sizeof(Type))

#define QueueFixedGetTail(Queue, Type, Size) QueueFixedGetTailType((struct QueueFixedBase*)&Queue, Size, sizeof(Type))

#define QueueFixedEmpty(Queue) QueueFixedEmptyType((struct QueueFixedBase*)&Queue)

#define QueueFixedFull(Queue, Size) QueueFixedFullType((struct QueueFixedBase*)&Queue, Size)

#define QueueFixedPush(Queue, Element, Type, Size) QueueFixedPushType((struct QueueFixedBase*)&Queue, (void*)Element, Size, sizeof(Type))

#define QueueFixedPop(Queue, Size) QueueFixedPopType((struct QueueFixedBase*)&Queue, Size)

#define QueueFixedIteratorNext(Queue, Iterator, Type, Size) \
	Iterator = QueueFixedIteratorNextType((struct QueueFixedBase*)&Queue, Iterator, Size, sizeof(Type))

#define QueueFixedIteratorPrev(Queue, Iterator, Type, Size) \
	Iterator = QueueFixedIteratorPrevType((struct QueueFixedBase*)&Queue, Iterator, Size, sizeof(Type))

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

void QueueFixedClearType(struct QueueFixedBase * Queue);

void * QueueFixedGetHeadType(struct QueueFixedBase * Queue, uint32 TypeSize);

void * QueueFixedGetTailType(struct QueueFixedBase * Queue, uint32 Size, uint32 TypeSize);

bool QueueFixedEmptyType(struct QueueFixedBase * Queue);

bool QueueFixedFullType(struct QueueFixedBase * Queue, uint32 Size);

void QueueFixedPushType(struct QueueFixedBase * Queue, void * Element, uint32 Size, uint32 TypeSize);

void QueueFixedPopType(struct QueueFixedBase * Queue, uint32 Size);

void * QueueFixedIteratorNextType(struct QueueFixedBase * Queue, void * Iterator, uint32 Size, uint32 TypeSize);

void * QueueFixedIteratorPrevType(struct QueueFixedBase * Queue, void * Iterator, uint32 Size, uint32 TypeSize);

#endif // DATATYPE_QUEUE_FIXED_H
