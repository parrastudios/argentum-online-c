/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/TerrainSound.h>

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a sound component of a terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainSoundCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a terrain sound component
////////////////////////////////////////////////////////////
void TerrainSoundDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the terrain sound implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * TerrainComponentSoundImpl()
{
	static const struct ComponentImplType TerrainComponentSoundTypeImpl = {
		ExprInit(Base, TypeInit(Create, &TerrainSoundCreate), TypeInit(Destroy, &TerrainSoundDestroy)),
		TypeInit(Type, COMPONENT_TYPE_SOUND),
		UnionInit(Attributes, Sound, &EntityComponentSoundAttr)
	};

	return &TerrainComponentSoundTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a sound component of a terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainSoundCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Create sound component data reference
		struct ComponentType * Sound = EntityComponentSound(Entity);
		struct ComponentSoundType * SoundType = ComponentTypeSound(Sound);

		// Set default terrain background theme
		SoundType->Stream(Entity, TERRAIN_SOUND_THEME_DEFAULT);

		// Set sound loop
		SoundType->Loop(Entity, true);

		// Play theme
		SoundType->Play(Entity);

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a terrain sound component
////////////////////////////////////////////////////////////
void TerrainSoundDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get sound component
		struct ComponentType * Sound = EntityComponentSound(Entity);
		struct ComponentSoundType * SoundType = ComponentTypeSound(Sound);

		// ..

	}
}

////////////////////////////////////////////////////////////
/// Return the terrain sound data size
////////////////////////////////////////////////////////////
uinteger TerrainComponentSoundSize()
{
	return COMPONENT_DATA_LENGTH_DEFAULT;
}
