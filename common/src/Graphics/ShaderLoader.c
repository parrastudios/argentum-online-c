/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/ShaderLoader.h>
#include <Memory/General.h>
#include <System/IOHelper.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////
bool ShaderLoadFromFile(struct ShaderType * Shader, const char * FilePath)
{
	FILE * File;
	uinteger Size;
	char * Buffer;

	if (!fopen(&File, FilePath, "rb"))
	{
		// Couldn't open the file
		return false;
	}

	fseek(File, 0, SEEK_END);

	Size = ftell(File);

	fseek(File, 0, SEEK_SET);

	Buffer = (char*)MemoryAllocate(sizeof(char) * Size);

	if (Buffer)
	{
		if (!fread(Buffer, sizeof(char), Size, File))
		{
			// Error when reading from file
			MemoryDeallocate(Buffer);
			return false;
		}

		fclose(File);

		// Set shader source
		ShaderSetSource(Shader, Buffer, Size);

		MemoryDeallocate(Buffer);

		return true;
	}

	return false;
}

bool ShaderLoadFromMemory(struct ShaderType * Shader, uint8 * Data, uinteger Size)
{
	// todo
	return false;
}
