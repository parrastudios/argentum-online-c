/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/EntityOcclusion.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Calculate occlusion of an entity
////////////////////////////////////////////////////////////
void EntityOcclusionCalculate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Build the occlusion when data is precalculated
////////////////////////////////////////////////////////////
void EntityOcclusionBuild(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create an occlusion component
////////////////////////////////////////////////////////////
struct EntityType * EntityOcclusionCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Occlusion = EntityComponentOcclusion(Entity);
		struct ComponentOcclusionData * OcclusionData = ComponentDataOcclusion(Occlusion);

		// ..

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a occlusion component
////////////////////////////////////////////////////////////
void EntityOcclusionDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Occlusion = EntityComponentOcclusion(Entity);
		struct ComponentOcclusionData * OcclusionData = ComponentDataOcclusion(Occlusion);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the entity occlusion implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentOcclusionType * EntityComponentOcclusionAttr()
{
	static const struct ComponentOcclusionType EntityComponentOcclusionAttr = {
		TypeInit(Calculate, &EntityOcclusionCalculate),
		TypeInit(Build, &EntityOcclusionBuild)
	};

	return (const struct ComponentOcclusionType *)&EntityComponentOcclusionAttr;
}

////////////////////////////////////////////////////////////
/// Return the entity occlusion implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentOcclusionImpl()
{
	static const struct ComponentImplType EntityComponentOcclusionTypeImpl = {
		ExprInit(Base, TypeInit(Create, &EntityOcclusionCreate), TypeInit(Destroy, &EntityOcclusionDestroy)),
		TypeInit(Type, COMPONENT_TYPE_OCCLUSION),
		UnionInit(Attributes, Occlusion, &EntityComponentOcclusionAttr)
	};

	return &EntityComponentOcclusionTypeImpl;
}

////////////////////////////////////////////////////////////
/// Updates occlusion component data
////////////////////////////////////////////////////////////
void EntityOcclusionAnimate(struct EntityType * Entity, float ElapsedTime)
{
	if (Entity)
	{
		struct ComponentType * Occlusion = EntityComponentOcclusion(Entity);
		struct ComponentOcclusionData * OcclusionData = ComponentDataOcclusion(Occlusion);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Calculate occlusion of an entity
////////////////////////////////////////////////////////////
void EntityOcclusionCalculate(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Occlusion = EntityComponentOcclusion(Entity);
		struct ComponentOcclusionData * OcclusionData = ComponentDataOcclusion(Occlusion);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Build the occlusion when data is precalculated
////////////////////////////////////////////////////////////
void EntityOcclusionBuild(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Occlusion = EntityComponentOcclusion(Entity);
		struct ComponentOcclusionData * OcclusionData = ComponentDataOcclusion(Occlusion);

		// ..
	}
}
