/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/Default.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct DataBase * DataBaseDefault = NULL;

/////////////////////////////////////////////////////////////
/// Initialize a default database
/////////////////////////////////////////////////////////////
bool DataBaseInitialize()
{
	// Create it
	DataBaseDefault = DBConnectorCreate(DATABASE_DEFAULT_SERVER,
										DATABASE_DEFAULT_USER,
										DATABASE_DEFAULT_PASSWORD,
										DATABASE_DEFAULT_NAME,
										DATABASE_DEFAULT_PORT);

	// Initialize
	return (DataBaseDefault && DBConnectorInitialize(DataBaseDefault));
}

/////////////////////////////////////////////////////////////
/// Reconnect a default database
/////////////////////////////////////////////////////////////
bool DataBaseReconnect()
{
	if (!DataBaseDefault)
	{
		return DataBaseInitialize();
	}

	// Reset connector
	return DBConnectorReset(DataBaseDefault);
}

/////////////////////////////////////////////////////////////
/// Destroy a default database
/////////////////////////////////////////////////////////////
bool DataBaseDestroy()
{
	return DBConnectorDestroy(DataBaseDefault);
}

/////////////////////////////////////////////////////////////
/// Get default database
/////////////////////////////////////////////////////////////
struct DataBase * DataBaseGetDefault()
{
	return DataBaseDefault;
}
