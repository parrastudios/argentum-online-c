#version 330

const int MAX_POINT_LIGHTS = 2;
const int MAX_SPOT_LIGHTS = 2;

in vec2 TexCoord0;
in vec3 Normal0;
in vec3 WorldPos0;

struct VSOutput
{
    vec2 TexCoord;
    vec3 Normal;
    vec3 WorldPos;
};

struct BaseLight
{
    vec3 Color;
    float AmbientIntensity;
    float DiffuseIntensity;
};

struct DirectionalLight
{
    BaseLight Base;
    vec3 Direction;
};

struct Attenuation
{
    float Constant;
    float Linear;
    float Exp;
};

struct PointLight
{
    BaseLight Base;
    vec3 Position;
    Attenuation Atten;
};

struct SpotLight
{
    PointLight Base;
    vec3 Direction;
    float Cutoff;
};

uniform int PointLightsSize;
uniform int SpotLightsSize;
uniform DirectionalLight DirectLight;
uniform PointLight PointLights[MAX_POINT_LIGHTS];
uniform SpotLight SpotLights[MAX_SPOT_LIGHTS];
uniform sampler2D ColorMap;
uniform vec3 EyeWorldPos;
uniform float MatSpecularIntensity;
uniform float SpecularPower;


vec4 CalcLightInternal(BaseLight Light, vec3 LightDirection, VSOutput In)
{
    vec4 AmbientColor = vec4(Light.Color * Light.AmbientIntensity, 1.0);
    float DiffuseFactor = dot(In.Normal, -LightDirection);

    vec4 DiffuseColor  = vec4(0, 0, 0, 0);
    vec4 SpecularColor = vec4(0, 0, 0, 0);

    if (DiffuseFactor > 0.0)
    {
        DiffuseColor = vec4(Light.Color * Light.DiffuseIntensity * DiffuseFactor, 1.0);

        vec3 VertexToEye = normalize(EyeWorldPos - In.WorldPos);
        vec3 LightReflect = normalize(reflect(LightDirection, In.Normal));
        float SpecularFactor = dot(VertexToEye, LightReflect);

        if (SpecularFactor > 0.0)
        {
            SpecularFactor = pow(SpecularFactor, SpecularPower);
            SpecularColor = vec4(Light.Color * MatSpecularIntensity * SpecularFactor, 1.0);
        }
    }

    return (AmbientColor + DiffuseColor + SpecularColor);
}

vec4 CalcDirectionalLight(VSOutput In)
{
    return CalcLightInternal(DirectLight.Base, DirectLight.Direction, In);
}

vec4 CalcPointLight(PointLight Light, VSOutput In)
{
    vec3 LightDirection = In.WorldPos - Light.Position;
    float Distance = length(LightDirection);
    LightDirection = normalize(LightDirection);

    vec4 Color = CalcLightInternal(Light.Base, LightDirection, In);

    float Attenuation =  Light.Atten.Constant +
                         Light.Atten.Linear * Distance +
                         Light.Atten.Exp * Distance * Distance;

    return Color / Attenuation;
}

vec4 CalcSpotLight(SpotLight Light, VSOutput In)
{
    vec3 LightToPixel = normalize(In.WorldPos - Light.Base.Position);
    float SpotFactor = dot(LightToPixel, Light.Direction);

    if (SpotFactor > Light.Cutoff)
    {
        vec4 Color = CalcPointLight(Light.Base, In);
        return Color * (1.0 - (1.0 - SpotFactor) * 1.0 / (1.0 - Light.Cutoff));
    }
    else
    {
        return vec4(0, 0, 0, 0);
    }
}

out vec4 FragColor;

void main()
{
    VSOutput In;
    In.TexCoord = TexCoord0;
    In.Normal   = normalize(Normal0);
    In.WorldPos = WorldPos0;

    vec4 TotalLight = CalcDirectionalLight(In);

    for (int i = 0; i < PointLightsSize; ++i)
    {
        TotalLight += CalcPointLight(PointLights[i], In);
    }

    for (int i = 0; i < SpotLightsSize; ++i)
    {
        TotalLight += CalcSpotLight(SpotLights[i], In);
    }

    FragColor = texture(ColorMap, In.TexCoord.xy) * TotalLight;
}
