#ifndef ASSIMP_REVISION_H_INC
#define ASSIMP_REVISION_H_INC

#ifdef use_again
#define GitVersion 0x@GIT_COMMIT_HASH@
#define GitBranch "@GIT_BRANCH@"
#else

// 20.12.2014 RR: is usable

#define GitVersion 0x0311
#define GitBranch "Iqe_Browswer use V3.1.1"

#endif

#endif // ASSIMP_REVISION_H_INC
