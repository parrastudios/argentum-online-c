/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Plugin/General.h>

#include <System/IOHelper.h> // todo: refactor into IO module

////////////////////////////////////////////////////////////
// Application entry point
////////////////////////////////////////////////////////////
int main(int argc, char * argv[])
{
	// Get the plugin manager instance
	PluginManagerType * PluginManager = PluginManagerGetInstance();

	// Create a plugin manager
	if (PluginManager->Create())
	{
		// Obtain plugin loader object
		PluginLoaderType * PluginLoader = PluginManager->Loader();

		// Load a plugin
		PluginType * Plugin = PluginManager->Attach("HelloWorldPlugin");

		// If plugin has been loaded
		if (Plugin)
		{
			// Execute fnHelloWorld from plugin, without arguments
			struct MetadataReturn * HelloWorldReturn = Plugin->Execute(Plugin, "fnHelloWorld", NULL, 0);

			// Print return value
			printf("fnHelloWorld return value: %d\n", MetadataTypeCast(UINT32, HelloWorldReturn->Value(HelloWorldReturn)));

			// Unload plugin
			PluginManager->Detach(Plugin);
		}

		// Destroy plugin manager
		PluginManager->Destroy();
	}

	return 0;
}
