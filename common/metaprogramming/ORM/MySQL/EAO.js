var getForeignKeys = require('./ForeignKeys').getForeignKeys;
var getTables = require('./Tables').getTables;
var CodeBuilder = require('./CodeBuilderMySQL');
var connection = require('./Connection');
var fs = require('fs');

var structs = '';

getTables(function (tables) {

  getForeignKeys(function (fks) {

    for (var tableName in tables) {
      
      // Columns inside the table
      var tablecolumns = tables[tableName];

      var structParams = [];
         
      // Iterate through each column
      tablecolumns.forEach(function (element) {
        structParams.push(element);
      });

      structs += CodeBuilder.structBuilder(tableName, structParams, fks);
    }

    fs.writeFile("tmp.c", structs, function (err) {
      if (err) {
        throw err;
      }
      console.log("The file was saved!");
    });


  });


  connection.end();

});




