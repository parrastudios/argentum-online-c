/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATABASE_CONNECTOR_H
#define DATABASE_CONNECTOR_H

////////////////////////////////////////////////////////////
///
/// Module for managing data stored in the database
///
/// ======================================
/// MySQL Server Configuration (default)
/// Server:    localhost
/// UserName:  root
/// Password:  root
/// DataBase:  aocdb
/// Port:      3306
/// ======================================
/// MongoDB Server Configuration (default)
/// Server:    
/// UserName:  
/// Password:  
/// DataBase:  
/// Port:      
/// ======================================
///
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>			///< Type basic
#include <System/Error.h>	///< Error handler

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define DATABASE_SUCCESS	true
#define DATABASE_ERROR		false

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct DataBase
{
	void *	Connection;
	char	Server[0xFF];
	char	User[0xFF];
	char	Password[0xFF];
	char	Database[0xFF];
	uint32	Port;
};

////////////////////////////////////////////////////////////
/// Create a connector
////////////////////////////////////////////////////////////
struct DataBase * DBConnectorCreate(char * Server, char * User, char * Password, char * Database, uint32 Port);

////////////////////////////////////////////////////////////
/// Connect to database
////////////////////////////////////////////////////////////
bool DBConnectorInitialize(struct DataBase * DB);

////////////////////////////////////////////////////////////
/// Reconnect to database
////////////////////////////////////////////////////////////
bool DBConnectorReset(struct DataBase * DB);

////////////////////////////////////////////////////////////
/// Disconnect from database
////////////////////////////////////////////////////////////
bool DBConnectorDestroy(struct DataBase * DB);

////////////////////////////////////////////////////////////
/// Get connector technology version
////////////////////////////////////////////////////////////
const char * DBConnectorGetVersion(struct DataBase * DB);

#endif // DATABASE_CONNECTOR_H
