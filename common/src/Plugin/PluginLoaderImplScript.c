/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Plugin/PluginLoaderImplScript.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
struct PluginLoaderImplType PluginLoaderImplScript = {
	TypeInit(Initialize, &PluginLoaderImplScriptInitialize),
	TypeInit(Destroy, &PluginLoaderImplScriptDestroy),
	TypeInit(Load, &PluginLoaderImplScriptLoad),
	TypeInit(LoadPath, &PluginLoaderImplScriptLoadPath),
	TypeInit(Register, &PluginLoaderImplScriptRegister),
	TypeInit(Unload, &PluginLoaderImplScriptUnload),
	TypeInit(Clear, &PluginLoaderImplScriptClear),
	TypeInit(Initialized, false),
	TypeInit(ResourceList, NULL)
};

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////

// ...

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get default plugin loader script implementation
////////////////////////////////////////////////////////////
struct PluginLoaderImplType * PluginLoaderImplScriptGet()
{
	return &PluginLoaderImplScript;
}

////////////////////////////////////////////////////////////
/// Initialize script plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderImplScriptInitialize(struct PluginLoaderImplType * PluginLoaderImpl)
{

	return false;
}

////////////////////////////////////////////////////////////
/// Destroy script plugin loader
////////////////////////////////////////////////////////////
void PluginLoaderImplScriptDestroy(struct PluginLoaderImplType * PluginLoaderImpl)
{


}

////////////////////////////////////////////////////////////
/// Load a plugin from script plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderImplScriptLoad(struct PluginLoaderImplType * PluginLoaderImpl, PluginId Id, PluginLoaderImplResourceType * Impl)
{

	return false;
}

////////////////////////////////////////////////////////////
/// Load a whole path of plugins from script plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderImplScriptLoadPath(struct PluginLoaderImplType * PluginLoaderImpl, const char * Path, List LoadedIdList)
{

	return false;
}

////////////////////////////////////////////////////////////
/// Register script from plugin loader to plugin handle
////////////////////////////////////////////////////////////
bool PluginLoaderImplScriptRegister(struct PluginLoaderImplType * PluginLoaderImpl, PluginLoaderImplResourceType Impl, PluginType * Plugin)
{

	return false;
}

////////////////////////////////////////////////////////////
/// Unload a plugin from script plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderImplScriptUnload(struct PluginLoaderImplType * PluginLoaderImpl, PluginLoaderImplResourceType Impl)
{

	return false;
}

////////////////////////////////////////////////////////////
/// Clear all plugins from script plugin loader
////////////////////////////////////////////////////////////
void PluginLoaderImplScriptClear(struct PluginLoaderImplType * PluginLoaderImpl)
{

}
