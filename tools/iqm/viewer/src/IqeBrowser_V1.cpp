/*
***********************************************************************************
 * IqeBrowser.cpp

 This file is based on the 'iqeview.c' from Tor Andersson.

 2014-03-21 RR: * Started to rework the iqeview.c program from Tor Anderssons
                  '3d Asset Tools' als IqeBrowser.

 2014-04-02 RR: * Merge the input files to a comma separated list and
                  handle all input files as one argument to IQM_Compiler().
                  ==> This must be an error of the iqm compiler.
                      According to the description, each inputfiles should be
                      one argument.
 2014-04-11 RR: * Reorganized source files to have all sourced togehther ...
                * Copied 'stb_image.c' from Asstools directory (3d Asset Tools
                  from Tor Andersson) to source directory.
                  Changed  #include "..\stb_image.c"
                  to       #include ".\stb_image.c"

 2014-04-30 RR: * Use framerate from model for display.
                  Also output framerate on screen.
                * After 'add' button press.
                  * If the added model is not the first model in the model container and
                    the added model has an animation,
                    copy skeleton and/or mesh of the first model to the added model
                    if the joint and poses count match.
                    ==> Pure animation files are displayed with skeleton and/or mesh.
                  * display the added model.
                * texture files can be of type .png, .jpg, .tga (in this order).
                * Display size of model on the screen.
                * Scale factor on the GUI too resize the model mesh.
                * Display joint names.
                  This is toggled with a 'j' keyboad input.
2014-05-01 RR: * Rearanged arguments for IQM_Compiler() call.
                 First models (from the model container) with meshes
                 goes to the first input file list.
                 All other files should have no animatins and feeded
                 as separated input file argument to IQM_Compiler().
2014-05-02 RR: * Max number of model in model container changed
                 from 16 to 64.
2014-10-27 RR: * IQE_loadmodel(), added support for 'pa' pose format.
               * Support for comment starting with '#' character for .iqe files.

01.11.2014 RR: * Moved structure defines to IqeBrowser.h so the model can be
                 use for the pinocchio api interface.

*/

#include "IqeBrowser.h"
#include "IqmCompiler.h"
#include "PolygonReduction/IqeBrowserInterface.h"
#include "iqm.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/freeglut.h>
#endif

#define GLUI_NO_LIB_PRAGMA
#include <GL/glui.h>

#ifndef GL_GENERATE_MIPMAP
#define GL_GENERATE_MIPMAP 0x8191
#endif

#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE 0x809D
#endif

/************************************************************************************
 * Some defines
 */

#define WIN_PROG_VERSION  "IqeBrowser / V1.10 2014-05-02"
#define WIN_DEFAULT_TITLE  "IQE model browser and compiler"

/************************************************************************************
 * Models to draw on GUI
 */

// current model loaded from file selection

struct IQE_model *pFileSelModel;   // file selected model
char   FileSelName[ 1024];         // file selected model, file name
char   FileSelFullName[ 1024];     // file selected model, path and file name

// model container

#define MODEL_CONTAINER_DISP 16    // model container, max number of models to display
#define MODEL_CONTAINER_MAX  64    // max number of models in model container

typedef struct {
  char   ModelName[ 1024];       // file selected model, file name
  char   ModelFullName[ 1024];   // file selected model, path and file name
  int    part_count_before_add;  // number of meshes before any add of mesh or skeleton
  IQE_model *pModel;             // parsed model data
} T_ModelContainer;

T_ModelContainer ModelContainer[ MODEL_CONTAINER_MAX]; // the model container
int    ModelContainer_n = 0;                           // number of models in container

// draw this model
struct IQE_model *pDrawModel = NULL;   // drawing this model

static int curframe = 0;
static struct anim *curanim = NULL;
static float curtime = 0;
static int lasttime = 0;

// or draw this message

static float DrawMessageColR, DrawMessageColG, DrawMessageColB;
static char DrawMessageText1[ 1024];
static char DrawMessageText2[ 1024];
static char DrawMessageText3[ 1024];

/*
============================================================================

					BYTE ORDER FUNCTIONS

============================================================================
*/

static int	bigendien;

// can't just use function pointers, or dll linkage can
// mess up when qcommon is included in multiple places
static short	(*_BigShort) (short l);
static short	(*_LittleShort) (short l);
static int		(*_BigLong) (int l);
static int		(*_LittleLong) (int l);
static float	(*_BigFloat) (float l);
static float	(*_LittleFloat) (float l);

//x/short	BigShort(short l){return _BigShort(l);}
short	LittleShort(short l) {return _LittleShort(l);}
//x/int		BigLong (int l) {return _BigLong(l);}
int		LittleLong (int l) {return _LittleLong(l);}
//x/float	BigFloat (float l) {return _BigFloat(l);}
float	LittleFloat (float l) {return _LittleFloat(l);}

static short   ShortSwap (short l)
{
	unsigned char    b1,b2;

	b1 = l&255;
	b2 = (l>>8)&255;

	return (b1<<8) + b2;
}

static short	ShortNoSwap (short l)
{
	return l;
}

static int    LongSwap (int l)
{
	unsigned char    b1,b2,b3,b4;

	b1 = l&255;
	b2 = (l>>8)&255;
	b3 = (l>>16)&255;
	b4 = (l>>24)&255;

	return ((int)b1<<24) + ((int)b2<<16) + ((int)b3<<8) + b4;
}

static int	LongNoSwap (int l)
{
	return l;
}

static float FloatSwap (float f)
{
	union
	{
		float	f;
		unsigned char	b[4];
	} dat1, dat2;


	dat1.f = f;
	dat2.b[0] = dat1.b[3];
	dat2.b[1] = dat1.b[2];
	dat2.b[2] = dat1.b[1];
	dat2.b[3] = dat1.b[0];
	return dat2.f;
}

static float FloatNoSwap (float f)
{
	return f;
}

/*
================
Swap_Init
================
*/
static void Swap_Init (void)
{
	unsigned char	swaptest[2] = {1,0};
	void *pSwapTest = &swaptest;

// set the byte swapping variables in a portable manner
	if ( *(short *)pSwapTest == 1)
	{
		bigendien = false;
		_BigShort = ShortSwap;
		_LittleShort = ShortNoSwap;
		_BigLong = LongSwap;
		_LittleLong = LongNoSwap;
		_BigFloat = FloatSwap;
		_LittleFloat = FloatNoSwap;
	}
	else
	{
		bigendien = true;
		_BigShort = ShortNoSwap;
		_LittleShort = ShortSwap;
		_BigLong = LongNoSwap;
		_LittleLong = LongSwap;
		_BigFloat = FloatNoSwap;
		_LittleFloat = FloatSwap;
	}
}

/************************************************************************************
 * animatemodel()
 */

static mat4 loc_pose_matrix[MAXBONE];
static mat4 abs_pose_matrix[MAXBONE];
static mat4 skin_matrix[MAXBONE];

void animatemodel(struct IQE_model *model, struct anim *anim, int frame)
{
	struct skel *skel = model->skel;
	struct mesh *mesh = model->mesh;

	frame = CLAMP(frame, 0, anim->len-1);

	calc_matrix_from_pose(loc_pose_matrix, anim->data[frame], skel->joint_count);
	calc_abs_matrix(abs_pose_matrix, loc_pose_matrix, skel->parent, skel->joint_count);
	calc_mul_matrix(skin_matrix, abs_pose_matrix, mesh->inv_bind_matrix, skel->joint_count);

	if( mesh->vertex_count == 0 || mesh->blendindex == NULL || mesh->blendweight == NULL) { // security test

    return;
	}

	if (!mesh->aposition) mesh->aposition = (float *)malloc(sizeof(float) * mesh->vertex_count * 3);
	if (!mesh->anormal) mesh->anormal = (float *)malloc(sizeof(float) * mesh->vertex_count * 3);

	int *bi = mesh->blendindex;
	float *bw = mesh->blendweight;
	float *sp = mesh->position;
	float *sn = mesh->normal;
	float *dp = mesh->aposition;
	float *dn = mesh->anormal;
	int n = mesh->vertex_count;

	while (n--) {
		int i;
		dp[0] = dp[1] = dp[2] = 0;
		dn[0] = dn[1] = dn[2] = 0;
		for (i = 0; i < 4; i++) {
			vec3 tp, tn;

			if( bw[i] == 0.0) {   // and of list
        break;
			}

			mat_vec_mul(tp, skin_matrix[bi[i]], sp);
			mat_vec_mul_n(tn, skin_matrix[bi[i]], sn);
			vec_scale(tp, tp, bw[i]);
			vec_scale(tn, tn, bw[i]);
			vec_add(dp, dp, tp);
			vec_add(dn, dn, tn);
		}
		bi += 4; bw += 4;
		sp += 3; sn += 3;
		dp += 3; dn += 3;
	}
}

/************************************************************************************
 * drawskeleton()
 */

static void getPixel3dTo2D( float x, float y, float z, int *pOutX, int *pOutY)
{
  GLdouble modelMatrix[16];
  GLdouble projMatrix[16];
  GLint viewport[4];

  GLdouble objx = x;
  GLdouble objy = y;
  GLdouble objz = z;

  GLdouble pix_x;
  GLdouble pix_y;
  GLdouble pix_z;

  memset( modelMatrix, 0, sizeof( modelMatrix));
  memset( projMatrix, 0, sizeof( projMatrix));
  memset( viewport, 0, sizeof( viewport));

  glGetDoublev(GL_MODELVIEW_MATRIX, modelMatrix);
  glGetDoublev(GL_PROJECTION_MATRIX, projMatrix);
  glGetIntegerv(GL_VIEWPORT, viewport);

  gluProject( objx, objy, objz, modelMatrix, projMatrix, viewport,
              &pix_x, &pix_y, &pix_z);

  //need to reverse y since OpenGL has y=0 at bottom, but windows has y=0 at top
  *pOutX = (int)pix_x - viewport[0];
  *pOutY = viewport[1] + viewport[3] - (int)pix_y;
}

#ifdef use_again
static int haschildren(int *parent, int count, int x)
{
	int i;
	for (i = x; i < count; i++)
		if (parent[i] == x)
			return 1;
	return 0;
}
#endif

void drawskeleton(struct IQE_model *model)
{
	struct skel *skel = model->skel;
	int i;

	// draw bones

	glBegin(GL_LINES);
	for (i = 0; i < skel->joint_count; i++) {
		float *a = abs_pose_matrix[i];


#ifdef use_again
		if( haschildren(skel->parent, skel->joint_count, i)) { // This joint has children
			glColor4f(1, 1, 0, 1);      // bone color
		} else {
			glColor4f(1, 0, 0, 1);      // end color
		}
#else
		glColor4f(1, 1, 0, 1);      // bone color
#endif

		if (skel->parent[i] >= 0) {   // have a parent
			float *b = abs_pose_matrix[skel->parent[i]];
			glVertex3f(a[12], a[13], a[14]);
			glColor4f(1, 1, 0, 1);     // bone color
			glVertex3f(b[12], b[13], b[14]);
		} else {
		  // this has no parent
			glVertex3f(a[12], a[13], a[14]);
			glColor4f(0, 0, 0, 1);     // black
			glVertex3f(0, 0, 0);
		}
	}
	glEnd();

	// draw joints

	glColor4f(1, 1, 0.5, 1);     // joint color

	glPointSize( 5.0);           // like bigger points

	glBegin(GL_POINTS);

	for (i = 0; i < skel->joint_count; i++) {
		float *a = abs_pose_matrix[i];

  	glVertex3f(a[12], a[13], a[14]);
	}
	glEnd();

	glPointSize( 1.0);           // reset point size

  // Get 2D positions of joints
  // NOTE: Must do this outside glBegin()/glEnd(
	for (i = 0; i < skel->joint_count; i++) {
		float *a = abs_pose_matrix[i];

    getPixel3dTo2D( a[12], a[13], a[14], skel->TempDrawPosX + i, skel->TempDrawPosY + i);
	}

}

/************************************************************************************
 * drawmodel()
 */

void drawmodel( struct IQE_model *model, int DoDrawBlendWeightColors)
{
	struct mesh *mesh = model->mesh;
	int i;

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, mesh->aposition ? mesh->aposition : mesh->position);

	if (mesh->normal || mesh->anormal) {
    glEnableClientState(GL_NORMAL_ARRAY);
    glNormalPointer(GL_FLOAT, 0, mesh->anormal ? mesh->anormal : mesh->normal);
	}

	if (mesh->texcoord) {
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(2, GL_FLOAT, 0, mesh->texcoord);
	}

	if( DoDrawBlendWeightColors && mesh->blendColors) {

    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(4, GL_FLOAT, 0, mesh->blendColors);

	} else if (mesh->color) {
    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(4, GL_FLOAT, 0, mesh->color);
	}

	for (i = 0; i < mesh->part_count; i++) {
		glColor4f(1, 1, 1, 1);
		glBindTexture(GL_TEXTURE_2D, mesh->part[i].material);
		glDrawElements(GL_TRIANGLES, mesh->part[i].count, GL_UNSIGNED_INT, mesh->element + mesh->part[i].first);
	}

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
}

/************************************************************************************
 * measuremodel()
 */

float measuremodel(struct IQE_model *model, float center[3], float size[3])
{
	struct skel *skel = model->skel;
	struct mesh *mesh = model->mesh;
	struct anim *anim;
	float dist, maxdist = 0.25;
	float minbox[3], maxbox[3];
	int i, k, nMinMax;

	center[0] = center[1] = center[2] = 0;

#ifdef use_again
	for (i = 0; i < mesh->vertex_count; i++)
		vec_add(center, center, mesh->position + i * 3);
	if (mesh->vertex_count) {
		center[0] /= mesh->vertex_count;
		center[1] /= mesh->vertex_count;
		center[2] /= mesh->vertex_count;
	}
#else
  // 25.03.2014 RR: use min/max box center, is better than center of gravity

  minbox[0] = minbox[1] = minbox[2] = 0;
  maxbox[0] = maxbox[1] = maxbox[2] = 0;
  nMinMax = 0;

  // vertex

	for (i = 0; i < mesh->vertex_count; i++) {

    if( nMinMax == 0) {

  		vec_copy( minbox, mesh->position + i * 3);
  		vec_copy( maxbox, mesh->position + i * 3);
    } else {

      vec_minmax( minbox, maxbox, mesh->position + i * 3);
    }

    nMinMax += 1;
  }

  // skelton

	if (skel->joint_count > 0) {

  	for (i = 0; i < skel->joint_count; i++) {

      if( nMinMax == 0) {

    		vec_copy( minbox, mesh->abs_bind_matrix[i] + 12);
  	  	vec_copy( maxbox, mesh->abs_bind_matrix[i] + 12);
      } else {

        vec_minmax( minbox, maxbox, mesh->abs_bind_matrix[i] + 12);
      }

      nMinMax += 1;
  	}

  	for( i = 0; i < model->anim_count; i++) {

		  anim = model->anim_data[ i];

			for (k = 0; anim && k < anim->len; k++) {
				calc_matrix_from_pose(loc_pose_matrix, anim->data[k], skel->joint_count);
				calc_abs_matrix(abs_pose_matrix, loc_pose_matrix, skel->parent, skel->joint_count);
				for (i = 0; i < skel->joint_count; i++) {

          if( nMinMax == 0) {

    	  	  vec_copy( minbox, abs_pose_matrix[i] + 12);
  	  	    vec_copy( maxbox, abs_pose_matrix[i] + 12);
          } else {

            vec_minmax( minbox, maxbox, abs_pose_matrix[i] + 12);
          }

          nMinMax += 1;
				}
			}
		}
	}

	center[0] = (minbox[0] + maxbox[0]) * 0.5;
	center[1] = (minbox[1] + maxbox[1]) * 0.5;
	center[2] = (minbox[2] + maxbox[2]) * 0.5;

	size[0] = maxbox[0] - minbox[0];
	size[1] = maxbox[1] - minbox[1];
	size[2] = maxbox[2] - minbox[2];
#endif

	for (i = 0; i < mesh->vertex_count; i++) {
		dist = vec_dist2(center, mesh->position + i * 3);
		if (dist > maxdist)
			maxdist = dist;
	}

	if (skel->joint_count > 0) {
		for (i = 0; i < skel->joint_count; i++) {
			dist = vec_dist2(center, mesh->abs_bind_matrix[i] + 12);
			if (dist > maxdist)
				maxdist = dist;
		}

  	for( i = 0; i < model->anim_count; i++) {

		  anim = model->anim_data[ i];

			for (k = 0; anim && k < anim->len; k++) {
				calc_matrix_from_pose(loc_pose_matrix, anim->data[k], skel->joint_count);
				calc_abs_matrix(abs_pose_matrix, loc_pose_matrix, skel->parent, skel->joint_count);
				for (i = 0; i < skel->joint_count; i++) {
					dist = vec_dist2(center, abs_pose_matrix[i] + 12);
					if (dist > maxdist)
						maxdist = dist;
				}
			}
		}

		memcpy(abs_pose_matrix, mesh->abs_bind_matrix, sizeof abs_pose_matrix);
	}

	return sqrt(maxdist);
}

/************************************************************************************
 * Boring UI and GLUT hooks.
 */

#define DIABLO 36.8698976	// 4:3 isometric view
#define ISOMETRIC 35.264	// true isometric view
#define DIMETRIC 30		// 2:1 'isometric' as seen in pixel art

int showhelp = 1;
int doplane = 1;
int dowire = 0;
int donormals = 0;
int dotexture = 1;
int dobackface = 1;
int doperspective = 1;
int doskeleton = 1;
int doJointNames = 1;
int doBlendWeightColors = 0;
int doplay = 1;

// ...

int screenw = 1024, screenh = 768;
int mousex, mousey, mouseleft = 0, mousemiddle = 0, mouseright = 0;

float gridsize = 3.0;
float gridstep = 1.0;
float mindist = 1.0;
float maxdist = 10.0;

float light_position[4] = { -1, -2, 2, 0 };

typedef struct {
	float distance;
	float yaw;
	float pitch;
	float ObjCenter[3];   // Center of object
	float ObjSize[3];     // size of object
} T_camera;

T_camera camera = { 3, 45, -DIMETRIC, { 0, 1, 0 } };

// setup model to draw

void SetupDrawModel( struct IQE_model *pModel)
{
  int i;

  // for draw model in use, unlink textures

  if( pDrawModel != NULL) {

	  for( i = 0; i < pDrawModel->mesh->part_count; i++) {

      if( pDrawModel->mesh->part[i].material > 0) {       // openGL has loaded

        unsigned int TempMaterial;

        TempMaterial = pDrawModel->mesh->part[i].material;

        glDeleteTextures( 1, &TempMaterial);
      }

      pDrawModel->mesh->part[i].material = -1;            // invalidate loaded texture
	  }
  }

  // model to draw

  pDrawModel = pModel;

  // for draw model in use, load materials

  if( pDrawModel != NULL) {

	  for( i = 0; i < pDrawModel->mesh->part_count; i++) {

      if( pDrawModel->mesh->part[i].material > 0) {       // openGL has loaded

        unsigned int TempMaterial;

        TempMaterial = pDrawModel->mesh->part[i].material;

        glDeleteTextures( 1, &TempMaterial);
      }

      pDrawModel->mesh->part[i].material = IqeB_TextureLoadMaterial( pDrawModel->mesh->part[i].pMaterialString, pDrawModel->mesh->part[i].pMaterialBasedir);  // load texture
	  }
  }

  // invalidate any message to draw

  strcpy( DrawMessageText1, "");
}

// setup messag to draw

#define DRAW_MSG_COL_ERR 1.0, 0.2, 0.2     // red, error
#define DRAW_MSG_COL_OK  0.1, 0.8, 0.1     // green, OK

void SetupDrawMessage( float r, float g, float b, char *pText1,  char *pText2, char *pText3)
{

  DrawMessageColR = r;
  DrawMessageColG = g;
  DrawMessageColB = b;

  DrawMessageText1[ 0] = '\0';  // reset to empty
  DrawMessageText2[ 0] = '\0';
  DrawMessageText3[ 0] = '\0';

  if( pText1 != NULL) strcpy( DrawMessageText1, pText1);
  if( pText2 != NULL) strcpy( DrawMessageText2, pText2);
  if( pText3 != NULL) strcpy( DrawMessageText3, pText3);
}

// ...

void perspective(float fov, float aspect, float znear, float zfar)
{
	fov = fov * 3.14159 / 360.0;
	fov = tan(fov) * znear;
	glFrustum(-fov * aspect, fov * aspect, -fov, fov, znear, zfar);
}

void orthogonal(float fov, float aspect, float znear, float zfar)
{
	glOrtho(-fov * aspect, fov * aspect, -fov, fov, znear, zfar);
}

void drawstring(float x, float y, char *s)
{
	glRasterPos2f(x+0.375, y+0.375);
	while (*s)
		//glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *s++);
		glutBitmapCharacter(GLUT_BITMAP_8_BY_13, *s++);
}

void mouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON) mouseleft = state == GLUT_DOWN;
	if (button == GLUT_MIDDLE_BUTTON) mousemiddle = state == GLUT_DOWN;
	if (button == GLUT_RIGHT_BUTTON) mouseright = state == GLUT_DOWN;
	mousex = x;
	mousey = y;
}

void motion(int x, int y)
{
	int dx = x - mousex;
	int dy = y - mousey;
	if (mouseleft) {
		camera.yaw -= dx * 0.3;
		camera.pitch -= dy * 0.2;
		if (camera.pitch < -85) camera.pitch = -85;
		if (camera.pitch > 85) camera.pitch = 85;
		if (camera.yaw < 0) camera.yaw += 360;
		if (camera.yaw > 360) camera.yaw -= 360;
	}
	if (mousemiddle || mouseright) {
		camera.distance += dy * 0.01 * camera.distance;
		if (camera.distance < mindist) camera.distance = mindist;
		if (camera.distance > maxdist) camera.distance = maxdist;
	}
	mousex = x;
	mousey = y;
	glutPostRedisplay();
}

void togglefullscreen(void)
{
	static int oldw = 100, oldh = 100;
	static int oldx = 0, oldy = 0;
	static int isfullscreen = 0;
	if (!isfullscreen) {
		oldw = screenw;
		oldh = screenh;
		oldx = glutGet(GLUT_WINDOW_X);
		oldy = glutGet(GLUT_WINDOW_Y);
		glutFullScreen();
	} else {
		glutPositionWindow(oldx, oldy);
		glutReshapeWindow(oldw, oldh);
	}
	isfullscreen = !isfullscreen;
}

void stepframe(int dir)
{
	curframe += dir;
	while (curframe < 0) curframe += curanim->len;
	while (curframe >= curanim->len) curframe -= curanim->len;
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case 27: case 'q': exit(1); break;
	case 'h': case '?': showhelp = !showhelp; break;
	case 'f': togglefullscreen(); break;
	case 'i': doperspective = 0; camera.yaw = 45; camera.pitch = -DIMETRIC; break;
	case 'I': doperspective = 0; camera.yaw = 45; camera.pitch = -ISOMETRIC; break;
	case 'D': doperspective = 0; camera.yaw = 45; camera.pitch = -DIABLO; break;
	case 'p': doperspective = !doperspective; break;
	case 'g': doplane = !doplane; break;
	case 't': dotexture = !dotexture; break;
	case 'w': dowire = !dowire; break;
	case 'n': donormals = !donormals; break;
	case 'b': dobackface = !dobackface; break;
	case 's': doskeleton = !doskeleton; break;
	case 'j': doJointNames = !doJointNames; break;
	case 'c': doBlendWeightColors = !doBlendWeightColors; break;
	case ' ': doplay = !doplay; break;
	case '0': curframe = 0; if (pDrawModel && curanim) animatemodel(pDrawModel, curanim, curframe); break;
	case ',': if (pDrawModel && curanim) { stepframe(-1); animatemodel(pDrawModel, curanim, curframe); } break;
	case '.': if (pDrawModel && curanim) { stepframe(1); animatemodel(pDrawModel, curanim, curframe); } break;
	case '<':
		if (pDrawModel && curanim && pDrawModel->anim_count > 1) {

      int ThisAnimNum;

      for( ThisAnimNum = 0; ThisAnimNum < pDrawModel->anim_count; ThisAnimNum++) {

        if( pDrawModel->anim_data[ ThisAnimNum] == curanim) {  // have not reched the active animation

          break;
        }
      }

      ThisAnimNum -= 1;

      if( ThisAnimNum >= 0) {

			  curanim = pDrawModel->anim_data[ ThisAnimNum];
      } else {

			  curanim = pDrawModel->anim_data[ pDrawModel->anim_count - 1];
      }

      curframe = 0;
			animatemodel(pDrawModel, curanim, curframe);
		}
		break;
	case '>':
		if (pDrawModel && curanim && pDrawModel->anim_count > 1) {

      int ThisAnimNum;

      for( ThisAnimNum = 0; ThisAnimNum < pDrawModel->anim_count; ThisAnimNum++) {

        if( pDrawModel->anim_data[ ThisAnimNum] == curanim) {  // have not reched the active animation

          break;
        }
      }

      ThisAnimNum += 1;

      if( ThisAnimNum < pDrawModel->anim_count) {

			  curanim = pDrawModel->anim_data[ ThisAnimNum];
      } else {

			  curanim = pDrawModel->anim_data[ 0];
      }

      curframe = 0;
			animatemodel(pDrawModel, curanim, curframe);
		}
		break;
	}

  // Setup animation
	if (pDrawModel && doplay) {
		if (!curanim && pDrawModel->anim_count > 0) {
			curanim = pDrawModel->anim_data[0];
		}
		lasttime = glutGet(GLUT_ELAPSED_TIME);
	}

	glutPostRedisplay();
}

void special(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_F4: exit(1); break;
	case GLUT_KEY_F1: showhelp = !showhelp; break;
	}
	glutPostRedisplay();
}


static int DrawAoi_tx, DrawAoi_ty, DrawAoi_tw, DrawAoi_th;

void ReshapeForGLUI()
{
  GLUI_Master.get_viewport_area( &DrawAoi_tx, &DrawAoi_ty, &DrawAoi_tw, &DrawAoi_th );
  glViewport( DrawAoi_tx, DrawAoi_ty, DrawAoi_tw, DrawAoi_th );

  // also copy to this, so projects are OK
	screenw = DrawAoi_tw;
	screenh = DrawAoi_th;
}

void reshape(int w, int h)
{
#ifdef use_again
	screenw = w;
	screenh = h;
	glViewport(0, 0, w, h);
#else
  ReshapeForGLUI();    // Reshape window size for GLUI, get window position and sizes
#endif
}

void display(void)
{
	char buf[256];
	int i, DoDrawJointNames, AxisX_X, AxisX_Y, AxisY_X, AxisY_Y;
	int DoDrawBlendWeightColors;

	int thistime = glutGet(GLUT_ELAPSED_TIME);
	int timediff = thistime - lasttime;
	lasttime = thistime;

	DoDrawJointNames = false;    // preset no draw of bone names

  ReshapeForGLUI();    // Reshape window size for GLUI, get window position and sizes

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if( pDrawModel == NULL) {           // no model loaded

    goto SkipDrawModel;
	}

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (doperspective)
		perspective(50, (float)screenw/screenh, mindist/5, maxdist*5);
	else
		orthogonal(camera.distance/2, (float)screenw/screenh, mindist/5, maxdist*5);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(-90, 1, 0, 0); // Z-up

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);

	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glTranslatef(0, camera.distance, 0);
	glRotatef(-camera.pitch, 1, 0, 0);
	glRotatef(-camera.yaw, 0, 0, 1);
#ifndef use_again
	glTranslatef(-camera.ObjCenter[0], -camera.ObjCenter[1], -camera.ObjCenter[2]);
#else
	glTranslatef(-camera.ObjSize[0] * 0.5, -camera.ObjSize[1] * 0.5, -camera.ObjSize[2] * 0.5);
#endif

	if (doplay && !curanim && pDrawModel->anim_count > 0) {
    curanim = pDrawModel->anim_data[ 0];
		lasttime = glutGet(GLUT_ELAPSED_TIME);
	} else if( curanim && pDrawModel->anim_data == NULL) {

    curanim = NULL;
	}

	if (doplay && curanim) {
		glutPostRedisplay();
		curtime = curtime + (timediff / 1000.0) * curanim->framerate;   // animate with 10.0 frames per second
		curframe = ((int)curtime) % curanim->len;
		animatemodel(pDrawModel, curanim, curframe);
	}

	// setup drawing of blend colors
  if( doBlendWeightColors &&                    // drawing blend colors is on
      pDrawModel->mesh->blendColors == NULL) {  // no blend colors

    IqeB_ModelBuildBlendColors( pDrawModel);  // build the blend colors
  }

	if( doBlendWeightColors && pDrawModel->mesh->blendColors) {  // Display blend weight colors

    DoDrawBlendWeightColors = true;

		glDisable(GL_TEXTURE_2D);                  // textures off

	} else {

    DoDrawBlendWeightColors = false;

  	if (dotexture)
	  	glEnable(GL_TEXTURE_2D);
	  else
		  glDisable(GL_TEXTURE_2D);

	}

  if (dowire)
  	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  else
	  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	if (dobackface)
		glDisable(GL_CULL_FACE);
	else
		glEnable(GL_CULL_FACE);

	glAlphaFunc(GL_GREATER, 0.2);
	glEnable(GL_ALPHA_TEST);

	drawmodel( pDrawModel, DoDrawBlendWeightColors);

	glDisable(GL_ALPHA_TEST);

	glDisable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_TEXTURE_2D);

	glDisable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);

	if( donormals && pDrawModel->mesh->normal != NULL) {

    float NormalLength;

    NormalLength = gridsize * 0.025;    // This part of grid size
    if( NormalLength < 0.05) {         // clip
      NormalLength = 0.05;
    }
    if( NormalLength > 1.0) {         // clip
      NormalLength = 1.0;
    }

		glBegin(GL_LINES);
		glColor4f( 0.8, 0.4, 0.4, 1);

  	for (i = 0; i < pDrawModel->mesh->vertex_count; i++) {

      vec3 p, n;

      vec_copy( p, pDrawModel->mesh->position + i * 3);  // get point
      vec_copy( n, pDrawModel->mesh->normal + i * 3);    // get normal
      vec_scale_to( n, NormalLength);                    // scale length for visualisation
      vec_add_to( n, p);                                 // end point of line we draw

      glVertex3f( p[ 0],  p[ 1],  p[ 2]);
      glVertex3f( n[ 0],  n[ 1],  n[ 2]);
  	}

		glEnd();
	}

#ifdef _DEBUG
#ifdef use_again
  // Draw normals of trinangles

  {
    int j;
    struct mesh *mesh;
    float NormalLength;

    NormalLength = gridsize * 0.025;    // This part of grid size
    if( NormalLength < 0.05) {         // clip
      NormalLength = 0.05;
    }
    if( NormalLength > 1.0) {         // clip
      NormalLength = 1.0;
    }

    mesh = pDrawModel->mesh;

		glBegin(GL_LINES);

  	for (i = 0; i < mesh->part_count; i++) {

      for( j = 0; j < mesh->part[i].count; j += 3) {

        int i1, i2, i3;
        vec3 p1, p2, p3, d1, d2, normal, center;

        // get points of triangle
        i1 = mesh->element[ mesh->part[ i].first + j + 0];
        i2 = mesh->element[ mesh->part[ i].first + j + 1];
        i3 = mesh->element[ mesh->part[ i].first + j + 2];

        vec_copy( p1, mesh->position + i1 * 3);  // get point
        vec_copy( p2, mesh->position + i2 * 3);  // get point
        vec_copy( p3, mesh->position + i3 * 3);  // get point

        // normal on triangle
        vec_sub( d1, p2, p1);
        vec_sub( d2, p3, p1);
        vec_cross( normal, d1, d2);
        vec_normalize( normal);
        vec_scale_to( normal, NormalLength);   // scale length for visualisation

        // Center of grafity of points

        vec_set( center, 0.0, 0.0, 0.0);
        vec_add_to( center, p1);
        vec_add_to( center, p2);
        vec_add_to( center, p3);
        vec_scale_to( center, 1.0 / 3.0);

        // draw from edge points to center
		    glColor4f( 0.4, 0.8, 0.4, 1);
        glVertex3f( center[ 0], center[ 1], center[ 2]);
        glVertex3f( p1[ 0], p1[ 1], p1[ 2]);
        glVertex3f( center[ 0], center[ 1], center[ 2]);
        glVertex3f( p2[ 0], p2[ 1], p2[ 2]);
        glVertex3f( center[ 0], center[ 1], center[ 2]);
        glVertex3f( p3[ 0], p3[ 1], p3[ 2]);

        // draw normal from center
        vec_copy( p1, center);
        vec_add_to( p1, normal);

		    glColor4f( 0.2, 1.0, 0.2, 1);
        glVertex3f( center[ 0], center[ 1], center[ 2]);
        glVertex3f( p1[ 0], p1[ 1], p1[ 2]);
      }
	  }

		glEnd();
  }
#endif
#endif

	if (doplane) {

    float g;

		glBegin(GL_LINES);

		// Draw Grid
		glColor4f(0.4, 0.4, 0.4, 1);
		for (g = 0; g >= - gridsize; g -= gridstep) {
      if( g == 0.0) {

			  glVertex3f(g, -gridsize, 0); glVertex3f(g, 0, 0);
			  glVertex3f(-gridsize, g, 0); glVertex3f(0, g, 0);
      } else {

			  glVertex3f(g, -gridsize, 0); glVertex3f(g, gridsize, 0);
			  glVertex3f(-gridsize, g, 0); glVertex3f(gridsize, g, 0);
      }
		}
		for (g = gridstep; g <= gridsize; g += gridstep) {
      if( g == 0.0) {

			  glVertex3f(g, -gridsize, 0); glVertex3f(g, 0, 0);
			  glVertex3f(-gridsize, g, 0); glVertex3f(0, g, 0);
      } else {

			  glVertex3f(g, -gridsize, 0); glVertex3f(g, gridsize, 0);
			  glVertex3f(-gridsize, g, 0); glVertex3f(gridsize, g, 0);
      }
		}

		// Draw X axis

		glColor4f( 0.4, 0.7, 0.7, 1);
	  glVertex3f( 0, 0, 0); glVertex3f( gridsize + 2, 0, 0);
	  glVertex3f( gridsize + 2, 0, 0); glVertex3f( gridsize + 1,  1, 0);
	  glVertex3f( gridsize + 2, 0, 0); glVertex3f( gridsize + 1, -1, 0);

		// Draw Y axis

		glColor4f( 0.4, 0.7, 0.7, 1);
	  glVertex3f( 0, 0, 0); glVertex3f( 0, gridsize + 2, 0);
	  glVertex3f( 0, gridsize + 2, 0); glVertex3f(  1, gridsize + 1, 0);
	  glVertex3f( 0, gridsize + 2, 0); glVertex3f( -1, gridsize + 1, 0);

		glEnd();

    // Map axis entpoints from 3D to 2D space
    // NOTE: Must do this outside glBegin()/glEnd(
    getPixel3dTo2D( gridsize + 3, 0, 0, &AxisX_X, &AxisX_Y);
    getPixel3dTo2D( 0, gridsize + 3, 0, &AxisY_X, &AxisY_Y);
	}

	glDisable(GL_DEPTH_TEST);

	if (doskeleton) {
		drawskeleton(pDrawModel);
  	DoDrawJointNames = doJointNames;    // set no draw of joint names
	}

SkipDrawModel:

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, screenw, screenh, 0, -1, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if( pDrawModel && DoDrawJointNames) {

	  struct skel *skel = pDrawModel->skel;

  	glColor4f( 1, 0.5, 0.0, 1);

  	for (i = 0; i < skel->joint_count; i++) {

  	  drawstring( skel->TempDrawPosX[ i] + 6, skel->TempDrawPosY[ i] + 4, skel->name[ i]);
  	}
	}

	if( pDrawModel && doplane) {

    // Text for X/Y axis
		glColor4f( 0.4, 0.7, 0.7, 1);
    drawstring( AxisX_X - 4, AxisX_Y - 4, (char *)"X");
    drawstring( AxisY_X - 4, AxisY_Y - 4, (char *)"Y");
	}

	glColor4f( 1, 1, 1, 1);

  if( pDrawModel != NULL) {

	  sprintf(buf, "%d meshes; %d vertices; %d faces; %d joints; %d animations; %d poses",
		  pDrawModel->mesh->part_count,
      pDrawModel->mesh->vertex_count,
      pDrawModel->mesh->element_count/3,
      pDrawModel->skel->joint_count,
      pDrawModel->anim_count,
      pDrawModel->anim_poses);
	  drawstring(8, 18+0, buf);

	  sprintf( buf, "Model sizes: %.2f / %.2f / %.2f",
      camera.ObjSize[ 0], camera.ObjSize[ 1], camera.ObjSize[ 2]);
	  drawstring(8, 18+16, buf);

	  if (curanim) {
      int ThisAnimNum;

      for( ThisAnimNum = 0; ThisAnimNum < pDrawModel->anim_count; ThisAnimNum++) {

        if( pDrawModel->anim_data[ ThisAnimNum] == curanim) {  // have not reched the active animation

          break;
        }
      }

		  sprintf(buf, "%s %2d / %2d (%03d / %03d), %.1f fps",
        curanim->name, ThisAnimNum + 1, pDrawModel->anim_count,
        curframe + 1, curanim->len, curanim->framerate);
		  drawstring(8, 18+32, buf);
	  }
  } else {

	  sprintf(buf, "No model loaded");
	  drawstring(8, 18+0, buf);
  }

  // Help

	if (showhelp) {
		#define Y(n) 18+56+n*14
		glColor4f(1, 1, 0.5, 1);
		drawstring(8, Y( 0), (char *)"t - toggle textures");
		drawstring(8, Y( 1), (char *)"c - toggle blend weight colors");
		drawstring(8, Y( 2), (char *)"w - toggle wireframe");
		drawstring(8, Y( 3), (char *)"b - toggle backface culling");
		drawstring(8, Y( 4), (char *)"s - toggle skeleton");
		drawstring(8, Y( 5), (char *)"j - toggle joint names");
		drawstring(8, Y( 6), (char *)"g - toggle ground plane");
		drawstring(8, Y( 8), (char *)"p - toggle orthogonal/perspective camera");
		drawstring(8, Y( 9), (char *)"i - set up dimetric camera (2:1)");
		drawstring(8, Y(10), (char *)"I - set up isometric camera (true)");
		drawstring(8, Y(12), (char *)"space - start/stop animation");
		drawstring(8, Y(13), (char *)"',' and '.' - step animation frame by frame");
		drawstring(8, Y(14), (char *)"'<' and '>' - switch animation");
	}

	// draw message

	if( DrawMessageText1[ 0] != '\0') {   // have to draw message

    glColor4f( DrawMessageColR, DrawMessageColG, DrawMessageColB, 1);

		drawstring( 8, DrawAoi_th - 42, DrawMessageText1);
		drawstring( 8, DrawAoi_th - 26, DrawMessageText2);
		drawstring( 8, DrawAoi_th - 10, DrawMessageText3);
	}

	// Program Version

  glColor4f( 1, 0.0, 0.0, 1);
  drawstring( DrawAoi_tw - (strlen( WIN_PROG_VERSION) + 1) * 8, DrawAoi_th - 10, (char *)WIN_PROG_VERSION);

	glutSwapBuffers();

	i = glGetError();

	if (i) {
    //x/fprintf(stderr, "opengl error: %d\n", i);
	}
}

/************************************************************************************
 * PrepareNewFileSelectionModel
 */

void PrepareNewFileSelectionModel( char *pFilename)
{
  char *p;

  // clean things

  IqeB_ModelFreeData( &pFileSelModel, IQE_MODEL_FREE_ALL);         // unload allocated data

  FileSelName[0] = '\0';
  FileSelFullName[0] = '\0';

  curanim = NULL;

  // try to load file

  if( pFilename != NULL) {

		glutSetWindowTitle( pFilename);

    pFileSelModel = IqeB_ImportFile( pFilename);

		if( pFileSelModel != NULL) {        // load was OK

		  float radius = measuremodel( pFileSelModel, camera.ObjCenter, camera.ObjSize);

		  if( radius < 0.01) radius = 0.01; // clip to minimum
		  camera.distance = radius * 2.5;
		  gridsize = radius * 1.01;
		  gridstep = radius * 0.1;
		  mindist = radius * 0.1;
		  maxdist = radius * 10;

		  if (pFileSelModel->mesh->part_count == 0 && pFileSelModel->skel->joint_count > 0)
			  doskeleton = 1;

      // get pointer to file name

	    p = strrchr( pFilename, '/');
	    if (!p) p = strrchr( pFilename, '\\');
	    if (!p) {
        p = pFilename;   // point to begin of file name
      } else {
        p += 1;          // point to begin of file name
      }

      // ...

      strcpy( FileSelName, p);                // file name
      strcpy( FileSelFullName, pFilename);    // path + file name
		}
  } else {

		glutSetWindowTitle( WIN_DEFAULT_TITLE);
  }

  SetupDrawModel( pFileSelModel);       // select model to draw
}

/************************************************************************************
 * PrepareContainerModel
 */

void PrepareContainerModel( struct IQE_model *pModel, char *pFilename)
{
  char *p;

  // clean things

  IqeB_ModelFreeData( &pFileSelModel, IQE_MODEL_FREE_ALL);      // unload allocated data

  FileSelName[0] = '\0';
  FileSelFullName[0] = '\0';

  curanim = NULL;

  // try to load file

  if( pFilename != NULL && pModel != NULL) {

		glutSetWindowTitle( pFilename);

		if( pModel != NULL) {        // load was OK

		  float radius = measuremodel( pModel, camera.ObjCenter, camera.ObjSize);

		  if( radius < 0.01) radius = 0.01; // clip to minimum
		  camera.distance = radius * 2.5;
		  gridsize = radius * 1.01;
		  gridstep = radius * 0.1;
		  mindist = radius * 0.1;
		  maxdist = radius * 10;

		  if (pModel->mesh->part_count == 0 && pModel->skel->joint_count > 0)
			  doskeleton = 1;

      // get pointer to file name

	    p = strrchr( pFilename, '/');
	    if (!p) p = strrchr( pFilename, '\\');
	    if (!p) {
        p = pFilename;   // point to begin of file name
      } else {
        p += 1;          // point to begin of file name
      }

      // ...

      strcpy( FileSelName, p);                // file name
      strcpy( FileSelFullName, pFilename);    // path + file name
		}
  } else {

		glutSetWindowTitle( WIN_DEFAULT_TITLE);
  }

  SetupDrawModel( pModel);       // select model to draw
}

/************************************************************************************
 * GLUI things
 */

#ifdef __GNUC__
#include <unistd.h>   // needed for getcwd()
#endif

static GLUI_FileBrowser *pGUI_fb;
static GLUI_List        *pGUI_Models;
static GLUI_EditText    *pGUI_WriteFileName;
static GLUI_Button      *pGUI_ButtonWriteFile;
static float             GUI_ScaleVale;

// -- update model container

void ModelContainerUpdate( int KeepSelection)
{
  int i;
  int LastSelection;

  LastSelection = pGUI_Models->get_current_item();

  pGUI_Models->delete_all();       // empty the list box

  // fill the list box

  for( i = 0; i < ModelContainer_n; i++) {

    pGUI_Models->add_item( i, ModelContainer[ i].ModelName);
  }

  if( KeepSelection) {

    pGUI_Models->curr_line = LastSelection;          // set to last added

    if( pGUI_Models->curr_line >= pGUI_Models->num_lines) {  // security test

      pGUI_Models->curr_line = pGUI_Models->num_lines - 1;
    }
  } else {

    pGUI_Models->curr_line = ModelContainer_n - 1;   // set to last added
  }

  pGUI_Models->start_line = 0;    // display starts at first line

  if( pGUI_Models->curr_line >= (pGUI_Models->visible_lines - 1)) {   // if current is not vissible

    pGUI_Models->start_line = pGUI_Models->num_lines - (pGUI_Models->visible_lines - 1);  // try to center

    if( pGUI_Models->start_line < 0) {    // clip to 0

      pGUI_Models->start_line = 0;
    }
  }

  pGUI_Models->update_and_draw_text();             // updated

  if( ModelContainer_n > 0) {     // have any models

    pGUI_ButtonWriteFile->enable();
  } else {

    pGUI_ButtonWriteFile->disable();
  }

}

// -- GUI control function, called on GUI events

void control_cb( int control) {
  int i, this_item;
  char file_name[ 1024];

  glutPostRedisplay();

  SetupDrawMessage( DRAW_MSG_COL_OK, NULL, NULL, NULL);  // reset any message

  switch( control) {

  case 1:             // list file browser

    strcpy( file_name, pGUI_fb->current_dir.c_str());

    if( strcmp( file_name, ".") == 0) {      // is current directory

#ifdef _WIN32
      GetCurrentDirectory( sizeof( file_name) - 256, file_name);

#elif defined(__GNUC__)

      getcwd( file_name, sizeof( file_name) - 256);
#endif

    }

    strcat( file_name, "/");
    strcat( file_name, pGUI_fb->get_file());

    PrepareNewFileSelectionModel( file_name);

    break;

  case 2:             // display model container line

    this_item = pGUI_Models->get_current_item();
    if (this_item >= 0 && this_item < ModelContainer_n) { // indexes is in range


      PrepareContainerModel( ModelContainer[ this_item].pModel, ModelContainer[ this_item].ModelFullName);

		  curframe = 0;
		  if( pDrawModel != NULL && pDrawModel->anim_count > 0) {

		    curanim  = pDrawModel->anim_data[ 0];
		  } else {

		    curanim  = NULL;
		  }
    }

    break;

  case 3:             // Add to model container

    if( pDrawModel == NULL ||                        // have a model loaded
        pFileSelModel != pDrawModel) {               // must be selected by the file browser

      SetupDrawMessage( DRAW_MSG_COL_ERR,
                        (char *)"Add:",
                        (char *)"Please select a file",
                        (char *)"with the file browser");

    } else if( ModelContainer_n >= MODEL_CONTAINER_MAX) {    // container is full

      SetupDrawMessage( DRAW_MSG_COL_ERR,
                        (char *)"Add:", (char *)"Model container is full", NULL);

    } else if( ModelContainer_n < MODEL_CONTAINER_MAX &&    // have free space
        pDrawModel != NULL &&                        // have a model loaded
        FileSelName[ 0] != '\0') {                   // have file selected

      // copy model and names

      if( pDrawModel->skel->joint_count == 0 && pDrawModel->anim_poses > 0) { // have NO sekelton but poses from an animation

        sprintf( ModelContainer[ ModelContainer_n].ModelName, "M%2d,P%2d,A%2d,%s",
                 pDrawModel->mesh->part_count,
                 pDrawModel->anim_poses,
                 pDrawModel->anim_count,
                 FileSelName);
      } else {

        sprintf( ModelContainer[ ModelContainer_n].ModelName, "M%2d,J%2d,A%2d,%s",
                 pDrawModel->mesh->part_count,
                 pDrawModel->skel->joint_count,
                 pDrawModel->anim_count,
                 FileSelName);
      }

      // copy the model from the file browser

      strcpy( ModelContainer[ ModelContainer_n].ModelFullName, FileSelFullName);
      IqeB_ModelCopy( &pDrawModel, &(ModelContainer[ ModelContainer_n].pModel), IQE_MODEL_COPY_NEW_ALL);

      // remember the original mesh count

      ModelContainer[ ModelContainer_n].part_count_before_add = pDrawModel->mesh->part_count;

      // If this model ist not the first model in the model container,
      // copy mesh and or sekelton from the first model in the container
      // if we have only an animation

      char AddMessage[ 256];

      strcpy( AddMessage, "");

      if( ModelContainer_n > 0 &&                    // is NOT the first model
          ModelContainer[ ModelContainer_n].pModel->anim_count > 0) { // have an animation

        int CopyFlags;

        CopyFlags = 0;                               // preset nothing to copy

        // Can copy skeleton.
        // On Merge this copied skelton is skipped
        if( ModelContainer[ ModelContainer_n].pModel->skel->joint_count == 0 &&  // have NO skeletion
            ModelContainer[ 0].pModel->skel->joint_count > 0 &&                  // but first model has one
            ModelContainer[ ModelContainer_n].pModel->anim_poses ==              // and number of poses of this model
              ModelContainer[ 0].pModel->skel->joint_count) {                    // match the number of joints of the first model

          CopyFlags |= IQE_MODEL_COPY_SKELETON;   // copy the skeleton

          strcat( AddMessage, "Copied skeleton");
        }

#ifdef use_again
        if( ModelContainer[ ModelContainer_n].pModel->mesh->part_count == 0 &&    // have NO meshes
            ModelContainer[ ModelContainer_n].pModel->mesh->vertex_count == 0 &&  // and have NO vertex data
            ModelContainer[ 0].pModel->mesh->part_count > 0 &&                    // but first model have a meshes
            ModelContainer[ 0].pModel->mesh->vertex_count > 0 &&                  // and first model has vertex data
            ((ModelContainer[ ModelContainer_n].pModel->anim_poses ==             // and number of poses of this model
               ModelContainer[ 0].pModel->skel->joint_count) ||                   // match the number of joints of the first model
             (ModelContainer[ ModelContainer_n].pModel->skel->joint_count >= 0 && //   or this model has a skeleton
               ModelContainer[ 0].pModel->skel->joint_count == 0))) {             //   and the first model has NO skeleton

          CopyFlags |= COPYMODEL_MESH;       // copy the mesh

          if( (CopyFlags & IQE_MODEL_COPY_SKELETON) != 0) {   // also copy skeleton

            strcat( AddMessage, " and mesh");
          } else {

            strcat( AddMessage, "Copied mesh");
          }
        }
#endif

        if( CopyFlags != 0) {     // have something to copy

          IqeB_ModelCopy( &(ModelContainer[ 0].pModel), &(ModelContainer[ ModelContainer_n].pModel), CopyFlags);

          strcat( AddMessage, " from 1. model");
        }
      }

      // ....

      ModelContainer_n += 1;                         // have one more

      ModelContainerUpdate( false);                  // update model container

      // display the just added model

      PrepareContainerModel( ModelContainer[ ModelContainer_n - 1].pModel, ModelContainer[ ModelContainer_n - 1].ModelFullName);

      SetupDrawMessage( DRAW_MSG_COL_OK,
                        (char *)"Add done", AddMessage, NULL);

		  curframe = 0;
		  if( pDrawModel != NULL && pDrawModel->anim_count > 0) {

		    curanim  = pDrawModel->anim_data[ 0];
		  } else {

		    curanim  = NULL;
		  }
    } else {

      SetupDrawMessage( DRAW_MSG_COL_ERR,
                        (char *)"Add failed", NULL, NULL);
    }

    break;

  case 4:             // Remove from model container

    this_item = pGUI_Models->get_current_item();
    if (this_item >= 0 && this_item < ModelContainer_n) { // index is in range

      if( this_item >= ModelContainer_n - 1) {    // is the last one

        // set last one to nil
        strcpy( ModelContainer[ ModelContainer_n - 1].ModelName, "");
        strcpy( ModelContainer[ ModelContainer_n - 1].ModelFullName, "");
        IqeB_ModelFreeData( &ModelContainer[ ModelContainer_n - 1].pModel, IQE_MODEL_FREE_ALL);

        ModelContainer_n -= 1;                // have one less
        ModelContainerUpdate( false);         // update model container
      } else {

        // copy down

        IqeB_ModelFreeData( &ModelContainer[ this_item].pModel, IQE_MODEL_FREE_ALL);

        for( i = this_item; i <  ModelContainer_n - 1; i++) {

          strcpy( ModelContainer[ i].ModelName, ModelContainer[ i + 1].ModelName);
          strcpy( ModelContainer[ i].ModelFullName, ModelContainer[ i + 1].ModelFullName);
          ModelContainer[ i].pModel = ModelContainer[ i + 1].pModel;
        }

        // set last one to nil
        strcpy( ModelContainer[ ModelContainer_n - 1].ModelName, "");
        strcpy( ModelContainer[ ModelContainer_n - 1].ModelFullName, "");
        ModelContainer[ ModelContainer_n - 1].pModel = NULL;

        ModelContainer_n -= 1;                // have one less
        ModelContainerUpdate( true);          // update model container
      }

      SetupDrawMessage( DRAW_MSG_COL_OK,
                        (char *)"Remove done", NULL, NULL);
    }

    break;

  case 5:             // Clear model container

    if( ModelContainer_n > 0) {       // have something to clear

      for( i = 0; i <  ModelContainer_n; i++) {

        strcpy( ModelContainer[ i].ModelName, "");
        strcpy( ModelContainer[ i].ModelFullName, "");
        IqeB_ModelFreeData( &ModelContainer[ i].pModel, IQE_MODEL_FREE_ALL);
      }

      ModelContainer_n = 0;             // set count to 0

      ModelContainerUpdate( false);     // update model container

      SetupDrawMessage( DRAW_MSG_COL_OK,
                      (char *)"Clear done", NULL, NULL);
    }
    break;

  case 6:             // compile model container

    if( ModelContainer_n > 0 && strlen( pGUI_WriteFileName->get_text()) > 0) {

      int argc, ResultValue;
      char *p, *argv[ 1024];
      char OutputFile[ 1024];
      char ResultString[ 1024];
      char ScaleString[ 1024];

      argc = 0;

      argv[ argc++] = (char *)"IqmCompiler";       // name of executable

      // optional scale argument

      if( GUI_ScaleVale != 1.0) {

        argv[ argc++] = (char *)"--scale";                // optional scale

        sprintf( ScaleString, "%f", GUI_ScaleVale);
        argv[ argc++] = ScaleString;              // argument for scale
      }

      // construct output file

      strcpy( OutputFile, ModelContainer[ 0].ModelFullName);  // get location of first modul
      p = strrchr( OutputFile, '/');        // try slash
      if( p == 0) {
        p = strrchr( OutputFile, '\\');     // try backslash
      }

      if( p != 0) {        // got any slash

        p[ 1] = '\0';      // delete after slash
        strcat( OutputFile, pGUI_WriteFileName->get_text());  // append output file name
        strcat( OutputFile,".iqm");                           // append iqm file type
      } else {

        strcpy( OutputFile, pGUI_WriteFileName->get_text());  // append output file name
        strcat( OutputFile,".iqm");                           // append iqm file type
      }

      argv[ argc++] = OutputFile;       // name of output

      // first files arguments are files with meshes, build  comma separeted file list

      char InputFilesWithMeshes[ 1024 * MODEL_CONTAINER_MAX];

      strcpy( InputFilesWithMeshes, "");

      for( i = 0; i < ModelContainer_n; i++) {

        // test for model with meshes
        if( ModelContainer[ i].part_count_before_add == 0) {   // have NO meshes

          break;
        }

        // this model has a mesh, add to comma separated file list

        if( i == 0) {

          strcpy( InputFilesWithMeshes, ModelContainer[ i].ModelFullName);

        } else {

          strcat( InputFilesWithMeshes, ",");
          strcat( InputFilesWithMeshes, ModelContainer[ i].ModelFullName);
        }
      }

      if( InputFilesWithMeshes[ 0] != '\0') {     // have input files

        argv[ argc++] = InputFilesWithMeshes;     // input files list
      }

      // other files must be animatina files, add to argument list
      for( ; i < ModelContainer_n; i++) {

        argv[ argc++] = ModelContainer[ i].ModelFullName;
      }

      // call the compiler

      ResultValue = IQM_Compiler( argc, argv, ResultString);

      if( ResultValue != 0) {    // have error

        SetupDrawMessage( DRAW_MSG_COL_ERR,
                          (char *)"Write IQM failed:", ResultString, NULL);
      } else {

        SetupDrawMessage( DRAW_MSG_COL_OK,
                          (char *)"Write IQM done", NULL, NULL);
      }
    }

    break;

  case 10:             // Merge all models in the countainer

    if( ModelContainer_n > 1) {       // have something to merge

      for( i = 1; i <  ModelContainer_n; i++) {

        // merge to first

        IqeB_ModelMergeTo( ModelContainer[ 0].pModel, ModelContainer[ i].pModel);

        // delete

        strcpy( ModelContainer[ i].ModelName, "");
        strcpy( ModelContainer[ i].ModelFullName, "");
        IqeB_ModelFreeData( &ModelContainer[ i].pModel, IQE_MODEL_FREE_ALL);
      }

      ModelContainer_n = 1;             // set count to 1

      ModelContainerUpdate( false);     // update model container

      SetupDrawMessage( DRAW_MSG_COL_OK,
                      (char *)"Merge done", NULL, NULL);

      PrepareContainerModel( ModelContainer[ 0].pModel, ModelContainer[ 0].ModelFullName);

    }

    break;

  case 11:             // automatic rig of the selected model

    this_item = pGUI_Models->get_current_item();
    if (this_item >= 0 && this_item < ModelContainer_n) { // index is in range

      int RetVal;
      char ErrorString[ 512];

      ErrorString[ 0] = '\0';    // reset error string

      RetVal = IqeB_ProcessForPinocchio( ModelContainer[ this_item].pModel,
                            (char *)"human", (char *)"", (char *)"", false, ErrorString);
      if( RetVal == 0) {

        SetupDrawMessage( DRAW_MSG_COL_OK,
                          (char *)"Auto rigging done", NULL, NULL);
      } else {

        if( ErrorString[ 0] == '\0') {    // got no error return

          strcpy( ErrorString, "Auto rigging failed");
        }

        SetupDrawMessage( DRAW_MSG_COL_ERR, ErrorString, NULL, NULL);
      }

      // remeasure

      if( pDrawModel == ModelContainer[ this_item].pModel) {  // drawing this model

		    float radius = measuremodel( pDrawModel, camera.ObjCenter, camera.ObjSize);

	  	  if( radius < 0.01) radius = 0.01; // clip to minimum
		    camera.distance = radius * 2.5;
		    gridsize = radius * 1.01;
		    gridstep = radius * 0.1;
		    mindist = radius * 0.1;
		    maxdist = radius * 10;

		    if (pDrawModel->mesh->part_count == 0 && pDrawModel->skel->joint_count > 0)
			    doskeleton = 1;
      }
    }

    break;

  case 12:             // mesh polygon reduction

    this_item = pGUI_Models->get_current_item();
    if (this_item >= 0 && this_item < ModelContainer_n) { // index is in range

      int RetVal;
      char ErrorString[ 512];

      ErrorString[ 0] = '\0';    // reset error string

      RetVal = IqeB_Mesh_VCGLib_Tridecimator( ModelContainer[ this_item].pModel->mesh, 10, 0, 50);

      if( RetVal == 0) {

        SetupDrawMessage( DRAW_MSG_COL_OK,
                          (char *)"Ploygon reduction done", NULL, NULL);
      } else {

        if( ErrorString[ 0] == '\0') {    // got no error return

          strcpy( ErrorString, "Ploygon reduction failed");
        }

        SetupDrawMessage( DRAW_MSG_COL_ERR, ErrorString, NULL, NULL);
      }

      // remeasure

      if( pDrawModel == ModelContainer[ this_item].pModel) {  // drawing this model

		    float radius = measuremodel( pDrawModel, camera.ObjCenter, camera.ObjSize);

  		  if( radius < 0.01) radius = 0.01; // clip to minimum
	  	  camera.distance = radius * 2.5;
		    gridsize = radius * 1.01;
		    gridstep = radius * 0.1;
		    mindist = radius * 0.1;
		    maxdist = radius * 10;

		    if (pDrawModel->mesh->part_count == 0 && pDrawModel->skel->joint_count > 0)
			    doskeleton = 1;
      }
    }

    break;

  case 13:             // mesh smooth

    this_item = pGUI_Models->get_current_item();
    if (this_item >= 0 && this_item < ModelContainer_n) { // index is in range

      IqeB_MeshSmooth( ModelContainer[ this_item].pModel->mesh, 1.0, 0.05, 0.3);

      SetupDrawMessage( DRAW_MSG_COL_OK,
                        (char *)"Smooth done", NULL, NULL);

      // remeasure

      if( pDrawModel == ModelContainer[ this_item].pModel) {  // drawing this model

		    float radius = measuremodel( pDrawModel, camera.ObjCenter, camera.ObjSize);

  		  if( radius < 0.01) radius = 0.01; // clip to minimum
	  	  camera.distance = radius * 2.5;
		    gridsize = radius * 1.01;
		    gridstep = radius * 0.1;
		    mindist = radius * 0.1;
		    maxdist = radius * 10;

		    if (pDrawModel->mesh->part_count == 0 && pDrawModel->skel->joint_count > 0)
			    doskeleton = 1;
      }
    }

    break;

  default:

    break;   // used as breakpoint
  }
}

//-- model list callback

void Model_list_callback( GLUI_Control *glui_object)
{
  int this_item;

  GLUI_List *list = dynamic_cast<GLUI_List*>(glui_object);
  if (!list)
    return;

  glutPostRedisplay();

  this_item = list->get_current_item();
  if (this_item >= 0 && this_item < list->num_lines) {

    if (this_item >= 0 && this_item < ModelContainer_n) { // indes is in range

#ifdef use_again
      PrepareNewModel( ModelContainerFullName[ this_item]);

		  curframe = 0;
		  curanim  = pDrawModel->anim;
#else
      // must do it this way, otherwise loaded textures got lost (wrong window selected or so).
      list->execute_callback();
#endif
    }
  }
}

//-- Setup the GUI things

void GLUI_Setup( GLUI *ep)
{
  GLUI_Panel  *pGUI_GroupFrame1, *pGUI_GroupFrame2;
  GLUI_Button *pGUI_Button;
  GLUI_EditText *pGUI_EditText;

  // File browser

  char FileExtensionList[ 2048];

  IqeB_ImportFileExtensions( FileExtensionList, sizeof( FileExtensionList)); // get file extensions we support

  pGUI_fb = new GLUI_FileBrowser(ep, "File browser", GLUI_PANEL_EMBOSSED, 1, control_cb, FileExtensionList);
  pGUI_fb->set_w( 254);
  pGUI_fb->set_h( GLUI_DEFAULT_CONTROL_HEIGHT * 24);
  pGUI_fb->list->set_click_type(GLUI_SINGLE_CLICK);

  // Model container

  pGUI_GroupFrame1 = new GLUI_Panel( ep, "Model container", GLUI_PANEL_EMBOSSED);
  pGUI_Models = new GLUI_List( pGUI_GroupFrame1, true, 2, control_cb);
  pGUI_Models->set_w( 250);
  pGUI_Models->set_h( 15 * MODEL_CONTAINER_DISP + 20);
  pGUI_Models->set_click_type(GLUI_SINGLE_CLICK);
  pGUI_Models->set_object_callback( Model_list_callback);

  // buttons for model container

  pGUI_GroupFrame2 = new GLUI_Panel( pGUI_GroupFrame1, "", GLUI_PANEL_NONE);
  pGUI_GroupFrame2->set_alignment( GLUI_ALIGN_LEFT);

  pGUI_Button = ep->add_button_to_panel( pGUI_GroupFrame2, "Add", 3, control_cb);
  pGUI_Button->set_w( 70);

  ep->add_column_to_panel( pGUI_GroupFrame2, false);

  pGUI_Button = ep->add_button_to_panel( pGUI_GroupFrame2, "Remove", 4, control_cb);
  pGUI_Button->set_w( 70);

  ep->add_column_to_panel( pGUI_GroupFrame2, false);

  pGUI_Button = ep->add_button_to_panel( pGUI_GroupFrame2, "Clear", 5, control_cb);
  pGUI_Button->set_w( 70);

  // more buttons

  pGUI_GroupFrame2 = new GLUI_Panel( pGUI_GroupFrame1, "", GLUI_PANEL_NONE);
  pGUI_GroupFrame2->set_alignment( GLUI_ALIGN_LEFT);

  pGUI_Button = ep->add_button_to_panel( pGUI_GroupFrame2, "Merge", 10, control_cb);
  pGUI_Button->set_w( 70);

  ep->add_column_to_panel( pGUI_GroupFrame2, false);

  pGUI_Button = ep->add_button_to_panel( pGUI_GroupFrame2, "AutoRig", 11, control_cb);
  pGUI_Button->set_w( 70);

  ep->add_column_to_panel( pGUI_GroupFrame2, false);

  pGUI_Button = ep->add_button_to_panel( pGUI_GroupFrame2, "Mesh reduce", 12, control_cb);
  pGUI_Button->set_w( 70);

  ep->add_column_to_panel( pGUI_GroupFrame2, false);

  pGUI_Button = ep->add_button_to_panel( pGUI_GroupFrame2, "Smooth", 13, control_cb);
  pGUI_Button->set_w( 70);

  // write file

  pGUI_GroupFrame2 = new GLUI_Panel( pGUI_GroupFrame1, "", GLUI_PANEL_NONE);
  pGUI_GroupFrame2->set_alignment( GLUI_ALIGN_LEFT);

  pGUI_ButtonWriteFile = ep->add_button_to_panel( pGUI_GroupFrame2, "Write IQM", 6, control_cb);
  pGUI_ButtonWriteFile->set_w( 70);
  pGUI_ButtonWriteFile->disable();    // disable write file button

  ep->add_column_to_panel( pGUI_GroupFrame2, false);

  pGUI_WriteFileName = new GLUI_EditText( pGUI_GroupFrame2, "", GLUI_EDITTEXT_TEXT, -1, control_cb);
  pGUI_WriteFileName->text_x_offset = 0;
  pGUI_WriteFileName->set_w( 180);
  pGUI_WriteFileName->set_text( "MyModel");

  GUI_ScaleVale = 1.0;
  pGUI_EditText = ep->add_edittext_to_panel( pGUI_GroupFrame2, "Scale",
                             GLUI_EDITTEXT_FLOAT, &GUI_ScaleVale, -1, GLUI_CB());
  pGUI_EditText->text_x_offset = 0;

}

/************************************************************************************
 * main
 */

int main(int argc, char **argv)
{
  int main_win;
	float clearcolor[4] = { 0.22, 0.22, 0.22, 1.0 };

	Swap_Init();                              // setup byte swap things

	glutInitWindowPosition(50, 50+24);
	glutInitWindowSize(screenw, screenh);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);

	main_win = glutCreateWindow("IQE Viewer");
	screenw = glutGet(GLUT_WINDOW_WIDTH);
	screenh = glutGet(GLUT_WINDOW_HEIGHT);

#ifdef __APPLE__
	int one = 1;
	void *ctx = CGLGetCurrentContext();
	CGLSetParameter(ctx, kCGLCPSwapInterval, &one);
#endif

	IqeB_TextureInitChecker();

	if (argc > 1) {

    PrepareNewFileSelectionModel( argv[1]);
  } else {

    PrepareNewFileSelectionModel( NULL);
	}

	glEnable(GL_MULTISAMPLE);
	glEnable(GL_NORMALIZE);
	glDepthFunc(GL_LEQUAL);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glClearColor(clearcolor[0], clearcolor[1], clearcolor[2], clearcolor[3]);

#ifdef use_again
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(special);
#else

  GLUI_Master.set_glutReshapeFunc( reshape);
  GLUI_Master.set_glutKeyboardFunc( keyboard);
  GLUI_Master.set_glutSpecialFunc( special);
  GLUI_Master.set_glutMouseFunc( mouse);
  GLUI_Master.set_glutIdleFunc( NULL);

	glutDisplayFunc(display);
	glutMotionFunc(motion);

  GLUI *glui_subwin = GLUI_Master.create_glui_subwindow( main_win, GLUI_SUBWINDOW_LEFT);
  glui_subwin->set_main_gfx_window( main_win);

  GLUI_Setup( glui_subwin);
#endif

	glutMainLoop();

	return 0;
}

/**************************** End Of File *****************************/


