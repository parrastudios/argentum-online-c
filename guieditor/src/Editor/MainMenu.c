/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Editor/MainMenu.h>
#include <GUI/WindowManager.h>
#include <GUI/EventHandler.h>

#include <stdio.h> // printf

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct GuiWindow * MainWindow;
struct GuiWidget * ButtonNew;
struct GuiWidget * ListNewPopup;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

void TogglePopup_ButtonReleased()
{

    ListNewPopup->X = MainWindow->FocusWidget->X;
    ListNewPopup->Y = MainWindow->FocusWidget->Y;
    if (!ListNewPopup->Visible)
        GuiWindowShowWidget(MainWindow, ListNewPopup, true);
    else
        GuiWindowShowWidget(MainWindow, ListNewPopup, false);
}

////////////////////////////////////////////////////////////
/// Register the main menu
////////////////////////////////////////////////////////////
void MainMenuRegister()
{
	// Void Button action
	GuiEventHandlerAdd("ToggleNewList_ButtonReleased", (void*)&TogglePopup_ButtonReleased);
}

////////////////////////////////////////////////////////////
/// Initialize the main menu
////////////////////////////////////////////////////////////
void MainMenuInitialize()
{
	// Obtain window references
	MainWindow = GuiGetWindow("frmEditorMain");


	// Obtain widget references
	ButtonNew = GuiWindowGetWidget(MainWindow, "ButtonNew");
	ListNewPopup = GuiWindowGetWidget(MainWindow, "ListNewPopup");
}

////////////////////////////////////////////////////////////
/// Open the main menu
////////////////////////////////////////////////////////////
void MainMenuOpen()
{
	if (MainWindow->hStatus == GUI_WINDOW_CLOSED)
	{
		// Open Test
		GuiOpenWindow(MainWindow);
	}
}

////////////////////////////////////////////////////////////
/// Close the main menu
////////////////////////////////////////////////////////////
void MainMenuClose()
{
	if (MainWindow->hStatus == GUI_WINDOW_OPENED)
	{
		// Close Test
		GuiCloseWindow(MainWindow);
	}
}

////////////////////////////////////////////////////////////
/// Update the main menu
////////////////////////////////////////////////////////////
void MainMenuUpdate(struct Event * Evt)
{
	// Update user controls
}

////////////////////////////////////////////////////////////
/// Render the main menu
////////////////////////////////////////////////////////////
void MainMenuRender()
{
	// Nothing extra here
}
