/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Memory/General.h>
#include <System/IOHelper.h>

#include <Graphics/Font.h>
#include <Graphics/TextureRender.h>

#include <GUI/Clipboard.h>
#include <GUI/Widget.h>

////////////////////////////////////////////////////////////
// Create a textbox
////////////////////////////////////////////////////////////
struct GuiWidget * GuiTextBoxCreate(int32 X, int32 Y, int32 Length, uint32 FontSize, char * Text, bool Hide)
{
	struct GuiWidget * Widget;

	// Create
	Widget = GuiWidgetCreate(GUI_WIDGET_TEXTBOX, X, Y, FontSize, Text);

	if (Widget)
	{
		struct GuiTextBox * TextBox = &Widget->Attributes.TextBox;

		// Set the size
		Widget->Width			= (FontSize * Length);
		Widget->Height			= (FontSize + 2);

		// Set the text manager
		Widget->Length			= strlen(Text);
		TextBox->SelectionPos	= GUI_TEXTBOX_NOT_SELECTED;
		TextBox->CurrentPos		= Widget->Length;
		TextBox->StartOffset	= 0;
		TextBox->HideText		= Hide;
		// TextBox->Selected		= false;

		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Create a textbox by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiTextBoxCreateDefault()
{
	// Create
	struct GuiWidget * Widget = GuiWidgetCreateDefault(GUI_WIDGET_TEXTBOX);

	if (Widget)
	{
		struct GuiTextBox * TextBox = &Widget->Attributes.TextBox;

		// Set the size
		Widget->Width			= 0;
		Widget->Height			= 0;

		// Set the text manager
		TextBox->SelectionPos	= GUI_TEXTBOX_NOT_SELECTED;
		TextBox->StartOffset	= 0;
		TextBox->CurrentPos		= 0;
		TextBox->HideText		= false;
		// TextBox->Selected		= false;

		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Clears the textbox (not erase it!!)
////////////////////////////////////////////////////////////
void GuiTextBoxClear(struct GuiWidget * Widget)
{
	struct GuiTextBox * TextBox = &Widget->Attributes.TextBox;

	TextBox->SelectionPos	= GUI_TEXTBOX_NOT_SELECTED;
	TextBox->CurrentPos		= 0;
	TextBox->StartOffset	= 0;
}

////////////////////////////////////////////////////////////
// Erase the textbox
////////////////////////////////////////////////////////////
void GuiTextBoxErase(struct GuiWidget * Widget)
{
	// Clear text box
	GuiTextBoxClear(Widget);

	// Clear widget text
	Widget->Text[0] = NULLCHAR;
	Widget->Length = 0;
}

////////////////////////////////////////////////////////////
// Clears the selection (not erase it!!)
////////////////////////////////////////////////////////////
void GuiTextBoxClearSelection(struct GuiWidget * Widget)
{
	Widget->Attributes.TextBox.SelectionPos	= GUI_TEXTBOX_NOT_SELECTED;
}

////////////////////////////////////////////////////////////
// Erase the selection
////////////////////////////////////////////////////////////
void GuiTextBoxEraseSelection(struct GuiWidget * Widget)
{
	struct GuiTextBox * TextBox = &Widget->Attributes.TextBox;

	if (TextBox->SelectionPos != GUI_TEXTBOX_NOT_SELECTED)
	{
		// TODO: This doesn't work...

		char Text[GUI_TEXTBOX_LENGTH] = { 0 };
		int32 Min = min(TextBox->SelectionPos, (int32)TextBox->CurrentPos);
		int32 Max = max(TextBox->SelectionPos, (int32)TextBox->CurrentPos);
		uint32 Length = (uint32)abs(Max - Min);

		if (Length > (Widget->Length - Max))
		{
			Length = Widget->Length - Max + 1;
		}

		// Update the text
		MemoryCopy(&Text[0], &Widget->Text[Max], Length);
		MemoryCopy(&Widget->Text[Min], &Text[0], Length);

		if (Min == TextBox->SelectionPos)
		{
			TextBox->CurrentPos = TextBox->SelectionPos;
		}

		GuiTextBoxClearSelection(Widget);
	}
}

////////////////////////////////////////////////////////////
// Set the selection
////////////////////////////////////////////////////////////
void GuiTextBoxSetSelection(struct GuiWidget * Widget, int32 Position, bool Released)
{
	int32 Pos = (Position - Widget->X + IndexGetGrh(GUI_TEXTBOX_VBORDER)->PixelWidth) / Widget->FontSize;

	if (Pos < 0)
	{
		Pos = 0;
	}
	else if (Pos > (int32)Widget->Length)
	{
		Pos = Widget->Length;
	}

	if (Pos != Widget->Attributes.TextBox.CurrentPos)
	{
		if (Released && Pos > (int32)Widget->Attributes.TextBox.CurrentPos)
		{
			Widget->Attributes.TextBox.SelectionPos = Widget->Attributes.TextBox.CurrentPos;
			Widget->Attributes.TextBox.CurrentPos = Pos;
		}
		else
		{
			Widget->Attributes.TextBox.SelectionPos = Pos;
		}
	}
	else
	{
		if (Released)
			GuiTextBoxClearSelection(Widget);
	}
}

////////////////////////////////////////////////////////////
// Copy the textbox selection
////////////////////////////////////////////////////////////
void GuiTextBoxCopySelection(struct GuiWidget * Widget)
{
	if (Widget->Attributes.TextBox.SelectionPos == GUI_TEXTBOX_NOT_SELECTED)
		return;

	// Copy it
	GuiClipboardSet(&Widget->Text[Widget->Attributes.TextBox.SelectionPos], Widget->Attributes.TextBox.CurrentPos - Widget->Attributes.TextBox.SelectionPos);
}

////////////////////////////////////////////////////////////
// Cut the textbox selection
////////////////////////////////////////////////////////////
void GuiTextBoxCutSelection(struct GuiWidget * Widget)
{
	GuiTextBoxCopySelection(Widget);
	GuiTextBoxEraseSelection(Widget);
}

////////////////////////////////////////////////////////////
// Paste the textbox copy
////////////////////////////////////////////////////////////
void GuiTextBoxPasteSelection(struct GuiWidget * Widget)
{
	uint32 Length = strlen(&GuiClipboardGet()[0]);

	if ((Length + Widget->Length) > GUI_TEXTBOX_LENGTH)
	{
		Length -= ((Length + Widget->Length) - GUI_TEXTBOX_LENGTH);
	}
	
	MemoryCopy(&Widget->Text[Widget->Attributes.TextBox.CurrentPos + Length], &Widget->Text[Widget->Attributes.TextBox.CurrentPos], Length);
	MemoryCopy(&Widget->Text[Widget->Attributes.TextBox.CurrentPos], &GuiClipboardGet()[0], Length);

	Widget->Attributes.TextBox.CurrentPos += Length;	
}

////////////////////////////////////////////////////////////
// Add character
////////////////////////////////////////////////////////////
void GuiTextBoxAddChar(struct GuiWidget * Widget, int8 Char)
{
	struct GuiTextBox * TextBox = &Widget->Attributes.TextBox;

	if (Widget->Length < GUI_TEXTBOX_LENGTH)
	{
		if (TextBox->CurrentPos != Widget->Length)
		{
			uint32 i;
			for (i = (Widget->Length - 1); i > TextBox->CurrentPos; i--)
			{
				Widget->Text[i+1] = Widget->Text[i];
			}
		}

		Widget->Text[TextBox->CurrentPos] = Char;
		Widget->Length++;
		Widget->Text[Widget->Length] = NULLCHAR;
		TextBox->CurrentPos++;

		if ( ((TextBox->CurrentPos * (Widget->FontSize - 1))) > (uint32)Widget->Width )
			TextBox->StartOffset++;
	}
}

////////////////////////////////////////////////////////////
// Remove character
////////////////////////////////////////////////////////////
void GuiTextBoxRemoveChar(struct GuiWidget * Widget)
{
	struct GuiTextBox * TextBox = &Widget->Attributes.TextBox;

	if (TextBox->CurrentPos <= 0)
		return;

	if (TextBox->SelectionPos != GUI_TEXTBOX_NOT_SELECTED)
	{
		uint32 Size = TextBox->CurrentPos - TextBox->SelectionPos;
		MemoryCopy(&Widget->Text[TextBox->SelectionPos], &Widget->Text[TextBox->CurrentPos], Size);
		MemorySet(&Widget->Text[Widget->Length - Size], NULLCHAR, Size);

		TextBox->CurrentPos = TextBox->SelectionPos;
		GuiTextBoxClearSelection(Widget);
		Widget->Length -= Size;

		// Start Offset?
	}
	else
	{
		if (TextBox->CurrentPos != Widget->Length)
		{
			uint32 i;

			for (i = TextBox->CurrentPos; i < Widget->Length; i++)
			{
				Widget->Text[i] = Widget->Text[i+1];
			}
		}

		TextBox->CurrentPos--;
		Widget->Length--;
		Widget->Text[Widget->Length] = NULLCHAR;

		if (TextBox->StartOffset > 0)
			TextBox->StartOffset--;
	}
}

////////////////////////////////////////////////////////////
// Set current position
////////////////////////////////////////////////////////////
void GuiTextBoxSetCurrentPos(struct GuiWidget * Widget, int32 Position)
{
    int32 Pos = (Position - Widget->X + IndexGetGrh(GUI_TEXTBOX_VBORDER)->PixelWidth) / Widget->FontSize;

	if (Pos >= 0 && Pos <= (int32)Widget->Length)
        Widget->Attributes.TextBox.CurrentPos = Pos;
}

////////////////////////////////////////////////////////////
// Render a textbox
////////////////////////////////////////////////////////////
void GuiTextBoxRender(struct GuiWidget * Form, struct GuiWidget * Widget, struct Color4ub * BackgroundColor, struct Color4ub * FontColor, bool Focused)
{
	struct GuiTextBox * TextBox = &Widget->Attributes.TextBox;
	char				Temp[GUI_TEXTBOX_LENGTH];

	// Render the background repeating it
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_BACKGROUND,
								Form->X + Widget->X, Form->Y + Widget->Y,
								Form->X + Widget->X + Widget->Width + 2, Form->Y + Widget->Y + Widget->Height,
								BackgroundColor);

	// Render the borders repeating it
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_VBORDER,
								Form->X + Widget->X - IndexGetGrh(GUI_TEXTBOX_VBORDER)->PixelWidth, Form->Y + Widget->Y,
								Form->X + Widget->X, Form->Y + Widget->Y + Widget->Height,
								BackgroundColor);
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_VBORDER + 0x01,
								Form->X + Widget->X + Widget->Width + 2, Form->Y + Widget->Y,
								Form->X + Widget->X + Widget->Width + IndexGetGrh(GUI_TEXTBOX_VBORDER + 0x01)->PixelWidth + 2, Form->Y + Widget->Y + Widget->Height,
								BackgroundColor);
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_HBORDER,
								Form->X + Widget->X, Form->Y + Widget->Y - IndexGetGrh(GUI_TEXTBOX_HBORDER)->PixelHeight,
								Form->X + Widget->X + Widget->Width + 2, Form->Y + Widget->Y,
								BackgroundColor);
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_HBORDER + 0x01,
								Form->X + Widget->X, Form->Y + Widget->Y + Widget->Height,
								Form->X + Widget->X + Widget->Width + 2, Form->Y + Widget->Y + Widget->Height + IndexGetGrh(GUI_TEXTBOX_HBORDER + 0x01)->PixelHeight,
								BackgroundColor);

	// Render the edges
	TextureRenderGrhColor(GUI_TEXTBOX_EDGE, Form->X + Widget->X - IndexGetGrh(GUI_TEXTBOX_EDGE)->PixelWidth, Form->Y + Widget->Y - IndexGetGrh(GUI_TEXTBOX_EDGE)->PixelHeight, BackgroundColor);
	TextureRenderGrhColor(GUI_TEXTBOX_EDGE + 0x01, Form->X + Widget->X + Widget->Width + 2, Form->Y + Widget->Y - IndexGetGrh(GUI_TEXTBOX_EDGE + 0x01)->PixelHeight, BackgroundColor);
	TextureRenderGrhColor(GUI_TEXTBOX_EDGE + 0x02, Form->X + Widget->X - IndexGetGrh(GUI_TEXTBOX_EDGE + 0x02)->PixelWidth, Form->Y + Widget->Y + Widget->Height, BackgroundColor);
	TextureRenderGrhColor(GUI_TEXTBOX_EDGE + 0x03, Form->X + Widget->X + Widget->Width + 2, Form->Y + Widget->Y + Widget->Height, BackgroundColor);

	if (Widget->Text[0] != NULLCHAR)
	{
		strcpy(&Temp[0], &Widget->Text[TextBox->StartOffset]);

		// Render the selection
		if (Widget->Enabled && (TextBox->SelectionPos >= 0))
		{
			glColor4ub(BackgroundColor->r, 0, BackgroundColor->b, 127);

			glRecti(Form->X + Widget->X + ((TextBox->SelectionPos) * (Widget->FontSize - 1)) + 2,
					Form->Y + Widget->Y,
					Form->X + Widget->X + ((TextBox->CurrentPos) * (Widget->FontSize - 1)) + 2,
					Form->Y + Widget->Y + Widget->FontSize + 2);
		}

		glColor4ub(FontColor->r, FontColor->g, FontColor->b, 255);

		if (!Widget->Attributes.TextBox.HideText)
		{
			FontRenderTextColor((float)(Widget->X + Form->X + 1),
								(float)(Widget->Y + Form->Y + 3), Temp, FontColor);
		}
		else
		{
			char * TextHide = (char*)MemoryAllocate(Widget->Length + 1);
			MemorySet(&TextHide[0], '*', (Widget->Length - TextBox->StartOffset));
			TextHide[Widget->Length] = NULLCHAR;
			FontRenderTextColor((float)(Widget->X + Form->X + 1),
								(float)(Widget->Y + Form->Y + 3), TextHide, FontColor);
			MemoryDeallocate(TextHide);
		}

		if (Widget->Enabled && Focused)
		{
			glBegin(GL_LINES);
				glVertex2i(Form->X + Widget->X + ((TextBox->CurrentPos - TextBox->StartOffset) * (Widget->FontSize - 1)), Form->Y + Widget->Y);
				glVertex2i(Form->X + Widget->X + ((TextBox->CurrentPos - TextBox->StartOffset) * (Widget->FontSize - 1)), Form->Y + Widget->Y + Widget->FontSize + 2);
			glEnd();
		}
	}
}
