/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/Cache.h>
#include <Graphics/Device.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define CacheAvailable(Cache)			((Cache)->Size - ((uint8 *)(Cache)->Rover - (uint8 *)(Cache)->Base))
#define CacheFatalError(Cache, Error)	do { /* ... */ } while (0)
#define CacheDebugMsg(Message, ...)		do { /*printf(Message, __VA_ARGS__);*/ } while (0)

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Allocate a memory block referenced to the cache object
////////////////////////////////////////////////////////////
struct CacheObjectType * CacheAllocateImpl(struct CacheType * Cache, uinteger Size, struct CacheObjectType ** Owner);

////////////////////////////////////////////////////////////
/// Create the cache
////////////////////////////////////////////////////////////
struct CacheType * CacheCreate(uinteger Size, bool Fast)
{
	struct CacheType * Cache = (struct CacheType *)MemoryAllocate(sizeof(struct CacheType));

	if (Cache)
	{
		CacheInitialize(Cache, Size, Fast);

		return Cache;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Initialize the cache
////////////////////////////////////////////////////////////
void CacheInitialize(struct CacheType * Cache, uinteger Size, bool Fast)
{
	if (Cache)
	{
		Cache->Fast = Fast;

		if (Cache->Fast && Cache->FastBase && DeviceGet()->Info.Support.VARFence)
		{
			#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
				DeviceGetExt()->wglFreeMemoryNV(Cache->FastBase);
			#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD
				DeviceGetExt()->glXFreeMemoryNV(Cache->FastBase);
			#endif
		}

		Cache->FastBase = NULL;

		if (Cache->Base)
		{
			CacheClear(Cache);
			MemoryDeallocate(Cache->Base);
		}

		Cache->Base = NULL;
		Cache->Rover = NULL;
		Cache->Size = 0;
		Cache->Thrashed = false;
		Cache->Wrapped = false;

		if (Size > 0)
		{
			Cache->Base = (struct CacheObjectType *)MemoryAllocate(Size);

			if (Cache->Base)
			{
				Cache->Size = Size;
				Cache->Rover = Cache->Base;
				Cache->Base->Next = NULL;
				Cache->Base->Owner = NULL;
				Cache->Base->Size = Cache->Size;

				if (Cache->Fast && DeviceGetInfo()->Support.VARFence)
				{
					#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
						Cache->FastBase = (uint8*)DeviceGetExt()->wglAllocateMemoryNV(Size, 0.0f, 0.0f, 1.0f);
					#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD
						Cache->FastBase = (uint8*)DeviceGetExt()->glXAllocateMemoryNV(Size, 0.0f, 0.0f, 1.0f);
					#endif

					if (!Cache->FastBase)
					{
						Cache->Fast = false;
					}
				}
				else
				{
					Cache->Fast = false;
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Clear memory in the cache
////////////////////////////////////////////////////////////
void CacheClear(struct CacheType * Cache)
{
	if (Cache && Cache->Base)
	{
		struct CacheObjectType * Object;

		for (Object = Cache->Base; Object != NULL; Object = Object->Next)
		{
			if (Cache->Fast && DeviceGet()->Info.Support.VARFence)
			{
				if (!DeviceGetExt()->glTestFenceNV(Object->Fence))
				{
					DeviceGetExt()->glFinishFenceNV(Object->Fence);
				}

				DeviceGetExt()->glDeleteFencesNV(1, &Object->Fence);
			}

			Object->Fence = INTEGER_SUFFIX(0);

			if (Object->Owner)
			{
				*Object->Owner = NULL;
			}
		}

		Cache->Rover = Cache->Base;
		Cache->Base->Next = NULL;
		Cache->Base->Owner = NULL;
		Cache->Base->Size = Cache->Size;
	}
}

////////////////////////////////////////////////////////////
/// Reset thrash data in the cache
////////////////////////////////////////////////////////////
void CacheReset(struct CacheType * Cache)
{
	Cache->Wrapped = false;
	Cache->Thrashed = false;
	Cache->Thrash = Cache->Rover;
}

////////////////////////////////////////////////////////////
/// Allocate a memory block referenced to the cache object
////////////////////////////////////////////////////////////
struct CacheObjectType * CacheAllocateImpl(struct CacheType * Cache, uinteger Size, struct CacheObjectType ** Owner)
{
	bool CurrentWrapped = false;

	struct CacheObjectType * Object = NULL;

	Size += sizeof(struct CacheObjectType);

	if (Size > Cache->Size)
	{
		CacheFatalError(Cache, "Cache allocation error");
		return NULL;
	}

	if (!Cache->Rover)
	{
		Cache->Rover = Cache->Base;
	}
	else if (CacheAvailable(Cache) < Size)
	{
		CurrentWrapped = true;
		Cache->Rover = Cache->Base;
	}

	Object = Cache->Rover;

	if (Cache->Fast && DeviceGet()->Info.Support.VARFence)
	{
		if (!DeviceGetExt()->glTestFenceNV(Cache->Rover->Fence))
		{
			DeviceGetExt()->glFinishFenceNV(Cache->Rover->Fence);
		}

		DeviceGetExt()->glDeleteFencesNV(1, &Cache->Rover->Fence);
	}

	if (Cache->Rover->Owner != NULL)
	{
		*(Cache->Rover->Owner) = NULL;
	}

	while (Object->Size < Size)
	{
		Cache->Rover = Cache->Rover->Next;

		if (Cache->Fast && DeviceGet()->Info.Support.VARFence)
		{
			if (!DeviceGetExt()->glTestFenceNV(Cache->Rover->Fence))
			{
				DeviceGetExt()->glFinishFenceNV(Cache->Rover->Fence);
			}

			DeviceGetExt()->glDeleteFencesNV(1, &Cache->Rover->Fence);
		}

		if (Cache->Rover->Owner)
		{
			*(Cache->Rover->Owner) = NULL;
		}

		Object->Size += Cache->Rover->Size;
		Object->Next = Cache->Rover->Next;
	}

	if (Object->Size - Size > CACHE_FRAGMENT_SIZE)
	{
		Cache->Rover = (struct CacheObjectType *)(((uint8 *)Object) + Size);
		Cache->Rover->Size = Object->Size - Size;
		Cache->Rover->Next = Object->Next;
		Cache->Rover->Owner = NULL;
		Object->Next = Cache->Rover;
		Object->Size = Size;
	}
	else
	{
		Cache->Rover = Object->Next;
	}

	*Owner = Object;
	Object->Owner = Owner;

	if (Cache->Fast && DeviceGet()->Info.Support.VARFence)
	{
		DeviceGetExt()->glGenFencesNV(1, &Object->Fence);
	}

	if (Cache->Wrapped)
	{
		if (CurrentWrapped || (Cache->Rover >= Cache->Thrash))
		{
			Cache->Thrashed = true;
		}
	}
	else if (CurrentWrapped)
	{
		Cache->Wrapped = true;
	}

	if (Cache->Fast)
	{
		Object->Data = (((uint8 *)(Object + 1)) - (uint8 *)Cache->Base) + Cache->FastBase;
	}
	else
	{
		Object->Data = (uint8 *)(Object + 1);
	}

	// CacheDebug(Cache);

	return Object;
}

////////////////////////////////////////////////////////////
/// Allocate a memory block referenced to the cache
////////////////////////////////////////////////////////////
void * CacheAllocate(struct CacheType * Cache, uinteger Size, struct CacheObjectType ** Owner)
{
	struct CacheObjectType * Object = CacheAllocateImpl(Cache, Size, Owner);

	if (Object)
	{
		return (void *)Object->Data;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Check if a cache is thrashed
////////////////////////////////////////////////////////////
bool CacheThrashed(struct CacheType * Cache)
{
	return Cache->Thrashed;
}

////////////////////////////////////////////////////////////
/// Debug the cache status
////////////////////////////////////////////////////////////
void CacheDebug(struct CacheType * Cache)
{
	struct CacheObjectType * Object;

	for (Object = Cache->Base; Object != NULL; Object = Object->Next)
	{
		if (Object == Cache->Rover)
		{
			CacheDebugMsg("Rover:\n");
		}

		CacheDebugMsg("%p : %i bytes\n", Object, Object->Size);
	}
}

////////////////////////////////////////////////////////////
/// Destroy the cache
////////////////////////////////////////////////////////////
void CacheDestroy(struct CacheType * Cache)
{
	if (Cache->Fast && Cache->FastBase && DeviceGet()->Info.Support.VARFence)
	{
		#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
			DeviceGetExt()->wglFreeMemoryNV(Cache->FastBase);
		#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD
			DeviceGetExt()->glXFreeMemoryNV(Cache->FastBase);
		#endif
	}

	if (Cache->Base)
	{
		MemoryDeallocate(Cache->Base);
	}

	Cache->Fast = false;
	Cache->Rover = NULL;
	Cache->Thrash = NULL;
	Cache->FastBase = NULL;
	Cache->Thrashed = false;
	Cache->Wrapped = false;
	Cache->Size = 0;
}
