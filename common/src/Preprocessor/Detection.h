/////////////////////////////////////////////////////////////////////////////
//	Preprocessor Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A cross-platform macro based library to give support
//	for preprocessor metaprogramming.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PREPROCESSOR_DETECTION_H
#define PREPROCESSOR_DETECTION_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Preprocessor/Arguments.h>

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define PREPROCESSOR_DETECT(...) PREPROCESSOR_ARGS_SECOND(__VA_ARGS__, 0, )
#define PREPROCESSOR_DETECT_TOKEN(Token) Token, 1

#define PREPROCESSOR_DETECT_PARENTHESIS_IMPL(...) PREPROCESSOR_DETECT_TOKEN(~)
#define PREPROCESSOR_DETECT_PARENTHESIS(Expr) PREPROCESSOR_DETECT(PREPROCESSOR_DETECT_PARENTHESIS_IMPL Expr)

#define PREPROCESSOR_DETECT_COMPARABLE(Expr) PREPROCESSOR_DETECT_PARENTHESIS(PREPROCESSOR_CONCAT(PREPROCESSOR_COMPARE_, Expr) (()) )

#endif // PREPROCESSOR_DETECTION_H
