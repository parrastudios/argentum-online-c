/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Chat.h>

#include <Game/Language.h>
#include <Game/User.h>

#include <GUI/WindowManager.h>
#include <GUI/EventHandler.h>

#include <Network/Protocol.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct GuiWindow * ChatWindow;
struct GuiWidget * ChatTextBoxInput;
struct GuiWidget * ChatConsole;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Handle chat routines (as old Argentum behavior)
////////////////////////////////////////////////////////////
void ChatWriteRoutine()
{
	if (ChatTextBoxInput->Visible)
	{
		// Make textbox invisible
		GuiWindowShowWidget(ChatWindow, ChatTextBoxInput, false);

		if (ChatTextBoxInput->Text[0] != NULLCHAR)
		{
			// Send text
			PacketSendChatPublic(ChatTextBoxInput->Text, ChatTextBoxInput->Length);

			// Clear text
			GuiTextBoxErase(ChatTextBoxInput);
		}
	}
	else
	{
		// Clear text
		GuiTextBoxClear(ChatTextBoxInput);

		// Make textbox visible
		GuiWindowShowWidget(ChatWindow, ChatTextBoxInput, true);
	}
}

////////////////////////////////////////////////////////////
/// Register the chat
////////////////////////////////////////////////////////////
void ChatRegister()
{
	// Chat window
	GuiEventHandlerAdd("ChatFormEnter_KeyReleased", (void*)&ChatFormEnter_KeyReleased);
	GuiEventHandlerAdd("ChatInputSend_ButtonReleased", (void*)&ChatInputSend_ButtonReleased);
}

////////////////////////////////////////////////////////////
/// Initialize the chat
////////////////////////////////////////////////////////////
void ChatInitialize()
{
	// Obtain window references
	ChatWindow = GuiGetWindow("frmChat");
	
	// Obtain widget references
	ChatTextBoxInput = GuiWindowGetWidget(ChatWindow, "txtChatInput");
	ChatConsole = GuiWindowGetWidget(ChatWindow, "cnsChat");
}

////////////////////////////////////////////////////////////
/// Send input released event
////////////////////////////////////////////////////////////
void ChatInputSend_ButtonReleased(struct MouseButtonEvent * ButtonEvent)
{
	ChatWriteRoutine();
}

////////////////////////////////////////////////////////////
/// Chat form enter key released event
////////////////////////////////////////////////////////////
void ChatFormEnter_KeyReleased(struct KeyEvent * Key)
{
	if (Key->Code == Return)
	{
		ChatWriteRoutine();
	}
}

////////////////////////////////////////////////////////////
/// Open the chat
////////////////////////////////////////////////////////////
void ChatOpen()
{
	if (ChatWindow->hStatus == GUI_WINDOW_CLOSED)
	{
		// Open Chat
		GuiOpenWindow(ChatWindow);

	
	}
}

////////////////////////////////////////////////////////////
/// Close the chat
////////////////////////////////////////////////////////////
void ChatClose()
{
	if (ChatWindow->hStatus == GUI_WINDOW_OPENED)
	{
		// Close Chat
		GuiCloseWindow(ChatWindow);
	}
}

////////////////////////////////////////////////////////////
/// Insert text into console
////////////////////////////////////////////////////////////
void ChatAddLine(char * Text)
{
	GuiConsoleAddLine(ChatConsole, Text);
}

////////////////////////////////////////////////////////////
/// Renders the chat
////////////////////////////////////////////////////////////
void ChatRender()
{
	// nothing here
}
