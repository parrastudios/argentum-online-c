/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_ENTITY_FACTORY_H
#define WORLD_ENTITY_FACTORY_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Preprocessor/For.h>

#include <DataType/List.h>
#include <DataType/HashMap.h>

#include <World/Component.h>
#include <World/Entity.h>
#include <World/EntityIndex.h>

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define EntityFactoryComponentCallback(Callback) \
	Callback();

#define EntityFactoryComponentCreate(Factory, Type, Size, ...) \
	do \
	{ \
		if (EntityFactoryCreateComponents(Factory, Type, Size())) \
		{ \
			PREPROCESSOR_IF(PREPROCESSOR_ARGS_NOT_EMPTY(__VA_ARGS__), \
				PREPROCESSOR_FOR_EACH(EntityFactoryComponentCallback, PREPROCESSOR_TUPLE_EXPAND(__VA_ARGS__)), \
				PREPROCESSOR_EMPTY_SYMBOL()) \
		} \
		else \
		{ \
			return false; \
		} \
	} while (0)

#define EntityFactoryComponentCreateImpl(Factory, Type, Size, ...) \
	EntityFactoryComponentCreate(Factory, Type, Size, __VA_ARGS__);

#define EntityFactoryComponentCreateDefineImpl(Factory, BuildType, DataType, Size, ...) \
	PREPROCESSOR_IF(WORLD_BUILD_TYPE_IS(BuildType), \
		EntityFactoryComponentCreateImpl(Factory, DataType, Size, PREPROCESSOR_ARGS_FIRST_OR_EMPTY(__VA_ARGS__)),\
		PREPROCESSOR_EMPTY_SYMBOL() \
	)

#define EntityFactoryComponentCreateDefine(Factory, ...) \
	do \
	{ \
		PREPROCESSOR_FOR_EACH_TUPLE_PREPEND(EntityFactoryComponentCreateDefineImpl, Factory, __VA_ARGS__) \
	} while (0)

#define EntityFactoryComponentDestroy(Factory, DataType, ...) \
	do \
	{ \
		EntityFactoryDestroyComponents(Factory, DataType); \
		{ \
			PREPROCESSOR_IF(PREPROCESSOR_ARGS_NOT_EMPTY(__VA_ARGS__), \
				PREPROCESSOR_FOR_EACH(EntityFactoryComponentCallback, PREPROCESSOR_TUPLE_EXPAND(__VA_ARGS__)), \
				PREPROCESSOR_EMPTY_SYMBOL()) \
		} \
	} while (0)

#define EntityFactoryComponentDestroyImpl(Factory, DataType, ...) \
	EntityFactoryComponentDestroy(Factory, DataType, __VA_ARGS__);

#define EntityFactoryComponentDestroyDefineImpl(Factory, BuildType, DataType, Size, ...) \
	PREPROCESSOR_IF(WORLD_BUILD_TYPE_IS(BuildType), \
		EntityFactoryComponentDestroyImpl(Factory, DataType, PREPROCESSOR_ARGS_SECOND_OR_EMPTY(__VA_ARGS__)), \
		PREPROCESSOR_EMPTY_SYMBOL() \
	)

#define EntityFactoryComponentDestroyDefine(Factory, ...) \
	do \
	{ \
		PREPROCESSOR_FOR_EACH_TUPLE_PREPEND(EntityFactoryComponentDestroyDefineImpl, Factory, __VA_ARGS__) \
	} while (0)

#define EntityFactorySetForEach(Type, Iterator) \
		SetForEach(EntityFactoryList(Type)->EntityPool, Iterator)

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct EntityFactoryType;

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef struct EntityFactoryType * (*EntityFactorySingleton)();

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct EntityFactoryType
{
	uinteger			Type;									//< Entity factory type
	struct EntityType *	(*Create)(struct EntityType * Entity);	//< Attaches all components to an entity
	void				(*Destroy)(struct EntityType * Entity);	//< Deataches all components of an entity
	bool				(*InitializeImpl)();					//< Initialize pool of vectors of entity entity factory
	void				(*DestroyImpl)();						//< Destroy pool of vectors of entity factory
	Set					EntityPool;								//< Container holding entity data
	List				EntityFreeIds;							//< Container holding free ids
	HashMap				ComponentMap;							//< Holds arrays of components (pool) sorted by data type
	uinteger			ComponentDataMask;						//< Holds the mask of component data types
	ComponentTypesImpl	ComponentTypes;							//< Pointer to derived component types
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return entity factory by type
////////////////////////////////////////////////////////////
struct EntityFactoryType * EntityFactoryList(uinteger Type);

////////////////////////////////////////////////////////////
/// Initialize a factory
////////////////////////////////////////////////////////////
void EntityFactoryInitialize(struct EntityFactoryType * EntityFactory);

////////////////////////////////////////////////////////////
/// Clear all entities from the factory
////////////////////////////////////////////////////////////
void EntityFactoryClear(struct EntityFactoryType * EntityFactory);

////////////////////////////////////////////////////////////
/// Destroy a factory from memory
////////////////////////////////////////////////////////////
void EntityFactoryDestroy(struct EntityFactoryType * EntityFactory);

////////////////////////////////////////////////////////////
/// Create a component vector into a factory
////////////////////////////////////////////////////////////
bool EntityFactoryCreateComponents(struct EntityFactoryType * EntityFactory, uinteger DataType, uinteger DerivedSize);

////////////////////////////////////////////////////////////
/// Link entity component map to each factory components
////////////////////////////////////////////////////////////
void EntityFactoryBindComponents(struct EntityFactoryType * EntityFactory, struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Get component from entity factory
////////////////////////////////////////////////////////////
struct ComponentType * EntityFactoryGetComponent(struct EntityFactoryType * EntityFactory, uinteger DataType, ComponentIdType ComponentId);

////////////////////////////////////////////////////////////
/// Destroy component vector from a factory
////////////////////////////////////////////////////////////
void EntityFactoryDestroyComponents(struct EntityFactoryType * EntityFactory, uinteger DataType);

////////////////////////////////////////////////////////////
/// Create an entity in a factory
////////////////////////////////////////////////////////////
struct EntityType * EntityFactoryCreateEntity(struct EntityFactoryType * EntityFactory);

////////////////////////////////////////////////////////////
/// Destroy an entity from a factory
////////////////////////////////////////////////////////////
bool EntityFactoryDestroyEntity(struct EntityType * Entity);

#endif // WORLD_ENTITY_FACTORY_H
