/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_THREAD_H
#define SYSTEM_THREAD_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT

	#define WIN32_LEAN_AND_MEAN
    #include <windows.h>

#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_MACOS || PLATFORM_TYPE == PLATFORM_BSD

    #include <pthread.h>

#endif

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////

typedef void (*FuncType)(void *);

#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT

	typedef struct {
		HANDLE		Handle;		///< Win32 thread handle
		FuncType	Function;	///< Function to call as the thread entry point
		void *		Data;		///< Data to pass to the thread function
	} Thread;

#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_MACOS || PLATFORM_TYPE == PLATFORM_BSD

	typedef struct {
		pthread_t	pThread;	///< Unix thread instance
		bool		IsActive;	///< Thread state (active or inactive)
		FuncType	Function;	///< Function to call as the thread entry point
		void *		Data;		///< Data to pass to the thread function
	} Thread;

#endif

////////////////////////////////////////////////////////////
/// Create and run the thread
///
////////////////////////////////////////////////////////////
void ThreadLaunch(Thread * ThreadEx, FuncType Function, void * UserData);

////////////////////////////////////////////////////////////
/// Wait until the thread finishes
///
////////////////////////////////////////////////////////////
void ThreadWait(Thread * ThreadEx);

////////////////////////////////////////////////////////////
/// Terminate the thread
/// Terminating a thread with this function is not safe,
/// you should rather try to make the thread function
/// terminate by itself
///
////////////////////////////////////////////////////////////
void ThreadTerminate(Thread * ThreadEx);

#endif // SYSTEM_THREAD_H
