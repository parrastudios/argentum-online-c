/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Character.h>
#include <DataBase/Character.h>

////////////////////////////////////////////////////////////
/// Initialize the character
////////////////////////////////////////////////////////////
void CharacterInitialize(struct CharacterData * Character, uint32 Type)
{
	
}

////////////////////////////////////////////////////////////
/// Create the character
////////////////////////////////////////////////////////////
struct CharacterData * CharacterCreate(uint32 Type)
{
	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy the character
////////////////////////////////////////////////////////////
void CharacterDestroy(struct CharacterData * Character)
{

}

////////////////////////////////////////////////////////////
/// Get size of a character
////////////////////////////////////////////////////////////
uinteger CharacterGetSize(uinteger Type)
{
	if (Type == CHARACTER_TYPE_PLAYER || Type == CHARACTER_TYPE_NPC)
	{
		return (sizeof(struct CharacterData));
	}
	else if (Type == CHARACTER_TYPE_PLAYER_DB || Type == CHARACTER_TYPE_NPC_DB)
	{
		return (sizeof(struct DBCharacterData));
	}

	return 0;
}

////////////////////////////////////////////////////////////
/// Convert gamelogic character to database character
////////////////////////////////////////////////////////////
void CharacterToDBCharacter(struct CharacterData * Character, struct DBCharacterData * DBCharacter, uinteger Type)
{
	// todo

	DBCharacter->Id = Character->Index;

	DBCharacter->Status.Head = Character->Aparence.Head;
	DBCharacter->Status.MaxCombatDefense = 0; //todo
	DBCharacter->Status.MinCombatDefense = 0; //todo
	DBCharacter->Status.MaxHit = 0; // todo
	DBCharacter->Status.MinHit = 0; //todo
	DBCharacter->Status.MaxMagicDefense = 0; //todo
	DBCharacter->Status.MinMagicDefense = 0; //todo

	if (Type == CHARACTER_TYPE_PLAYER)
	{
		DBCharacter->Attributes.Player.Id = Character->Index;
		strcpy(DBCharacter->Attributes.Player.Name, Character->Attributes.Player.Name);


		/*
	uint32	Id;
	char	Name[DB_PLAYER_NAME_MAX_SIZE];
	uint32	AccountId;
	bool	Active;
	char	Desc[DB_PLAYER_DESC_MAX_SIZE];
	bool	Ban;
	
	// Old table (todo: improve design..)
	uint32	Class;
	uint32	Gender;
	uint32	Race;
	uint32	Home;
	uint32	Head;
	uint32	Body;

	uint32	Strength;
	uint32	Agility;
	uint32	Inteligence;
	uint32	Charisma;
	uint32	Constitution;

	uint32	Level;
	uint32	Exp;
	uint32	Gold;

	uint32	Weapon;
	uint32	Shield;
	uint32	Helmet;
	uint32	Footwear;

	uint32	MaxHp,	MinHp;
	uint32	MaxSta, MinSta;
	uint32	MaxMan, MinMan;
	uint32	MaxHun, MinHun;
	uint32	MaxThi, MinThi;
		*/

		DBCharacter->Status.Level = Character->Attributes.Player.Stats.Level;
		DBCharacter->Status.Experience = Character->Attributes.Player.Stats.Exp;

		DBCharacter->Status.MaxHp = Character->Attributes.Player.Stats.Hp[PLAYER_STATS_MAX];
		DBCharacter->Status.MinHp = Character->Attributes.Player.Stats.Hp[PLAYER_STATS_MIN];
		DBCharacter->Status.MaxMana = Character->Attributes.Player.Stats.Mana[PLAYER_STATS_MAX];
		DBCharacter->Status.MinMana = Character->Attributes.Player.Stats.Mana[PLAYER_STATS_MIN];

		DBCharacter->Status.Paralized = Character->Attributes.Player.Flags.Paralized;
		DBCharacter->Status.Poisoned = Character->Attributes.Player.Flags.Poisoned;
		DBCharacter->Status.Magic = false; // to remove
		
		DBCharacter->EquipedItems.Armor = Character->Attributes.Player.Inventory.ArmorEqpIndex;
		DBCharacter->EquipedItems.Footwear = Character->Attributes.Player.Inventory.FootwearEqpIndex;
		DBCharacter->EquipedItems.Helmet = Character->Attributes.Player.Inventory.HelmetEqpIndex;
		DBCharacter->EquipedItems.Shield = Character->Attributes.Player.Inventory.ShieldEqpIndex;
		DBCharacter->EquipedItems.Weapon = Character->Attributes.Player.Inventory.WeaponEqpIndex;
	}
	else if (Type == CHARACTER_TYPE_NPC)
	{
		// todo

		/*
		DBCharacter->Attributes.Npc = Character->Attributes.Npc.Flags.Magic 
		Character->Attributes.Npc.Flags.ParalysisResistant
		Character->Attributes.Npc.Stats.Defense
		Character->Attributes.Npc.Stats.Hit
		Character->Attributes.Npc.Stats.Hp
		Character->Attributes.Npc.Stats.Level
		Character->Attributes.Npc.Stats.MagicDefense
		Character->Attributes.Npc.Stats.Paralized
		*/

	}
}

////////////////////////////////////////////////////////////
/// Convert database character to gamelogic character
////////////////////////////////////////////////////////////
void DBCharacterToCharacter(struct DBCharacterData * DBCharacter, struct CharacterData * Character, uinteger Type)
{
	// todo

	Character->Index = DBCharacter->Id;
	Character->Algin = DBCharacter->Algin;
	Character->World = DBCharacter->Attributes.Player.World; // duplicated


	// Character->Aparence.Head = DBCharacter->Status.Head;
	// x = DBCharacter->Status.MaxCombatDefense; //todo
	// x = DBCharacter->Status.MinCombatDefense; //todo
	// x = DBCharacter->Status.MaxHit; // todo
	// x = DBCharacter->Status.MinHit; //todo
	// x = DBCharacter->Status.MaxMagicDefense; //todo
	// x = DBCharacter->Status.MinMagicDefense; //todo

	if (Type == CHARACTER_TYPE_PLAYER)
	{
/*
		QueryGetField(Row, DB_PLAYER_FIELD_NAME, QUERY_FIELD_TYPE_STR, Character->Attributes.Player.Name);
		QueryGetField(Row, DB_PLAYER_FIELD_ACCOUNT_ID, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.AccountId);
		QueryGetField(Row, DB_PLAYER_FIELD_ACTIVE, QUERY_FIELD_TYPE_BOOL, &Character->Attributes.Player.Active);
		QueryGetField(Row, DB_PLAYER_FIELD_BAN, QUERY_FIELD_TYPE_BOOL, &Character->Attributes.Player.Ban);

		QueryGetField(Row, DB_PLAYER_FIELD_CLASS, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Class);
		QueryGetField(Row, DB_PLAYER_FIELD_GENDER, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Gender);
		QueryGetField(Row, DB_PLAYER_FIELD_CLASS, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Class);
		QueryGetField(Row, DB_PLAYER_FIELD_RACE, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Race);

		QueryGetField(Row, DB_PLAYER_FIELD_HOME, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Home);

		QueryGetField(Row, DB_PLAYER_FIELD_HEAD, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Head);
		QueryGetField(Row, DB_PLAYER_FIELD_BODY, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Body);

		QueryGetField(Row, DB_PLAYER_FIELD_STRENGTH, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Strength);
		QueryGetField(Row, DB_PLAYER_FIELD_AGILITY, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Agility);
		QueryGetField(Row, DB_PLAYER_FIELD_INTELIGENCE, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Inteligence);
		QueryGetField(Row, DB_PLAYER_FIELD_CHARISMA, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Charisma);
		QueryGetField(Row, DB_PLAYER_FIELD_CONSTITUTION, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Constitution);

		QueryGetField(Row, DB_PLAYER_FIELD_LEVEL, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Level);
		QueryGetField(Row, DB_PLAYER_FIELD_EXP, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Exp);
		QueryGetField(Row, DB_PLAYER_FIELD_GOLD, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Gold);

		QueryGetField(Row, DB_PLAYER_FIELD_WEAPON, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Weapon);
		QueryGetField(Row, DB_PLAYER_FIELD_SHIELD, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Shield);
		QueryGetField(Row, DB_PLAYER_FIELD_HELMET, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Helmet);
		QueryGetField(Row, DB_PLAYER_FIELD_FOOTWEAR, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Footwear);

		QueryGetField(Row, DB_PLAYER_FIELD_MAXHP, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MaxHp);
		QueryGetField(Row, DB_PLAYER_FIELD_MINHP, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MinHp);
		QueryGetField(Row, DB_PLAYER_FIELD_MAXSTA, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MaxSta);
		QueryGetField(Row, DB_PLAYER_FIELD_MINSTA, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MinSta);
		QueryGetField(Row, DB_PLAYER_FIELD_MAXMAN, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MaxMan);
		QueryGetField(Row, DB_PLAYER_FIELD_MINMAN, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MinMan);
		QueryGetField(Row, DB_PLAYER_FIELD_MAXHUN, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MaxHun);
		QueryGetField(Row, DB_PLAYER_FIELD_MINHUM, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MinHun);
		QueryGetField(Row, DB_PLAYER_FIELD_MAXTHI, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MaxThi);
		QueryGetField(Row, DB_PLAYER_FIELD_MINTHI, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MinThi);

*/
		strcpy(Character->Attributes.Player.Name, DBCharacter->Attributes.Player.Name);
		Character->Attributes.Player.Ban = DBCharacter->Attributes.Player.Ban;
		Character->Attributes.Player.Active = DBCharacter->Attributes.Player.Active;

		Character->Attributes.Player.Stats.Level = DBCharacter->Attributes.Player.Level;
		Character->Attributes.Player.Stats.Exp = DBCharacter->Attributes.Player.Exp;
		Character->Attributes.Player.Stats.Gold = DBCharacter->Attributes.Player.Gold;

		Character->Attributes.Player.Stats.Hp[PLAYER_STATS_MAX] = DBCharacter->Attributes.Player.MaxHp;
		Character->Attributes.Player.Stats.Hp[PLAYER_STATS_MIN] = DBCharacter->Attributes.Player.MinHp;
		Character->Attributes.Player.Stats.Mana[PLAYER_STATS_MAX] = DBCharacter->Attributes.Player.MaxMan;
		Character->Attributes.Player.Stats.Mana[PLAYER_STATS_MIN] = DBCharacter->Attributes.Player.MinMan;

		
		//Character->Attributes.Player.Flags.Paralized = DBCharacter->Attributes.Player.Paralized;
		//Character->Attributes.Player.Flags.Poisoned = DBCharacter->Attributes.Player.Poisoned;
		Character->Attributes.Player.Flags.Dead = (DBCharacter->Attributes.Player.MinHp == 0);
		// DBCharacter->Status.Magic; // to remove
		
		/*
		Character->Attributes.Player.Inventory.ArmorEqpIndex = DBCharacter->EquipedItems.Armor;
		Character->Attributes.Player.Inventory.FootwearEqpIndex = DBCharacter->EquipedItems.Footwear;
		Character->Attributes.Player.Inventory.HelmetEqpIndex = DBCharacter->EquipedItems.Helmet;
		Character->Attributes.Player.Inventory.ShieldEqpIndex = DBCharacter->EquipedItems.Shield;
		Character->Attributes.Player.Inventory.WeaponEqpIndex = DBCharacter->EquipedItems.Weapon;
		*/

		Character->Attributes.Player.World = DBCharacter->Attributes.Player.World;
		Vector3fCopy(&Character->Attributes.Player.Position, &DBCharacter->Attributes.Player.Position);
	}
	else if (Type == CHARACTER_TYPE_NPC)
	{
		// todo

		/*
		Character->Attributes.Npc.Flags.Magic = DBCharacter->Attributes.Npc 
		Character->Attributes.Npc.Flags.ParalysisResistant
		Character->Attributes.Npc.Stats.Defense
		Character->Attributes.Npc.Stats.Hit
		Character->Attributes.Npc.Stats.Hp
		Character->Attributes.Npc.Stats.Level
		Character->Attributes.Npc.Stats.MagicDefense
		Character->Attributes.Npc.Stats.Paralized
		*/

	}
}
