#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>

#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/freeglut.h>

#include "util.h"
#include "geom.h"
#include "iqm.h"

#define EXTS(EXT) \
    EXT(PFNGLUSEPROGRAMPROC, glUseProgram, true) \
    EXT(PFNGLCREATEPROGRAMPROC, glCreateProgram, true) \
    EXT(PFNGLCREATESHADERPROC, glCreateShader, true) \
    EXT(PFNGLDELETEPROGRAMPROC, glDeleteProgram, true) \
    EXT(PFNGLDELETESHADERPROC, glDeleteShader, true) \
    EXT(PFNGLATTACHSHADERPROC, glAttachShader, true) \
    EXT(PFNGLBINDATTRIBLOCATIONPROC, glBindAttribLocation, true) \
    EXT(PFNGLCOMPILESHADERPROC, glCompileShader, true) \
    EXT(PFNGLLINKPROGRAMPROC, glLinkProgram, true) \
    EXT(PFNGLSHADERSOURCEPROC, glShaderSource, true) \
    EXT(PFNGLGETPROGRAMIVPROC, glGetProgramiv, true) \
    EXT(PFNGLGETSHADERIVPROC, glGetShaderiv, true) \
    EXT(PFNGLGETPROGRAMINFOLOGPROC, glGetProgramInfoLog, true) \
    EXT(PFNGLGETSHADERINFOLOGPROC, glGetShaderInfoLog, true) \
    EXT(PFNGLDISABLEVERTEXATTRIBARRAYPROC, glDisableVertexAttribArray, true) \
    EXT(PFNGLENABLEVERTEXATTRIBARRAYPROC, glEnableVertexAttribArray, true) \
    EXT(PFNGLVERTEXATTRIBPOINTERPROC, glVertexAttribPointer, true) \
    EXT(PFNGLUNIFORMMATRIX3X4FVPROC, glUniformMatrix3x4fv, true) \
    EXT(PFNGLUNIFORM1IPROC, glUniform1i, true) \
    EXT(PFNGLGETUNIFORMLOCATIONPROC, glGetUniformLocation, true) \
    EXT(PFNGLBINDBUFFERPROC, glBindBuffer, true) \
    EXT(PFNGLDELETEBUFFERSPROC, glDeleteBuffers, true) \
    EXT(PFNGLGENBUFFERSPROC, glGenBuffers, true) \
    EXT(PFNGLBUFFERDATAPROC, glBufferData, true) \
    EXT(PFNGLBUFFERSUBDATAPROC, glBufferSubData, true) \

#define DEFEXT(type, name, required) type name##_ = NULL;

EXTS(DEFEXT)

void fatal(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    fputc('\n', stderr);
    va_end(ap);
    exit(EXIT_FAILURE);
}

void loadexts()
{
    const char *version = (const char *)glGetString(GL_VERSION);
    if(strcmp(version, "2.1") < 0) fatal("OpenGL version 2.1 required, found version: %s", version);

    #define LOADEXT(type, name, required) if(required) { name##_ = (type)glutGetProcAddress(#name); if(!name##_) fatal("failed getting proc address: %s", #name); }
    EXTS(LOADEXT)
}

extern GLuint loadtexture(const char *name, int clamp);

// Note that while this demo stores pointer directly into mesh data in a buffer 
// of the entire IQM file's data, it is recommended that you copy the data and
// convert it into a more suitable internal representation for whichever 3D
// engine you use.
uchar *meshdata = NULL;
int nummeshes = 0, numtris = 0, numverts = 0, numjoints = 0;
iqmmesh *meshes = NULL;
GLuint *textures = NULL;
GLuint notexture = 0;
iqmjoint *joints = NULL;
iqmpose *poses = NULL;
Matrix3x4 *baseframe = NULL, *inversebaseframe = NULL;

struct vertex
{
    GLfloat position[3];
    GLfloat normal[3];
    GLfloat tangent[4];
    GLfloat texcoord[2];
    GLubyte blendindex[4];
    GLubyte blendweight[4];
};
GLuint ebo = 0, vbo = 0;

void cleanupiqm()
{
    if(textures)
    {
        glDeleteTextures(nummeshes, textures);
        delete[] textures;
    }
    if(notexture) glDeleteTextures(1, &notexture);
    delete[] baseframe;
    delete[] inversebaseframe;
    if(ebo) glDeleteBuffers_(1, &ebo);
    if(vbo) glDeleteBuffers_(1, &vbo);
}

bool loadiqmmeshes(const char *filename, const iqmheader &hdr, uchar *buf)
{
    if(meshdata) return false;

    lilswap((uint *)&buf[hdr.ofs_vertexarrays], hdr.num_vertexarrays*sizeof(iqmvertexarray)/sizeof(uint));
    lilswap((uint *)&buf[hdr.ofs_triangles], hdr.num_triangles*sizeof(iqmtriangle)/sizeof(uint));
    lilswap((uint *)&buf[hdr.ofs_meshes], hdr.num_meshes*sizeof(iqmmesh)/sizeof(uint));
    lilswap((uint *)&buf[hdr.ofs_joints], hdr.num_joints*sizeof(iqmjoint)/sizeof(uint));

    meshdata = buf;
    nummeshes = hdr.num_meshes;
    numtris = hdr.num_triangles;
    numverts = hdr.num_vertexes;
    numjoints = hdr.num_joints;
    textures = new GLuint[nummeshes];
    memset(textures, 0, nummeshes*sizeof(GLuint));

    float *inposition = NULL, *innormal = NULL, *intangent = NULL, *intexcoord = NULL;
    uchar *inblendindex = NULL, *inblendweight = NULL;
    const char *str = hdr.ofs_text ? (char *)&buf[hdr.ofs_text] : "";
    iqmvertexarray *vas = (iqmvertexarray *)&buf[hdr.ofs_vertexarrays];
    for(int i = 0; i < (int)hdr.num_vertexarrays; i++)
    {
        iqmvertexarray &va = vas[i];
        switch(va.type)
        {
        case IQM_POSITION: if(va.format != IQM_FLOAT || va.size != 3) return false; inposition = (float *)&buf[va.offset]; lilswap(inposition, 3*hdr.num_vertexes); break;
        case IQM_NORMAL: if(va.format != IQM_FLOAT || va.size != 3) return false; innormal = (float *)&buf[va.offset]; lilswap(innormal, 3*hdr.num_vertexes); break;
        case IQM_TANGENT: if(va.format != IQM_FLOAT || va.size != 4) return false; intangent = (float *)&buf[va.offset]; lilswap(intangent, 4*hdr.num_vertexes); break;
        case IQM_TEXCOORD: if(va.format != IQM_FLOAT || va.size != 2) return false; intexcoord = (float *)&buf[va.offset]; lilswap(intexcoord, 2*hdr.num_vertexes); break;
        case IQM_BLENDINDEXES: if(va.format != IQM_UBYTE || va.size != 4) return false; inblendindex = (uchar *)&buf[va.offset]; break;
        case IQM_BLENDWEIGHTS: if(va.format != IQM_UBYTE || va.size != 4) return false; inblendweight = (uchar *)&buf[va.offset]; break;
        }
    }
    meshes = (iqmmesh *)&buf[hdr.ofs_meshes];
    joints = (iqmjoint *)&buf[hdr.ofs_joints];

    baseframe = new Matrix3x4[hdr.num_joints];
    inversebaseframe = new Matrix3x4[hdr.num_joints];
    for(int i = 0; i < (int)hdr.num_joints; i++)
    {
        iqmjoint &j = joints[i];
        baseframe[i] = Matrix3x4(Quat(j.rotate).normalize(), Vec3(j.translate), Vec3(j.scale));
        inversebaseframe[i].invert(baseframe[i]);
        if(j.parent >= 0)
        {
            baseframe[i] = baseframe[j.parent] * baseframe[i];
            inversebaseframe[i] *= inversebaseframe[j.parent];
        }
    }

    for(int i = 0; i < (int)hdr.num_meshes; i++)
    {
        iqmmesh &m = meshes[i];
        printf("%s: loaded mesh: %s\n", filename, &str[m.name]);
        textures[i] = loadtexture(&str[m.material], 0);
        if(textures[i]) printf("%s: loaded material: %s\n", filename, &str[m.material]);
    }

    iqmtriangle *tris = (iqmtriangle *)&buf[hdr.ofs_triangles];

    if(!ebo) glGenBuffers_(1, &ebo);
    glBindBuffer_(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData_(GL_ELEMENT_ARRAY_BUFFER, hdr.num_triangles*sizeof(iqmtriangle), tris, GL_STATIC_DRAW);
    glBindBuffer_(GL_ELEMENT_ARRAY_BUFFER, 0);

    vertex *verts = new vertex[hdr.num_vertexes];
    memset(verts, 0, hdr.num_vertexes*sizeof(vertex));
    for(int i = 0; i < (int)hdr.num_vertexes; i++)
    {
        vertex &v = verts[i];
        if(inposition) memcpy(v.position, &inposition[i*3], sizeof(v.position));
        if(innormal) memcpy(v.normal, &innormal[i*3], sizeof(v.normal));
        if(intangent) memcpy(v.tangent, &intangent[i*4], sizeof(v.tangent));
        if(intexcoord) memcpy(v.texcoord, &intexcoord[i*2], sizeof(v.texcoord));
        if(inblendindex) memcpy(v.blendindex, &inblendindex[i*4], sizeof(v.blendindex));
        if(inblendweight) memcpy(v.blendweight, &inblendweight[i*4], sizeof(v.blendweight));
    }

    if(!vbo) glGenBuffers_(1, &vbo);
    glBindBuffer_(GL_ARRAY_BUFFER, vbo);
    glBufferData_(GL_ARRAY_BUFFER, hdr.num_vertexes*sizeof(vertex), verts, GL_STATIC_DRAW);
    glBindBuffer_(GL_ARRAY_BUFFER, 0);
    delete[] verts;

    return true;
}

bool loadiqm(const char *filename)
{
    FILE *f = fopen(filename, "rb");
    if(!f) return false;

    uchar *buf = NULL;
    iqmheader hdr;
    if(fread(&hdr, 1, sizeof(hdr), f) != sizeof(hdr) || memcmp(hdr.magic, IQM_MAGIC, sizeof(hdr.magic)))
        goto error;
    lilswap(&hdr.version, (sizeof(hdr) - sizeof(hdr.magic))/sizeof(uint));
    if(hdr.version != IQM_VERSION)
        goto error;
    if(hdr.filesize > (16<<20)) 
        goto error; // sanity check... don't load files bigger than 16 MB
    buf = new uchar[hdr.filesize];
    if(fread(buf + sizeof(hdr), 1, hdr.filesize - sizeof(hdr), f) != hdr.filesize - sizeof(hdr))
        goto error;

    if(hdr.num_meshes > 0 && !loadiqmmeshes(filename, hdr, buf)) goto error;

    fclose(f);
    return true;

error:
    printf("%s: error while loading\n", filename);
    if(buf != meshdata) delete[] buf;
    fclose(f);
    return false;
}

struct binding
{
    const char *name;
    GLint index;
};

struct shader
{
    const char *name, *vsstr, *psstr;
    const binding *attribs, *texs;
    GLuint vs, ps, program, vsobj, psobj;

    shader(const char *name, const char *vsstr = NULL, const char *psstr = NULL, const binding *attribs = NULL, const binding *texs = NULL) : name(name), vsstr(vsstr), psstr(psstr), attribs(attribs), texs(texs), vs(0), ps(0), program(0), vsobj(0), psobj(0) {}

    static void showinfo(GLuint obj, const char *tname, const char *name)
    {
        GLint length = 0;
        if(!strcmp(tname, "PROG")) glGetProgramiv_(obj, GL_INFO_LOG_LENGTH, &length);
        else glGetShaderiv_(obj, GL_INFO_LOG_LENGTH, &length);
        if(length > 1)
        {
            GLchar *log = new GLchar[length];
            if(!strcmp(tname, "PROG")) glGetProgramInfoLog_(obj, length, &length, log);
            else glGetShaderInfoLog_(obj, length, &length, log);
            printf("GLSL ERROR (%s:%s)\n", tname, name);
            puts(log);
            delete[] log;
        }
    }

    static void compile(GLenum type, GLuint &obj, const char *def, const char *tname, const char *name, bool msg = true)
    {
        const GLchar *source = (const GLchar*)(def + strspn(def, " \t\r\n"));
        obj = glCreateShader_(type);
        glShaderSource_(obj, 1, &source, NULL);
        glCompileShader_(obj);
        GLint success;
        glGetShaderiv_(obj, GL_COMPILE_STATUS, &success);
        if(!success)
        {
            if(msg) showinfo(obj, tname, name);
            glDeleteShader_(obj);
            obj = 0;
            fatal("error compiling shader");
        }
    }

    void link(const binding *attribs = NULL, bool msg = true)
    {
        program = vsobj && psobj ? glCreateProgram_() : 0;
        GLint success = 0;
        if(program)
        {
            glAttachShader_(program, vsobj);
            glAttachShader_(program, psobj);

            if(attribs) for(const binding *a = attribs; a->name; a++)
                glBindAttribLocation_(program, a->index, a->name);

            glLinkProgram_(program);
            glGetProgramiv_(program, GL_LINK_STATUS, &success);
        }
        if(!success)
        {
            if(program)
            {
                if(msg) showinfo(program, "PROG", name);
                glDeleteProgram_(program);
                program = 0;
            }
            fatal("error linking shader");
        }
    }

    void compile(const char *vsdef, const char *psdef, const binding *attribs = NULL)
    {
        compile(GL_VERTEX_SHADER,   vsobj, vsdef, "VS", name);
        compile(GL_FRAGMENT_SHADER, psobj, psdef, "PS", name);
        link(attribs, true);
    }

    void compile()
    {
        if(vsstr && psstr) compile(vsstr, psstr, attribs);
    }

    void set()
    {
        glUseProgram_(program);
        bindtexs();
    }

    GLint getparam(const char *pname)
    {
        return glGetUniformLocation_(program, pname);
    }

    void bindtex(const char *tname, GLint index)
    {
        GLint loc = getparam(tname);
        if(loc != -1) glUniform1i_(loc, index);
    }

    void bindtexs()
    {
        if(texs) for(const binding *t = texs; t->name; t++)
            bindtex(t->name, t->index);
    }
};

binding noskinattribs[] = { { "vtangent", 1 }, { NULL, -1 } };
binding noskintexs[] = { { "tex", 0 }, { NULL, -1 } };
shader noskin("no skin",

"#version 120\n"
"attribute vec4 vtangent;\n"
"void main(void)\n"
"{\n"
"   gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;\n"
"   gl_TexCoord[0] = gl_MultiTexCoord0;\n"
"   vec3 vbitangent = cross(gl_Normal, vtangent.xyz) * vtangent.w;\n" // bitangent not used, just here as an example
"   gl_FrontColor = gl_Color * (clamp(dot(normalize(gl_NormalMatrix * gl_Normal), gl_LightSource[0].position.xyz), 0.0, 1.0) * gl_LightSource[0].diffuse + gl_LightSource[0].ambient);\n"
"}\n",

"#version 120\n"
"uniform sampler2D tex;\n"
"void main(void)\n"
"{\n"
"   gl_FragColor = gl_Color * texture2D(tex, gl_TexCoord[0].xy);\n"
"}\n",

noskinattribs, noskintexs);

float scale = 1, rotate = 0;

void renderiqm()
{
    static const GLfloat zero[4] = { 0, 0, 0, 0 }, 
                         one[4] = { 1, 1, 1, 1 },
                         ambientcol[4] = { 0.5f, 0.5f, 0.5f, 1 }, 
                         diffusecol[4] = { 0.5f, 0.5f, 0.5f, 1 },
                         lightdir[4] = { cosf(radians(-60)), 0, sinf(radians(-60)), 0 };

    glPushMatrix();
    glRotatef(rotate, 0, 0, -1);
    glScalef(scale, scale, scale);

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, zero);
    glMaterialfv(GL_FRONT, GL_SPECULAR, zero);
    glMaterialfv(GL_FRONT, GL_EMISSION, zero);
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, one);
    glLightfv(GL_LIGHT0, GL_SPECULAR, zero);
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientcol);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffusecol);
    glLightfv(GL_LIGHT0, GL_POSITION, lightdir);

    glColor3f(1, 1, 1);

    noskin.set();

    glBindBuffer_(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBindBuffer_(GL_ARRAY_BUFFER, vbo);

    vertex *vert = NULL;
    glVertexPointer(3, GL_FLOAT, sizeof(vertex), &vert->position);
    glNormalPointer(GL_FLOAT, sizeof(vertex), &vert->normal);
    glTexCoordPointer(2, GL_FLOAT, sizeof(vertex), &vert->texcoord);
    glVertexAttribPointer_(1, 4, GL_FLOAT, GL_FALSE, sizeof(vertex), &vert->tangent);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnableVertexAttribArray_(1);

    iqmtriangle *tris = NULL;
    for(int i = 0; i < nummeshes; i++)
    {
        iqmmesh &m = meshes[i];
        glBindTexture(GL_TEXTURE_2D, textures[i] ? textures[i] : notexture);
        glDrawElements(GL_TRIANGLES, 3*m.num_triangles, GL_UNSIGNED_INT, &tris[m.first_triangle]);
    }

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableVertexAttribArray_(1);

    glBindBuffer_(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer_(GL_ARRAY_BUFFER, 0);

    glPopMatrix();
}

void initgl()
{
    glClearColor(0, 0, 0, 0);
    glClearDepth(1);
    glDisable(GL_FOG);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    noskin.compile();

    notexture = loadtexture("notexture.tga", 0);
}

int scrw = 0, scrh = 0;

void reshapefunc(int w, int h)
{
    scrw = w;
    scrh = h;
    glViewport(0, 0, w, h);
}

float camyaw = -90, campitch = 0, camroll = 0;
Vec3 campos(20, 0, 5);

void setupcamera()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    GLdouble aspect = double(scrw)/scrh,
             fov = radians(90),
             fovy = 2*atan2(tan(fov/2), aspect),
             nearplane = 1e-2f, farplane = 1000,
             ydist = nearplane * tan(fovy/2), xdist = ydist * aspect;
    glFrustum(-xdist, xdist, -ydist, ydist, nearplane, farplane);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glRotatef(camroll, 0, 0, 1);
    glRotatef(campitch, -1, 0, 0);
    glRotatef(camyaw, 0, 1, 0);
    glRotatef(-90, 1, 0, 0);
    glScalef(1, -1, 1);
    glTranslatef(-campos.x, -campos.y, -campos.z);
}

void displayfunc()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    setupcamera();

    //animateiqm(animate);
    renderiqm();

    glutSwapBuffers();
}

void keyboardfunc(uchar c, int x, int y)
{
    switch(c)
    {
    case 27:
	{
	cleanupiqm();
        exit(EXIT_SUCCESS);
        break;
	}
    }
}

int main(int argc, char **argv)
{
    glutInitWindowSize(640, 480);
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);
    glutCreateWindow("IQM GPU Skinning Demo");

    loadexts();

    for(int i = 1; i < argc; i++)
    {
        if(argv[i][0] == '-') switch(argv[i][1])
        {
        case 's':
            if(i + 1 < argc) scale = clamp(atof(argv[++i]), 1e-8, 1e8);
            break;
        case 'r':
            if(i + 1 < argc) rotate = atof(argv[++i]);
            break;
        }
        else if(!loadiqm(argv[i])) return EXIT_FAILURE;
    }
    if(!meshdata && !loadiqm("mrfixit.iqm")) return EXIT_FAILURE;

    initgl();

    glutReshapeFunc(reshapefunc);
    glutDisplayFunc(displayfunc);
    glutKeyboardFunc(keyboardfunc);
    glutMainLoop();

    return EXIT_SUCCESS;
}

