/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	METADATA_FUNCTION_H
#define METADATA_FUNCTION_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/Storage.h>
#include <MetaData/Scope.h>
#include <MetaData/Parameter.h>

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef MetadataStorageIndexType		MetadataFunctionType;
typedef char *							MetadataFunctionNameType;
typedef struct MetadataFunctionType *	MetadataFunctionTypeImpl;
typedef void (*MetadataFunctionPtr)(MetadataParameterListType);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a function
////////////////////////////////////////////////////////////
MetadataFunctionType MetadataFunctionCreate(MetadataScopeType Scope, MetadataParameterListType Signature);

////////////////////////////////////////////////////////////
/// Bind a function
////////////////////////////////////////////////////////////
MetadataFunctionType MetadataFunctionBind(MetadataScopeType Scope, MetadataParameterListType Signature, MetadataFunctionPtr FunctionPtr);

////////////////////////////////////////////////////////////
/// Copy a function
////////////////////////////////////////////////////////////
MetadataFunctionType MetadataFunctionCopy(MetadataScopeType Scope, MetadataFunctionType Function);

////////////////////////////////////////////////////////////
/// Get signature of a function
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataFunctionGetSignature(MetadataScopeType Scope, MetadataFunctionType Function);

////////////////////////////////////////////////////////////
/// Execute a function dynamically
////////////////////////////////////////////////////////////
void MetadataFunctionExecute(MetadataScopeType Scope, MetadataFunctionType Function, MetadataParameterListType ParameterList);

////////////////////////////////////////////////////////////
/// Reference a function
////////////////////////////////////////////////////////////
void MetadataFunctionReference(MetadataScopeType Scope, MetadataFunctionType Function);

////////////////////////////////////////////////////////////
/// Check if function is referenced
////////////////////////////////////////////////////////////
bool MetadataFunctionIsReferenced(MetadataScopeType Scope, MetadataFunctionType Function);

////////////////////////////////////////////////////////////
/// Dereference a function
////////////////////////////////////////////////////////////
void MetadataFunctionDereference(MetadataScopeType Scope, MetadataFunctionType Function);

////////////////////////////////////////////////////////////
/// Destroy a function
////////////////////////////////////////////////////////////
void MetadataFunctionDestroy(MetadataScopeType Scope, MetadataFunctionType Function);

#endif // METADATA_FUNCTION_H
