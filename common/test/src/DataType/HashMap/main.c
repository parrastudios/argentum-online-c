/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/DataTypes/HashMap.h>
#include <stdio.h>

////////////////////////////////////////////////////////////
/// Integer hash function implementation for set
////////////////////////////////////////////////////////////
uint32 IntHash(void * Integer)
{
	uint32 Value = (uint32)(Integer);

    Value = Value ^ (Value >> 4);
    Value = (Value ^ 0xDEADBEEF) + (Value << 5);
    Value = Value ^ (Value >> 11);

    return Value;
}

////////////////////////////////////////////////////////////
/// Program entry point
////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
    struct HashMapType * HashMap;
    struct HashMapIterator Iterator;

	// Create hash map
	HashMap = HashMapCreate();

	// Initialize hash map (int -> str)
    HashMapInitialize(HashMap, &IntHash);

	/*
	Output:

    < 0 (1)
    < 1 (1)
    < 2 (1)
    < 1 (1)
    < 3 (1)
    < 1 (1)
    < 4 (1)
    < 5 (1)
    < 6 (1)
    < 7 (1)
    < 8 (1)
    < 9 (1)
	*/

    printf("< 0 (%d)\n", (int)HashMapInsert(HashMap, (void*)0, (void*)"0"));
    printf("< 1 (%d)\n", (int)HashMapInsert(HashMap, (void*)1, (void*)"1"));
    printf("< 2 (%d)\n", (int)HashMapInsert(HashMap, (void*)2, (void*)"2"));
    printf("< 1 (%d)\n", (int)HashMapInsert(HashMap, (void*)1, (void*)"3"));
    printf("< 3 (%d)\n", (int)HashMapInsert(HashMap, (void*)3, (void*)"4"));
    printf("< 1 (%d)\n", (int)HashMapInsert(HashMap, (void*)1, (void*)"5"));
    printf("< 4 (%d)\n", (int)HashMapInsert(HashMap, (void*)4, (void*)"6"));
    printf("< 5 (%d)\n", (int)HashMapInsert(HashMap, (void*)5, (void*)"7"));
    printf("< 6 (%d)\n", (int)HashMapInsert(HashMap, (void*)6, (void*)"8"));
    printf("< 7 (%d)\n", (int)HashMapInsert(HashMap, (void*)7, (void*)"9"));
    printf("< 8 (%d)\n", (int)HashMapInsert(HashMap, (void*)8, (void*)"10"));
    printf("< 9 (%d)\n", (int)HashMapInsert(HashMap, (void*)95, (void*)"11"));

	/*
	Output:

    > 8 -> 10
    > 0 -> 0
    > 1 -> 5
    > 2 -> 2
    > 3 -> 4
    > 95 -> 11
    > 4 -> 6
    > 5 -> 7
    > 6 -> 8
    > 7 -> 9
	------------
	*/

    for (HashMapIteratorBegin(&Iterator, HashMap); !HashMapIteratorEnd(&Iterator); HashMapIteratorNext(&Iterator))
    {
		struct HashMapPairType * Pair = HashMapIteratorData(&Iterator);
        printf("> %d -> %s\n", (uint32)Pair->Key, (char*)Pair->Data);
    }

    printf("------------\n");

    HashMapClear(HashMap);

	/*
	Output:

    < 0 (1)
    < 1 (1)
    < 2 (1)
    < 1 (1)
	*/

    printf("< 0 (%d)\n", (uint32)HashMapInsert(HashMap, (void*)0, (void*)"0"));
    printf("< 1 (%d)\n", (uint32)HashMapInsert(HashMap, (void*)1, (void*)"1"));
    printf("< 2 (%d)\n", (uint32)HashMapInsert(HashMap, (void*)2, (void*)"2"));
    printf("< 1 (%d)\n", (uint32)HashMapInsert(HashMap, (void*)1, (void*)"3"));

	/*
	Output:

    > 0 -> 0
    > 1 -> 3
    > 2 -> 2
	*/

    for (HashMapIteratorBegin(&Iterator, HashMap); !HashMapIteratorEnd(&Iterator); HashMapIteratorNext(&Iterator))
    {
		struct HashMapPairType * Pair = HashMapIteratorData(&Iterator);
        printf("> %d -> %s\n", (int)Pair->Key, (char*)Pair->Data);
    }

	// Destroy hash map
    HashMapDestroy(HashMap);

    return 0;
}
