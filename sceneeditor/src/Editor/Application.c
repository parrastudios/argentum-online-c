/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Editor/Application.h>		//< Editor includes
//#include <Editor/MainMenu.h>

#include <System/Platform.h>		//< Platform SO includes
#include <System/Thread.h>
#include <System/Mutex.h>
#include <System/CPUInfo.h>

#include <Math/General.h>			//< Math includes

#include <Window/Window.h>			//< Window includes

#include <Graphics/Device.h> 		//< Graphic includes
#include <Graphics/StatesManager.h>
#include <Graphics/Font.h>
#include <Graphics/Texture.h>
#include <Graphics/TextureManager.h>
#include <Graphics/VideoRender.h>
#include <Graphics/Index.h>

#include <Audio/OpenALHelper.h> 	//< Audio includes
#include <Audio/SoundManager.h>

#include <Gui/WindowManager.h>		//< GUI includes
#include <Gui/WindowParser.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
uinteger	ApplicationStatus;			///< Application status controller
Mutex		ApplicationStatusMutex;		///< Application status mutex
Thread		ApplicationEventsThread;	///< Application event update thread

////////////////////////////////////////////////////////////
/// Initialize the application
////////////////////////////////////////////////////////////
bool ApplicationInitialize()
{
	struct VideoMode		DefaultMode;
	struct WindowSettings	Settings;

	// Initialize math library
	MathInitialize();

	// Initialize status variable
	ApplicationStatus = APPLICATION_DESTROY;

	// Initialize the application status mutex
	MutexInitialize(&ApplicationStatusMutex);

	// Set status of the editor
	ApplicationSetStatus(APPLICATION_INITIALIZE);

	// Initialize the configuration
	// ConfigInitialize();

	// Platform Initialize
	#if PLATFORM_TYPE == PLATFORM_MACOS
		InitializeWorkingDirectory(); // Specific in MacOS
	#endif

	// Get the CPU features
	CPUInitialize();

	// Create the window handle
	DefaultMode.Width = 1024;
	DefaultMode.Height = 768;
	DefaultMode.BitsPerPixel = 32; // ConfigGet()->BitsPerPixel;

	Settings.DepthBits = 24;
	Settings.StencilBits = 8;
	Settings.AntialiasingLevel = 0;

	if (!WindowCreate(&DefaultMode, APPLICATION_NAME, NonStyle, &Settings))
        return false;

	// Set the processor affinity
	#if COMPILE_TYPE == DEBUG_MODE
		// SystemSetProcessorAffinity();
    #endif

	#if COMPILE_TYPE == DEBUG_MODE
        WindowShowMouseCursor(true);
    #else
        WindowShowMouseCursor(false);
    #endif

	DeviceUseVerticalSync(DeviceGet(), false); // ConfigGet()->Vsync

	// Initialize Fonts
	FontInitialize();

	// Initialize Audio Device
	AudioInitializeDevice();

	if (!IndexInitialize())
		return false;

	// Initialize language
	// if (!LanguageInitialize())
	//	return false;

	// Gui initialization block
	{
		// Register maps to templating the script
		// GuiParserRegisterMap(GUI_PARSER_MAP_LANG_ID, (WindowParserMapFunc)&LanguageMessageGet);
		// GuiParserRegisterMap(GUI_PARSER_MAP_CONFIG_ID, (WindowParserMapFunc)&ConfigToString);

		// Register the main menu
		// MainMenuRegister();

		// Initialize the gui
		GuiInitialize(0); // ConfigGet()->GuiTemplate

		// Initialize the main menu
		// MainMenuInitialize();
	}


	// Set status to intro
	#if COMPILE_TYPE == RELEASE_MODE
		ApplicationSetStatus(APPLICATION_INTRO);
	#else
		ApplicationSetStatus(APPLICATION_RUN);
	#endif

    // Launch the event thread
    // ThreadLaunch(&ApplicationEventsThread, (FuncType)&ApplicationEvents, NULL);

	return true;
}

////////////////////////////////////////////////////////////
/// Update the events of the application
////////////////////////////////////////////////////////////
void ApplicationEvents()
{
    uinteger StatusController = APPLICATION_DESTROY;

	do
    {
		struct Event Evt = {{0}};

        // Get the event
        WindowGetEvent(&Evt);

        // Get the application status
        ApplicationGetStatus(&StatusController);

        // Update application status events
		switch (StatusController)
		{
			case APPLICATION_INTRO :
			{
				if (Evt.Key.Code == Esc)
					ApplicationSetStatus(APPLICATION_DESTROY);

				if (Evt.Key.Code == Return)
					ApplicationSetStatus(APPLICATION_RUN);

				break;
			}

			case APPLICATION_RUN :
			{
				if (Evt.Key.Code == Esc)
					ApplicationSetStatus(APPLICATION_DESTROY);

				if (Evt.Key.Code == T)
					ApplicationSetStatus(APPLICATION_TEST);

				if (Evt.Key.Code == F1)
					TextureScreenShot(WindowWidth, WindowHeight);

				// Update main menu events
				// MainMenuUpdate(&Evt);

				break;
			}

			case APPLICATION_TEST :
			{
				if (Evt.Key.Code == Esc)
					ApplicationSetStatus(APPLICATION_RUN);

				break;
			}
		}

        // Compute the user interface
        GuiUpdate(&Evt);

		// Clear events
		WindowClearEvent();

		#if COMPILE_TYPE == DEBUG_MODE
			// SystemSleep(5);
		#endif
    } while (StatusController != APPLICATION_DESTROY);
}

////////////////////////////////////////////////////////////
/// Run the application
////////////////////////////////////////////////////////////
void ApplicationRun()
{
    uint32 StatusController = APPLICATION_DESTROY;

	do
	{
		// Clear device scene
		DeviceClear(DeviceGet(), true, false);

		// Get the application status
		ApplicationGetStatus(&StatusController);

		// Check keys
		switch (StatusController)
		{
			case APPLICATION_INTRO :
			{
				// Set orthogonal projection
				DeviceSetMatrixOrthogonal(DeviceGet());

				DeviceBegin(DeviceGet());
					DeviceSetState(DeviceGet(), DEVICE_RENDERING_2D);

					// Show intro
					// IntroShow();

				DeviceEnd(DeviceGet());

				break;
			}

			case APPLICATION_RUN :
			{
				// Set orthogonal projection
				DeviceSetMatrixOrthogonal(DeviceGet());

				DeviceBegin(DeviceGet());
					DeviceSetState(DeviceGet(), DEVICE_RENDERING_2D);

					// Render editor main menu
					// MainMenuRender();

					// ...

				DeviceEnd(DeviceGet());

				break;
			}

			case APPLICATION_TEST :
			{
				// Set orthogonal projection
				DeviceSetMatrixOrthogonal(DeviceGet());

				DeviceBegin(DeviceGet());
					DeviceSetState(DeviceGet(), DEVICE_RENDERING_2D | DEVICE_RENDERING_ALPHA);

					// Render GUI Test
					// GuiTestRender();

				DeviceEnd(DeviceGet());

				break;
			}
		}

		// Set orthogonal projection
		DeviceSetMatrixOrthogonal(DeviceGet());

		DeviceBegin(DeviceGet());
			DeviceSetState(DeviceGet(), DEVICE_RENDERING_2D | DEVICE_RENDERING_ALPHA);

			// Render GUI
			GuiRender();

		DeviceEnd(DeviceGet());

		// Display on window handle
		WindowDisplay();

		// Process input events
		WindowProcessEvents();

		#if COMPILE_TYPE == DEBUG_MODE
		//	SystemSleep(5);
		#endif
	} while (StatusController != APPLICATION_DESTROY);
}

////////////////////////////////////////////////////////////
/// Destroy and cleanup the application
////////////////////////////////////////////////////////////
bool ApplicationDestroy()
{
	// Save config
	// ConfigSave();

	// Destroy Video Device
	VideoDestroy();

	// Destroy GUI
	GuiDestroy();

	// Destroy Fonts
	FontDestroy();

	// Destroy textures
	TextureManagerDestroy();

	// Destroy graphical device
	DeviceDestroy(DeviceGet());

	// Destroy Sound Buffer
	SoundBufferDestroy();

	// Destroy Audio Device
	AudioDestroyDevice();

	// Destroy the window handle
	WindowClose();

	// Destroy cpu queries
	CPUDestroy();

	return true;
}

////////////////////////////////////////////////////////////
/// Set up the status of the application
////////////////////////////////////////////////////////////
void ApplicationSetStatus(uinteger State)
{
	switch (State)
	{
		case APPLICATION_INITIALIZE :

			break;

		case APPLICATION_RUN :
			// Open main menu
			// MainMenuOpen();

			break;

		case APPLICATION_DESTROY :
			// Close main menu
			// MainMenuClose();

			break;
	}

	// Set the status
    MutexLock(&ApplicationStatusMutex);

	ApplicationStatus = State;

	MutexUnlock(&ApplicationStatusMutex);
}

////////////////////////////////////////////////////////////
/// Get the status of the application
////////////////////////////////////////////////////////////
void ApplicationGetStatus(uinteger * State)
{
    MutexLock(&ApplicationStatusMutex);

    *State = ApplicationStatus;

    MutexUnlock(&ApplicationStatusMutex);
}
