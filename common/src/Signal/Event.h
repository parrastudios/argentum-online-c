


#define SIGNAL_EVENT_MODE_SYNC			0x01 << 0x00
#define SIGNAL_EVENT_MODE_ASYNC			0x01 << 0x01

#define SIGNAL_EVENT_TYPE_CALLBACK		0x01 << 0x00
#define SIGNAL_EVENT_TYPE_TASK			0x01 << 0x01
#define SIGNAL_EVENT_TYPE_TIMEOUT		0x01 << 0x02
#define SIGNAL_EVENT_TYPE_TIMER			0x01 << 0x03
#define SIGNAL_EVENT_TYPE_SYSTEM		0x01 << 0x04


typedef void (*EventCallbackFuncType)(void *);
typedef void (*EventProgressFuncType)(float);
typedef void (*EventTerminationFuncType)();

struct EventData
{
	EventCallbackFuncType		Callback;			//< Callback function
	EventProgressFuncType		Progress;			//< Progress function
	EventTerminationFuncType	Termination;		//< Termination function
	void *						Data;				//< Data attached to the event
	uint32						Type;				//< Event type
	uint32						Mode;				//< Event mode
	uint32						Priority;			//< Event priority

	struct
	{
		union			//< Related to function call mode
		{
			struct
			{
				
			} Synchronous;		

			struct
			{
				
			} Asynchronous;
		};
	} Mode;

	struct
	{
		union			//< Related to function call type
		{
			struct
			{

			} Callback;

			struct
			{

			} Task;

			struct
			{

			} Timeout;

			struct
			{
			
			} Timer;

			struct
			{
			
			} System;
		};
	} Type;
};

