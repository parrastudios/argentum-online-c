/*
***********************************************************************************
 IqeB_Tools_Animation.cpp

 Tools for working with animations.

05.05.2015 RR: * First editon of this file.

*/

#include "IqeBrowser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>


/************************************************************************************
 * IqeB_AnimAction
 */

void IqeB_AnimAction( int ActionCode, int ActionArgument)
{

  switch( ActionCode) {
  case IQE_ANIMATE_ACTION_UPDATE:  // Update animation calculation after size changes (ot such things ...)
	  if( pDrawModel && pDrawModelCurAnim && pDrawModelCurAnim->len >= 1) {
      IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
    }
    break;

  //
  // Frame ...
  //
  case IQE_ANIMATE_ACTION_FRAME_PREV:  // Previous animation frame
	  if( pDrawModel && pDrawModelCurAnim && pDrawModelCurAnim->len >= 1) {
	    DrawModelCurframe -= 1;
	    while (DrawModelCurframe < 0) DrawModelCurframe += pDrawModelCurAnim->len;
	    while (DrawModelCurframe >= pDrawModelCurAnim->len) DrawModelCurframe -= pDrawModelCurAnim->len;
      IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
    }
    break;
  case IQE_ANIMATE_ACTION_FRAME_NEXT:  // Next animation frame
	  if( pDrawModel && pDrawModelCurAnim && pDrawModelCurAnim->len >= 1) {
	    DrawModelCurframe += 1;
	    while (DrawModelCurframe < 0) DrawModelCurframe += pDrawModelCurAnim->len;
	    while (DrawModelCurframe >= pDrawModelCurAnim->len) DrawModelCurframe -= pDrawModelCurAnim->len;
      IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
    }
    break;
  case IQE_ANIMATE_ACTION_FRAME_FIRST: // First animation frame
	  if( pDrawModel && pDrawModelCurAnim) {
      if( pDrawModelCurAnim->len != 0) {
  	    DrawModelCurframe = 0;
        IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
      }
	  }
    break;
  case IQE_ANIMATE_ACTION_FRAME_LAST:  // Last animation frame
	  if( pDrawModel && pDrawModelCurAnim) {
      if( pDrawModelCurAnim->len > 0 && DrawModelCurframe != pDrawModelCurAnim->len - 1) {
  	    DrawModelCurframe = pDrawModelCurAnim->len - 1;
        IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
      }
	  }
    break;
  case IQE_ANIMATE_ACTION_FRAME_SET:   // Set animation frame
	  if( pDrawModel && pDrawModelCurAnim) {

      if( ActionArgument >= pDrawModelCurAnim->len) {  // clip
        ActionArgument = pDrawModelCurAnim->len - 1;
      }
      if( ActionArgument < 0) {  // clip
        ActionArgument = 0;
      }

      if( pDrawModelCurAnim->len != ActionArgument) {
	      DrawModelCurframe = ActionArgument;
        IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
      }
    }
    break;

  //
  // Animations ...
  //
  case IQE_ANIMATE_ACTION_PREV:        // Previous animation
		if( pDrawModel && pDrawModelCurAnim && pDrawModel->anim_count > 1) {

      int ThisAnimNum;

      for( ThisAnimNum = 0; ThisAnimNum < pDrawModel->anim_count; ThisAnimNum++) {
        if( pDrawModel->anim_data[ ThisAnimNum] == pDrawModelCurAnim) {  // have not reched the active animation
          break;
        }
      }

      ThisAnimNum -= 1;

      if( ThisAnimNum >= 0) {
			  pDrawModelCurAnim = pDrawModel->anim_data[ ThisAnimNum];
      } else {                                              // wrap around
			  pDrawModelCurAnim = pDrawModel->anim_data[ pDrawModel->anim_count - 1];
      }

      DrawModelCurframe = 0;
			IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
		}
    break;
  case IQE_ANIMATE_ACTION_NEXT:        // Next animation
		if( pDrawModel && pDrawModelCurAnim && pDrawModel->anim_count > 1) {

      int ThisAnimNum;

      for( ThisAnimNum = 0; ThisAnimNum < pDrawModel->anim_count; ThisAnimNum++) {
        if( pDrawModel->anim_data[ ThisAnimNum] == pDrawModelCurAnim) {  // have not reched the active animation
          break;
        }
      }

      ThisAnimNum += 1;

      if( ThisAnimNum < pDrawModel->anim_count) {
			  pDrawModelCurAnim = pDrawModel->anim_data[ ThisAnimNum];
      } else {                                              // wrap around
			  pDrawModelCurAnim = pDrawModel->anim_data[ 0];
      }

      DrawModelCurframe = 0;
			IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
		}
    break;
  case IQE_ANIMATE_ACTION_FIRST:       // First animation
		if( pDrawModel && pDrawModelCurAnim && pDrawModel->anim_count >= 1) {

      pDrawModelCurAnim = pDrawModel->anim_data[ 0];

      DrawModelCurframe = 0;
			IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
		}
    break;
  case IQE_ANIMATE_ACTION_LAST:        // Last animation
		if( pDrawModel && pDrawModelCurAnim && pDrawModel->anim_count >= 1) {

      pDrawModelCurAnim = pDrawModel->anim_data[ pDrawModel->anim_count - 1];

      DrawModelCurframe = 0;
			IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
		}
    break;
  case IQE_ANIMATE_ACTION_SET:         // Set animation
		if( pDrawModel && pDrawModel->anim_count >= 1) {

      if( ActionArgument >= pDrawModel->anim_count) {  // clip
        ActionArgument = pDrawModel->anim_count - 1;
      }
      if( ActionArgument < 0) {  // clip
        ActionArgument = 0;
      }

      pDrawModelCurAnim = pDrawModel->anim_data[ ActionArgument];

      DrawModelCurframe = 0;
			IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
		}
    break;

  //
  // Management ...
  //
  case IQE_ANIMATE_ACTION_DELETE_ALL:  // Delete all animations
		if( pDrawModel && pDrawModel->anim_count >= 1) {

      if( ActionArgument >= 0 && ActionArgument < pDrawModel->anim_count) {  // in range


        IqeB_ModelFreeData( &pDrawModel, IQE_MODEL_FREE_ANIMATION);  // Free animation data

        pDrawModelCurAnim = NULL;
        DrawModelCurframe = 0;
  			IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
      }
		}
    break;
  case IQE_ANIMATE_ACTION_DELETE_ARG:  // Delete animation named in argument (use argument)
		if( pDrawModel && pDrawModel->anim_count >= 1) {

      if( ActionArgument >= 0 && ActionArgument < pDrawModel->anim_count) {  // in range

        struct anim *anim;
        int j;

        // Delete selected

	      anim = pDrawModel->anim_data[ ActionArgument];

        free( anim->name);

        if( anim->data) {

          for( j = 0; j < anim->len; j++) {

            free( anim->data[ j]);
          }

          free( anim->data);
        }

        free( anim);

        // Move next animations down

        for( j = ActionArgument; j < pDrawModel->anim_count - 1; j++) {

          pDrawModel->anim_data[ j] = pDrawModel->anim_data[ j + 1];
        }

        pDrawModel->anim_count -= 1;   // One animation less

        if( pDrawModel->anim_count == 0) {   // All removed

          IqeB_ModelFreeData( &pDrawModel, IQE_MODEL_FREE_ANIMATION);  // Free animation data

          pDrawModelCurAnim = NULL;
          DrawModelCurframe = 0;

        } else {

          // Select animation

          if( ActionArgument >= pDrawModel->anim_count) {   // Clipp animation

            ActionArgument = pDrawModel->anim_count - 1;
          }

          if( ActionArgument >= 0) {

            pDrawModelCurAnim = pDrawModel->anim_data[ ActionArgument];
            DrawModelCurframe = 0;
          } else {

            pDrawModelCurAnim = NULL;
            DrawModelCurframe = 0;
          }
        }

        IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
      }
		}
    break;
  case IQE_ANIMATE_ACTION_NEW_ARG:  // Create new animation (after named argument nr.) (use argument)
    if( pDrawModel != NULL) {

      int i, bone_count;
      struct anim *pAnim;
      struct pose *pPose;

      bone_count = 0;
      if( pDrawModel->skel != NULL && pDrawModel->skel->joint_count > 0) {

        bone_count = pDrawModel->skel->joint_count;
      }

      if( pDrawModel->anim_count > 0 && pDrawModel->anim_poses > 0) {  // Already have animatins

        if( bone_count != pDrawModel->anim_poses) {   // Security test, must be equal

          bone_count = 0;  // reset
        }
      }

      if( bone_count > 0) {          // Have enought bones to add animation

        // create new animation
        pAnim = pushanim( pDrawModel, (char *)"New animation");

        // Add one frame
        pPose = pushframe( pAnim, bone_count);

        // And preset the pose with the skeleton

        memcpy( pPose, pDrawModel->skel->pose, sizeof( struct pose) * bone_count);

        if( pDrawModel->anim_count == 1) {    // Is this the first created animation

          pDrawModel->anim_poses = bone_count;
        }

        // New animation is the last one

        pDrawModelCurAnim = pDrawModel->anim_data[ pDrawModel->anim_count - 1];

        // Preset KeyFlags

        IqeB_AnimKeyFlagsPresetAnimation( pDrawModelCurAnim, pDrawModel->anim_poses);

        // Exchange position ?

        if( ActionArgument >= 0 && ActionArgument < pDrawModel->anim_count - 1) {  // in range

          // shift down others

          for( i = pDrawModel->anim_count - 1; i > ActionArgument; i--) {

            pDrawModel->anim_data[ i] = pDrawModel->anim_data[ i - 1];
          }

          pDrawModel->anim_data[ ActionArgument] = pDrawModelCurAnim;
        }

        DrawModelCurframe = 0;
	  		IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
      }
		}
    break;
  case IQE_ANIMATE_ACTION_SHIFT_UP:  // Shift named animation up (use argument)
		if( pDrawModel && pDrawModel->anim_count >= 1) {

      if( ActionArgument >= 1 && ActionArgument < pDrawModel->anim_count) {  // in range

        // exchange animations

        pDrawModelCurAnim = pDrawModel->anim_data[ ActionArgument];

        pDrawModel->anim_data[ ActionArgument] = pDrawModel->anim_data[ ActionArgument - 1];
        pDrawModel->anim_data[ ActionArgument - 1] = pDrawModelCurAnim;

        DrawModelCurframe = 0;
	  		IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
      }
		}
    break;
  case IQE_ANIMATE_ACTION_SHIFT_DOWN:  // Shift named animation down (use argument)
		if( pDrawModel && pDrawModel->anim_count >= 1) {

      if( ActionArgument >= 0 && ActionArgument < pDrawModel->anim_count - 1) {  // in range

        // exchange animations

        pDrawModelCurAnim = pDrawModel->anim_data[ ActionArgument];

        pDrawModel->anim_data[ ActionArgument] = pDrawModel->anim_data[ ActionArgument + 1];
        pDrawModel->anim_data[ ActionArgument + 1] = pDrawModelCurAnim;

        DrawModelCurframe = 0;
	  		IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
      }
		}
    break;
  case IQE_ANIMATE_ACTION_NUM_FRAMES:  // Change number of frames of current animation (use argument)
		if( pDrawModel && pDrawModelCurAnim) {

      if( ActionArgument < pDrawModelCurAnim->len) {  // Delete frames

        // NOTE: We keep minimum one animation
        while( pDrawModelCurAnim->data != NULL &&  pDrawModelCurAnim->len > 1 && ActionArgument < pDrawModelCurAnim->len) {  // Still have to delete

          free( pDrawModelCurAnim->data[ pDrawModelCurAnim->len - 1]);  // Delete last

          pDrawModelCurAnim->data[ pDrawModelCurAnim->len - 1] = NULL;

          pDrawModelCurAnim->len -= 1; // one less
        }

        DrawModelCurframe = 0;

        // Preset KeyFlags

        IqeB_AnimKeyFlagsPresetAnimation( pDrawModelCurAnim, pDrawModel->anim_poses);

        // ...

        IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);

      } else if( ActionArgument > pDrawModelCurAnim->len) {  // Add frames

        int bone_count;
        struct pose *pPose;

        bone_count = 0;
        if( pDrawModel->skel != NULL && pDrawModel->skel->joint_count > 0) {

          bone_count = pDrawModel->skel->joint_count;
        }

        if( pDrawModel->anim_count > 0 && pDrawModel->anim_poses > 0) {  // Already have animatins

          if( bone_count != pDrawModel->anim_poses) {   // Security test, must be equal

            bone_count = 0;  // reset
          }
        }

        while( bone_count > 0 &&          // Have enought bones to add animation
               ActionArgument > pDrawModelCurAnim->len) {    // Still have to add

          // Add one frame
          pPose = pushframe( pDrawModelCurAnim, bone_count);

          // And preset the pose with the skeleton

          memcpy( pPose, pDrawModel->skel->pose, sizeof( struct pose) * bone_count);
        }

        // Preset KeyFlags

        if( bone_count > 0 &&          // Have enought bones to add animation
            pDrawModelCurAnim->len > 0) {

          IqeB_AnimKeyFlagsPresetAnimation( pDrawModelCurAnim, pDrawModel->anim_poses);
        }

        // ...

        IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
      }
		}
  }  // end switch
}

/************************************************************************************
 * IqeB_AnimFindByName
 *
 * Find an animation by the given name.
 *
 * Return:   >= 0:  Index of the given name
 *            < 0:  Animation not found
 */

int IqeB_AnimFindByName( IQE_model *pModel, char *pName)
{
  int iAnimation;

  if( pModel == NULL ||
      pModel->anim_count <= 0 ||
      pModel->anim_poses <= 0 ||
      pModel->anim_data == NULL) { // security test

    return( -1);  // Not found
  }

  for( iAnimation = 0; iAnimation < pModel->anim_count; iAnimation++) {

    if( _stricmp( pModel->anim_data[ iAnimation]->name, pName) == 0) {  // Got it

      return( iAnimation);
    }
  }

  return( -1);  // Not found
}

/************************************************************************************
 * IqeB_AnimRotToAnglesFrame
 *
 * For the poses of a frame:
 * Convert rotation quaterion to angles in degree
 */

void IqeB_AnimRotToAnglesFrame( struct pose *pPose, int nPoses)
{
  int iPose;
  vec3 Angles;

  for( iPose = 0; iPose < nPoses; iPose++, pPose++) {

    quat_to_rotation( pPose->rotation, Angles);

    pPose->angles[ 0] = RAD2DEG( Angles[ 0]);
    pPose->angles[ 1] = RAD2DEG( Angles[ 1]);
    pPose->angles[ 2] = RAD2DEG( Angles[ 2]);

    // Enshure angles in range -180.0  .. 180.0

    while( pPose->angles[ 0] < -180.0) pPose->angles[ 0] += 360.0;
    while( pPose->angles[ 0] >  180.0) pPose->angles[ 0] -= 360.0;
    while( pPose->angles[ 1] < -180.0) pPose->angles[ 1] += 360.0;
    while( pPose->angles[ 1] >  180.0) pPose->angles[ 1] -= 360.0;
    while( pPose->angles[ 2] < -180.0) pPose->angles[ 2] += 360.0;
    while( pPose->angles[ 2] >  180.0) pPose->angles[ 2] -= 360.0;

    // catch tiny values, clip them to 0
    if( pPose->angles[ 0] > -0.0001 && pPose->angles[ 0] < 0.0001) pPose->angles[ 0] = 0.0;
    if( pPose->angles[ 1] > -0.0001 && pPose->angles[ 1] < 0.0001) pPose->angles[ 1] = 0.0;
    if( pPose->angles[ 2] > -0.0001 && pPose->angles[ 2] < 0.0001) pPose->angles[ 2] = 0.0;
  }
}

/************************************************************************************
 * IqeB_AnimRotToAnglesAnimation
 *
 * For all frames of an animation:
 * Convert rotation quaterion to angles in degree
 */

void IqeB_AnimRotToAnglesAnimation( struct anim *pAnim, int nPoses)
{
  int iFrame;

  for( iFrame = 0; iFrame < pAnim->len; iFrame++) {

    IqeB_AnimRotToAnglesFrame( pAnim->data[ iFrame], nPoses);
  }
}

/************************************************************************************
 * IqeB_AnimRotToAnglesModel
 *
 * For all animations of a model:
 * Convert rotation quaterion to angles in degree
 */

void IqeB_AnimRotToAnglesModel( IQE_model *pModel)
{
  int iAnimation;

  if( pModel->anim_count <= 0 ||
      pModel->anim_poses <= 0 ||
      pModel->anim_data == NULL) { // security test

    return;  // Nothing to do
  }

  for( iAnimation = 0; iAnimation < pModel->anim_count; iAnimation++) {

    IqeB_AnimRotToAnglesAnimation( pModel->anim_data[ iAnimation], pModel->anim_poses);
  }
}

/************************************************************************************
 * IqeB_AnimAnglesToRotPose
 *
 * For a single pose:
 * Convert angles (in degree) to rotation quaterion
 */

void IqeB_AnimAnglesToRotPose( struct pose *pPose)
{
  vec3 Angles;

  // Enshure angles in range -180.0  .. 180.0

  while( pPose->angles[ 0] < -180.0) pPose->angles[ 0] += 360.0;
  while( pPose->angles[ 0] >  180.0) pPose->angles[ 0] -= 360.0;
  while( pPose->angles[ 1] < -180.0) pPose->angles[ 1] += 360.0;
  while( pPose->angles[ 1] >  180.0) pPose->angles[ 1] -= 360.0;
  while( pPose->angles[ 2] < -180.0) pPose->angles[ 2] += 360.0;
  while( pPose->angles[ 2] >  180.0) pPose->angles[ 2] -= 360.0;

  // catch tiny values, clip them to 0
  if( pPose->angles[ 0] > -0.0001 && pPose->angles[ 0] < 0.0001) pPose->angles[ 0] = 0.0;
  if( pPose->angles[ 1] > -0.0001 && pPose->angles[ 1] < 0.0001) pPose->angles[ 1] = 0.0;
  if( pPose->angles[ 2] > -0.0001 && pPose->angles[ 2] < 0.0001) pPose->angles[ 2] = 0.0;

  // ...

  if( pPose->angles[ 0] == 0.0 &&  // Test for no rotation
      pPose->angles[ 1] == 0.0 &&
      pPose->angles[ 2] == 0.0) {

    quat_set_identity( pPose->rotation); // default rotation
  } else {

    Angles[ 0] = DEG2RAD( pPose->angles[ 0]);
    Angles[ 1] = DEG2RAD( pPose->angles[ 1]);
    Angles[ 2] = DEG2RAD( pPose->angles[ 2]);

    quat_from_rotation( pPose->rotation, Angles);
  }
}

/************************************************************************************
 * IqeB_AnimKeyFlagsPresetAnimation
 *
 * For this animation:
 * Preset keyflags of the animations.
 */

void IqeB_AnimKeyFlagsPresetAnimation( struct anim *pAnim, int nPoses)
{
  int iFrame, iPose, iAxis;
  float DeltaOfDeltas;
  struct pose *pPoseThis;
  struct pose *pPoseLast;
	struct pose Deltas1[ MAXBONE], Deltas2[ MAXBONE];

	pPoseLast = NULL;  // No last pose

  for( iFrame = 0; iFrame <= pAnim->len; iFrame++) {   // NOTE: We make one more loop than we have frames

    pPoseLast = pPoseThis;                             // Remember last pose
    pPoseThis = pAnim->data[ iFrame % pAnim->len];     // Wrap around

    if( iFrame == 0) {   // The first frame

      // First frame, set all keyframe bits

      for( iPose = 0; iPose < nPoses; iPose++) {

        pPoseThis[ iPose].KeyFlags = KEYFLAGS_ALL;  // Set all keyflag bits

        for( iAxis = 0; iAxis < 3; iAxis++) {

          // Enshure angles in range -180.0  .. 180.0

          while( pPoseThis[ iPose].angles[ iAxis] < -180.0) pPoseThis[ iPose].angles[ iAxis] += 360.0;
          while( pPoseThis[ iPose].angles[ iAxis] >  180.0) pPoseThis[ iPose].angles[ iAxis] -= 360.0;
        }
      }
    } else {

      // Calculate changes from this to last

      for( iPose = 0; iPose < nPoses; iPose++) {

        for( iAxis = 0; iAxis < 3; iAxis++) {

          Deltas1[ iPose].location[ iAxis] = pPoseThis[ iPose].location[ iAxis] - pPoseLast[ iPose].location[ iAxis];
          Deltas1[ iPose].scale[ iAxis]    = pPoseThis[ iPose].scale[ iAxis]    - pPoseLast[ iPose].scale[ iAxis];

          // Enshure angles in range -180.0  .. 180.0

          while( pPoseThis[ iPose].angles[ iAxis] < -180.0) pPoseThis[ iPose].angles[ iAxis] += 360.0;
          while( pPoseThis[ iPose].angles[ iAxis] >  180.0) pPoseThis[ iPose].angles[ iAxis] -= 360.0;

          Deltas1[ iPose].angles[ iAxis] = pPoseThis[ iPose].angles[ iAxis] - pPoseLast[ iPose].angles[ iAxis];

          // For deltas, keep rang -180.0 .. 180.0

          if( Deltas1[ iPose].angles[ iAxis] > 180.0) {

            Deltas1[ iPose].angles[ iAxis] -= 360.0;

          } else if( Deltas1[ iPose].angles[ iAxis] < -180.0) {

            Deltas1[ iPose].angles[ iAxis] += 360.0;
          }
        }
      }

      // Check changes between the deltas
      // If to big, set the key flags for the last pose

      if( iFrame > 1) {   // Have more than two frams

        for( iPose = 0; iPose < nPoses; iPose++) {

          pPoseLast[ iPose].KeyFlags = 0;  // Reset all keyflag bits

          for( iAxis = 0; iAxis < 3; iAxis++) {

            // Locations
            DeltaOfDeltas = Deltas1[ iPose].location[ iAxis] - Deltas2[ iPose].location[ iAxis];
            if( DeltaOfDeltas < 0.0) {
              DeltaOfDeltas = - DeltaOfDeltas;
            }
            if( DeltaOfDeltas >= 0.01 ) {  // Change is over delta

              pPoseLast[ iPose].KeyFlags |= KEYFLAG_LOC_0 << iAxis;  // Set keyflag bit
            }

            // Scale
            DeltaOfDeltas = Deltas1[ iPose].scale[ iAxis] - Deltas2[ iPose].scale[ iAxis];
            if( DeltaOfDeltas < 0.0) {
              DeltaOfDeltas = - DeltaOfDeltas;
            }
            if( DeltaOfDeltas >= 0.01 ) {  // Change is over delta

              pPoseLast[ iPose].KeyFlags |= KEYFLAG_SCALE_0 << iAxis;  // Set keyflag bit
            }

            // Angles
            DeltaOfDeltas = Deltas1[ iPose].angles[ iAxis] - Deltas2[ iPose].angles[ iAxis];
            if( DeltaOfDeltas < 0.0) {
              DeltaOfDeltas = - DeltaOfDeltas;
            }

            if( DeltaOfDeltas >= 0.01 ) {  // Change is over delta

              pPoseLast[ iPose].KeyFlags |= KEYFLAG_ANG_0 << iAxis;  // Set keyflag bit
            }
          }
        }
      }

      // Copy this deltas to last

      memcpy( Deltas2, Deltas1, sizeof( Deltas2));
    }
  }
}

/************************************************************************************
 * IqeB_AnimKeyFlagsPresetModel
 *
 * For all animations of a model:
 * Preset keyflags of the animations.
 */

void IqeB_AnimKeyFlagsPresetModel( IQE_model *pModel)
{
  int iAnimation;

  if( pModel->anim_count <= 0 ||
      pModel->anim_poses <= 0 ||
      pModel->anim_data == NULL) { // security test

    return;  // Nothing to do
  }

  for( iAnimation = 0; iAnimation < pModel->anim_count; iAnimation++) {

    IqeB_AnimKeyFlagsPresetAnimation( pModel->anim_data[ iAnimation], pModel->anim_poses);
  }
}

/************************************************************************************
 * IqeB_AnimKeyFlagsInterpolateFrames
 *
 * For this animation:
 * Recalc poses between given frames.
 * NOTES: * 'KeyFlagBit' has only one bit set.
 *        * The begin/end frame must not be recalculated.
 *        * The begin/end frame has (usual the) 'KeyFlagBit' set
 *        * 'iFrameBegin' is lower than 'iFrameEnd'.
 *        * 'iFrameEnd' can be one higher than the last pose element.
 *           See IqeB_AnimKeyFlagsRecalcAnimation() frame loop.
 */

static void IqeB_AnimKeyFlagsInterpolateFrames( struct anim *pAnim, int nPoses,
                     int iFrameBegin, int iFrameEnd, int iPose, int KeyFlagBit, int iAxis,
                     struct pose *pBoseBeginValue, struct pose *pBoseEndValue)
{
  struct pose *pPoseThis;
  float ValBegin, ValDelta;
  int FrameDist, iFrame, iDelta;

  // Get number of frames

  FrameDist = iFrameEnd - iFrameBegin;   // Numer of deltas to process

  if( FrameDist <= 1) {                  // 'iFrameBegin' and 'iFrameEnd' are neighbours

    return;                              // Have nothing to do
  }

  // ...

  iFrameBegin = iFrameBegin + 1;         // next frame
  iFrameEnd   = iFrameEnd - 1;           // previous frame

  if( KeyFlagBit & KEYFLAG_LOC_ALL) {

    ValBegin = pBoseBeginValue->location[ iAxis];
    ValDelta = pBoseEndValue->location[ iAxis] - ValBegin;

    iDelta = 1;
    for( iFrame = iFrameBegin; iFrame <= iFrameEnd; iFrame++) {   // NOTE: We make one more loop than we have frames

      pPoseThis = pAnim->data[ iFrame % pAnim->len];     // Wrap around
      pPoseThis += iPose;
      pPoseThis->location[ iAxis] = ValBegin + (ValDelta * iDelta) / FrameDist;

      iDelta += 1;
    }

  } else if( KeyFlagBit & KEYFLAG_ANG_ALL) {

    ValBegin = pBoseBeginValue->angles[ iAxis];
    ValDelta = pBoseEndValue->angles[ iAxis] - ValBegin;

    iDelta = 1;
    for( iFrame = iFrameBegin; iFrame <= iFrameEnd; iFrame++) {   // NOTE: We make one more loop than we have frames

      pPoseThis = pAnim->data[ iFrame % pAnim->len];     // Wrap around
      pPoseThis += iPose;
      pPoseThis->angles[ iAxis] = ValBegin + (ValDelta * iDelta) / FrameDist;

      iDelta += 1;
    }

  } else if( KeyFlagBit & KEYFLAG_SCALE_ALL) {

    ValBegin = pBoseBeginValue->scale[ iAxis];
    ValDelta = pBoseEndValue->scale[ iAxis] - ValBegin;

    iDelta = 1;
    for( iFrame = iFrameBegin; iFrame <= iFrameEnd; iFrame++) {   // NOTE: We make one more loop than we have frames

      pPoseThis = pAnim->data[ iFrame % pAnim->len];     // Wrap around
      pPoseThis += iPose;
      pPoseThis->scale[ iAxis] = ValBegin + (ValDelta * iDelta) / FrameDist;

      iDelta += 1;
    }

  }
}

/************************************************************************************
 * IqeB_AnimKeyFlagsRecalcAnimation
 *
 * For this animation:
 * Recalc poses. Interpolate poses between set keyflags.
 */

void IqeB_AnimKeyFlagsRecalcAnimation( struct anim *pAnim, int nPoses)
{
  int iFrame, iPose, iAxis, KeyFlagsSetThis, KeyFlagsSetLast, KeyFlagBit;
  int FrameLastLocation[ 3], FrameLastScale[ 3], FrameLastAngle[ 3];
  struct pose *pPoseThis, PoseLast;

  for( iPose = 0; iPose < nPoses; iPose++) {           // Over all Poses

  	KeyFlagsSetLast = 0;

    for( iFrame = 0; iFrame <= pAnim->len; iFrame++) {   // NOTE: We make one more loop than we have frames

      pPoseThis = pAnim->data[ iFrame % pAnim->len];     // Wrap around
      pPoseThis += iPose;

      KeyFlagsSetThis = pPoseThis->KeyFlags;

      for( iAxis = 0; iAxis < 3; iAxis++) {

        // Enshure angles in range -180.0  .. 180.0

        while( pPoseThis->angles[ iAxis] < -180.0) pPoseThis->angles[ iAxis] += 360.0;
        while( pPoseThis->angles[ iAxis] >  180.0) pPoseThis->angles[ iAxis] -= 360.0;

        // Locations

        KeyFlagBit = KEYFLAG_LOC_0 << iAxis;   // Work with this keyflag bit

        if( KeyFlagsSetThis & KeyFlagBit) {    // This has a keyflag bit set

          if( KeyFlagsSetLast & KeyFlagBit) {  // Last has a keyflag bit set too

            // Interpolate between this and last

            IqeB_AnimKeyFlagsInterpolateFrames( pAnim, nPoses,
                     FrameLastLocation[ iAxis], iFrame, iPose, KeyFlagBit, iAxis,
                     &PoseLast, pPoseThis);
          }

          // Set this as last

          KeyFlagsSetLast |= KeyFlagBit;
          FrameLastLocation[ iAxis] = iFrame;
          PoseLast.location[ iAxis] = pPoseThis->location[ iAxis];
        }

        // Scale

        KeyFlagBit = KEYFLAG_SCALE_0 << iAxis;   // Work with this keyflag bit

        if( KeyFlagsSetThis & KeyFlagBit) {    // This has a keyflag bit set

          if( KeyFlagsSetLast & KeyFlagBit) {  // Last has a keyflag bit set too

            // Interpolate between this and last

            IqeB_AnimKeyFlagsInterpolateFrames( pAnim, nPoses,
                     FrameLastScale[ iAxis], iFrame, iPose, KeyFlagBit, iAxis,
                     &PoseLast, pPoseThis);
          }

          // Set this as last

          KeyFlagsSetLast |= KeyFlagBit;
          FrameLastScale[ iAxis] = iFrame;
          PoseLast.scale[ iAxis] = pPoseThis->scale[ iAxis];
        }

        // Angle

        KeyFlagBit = KEYFLAG_ANG_0 << iAxis;   // Work with this keyflag bit

        if( KeyFlagsSetThis & KeyFlagBit) {    // This has a keyflag bit set

          if( KeyFlagsSetLast & KeyFlagBit) {  // Last has a keyflag bit set too

            // Interpolate between this and last

            IqeB_AnimKeyFlagsInterpolateFrames( pAnim, nPoses,
                     FrameLastAngle[ iAxis], iFrame, iPose, KeyFlagBit, iAxis,
                     &PoseLast, pPoseThis);
          }

          // Set this as last

          KeyFlagsSetLast |= KeyFlagBit;
          FrameLastAngle[ iAxis] = iFrame;
          PoseLast.angles[ iAxis] = pPoseThis->angles[ iAxis];
        }
      }
    }

    // Angels (may have) changed, convert to quterion
    for( iFrame = 0; iFrame < pAnim->len; iFrame++) {   // NOTE: We make one more loop than we have frames

      pPoseThis = pAnim->data[ iFrame];
      pPoseThis += iPose;

      IqeB_AnimAnglesToRotPose( pPoseThis);
    }
  }
}

/************************************************************************************
 * IqeB_AnimKeyFlagsChangeAnimation
 *
 * Change the keyfalgs of an animation.
 *
 * iFrameArg:  < 0   Change for all frames
 *            >= 0   Change for named frame
 *
 * iPoseArg:   < 0   Change for all poses
 *            >= 0   Change for named pose
 */

void IqeB_AnimKeyFlagsChangeAnimation( struct anim *pAnim, int nPoses, int iFrameArg, int iPoseArg, int KeyFlagMask, int KeyFlagValue)
{
  int iFrame, FrameBegin, FrameEnd, iPose, PoseBegin, PoseEnd, KeyFlagMaskInv;
  struct pose *pPoseThis;

  // Process arguments

  if( iFrameArg >= 0) {  // This frame

    FrameBegin = iFrameArg;      // First frame
    if( FrameBegin >= pAnim->len) FrameBegin = pAnim->len - 1; // Clippint
    FrameEnd   = FrameBegin + 1; // End after the end
  } else {                       // All frames

    FrameBegin = 0;              // First frame
    FrameEnd   = pAnim->len;     // End after the end
  }

  if( iPoseArg >= 0) {           // This frame

    PoseBegin = iPoseArg;        // First frame
    if( PoseBegin >= nPoses) PoseBegin = nPoses - 1; // Clippint
    PoseEnd   = PoseBegin + 1;   // End after the end
  } else {                       // All frames

    PoseBegin = 0;               // First frame
    PoseEnd   = nPoses;          // End after the end
  }

  KeyFlagMaskInv = ~ KeyFlagMask;                // Inverted mask
  KeyFlagValue   = KeyFlagValue & KeyFlagMask;   // Enshure, only mask bits stay

  // Process

  for( iFrame = FrameBegin; iFrame < FrameEnd; iFrame++) {  // Loop over frames

    pPoseThis = pAnim->data[ iFrame];                       // Point to this frame data
    pPoseThis += PoseBegin;                                 // Offset to the begin pose

    for( iPose = PoseBegin; iPose < PoseEnd; iPose++) {     // Over all Poses

      pPoseThis->KeyFlags = (pPoseThis->KeyFlags & KeyFlagMaskInv) | KeyFlagValue;

      pPoseThis += 1;                                       // Point to next
    }
  }
}

/************************************************************************************
 * IqeB_AnimNormalizeModel
 *
 * Normalizes the animations of a model.
 *
 * Also normalize the skeleton too.
 *
 * Return:   0 OK
 *          -1 Skeleton missing or other joint count than animations.
 */

int IqeB_AnimNormalizeModel( IQE_model *pModel)
{

  //
  // have to normalize the skeleton ?
  //

  if( pModel != NULL && pModel->skel != NULL &&
      pModel->skel->IsNormalized == false) {   // Skeleton is not normalized

    // No rotations ...

    IqeB_ModelSkelNormalize( pModel);

  }

  return( 0);  // return OK
}

/************************************************************************************
 * IqeB_AnimPrepareAfterLoad
 *
 * Prepare the animations of a model after the model has been loaded.
 * For all animations of a model:
 * * Convert rotation quaterion to angles in degree
 * * Flag keyframes
 */

void IqeB_AnimPrepareAfterLoad( IQE_model *pModel)
{

	// Enshure numer of poses set

	if( pModel->anim_count > 0 && pModel->anim_poses == 0 &&     // Have animations and poses not set
      pModel->skel != NULL && pModel->skel->joint_count > 0) { // but have a skeleton

	  pModel->anim_poses = pModel->skel->joint_count;            // set number of poses in an animation frame

	} if( pModel->anim_count == 0 && pModel->anim_poses > 0) {   // no animations but poses set
	  pModel->anim_poses = 0;                                    // reset number of poses in an animation frame
	}

  // Convert rotation quaterion to angles in degree

  IqeB_AnimRotToAnglesModel( pModel);

  // Preset the keyflags

  IqeB_AnimKeyFlagsPresetModel( pModel);
}

/*********************************** End Of File ***********************************/
