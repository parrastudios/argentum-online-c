/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Memory/General.h>
#include <DataBase/StoredProcedureArgument.h>

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef void * (*StoredProcedureAgumentValueInitializeImpl)(union StoredProcedureArgumentDataValueType *);
typedef void * (*StoredProcedureAgumentValueCopyImpl)(union StoredProcedureArgumentDataValueType *, union StoredProcedureArgumentDataValueType *);
typedef StoredProcedureArgumentLengthType * (*StoredProcedureAgumentValueLengthImpl)(union StoredProcedureArgumentDataValueType *);
typedef void (*StoredProcedureAgumentValueDestroyImpl)(union StoredProcedureArgumentDataValueType *);

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
union StoredProcedureArgumentDataType
{
	StoredProcedureArgumentDataValueType		Value;			//< Data type value
	StoredProcedureArgumentDataRefType			Ref;			//< Data type pointer
};

struct StoredProcedureArgumentType
{
	enum StoredProcedureArgumentTypeId			Type;			//< Type id
	union StoredProcedureArgumentDataType		Data;			//< Data representation
};

struct StoredProcedureArgumentListType
{
	struct StoredProcedureArgumentType *		List;			//< Argument list pointer
	enum StoredProcedureArgumentPassing			Passing;		//< Argument passing mechanism for all argument list
	uinteger									Size;			//< Argument size
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
/// Initialize a stored procedure argument value (tiny)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueInitializeTiny(union StoredProcedureArgumentDataValueType * Value)
{
	Value->Tiny = INT8_SUFFIX(0);

	return &Value->Tiny;
}

/////////////////////////////////////////////////////////////
/// Initialize a stored procedure argument value (short)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueInitializeShort(union StoredProcedureArgumentDataValueType * Value)
{
	Value->Short = INT16_SUFFIX(0);

	return &Value->Short;
}

/////////////////////////////////////////////////////////////
/// Initialize a stored procedure argument value (long)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueInitializeLong(union StoredProcedureArgumentDataValueType * Value)
{
	Value->Long = INT32_SUFFIX(0);

	return &Value->Long;
}

/////////////////////////////////////////////////////////////
/// Initialize a stored procedure argument value (longlong)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueInitializeLongLong(union StoredProcedureArgumentDataValueType * Value)
{
	Value->LongLong = INT64_SUFFIX(0);

	return &Value->LongLong;
}


/////////////////////////////////////////////////////////////
/// Initialize a stored procedure argument value (float)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueInitializeFloat(union StoredProcedureArgumentDataValueType * Value)
{
	Value->Float = FLOAT_SUFFIX(0.0);

	return &Value->Float;
}

/////////////////////////////////////////////////////////////
/// Initialize a stored procedure argument value (double)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueInitializeDouble(union StoredProcedureArgumentDataValueType * Value)
{
	Value->Double = DOUBLE_SUFFIX(0);

	return &Value->Double;
}

/////////////////////////////////////////////////////////////
/// Initialize a stored procedure argument value (string)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueInitializeString(union StoredProcedureArgumentDataValueType * Value)
{
	Value->String.Length = UINTEGER_SUFFIX(0);

	Value->String.Ptr = MemoryAllocate(sizeof(char) * STORED_PROCEDURE_ARGUMENT_STRING_SIZE);

	return Value->String.Ptr;
}

/////////////////////////////////////////////////////////////
/// Initialize a stored procedure argument value (blob)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueInitializeBlob(union StoredProcedureArgumentDataValueType * Value)
{
	Value->Blob.Size = UINTEGER_SUFFIX(0);

	Value->Blob.Ptr = MemoryAllocate(sizeof(int8) * STORED_PROCEDURE_ARGUMENT_BLOB_SIZE);

	return Value->Blob.Ptr;
}

/////////////////////////////////////////////////////////////
/// Initialize a stored procedure argument value (null)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueInitializeNull(union StoredProcedureArgumentDataValueType * Value)
{
	return NULL;
}

/////////////////////////////////////////////////////////////
/// Initialize a stored procedure argument value
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueInitialize(union StoredProcedureArgumentDataValueType * Value, enum StoredProcedureArgumentTypeId Type)
{
	if (Value && Type < STORED_PROCEDURE_COUNT)
	{
		static StoredProcedureAgumentValueInitializeImpl ArgumentInitializeImplMapping[STORED_PROCEDURE_COUNT] =
		{
			ArrayInit(STORED_PROCEDURE_TINY, &StoredProcedureArgumentValueInitializeTiny),
			ArrayInit(STORED_PROCEDURE_SHORT, &StoredProcedureArgumentValueInitializeShort),
			ArrayInit(STORED_PROCEDURE_LONG, &StoredProcedureArgumentValueInitializeLong),
			ArrayInit(STORED_PROCEDURE_LONG_LONG, &StoredProcedureArgumentValueInitializeLongLong),
			ArrayInit(STORED_PROCEDURE_FLOAT, &StoredProcedureArgumentValueInitializeFloat),
			ArrayInit(STORED_PROCEDURE_DOUBLE, &StoredProcedureArgumentValueInitializeDouble),
			ArrayInit(STORED_PROCEDURE_DATE, &StoredProcedureArgumentValueInitializeString),
			ArrayInit(STORED_PROCEDURE_TIME, &StoredProcedureArgumentValueInitializeString),
			ArrayInit(STORED_PROCEDURE_STRING, &StoredProcedureArgumentValueInitializeString),
			ArrayInit(STORED_PROCEDURE_BLOB, &StoredProcedureArgumentValueInitializeBlob),
			ArrayInit(STORED_PROCEDURE_NULL, &StoredProcedureArgumentValueInitializeNull)
		};

		return ArgumentInitializeImplMapping[Type](Value);
	}

	return NULL;
}

/////////////////////////////////////////////////////////////
/// Return size of an argument type
/////////////////////////////////////////////////////////////
StoredProcedureArgumentSizeType StoredProcedureArgumentSizeOf(enum StoredProcedureArgumentTypeId Type)
{
	if (Type < STORED_PROCEDURE_COUNT)
	{
		static StoredProcedureArgumentSizeType ArgumentSizeOfMapping[STORED_PROCEDURE_COUNT] =
		{
			ArrayInit(STORED_PROCEDURE_TINY, sizeof(int8)),
			ArrayInit(STORED_PROCEDURE_SHORT, sizeof(int16)),
			ArrayInit(STORED_PROCEDURE_LONG, sizeof(int32)),
			ArrayInit(STORED_PROCEDURE_LONG_LONG, sizeof(int64)),
			ArrayInit(STORED_PROCEDURE_FLOAT, sizeof(float32)),
			ArrayInit(STORED_PROCEDURE_DOUBLE, sizeof(float64)),
			ArrayInit(STORED_PROCEDURE_DATE, STORED_PROCEDURE_ARGUMENT_STRING_SIZE),
			ArrayInit(STORED_PROCEDURE_TIME, STORED_PROCEDURE_ARGUMENT_STRING_SIZE),
			ArrayInit(STORED_PROCEDURE_STRING, STORED_PROCEDURE_ARGUMENT_STRING_SIZE),
			ArrayInit(STORED_PROCEDURE_BLOB, STORED_PROCEDURE_ARGUMENT_BLOB_SIZE),
			ArrayInit(STORED_PROCEDURE_NULL, UINTEGER_SUFFIX(0))
		};

		return ArgumentSizeOfMapping[Type];
	}

	return UINTEGER_SUFFIX(0);
}

/////////////////////////////////////////////////////////////
/// Return length of an argument type if any (null)
/////////////////////////////////////////////////////////////
StoredProcedureArgumentLengthType * StoredProcedureArgumenValueLengthNull(union StoredProcedureArgumentDataValueType * Value)
{
	return NULL;
}

/////////////////////////////////////////////////////////////
/// Return length of an argument type if any (string)
/////////////////////////////////////////////////////////////
StoredProcedureArgumentLengthType * StoredProcedureArgumenValueLengthString(union StoredProcedureArgumentDataValueType * Value)
{
	return &Value->String.Length;
}

/////////////////////////////////////////////////////////////
/// Return length of an argument type if any (blob)
/////////////////////////////////////////////////////////////
StoredProcedureArgumentLengthType * StoredProcedureArgumenValueLengthBlob(union StoredProcedureArgumentDataValueType * Value)
{
	return &Value->Blob.Size;
}

/////////////////////////////////////////////////////////////
/// Return length of an argument type if any
/////////////////////////////////////////////////////////////
StoredProcedureArgumentLengthType * StoredProcedureArgumenValueLength(union StoredProcedureArgumentDataValueType * Value, enum StoredProcedureArgumentTypeId Type)
{
	if (Value && Type < STORED_PROCEDURE_COUNT)
	{
		static StoredProcedureAgumentValueLengthImpl ArgumentLengthImplMapping[STORED_PROCEDURE_COUNT] =
		{
			ArrayInit(STORED_PROCEDURE_TINY, &StoredProcedureArgumenValueLengthNull),
			ArrayInit(STORED_PROCEDURE_SHORT, &StoredProcedureArgumenValueLengthNull),
			ArrayInit(STORED_PROCEDURE_LONG, &StoredProcedureArgumenValueLengthNull),
			ArrayInit(STORED_PROCEDURE_LONG_LONG, &StoredProcedureArgumenValueLengthNull),
			ArrayInit(STORED_PROCEDURE_FLOAT, &StoredProcedureArgumenValueLengthNull),
			ArrayInit(STORED_PROCEDURE_DOUBLE, &StoredProcedureArgumenValueLengthNull),
			ArrayInit(STORED_PROCEDURE_DATE, &StoredProcedureArgumenValueLengthString),
			ArrayInit(STORED_PROCEDURE_TIME, &StoredProcedureArgumenValueLengthString),
			ArrayInit(STORED_PROCEDURE_STRING, &StoredProcedureArgumenValueLengthString),
			ArrayInit(STORED_PROCEDURE_BLOB, &StoredProcedureArgumenValueLengthBlob),
			ArrayInit(STORED_PROCEDURE_NULL, &StoredProcedureArgumenValueLengthNull)
		};

		return ArgumentLengthImplMapping[Type](Value);
	}

	return NULL;
}

/////////////////////////////////////////////////////////////
/// Copy a stored procedure argument value (tiny)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueCopyTiny(union StoredProcedureArgumentDataValueType * Dest, union StoredProcedureArgumentDataValueType * Source)
{
	Dest->Tiny = Source->Tiny;

	return &Dest->Tiny;
}

/////////////////////////////////////////////////////////////
/// Copy a stored procedure argument value (short)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueCopyShort(union StoredProcedureArgumentDataValueType * Dest, union StoredProcedureArgumentDataValueType * Source)
{
	Dest->Short = Source->Short;

	return &Dest->Short;
}

/////////////////////////////////////////////////////////////
/// Copy a stored procedure argument value (long)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueCopyLong(union StoredProcedureArgumentDataValueType * Dest, union StoredProcedureArgumentDataValueType * Source)
{
	Dest->Long = Source->Long;

	return &Dest->Long;
}

/////////////////////////////////////////////////////////////
/// Copy a stored procedure argument value (longlong)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueCopyLongLong(union StoredProcedureArgumentDataValueType * Dest, union StoredProcedureArgumentDataValueType * Source)
{
	Dest->LongLong = Source->LongLong;

	return &Dest->LongLong;
}


/////////////////////////////////////////////////////////////
/// Copy a stored procedure argument value (float)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueCopyFloat(union StoredProcedureArgumentDataValueType * Dest, union StoredProcedureArgumentDataValueType * Source)
{
	Dest->Float = Source->Float;

	return &Dest->Float;
}

/////////////////////////////////////////////////////////////
/// Copy a stored procedure argument value (double)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueCopyDouble(union StoredProcedureArgumentDataValueType * Dest, union StoredProcedureArgumentDataValueType * Source)
{
	Dest->Double = Source->Double;

	return &Dest->Double;
}

/////////////////////////////////////////////////////////////
/// Copy a stored procedure argument value (string)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueCopyString(union StoredProcedureArgumentDataValueType * Dest, union StoredProcedureArgumentDataValueType * Source)
{
	uinteger Size = Source->String.Length + 1;

	Dest->String.Ptr = MemoryAllocate(sizeof(char) * Size);

	if (Dest->String.Ptr)
	{
		Dest->String.Length = Source->String.Length;

		MemoryCopy(Dest->String.Ptr, Source->String.Ptr, Size);

		Dest->String.Ptr[Dest->String.Length] = NULLCHAR;

		return Dest->String.Ptr;
	}

	Dest->String.Length = UINTEGER_SUFFIX(0);

	return NULL;
}

/////////////////////////////////////////////////////////////
/// Copy a stored procedure argument value (blob)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueCopyBlob(union StoredProcedureArgumentDataValueType * Dest, union StoredProcedureArgumentDataValueType * Source)
{
	Dest->Blob.Size = Source->Blob.Size;

	Dest->Blob.Ptr = MemoryAllocate(sizeof(int8) * Dest->Blob.Size);

	MemoryCopy(Dest->Blob.Ptr, Source->Blob.Ptr, Dest->Blob.Size);

	return Dest->Blob.Ptr;
}

/////////////////////////////////////////////////////////////
/// Copy a stored procedure argument value (null)
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueCopyNull(union StoredProcedureArgumentDataValueType * Dest, union StoredProcedureArgumentDataValueType * Source)
{
	return NULL;
}

/////////////////////////////////////////////////////////////
/// Copy a stored procedure argument value
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueCopy(union StoredProcedureArgumentDataValueType * Dest, union StoredProcedureArgumentDataValueType * Source, enum StoredProcedureArgumentTypeId Type)
{
	if (Dest && Source && Type < STORED_PROCEDURE_COUNT)
	{
		static StoredProcedureAgumentValueCopyImpl ArgumentCopyImplMapping[STORED_PROCEDURE_COUNT] =
		{
			ArrayInit(STORED_PROCEDURE_TINY, &StoredProcedureArgumentValueCopyTiny),
			ArrayInit(STORED_PROCEDURE_SHORT, &StoredProcedureArgumentValueCopyShort),
			ArrayInit(STORED_PROCEDURE_LONG, &StoredProcedureArgumentValueCopyLong),
			ArrayInit(STORED_PROCEDURE_LONG_LONG, &StoredProcedureArgumentValueCopyLongLong),
			ArrayInit(STORED_PROCEDURE_FLOAT, &StoredProcedureArgumentValueCopyFloat),
			ArrayInit(STORED_PROCEDURE_DOUBLE, &StoredProcedureArgumentValueCopyDouble),
			ArrayInit(STORED_PROCEDURE_DATE, &StoredProcedureArgumentValueCopyString),
			ArrayInit(STORED_PROCEDURE_TIME, &StoredProcedureArgumentValueCopyString),
			ArrayInit(STORED_PROCEDURE_STRING, &StoredProcedureArgumentValueCopyString),
			ArrayInit(STORED_PROCEDURE_BLOB, &StoredProcedureArgumentValueCopyBlob),
			ArrayInit(STORED_PROCEDURE_NULL, &StoredProcedureArgumentValueCopyNull)
		};

		return ArgumentCopyImplMapping[Type](Dest, Source);
	}

	return NULL;
}

/////////////////////////////////////////////////////////////
/// Create stored procedure argument list
/////////////////////////////////////////////////////////////
StoredProcedureArgumentList StoredProcedureArgumentCreate(enum StoredProcedureArgumentPassing Passing, uinteger Size)
{
	StoredProcedureArgumentList Arguments = (StoredProcedureArgumentList)MemoryAllocate(sizeof(struct StoredProcedureArgumentListType));

	if (Arguments)
	{
		Arguments->Passing = Passing;
		Arguments->Size = Size;

		if (Arguments->Size > 0)
		{
			Arguments->List = (struct StoredProcedureArgumentType *)MemoryAllocate(sizeof(struct StoredProcedureArgumentType) * Arguments->Size);

			if (Arguments->List)
			{
				uinteger Argument;

				for (Argument = 0; Argument < Arguments->Size; ++Argument)
				{
					Arguments->List[Argument].Type = STORED_PROCEDURE_INVALID;

					MemorySet(&Arguments->List[Argument].Data, 0, sizeof(union StoredProcedureArgumentDataType));
				}
			}
		}
		else
		{
			Arguments->List = NULL;
		}
	}

	return Arguments;
}

/////////////////////////////////////////////////////////////
/// Copy stored procedure argument list
/////////////////////////////////////////////////////////////
StoredProcedureArgumentList StoredProcedureArgumentCopy(StoredProcedureArgumentList Arguments)
{
	StoredProcedureArgumentList Copy = (StoredProcedureArgumentList)MemoryAllocate(sizeof(struct StoredProcedureArgumentListType));

	if (Arguments)
	{
		Copy->Passing = Arguments->Passing;
		Copy->Size = Arguments->Size;

		if (Copy->Size > 0)
		{
			Copy->List = (struct StoredProcedureArgumentType *)MemoryAllocate(sizeof(struct StoredProcedureArgumentType) * Copy->Size);

			if (Copy->List)
			{
				if (Copy->Passing == STORED_PROCEDURE_BY_VALUE)
				{
					uinteger Argument;

					for (Argument = 0; Argument < Copy->Size; ++Argument)
					{
						struct StoredProcedureArgumentType * ArgPtr = &Arguments->List[Argument];
						struct StoredProcedureArgumentType * CopyPtr = &Copy->List[Argument];

						// Copy argument type
						CopyPtr->Type = ArgPtr->Type;
						
						// Copy argument value
						StoredProcedureArgumentValueCopy(&CopyPtr->Data.Value, &ArgPtr->Data.Value, CopyPtr->Type);
					}
				}
				else if (Copy->Passing == STORED_PROCEDURE_BY_REFERENCE)
				{
					uinteger Argument;

					for (Argument = 0; Argument < Copy->Size; ++Argument)
					{
						struct StoredProcedureArgumentType * ArgPtr = &Arguments->List[Argument];
						struct StoredProcedureArgumentType * CopyPtr = &Copy->List[Argument];

						// Copy argument type
						CopyPtr->Type = ArgPtr->Type;

						// Copy argument reference
						CopyPtr->Data.Ref = ArgPtr->Data.Ref;
					}
				}
				else if (Copy->Passing == STORED_PROCEDURE_BY_NULL)
				{
					uinteger Argument;

					for (Argument = 0; Argument < Copy->Size; ++Argument)
					{
						struct StoredProcedureArgumentType * ArgPtr = &Arguments->List[Argument];
						struct StoredProcedureArgumentType * CopyPtr = &Copy->List[Argument];

						// Copy argument type
						CopyPtr->Type = ArgPtr->Type;

						// Set reference to null
						CopyPtr->Data.Ref = NULL;
					}
				}
			}
		}
		else
		{
			Copy->List = NULL;
		}
	}

	return Copy;
}

/////////////////////////////////////////////////////////////
/// Get procedure argument list size
/////////////////////////////////////////////////////////////
uinteger StoredProcedureArgumentSize(StoredProcedureArgumentList Arguments)
{
	if (Arguments)
	{
		return Arguments->Size;
	}

	return UINTEGER_SUFFIX(0);
}

/////////////////////////////////////////////////////////////
/// Get argument type from procedure argument list
/////////////////////////////////////////////////////////////
enum StoredProcedureArgumentTypeId StoredProcedureArgumentListGetType(StoredProcedureArgumentList Arguments, uinteger Parameter)
{
	if (Arguments && Parameter < Arguments->Size)
	{
		return Arguments->List[Parameter].Type;
	}

	return STORED_PROCEDURE_INVALID;
}

/////////////////////////////////////////////////////////////
/// Set argument type from procedure argument list
/////////////////////////////////////////////////////////////
void StoredProcedureArgumentListSetType(StoredProcedureArgumentList Arguments, uinteger Parameter, enum StoredProcedureArgumentTypeId Type)
{
	if (Arguments && Parameter < Arguments->Size && Type < STORED_PROCEDURE_COUNT)
	{
		Arguments->List[Parameter].Type = Type;
	}
}

/////////////////////////////////////////////////////////////
/// Destroy a stored procedure argument value (string)
/////////////////////////////////////////////////////////////
void StoredProcedureArgumentValueDestroyString(union StoredProcedureArgumentDataValueType * Value)
{
	MemoryDeallocate(Value->String.Ptr);
}

/////////////////////////////////////////////////////////////
/// Destroy a stored procedure argument value (blob)
/////////////////////////////////////////////////////////////
void StoredProcedureArgumentValueDestroyBlob(union StoredProcedureArgumentDataValueType * Value)
{
	MemoryDeallocate(Value->Blob.Ptr);
}

/////////////////////////////////////////////////////////////
/// Destroy a stored procedure argument value
/////////////////////////////////////////////////////////////
void StoredProcedureArgumentValueDestroy(union StoredProcedureArgumentDataValueType * Value, enum StoredProcedureArgumentTypeId Type)
{
	if (Value && Type < STORED_PROCEDURE_COUNT)
	{
		static StoredProcedureAgumentValueDestroyImpl ArgumentDestroyImplMapping[STORED_PROCEDURE_COUNT] =
		{
			ArrayInit(STORED_PROCEDURE_TINY, NULL),
			ArrayInit(STORED_PROCEDURE_SHORT, NULL),
			ArrayInit(STORED_PROCEDURE_LONG, NULL),
			ArrayInit(STORED_PROCEDURE_LONG_LONG, NULL),
			ArrayInit(STORED_PROCEDURE_FLOAT, NULL),
			ArrayInit(STORED_PROCEDURE_DOUBLE, NULL),
			ArrayInit(STORED_PROCEDURE_DATE, &StoredProcedureArgumentValueDestroyString),
			ArrayInit(STORED_PROCEDURE_TIME, &StoredProcedureArgumentValueDestroyString),
			ArrayInit(STORED_PROCEDURE_STRING, &StoredProcedureArgumentValueDestroyString),
			ArrayInit(STORED_PROCEDURE_BLOB, &StoredProcedureArgumentValueDestroyBlob),
			ArrayInit(STORED_PROCEDURE_NULL, NULL)
		};

		if (ArgumentDestroyImplMapping[Type])
		{
			ArgumentDestroyImplMapping[Type](Value);
		}
	}
}

/////////////////////////////////////////////////////////////
/// Destroy stored procedure argument list
/////////////////////////////////////////////////////////////
void StoredProcedureArgumentDestroy(StoredProcedureArgumentList Arguments)
{
	if (Arguments)
	{
		if (Arguments->List)
		{
			if (Arguments->Passing == STORED_PROCEDURE_BY_VALUE)
			{
				uinteger Parameter;

				// Destroy allocation if any
				for (Parameter = 0; Parameter < Arguments->Size; ++Parameter)
				{
					// Destroy the value
					StoredProcedureArgumentValueDestroy(&Arguments->List[Parameter].Data.Value, Arguments->List[Parameter].Type);
				}
			}

			// Destroy list pointer
			MemoryDeallocate(Arguments->List);
		}

		// Destroy argument pointer
		MemoryDeallocate(Arguments);
	}
}

/////////////////////////////////////////////////////////////
/// Set stored procedure argument list by reference
/////////////////////////////////////////////////////////////
void StoredProcedureArgumentSetRef(StoredProcedureArgumentList Arguments, uinteger Parameter, enum StoredProcedureArgumentTypeId Type, StoredProcedureArgumentDataRefType Data)
{
	if (Arguments && Arguments->List && Parameter < STORED_PROCEDURE_ARGUMENT_SIZE && Type < STORED_PROCEDURE_COUNT && Data)
	{
		// Binds to a predefined data
		Arguments->List[Parameter].Type = Type;
		Arguments->List[Parameter].Data.Ref = Data;
	}
}

/////////////////////////////////////////////////////////////
/// Set stored procedure argument list by value
/////////////////////////////////////////////////////////////
void StoredProcedureArgumentSetValue(StoredProcedureArgumentList Arguments, uinteger Parameter, enum StoredProcedureArgumentTypeId Type, StoredProcedureArgumentDataValueType * Data)
{
	if (Arguments && Arguments->List && Parameter < STORED_PROCEDURE_ARGUMENT_SIZE && Type < STORED_PROCEDURE_COUNT && Data)
	{
		// Copy to a internal value
		Arguments->List[Parameter].Type = Type;
		MemoryCopy(&Arguments->List[Parameter].Data.Value, Data, sizeof(StoredProcedureArgumentDataValueType));
	}
}

/////////////////////////////////////////////////////////////
/// Get stored procedure argument list by reference
/////////////////////////////////////////////////////////////
StoredProcedureArgumentDataRefType StoredProcedureArgumentGetRef(StoredProcedureArgumentList Arguments, uinteger Parameter, enum StoredProcedureArgumentTypeId Type)
{
	if (Arguments && Arguments->List && Parameter < STORED_PROCEDURE_ARGUMENT_SIZE)
	{
		// Type safety
		if (Arguments->List[Parameter].Type == Type)
		{
			// Return reference to binded data
			return Arguments->List[Parameter].Data.Ref;
		}
	}

	return NULL;
}

/////////////////////////////////////////////////////////////
/// Get stored procedure argument list by value
/////////////////////////////////////////////////////////////
StoredProcedureArgumentDataValueType * StoredProcedureArgumentGetValue(StoredProcedureArgumentList Arguments, uinteger Parameter, enum StoredProcedureArgumentTypeId Type)
{
	if (Arguments && Arguments->List && Parameter < STORED_PROCEDURE_ARGUMENT_SIZE && Type != STORED_PROCEDURE_NULL)
	{
		// Type safety
		if (Arguments->List[Parameter].Type == Type)
		{
			// Return reference to internal value data
			return &Arguments->List[Parameter].Data.Value;
		}
	}

	return NULL;
}

/////////////////////////////////////////////////////////////
/// Create builder for stored procedure argument list
/////////////////////////////////////////////////////////////
void StoredProcedureArgumentBuildCreate(struct StoredProcedureArgumentBuilder * Builder, StoredProcedureArgumentList Arguments)
{
	if (Builder)
	{
		// Initialize builder
		Builder->Arguments = Arguments;
		Builder->Count = UINTEGER_SUFFIX(0);
	}
}

/////////////////////////////////////////////////////////////
/// Build set callback for stored procedure argument list by ref
/////////////////////////////////////////////////////////////
struct StoredProcedureArgumentBuilder * StoredProcedureArgumentSetBuildRefCallback(struct StoredProcedureArgumentBuilder * Builder, enum StoredProcedureArgumentTypeId Type, StoredProcedureArgumentDataRefType Data)
{
	if (Builder)
	{
		if (Builder->Arguments && Builder->Count < Builder->Arguments->Size && Type < STORED_PROCEDURE_COUNT && Data)
		{
			// Set current argument
			StoredProcedureArgumentSetRef(Builder->Arguments, Builder->Count, Type, Data);

			// Increment current argument
			++Builder->Count;
		}

		return Builder;
	}

	return NULL;
}

/////////////////////////////////////////////////////////////
/// Build set callback for stored procedure argument list by val
/////////////////////////////////////////////////////////////
struct StoredProcedureArgumentBuilder * StoredProcedureArgumentSetBuildValueCallback(struct StoredProcedureArgumentBuilder * Builder, enum StoredProcedureArgumentTypeId Type, StoredProcedureArgumentDataValueType * Data)
{
	if (Builder)
	{
		if (Builder->Arguments && Builder->Count < Builder->Arguments->Size && Type < STORED_PROCEDURE_COUNT && Data)
		{
			// Set current argument
			StoredProcedureArgumentSetValue(Builder->Arguments, Builder->Count, Type, Data);

			// Increment current argument
			++Builder->Count;
		}

		return Builder;
	}

	return NULL;
}

/////////////////////////////////////////////////////////////
/// Build get callback for stored procedure argument list by ref
/////////////////////////////////////////////////////////////
struct StoredProcedureArgumentBuilder * StoredProcedureArgumentGetBuildRefCallback(struct StoredProcedureArgumentBuilder * Builder, enum StoredProcedureArgumentTypeId Type, StoredProcedureArgumentDataRefType * Data)
{
	if (Builder)
	{
		if (Builder->Arguments && Builder->Count < Builder->Arguments->Size && Type < STORED_PROCEDURE_COUNT && Data)
		{
			// Get current argument
			*Data = StoredProcedureArgumentGetRef(Builder->Arguments, Builder->Count, Type);

			// Increment current argument
			++Builder->Count;
		}

		return Builder;
	}

	return NULL;
}

/////////////////////////////////////////////////////////////
/// Build get callback for stored procedure argument list by val
/////////////////////////////////////////////////////////////
struct StoredProcedureArgumentBuilder * StoredProcedureArgumentGetBuildValueCallback(struct StoredProcedureArgumentBuilder * Builder, enum StoredProcedureArgumentTypeId Type, StoredProcedureArgumentDataValueType ** Data)
{
	if (Builder)
	{
		if (Builder->Arguments && Builder->Count < Builder->Arguments->Size && Type < STORED_PROCEDURE_COUNT && Data)
		{
			// Set current argument
			*Data = StoredProcedureArgumentGetValue(Builder->Arguments, Builder->Count, Type);

			// Increment current argument
			++Builder->Count;
		}

		return Builder;
	}

	return NULL;
}
