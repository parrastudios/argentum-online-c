/**
 *	Promise
 *
 * @author Vicente Ferrer
 * @lastmodify 130214 by Vicente
 * @brief A module that makes a closure for management of promises.
 */

var Arguments = Arguments || (function() {
	var params_ = null;
	
	var set_impl = (function(args) {
		// get parameters into array
		params_ = Array.apply(null, args);
	});
	
	var constructor = (function(args) {
		set_impl(args);
	})(arguments);
	
	return {
		size : function() {
			return params_.length;
		},
	
		set : function() {
			set_impl(arguments);
		},
		
		get : function() {
			return params_;
		},
	
		set_value : function(id, value) {
			params_[id] = value;
		},
		
		add_value : function(value) {
			return params_.push(value);
		},
	
		get_value : function(id) {
			if (id >= 0 && id < params_.length) {
				return params_[id];
			} else {
				return null;
			}
		}
	};
});

var Callback = Callback || (function(func, args) {
	var func_ = null;
	var args_ = null;
	
	var constructor = (function(f, a) {
		func_ = f;
		
		if (a.size() > 0) {
			args_ = a;
		}
	})(func, args);
	
	return {
		func : function() {
			return func_;
		},
		
		arguments : function() {
			return args_;
		},

		raise : function() {
			func_(args_);
		}
	};
});

function PromiseModuleWrapper(promise_module_name) {
	if (arguments.callee._singleton_instance_) {
		return arguments.callee._singleton_instance_;
	}
		
	arguments.callee._singleton_instance_ = this;
		
	if (typeof promise_module_name != 'undefined') {
		var module_impl = require(String(promise_module_name));
	
		this.module = (function() {
			return module_impl;
		});
			
		this.name = (function() {
			return promise_module_name;
		});
	}
}

var PromiseQImpl = PromiseQImpl || (function() {
	var q_ = null;
	var d_ = null;
	
	var defer_impl = (function() {
		d_ = q_.defer();
	});
	
	var contructor = (function() {
		q_ = PromiseModuleWrapper('q').module();
	})();
	
	return {
		defer : function() {
			if (d_ == null) {
				defer_impl();
			}

			return d_;
		},
	
		resolve : function() {
			d_.resolve();
		},
		
		reject : function() {
			d_.reject();
		},
		
		result : function() {
			return d.promise;
		},
		
		raise : function(result, error) {
			d_.promise.then(result, error);
		}
	};
});

function PromiseImplConstructorWrapper() {
	var module_name = PromiseModuleWrapper().name();

	if (module_name == 'q') {
		return new PromiseQImpl();
	} else if (module_name == 'when') {
	//	return new PromiseWhenImpl();
	} else {
	// ..
	}
	
	return null;
}

var Promise = Promise || (function() {
	var promise_impl_ = null;
	var result_ = null;
	var error_ = null;
	
	var constructor = (function() {
		promise_impl_ = PromiseImplConstructorWrapper();
	})();
	
	return {
		defer : function() {
			promise_impl_.defer();
		},
	
		resolve : function() {
			promise_impl_.resolve();
		},
		
		reject : function() {
			promise_impl_.reject();
		},
	
		register_result : function(result_callback) {
			result_ = result_callback;
		},
		
		register_error : function(error_callback) {
			error_ = error_callback;
		},
		
		raise : function() {
			// actually, inline function parameters (result, error) are not used
			promise_impl_.raise(function(result) {
				if (result_ != null) {
					result_.raise();
				}
			}, function(error) {
				if (error_ != null) {
					error_.raise();
				}
			});
		}
	};
});

var module_initializer = module_initializer || (function(module_name) {
	PromiseModuleWrapper(module_name);
});

/**
 * Public
 */
exports.Arguments	= Arguments;
exports.Callback	= Callback;
exports.Promise		= Promise;
exports.Initializer = module_initializer;