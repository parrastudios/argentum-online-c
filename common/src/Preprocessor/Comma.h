/////////////////////////////////////////////////////////////////////////////
//	Preprocessor Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A cross-platform macro based library to give support
//	for preprocessor metaprogramming.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PREPROCESSOR_COMMA_H
#define PREPROCESSOR_COMMA_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Preprocessor/Empty.h>
#include <Preprocessor/If.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PREPROCESSOR_COMMA_SYMBOL() ,

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define PREPROCESSOR_COMMA_IF_IMPL(Expr) \
	PREPROCESSOR_IF(Expr, PREPROCESSOR_COMMA_SYMBOL, PREPROCESSOR_EMPTY_SYMBOL)

#define PREPROCESSOR_COMMA_IF(Expr) \
	PREPROCESSOR_COMMA_IF_IMPL(Expr)

#define PREPROCESSOR_COMMA_VARIDIC(...) ,

#endif // PREPROCESSOR_COMMA_H
