/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/HashMap.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define HASH_MAP_MAX_LOAD_FACTOR	0.75f
#define HASH_MAP_MID_LOAD_FACTOR	0.40f
#define HASH_MAP_MIN_LOAD_FACTOR	0.0f
#define HASH_MAP_MIN_BUCKETS_SIZE	UINTEGER_SUFFIX(16)

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct HashMapPairType
{
	HashKeyData					Key;
	void *						Data;
};

struct HashMapNodeType
{
	struct HashMapPairType		Pair;
	HashData					Hash;
	struct HashMapNodeType *	Next;
};

struct HashMapType
{
	uinteger					Size;
	uinteger					BucketsSize;
	struct HashMapNodeType **	Buckets;
	HashCallback				HashFunc;
	CompareCallback				CompareFunc;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Find a node by key and hash on a hash map
////////////////////////////////////////////////////////////
struct HashMapNodeType * HashMapFindNode(HashMap Map, HashKeyData Key, HashData Hash)
{
	uinteger Bucket = Hash % Map->BucketsSize;

	if (Map->CompareFunc)
	{
		struct HashMapNodeType * Node;

		for (Node = Map->Buckets[Bucket]; Node != NULL; Node = Node->Next)
		{
			if (Node->Hash == Hash && (Map->CompareFunc(Node->Pair.Key, Key) & OPERATOR_EQUAL))
			{
				return Node;
			}
		}
	}
	else
	{
		struct HashMapNodeType * Node;

		for (Node = Map->Buckets[Hash % Map->BucketsSize]; Node != NULL; Node = Node->Next)
		{
			if (Node->Hash == Hash)
			{
				return Node;
			}
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Create a hash map
////////////////////////////////////////////////////////////
HashMap HashMapCreate(HashCallback HashFunc, CompareCallback CompareFunc)
{
	HashMap Map = (struct HashMapType *)MemoryAllocate(sizeof(struct HashMapType));

	if (Map)
	{
		Map->Buckets = NULL;

		Map->BucketsSize = 0;

		if (HashMapInitialize(Map, HashFunc, CompareFunc))
		{
			return Map;
		}
		else
		{
			MemoryDeallocate(Map);

			return NULL;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Create a hash map with custom hash and compare functions
////////////////////////////////////////////////////////////
bool HashMapInitialize(HashMap Map, HashCallback HashFunc, CompareCallback CompareFunc)
{
    if (Map && HashFunc)
	{
		if (Map->Buckets == NULL && Map->BucketsSize == 0)
		{
			uinteger BucketsMemSize = sizeof(struct HashMapNodeType *) * HASH_MAP_MIN_BUCKETS_SIZE;

			Map->BucketsSize = HASH_MAP_MIN_BUCKETS_SIZE;

			Map->Size = 0;

			Map->Buckets = (struct HashMapNodeType **)MemoryAllocate(BucketsMemSize);

			if (Map->Buckets)
			{
				MemorySet(Map->Buckets, 0, BucketsMemSize);
			}
			else
			{
				// Handle error
				return false;
			}
		}

        Map->HashFunc = HashFunc;

        Map->CompareFunc = CompareFunc;

        return true;
    }

    return false;
}

////////////////////////////////////////////////////////////
/// Destroy a hash map
////////////////////////////////////////////////////////////
void HashMapDestroy(HashMap Map)
{
	if (Map)
	{
		// Clear all entries
        HashMapClear(Map);

		// Deallocate table
        MemoryDeallocate(Map->Buckets);

		// Deallocate map
		MemoryDeallocate(Map);
    }
}

////////////////////////////////////////////////////////////
/// Insert data by key into a hash map
////////////////////////////////////////////////////////////
bool HashMapInsert(HashMap Map, HashKeyData Key, void * Data)
{
	HashData Hash = Map->HashFunc(Key);

	// On invalid hash, never insert it
	if (Hash != HASH_MAP_HASH_INVALID)
	{
		uinteger Bucket = Hash % Map->BucketsSize;

		struct HashMapNodeType * Node = HashMapFindNode(Map, Key, Hash);

		if (Node != NULL)
		{
			// warning: what happens here? memory will be not released, and pointer lost.
			// Node->Pair.Data = Data;

			return true;
		}

		Node = (struct HashMapNodeType *)MemoryAllocate(sizeof(struct HashMapNodeType));

		if (Node)
		{
			Node->Pair.Key = Key;
			Node->Pair.Data = Data;
			Node->Hash = Hash;
			Node->Next = Map->Buckets[Bucket];

			Map->Buckets[Bucket] = Node;

			++Map->Size;

			HashMapResize(Map);

			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Remove data by key from a hash map
////////////////////////////////////////////////////////////
bool HashMapRemove(HashMap Map, HashKeyData Key)
{
	HashData Hash = Map->HashFunc(Key);

	uinteger Bucket = Hash % Map->BucketsSize;

	struct HashMapNodeType * Node, * Prev = NULL;

	if (Map->CompareFunc)
	{
		for (Node = Map->Buckets[Bucket]; Node != NULL; Prev = Node, Node = Node->Next)
		{
			if (Node->Hash == Hash && (Map->CompareFunc(Node->Pair.Key, Key) & OPERATOR_EQUAL))
			{
				// Remove
				if (Prev)
				{
					Prev->Next = Node->Next;
				}
				else
				{
					Map->Buckets[Bucket] = Node->Next;
				}

				// Clear node memory
				MemoryDeallocate(Node);

				return true;
			}
		}
	}
	else
	{
		for (Node = Map->Buckets[Bucket]; Node != NULL; Prev = Node, Node = Node->Next)
		{
			if (Node->Hash == Hash)
			{
				// Remove
				if (Prev)
				{
					Prev->Next = Node->Next;
				}
				else
				{
					Map->Buckets[Bucket] = Node->Next;
				}

				// Clear node memory
				MemoryDeallocate(Node);

				return true;
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Get data by key from a hash map
////////////////////////////////////////////////////////////
void * HashMapGet(HashMap Map, HashKeyData Key)
{
	HashData Hash = Map->HashFunc(Key);

	struct HashMapNodeType * Node = HashMapFindNode(Map, Key, Hash);

	if (Node != NULL)
	{
		return Node->Pair.Data;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Clear a hash map
////////////////////////////////////////////////////////////
void HashMapClear(HashMap Map)
{
	uinteger Bucket;
	struct HashMapNodeType * Node, * Next;

	for (Bucket = 0; Bucket < Map->BucketsSize; ++Bucket)
	{
		Node = Map->Buckets[Bucket];

		while (Node != NULL)
		{
			Next = Node->Next;

			MemoryDeallocate(Node);

			Node = Next;
		}

		Map->Buckets[Bucket] = NULL;
	}

	Map->Size = 0;

	HashMapResize(Map);
}

////////////////////////////////////////////////////////////
/// Resize a hash map
////////////////////////////////////////////////////////////
void HashMapResize(HashMap Map)
{
	double LoadFactor = (double)Map->Size / (double)Map->BucketsSize;

	if (LoadFactor > HASH_MAP_MAX_LOAD_FACTOR || LoadFactor < HASH_MAP_MIN_LOAD_FACTOR)
	{
		uinteger NewBucketsSize = (uinteger)(Map->Size / HASH_MAP_MID_LOAD_FACTOR);

		if (NewBucketsSize < HASH_MAP_MIN_BUCKETS_SIZE)
		{
			NewBucketsSize = HASH_MAP_MIN_BUCKETS_SIZE;
		}

		if (NewBucketsSize != Map->BucketsSize)
		{
			uinteger NewBucketsMemSize = sizeof(struct HashMapNodeType *) * NewBucketsSize;
			struct HashMapNodeType * Node, * Next;
			uinteger Bucket, NewBucket;

			struct HashMapNodeType ** NewBuckets = (struct HashMapNodeType **)MemoryAllocate(NewBucketsMemSize);

			if (NewBuckets)
			{
				MemorySet(NewBuckets, 0, NewBucketsMemSize);

				for (Bucket = 0; Bucket < Map->BucketsSize; ++Bucket)
				{
					Node = Map->Buckets[Bucket];

					while (Node != NULL)
					{
						Next = Node->Next;

						NewBucket = Node->Hash % NewBucketsSize;

						Node->Next = NewBuckets[NewBucket];

						NewBuckets[NewBucket] = Node;

						Node = Next;
					}
				}
			}
			else
			{
				// Handle error
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Initialize iteration to first bucket of a hash map
////////////////////////////////////////////////////////////
bool HashMapIteratorBegin(HashMapIterator * Iterator, HashMap Map)
{
	if (Iterator && Map && Map->Size > 0)
	{

		Iterator->Map = Map;
		Iterator->Bucket = 0;
		Iterator->Node = NULL;

		HashMapIteratorNext(Iterator);

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Skip to the next iterator of a hash map
////////////////////////////////////////////////////////////
bool HashMapIteratorNext(HashMapIterator * Iterator)
{
	if (Iterator->Node == NULL)
	{
		do
		{
			Iterator->Node = Iterator->Map->Buckets[Iterator->Bucket++];

		} while (Iterator->Bucket < Iterator->Map->BucketsSize && Iterator->Node == NULL);
	}
	else
	{
		Iterator->Node = Iterator->Node->Next;

		if (Iterator->Node == NULL)
		{
			HashMapIteratorNext(Iterator);
		}
	}

	return (Iterator->Node != NULL);
}

////////////////////////////////////////////////////////////
/// Check where iterator of a hash map is at the end
////////////////////////////////////////////////////////////
bool HashMapIteratorEnd(HashMapIterator * Iterator)
{
	return (Iterator->Node == NULL);
}

////////////////////////////////////////////////////////////
/// Retreive data of an iterator
////////////////////////////////////////////////////////////
void * HashMapIteratorData(HashMapIterator * Iterator)
{
	if (Iterator && Iterator->Node)
	{
		return Iterator->Node->Pair.Data;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Retreive key of an iterator
////////////////////////////////////////////////////////////
void * HashMapIteratorKey(HashMapIterator * Iterator)
{
	if (Iterator && Iterator->Node)
	{
		return Iterator->Node->Pair.Key;
	}

	return NULL;
}
