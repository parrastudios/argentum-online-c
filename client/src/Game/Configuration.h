/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

#ifndef GAME_CONFIGURATION_H
#define GAME_CONFIGURATION_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <Game/Account.h>
#include <Window/Window.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define CONFIG_DEFAULT_PATH			"data/config"
#define CONFIG_ERROR_INVALID		0xFFFFFFFF
#define CONFIG_FLAG_LANGUAGE		0x00000001 << 0x00
#define CONFIG_FLAG_WINDOW_MODE		0x00000001 << 0x01
#define CONFIG_FLAG_WINDOW_BPP		0x00000001 << 0x02
#define CONFIG_FLAG_WINDOW_VSYNC	0x00000001 << 0x03
#define CONFIG_FLAG_NAME			0x00000001 << 0x04
#define CONFIG_FLAG_PASSWORD		0x00000001 << 0x05
#define CONFIG_FLAG_TEMPLATE		0x00000001 << 0x06

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct ConfigData
{
	uint32		Language;							///< (Spanish, English)
	uint32		WindowStyle;						///< (NonStyle, Fullscreen)
	uint32		BitsPerPixel;						///< (16, 24, 32)
	bool		Vsync;								///< (enabled, disabled)
	char		Name[ACCOUNT_NAME_MAX_SIZE];		///< User name
	char		Password[ACCOUNT_PASS_MAX_SIZE];	///< User password
	uint32		GuiTemplate;						///< Graphical user interface template
};

////////////////////////////////////////////////////////////
/// Configuration initializer
////////////////////////////////////////////////////////////
void ConfigInitialize();

////////////////////////////////////////////////////////////
/// Configuration default seter
////////////////////////////////////////////////////////////
void ConfigSetDefault(struct ConfigData * Config);

////////////////////////////////////////////////////////////
/// Configuration save data
////////////////////////////////////////////////////////////
bool ConfigSave();

////////////////////////////////////////////////////////////
/// Check if the values are correct
////////////////////////////////////////////////////////////
uint32 ConfigCheckIntegrity(struct ConfigData * Config);

////////////////////////////////////////////////////////////
/// Configuration getter
////////////////////////////////////////////////////////////
struct ConfigData * ConfigGet();

////////////////////////////////////////////////////////////
/// Get values of config from an id
////////////////////////////////////////////////////////////
char * ConfigToString(char * Id);

#endif // GAME_CONFIGURATION_H
