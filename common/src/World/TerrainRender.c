/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2016 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/TerrainBase.h>
#include <World/TerrainRender.h>
#include <World/TerrainTexture.h>

#include <World/Camera.h>

#include <Math/General.h>

#include <Graphics/Device.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define TERRAIN_RENDER_QUADTREE_FACTOR			4
#define TERRAIN_RENDER_INVALID_VERTEX_INDEX		UINT16_SUFFIX(0xFFFF)
#define TERRAIN_RENDER_VERTEX_STRIDE			8
#define TERRAIN_RENDER_FOG                      1

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef enum
{
	TERRAIN_RENDER_PATCH_ERROR_MAX = 0x00,
	TERRAIN_RENDER_PATCH_ERROR_AVERAGE = 0x01
} TerrainRenderPatchErrorComputeType;

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
///	Return a pointer to the vertex array for a patch
////////////////////////////////////////////////////////////
#define TerrainRenderPatchVertexPtr(RenderData, Node)		((RenderData)->PatchVertexPtr + (Node))

////////////////////////////////////////////////////////////
///	Return a pointer to the index array for a patch
////////////////////////////////////////////////////////////
#define TerrainRenderPatchIndexPtr(RenderData, Node)		((RenderData)->PatchIndexPtr + (Node))

////////////////////////////////////////////////////////////
/// Return a pointer to the error array
////////////////////////////////////////////////////////////
#define TerrainRenderPatchError(RenderData, Node)			(float *)(((uint8 *)(RenderData)->PatchErrorArray) + (Node) * (RenderData)->PatchErrorArraySize)

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create render component in a entity terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainRenderCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Allocate render component data in a entity terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainRenderAllocate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Initialize render component in a entity terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainRenderInitialize(struct EntityType * Entity,
	uinteger Height,
	uinteger TextureSize, uinteger TextureTile, uinteger BaseTextureTile,
	uinteger PatchVertexCacheSize, uinteger PatchIndexCacheSize);

////////////////////////////////////////////////////////////
/// Destroy a terrain render component
////////////////////////////////////////////////////////////
void TerrainRenderDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Render a terrain
////////////////////////////////////////////////////////////
void TerrainRenderDraw(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Set current reference to camera
////////////////////////////////////////////////////////////
void TerrainRenderSetCameraTarget(struct EntityType * Entity, EntityIdType CameraId);

////////////////////////////////////////////////////////////
/// Set the error metric for the current field of view and window height
////////////////////////////////////////////////////////////
void TerrainRenderSetErrorMetric(struct EntityType * Entity, float FovX, uinteger Height);

////////////////////////////////////////////////////////////
/// Return number of vertex for a patch size at desired mip level
////////////////////////////////////////////////////////////
uinteger TerrainRenderPatchVertexSize(struct TerrainComponentBaseData * BaseData, uinteger Level);

////////////////////////////////////////////////////////////
/// Return number of triangles for a patch size at desired mip level
////////////////////////////////////////////////////////////
uinteger TerrainRenderPatchTriangleSize(struct TerrainComponentBaseData * BaseData, uinteger Level);

////////////////////////////////////////////////////////////
/// Return a pointer to the patch
////////////////////////////////////////////////////////////
struct TerrainPatchRenderData * TerrainRenderPatchImpl(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData, uinteger X, uinteger Y);

////////////////////////////////////////////////////////////
/// Retreive a patch
////////////////////////////////////////////////////////////
struct TerrainPatchRenderData * TerrainRenderPatch(struct EntityType * Entity, uinteger X, uinteger Y);

////////////////////////////////////////////////////////////
/// Create the patch error arrays
////////////////////////////////////////////////////////////
void TerrainRenderPatchErrorInitialize(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Create the index array that will be used to access
///	patch vertex arrays in row major order
////////////////////////////////////////////////////////////
void TerrainRenderPatchIndexInitialize(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Return the cached index array for a patch
////////////////////////////////////////////////////////////
uint16 * TerrainRenderPatchIndex(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger NodeIndex, uinteger Level, uinteger NeighbourLevel);

////////////////////////////////////////////////////////////
/// Create the vertex array with vertexes in mip level order
////////////////////////////////////////////////////////////
void TerrainRenderPatchVertexInitialize(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger Left, uinteger Right, uinteger Top, uinteger Bottom, float * VertexArray);

////////////////////////////////////////////////////////////
/// Return the cached vertex array for a patch
////////////////////////////////////////////////////////////
float * TerrainRenderPatchVertex(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger NodeIndex, uinteger Left, uinteger Right, uinteger Top, uinteger Bottom);

////////////////////////////////////////////////////////////
/// Set the geo-mip level for the visible parts of the terrain
////////////////////////////////////////////////////////////
void TerrainRenderSetMipLevel(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Force all patches to a given geo-mip level
////////////////////////////////////////////////////////////
void TerrainRenderForceMipLevel(struct EntityType * Entity, uinteger Level);

////////////////////////////////////////////////////////////
/// Initialize terrain render quadtree
////////////////////////////////////////////////////////////
void TerrainRenderQuadtreeInitialize(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Clip the terrain render to a view frustum
////////////////////////////////////////////////////////////
void TerrainRenderClip(struct EntityType * Entity, struct FrustumType * Frustum);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the terrain render implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentRenderType * TerrainComponentRenderAttr()
{
	static const struct TerrainComponentRenderType TerrainComponentRenderImplAttr = {
		TypeInit(SetCameraTarget, &TerrainRenderSetCameraTarget),
		TypeInit(Initialize, &TerrainRenderInitialize),
		TypeInit(SetErrorMetric, &TerrainRenderSetErrorMetric),
		TypeInit(SetMipLevel, &TerrainRenderSetMipLevel),
		TypeInit(ForceMipLevel, &TerrainRenderForceMipLevel),
		TypeInit(Patch, &TerrainRenderPatch)
	};

	static const struct ComponentRenderType TerrainComponentRenderAttr = {
		TypeInit(Draw, &TerrainRenderDraw),
        TypeInit(TypeImpl, (const void *)&TerrainComponentRenderImplAttr)
	};

	return (const void *)&TerrainComponentRenderAttr;
}

////////////////////////////////////////////////////////////
/// Return the terrain render implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * TerrainComponentRenderImpl()
{
	static const struct ComponentImplType TerrainComponentRenderTypeImpl = {
		ExprInit(Base, TypeInit(Create, &TerrainRenderCreate), TypeInit(Destroy, &TerrainRenderDestroy)),
		TypeInit(Type, COMPONENT_TYPE_RENDER),
		UnionInit(Attributes, Render, &TerrainComponentRenderAttr)
	};

	return &TerrainComponentRenderTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create render component in a entity terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainRenderCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain render component data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

		// Initialize by default the terrain render component
		//RenderData->DrawMode = TERRAIN_RENDER_DRAW_WIREFRAME;
		//RenderData->DrawMode = TERRAIN_RENDER_DRAW_TEXTURE;
		//RenderData->DrawMode = TERRAIN_RENDER_DRAW_TEXTURE_SPLAT;
		RenderData->DrawMode = TERRAIN_RENDER_DRAW_TEXTURE_SPLAT_CVA;
		RenderData->TextureCombine = false;
		RenderData->CameraEntityId = ENTITY_INDEX_INVALID;
		RenderData->SourceHeight = 0;

		RenderData->ErrorMetric = 0.0f;
		RenderData->Quadtree = NULL;

		RenderData->BaseTexture = 0;
		RenderData->SplatTexture[0] = RenderData->SplatTexture[1] = RenderData->SplatTexture[2] = 0;
		RenderData->TextureSize = 0;
		RenderData->TextureTile = 0;
		RenderData->BaseTextureTile = 0;

		RenderData->PatchLevels = 0;
		RenderData->PatchIndex = NULL;
		RenderData->PatchVertexCacheSize = 0;
		RenderData->PatchVertexPtr = NULL;
		RenderData->PatchErrorArray = NULL;
		RenderData->PatchErrorArraySize = 0;
		RenderData->PatchIndexCacheSize = 0;
		RenderData->PatchIndexPtr = NULL;
		RenderData->Patches = NULL;

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Allocate render component data in a entity terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainRenderAllocate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);
		struct TerrainComponentBaseType * BaseType = ComponentTypeDerived(Base, struct TerrainComponentBaseType);

		// Obtain render component data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

		uinteger PatchIndexSize = TerrainRenderPatchVertexSize(BaseData, 0) * sizeof(uint16);
		uinteger HeightMapPatchSize = sqr(BaseData->PatchTile);
		uinteger TerrainPatchSize = sqr(BaseData->HeightMapTile * BaseData->PatchTile);
		uinteger HeightMapPatchIndex = BaseType->PatchNodes(Entity, BaseData->HeightMapSize >> 1);
		uinteger HeightMapTilePatchIndex = BaseType->PatchNodes(Entity, (BaseData->HeightMapSize * BaseData->HeightMapTile) >> 1);
		uinteger PatchVertexPtrSize = (HeightMapPatchIndex + HeightMapPatchSize) * sizeof(struct CacheObjectType *);
		uinteger PatchErrorArraySize;
		uinteger PatchIndexPtrSize = (HeightMapTilePatchIndex + TerrainPatchSize) * sizeof(struct CacheObjectType *);
		uinteger PatchesSize = TerrainPatchSize * sizeof(struct TerrainPatchRenderData);
		uinteger QuadtreeSize = BaseType->NodesSize(Entity) * sizeof(uinteger);

		// Allocate patch indexes
		RenderData->PatchIndex = MemoryAllocate(PatchIndexSize);

		MemorySet(RenderData->PatchIndex, 0, PatchIndexSize);

		// Allocate patch vertex cache objects
		RenderData->PatchVertexPtr = MemoryAllocate(PatchVertexPtrSize);

		MemorySet(RenderData->PatchVertexPtr, 0, PatchVertexPtrSize);

		// Allocate patch error array
		RenderData->PatchErrorArraySize = RenderData->PatchLevels * sizeof(float);

		PatchErrorArraySize = (HeightMapPatchIndex + HeightMapPatchSize) * RenderData->PatchErrorArraySize;

		RenderData->PatchErrorArray = MemoryAllocate(PatchErrorArraySize);

		MemorySet(RenderData->PatchErrorArray, 0, PatchErrorArraySize);

		// Allocate patch index cache objects
		RenderData->PatchIndexPtr = MemoryAllocate(PatchIndexPtrSize);

		MemorySet(RenderData->PatchIndexPtr, 0, PatchIndexPtrSize);

		// Allocate patches
		RenderData->Patches = MemoryAllocate(PatchesSize);

		MemorySet(RenderData->Patches, 0, PatchesSize);

		// Allocate quadtree
		RenderData->Quadtree = MemoryAllocate(QuadtreeSize);

		MemorySet(RenderData->Quadtree, 0, QuadtreeSize);

		// Check for errors
		if (RenderData->PatchIndex && RenderData->PatchVertexPtr && RenderData->PatchErrorArray &&
			RenderData->PatchIndexPtr && RenderData->Patches && RenderData->Quadtree)
		{
			// Initialize vertex array cache
			MemorySet(&RenderData->PatchVertexCache, 0, sizeof(struct CacheType));

			#ifdef TERRAIN_RENDER_USE_NV_VAR_FENCE
				RenderData->PatchVertexCache.Fast = true;
			#else
				RenderData->PatchVertexCache.Fast = false;
			#endif

			// Initialize vertex array cache object
			CacheInitialize(&RenderData->PatchVertexCache, RenderData->PatchVertexCacheSize, RenderData->PatchVertexCache.Fast);

			// Initialize index array cache
			MemorySet(&RenderData->PatchIndexCache, 0, sizeof(struct CacheType));

			// Initialize index array cache object
			CacheInitialize(&RenderData->PatchIndexCache, RenderData->PatchIndexCacheSize, false);

			return Entity;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Calculate the geomipmap level for a patch
////////////////////////////////////////////////////////////
uinteger TerrainRenderPatchLevel(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		uinteger Level = 0;
		uinteger PatchSizeIt = BaseData->PatchSize;

		while (PatchSizeIt > 0)
		{
			// Iterate to the next patch level
			PatchSizeIt >>= 1;

			// Count the level
			++Level;
		}

		return Level;
	}

	return TERRAIN_RENDER_PATCH_LEVEL_INVALID;
}

////////////////////////////////////////////////////////////
/// Initialize render component in a entity terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainRenderInitialize(struct EntityType * Entity,
	uinteger Height,
	uinteger TextureSize, uinteger TextureTile, uinteger BaseTextureTile,
	uinteger PatchVertexCacheSize, uinteger PatchIndexCacheSize)
{
	if (Entity)
	{
		// Obtain render component data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

		// Initialize terrain render component
		RenderData->SourceHeight = Height;
		RenderData->PatchLevels = TerrainRenderPatchLevel(Entity);
		RenderData->BaseTexture = 0;
		RenderData->TextureSize = TextureSize;
		RenderData->TextureTile = TextureTile;
		RenderData->BaseTextureTile = BaseTextureTile;
		RenderData->PatchVertexCacheSize = PatchVertexCacheSize * 1024;
		RenderData->PatchIndexCacheSize = PatchIndexCacheSize * 1024;

		// Allocate memory for the render component
		if (TerrainRenderAllocate(Entity))
		{
			glClearDepth(1.0f);
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LEQUAL);
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			glEnableClientState(GL_VERTEX_ARRAY);

			if (DeviceGet()->Info.Limits.Units > 1)
			{
				DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
				glDisable(GL_TEXTURE_2D);
				DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
				glDisableClientState(GL_TEXTURE_COORD_ARRAY);
				DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
				glEnable(GL_TEXTURE_2D);
				glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
				DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE0_ARB);
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			}
			else
			{
				glEnable(GL_TEXTURE_2D);
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			}

			// Initialize patch indexes
			TerrainRenderPatchIndexInitialize(Entity);

			// Initialize patch error arrays
			TerrainRenderPatchErrorInitialize(Entity);

			// Initialize quadtree
			TerrainRenderQuadtreeInitialize(Entity);

			// Initialize vertex cache object
			if (RenderData->PatchVertexCache.Fast)
			{
				DeviceGetExt()->glVertexArrayRangeNV(RenderData->PatchVertexCache.Size, RenderData->PatchVertexCache.FastBase);
				glEnableClientState(GL_VERTEX_ARRAY_RANGE_NV);
			}

			// Create color map textures
			if (TerrainTextureColorMapCreate(Entity, 1.0f))
			{
				// Create texture splatting
				if (TerrainTextureSplatCreate(Entity, 1.0f))
				{
					float FogColor[4] = { 0.5f, 0.5f, 0.5f, 1.0f };

					struct EntityType * Camera = EntityGet(ENTITY_TYPE_CAMERA, RenderData->CameraEntityId);

					// Obtain camera base component data reference
					struct ComponentType * CameraBase = EntityComponentBase(Camera);
					struct CameraComponentBaseData * CameraBaseData = ComponentDataDerived(CameraBase, struct CameraComponentBaseData);

					// Enable fog
                    #if (TERRAIN_RENDER_FOG == 1)
                        glEnable(GL_FOG);
                        glFogi(GL_FOG_MODE, GL_LINEAR);
                        glFogfv(GL_FOG_COLOR, FogColor);
                        glHint(GL_FOG_HINT, GL_DONT_CARE);
                        glFogf(GL_FOG_START, 0.6f * CameraBaseData->Far);
                        glFogf(GL_FOG_END, 0.9f * CameraBaseData->Far);
                        glDisable(GL_FOG);
					#endif
				}
			}

			return Entity;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a terrain render component
////////////////////////////////////////////////////////////
void TerrainRenderDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get render component
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

		// Destroy index cache
		CacheDestroy(&RenderData->PatchIndexCache);

		// Destroy vertex cache
		CacheDestroy(&RenderData->PatchVertexCache);

		// Deallocate patch indexes
		MemoryDeallocate(RenderData->PatchIndex);

		// Deallocate patch vertex cache objects
		MemoryDeallocate(RenderData->PatchVertexPtr);

		// Deallocate patch error array
		MemoryDeallocate(RenderData->PatchErrorArray);

		// Deallocate patch index cache objects
		MemoryDeallocate(RenderData->PatchIndexPtr);

		// Deallocate patches
		MemoryDeallocate(RenderData->Patches);

		// Deallocate quadtree
		MemoryDeallocate(RenderData->Quadtree);
	}
}

////////////////////////////////////////////////////////////
/// Obtain level for desired patch coordinates
////////////////////////////////////////////////////////////
uinteger TerrainRenderPatchNeighbourLevel(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger X, uinteger Y, uinteger Index)
{
	if (BaseData && RenderData)
	{
		uinteger Size = BaseData->PatchTile * BaseData->HeightMapTile;

		if (X < Size && Y < Size)
		{
			return (TerrainRenderPatchImpl(BaseData, RenderData, X, Y)->Level & UINTEGER_SUFFIX(0xFF)) << (Index * 8);
		}
	}

	return UINTEGER_SUFFIX(0);
}

////////////////////////////////////////////////////////////
/// Render a terrain patch
////////////////////////////////////////////////////////////
void TerrainRenderPatchDraw(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger NodeIndex, uinteger ParentIndex, uinteger Left, uinteger Right, uinteger Top, uinteger Bottom)
{

	uinteger X = Left / BaseData->PatchSize;
	uinteger Y = Top / BaseData->PatchSize;
	struct TerrainPatchRenderData * Patch = TerrainRenderPatchImpl(BaseData, RenderData, X, Y);
	uinteger NeighbourLevel = 0, IndexCount = 0;
	float * VertexArray;
	uint16 * IndexArray;
	struct Vector3f Offset;

	// Check for rebuild (left)
	NeighbourLevel |= TerrainRenderPatchNeighbourLevel(BaseData, RenderData, (X > 0) ? X - 1 : 0, Y, 0);

	// Check for rebuild (right)
	NeighbourLevel |= TerrainRenderPatchNeighbourLevel(BaseData, RenderData, X + 1, Y, 1);

	// Check for rebuild (top)
	NeighbourLevel |= TerrainRenderPatchNeighbourLevel(BaseData, RenderData, X, Y + 1, 2);

	// Check for rebuild (bottom)
	NeighbourLevel |= TerrainRenderPatchNeighbourLevel(BaseData, RenderData, Y, (Y > 0) ? Y - 1 : 0, 3);

	// Calculate translation (integer math - get rid of remainders)
	Vector3fSet(&Offset,
		(X / BaseData->PatchTile) * BaseData->HeightMapSize * BaseData->Scale.x,
		(Y / BaseData->PatchTile) * BaseData->HeightMapSize * BaseData->Scale.y,
		0.0f);

	// Translate patch
	glTranslatef(Offset.x, Offset.y, Offset.z);

	// Calculate vertex and index pointer arrays
	VertexArray = TerrainRenderPatchVertex(BaseData, RenderData, ParentIndex, Left, Right, Top, Bottom);
	IndexArray = TerrainRenderPatchIndex(BaseData, RenderData, NodeIndex, Patch->Level, NeighbourLevel);

	// Retrieve triangle count (in the first element)
	IndexCount = IndexArray[0] * 3;

	// Advance index array pointer (to the index array)
	++IndexArray;

	// Draw with specified mode
	if (RenderData->DrawMode == TERRAIN_RENDER_DRAW_WIREFRAME)
	{
		// Line mode
		glVertexPointer(3, GL_FLOAT, 32, VertexArray);
		glDrawElements(GL_TRIANGLES, IndexCount, GL_UNSIGNED_SHORT, IndexArray);
	}
	else if (RenderData->DrawMode == TERRAIN_RENDER_DRAW_TEXTURE)
	{
		// Single pass texture mode
		if (DeviceGet()->Info.Limits.Units > 1)
		{
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glTexCoordPointer(2, GL_FLOAT, 32, VertexArray + 6);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE0_ARB);
		}

		glBindTexture(GL_TEXTURE_2D, Patch->ColourMapTexure);
		glTexCoordPointer(2, GL_FLOAT, 32, VertexArray + 4);
		glVertexPointer(3, GL_FLOAT, 32, VertexArray);
		glDrawElements(GL_TRIANGLES, IndexCount, GL_UNSIGNED_SHORT, IndexArray);
	}
	else if (RenderData->TextureCombine == false)
	{
		// Texture splat mode, assume multi-texture extensions
		float Alpha = 0.0f;

		struct EntityType * Camera = EntityGet(ENTITY_TYPE_CAMERA, RenderData->CameraEntityId);

		// Obtain camera base component data reference
		struct ComponentType * CameraBase = EntityComponentBase(Camera);
		struct CameraComponentBaseData * CameraBaseData = ComponentDataDerived(CameraBase, struct CameraComponentBaseData);

		// Get distance blend factor
		if (Patch->ViewPointDistance > CameraBaseData->Far / 2.0f)
		{
			// Too far, no splatting
			Alpha = 1.0f;
		}
		else if (Patch->ViewPointDistance > CameraBaseData->Far / 4.0f)
		{
			// Middle distance, fade out splatting
			Alpha = (2.0f * Patch->ViewPointDistance) / CameraBaseData->Far;

			Alpha = clamp(Alpha, 0.5f, 1.0f);
		}
		else
		{
			// Foreground
			Alpha = 0.5f;
		}

		if (Alpha == 1.0f)
		{
			// Too far, no splatting
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glDisable(GL_TEXTURE_2D);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			glBindTexture(GL_TEXTURE_2D, Patch->ColourMapTexure);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE0_ARB);
			glTexCoordPointer(2, GL_FLOAT, 32, VertexArray + 4);
			glVertexPointer(3, GL_FLOAT, 32, VertexArray);
			glDrawElements(GL_TRIANGLES, IndexCount, GL_UNSIGNED_SHORT, IndexArray);

			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glEnable(GL_TEXTURE_2D);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE0_ARB);
		}
		else
		{
			// Set distance blend factor
			glColor4f(0.0f, 0.0f, 0.0f, Alpha);

			glEnable(GL_BLEND);

			glBlendFunc(GL_SRC_ALPHA, GL_ZERO);

			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			glBindTexture(GL_TEXTURE_2D, RenderData->SplatTexture[0]);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glTexCoordPointer(2, GL_FLOAT, 32, VertexArray + 6);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			glBindTexture(GL_TEXTURE_2D, Patch->BlendMapTexture[1]);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE0_ARB);
			glTexCoordPointer(2, GL_FLOAT, 32, VertexArray + 4);
			glVertexPointer(3, GL_FLOAT, 32, VertexArray);

			if (DeviceGet()->Info.Support.CVA && RenderData->DrawMode == TERRAIN_RENDER_DRAW_TEXTURE_SPLAT_CVA)
			{
				DeviceGetExt()->glLockArraysEXT(0, TerrainRenderPatchVertexSize(BaseData, Patch->Level));
			}

			glDrawElements(GL_TRIANGLES, IndexCount, GL_UNSIGNED_SHORT, IndexArray);

			glBlendFunc(GL_SRC_ALPHA, GL_ONE);

			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glBindTexture(GL_TEXTURE_2D, RenderData->SplatTexture[1]);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			glBindTexture(GL_TEXTURE_2D, Patch->BlendMapTexture[2]);
			glDrawElements(GL_TRIANGLES, IndexCount, GL_UNSIGNED_SHORT, IndexArray);

			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glBindTexture(GL_TEXTURE_2D, RenderData->SplatTexture[2]);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			glBindTexture(GL_TEXTURE_2D, Patch->BlendMapTexture[3]);
			glDrawElements(GL_TRIANGLES, IndexCount, GL_UNSIGNED_SHORT, IndexArray);

			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glDisable(GL_TEXTURE_2D);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			glBindTexture(GL_TEXTURE_2D, Patch->ColourMapTexure);
			glDrawElements(GL_TRIANGLES, IndexCount, GL_UNSIGNED_SHORT, IndexArray);

			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glEnable(GL_TEXTURE_2D);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE0_ARB);

			glDisable(GL_BLEND);

			if (DeviceGet()->Info.Support.CVA && RenderData->DrawMode == TERRAIN_RENDER_DRAW_TEXTURE_SPLAT_CVA)
			{
				DeviceGetExt()->glUnlockArraysEXT();
			}
		}
	}
	else
	{
		// Texture splat mode, assume multi-texture extensions and texture_env_combine extension
		float Alpha = 0.0f;

		struct EntityType * Camera = EntityGet(ENTITY_TYPE_CAMERA, RenderData->CameraEntityId);

		// Obtain camera base component data reference
		struct ComponentType * CameraBase = EntityComponentBase(Camera);
		struct CameraComponentBaseData * CameraBaseData = ComponentDataDerived(CameraBase, struct CameraComponentBaseData);

		// Get distance blend factor
		if (Patch->ViewPointDistance > CameraBaseData->Far / 2.0f)
		{
			// Too far, no splatting
			Alpha = 1.0f;
		}
		else if (Patch->ViewPointDistance > CameraBaseData->Far / 4.0f)
		{
			// Middle distance, fade out splatting
			Alpha = (2.0f * Patch->ViewPointDistance) / CameraBaseData->Far;

			Alpha = clamp(Alpha, 0.5f, 1.0f);
		}
		else
		{
			// Foreground
			Alpha = 0.5f;
		}

		if (Alpha == 1.0f)
		{
			// Too far, no splatting
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glDisable(GL_TEXTURE_2D);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_ARB);
			glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB_ARB, GL_MODULATE);
			glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB_ARB, GL_TEXTURE);
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_RGB_ARB, GL_SRC_COLOR);
			glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_RGB_ARB, GL_TEXTURE);
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND1_RGB_ARB, GL_SRC_ALPHA);
			glBindTexture(GL_TEXTURE_2D, Patch->BlendMapTexture[0]);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE0_ARB);
			glTexCoordPointer(2, GL_FLOAT, 32, VertexArray + 4);
			glVertexPointer(3, GL_FLOAT, 32, VertexArray);
			glDrawElements(GL_TRIANGLES, IndexCount, GL_UNSIGNED_SHORT, IndexArray);

			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glEnable(GL_TEXTURE_2D);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE0_ARB);
		}
		else
		{
			// Set distance blend factor
			glColor4f(0.0f, 0.0f, 0.0f, Alpha);

			glEnable(GL_BLEND);

			glBlendFunc(GL_SRC_ALPHA, GL_ZERO);

			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_ARB);
			glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB_ARB, GL_INTERPOLATE_ARB);
			glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB_ARB, GL_PREVIOUS_ARB);
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_RGB_ARB, GL_SRC_COLOR);
			glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_RGB_ARB, GL_TEXTURE);
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND1_RGB_ARB, GL_SRC_COLOR);
			glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE2_RGB_ARB, GL_PRIMARY_COLOR_ARB);
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND2_RGB_ARB, GL_ONE_MINUS_SRC_ALPHA);
			glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_ALPHA_ARB, GL_REPLACE);
			glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_ARB, GL_TEXTURE);
			glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_ARB, GL_SRC_ALPHA);
			glBindTexture(GL_TEXTURE_2D, Patch->BlendMapTexture[1]);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glTexCoordPointer(2, GL_FLOAT, 32, VertexArray + 4);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			glBindTexture(GL_TEXTURE_2D, RenderData->SplatTexture[0]);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE0_ARB);
			glTexCoordPointer(2, GL_FLOAT, 32, VertexArray + 6);
			glVertexPointer(3, GL_FLOAT, 32, VertexArray);

			if (DeviceGet()->Info.Support.CVA && RenderData->DrawMode == TERRAIN_RENDER_DRAW_TEXTURE_SPLAT_CVA)
			{
				DeviceGetExt()->glLockArraysEXT(0, TerrainRenderPatchVertexSize(BaseData, Patch->Level));
			}

			glDrawElements(GL_TRIANGLES, IndexCount, GL_UNSIGNED_SHORT, IndexArray);

			glBlendFunc(GL_SRC_ALPHA, GL_ONE);

			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glBindTexture(GL_TEXTURE_2D, Patch->BlendMapTexture[2]);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			glBindTexture(GL_TEXTURE_2D, RenderData->SplatTexture[1]);
			glDrawElements(GL_TRIANGLES, IndexCount, GL_UNSIGNED_SHORT, IndexArray);

			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glBindTexture(GL_TEXTURE_2D, Patch->BlendMapTexture[3]);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			glBindTexture(GL_TEXTURE_2D, RenderData->SplatTexture[2]);
			glDrawElements(GL_TRIANGLES, IndexCount, GL_UNSIGNED_SHORT, IndexArray);

			glDisable(GL_BLEND);

			if (DeviceGet()->Info.Support.CVA && RenderData->DrawMode == TERRAIN_RENDER_DRAW_TEXTURE_SPLAT_CVA)
			{
				DeviceGetExt()->glUnlockArraysEXT();
			}
		}
	}

	// Set fence for a patch so the cache manager won't reallocate its vertex array until it has been DMA'd
	if (RenderData->PatchVertexCache.Fast)
	{
		struct CacheObjectType ** Object = TerrainRenderPatchVertexPtr(RenderData, ParentIndex);

		DeviceGetExt()->glSetFenceNV((*Object)->Fence, GL_ALL_COMPLETED_NV);
	}

	// Restore patch translation
	glTranslatef(-Offset.x, -Offset.y, -Offset.z);
}

////////////////////////////////////////////////////////////
/// Render a terrain recursively
////////////////////////////////////////////////////////////
void TerrainRenderDrawRecursive(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger NodeIndex, uinteger ParentIndex, uinteger Left, uinteger Right, uinteger Top, uinteger Bottom)
{
	if (RenderData)
	{
		uinteger Node = RenderData->Quadtree[NodeIndex];
		uinteger ClipFlags = TerrainQuadtreeGetClipFlags(Node);

		// Check if node is outside
		if (FrustumInside(ClipFlags) || FrustumOverlap(ClipFlags))
		{
			uinteger Width = (Right >= Left) ? Right - Left : 0;

			if (Width > BaseData->HeightMapSize)
			{
				// Root case, start recursion
				uinteger X, Y;

				X = (Left + Right) >> 1;
				Y = (Top + Bottom) >> 1;

				TerrainRenderDrawRecursive(BaseData, RenderData, TerrainQuadtreeBottomLeft(NodeIndex), 0, Left, X, Top, Y);
				TerrainRenderDrawRecursive(BaseData, RenderData, TerrainQuadtreeBottomRight(NodeIndex), 0, X, Right, Top, Y);
				TerrainRenderDrawRecursive(BaseData, RenderData, TerrainQuadtreeTopLeft(NodeIndex), 0, Left, X, Y, Bottom);
				TerrainRenderDrawRecursive(BaseData, RenderData, TerrainQuadtreeTopRight(NodeIndex), 0, X, Right, Y, Bottom);
			}
			else if (Width > BaseData->PatchSize)
			{
				// Node case, recurse until leafs
				uinteger X, Y;

				X = (Left + Right) >> 1;
				Y = (Top + Bottom) >> 1;

				TerrainRenderDrawRecursive(BaseData, RenderData, TerrainQuadtreeBottomLeft(NodeIndex), TerrainQuadtreeBottomLeft(ParentIndex), Left, X, Top, Y);
				TerrainRenderDrawRecursive(BaseData, RenderData, TerrainQuadtreeBottomRight(NodeIndex), TerrainQuadtreeBottomRight(ParentIndex), X, Right, Top, Y);
				TerrainRenderDrawRecursive(BaseData, RenderData, TerrainQuadtreeTopLeft(NodeIndex), TerrainQuadtreeTopLeft(ParentIndex), Left, X, Y, Bottom);
				TerrainRenderDrawRecursive(BaseData, RenderData, TerrainQuadtreeTopRight(NodeIndex), TerrainQuadtreeTopRight(ParentIndex), X, Right, Y, Bottom);
			}
			else
			{
				// Leaf case, render patch
				TerrainRenderPatchDraw(BaseData, RenderData, NodeIndex, ParentIndex, Left, Right, Top, Bottom);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Render a terrain
////////////////////////////////////////////////////////////
void TerrainRenderDraw(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		// Get render component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

		struct EntityType * Camera = EntityGet(ENTITY_TYPE_CAMERA, RenderData->CameraEntityId);

		// Get camera base component data reference
		struct ComponentType * CameraBase = EntityComponentBase(Camera);
		struct CameraComponentBaseData * CameraBaseData = ComponentDataDerived(CameraBase, struct CameraComponentBaseData);

		// Get collision component data reference
		struct ComponentType * CameraCollision = EntityComponentCollisionFrustum(Camera);
		struct ComponentCollisionFrustumData * CameraCollisionData = ComponentDataCollisionFrustum(CameraCollision);

		// Calculate error metric
		TerrainRenderSetErrorMetric(Entity, CameraBaseData->FovX, RenderData->SourceHeight);

		// Calculate clip
		TerrainRenderClip(Entity, &CameraCollisionData->Frustum);

		// Calculate mip level
		TerrainRenderSetMipLevel(Entity);

		// Initialize cache frame
		CacheReset(&RenderData->PatchVertexCache);
		CacheReset(&RenderData->PatchIndexCache);

        // Enable fog
        #if (TERRAIN_RENDER_FOG == 1)
            glEnable(GL_FOG);
		#endif

		// Enable vertex attribs
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);

		if (RenderData->DrawMode == TERRAIN_RENDER_DRAW_WIREFRAME)
		{
			// Line mode
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			if (DeviceGet()->Info.Limits.Units > 1)
			{
				DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
				glDisable(GL_TEXTURE_2D);
				DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
				glDisableClientState(GL_TEXTURE_COORD_ARRAY);
				DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
				DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE0_ARB);
			}

			glDisable(GL_TEXTURE_2D);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		}
		else if (RenderData->DrawMode == TERRAIN_RENDER_DRAW_TEXTURE)
		{
			// Single pass texture mode
			if (DeviceGet()->Info.Limits.Units > 1)
			{
				DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
				glEnable(GL_TEXTURE_2D);
				glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
				glBindTexture(GL_TEXTURE_2D, RenderData->BaseTexture);
				DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
				DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE0_ARB);
			}

			glEnable(GL_TEXTURE_2D);
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		}
		else
		{
			// Multi-pass texture splat mode
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glEnable(GL_TEXTURE_2D);
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			glEnable(GL_TEXTURE_2D);
			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE0_ARB);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		}

		// Render terrain recursively starting from root node
		TerrainRenderDrawRecursive(BaseData, RenderData,
			0, 0,
			0, BaseData->HeightMapSize * BaseData->HeightMapTile,
			0, BaseData->HeightMapSize * BaseData->HeightMapTile);

		// Set back to default mode
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		if (DeviceGet()->Info.Limits.Units > 1)
		{
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE1_ARB);
			glDisable(GL_TEXTURE_2D);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE1_ARB);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0_ARB);
			DeviceGetExt()->glClientActiveTextureARB(GL_TEXTURE0_ARB);
		}

		glEnable(GL_TEXTURE_2D);
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		// Disable vertex attribs
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);


		// Disable fog
		#if (TERRAIN_RENDER_FOG == 1)
            glDisable(GL_FOG);
		#endif
	}

}

////////////////////////////////////////////////////////////
/// Set current reference to camera
////////////////////////////////////////////////////////////
void TerrainRenderSetCameraTarget(struct EntityType * Entity, EntityIdType CameraId)
{
	if (Entity && EntityGet(ENTITY_TYPE_CAMERA, CameraId))
	{
		// Get render component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

		// Set the camera id
		RenderData->CameraEntityId = CameraId;
	}
}

////////////////////////////////////////////////////////////
/// Set the error metric for the current field of view and window height
////////////////////////////////////////////////////////////
void TerrainRenderSetErrorMetric(struct EntityType * Entity, float FovX, uinteger Height)
{
	if (Entity)
	{
		// Get render component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

		FovX = clamp(FovX, 0.0f, 179.99f);

		RenderData->ErrorMetric = (float)tan(degtorad(FovX / 2.0f)) / (float)Height;
	}
}

////////////////////////////////////////////////////////////
/// Return number of vertex for a patch size at desired mip level
////////////////////////////////////////////////////////////
uinteger TerrainRenderPatchVertexSize(struct TerrainComponentBaseData * BaseData, uinteger Level)
{
	if (BaseData)
	{
		return sqr((BaseData->PatchSize >> Level) + 1);
	}

	return TERRAIN_RENDER_TRIANGLE_SIZE_INVALID;
}

////////////////////////////////////////////////////////////
/// Return number of triangles for a patch size at desired mip level
////////////////////////////////////////////////////////////
uinteger TerrainRenderPatchTriangleSize(struct TerrainComponentBaseData * BaseData, uinteger Level)
{
	if (BaseData)
	{
		return sqr(BaseData->PatchSize >> Level) * 2;
	}

	return TERRAIN_RENDER_VERTEX_SIZE_INVALID;
}

////////////////////////////////////////////////////////////
/// Return a pointer to the patch
////////////////////////////////////////////////////////////
struct TerrainPatchRenderData * TerrainRenderPatchImpl(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData, uinteger X, uinteger Y)
{
	if (BaseData && RenderData)
	{
		return &RenderData->Patches[X + Y * BaseData->PatchTile * BaseData->HeightMapTile];
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Retreive a patch
////////////////////////////////////////////////////////////
struct TerrainPatchRenderData * TerrainRenderPatch(struct EntityType * Entity, uinteger X, uinteger Y)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		// Get render component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

		return TerrainRenderPatchImpl(BaseData, RenderData, X, Y);
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Calculate the error for a patch at a given level
////////////////////////////////////////////////////////////
float TerrainRenderPatchErrorCompute(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger Level, float * VertexArray, TerrainRenderPatchErrorComputeType ComputeType)
{
	if (BaseData && RenderData && Level > 0 && VertexArray)
	{
		uinteger Count = 0;
		uinteger Step = 1 << Level;
		float Error = 0.0f;
		uinteger i0, i1, i2, j0, j1, j2;
		uinteger LimitX, LimitY;

		for (j0 = 0; j0 < BaseData->PatchSize; j0 = j1)
		{
			j1 = j0 + Step;

			if (j1 == BaseData->PatchSize)
			{
				LimitY = Step + 1;
			}
			else
			{
				LimitY = Step;
			}

			for (i0 = 0; i0 < BaseData->PatchSize; i0 = i1)
			{
				float HeightMapZ[TERRAIN_RENDER_QUADTREE_FACTOR];
				float LeftEdgeZ, RightEdgeZ;
				float LeftLerpedEdgeZ, RightLerpedEdgeZ;
				float InterpolantZ, LerpedZ, RealZ;

				i1 = i0 + Step;

				if (i1 == BaseData->PatchSize)
				{
					LimitX = Step + 1;
				}
				else
				{
					LimitX = Step;
				}

				// Heightmap z for patch points for mip level
				HeightMapZ[0] = VertexArray[TERRAIN_RENDER_VERTEX_STRIDE * RenderData->PatchIndex[j0 * (BaseData->PatchSize + 1) + i0] + 2];
				HeightMapZ[1] = VertexArray[TERRAIN_RENDER_VERTEX_STRIDE * RenderData->PatchIndex[j1 * (BaseData->PatchSize + 1) + i0] + 2];
				HeightMapZ[2] = VertexArray[TERRAIN_RENDER_VERTEX_STRIDE * RenderData->PatchIndex[j0 * (BaseData->PatchSize + 1) + i1] + 2];
				HeightMapZ[3] = VertexArray[TERRAIN_RENDER_VERTEX_STRIDE * RenderData->PatchIndex[j1 * (BaseData->PatchSize + 1) + i1] + 2];

				// Left & right edge interpolants
				LeftEdgeZ = (HeightMapZ[1] - HeightMapZ[0]) / (float)Step;
				RightEdgeZ = (HeightMapZ[3] - HeightMapZ[2]) / (float)Step;

				for (j2 = 0; j2 < LimitY; ++j2)
				{
					// Lerped left & right edge z
					LeftLerpedEdgeZ = HeightMapZ[0] + (float)j2 * LeftEdgeZ;
					RightLerpedEdgeZ = HeightMapZ[2] + (float)j2 * RightEdgeZ;

					InterpolantZ = (RightLerpedEdgeZ - LeftLerpedEdgeZ) / (float)Step;

					for (i2 = 0; i2 < LimitX; ++i2)
					{
						// Lerped z for patch point
						LerpedZ = LeftLerpedEdgeZ + (float)i2 * InterpolantZ;

						// Real heightmap z for patch point
						RealZ = VertexArray[TERRAIN_RENDER_VERTEX_STRIDE * RenderData->PatchIndex[(j0 + j2) * (BaseData->PatchSize + 1) + i0 + i2] + 2];

						if (ComputeType == TERRAIN_RENDER_PATCH_ERROR_MAX)
						{
							// Max error
							if ((float)fabs(LerpedZ - RealZ) > Error)
							{
								Error = (float)fabs(LerpedZ - RealZ);
							}
						}
						else if (ComputeType == TERRAIN_RENDER_PATCH_ERROR_AVERAGE)
						{
							// Average error
							Error += (float)fabs(LerpedZ - RealZ);
							++Count;
						}
					}
				}
			}
		}

		if (ComputeType == TERRAIN_RENDER_PATCH_ERROR_AVERAGE)
		{
			if (Count > 0)
			{
				Error /= (float)Count;
			}
		}

		return Error;
	}

	return 0.0f;
}

////////////////////////////////////////////////////////////
/// Recursively create the patch error arrays
////////////////////////////////////////////////////////////
void TerrainRenderPatchErrorInitializeRecursive(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger NodeIndex, uinteger Left, uinteger Right, uinteger Top, uinteger Bottom)
{
	if (RenderData)
	{
		uinteger Width = (Right >= Left) ? Right - Left : 0;

		if (Width > BaseData->PatchSize)
		{
			// Node case, recurse until leafs
			uinteger X, Y;

			X = (Left + Right) >> 1;
			Y = (Top + Bottom) >> 1;

			TerrainRenderPatchErrorInitializeRecursive(BaseData, RenderData, TerrainQuadtreeBottomLeft(NodeIndex), Left, X, Top, Y);
			TerrainRenderPatchErrorInitializeRecursive(BaseData, RenderData, TerrainQuadtreeBottomRight(NodeIndex), X, Right, Top, Y);
			TerrainRenderPatchErrorInitializeRecursive(BaseData, RenderData, TerrainQuadtreeTopLeft(NodeIndex), Left, X, Y, Bottom);
			TerrainRenderPatchErrorInitializeRecursive(BaseData, RenderData, TerrainQuadtreeTopRight(NodeIndex), X, Right, Y, Bottom);
		}
		else
		{
			// Leaf case, set up patch vertex
			float * VertexArray = TerrainRenderPatchVertex(BaseData, RenderData, NodeIndex, Left, Right, Top, Bottom);
			float * ErrorArray = TerrainRenderPatchError(RenderData, NodeIndex);
			uinteger Level = 0;

			ErrorArray[Level++] = 0.0f;

			while (Level < RenderData->PatchLevels)
			{
				float Error = TerrainRenderPatchErrorCompute(BaseData, RenderData, Level, VertexArray, TERRAIN_RENDER_PATCH_ERROR_MAX);

				ErrorArray[Level] = ErrorArray[Level - 1];

				if (Error > ErrorArray[Level])
				{
					ErrorArray[Level] = Error;
				}

				++Level;
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Create the patch error arrays
////////////////////////////////////////////////////////////
void TerrainRenderPatchErrorInitialize(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		// Get render component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

		TerrainRenderPatchErrorInitializeRecursive(BaseData, RenderData,
			0,
			0, BaseData->HeightMapSize,
			0, BaseData->HeightMapSize);
	}
}

////////////////////////////////////////////////////////////
/// Create the index array that will be used to access
///	patch vertex arrays in row major order
////////////////////////////////////////////////////////////
void TerrainRenderPatchIndexInitialize(struct EntityType * Entity)
{
	// Get base component and obtain data reference
	struct ComponentType * Base = EntityComponentBase(Entity);
	struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

	// Get render component and obtain data reference
	struct ComponentType * Render = EntityComponentRender(Entity);
	struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

	uinteger Step = BaseData->PatchSize;
	uinteger IndexCount = 0;
	uinteger i, j;

	// Initialize all index
	for (i = 0; i < sqr(BaseData->PatchSize + 1); ++i)
	{
		RenderData->PatchIndex[i] = TERRAIN_RENDER_INVALID_VERTEX_INDEX;
	}

	// Convert to row major order
	while (Step > 0)
	{
		for (j = 0; j <= BaseData->PatchSize; j += Step)
		{
			for (i = 0; i <= BaseData->PatchSize; i += Step)
			{
				uint16 * Index = &RenderData->PatchIndex[j * (BaseData->PatchSize + 1) + i];

				if (*Index == TERRAIN_RENDER_INVALID_VERTEX_INDEX)
				{
					*Index = IndexCount++;
				}
			}
		}

		Step >>= 1;
	}
}

////////////////////////////////////////////////////////////
/// Create the vertex array with vertexes in mip level order
////////////////////////////////////////////////////////////
void TerrainRenderPatchVertexInitialize(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger Left, uinteger Right, uinteger Top, uinteger Bottom, float * VertexArray)
{
	if (VertexArray)
	{
		uinteger i, j;
		uint8 * HeightMap = BaseData->HeightMap;
		float TextureScale = (float)RenderData->TextureTile / (float)BaseData->HeightMapSize;
		float BaseTextureScale = (float)RenderData->BaseTextureTile / (float)BaseData->HeightMapSize;
		uinteger CoordS = Left % (BaseData->HeightMapSize / RenderData->TextureTile);
		uinteger CoordT = Top % (BaseData->HeightMapSize / RenderData->TextureTile);

		if (HeightMap)
		{
			HeightMap = &HeightMap[Left + Top * (BaseData->HeightMapSize + 1)];
		}

		for (j = 0; j <= BaseData->PatchSize; ++j)
		{
			for (i = 0; i <= BaseData->PatchSize; ++i)
			{
				struct Vector3f Vertex;

				uinteger Index = RenderData->PatchIndex[j * (BaseData->PatchSize + 1) + i];

				Vertex.x = (float)(i + Left);
				Vertex.y = (float)(j + Top);

				// Texture 0
				VertexArray[TERRAIN_RENDER_VERTEX_STRIDE * Index + 4] = (CoordS + i) * TextureScale;
				VertexArray[TERRAIN_RENDER_VERTEX_STRIDE * Index + 5] = (CoordT + j) * TextureScale;

				// Texture 1
				VertexArray[TERRAIN_RENDER_VERTEX_STRIDE * Index + 6] = Vertex.x * BaseTextureScale;
				VertexArray[TERRAIN_RENDER_VERTEX_STRIDE * Index + 7] = Vertex.y * BaseTextureScale;

				// Vertex
				if (HeightMap)
				{
					Vertex.z = HeightMap[j*(BaseData->HeightMapSize + 1) + i];
				}
				else
				{
					Vertex.z = 0.0f;
				}

				// Scale the vector
				Vector3fMultiplyVectorEx(&Vertex, &Vertex, &BaseData->Scale);

				// Copy the vertex to the array
				VertexArray[8 * Index] = Vertex.x;
				VertexArray[8 * Index + 1] = Vertex.y;
				VertexArray[8 * Index + 2] = Vertex.z;
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Return the cached vertex array for a patch
////////////////////////////////////////////////////////////
float * TerrainRenderPatchVertex(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger NodeIndex, uinteger Left, uinteger Right, uinteger Top, uinteger Bottom)
{
	if (BaseData && RenderData)
	{
		struct CacheObjectType ** Object = TerrainRenderPatchVertexPtr(RenderData, NodeIndex);
		float * VertexArray;

		// todo: don't always use level 0, check error array first to get
		//		lowest mip level needed, don't forget dependence of build error
		//		array on get vertex array

		// Check if this cache object is valid
		if (*Object == NULL || (*Object)->Owner != Object)
		{
			// Stride = 8 for vertex array (incl 2 sets of texture coords)
			uinteger Size = (TerrainRenderPatchVertexSize(BaseData, 0) * TERRAIN_RENDER_VERTEX_STRIDE * sizeof(float));

			// Allocate cache object
			VertexArray = (float *)CacheAllocate(&RenderData->PatchVertexCache, Size, Object);

			// Rebuild array
			if (VertexArray != NULL)
			{
				// todo: replace '/ Size' and '% Size' with shift and mask (in whole code)
				Left %= BaseData->HeightMapSize;
				Right %= BaseData->HeightMapSize;
				Top %= BaseData->HeightMapSize;
				Bottom %= BaseData->HeightMapSize;

				// Initialize vertex array
				TerrainRenderPatchVertexInitialize(BaseData, RenderData, Left, Right, Top, Bottom, VertexArray);
			}
		}
		else
		{
			VertexArray = (float *)((*Object)->Data);
		}

		return VertexArray;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Write a horizontal triangle strip into a patch index array
////////////////////////////////////////////////////////////
uint16 TerrainRenderPatchIndexStripAcross(uint16 * PatchIndex, uinteger PatchSize,
	uint16 * TriangleIndex, uinteger * Index, uinteger X, uinteger Y, uinteger Step, uinteger End)
{
	uint16 TriangleCount = 0;
	uinteger i, j0 = Y, j1 = Y + Step, IndexCount = *Index;

	for (i = X; i < End; i += Step)
	{
		TriangleIndex[IndexCount++] = PatchIndex[j1 * (PatchSize + 1) + i];
		TriangleIndex[IndexCount++] = PatchIndex[j0 * (PatchSize + 1) + i];
		TriangleIndex[IndexCount++] = PatchIndex[j1 * (PatchSize + 1) + i + Step];

		TriangleIndex[IndexCount++] = PatchIndex[j0 * (PatchSize + 1) + i + Step];
		TriangleIndex[IndexCount] = TriangleIndex[IndexCount - 2];
		IndexCount++;
		TriangleIndex[IndexCount] = TriangleIndex[IndexCount - 4];
		IndexCount++;

		TriangleCount += 2;
	}

	*Index = IndexCount;

	return TriangleCount;
}

////////////////////////////////////////////////////////////
/// Write a vertical triangle strip into a patch index array
////////////////////////////////////////////////////////////
uint16 TerrainRenderPatchIndexStripDown(uint16 * PatchIndex, uinteger PatchSize,
	uint16 * TriangleIndex, uinteger * Index, uinteger X, uinteger Y, uinteger Step, uinteger End)
{
	uint16 TriangleCount = 0;
	uinteger i0 = X, i1 = X + Step, j, IndexCount = *Index;

	for (j = Y; j < End; j += Step)
	{
		TriangleIndex[IndexCount++] = PatchIndex[j * (PatchSize + 1) + i0];
		TriangleIndex[IndexCount++] = PatchIndex[j * (PatchSize + 1) + i1];
		TriangleIndex[IndexCount++] = PatchIndex[(j + Step) * (PatchSize + 1) + i0];

		TriangleIndex[IndexCount++] = PatchIndex[(j + Step) * (PatchSize + 1) + i1];
		TriangleIndex[IndexCount] = TriangleIndex[IndexCount - 2];
		IndexCount++;
		TriangleIndex[IndexCount] = TriangleIndex[IndexCount - 4];
		IndexCount++;

		TriangleCount += 2;
	}

	*Index = IndexCount;

	return TriangleCount;
}

////////////////////////////////////////////////////////////
/// Write the bottom row of triangle fans into a patch
/// index array, to glue the patch to a neighbour at
/// a higher mip level
////////////////////////////////////////////////////////////
uint16 TerrainRenderPatchIndexFansBottom(uint16 * PatchIndex, uinteger PatchSize,
	uint16 * TriangleIndex, uinteger * Index, uinteger MinLevel, uinteger MaxLevel)
{
	uint16 TriangleCount = 0;
	uinteger MaxStep = 1 << MaxLevel;
	uinteger MinStep = 1 << MinLevel;
	uinteger i = 0, IndexCount = *Index;
	uinteger j0 = MinStep, j1 = 2 * MinStep;

	while (i < PatchSize)
	{
		while (j0 < (i + MaxStep) && j0 < (PatchSize - MinStep))
		{
			TriangleIndex[IndexCount++] = PatchIndex[i];
			TriangleIndex[IndexCount++] = PatchIndex[MinStep*(PatchSize + 1) + j1];
			TriangleIndex[IndexCount++] = PatchIndex[MinStep*(PatchSize + 1) + j0];

			j0 = j1;
			j1 += MinStep;

			++TriangleCount;
		}

		TriangleIndex[IndexCount++] = PatchIndex[i];
		i += MaxStep;
		TriangleIndex[IndexCount++] = PatchIndex[i];
		TriangleIndex[IndexCount++] = PatchIndex[MinStep*(PatchSize + 1) + j0];

		++TriangleCount;
	}

	*Index = IndexCount;

	return TriangleCount;
}

////////////////////////////////////////////////////////////
/// Write the top row of triangle fans into a patch
/// index array, to glue the patch to a neighbour at
/// a higher mip level
////////////////////////////////////////////////////////////
uint16 TerrainRenderPatchIndexFansTop(uint16 * PatchIndex, uinteger PatchSize,
	uint16 * TriangleIndex, uinteger * Index, uinteger MinLevel, uinteger MaxLevel)
{
	uint16 TriangleCount = 0;
	uinteger MaxStep = 1 << MaxLevel;
	uinteger MinStep = 1 << MinLevel;
	uinteger i = 0, IndexCount = *Index, PatchSizeSqr = sqr(PatchSize);
	uinteger j0 = MinStep, j1 = 2 * MinStep;

	while (i < PatchSize)
	{

		while (j0 < (i + MaxStep) && j0 < (PatchSize - MinStep))
		{
			TriangleIndex[IndexCount++] = PatchIndex[PatchSizeSqr + PatchSize + i];
			TriangleIndex[IndexCount++] = PatchIndex[(PatchSize - MinStep) * (PatchSize + 1) + j0];
			TriangleIndex[IndexCount++] = PatchIndex[(PatchSize - MinStep) * (PatchSize + 1) + j1];

			j0 = j1;
			j1 += MinStep;

			++TriangleCount;
		}

		TriangleIndex[IndexCount++] = PatchIndex[PatchSizeSqr + PatchSize + i];
		TriangleIndex[IndexCount++] = PatchIndex[(PatchSize - MinStep) * (PatchSize + 1) + j0];
		i += MaxStep;
		TriangleIndex[IndexCount++] = PatchIndex[PatchSizeSqr + PatchSize + i];

		++TriangleCount;
	}

	*Index = IndexCount;

	return TriangleCount;
}

////////////////////////////////////////////////////////////
/// Write the right column of triangle fans into a patch
/// index array, to glue the patch to a neighbour at
/// a higher mip level
////////////////////////////////////////////////////////////
uint16 TerrainRenderPatchIndexFansRight(uint16 * PatchIndex, uinteger PatchSize,
	uint16 * TriangleIndex, uinteger * Index, uinteger MinLevel, uinteger MaxLevel)
{
	uint16 TriangleCount = 0;
	uinteger MaxStep = 1 << MaxLevel;
	uinteger MinStep = 1 << MinLevel;
	uinteger i = 0, IndexCount = *Index;
	uinteger j0 = MinStep, j1 = 2 * MinStep;

	while (i < PatchSize)
	{
		while (j0 < (i + MaxStep) && j0 < (PatchSize - MinStep))
		{
			TriangleIndex[IndexCount++] = PatchIndex[PatchSize + i*(PatchSize + 1)];
			TriangleIndex[IndexCount++] = PatchIndex[PatchSize - MinStep + j1 * (PatchSize + 1)];
			TriangleIndex[IndexCount++] = PatchIndex[PatchSize - MinStep + j0 * (PatchSize + 1)];

			j0 = j1;
			j1 += MinStep;

			++TriangleCount;
		}

		TriangleIndex[IndexCount++] = PatchIndex[PatchSize + i * (PatchSize + 1)];
		i += MaxStep;
		TriangleIndex[IndexCount++] = PatchIndex[PatchSize + i * (PatchSize + 1)];
		TriangleIndex[IndexCount++] = PatchIndex[PatchSize - MinStep + j0 * (PatchSize + 1)];

		++TriangleCount;
	}

	*Index = IndexCount;

	return TriangleCount;
}

////////////////////////////////////////////////////////////
///	Write the left column of triangle fans into a patch
/// index array, to glue the patch to a neighbour at
/// a higher mip level
////////////////////////////////////////////////////////////
uint16 TerrainRenderPatchIndexFansLeft(uint16 * PatchIndex, uinteger PatchSize,
	uint16 * TriangleIndex, uinteger * Index, uinteger MinLevel, uinteger MaxLevel)
{
	uint16 TriangleCount = 0;
	uinteger MaxStep = 1 << MaxLevel;
	uinteger MinStep = 1 << MinLevel;
	uinteger i = 0, IndexCount = *Index;
	uinteger j0 = MinStep, j1 = 2 * MinStep;

	while (i < PatchSize)
	{
		while (j0 < (i + MaxStep) && j0 < (PatchSize - MinStep))
		{
			TriangleIndex[IndexCount++] = PatchIndex[i * (PatchSize + 1)];
			TriangleIndex[IndexCount++] = PatchIndex[MinStep + (PatchSize + 1) * j0];
			TriangleIndex[IndexCount++] = PatchIndex[MinStep + (PatchSize + 1) * j1];

			j0 = j1;
			j1 += MinStep;

			++TriangleCount;
		}

		TriangleIndex[IndexCount++] = PatchIndex[i * (PatchSize + 1)];
		TriangleIndex[IndexCount++] = PatchIndex[MinStep + (PatchSize + 1) * j0];
		i += MaxStep;
		TriangleIndex[IndexCount++] = PatchIndex[i * (PatchSize + 1)];

		++TriangleCount;
	}

	*Index = IndexCount;

	return TriangleCount;
}

////////////////////////////////////////////////////////////
/// Calculate the index array for patch at geomip level,
///	taking into account the mip levels of its neighbour patches
////////////////////////////////////////////////////////////
void TerrainRenderPatchIndexArrayInitialize(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger Level, uinteger NeighbourLevel, uint16 * IndexArray)
{
	uinteger Index = 1;
	uinteger EdgeMask = 0;
	uinteger Step = 1 << Level;
	uint16 TriangleCount = 0;
	uinteger i;

	for (i = 0; i < TERRAIN_RENDER_QUADTREE_FACTOR; i++)
	{
		if (((NeighbourLevel >> (i * 8)) & UINTEGER_SUFFIX(0xFF)) > Level)
		{
			EdgeMask |= UINTEGER_SUFFIX(0x01) << i;
		}
	}

	if (EdgeMask == 0)
	{
		// All neighbours at same or lower level, just output all triangle strips
		for (i = 0; i < BaseData->PatchSize; i += Step)
		{
			TriangleCount += TerrainRenderPatchIndexStripAcross(RenderData->PatchIndex, BaseData->PatchSize,
				IndexArray, &Index,
				0, i,
				Step, BaseData->PatchSize);
		}
	}
	else
	{
		// Output triangle strips for patch middle
		for (i = Step; i < BaseData->PatchSize - Step; i += Step)
		{
			TriangleCount += TerrainRenderPatchIndexStripAcross(RenderData->PatchIndex, BaseData->PatchSize,
				IndexArray, &Index,
				Step, i,
				Step, BaseData->PatchSize - Step);
		}

		// Patch left edge
		if (EdgeMask & UINTEGER_SUFFIX(0x01))
		{
			// Output triangle fans
			TriangleCount += TerrainRenderPatchIndexFansLeft(RenderData->PatchIndex, BaseData->PatchSize,
				IndexArray, &Index,
				Level, NeighbourLevel & UINTEGER_SUFFIX(0xFF));
		}
		else
		{
			// Output triangle strip
			IndexArray[Index++] = RenderData->PatchIndex[0];
			IndexArray[Index++] = RenderData->PatchIndex[Step * (BaseData->PatchSize + 2)];
			IndexArray[Index++] = RenderData->PatchIndex[Step * (BaseData->PatchSize + 1)];

			++TriangleCount;

			TriangleCount += TerrainRenderPatchIndexStripDown(RenderData->PatchIndex, BaseData->PatchSize,
				IndexArray, &Index,
				0, Step,
				Step, BaseData->PatchSize - Step);

			IndexArray[Index++] = RenderData->PatchIndex[(BaseData->PatchSize - Step)*(BaseData->PatchSize + 1)];
			IndexArray[Index++] = RenderData->PatchIndex[BaseData->PatchSize * (BaseData->PatchSize + 1 - Step)];
			IndexArray[Index++] = RenderData->PatchIndex[BaseData->PatchSize * (BaseData->PatchSize + 1)];

			++TriangleCount;
		}

		// Patch right edge
		if (EdgeMask & UINTEGER_SUFFIX(0x02))
		{
			// Output triangle fans
			TriangleCount += TerrainRenderPatchIndexFansRight(RenderData->PatchIndex, BaseData->PatchSize,
				IndexArray, &Index,
				Level, (NeighbourLevel >> 8) & UINTEGER_SUFFIX(0xFF));
		}
		else
		{
			// Output triangle strip
			IndexArray[Index++] = RenderData->PatchIndex[BaseData->PatchSize];
			IndexArray[Index++] = RenderData->PatchIndex[BaseData->PatchSize * (Step + 1) + Step];
			IndexArray[Index++] = RenderData->PatchIndex[(Step + 1) * BaseData->PatchSize];

			++TriangleCount;

			TriangleCount += TerrainRenderPatchIndexStripDown(RenderData->PatchIndex, BaseData->PatchSize,
				IndexArray, &Index,
				BaseData->PatchSize - Step, Step,
				Step, BaseData->PatchSize - Step);

			IndexArray[Index++] = RenderData->PatchIndex[BaseData->PatchSize * (BaseData->PatchSize + 2)];
			IndexArray[Index++] = RenderData->PatchIndex[(BaseData->PatchSize - Step)*(BaseData->PatchSize + 2)];
			IndexArray[Index++] = RenderData->PatchIndex[BaseData->PatchSize * (BaseData->PatchSize + 2 - Step) - Step];

			++TriangleCount;
		}

		// Patch top edge
		if (EdgeMask & UINTEGER_SUFFIX(0x04))
		{
			// Output triangle fans
			TriangleCount += TerrainRenderPatchIndexFansTop(RenderData->PatchIndex, BaseData->PatchSize,
				IndexArray, &Index,
				Level, (NeighbourLevel >> 16) & UINTEGER_SUFFIX(0xFF));
		}
		else
		{
			// Output triangle strip
			IndexArray[Index++] = RenderData->PatchIndex[BaseData->PatchSize * (BaseData->PatchSize + 1) + Step];
			IndexArray[Index++] = RenderData->PatchIndex[BaseData->PatchSize * (BaseData->PatchSize + 1)];
			IndexArray[Index++] = RenderData->PatchIndex[BaseData->PatchSize * (BaseData->PatchSize + 1 - Step)];

			++TriangleCount;

			TriangleCount += TerrainRenderPatchIndexStripAcross(RenderData->PatchIndex, BaseData->PatchSize,
				IndexArray, &Index,
				Step, BaseData->PatchSize - Step,
				Step, BaseData->PatchSize - Step);

			IndexArray[Index++] = RenderData->PatchIndex[(BaseData->PatchSize - Step) * (BaseData->PatchSize + 2)];
			IndexArray[Index++] = RenderData->PatchIndex[BaseData->PatchSize * (BaseData->PatchSize + 2)];
			IndexArray[Index++] = RenderData->PatchIndex[BaseData->PatchSize * (BaseData->PatchSize + 2) - Step];

			++TriangleCount;
		}

		// Patch bottom edge
		if (EdgeMask & UINTEGER_SUFFIX(0x08))
		{
			// Output triangle fans
			TriangleCount += TerrainRenderPatchIndexFansBottom(RenderData->PatchIndex, BaseData->PatchSize,
				IndexArray, &Index,
				Level, (NeighbourLevel >> 24) & UINTEGER_SUFFIX(0xFF));
		}
		else
		{
			// Output triangle strip
			IndexArray[Index++] = RenderData->PatchIndex[Step * (BaseData->PatchSize + 2)];
			IndexArray[Index++] = RenderData->PatchIndex[0];
			IndexArray[Index++] = RenderData->PatchIndex[Step];

			++TriangleCount;

			TriangleCount += TerrainRenderPatchIndexStripAcross(RenderData->PatchIndex, BaseData->PatchSize,
				IndexArray, &Index,
				Step, 0,
				Step, BaseData->PatchSize - Step);

			IndexArray[Index++] = RenderData->PatchIndex[BaseData->PatchSize - Step];
			IndexArray[Index++] = RenderData->PatchIndex[BaseData->PatchSize];
			IndexArray[Index++] = RenderData->PatchIndex[(Step + 1) * BaseData->PatchSize];

			++TriangleCount;
		}
	}

	IndexArray[0] = TriangleCount;
}

////////////////////////////////////////////////////////////
/// Return the cached index array for a patch
////////////////////////////////////////////////////////////
uint16 * TerrainRenderPatchIndex(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger NodeIndex, uinteger Level, uinteger NeighbourLevel)
{
	if (BaseData && RenderData)
	{
		struct CacheObjectType ** Object = TerrainRenderPatchIndexPtr(RenderData, NodeIndex);
		uint16 * IndexArray;
		bool Alloc = false, Build = false;

		// Check if this cache object is valid
		if (*Object == NULL || (*Object)->Owner != Object)
		{
			// Not valid, allocate cache object and rebuild index array
			Alloc = Build = true;
		}
		else
		{
			uinteger * LevelPtr;

			// Valid, check level and neighbour levels
			IndexArray = (uint16 *)((*Object)->Data);

			// Set level pointer for testing levels
			LevelPtr = (uinteger *)IndexArray;

			if (*LevelPtr != Level)
			{
				// Level is wrong
				Alloc = Build = true;
			}

			// Go to next position
			++LevelPtr;

			if (*LevelPtr != NeighbourLevel)
			{
				// Neighbour levels are wrong
				Build = true;
				*LevelPtr = NeighbourLevel;
			}

			// Go to next position
			++LevelPtr;

			// Restore index array pointer to the first triangle list element (the triangle count)
			IndexArray = (uint16 *)LevelPtr;
		}

		// Reallocate if need
		if (Alloc)
		{
			// Get a new cache object
			// [Level, NeighbourLevel] (2 * sizeof(uinteger)) + [TriangleCount] (sizeof(uint16)) + [IndexArray] (sizeof(uint16) * (3 * TriangleCount))
			uinteger Size = ((3 * TerrainRenderPatchTriangleSize(BaseData, Level) + 1) * sizeof(uint16)) + (sizeof(uinteger) * 2);

			// Allocate cache index array
			IndexArray = (uint16 *)CacheAllocate(&RenderData->PatchIndexCache, Size, Object);

			if (IndexArray != NULL)
			{
				uinteger * LevelPtr = (uinteger *)IndexArray;

				// Set current level
				*LevelPtr = Level;

				// Go to next position
				++LevelPtr;

				// Set neighbour level
				*LevelPtr = NeighbourLevel;

				// Go to next position
				++LevelPtr;

				// Restore index array pointer to the first triangle list element (the triangle count)
				IndexArray = (uint16 *)LevelPtr;
			}
			else
			{
				// Invalid vertex array, handle error
			}
		}

		// Rebuild if need
		if (Build)
		{
			TerrainRenderPatchIndexArrayInitialize(BaseData, RenderData, Level, NeighbourLevel, IndexArray);
		}

		return IndexArray;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set the geo-mip level for the visible parts of the terrain
////////////////////////////////////////////////////////////
void TerrainRenderSetMipLevelRecursive(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger NodeIndex, uinteger ParentIndex, uinteger Left, uinteger Right, uinteger Top, uinteger Bottom)
{
	if (BaseData && RenderData)
	{
		uinteger Node = RenderData->Quadtree[NodeIndex];
		uinteger ClipFlags = TerrainQuadtreeGetClipFlags(Node);

		// Check if node is outside
		if (FrustumInside(ClipFlags) || FrustumOverlap(ClipFlags))
		{
			uinteger Width = (Right >= Left) ? Right - Left : 0;
			uinteger X, Y;

			X = (Left + Right) >> 1;
			Y = (Top + Bottom) >> 1;

			if (Width > BaseData->HeightMapSize)
			{
				// Root case, start recursion
				TerrainRenderSetMipLevelRecursive(BaseData, RenderData, TerrainQuadtreeBottomLeft(NodeIndex), 0, Left, X, Top, Y);
				TerrainRenderSetMipLevelRecursive(BaseData, RenderData, TerrainQuadtreeBottomRight(NodeIndex), 0, X, Right, Top, Y);
				TerrainRenderSetMipLevelRecursive(BaseData, RenderData, TerrainQuadtreeTopLeft(NodeIndex), 0, Left, X, Y, Bottom);
				TerrainRenderSetMipLevelRecursive(BaseData, RenderData, TerrainQuadtreeTopRight(NodeIndex), 0, X, Right, Y, Bottom);
			}
			else if (Width > BaseData->PatchSize)
			{
				// Node case, recurse until leafs
				TerrainRenderSetMipLevelRecursive(BaseData, RenderData, TerrainQuadtreeBottomLeft(NodeIndex), TerrainQuadtreeBottomLeft(ParentIndex), Left, X, Top, Y);
				TerrainRenderSetMipLevelRecursive(BaseData, RenderData, TerrainQuadtreeBottomRight(NodeIndex), TerrainQuadtreeBottomRight(ParentIndex), X, Right, Top, Y);
				TerrainRenderSetMipLevelRecursive(BaseData, RenderData, TerrainQuadtreeTopLeft(NodeIndex), TerrainQuadtreeTopLeft(ParentIndex), Left, X, Y, Bottom);
				TerrainRenderSetMipLevelRecursive(BaseData, RenderData, TerrainQuadtreeTopRight(NodeIndex), TerrainQuadtreeTopRight(ParentIndex), X, Right, Y, Bottom);
			}
			else
			{
				// Leaf case, set the patch mip level
				struct TerrainPatchRenderData * Patch = TerrainRenderPatchImpl(BaseData, RenderData, Left / BaseData->PatchSize, Top / BaseData->PatchSize);

				float * ErrorArray = TerrainRenderPatchError(RenderData, ParentIndex);

				uinteger LevelIt, Level = RenderData->PatchLevels - 1;

				struct EntityType * Camera = EntityGet(ENTITY_TYPE_CAMERA, RenderData->CameraEntityId);

				// Obtain camera base component data reference
				struct ComponentType * CameraBase = EntityComponentBase(Camera);
				struct CameraComponentBaseData * CameraBaseData = ComponentDataDerived(CameraBase, struct CameraComponentBaseData);

				// Get transform component
				struct ComponentType * CameraTransform = EntityComponentTransform(Camera);
				struct CameraComponentTransformData * CameraTransformData = ComponentDataDerived(CameraTransform, struct CameraComponentTransformData);

				float LogDistance, Distance;

				struct Vector3f Middle, Orientation;

				// Calculate the middle point of the patch bounding box
				Middle.x = X * BaseData->Scale.x;
				Middle.y = Y * BaseData->Scale.y;
				Middle.z = ((TerrainQuadtreeGetMinZ(Node) + TerrainQuadtreeGetMaxZ(Node)) >> 1) * BaseData->Scale.z;

				// Retreive orientation vector
				QuaternionToAxisAngle(&CameraTransformData->Orientation, &Orientation, NULL);

				// Get perpendicular distance between viewpoint and patch middle
				Distance = fabsf(Vector3fDot(&CameraBaseData->Target, &Orientation) - Vector3fDot(&Middle, &Orientation));

				Patch->ViewPointDistance = Distance;

				// Get mip level based on log distance
				LogDistance = (CameraBaseData->Far - Distance) / CameraBaseData->Far;

				if (LogDistance <= 0.0f)
				{
					LevelIt = 0;
				}
				else
				{
					LevelIt = (uinteger)(BaseData->PatchSize * LogDistance);

					while (LevelIt)
					{
						--Level;
						LevelIt >>= 1;
					}
				}

				// Get mip level based on screen distortion metric
				Distance *= RenderData->ErrorMetric;

				LevelIt = RenderData->PatchLevels - 1;

				while (LevelIt >= 0 && ErrorArray[LevelIt] > Distance)
				{
					--LevelIt;
				}

				// If screen distortion error allows a higher mip level then use it, e.g. flat patch
				if (LevelIt > Level)
				{
					Level = LevelIt;
				}

				// Set patch level
				Patch->Level = Level;
			}
		}

	}
}

////////////////////////////////////////////////////////////
/// Set the geo-mip level for the visible parts of the terrain
////////////////////////////////////////////////////////////
void TerrainRenderSetMipLevel(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		// Get render component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

		uinteger Size = BaseData->HeightMapSize * BaseData->HeightMapTile;

		// Set recursively the geo-mip level
		TerrainRenderSetMipLevelRecursive(BaseData, RenderData,
			0, 0,
			0, Size, 0, Size);
	}
}

////////////////////////////////////////////////////////////
/// Force all patches to a given geo-mip level
////////////////////////////////////////////////////////////
void TerrainRenderForceMipLevel(struct EntityType * Entity, uinteger Level)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		// Get render component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

		uinteger PatchCount = BaseData->PatchTile * BaseData->HeightMapTile;
		uinteger i, j;

		for (j = 0; j < PatchCount; ++j)
		{
			for (i = 0; i < PatchCount; ++i)
			{
				struct TerrainPatchRenderData * Patch = TerrainRenderPatchImpl(BaseData, RenderData, i, j);

				// Set the level
				Patch->Level = Level;
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Recursively initialize terrain render quadtree
////////////////////////////////////////////////////////////
uinteger TerrainRenderQuadtreeInitializeRecursive(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger NodeIndex, uinteger ParentIndex, uinteger Left, uinteger Right, uinteger Top, uinteger Bottom)
{
	if (BaseData && RenderData)
	{
		uinteger Width = (Right >= Left) ? Right - Left : 0;
		uinteger X, Y;
		uint8 Z[2] =
		{
            ArrayInit(0, UINT8_MAX_RANGE), ArrayInit(1, UINT8_MIN_RANGE)
		}; // 0 = Min, 1 = Max

		X = (Left + Right) >> 1;
		Y = (Top + Bottom) >> 1;

		if (Width > BaseData->HeightMapSize)
		{
			// Root case, start recursion
			uinteger i, Quad[TERRAIN_RENDER_QUADTREE_FACTOR];

			Quad[0] = TerrainRenderQuadtreeInitializeRecursive(BaseData, RenderData, TerrainQuadtreeBottomLeft(NodeIndex), 0, Left, X, Top, Y);
			Quad[1] = TerrainRenderQuadtreeInitializeRecursive(BaseData, RenderData, TerrainQuadtreeBottomRight(NodeIndex), 0, X, Right, Top, Y);
			Quad[2] = TerrainRenderQuadtreeInitializeRecursive(BaseData, RenderData, TerrainQuadtreeTopLeft(NodeIndex), 0, Left, X, Y, Bottom);
			Quad[3] = TerrainRenderQuadtreeInitializeRecursive(BaseData, RenderData, TerrainQuadtreeTopRight(NodeIndex), 0, X, Right, Y, Bottom);

			// Get max and min z across all child nodes
			for (i = 0; i < TERRAIN_RENDER_QUADTREE_FACTOR; ++i)
			{
				uint8 MinZ = TerrainQuadtreeGetMinZ(Quad[i]);
				uint8 MaxZ = TerrainQuadtreeGetMaxZ(Quad[i]);

				if (MinZ < Z[0])
				{
					Z[0] = MinZ;
				}

				if (MaxZ > Z[1])
				{
					Z[1] = MaxZ;
				}
			}
		}
		else if (Width > BaseData->PatchSize)
		{
			// Node case, recurse until leafs
			uinteger i, Quad[TERRAIN_RENDER_QUADTREE_FACTOR];

			Quad[0] = TerrainRenderQuadtreeInitializeRecursive(BaseData, RenderData, TerrainQuadtreeBottomLeft(NodeIndex), TerrainQuadtreeBottomLeft(ParentIndex), Left, X, Top, Y);
			Quad[1] = TerrainRenderQuadtreeInitializeRecursive(BaseData, RenderData, TerrainQuadtreeBottomRight(NodeIndex), TerrainQuadtreeBottomRight(ParentIndex), X, Right, Top, Y);
			Quad[2] = TerrainRenderQuadtreeInitializeRecursive(BaseData, RenderData, TerrainQuadtreeTopLeft(NodeIndex), TerrainQuadtreeTopLeft(ParentIndex), Left, X, Y, Bottom);
			Quad[3] = TerrainRenderQuadtreeInitializeRecursive(BaseData, RenderData, TerrainQuadtreeTopRight(NodeIndex), TerrainQuadtreeTopRight(ParentIndex), X, Right, Y, Bottom);

			// Get max and min z across all child nodes
			for (i = 0; i < TERRAIN_RENDER_QUADTREE_FACTOR; ++i)
			{
				uint8 MinZ = TerrainQuadtreeGetMinZ(Quad[i]);
				uint8 MaxZ = TerrainQuadtreeGetMaxZ(Quad[i]);

				if (MinZ < Z[0])
				{
					Z[0] = MinZ;
				}

				if (MaxZ > Z[1])
				{
					Z[1] = MaxZ;
				}
			}
		}
		else
		{
			// Leaf case, get the patch height
			float * VertexArray = TerrainRenderPatchVertex(BaseData, RenderData, ParentIndex, Left, Right, Top, Bottom);
			uinteger i, j;

			// Get max and min z for patch
			for (j = 0; j <= BaseData->PatchSize; ++j)
			{
				for (i = 0; i <= BaseData->PatchSize; ++i)
				{
					uinteger Index = RenderData->PatchIndex[j * (BaseData->PatchSize + 1) + i];
					uint8 VertexZ = (uint8)(VertexArray[TERRAIN_RENDER_VERTEX_STRIDE * Index + 2] / BaseData->Scale.z);

					if (VertexZ < Z[0])
					{
						Z[0] = VertexZ;
					}

					if (VertexZ > Z[1])
					{
						Z[1] = VertexZ;
					}
				}
			}
		}

		// Save max and min z
		RenderData->Quadtree[NodeIndex] = 0;
		RenderData->Quadtree[NodeIndex] = TerrainQuadtreeSetMinZ(RenderData->Quadtree[NodeIndex], Z[0]);
		RenderData->Quadtree[NodeIndex] = TerrainQuadtreeSetMaxZ(RenderData->Quadtree[NodeIndex], Z[1]);

		// Set all clip flags on initially
		RenderData->Quadtree[NodeIndex] = TerrainQuadtreeSetClipFlags(RenderData->Quadtree[NodeIndex], FRUSTUM_OVERLAP);

		return RenderData->Quadtree[NodeIndex];
	}

	return 0;
}

////////////////////////////////////////////////////////////
/// Initialize terrain render quadtree
////////////////////////////////////////////////////////////
void TerrainRenderQuadtreeInitialize(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get base component and obtain data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		// Get render component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

		uinteger Size = BaseData->HeightMapSize * BaseData->HeightMapTile;

		TerrainRenderQuadtreeInitializeRecursive(BaseData, RenderData,
			0, 0,
			0, Size, 0, Size);
	}
}

////////////////////////////////////////////////////////////
/// Recursively clip the terrain quadtree to a view frustum
////////////////////////////////////////////////////////////
void TerrainRenderClipRecursive(struct TerrainComponentBaseData * BaseData, struct TerrainComponentRenderData * RenderData,
	uinteger NodeIndex, uinteger Left, uinteger Right, uinteger Top, uinteger Bottom,
	struct FrustumType * Frustum, uinteger ParentClipFlags)
{
	if (BaseData && RenderData)
	{
		uinteger Node = RenderData->Quadtree[NodeIndex];
		uinteger ClipFlags = TerrainQuadtreeGetClipFlags(Node);
		uinteger PrevClipFlags = ClipFlags;
		uinteger Width = (Right >= Left) ? Right - Left : 0;

		// Test clip flags
		if (FrustumInside(ParentClipFlags))
		{
			// If parent node is in, the children are too
			ClipFlags = FRUSTUM_INSIDE;
		}
		else if (FrustumOutside(ParentClipFlags))
		{
			// If parent node is out, the children are too
			ClipFlags = FRUSTUM_OUTSIDE;
		}
		else
		{
			struct Vector3f Min, Max;
			struct BoundingBox Box;

			// Calculate bounds
			Vector3fSet(&Min, (float)Left, (float)Top, (float)TerrainQuadtreeGetMinZ(Node));
			Vector3fSet(&Max, (float)Right, (float)Bottom, (float)TerrainQuadtreeGetMaxZ(Node));

			// Scale bounds
			Vector3fMultiplyVectorEx(&Min, &Min, &BaseData->Scale);
			Vector3fMultiplyVectorEx(&Max, &Max, &BaseData->Scale);

			// Construct quadtree node bounding box
			BoundingBoxSet(&Box, &Min, &Max);

			// Clip to frustum
			ClipFlags = FrustumCheckBoundingBox(Frustum, &Box, ParentClipFlags);
		}

		// Store the clip flags in the node
		if (ClipFlags != PrevClipFlags)
		{
			Node = TerrainQuadtreeSetClipFlags(Node, ClipFlags);
			RenderData->Quadtree[NodeIndex] = Node;
		}

		// Test clip flags for recursion
		if (FrustumOutside(ClipFlags))
		{
			// If node is outside, don't go more deep
			return;
		}
		else if ((ClipFlags == PrevClipFlags) && FrustumInside(ClipFlags))
		{
			// If clip has not changed and all children nodes are inside,
			// all of them have not changed too
			return;
		}
		else if (Width > BaseData->PatchSize)
		{
			// If this node is not a leaf, go deeper
			uinteger X, Y;

			X = (Left + Right) >> 1;
			Y = (Top + Bottom) >> 1;

			TerrainRenderClipRecursive(BaseData, RenderData, TerrainQuadtreeBottomLeft(NodeIndex), Left, X, Top, Y, Frustum, ClipFlags);
			TerrainRenderClipRecursive(BaseData, RenderData, TerrainQuadtreeBottomRight(NodeIndex), X, Right, Top, Y, Frustum, ClipFlags);
			TerrainRenderClipRecursive(BaseData, RenderData, TerrainQuadtreeTopLeft(NodeIndex), Left, X, Y, Bottom, Frustum, ClipFlags);
			TerrainRenderClipRecursive(BaseData, RenderData, TerrainQuadtreeTopRight(NodeIndex), X, Right, Y, Bottom, Frustum, ClipFlags);
		}
	}
}

////////////////////////////////////////////////////////////
/// Clip the terrain render to a view frustum
////////////////////////////////////////////////////////////
void TerrainRenderClip(struct EntityType * Entity, struct FrustumType * Frustum)
{
	if (Entity && Frustum)
	{
		// Get base component and obtain data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		// Get render component and obtain data reference
		struct ComponentType * Render = EntityComponentRender(Entity);
		struct TerrainComponentRenderData * RenderData = ComponentDataDerived(Render, struct TerrainComponentRenderData);

		uinteger Size = BaseData->HeightMapSize * BaseData->HeightMapTile;

		TerrainRenderClipRecursive(BaseData, RenderData,
			0,
			0, Size, 0, Size,
			Frustum, FRUSTUM_OVERLAP);
	}
}

////////////////////////////////////////////////////////////
/// Return the terrain render derived data size
////////////////////////////////////////////////////////////
uinteger TerrainComponentRenderSize()
{
	return sizeof(struct TerrainComponentRenderData);
}
