/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Account.h>
#include <Game/PlayerManager.h>
#include <DataBase/Account.h>
#include <DataType/Vector.h>
#include <Memory/General.h>
#include <string.h> // strcmp

////////////////////////////////////////////////////////////
///	Initialize account player info data
////////////////////////////////////////////////////////////
void AccountPlayerInfoInitialize(struct AccountPlayerInfo * PlayerInfo)
{
	PlayerInfo->Name[0] = NULLCHAR;
	PlayerInfo->Level = 0;
	PlayerInfo->Gold = 0;
	PlayerInfo->Dead = false;
	PlayerInfo->Aparence.Head = PlayerInfo->Aparence.Body = PlayerInfo->Aparence.Footwear = 0;
}

////////////////////////////////////////////////////////////
///	Initialize account data
////////////////////////////////////////////////////////////
void AccountInitialize(struct AccountData * Account)
{
	Account->Name[0] = Account->Password[0] = Account->Email[0] = NULLCHAR;

	Account->Banned = Account->Logged = Account->Active = false;

	Account->PlayersReference = VectorNew(sizeof(uinteger));

	Account->PlayerLogged = ACCOUNT_PLAYER_NO_LOGGED;
}

////////////////////////////////////////////////////////////
///	Check if the password is correct
////////////////////////////////////////////////////////////
bool AccountCheckPassword(struct AccountData * Account, char * Password)
{
	return (strcmp(Account->Password, Password) == 0);
}

////////////////////////////////////////////////////////////
///	Check if the email is correct
////////////////////////////////////////////////////////////
bool AccountCheckEmail(struct AccountData * Account, char * Email)
{
	return (strcmp(Account->Email, Email) != 0);
}

////////////////////////////////////////////////////////////
///	Check if account is banned
////////////////////////////////////////////////////////////
bool AccountCheckBan(struct AccountData * Account)
{
	return (Account->Banned == true);
}

////////////////////////////////////////////////////////////
///	Check if account is logged
////////////////////////////////////////////////////////////
bool AccountCheckLogged(struct AccountData * Account)
{
	return (Account->Logged == true);
}

////////////////////////////////////////////////////////////
///	Login an account
////////////////////////////////////////////////////////////
uint32 AccountLogin(struct AccountData * Account, char * Email, char * Password)
{
	struct DBAccountData DBAccount;

	// Get account from database
	if (DBAccountGetDataByEmail(&DBAccount, Email))
	{
		// Convert account to database object
		DBAccountToAccount(&DBAccount, Account);

		// Check password
		if (!AccountCheckPassword(Account, Password))
		{
			return ACCOUNT_LOG_IN_ERROR_PASSWORD;
		}

		// Check ban
		if (AccountCheckBan(Account))
		{
			return ACCOUNT_LOG_IN_ERROR_BAN;
		}

		// Check if it is logged
		if (AccountCheckLogged(Account))
		{
			return ACCOUNT_LOG_IN_ERROR_LOGGED;
		}

		// Update basic player data
		if (DBAccountGetPlayersSize(&DBAccount) > 0)
		{
			Vector DBPlayerList = VectorNew(sizeof(struct DBCharacterData));

			// Get players from
			DBAccountGetPlayers(&DBAccount, DBPlayerList);

			// Insert into player manager and obtain the references
			PlayerManagerAddList(DBPlayerList, Account->PlayersReference);

			// Destroy temporal player list
			VectorDestroy(DBPlayerList);
		}

		return ACCOUNT_LOG_IN_ERROR_NONE;
	}

	// Invalid account name
	return ACCOUNT_LOG_IN_ERROR_NAME;
}


////////////////////////////////////////////////////////////
///	Logout an account
////////////////////////////////////////////////////////////
bool AccountLogout(struct AccountData * Account)
{
	// todo

	/*
	bool LoggedFlag = false;

	// Update logged flag
	Account->Logged = false;

	// Check from database the value
	if (DBAccountCheckLogged(Account, &LoggedFlag))
	{
		// Check for integrity of the account
		if (LoggedFlag)
		{
			// Save the user ip
			// ..
		}
		else
		{
			// Ban this account
			Account->Banned = true;
		}

		// Store it
		// ..

		return LoggedFlag;
	}
	*/
	return false;
}

////////////////////////////////////////////////////////////
/// Get player info list of an account
////////////////////////////////////////////////////////////
void AccountGetPlayersInfo(struct AccountData * Account, struct AccountPlayerInfo * PlayerList, uint32 * Count)
{
	uint32 i;

	// Update number of players
	*Count = VectorSize(Account->PlayersReference);

	for (i = 0; i < *Count; i++)
	{
		uinteger Position = VectorAtT(Account->PlayersReference, i, uinteger);
		struct CharacterData * Player = PlayerManagerGetByPosition(Position);

		// Copy each information of player
		AccountPlayerInfoCopyPlayer(&PlayerList[i], Player);
	}
}

////////////////////////////////////////////////////////////
///	Login a player of an account
////////////////////////////////////////////////////////////
uint32 AccountPlayerLogin(struct AccountData * Account, uint32 Index)
{
	// Check if player is logged
	if (Account->PlayerLogged != ACCOUNT_PLAYER_NO_LOGGED)
	{
		return PLAYER_LOG_IN_ERROR_LOGGED;
	}
	else if (Account->PlayerLogged == ACCOUNT_PLAYER_NO_LOGGED && Index < VectorSize(Account->PlayersReference))
	{
		struct CharacterData * Player = PlayerManagerGetByPosition(VectorAtT(Account->PlayersReference, Index, uinteger));

		uint32 Result = PlayerLogin(Player);

		if (Result == PLAYER_LOG_IN_ERROR_NONE)
		{
			// Set the current player
			Account->PlayerLogged = Index;

			return PLAYER_LOG_IN_ERROR_NONE;
		}
		else
		{
			Account->PlayerLogged = ACCOUNT_PLAYER_NO_LOGGED;

			return Result;
		}
	}
	else
	{
		return PLAYER_LOG_IN_ERROR_INVALID;
	}
}

////////////////////////////////////////////////////////////
///	Logout a player of an account
////////////////////////////////////////////////////////////
uint32 AccountPlayerLogout(struct AccountData * Account)
{
	// todo

	// Check if user can exit
	// if ()
	{
	//	return PLAYER_LOG_OUT_ERROR_NONE;
	}
	//else
	{
		return PLAYER_LOG_OUT_ERROR_NOT_EXIT;
	}
}

////////////////////////////////////////////////////////////
///	Get data of current logged player
////////////////////////////////////////////////////////////
struct CharacterData * AccountGetLoggedPlayerData(struct AccountData * Account)
{
	return PlayerManagerGetByPosition(VectorAtT(Account->PlayersReference, Account->PlayerLogged, uinteger));
}
