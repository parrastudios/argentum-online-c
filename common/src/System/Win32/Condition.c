/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/Condition.h>
#include <System/ThreadLocal.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define SPINCOUNT_MUTEX_ADAPTIVE 200

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
static ThreadLocal	EventLocal = TLS_OUT_OF_INDEXES;
static uint32		ConditionRefCount = 0;

////////////////////////////////////////////////////////////
/// Initialize a condition variable
////////////////////////////////////////////////////////////
void ConditionInitialize(Condition * ConditionData)
{
#	if (_WIN32_WINNT < 0x0600)
		if (ConditionRefCount == 0)
		{
			ThreadLocalInitialize(&EventLocal);
		}

		ConditionRefCount++;

		// Condition variable implementation for Windows XP
		ConditionData->Count	= 0;
		ConditionData->Size		= 1 << 3;  // by default
		ConditionData->Handles	= (HANDLE*)MemoryAllocate(sizeof(HANDLE) * ConditionData->Size);

		MutexInitialize(&ConditionData->MutexData);

		SetCriticalSectionSpinCount(&ConditionData->MutexData, SPINCOUNT_MUTEX_ADAPTIVE);
#	else
		// Vista+
		InitializeConditionVariable(ConditionData);
#	endif
}

////////////////////////////////////////////////////////////
/// Destroy a condition variable
////////////////////////////////////////////////////////////
void ConditionDestroy(Condition * ConditionData)
{
#	if (_WIN32_WINNT < 0x0600)
		MutexDestroy(&ConditionData->MutexData);
		
		MemoryDeallocate(ConditionData->Handles);

		ConditionRefCount--;

		if (ConditionRefCount == 0)
		{
			ThreadLocalDestroy(&EventLocal);
		}
#	endif
}

////////////////////////////////////////////////////////////
/// Unblock a thread
////////////////////////////////////////////////////////////
void ConditionSignal(Condition * ConditionData)
{
#	if (_WIN32_WINNT < 0x0600)
		MutexLock(&ConditionData->MutexData);

		if (ConditionData->Count > 0)
		{
			SetEvent(ConditionData->Handles[0]);
			MemoryMove(&ConditionData->Handles[0], &ConditionData->Handles[1], --ConditionData->Count);
		}

		MutexUnlock(&ConditionData->MutexData);
#	else
		WakeConditionVariable(ConditionData);
#	endif
}

////////////////////////////////////////////////////////////
/// Unblock all threads
////////////////////////////////////////////////////////////
void ConditionBroadcast(Condition * ConditionData)
{
#	if (_WIN32_WINNT < 0x0600)
		uint32 i;
	
		MutexLock(&ConditionData->MutexData);

		for (i = 0; i < ConditionData->Count; i++)
		{
			SetEvent(ConditionData->Handles[i]);
		}

		ConditionData->Count = 0;

		MutexUnlock(&ConditionData->MutexData);
#	else
		WakeAllConditionVariable(ConditionData);
#	endif
}

////////////////////////////////////////////////////////////
/// Block on a condition variable
////////////////////////////////////////////////////////////
void ConditionWait(Condition * ConditionData, Mutex * Spinlock)
{
#	if (_WIN32_WINNT < 0x0600)
		DWORD Status;
		HANDLE EventHandle = ThreadLocalGetValue(&EventLocal);

		if (!EventHandle)
		{
			EventHandle = CreateEvent(0, FALSE, FALSE, NULL);
			ThreadLocalSetValue(&EventLocal, EventHandle);
		}

		MutexLock(&ConditionData->MutexData);

		WaitForSingleObject(EventHandle, 0);

		if ((ConditionData->Count + 1) > ConditionData->Size)
		{
			ConditionData->Size <<= 1;
			ConditionData->Handles = MemoryReallocate(ConditionData->Handles, ConditionData->Size);
		}

		ConditionData->Handles[ConditionData->Count++] = EventHandle;

		MutexUnlock(&ConditionData->MutexData);

		MutexLock(Spinlock);
		
		Status = WaitForSingleObject(EventHandle, INFINITE);

		MutexUnlock(Spinlock);

		if (Status == WAIT_TIMEOUT)
		{
			uint32 i;

			MutexLock(&ConditionData->MutexData);
			
			for (i = 0; i < ConditionData->Count; i++)
			{
				if (ConditionData->Handles[i] == EventHandle)
				{
					if (i != ConditionData->Count - 1)
					{
						MemoryMove(&ConditionData->Handles[i],
								&ConditionData->Handles[i + 1],
								(ConditionData->Count - i - 1) * sizeof(HANDLE));
					}

					ConditionData->Count--;
				}
			}

			Status = WaitForSingleObject(EventHandle, 0);

			MutexUnlock(&ConditionData->MutexData);
		}
#	else
		SleepConditionVariableCS(ConditionData, Spinlock, INFINITE);
#	endif
}

////////////////////////////////////////////////////////////
/// Block on a condition variable (with timeout : milliseconds)
////////////////////////////////////////////////////////////
void ConditionTimedWait(Condition * ConditionData, Mutex * Spinlock, float TimeOut)
{
// todo

#	if (_WIN32_WINNT < 0x0600)
		
#	else
		// todo: proper float to DWORD conversion
		//SleepConditionVariableCS(ConditionData, Spinlock, (DWORD)TimeOut);
#	endif
}
