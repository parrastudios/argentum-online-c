/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

#ifndef GAME_APPLICATION_H
#define GAME_APPLICATION_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Config.h>
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define GAME_INITIALIZE		0
#define GAME_INTRO			1
#define GAME_LOGIN			2
#define GAME_ACCOUNT		3
#define GAME_RUN			4
#define GAME_DESTROY		5

////////////////////////////////////////////////////////////
/// Initialize the application
///
////////////////////////////////////////////////////////////
bool GameInitialize();

////////////////////////////////////////////////////////////
/// Update the events of the application
///
////////////////////////////////////////////////////////////
void GameEvents();

////////////////////////////////////////////////////////////
/// Run the application
///
////////////////////////////////////////////////////////////
void GameRun();

////////////////////////////////////////////////////////////
/// Destroy and cleanup the application
///
////////////////////////////////////////////////////////////
bool GameDestroy();

////////////////////////////////////////////////////////////
/// Set up the status of the game
///
////////////////////////////////////////////////////////////
void GameSetStatus(uint32 State);

////////////////////////////////////////////////////////////
/// Get the status of the game
///
////////////////////////////////////////////////////////////
void GameGetStatus(uint32 * State);

#endif // GAME_APPLICATION_H
