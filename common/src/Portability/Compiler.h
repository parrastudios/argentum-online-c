/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PORTABILITY_COMPILER_H
#define PORTABILITY_COMPILER_H

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define COMPILER_UNKNOWN			0x00
#define COMPILER_GCCE				0x01
#define COMPILER_WINSCW				0x02
#define COMPILER_MSVC				0x03
#define COMPILER_MSQC				0x04
#define COMPILER_MINGW				0x05
#define COMPILER_BORLAND			0x06
#define COMPILER_GNUC				0x07
#define COMPILER_WATCOM				0x08
#define COMPILER_POWERC				0x09
#define COMPILER_COMEAU				0x0A
#define COMPILER_CRAY				0x0B
#define COMPILER_CYGWIN				0x0C
#define COMPILER_COMPAQ				0x0D
#define COMPILER_DIAB				0x0E
#define COMPILER_DIGMARS			0x0F
#define COMPILER_HPCC				0x10
#define COMPILER_INTEL				0x11
#define COMPILER_KAY				0x12
#define COMPILER_SGI				0x13
#define COMPILER_MPW				0x14
#define COMPILER_MWERKS				0x15
#define COMPILER_NORCROFT			0x16
#define COMPILER_SCO				0x17
#define COMPILER_SUNPRO				0x18
#define COMPILER_TENDRA				0x19
#define COMPILER_USLC				0x1A
#define COMPILER_XLC				0x1B
#define COMPILER_EDG				0x1C
#define COMPILER_CLANG				0x1D

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Macro for building the compiler version
////////////////////////////////////////////////////////////
#define CompilerVersionBuild(Major, Minor, Micro) (((Major) << 24) + ((Minor) << 16) + (Micro))

////////////////////////////////////////////////////////////
// Detection
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Identify the compiler
////////////////////////////////////////////////////////////
#if defined(__GCCE__)
#	define COMPILER_TYPE COMPILER_GCCE
#	define COMPILER_VERSION _MSC_VER
#	include <staticlibinit_gcce.h>
#elif defined(__WINSCW__)
#	define COMPILER_TYPE COMPILER_WINSCW
#	define COMPILER_VERSION _MSC_VER
#	pragma warning (disable: 4996)
#elif defined(_QC)
#	define COMPILER_TYPE COMPILER_MSQC
#	define COMPILER_VERSION _QC
#elif defined(_MSC_VER)
#	define COMPILER_TYPE COMPILER_MSVC
#	if defined(_MSC_FULL_VER)
#		define COMPILER_VERSION CompilerVersionBuild(_MSC_FULL_VER / 1000000, (_MSC_FULL_VER % 1000000) / 10000, _MSC_FULL_VER % 10000)
#	else
#		define COMPILER_VERSION CompilerVersionBuild(_MSC_VER / 100, _MSC_VER % 100, 0)
#	endif
#elif defined(__MINGW32__)
#	define COMPILER_TYPE COMPILER_MINGW
#	define COMPILER_VERSION __MINGW32__
#elif defined (__GNUC__)
#	define COMPILER_TYPE COMPILER_GNUC
#	if defined(__GNUC_PATCHLEVEL__)
#		define COMPILER_VERSION CompilerVersionBuild(__GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__)
#	else
#		define COMPILER_VERSION CompilerVersionBuild(__GNUC__, __GNUC_MINOR__, 0)
#	endif
#elif defined(__BORLANDC__)
#	define COMPILER_TYPE COMPILER_BORLAND
#	define COMPILER_VERSION __BCPLUSPLUS__
#	define __FUNCTION__ __FUNC__
#elif defined(__WATCOMC__)
#	define COMPILER_TYPE COMPILER_WATCOM
#	define COMPILER_VERSION __WATCOMC__
#elif defined(__POWERC)
#	define COMPILER_TYPE COMPILER_POWERC
#	define COMPILER_VERSION __POWERC
#elif defined(__COMO__)
#	define COMPILER_TYPE COMPILER_COMEAU
#	define COMPILER_VERSION __COMO_VERSION__
#elif defined(_CRAYC)
#	define COMPILER_TYPE COMPILER_CRAY
#	define COMPILER_VERSION _REVISION
#elif defined(__CYGWIN__)
#	define COMPILER_TYPE COMPILER_CYGWIN
#	define COMPILER_VERSION 0
#elif (defined(__DECC) || defined(__DECCXX)) || (defined(VAXC) || defined(__VAXC)) || (defined(__osf__) && defined(__LANGUAGE_C__) && !defined(__GNUC__))
#	define COMPILER_TYPE COMPILER_COMPAQ
#	if defined(__DECC_VER)
#		define COMPILER_VERSION __DECC_VER
#	else
#		define COMPILER_VERSION 0
#	endif
#elif ((defined(__DCC__) && defined(__VERSION_NUMBER__)))
#	define COMPILER_TYPE COMPILER_DIAB
#	define COMPILER_VERSION __VERSION_NUMBER__
#elif (defined(__DMC__) || (defined(__SC__) || defined(__ZTC__)))
#	define COMPILER_TYPE COMPILER_DIGMARS
#	if defined(__DMC__)
#		define COMPILER_VERSION __DMC__
#	else
#		define COMPILER_VERSION 0
#	endif
#elif (defined(__HP_aCC) || defined(__HP_cc) || ((__cplusplus - 0 >= 199707L) && defined(__hpux)))
#	define COMPILER_TYPE COMPILER_HPCC
#	if defined(__HP_aCC)
#		if (__HP_aCC == 1)
#			define COMPILER_VERSION 11500
#		else
#			define COMPILER_VERSION __HP_aCC
#		endif
#	elif defined(__HP_cc)
#		define COMPILER_VERSION __HP_cc
#	else
#		define COMPILER_VERSION 0
#	endif
#elif (defined(__INTEL_COMPILER) || defined(__ICC))
#	define COMPILER_TYPE COMPILER_INTEL
#	if defined(__INTEL_COMPILER)
#		define COMPILER_VERSION __INTEL_COMPILER
#	else
#		define COMPILER_VERSION 0
#	endif
#elif defined(__KCC)
#	define COMPILER_TYPE COMPILER_KAY
#	define COMPILER_VERSION __KCC_VERSION
#elif ((defined(sgi) || defined(__sgi)) && defined(_COMPILER_VERSION))
#	define COMPILER_TYPE COMPILER_SGI
#	define COMPILER_VERSION _COMPILER_VERSION
#elif (defined(__MRC__) || defined(MPW_C) || defined(MPW_CPLUS))
#	define COMPILER_TYPE COMPILER_MPW
#	if defined(__MRC__)
#		define COMPILER_VERSION __MRC__
#	else
#		define COMPILER_VERSION 0
#	endif
#elif defined(__MWERKS__)
#	define COMPILER_TYPE COMPILER_MWERKS
#	define COMPILER_VERSION __MWERKS__
#elif defined(__CC_NORCROFT)
#	define COMPILER_TYPE COMPILER_NORCROFT
#	define COMPILER_VERSION 0 // __ARMCC_VERSION is a float
#elif defined(_SCO_DS)
#	define COMPILER_TYPE COMPILER_SCO
#	define COMPILER_VERSION 0
#elif (defined(__SUNPRO_C) || defined(__SUNPRO_CC))
#	define COMPILER_TYPE COMPILER_SUNPRO
#	if defined(__SUNPRO_C)
#		define COMPILER_VERSION __SUNPRO_C
#	elif defined(__SUNPRO_CC)
#		define COMPILER_VERSION __SUNPRO_CC
#	else
#		define COMPILER_VERSION 0
#	endif
#elif defined(__TenDRA__)
#	define COMPILER_TYPE COMPILER_TENDRA
#	define COMPILER_VERSION 0
#elif defined(__USLC__)
#	define COMPILER_TYPE COMPILER_USLC
#	if defined(__SCO_VERSION__)
#		define COMPILER_VERSION __SCO_VERSION__
#	else
#		define COMPILER_VERSION 0
#	endif
#elif (defined(__xlC__) || (defined(__IBMC__) || defined(__IBMCPP__)) || (defined(_AIX) && !defined(__GNUC__)))
#	define COMPILER_TYPE COMPILER_XLC
#	if defined(__xlC__)
#		define COMPILER_VERSION __xlC__
#	else
#		define COMPILER_VERSION 0
#	endif
#elif (defined(__EDG__) || defined(__EDG_VERSION__))
#	if (defined(_MSC_VER) && (defined(__INTELLISENSE__) || __EDG_VERSION__ >= 308))
#		define COMPILER_TYPE COMPILER_MSVC
#		if defined(_MSC_FULL_VER)
#			define COMPILER_VERSION CompilerVersionBuild(_MSC_FULL_VER / 1000000, (_MSC_FULL_VER % 1000000) / 10000, _MSC_FULL_VER % 10000)
#		else
#			define COMPILER_VERSION CompilerVersionBuild(_MSC_VER / 100, _MSC_VER % 100, 0)
#		endif
#	else
#		define COMPILER_TYPE COMPILER_EDG
#		define COMPILER_VERSION CompilerVersionBuild(__EDG_VERSION__ / 100, __EDG_VERSION__ % 100, 0)
#	endif
#elif defined(__clang__)
#	define COMPILER_TYPE COMPILER_CLANG
#	if (defined(__clang_major__) && defined(__clang_minor__) && defined(__clang_patchlevel__))
#		define COMPILER_VERSION CompilerVersionBuild(__clang_major__, __clang_minor__, __clang_patchlevel__)
#	elif (defined(__clang_major__) && defined(__clang_minor__))
#		define COMPILER_VERSION CompilerVersionBuild(__clang_major__, __clang_minor__, 0)
#	elif defined(__apple_build_version__)
#		if __apple_build_version__ >= 6000051
#			define COMPILER_VERSION CompilerVersionBuild(3, 0, 5)
#		elif __apple_build_version__ >= 5030038
#			define COMPILER_VERSION CompilerVersionBuild(3, 0, 4)
#		elif __apple_build_version__ >= 5000275
#			define COMPILER_VERSION CompilerVersionBuild(3, 0, 3)
#		elif __apple_build_version__ >= 4250024
#			define COMPILER_VERSION CompilerVersionBuild(3, 0, 2)
#		elif __apple_build_version__ >= 3180045
#			define COMPILER_VERSION CompilerVersionBuild(3, 0, 1)
#		elif __apple_build_version__ >= 2111001
#			define COMPILER_VERSION CompilerVersionBuild(3, 0, 0)
#		else
#			define COMPILER_VERSION 0
#		endif
#	else
#		define COMPILER_VERSION 0
#	endif
#else
#	error This compiler is not supported
#endif

#endif // PORTABILITY_COMPILER_H
