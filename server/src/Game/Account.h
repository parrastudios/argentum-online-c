/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

#ifndef GAME_ACCOUNT_H
#define GAME_ACCOUNT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/Vector.h>
#include <Game/AccountDef.h>

////////////////////////////////////////////////////////////
// Member Data
////////////////////////////////////////////////////////////
struct AccountData
{
	uint64		Id;									///< Account id
	char		Name[ACCOUNT_NAME_MAX_SIZE];		///< Account name
	char		Password[ACCOUNT_PASS_MAX_SIZE];	///< Account password
	char		Email[ACCOUNT_MAIL_MAX_SIZE];		///< Account email
	bool		Banned;								///< Account ban status
	bool		Logged;								///< Check if account is logged in
	bool		Active;								///< Check if account is activated
	uinteger	PlayerLogged;						///< Current player logged in
	Vector		PlayersReference;					///< Array of references to players in the manager
};

////////////////////////////////////////////////////////////
///	Initialize account data
////////////////////////////////////////////////////////////
void AccountInitialize(struct AccountData * Account);

////////////////////////////////////////////////////////////
///	Check if the password is correct
////////////////////////////////////////////////////////////
bool AccountCheckPassword(struct AccountData * Account, char * Password);

////////////////////////////////////////////////////////////
///	Check if the email is correct
////////////////////////////////////////////////////////////
bool AccountCheckEmail(struct AccountData * Account, char * Email);

////////////////////////////////////////////////////////////
///	Check if account is banned
////////////////////////////////////////////////////////////
bool AccountCheckBan(struct AccountData * Account);

////////////////////////////////////////////////////////////
///	Check if account is logged
////////////////////////////////////////////////////////////
bool AccountCheckLogged(struct AccountData * Account);

////////////////////////////////////////////////////////////
///	Login account
////////////////////////////////////////////////////////////
uint32 AccountLogin(struct AccountData * Account, char * Email, char * Password);

////////////////////////////////////////////////////////////
///	Logout an account
////////////////////////////////////////////////////////////
bool AccountLogout(struct AccountData * Account);

////////////////////////////////////////////////////////////
///	Login a player of an account
////////////////////////////////////////////////////////////
uint32 AccountPlayerLogin(struct AccountData * Account, uint32 Index);

////////////////////////////////////////////////////////////
/// Get player info list of an account
////////////////////////////////////////////////////////////
void AccountGetPlayersInfo(struct AccountData * Account, struct AccountPlayerInfo * PlayerList, uint32 * Count);

////////////////////////////////////////////////////////////
///	Logout a player of an account
////////////////////////////////////////////////////////////
uint32 AccountPlayerLogout(struct AccountData * Account);

////////////////////////////////////////////////////////////
///	Get data of current logged player
////////////////////////////////////////////////////////////
struct CharacterData * AccountGetLoggedPlayerData(struct AccountData * Account);

#endif // GAME_ACCOUNT_H
