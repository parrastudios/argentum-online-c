/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <GUI/WindowParser.h>
#include <GUI/Widget.h>
#include <GUI/WindowManager.h>

#include <DataType/String/Lexer.h>
#include <System/IOHelper.h>
#include <System/Error.h>

#include <Window/Window.h>

#include <stdlib.h> // atoi

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define GUI_PARSER_STATUS_ERROR				0x00	///< Parser error
#define GUI_PARSER_STATUS_EOF				0x01	///< The parser finished
#define GUI_PARSER_STATUS_PROCESSING		0x02	///< The parser is creating and adding the widget
#define GUI_PARSER_STATUS_READING_WIDGET	0x03	///< The parser is reading the widget type
#define GUI_PARSER_STATUS_READING_PARAMS	0x04	///< The parser is reading a widget params
#define GUI_PARSER_STATUS_WAITING			0x05	///< The parser is waiting for new data

#define GUI_PARSER_PROCESSING_ERROR			0x00	///< Processing error
#define GUI_PARSER_PROCESSING_SIMPLE		0x01	///< Processing a simple tag
#define GUI_PARSER_PROCESSING_OPENING		0x02	///< Processing an opening tag
#define GUI_PARSER_PROCESSING_CLOSING		0x03	///< Processing a closing tag

#define GUI_PARSER_PREFIX_SIZE              0x03

#define GUI_PARSER_MAP_LANG_NAME			"MSG"
#define GUI_PARSER_MAP_CONFIG_NAME			"CFG"

static struct WindowParserMapTagType
	WindowParserMapTags[GUI_PARSER_MAP_COUNT] =
	{
		{ GUI_PARSER_MAP_LANG_NAME },
		{ GUI_PARSER_MAP_CONFIG_NAME }
	};

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct GuiWindow * ParserWindow	= NULL;							///< Parser window
struct GuiWidget * ParserWidget	= NULL;							///< Widget parsed
uinteger ParserStatus			= GUI_PARSER_STATUS_WAITING;	///< Parser status indicator
uinteger ProcessingStatus		= GUI_PARSER_PROCESSING_ERROR;	///< Processing status indicator
uinteger WidgetType				= GUI_WIDGET_ERROR;				///< Type of the widget parsed
uinteger ParameterType			= GUI_PARAMETER_ERROR;			///< Type of the parameter parsed

struct WindowParserMapType WindowParserMap[GUI_PARSER_MAP_COUNT];

////////////////////////////////////////////////////////////
// Internal functions
////////////////////////////////////////////////////////////

static bool Lower(char * String, uinteger Len)
{
	uinteger i;

	for (i = 0; i < Len; i++)
	{
		if (!tolower(String[i]))
		{
			return false;
		}
	}

	return true;
}

static bool IsAlpha(char * String, uinteger Len)
{
	uinteger i;

	for (i = 0; i < Len; i++)
	{
		if (!isalpha(String[i]))
		{
			return false;
		}
	}

	return true;
}

static bool GetBool(char * str)
{
	if (strcmp(str, "true") == 0)
		return true;

	return false;
}

static bool CheckIntegrity(struct Lexer * Input)
{
	if (LexerOff(Input))
	{
		ParserStatus = GUI_PARSER_STATUS_EOF;

		return false;
	}
	else if (blength(LexerItemGet(Input)) == 0)
	{
		ParserStatus = GUI_PARSER_STATUS_ERROR;

		MessageError("GuiParserRead", "Invalid syntax (character: %d)", Input->Current);

		bdestroy(LexerItemGet(Input));

		return false;
	}

	return true;
}

static uinteger GuiParserWidgetType(char * Type)
{
    if		(strcmp(Type, "form")		== 0)	return GUI_WIDGET_FORM;
    else if (strcmp(Type, "button")		== 0)	return GUI_WIDGET_BUTTON;
    else if (strcmp(Type, "textbox")	== 0)	return GUI_WIDGET_TEXTBOX;
	else if (strcmp(Type, "console")	== 0)	return GUI_WIDGET_CONSOLE;
	else if (strcmp(Type, "scroll")		== 0)	return GUI_WIDGET_SCROLL;
    else if (strcmp(Type, "label")		== 0)	return GUI_WIDGET_LABEL;
    else if (strcmp(Type, "list")		== 0)	return GUI_WIDGET_LIST;
    else if (strcmp(Type, "image")		== 0)	return GUI_WIDGET_IMAGE;
    else if (strcmp(Type, "shape")		== 0)	return GUI_WIDGET_SHAPE;
    else if (strcmp(Type, "table")		== 0)	return GUI_WIDGET_TABLE;

	return GUI_WIDGET_ERROR;
}

static uinteger GuiParserParamType(char * Param)
{
    if		(strcmp(Param, "name") == 0)		return GUI_PARAMETER_NAME;
    else if (strcmp(Param, "text") == 0)		return GUI_PARAMETER_TEXT;
    else if (strcmp(Param, "style") == 0)		return GUI_PARAMETER_STYLE;
    else if (strcmp(Param, "x") == 0)			return GUI_PARAMETER_X;
    else if (strcmp(Param, "y") == 0)			return GUI_PARAMETER_Y;
    else if (strcmp(Param, "height") == 0)		return GUI_PARAMETER_HEIGHT;
    else if (strcmp(Param, "width") == 0)		return GUI_PARAMETER_WIDTH;
    else if (strcmp(Param, "fontsize") == 0)	return GUI_PARAMETER_FONTSIZE;
	else if (strcmp(Param, "length") == 0)		return GUI_PARAMETER_LENGTH;
	else if (strcmp(Param, "hide") == 0)		return GUI_PARAMETER_HIDE;
    else if (strcmp(Param, "event") == 0)		return GUI_PARAMETER_EVENT;
    else if (strcmp(Param, "enabled") == 0)		return GUI_PARAMETER_ENABLED;
	else if (strcmp(Param, "visible") == 0)		return GUI_PARAMETER_VISIBLE;
	else if (strcmp(Param, "border") == 0)		return GUI_PARAMETER_BORDER;

	return GUI_PARAMETER_ERROR;
}

static void GuiParserCreateWidget()
{
	switch (WidgetType)
	{
		case GUI_WIDGET_FORM :
		{
			ParserWidget = GuiFormCreateDefault();
			break;
		}

		case GUI_WIDGET_BUTTON :
		{
			ParserWidget = GuiButtonCreateDefault();
			break;
		}

		case GUI_WIDGET_TEXTBOX :
		{
			ParserWidget = GuiTextBoxCreateDefault();
			break;
		}


		case GUI_WIDGET_CONSOLE :
		{
			ParserWidget = GuiConsoleCreateDefault();
			break;
		}

		case GUI_WIDGET_SCROLL :
		{
			// ParserWidget = GuiScrollCreateDefault();
			break;
		}

		case GUI_WIDGET_LABEL :
		{
			ParserWidget = GuiLabelCreateDefault();
			break;
		}

		case GUI_WIDGET_LIST :
		{
			ParserWidget = GuiListCreateDefault();
			break;
		}

		case GUI_WIDGET_IMAGE :
		{
			// ParserWidget = GuiImageCreateDefault();
			break;
		}

		case GUI_WIDGET_SHAPE :
		{
			ParserWidget = GuiShapeCreateDefault();
			break;
		}

		case GUI_WIDGET_TABLE :
		{
			ParserWidget = GuiTableCreateDefault();
			break;
		}
	}
}

static bool GuiParserEvent(char * Value)
{
	// Get the event pointer
	struct EventPtr * Event = GuiEventHandlerGet(Value);

	if (Event == NULL)
		return false;

	// Get the event type
	switch (GuiEventHandlerGetType(Value))
	{
		case GUI_EVENT_KEY_PRESSED :
		{
			ParserWidget->KeyPressed = Event->Ptr.KeyPressed;
			break;
		}

		case GUI_EVENT_KEY_RELEASED :
		{
			ParserWidget->KeyReleased = Event->Ptr.KeyReleased;
			break;
		}

		case GUI_EVENT_TEXT_ENTERED :
		{
			ParserWidget->TextEntered = Event->Ptr.TextEntered;
			break;
		}

		case GUI_EVENT_BUTTON_PRESSED :
		{
			ParserWidget->ButtonPressed = Event->Ptr.ButtonPressed;
			break;
		}

		case GUI_EVENT_BUTTON_RELEASED :
		{
			ParserWidget->ButtonReleased = Event->Ptr.ButtonReleased;
			break;
		}

		case GUI_EVENT_MOUSE_MOVE :
		{
			ParserWidget->MouseMove = Event->Ptr.MouseMove;
			break;
		}

		case GUI_EVENT_OPEN :
		{
			ParserWidget->Open = Event->Ptr.Open;
			break;
		}

		case GUI_EVENT_CLOSE :
		{
			ParserWidget->Close = Event->Ptr.Close;
			break;
		}

		case GUI_EVENT_ERROR :
			return false;

		default :
			return false;
	}

	return true;
}

static bool GuiParserValue(char * Value, uinteger Length)
{
	switch (ParameterType)
	{
		case GUI_PARAMETER_NAME :
		{
			strncpy(ParserWidget->Name, Value, Length + 1);
			break;
		}

		case GUI_PARAMETER_TEXT :
		{
		    bool Found = false;

			if (Length > GUI_PARSER_PREFIX_SIZE)
			{
				uinteger i;

				char Prefix[GUI_PARSER_PREFIX_SIZE + 1];

				strncpy(&Prefix[0], &Value[0], GUI_PARSER_PREFIX_SIZE);

				Prefix[GUI_PARSER_PREFIX_SIZE] = NULLCHAR;

				for (i = 0; i < GUI_PARSER_MAP_COUNT && Found == false; ++i)
				{
					if (WindowParserMap[i].MapFunc)
					{
						if (strncmp(&Prefix[0], WindowParserMapTags[i].Name, GUI_PARSER_PREFIX_SIZE) == 0)
						{
							char Key[0xFF];

							// Read key associated to this map (prefix)
							uinteger KeyLength = snreadfield(Key, 0xFF, Value, Length, '_', 1);

							// Obtain the map value
							strcpy(ParserWidget->Text, WindowParserMap[i].MapFunc(Key));

							Found = true;
						}
					}
				}
			}

            if (!Found)
            {
                strncpy(ParserWidget->Text, Value, Length);
            }

			break;
		}

		case GUI_PARAMETER_STYLE :
		{
		    if (ParserWidget->Type == GUI_WIDGET_FORM)
            {
                ParserWidget->Attributes.Form.Background = atoi(Value);
            }
			else if (ParserWidget->Type == GUI_WIDGET_SHAPE)
            {
				GuiShapeSetStyleType(&ParserWidget->Attributes.Shape, atoi(Value));
            }
			break;
		}

		case GUI_PARAMETER_X :
		{
			ParserWidget->X = atoi(Value);
			break;
		}

		case GUI_PARAMETER_Y :
		{
			ParserWidget->Y = atoi(Value);
			break;
		}

		case GUI_PARAMETER_HEIGHT :
		{
			ParserWidget->Height = atoi(Value);
			break;
		}

		case GUI_PARAMETER_WIDTH :
		{
			ParserWidget->Width = atoi(Value);
			break;
		}

		case GUI_PARAMETER_FONTSIZE :
		{
			ParserWidget->FontSize = atoi(Value);
			break;
		}

		case GUI_PARAMETER_LENGTH :
		{
			ParserWidget->Length = atoi(Value);
			break;
		}

		case GUI_PARAMETER_HIDE :
		{
			ParserWidget->Attributes.TextBox.HideText = GetBool(Value);
			break;
		}

		case GUI_PARAMETER_EVENT :
		{
			if (!GuiParserEvent(Value))
				return false;
			break;
		}

		case GUI_PARAMETER_ENABLED :
		{
			ParserWidget->Enabled = GetBool(Value);
			break;
		}

		case GUI_PARAMETER_VISIBLE :
		{
			ParserWidget->Visible = GetBool(Value);
			break;
		}

		case GUI_PARAMETER_BORDER :
		{
			ParserWidget->Border = GetBool(Value);
			break;
		}

		default :
			return false;
	}

	return true;
}

////////////////////////////////////////////////////////////
/// Parse each widget (return false on parsing error)
////////////////////////////////////////////////////////////
static bool GuiParserRead(struct Lexer * Input)
{
	while (ParserStatus != GUI_PARSER_STATUS_ERROR && ParserStatus != GUI_PARSER_STATUS_EOF)
	{
		// Skip the blank chars
		LexerSkip(Input, LEXER_BLANK);

		if (LexerOff(Input))
		{
			ParserStatus = GUI_PARSER_STATUS_EOF;
		}
		else
		{
			switch (ParserStatus)
			{
				case GUI_PARSER_STATUS_WAITING :
				{
					char Character;

					// Search for a next tag, comment or text
					LexerSkipTo(Input, GUI_PARSER_WIDGET_START GUI_PARSER_COMMENT LEXER_LOWER);

					// Get the value
					LexerNextChar(Input, GUI_PARSER_WIDGET_START GUI_PARSER_COMMENT LEXER_LOWER);

					if (!CheckIntegrity(Input))
						break;

					Character = LexerItemGet(Input)->data[0];

					bdestroy(LexerItemGet(Input));

					switch (Character)
					{
						case '<' :
						{
							ParserStatus = GUI_PARSER_STATUS_READING_WIDGET;
							break;
						}

						case ';' :
						{
							// Comment found, skip to the next line
							uinteger Result = LexerNextLine(Input);

							ParserStatus = GUI_PARSER_STATUS_WAITING;

							if (Result == LEXER_EOF)
							{
								ParserStatus = GUI_PARSER_STATUS_EOF;
							}

							break;
						}

						default :
						{
							ParserStatus = GUI_PARSER_STATUS_ERROR;
							break;
						}
					}

					break;
				}

				case GUI_PARSER_STATUS_READING_WIDGET :
				{
					// Two possible cases, it's a simple widget or a closing tag
					LexerNextTo(Input, LEXER_BLANK GUI_PARSER_WIDGET_CLOSE);

					if (!CheckIntegrity(Input))
						break;

					if (LexerItemGet(Input)->data[0] == '/')
					{
						// Closing tag found
						WidgetType = GuiParserWidgetType(&LexerItemGet(Input)->data[1]);

						if (WidgetType == GUI_WIDGET_ERROR)
						{
							ParserStatus = GUI_PARSER_STATUS_ERROR;

							MessageError("GuiParserRead", "Invalid syntax in a widget type (closing) tag (character: %d)", Input->Current);
						}
						else
						{
							// Process it
							ParserStatus = GUI_PARSER_STATUS_PROCESSING;
							ProcessingStatus = GUI_PARSER_PROCESSING_CLOSING;
						}
					}
					else if (Lower(LexerItemGet(Input)->data, blength(LexerItemGet(Input))))
					{
						WidgetType = GuiParserWidgetType(LexerItemGet(Input)->data);

						if (WidgetType == GUI_WIDGET_ERROR)
						{
							ParserStatus = GUI_PARSER_STATUS_ERROR;

							MessageError("GuiParserRead", "Invalid widget type (character: %d)", Input->Current);
						}
						else
						{
							// Create a specific type widget
							GuiParserCreateWidget();

							// A simple widget, read it
							ParserStatus = GUI_PARSER_STATUS_READING_PARAMS;
						}
					}
					else
					{
						ParserStatus = GUI_PARSER_STATUS_ERROR;

						MessageError("GuiParserRead", "Invalid syntax in a widget type tag (character: %d)", Input->Current);
					}

					bdestroy(LexerItemGet(Input));

					break;
				}

				case GUI_PARSER_STATUS_READING_PARAMS :
				{
					char Character;

					// Go to the value
					LexerSkipTo(Input, LEXER_ALPHANUMERIC GUI_PARSER_WIDGET_FINISH GUI_PARSER_WIDGET_CLOSE);

					// It can be a param, a simple widget or an opening widget tag
					LexerNextTo(Input, GUI_PARSER_EQUAL GUI_PARSER_WIDGET_FINISH GUI_PARSER_WIDGET_CLOSE GUI_PARSER_COMMENT);

					if (LexerOff(Input))
					{
						ParserStatus = GUI_PARSER_STATUS_EOF;
						break;
					}
					else if (blength(LexerItemGet(Input)) == 0)
					{
						// Check the result
						LexerNextChar(Input, GUI_PARSER_EQUAL GUI_PARSER_WIDGET_FINISH GUI_PARSER_WIDGET_CLOSE GUI_PARSER_COMMENT);

						Character = LexerItemGet(Input)->data[0];

						bdestroy(LexerItemGet(Input));

						switch (Character)
						{
							case '/' :
							{
								// Process the widget (closing tag in the same line)
								ParserStatus = GUI_PARSER_STATUS_PROCESSING;
								ProcessingStatus = GUI_PARSER_PROCESSING_SIMPLE;
								break;
							}

							case '>' :
							{
								// Process the widget (opening tag)
								ParserStatus = GUI_PARSER_STATUS_PROCESSING;
								ProcessingStatus = GUI_PARSER_PROCESSING_OPENING;
								break;
							}

							case ';' :
							{
								if (LexerNextLine(Input) == LEXER_EOF)
								{
									ParserStatus = GUI_PARSER_STATUS_EOF;
								}

								break;
							}

							default :
							{
								ParserStatus = GUI_PARSER_STATUS_ERROR;
								break;
							}
						}
					}
					else
					{
						bstring Result = bstrcpy(LexerItemGet(Input));

						bdestroy(LexerItemGet(Input));

						// Check the result
						LexerNextChar(Input, GUI_PARSER_EQUAL GUI_PARSER_WIDGET_FINISH GUI_PARSER_COMMENT);

						Character = LexerItemGet(Input)->data[0];

						bdestroy(LexerItemGet(Input));

						switch (Character)
						{
							case '=' :
							{
								if (Lower(Result->data, blength(Result)))
								{
									ParameterType = GuiParserParamType(Result->data);
								}
								else
								{
									ParameterType = GUI_PARAMETER_ERROR;
								}

								break;
							}

							default :
							{
								ParserStatus = GUI_PARSER_STATUS_ERROR;
								break;
							}
						}

						bdestroy(Result);

						// Read the value
						if (ParameterType == GUI_PARAMETER_ERROR)
						{
							ParserStatus = GUI_PARSER_STATUS_ERROR;

							MessageError("GuiParserRead", "Invalid syntax in a widget parameter tag (character: %d)", Input->Current);
						}
						else
						{
							// Go to the value
							LexerSkipTo(Input, LEXER_ALPHANUMERIC);

							// Get the value
							LexerNextTo(Input, GUI_PARSER_VALUE_TAG);

							if (CheckIntegrity(Input))
                            {
                                char * Data = LexerItemGet(Input)->data;
								uinteger Length = blength(LexerItemGet(Input));

                                // Add the value to the widget object
                                if (!GuiParserValue(Data, Length))
                                {
                                    ParserStatus = GUI_PARSER_STATUS_ERROR;

                                    MessageError("GuiParserRead", "Invalid parameter type (character: %d, data: %s)", Input->Current, Data);
                                }

                                bdestroy(LexerItemGet(Input));
                            }
						}
					}

					break;
				}

				case GUI_PARSER_STATUS_PROCESSING :
				{
					// Process the data
					switch (ProcessingStatus)
					{
						case GUI_PARSER_PROCESSING_SIMPLE :
						{
							// Create the window in the case of form
							if (WidgetType == GUI_WIDGET_FORM)
							{
								ParserWindow = GuiWindowCreate();

								if (ParserWidget->X == 16)
									ParserWidget->X = (WindowWidth / 2) - ((ParserWidget->Width + 16) / 2);

								if (ParserWidget->Y == 16)
									ParserWidget->Y = (WindowHeight / 2) - ((ParserWidget->Height + 16) / 2);
							}
							else if (WidgetType == GUI_WIDGET_TEXTBOX)
							{
								// Set the size
								if (ParserWidget->Length == 0)
									ParserWidget->Length = 30;

								if (ParserWidget->Width == 0)
									ParserWidget->Width = (ParserWidget->FontSize * ParserWidget->Length);

								if (ParserWidget->Height == 0)
									ParserWidget->Height = (ParserWidget->FontSize + 2);

								ParserWidget->Length = strlen(ParserWidget->Text);

								ParserWidget->Attributes.TextBox.CurrentPos = ParserWidget->Length;
							}

							// Add the widget
							GuiWindowAddWidget(ParserWindow, ParserWidget);

							break;
						}

						case GUI_PARSER_PROCESSING_CLOSING :
						{
							switch (WidgetType)
							{
								case GUI_WIDGET_FORM :
								{
									GuiLoadWindow(ParserWindow);
									break;
								}
							}
							break;
						}

						case GUI_PARSER_PROCESSING_OPENING :
						{
							switch (WidgetType)
							{
								case GUI_WIDGET_FORM :
								{
									// Create the window
									ParserWindow = GuiWindowCreate();

									if (ParserWidget->X == 16)
										ParserWidget->X = (WindowWidth / 2) - ((ParserWidget->Width + 16) / 2);

									if (ParserWidget->Y == 16)
										ParserWidget->Y = (WindowHeight / 2) - ((ParserWidget->Height + 16) / 2);

									// Add the widget
									GuiWindowAddWidget(ParserWindow, ParserWidget);
									break;
								}
							}

							break;
						}

						case GUI_PARSER_PROCESSING_ERROR :
						{
							ParserStatus = GUI_PARSER_STATUS_ERROR;
							break;
						}
					}

					// Wait for the next widget
					ParserStatus = GUI_PARSER_STATUS_WAITING;
					break;
				}

				default :
					break;
			}
		}
	}

	if (ParserStatus == GUI_PARSER_STATUS_ERROR)
		return false;

	return true;
}

////////////////////////////////////////////////////////////
/// Initialize the template and window functions
////////////////////////////////////////////////////////////
bool GuiParserInitialize(uinteger DefaultTemplate, char * Path)
{
	if (GuiParserLoadTemplate(DefaultTemplate))
		return GuiParserLoadWindows(Path);

	return false;
}

////////////////////////////////////////////////////////////
/// Destroy all data
////////////////////////////////////////////////////////////
bool GuiParserDestroy()
{

	return true;
}

////////////////////////////////////////////////////////////
/// Register a mapping for script templating
////////////////////////////////////////////////////////////
void GuiParserRegisterMap(uinteger Index, WindowParserMapFunc MapFunction)
{
	WindowParserMap[Index].Tag = WindowParserMapTags[Index].Name;
	WindowParserMap[Index].MapFunc = MapFunction;
}

////////////////////////////////////////////////////////////
/// Load a template or change it if there are one
////////////////////////////////////////////////////////////
bool GuiParserLoadTemplate(uinteger Index)
{

	return true;
}

////////////////////////////////////////////////////////////
/// Load all windows
////////////////////////////////////////////////////////////
bool GuiParserLoadWindows(char * Path)
{
	bool Result = false;
	FILE * WindowFile;
	struct Lexer * Input;

	// Open the file
	if (!fopen(&WindowFile, Path, "r"))
	{
		MessageError("GuiParserLoadWindows", "An internal user interface error : '%s' not found or corrupted.", Path);
		return false;
	}

	// Initialize the lexer
	Input = LexerCreate(WindowFile);

	// Parse the input data
	Result = GuiParserRead(Input);

	// Destroy the lexer
	LexerDestroy(Input);
	Input = NULL;

	// Close the file
	fclose(WindowFile);

	return Result;
}
