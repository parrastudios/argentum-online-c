/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_LIGHT_H
#define WORLD_LIGHT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Build.h>
#include <World/Entity.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define LIGHT_TYPE_SPOT			0x00
#define LIGHT_TYPE_POINT		0x01

static struct ComponentBaseType	ComponentLightBaseAttr;

static struct ComponentImplType	ComponentLightBaseImpl;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct LightBaseComponentData
{
	uinteger	Id;			//< Refers to light manager
	uinteger	Type;		//< Type of light
	bool		Enabled;	//< If light is enabled or not
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get light factory instance
////////////////////////////////////////////////////////////
struct EntityFactoryType * LightFactoryGet();

////////////////////////////////////////////////////////////
/// Create a light entity
////////////////////////////////////////////////////////////
struct EntityType * LightCreate(struct EntityType * Entity, void * Data);

////////////////////////////////////////////////////////////
/// Destroy a light entity
////////////////////////////////////////////////////////////
void LightDestroy(struct EntityType * Entity);

#endif // WORLD_LIGHT_H
