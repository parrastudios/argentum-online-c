/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_FOG_H
#define GRAPHICS_FOG_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/Device.h>
#include <Graphics/Color.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define FOG_QUALITY_LOW		0x00
#define FOG_QUALITY_MEDIUM	0x01
#define FOG_QUALITY_HIGH	0x02

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct FogType
{
	uint32			Mode;
	struct Color4f	Color;
	float			Density;
	float			Near;
	float			Far;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

struct FogType * FogCreate(uint32 Type, struct Color4f * Color, float Density, float Near, float Far);

void FogDestroy(struct FogType * Fog);

void FogSet(struct FogType * Fog, uint32 Type, struct Color4f * Color, float Density, float Near, float Far);

void FogEnable(struct FogType * Fog);

void FogDisable(struct FogType * Fog);

#endif // GRAPHICS_FOG_H