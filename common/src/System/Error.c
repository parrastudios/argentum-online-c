/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/Error.h>
#include <System/IOHelper.h>

#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
#	define WIN32_LEAN_AND_MEAN
#	include <windows.h>
#endif

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define ERROR_MESSAGE_SIZE UINTEGER_SUFFIX(0x03FF)
#define ERROR_MESSAGE_SHOW (COMPILE_TYPE != DEBUG_MODE)

////////////////////////////////////////////////////////////
/// Show a custom msgbox error
////////////////////////////////////////////////////////////
void MessageError(char * Title, char * Message, ...)
{
	// Output string
	char String[ERROR_MESSAGE_SIZE], Error[ERROR_MESSAGE_SIZE];
	FILE * File;

	// Argument list
	va_list ArgList;

	// Initialize the argument list
	va_start(ArgList, Message);

	// sprintf arguments on string
	#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT && COMPILER_TYPE == COMPILER_MSVC
		vsprintf_s(String, ERROR_MESSAGE_SIZE, Message, ArgList);
	#else
		vsnprintf(String, ERROR_MESSAGE_SIZE, Message, ArgList);
	#endif

	// Destroy argument list
	va_end(ArgList);

#if ERROR_MESSAGE_SHOW
	// Show Message
	#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT

		MessageBox(NULL, String, Title, (MB_ICONWARNING | MB_OK));

	#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD

		char Command[ERROR_MESSAGE_SIZE];

		sprintf(Command, "xmessage -center \"%s\" \n\n\n", String);

		if (fork() == 0)
		{
			close(1); close(2);
			system(Command);
			exit(0);
		}

	#elif PLATFORM_TYPE == PLATFORM_MACOS

		int Result = NSRunCriticalAlertPanel(@String, @Title, @"OK", NULL, NULL);

		if ( Result = NSAlertErrorReturn )
		{
			NSLog(@"Error in NSAlertMessage.");
		}

	#endif
#endif

	// Log
	if (fopen(&File, ERROR_FILEPATH, "w"))
	{
		sprintf(Error, "Error : Section(%s) - Reason : %s\n", Title, String);
		fputs(Error, File);
		fclose(File);
	}
}
