/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_VERTEX_ATTRIBUTE_H
#define GRAPHICS_VERTEX_ATTRIBUTE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define VERTEX_ATTRIB_UINT8_1	0x00
#define VERTEX_ATTRIB_UINT8_2	0x01
#define VERTEX_ATTRIB_UINT8_3	0x02
#define VERTEX_ATTRIB_UINT8_4	0x03
#define VERTEX_ATTRIB_UINT32_1	0x04
#define VERTEX_ATTRIB_UINT32_2	0x05
#define VERTEX_ATTRIB_FLOAT_1	0x06
#define VERTEX_ATTRIB_FLOAT_2	0x07
#define VERTEX_ATTRIB_FLOAT_3	0x08
#define VERTEX_ATTRIB_FLOAT_4	0x09

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct VertexAttrib
{
	char*	Name;
	uint32	Type;
	uint32	Offset;
};

#endif // GRAPHICS_VERTEX_ATTRIBUTE_H