/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Config.h>
#include <Memory/General.h>
#include <Graphics/TextureLoader.h>

#include <System/IOHelper.h> // todo: refactor this

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define OUTPUT_PATH		    "../data/heightmap/"
#define OUTPUT_FORMAT		"heightmap%d_%d.%c.raw"
#define INPUT_PATH		    "../data/texture/"
#define INPUT_FORMAT		"texture%d.png"
#define INDEX_MAX_SIZE		0x7FFFFFFF

typedef enum
{
	CHANNEL_RED		    = 0x01,
	CHANNEL_GREEN		= 0x02,
	CHANNEL_BLUE		= 0x04,
	CHANNEL_ALPHA		= 0x08,
	CHANNEL_COUNT		= 0x09

} ChannelMask;

#define BUFFER_CHANNEL_COUNT	0x04

typedef enum OperationType
{
	OPERATION_NONE		= 0x00,
	OPERATION_AVERAGE	= 0x01,
	OPERATION_BLEND		= 0x02,
	OPERATION_INVERT	= 0x03,
	OPERATION_DERIVATE	= 0x04,
	OPERATION_COUNT		= 0x05

} OperationType;

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define ChannelMaskCheck(Channel, Mask) ((Channel) & (Mask))

#define ForEachChannel(Iterator) \
	for (Iterator = CHANNEL_RED; Iterator < CHANNEL_COUNT; Iterator <<= 1)

#define ForEachChannelMask(Iterator, Mask) \
	for (Iterator = CHANNEL_RED; Iterator < CHANNEL_COUNT; Iterator <<= 1) \
		if (ChannelMaskCheck(Iterator, Mask))

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct OperationBlendType
{
	float BlendFactor;
};

typedef union
{
	struct OperationBlendType Blend;
	// ..

} OperationArguments;

typedef uint8 * (*OperationCallback)(ChannelMask InputChannel, ChannelMask OutputChannel, OperationArguments * Args, uint8 * InputBuffer, uint8 * OutputBuffer, uinteger SizeInPixels);

struct ArgumentInfo
{
	uinteger Index;
	ChannelMask InputChannel;
	ChannelMask OutputChannel;
	OperationType Operation;
	OperationArguments OperationArgs;
};

static uinteger ChannelMaskToArray[CHANNEL_COUNT] = {
	0,
	0, // CHANNEL_RED	0x01 [0]
	1, // CHANNEL_GREEN	0x02 [1]
	0,
	2, // CHANNEL_BLUE	0x04 [2]
	0,
	0,
	0,
	3  // CHANNEL_ALPHA	0x08 [3]
};

static char ChannelMaskToId[CHANNEL_COUNT] = {
	'0',
	'r', // CHANNEL_RED	0x01
	'g', // CHANNEL_GREEN	0x02
	'0',
	'b', // CHANNEL_BLUE	0x04
	'0',
	'0',
	'0',
	'a'  // CHANNEL_ALPHA	0x08
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Write output buffer to raw files
////////////////////////////////////////////////////////////
void WriteOutputBuffer(ChannelMask OutputChannel, uint8 * OutputBuffer, uinteger Index, uinteger Size, uinteger SizeInPixels)
{
	uinteger OutputBufferCount = 0;

	ChannelMask Channel;

	ForEachChannel(Channel)
	{
		if (ChannelMaskCheck(Channel, OutputChannel))
		{
			FILE * File;

			char Path[0xFF];

			sprintf(Path, OUTPUT_PATH OUTPUT_FORMAT, (unsigned int)Index, (unsigned int)Size, ChannelMaskToId[Channel]);

			if (fopen(&File, Path, "wb"))
			{
				// Write current channel
				uinteger WriteBytes = fwrite(&OutputBuffer[OutputBufferCount], sizeof(uint8), SizeInPixels, File);

				if (WriteBytes != SizeInPixels)
				{
					printf("Error: Invalid generated file (%s)\n", Path);
					printf("\tWriteBytes: %u, SizeInPixels: %u\n", (unsigned int)WriteBytes, (unsigned int)SizeInPixels);
				}

				// Close file
				fclose(File);
			}
		}

		// Skip output buffer to the next array
		OutputBufferCount += SizeInPixels;
	}
}

////////////////////////////////////////////////////////////
/// Read input buffer from texture
////////////////////////////////////////////////////////////
uint8 * ReadInputBuffer(struct Texture * HeightMapTexture, ChannelMask InputChannel)
{
	uinteger TextureSizeInPixels = HeightMapTexture->Width * HeightMapTexture->Height;

	uint8 * InputBuffer = MemoryAllocate(sizeof(uint8) * TextureSizeInPixels * HeightMapTexture->Channels);

	if (InputBuffer)
	{
        ChannelMask Channel;
        uinteger InputBufferCount = 0;

        // Store texture in channels instead of by pixels
        ForEachChannel(Channel)
        {
            if (ChannelMaskCheck(Channel, InputChannel) && ChannelMaskToArray[Channel] < HeightMapTexture->Channels)
            {
                uinteger Pixel;

                for (Pixel = 0; Pixel < TextureSizeInPixels; ++Pixel)
                {
                    uinteger HeightMapIndex = Pixel * HeightMapTexture->Channels + ChannelMaskToArray[Channel];

                    InputBuffer[InputBufferCount++] = HeightMapTexture->Pixels[HeightMapIndex];
                }
            }
        }

        if (InputBufferCount != (TextureSizeInPixels * HeightMapTexture->Channels))
        {
            MemoryDeallocate(InputBuffer);

            return NULL;
        }

		return InputBuffer;
	}

	return NULL;
}


////////////////////////////////////////////////////////////
/// Print tool options
////////////////////////////////////////////////////////////
void PrintHelp()
{
	printf(	"\nHeightmap generator v0.1 by Parra Studios\n"
		"Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garcia (Parra) <vic798@gmail.com>\n\n"
		"Name\n"
		"\theightmap - Converts a rgba png texture into an heightmap appliying desired operations and filters\n\n"
		"Synopsis\n"
		"\theightmap [OPTION]... \n\n"
		"\t-d, --index\n"
		"\t\tspecify the index of the texture and the output heightmap [unsigned integer]\n\n"
		"\t-i, --inputchannels\n"
		"\t\ttexture input channels [rgba]\n\n"
		"\t-o, --outputchannels\n"
		"\t\ttexture output channels [rgba]\n\n"
		"\t-p, --operation\n"
		"\t\tthe operation to be applied [none, average, blend, derivate] [ARGS]...\n\n"
		"\t-h, --help\n"
		"\t\tprint man\n\n");
}

////////////////////////////////////////////////////////////
/// Parse channel options
////////////////////////////////////////////////////////////
bool ParseChannel(ChannelMask * Channel, char * Argument)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Parse operation options
////////////////////////////////////////////////////////////
bool ParseOperation(OperationType * Operation, char * Argument)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Parse operation argument options
////////////////////////////////////////////////////////////
bool ParseOperationArguments(OperationArguments * OpArgs, char * Arguments[], uinteger * Count)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Parse application arguments
////////////////////////////////////////////////////////////
bool ParseArguments(struct ArgumentInfo * Arg, int argc, char * argv[])
{
	// Check for valid arguments
	if (argc > 1)
	{
		uinteger i;

		// Iterate through argument list
		for (i = 1; i < argc; ++i)
		{
			switch (argv[i][1]) // switch (-[%c])
			{
				case 'd' :
				{
					// Set up the index
					Arg->Index = (uinteger)atoi(argv[++i]);

					if (Arg->Index > INDEX_MAX_SIZE)
					{
						printf("Error: Invalid index (%u), index must be between [0, %u]\n", (unsigned int)Arg->Index, (unsigned int)INDEX_MAX_SIZE);

						return false;
					}

					break;
				}

				case 'i' :
				{
					// Parse input channel
					if (!ParseChannel(&Arg->InputChannel, argv[++i]))
					{
						printf("Error: Invalid input channel\n");

						return false;
					}

					break;
				}

				case 'o' :
				{
					// Parse output channel
					if (!ParseChannel(&Arg->OutputChannel, argv[++i]))
					{
						printf("Error: Invalid output channel\n");

						return false;
					}

					break;
				}

				case 'p' :
				{
					// Parse operation
					if (!ParseOperation(&Arg->Operation, argv[++i]))
					{
						printf("Error: Invalid operation type\n");

						return false;
					}

					// Parse operation arguments
					if (!ParseOperationArguments(&Arg->OperationArgs, argv, &i))
					{
						printf("Error: Invalid operation arguments\n");

						return false;
					}

					break;
				}

				case 'h' :
				{
					// Print man
					PrintHelp();

					break;
				}

			};
		}
	}
	else
	{
		// Default arguments
		Arg->Index = 2;
		Arg->InputChannel = CHANNEL_RED;
		Arg->OutputChannel = CHANNEL_RED;
		Arg->Operation = OPERATION_NONE;
	}

	return true;
}

////////////////////////////////////////////////////////////
/// Execute a no operation
////////////////////////////////////////////////////////////
uint8 * OperationCallbackNone(ChannelMask InputChannel, ChannelMask OutputChannel, OperationArguments * Args, uint8 * InputBuffer, uint8 * OutputBuffer, uinteger SizeInPixels)
{
    uinteger InputBufferCount = 0;
	uinteger OutputBufferCount = 0;

	ChannelMask Channel;

	ForEachChannel(Channel)
	{
		if (ChannelMaskCheck(Channel, InputChannel) && ChannelMaskCheck(Channel, OutputChannel))
		{
			MemoryCopy(&OutputBuffer[OutputBufferCount], &InputBuffer[InputBufferCount], SizeInPixels);

            OutputBufferCount += SizeInPixels;
		}

		InputBufferCount += SizeInPixels;
	}

	return OutputBuffer;
}

////////////////////////////////////////////////////////////
/// Execute the desired operation
////////////////////////////////////////////////////////////
uint8 * OperationExecute(OperationType Operation, ChannelMask InputChannel, ChannelMask OutputChannel, OperationArguments * Args, uint8 * InputBuffer, uinteger SizeInPixels)
{
	uint8 * OutputBuffer = MemoryAllocate(sizeof(uint8) * SizeInPixels * BUFFER_CHANNEL_COUNT);

	if (OutputBuffer)
	{
		static OperationCallback OperationCallbackList[OPERATION_COUNT] = {
			&OperationCallbackNone,
			NULL,
			NULL,
			NULL,
			NULL
		};

		return OperationCallbackList[Operation](InputChannel, OutputChannel, Args, InputBuffer, OutputBuffer, SizeInPixels);
	}

	return OutputBuffer;
}

////////////////////////////////////////////////////////////
// Application entry point
////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
	struct ArgumentInfo Arg;

	if (ParseArguments(&Arg, argc, argv))
	{
		char Path[0xFF];

		struct Texture HeightMapTexture;

		sprintf(Path, INPUT_PATH INPUT_FORMAT, (unsigned int)Arg.Index);

		// Load png texture
		if (TextureLoadFromFile(Path, &HeightMapTexture))
		{
			if (HeightMapTexture.Pixels && (HeightMapTexture.Height == HeightMapTexture.Width))
			{
				// Generate the input buffer
				uint8 * InputBuffer = ReadInputBuffer(&HeightMapTexture, Arg.InputChannel);

				if (InputBuffer)
				{
					uinteger SizeInPixels = HeightMapTexture.Width * HeightMapTexture.Height;

					// Apply the operation from input to output buffer
					uint8 * OutputBuffer = OperationExecute(Arg.Operation, Arg.InputChannel, Arg.OutputChannel, &Arg.OperationArgs, InputBuffer, SizeInPixels);

					if (OutputBuffer)
					{
						// Write resulting output buffer
						WriteOutputBuffer(Arg.OutputChannel, OutputBuffer, Arg.Index, HeightMapTexture.Width, SizeInPixels);

						// Deallocate output buffer
						MemoryDeallocate(OutputBuffer);

						// Show result message
						printf("Heightmap created sucessfully.\n");
					}

					// Deallocate input buffer
					MemoryDeallocate(InputBuffer);
				}
			}

			// Clear texture
			MemoryDeallocate(HeightMapTexture.Pixels);
		}

		return 0;
	}
	else
	{
		printf("Invalid program arguments, abort.\n");

		return 1;
	}
}
