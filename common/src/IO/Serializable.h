/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef IO_SERIALIZABLE_H
#define IO_SERIALIZABLE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Types.h>

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define SerializableDeclareValueData(Id, Type) \
	SERIALIZABLE_VALUE_DATA_##Type = Id

#define SerializableValueData(Type) SERIALIZABLE_VALUE_DATA_##Type

#define SERIALIZABLE_VALUE_CAST_UINT8	(uint8)
#define SERIALIZABLE_VALUE_CAST_INT8	(int8)
#define SERIALIZABLE_VALUE_CAST_UINT16	(uint16)
#define SERIALIZABLE_VALUE_CAST_INT16	(int16)
#define SERIALIZABLE_VALUE_CAST_UINT32	(uint32)
#define SERIALIZABLE_VALUE_CAST_INT32	(int32)
#define SERIALIZABLE_VALUE_CAST_UINT64	(uint64)
#define SERIALIZABLE_VALUE_CAST_INT64	(int64)
#define SERIALIZABLE_VALUE_CAST_FLOAT	(float32)
#define SERIALIZABLE_VALUE_CAST_DOUBLE	(float64)
#define SERIALIZABLE_VALUE_CAST_STRING	(uint8 *)

#define SerializableValueCast(TypeName) \
	SERIALIZABLE_VALUE_CAST_##TypeName

#define SerializableValueGet(Value, TypeName) \
	SerializableValueCast(TypeName)SerializableValueGetData(Value)

#define SERIALIZABLE_VALUE_SIZE_UINT8	sizeof(uint8)
#define SERIALIZABLE_VALUE_SIZE_INT8	sizeof(int8)
#define SERIALIZABLE_VALUE_SIZE_UINT16	sizeof(uint16)
#define SERIALIZABLE_VALUE_SIZE_INT16	sizeof(int16)
#define SERIALIZABLE_VALUE_SIZE_UINT32	sizeof(uint32)
#define SERIALIZABLE_VALUE_SIZE_INT32	sizeof(int32)
#define SERIALIZABLE_VALUE_SIZE_UINT64	sizeof(uint64)
#define SERIALIZABLE_VALUE_SIZE_INT64	sizeof(int64)
#define SERIALIZABLE_VALUE_SIZE_FLOAT	sizeof(float32)
#define SERIALIZABLE_VALUE_SIZE_DOUBLE	sizeof(float64)
#define SERIALIZABLE_VALUE_SIZE_STRING	SERIALIZABLE_VALUE_DATA_STRING_LENGTH

#define SerializableValueSize(TypeName) \
	SERIALIZABLE_VALUE_SIZE_##TypeName

#define SerializableDeclareValueSize(TypeName) \
	ArrayInit(SerializableValueData(TypeName), SerializableValueSize(TypeName))

#define SerializableValueDataSizeInfo(Type) \
	SerializableValueDataSizeInfo[Type]

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define SERIALIZABLE_VALUE_DATA_STRING_LENGTH	0xFF
#define SERIALIZABLE_FUNCTION_NAME_LENGTH		0xFF

enum SerializableValueDataInfo
{
	SerializableDeclareValueData(0x00, UINT8),
	SerializableDeclareValueData(0x01, INT8),
	SerializableDeclareValueData(0x02, UINT16),
	SerializableDeclareValueData(0x03, INT16),
	SerializableDeclareValueData(0x04, UINT32),
	SerializableDeclareValueData(0x05, INT32),
	SerializableDeclareValueData(0x06, UINT64),
	SerializableDeclareValueData(0x07, INT64),
	SerializableDeclareValueData(0x08, FLOAT),
	SerializableDeclareValueData(0x09, DOUBLE),
	SerializableDeclareValueData(0x0A, STRING),

	SERIALIZABLE_VALUE_DATA_TYPE_SIZE,

	SERIALIZABLE_VALUE_DATA_TYPE_INVALID = 0xFFFFFFFF
};

typedef uinteger SerializableValueDataSize;

static const SerializableValueDataSize SerializableValueDataSizeInfo[SERIALIZABLE_VALUE_DATA_TYPE_SIZE] =
{
	SerializableDeclareValueSize(UINT8),
	SerializableDeclareValueSize(INT8),
	SerializableDeclareValueSize(UINT16),
	SerializableDeclareValueSize(INT16),
	SerializableDeclareValueSize(UINT32),
	SerializableDeclareValueSize(INT32),
	SerializableDeclareValueSize(UINT64),
	SerializableDeclareValueSize(INT64),
	SerializableDeclareValueSize(FLOAT),
	SerializableDeclareValueSize(DOUBLE),
	SerializableDeclareValueSize(STRING)
};

typedef uinteger								SerializableValueDataType;
typedef uinteger								SerializableValueDataCount;
typedef struct SerializableValueType *			SerializableValueType;
typedef struct SerializableReferenceType *		SerializableReferenceType;
typedef union SerializableParameterType *		SerializableParameterType;
typedef uinteger								SerializableParameterListSize;
typedef struct SerializableParameterListType *	SerializableParameterListType;
typedef char									SerializableFunctionNameType[SERIALIZABLE_FUNCTION_NAME_LENGTH];
typedef struct SerializableFunctionType *		SerializableFunctionType;
typedef struct SerializableObjectType *			SerializableObjectType;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a serial value
////////////////////////////////////////////////////////////
SerializableValueType SerializableValueCreate(SerializableValueDataType Type, SerializableValueDataCount Count, void * Data);

////////////////////////////////////////////////////////////
/// Get the type of a value
////////////////////////////////////////////////////////////
SerializableValueDataType SerializableValueTypeOf(SerializableValueType Value);

////////////////////////////////////////////////////////////
/// Get the amount of values that contains itself
////////////////////////////////////////////////////////////
SerializableValueDataCount SerializableValueCount(SerializableValueType Value);

////////////////////////////////////////////////////////////
/// Get the data wraped by a value
////////////////////////////////////////////////////////////
void * SerializableValueGetData(SerializableValueType Value);

////////////////////////////////////////////////////////////
/// Set the internal data of a value
////////////////////////////////////////////////////////////
void SerializableValueSet(SerializableValueType Value, void * Data);

////////////////////////////////////////////////////////////
/// Destroy a value
////////////////////////////////////////////////////////////
void SerializableValueDestroy(SerializableValueType Value);

////////////////////////////////////////////////////////////
/// Create a serial reference
////////////////////////////////////////////////////////////
SerializableReferenceType SerializableReferenceCreate(SerializableValueType Value);

////////////////////////////////////////////////////////////
/// Set a reference to a value
////////////////////////////////////////////////////////////
void SerializableReferenceSet(SerializableReferenceType Reference, SerializableValueType Value);

////////////////////////////////////////////////////////////
/// Get the data that reference is pointing to
////////////////////////////////////////////////////////////
void * SerializableReferenceGetValue(SerializableReferenceType Reference);

////////////////////////////////////////////////////////////
/// Set the internal data of a value
////////////////////////////////////////////////////////////
void SerializableReferenceSetValue(SerializableReferenceType Reference, void * Data);

////////////////////////////////////////////////////////////
/// Destroy a reference
////////////////////////////////////////////////////////////
void SerializableReferenceDestroy(SerializableReferenceType Reference);









////////////////////////////////////////////////////////////
/// Create a parameter list
////////////////////////////////////////////////////////////
SerializableParameterListType SerializableParameterListCreate(SerializableParameterListSize Size);


// ...


////////////////////////////////////////////////////////////
/// Destroy a parameter list
////////////////////////////////////////////////////////////
void SerializableParameterListDestroy(SerializableParameterListType ParameterList);


#endif // IO_STREAM_H
