/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/Skin.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Methods
///////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a skinned mesh
////////////////////////////////////////////////////////////
struct SkinType * SkinCreate(struct ModelResourceType * Resource)
{
	// Create mesh
	struct MeshType * Mesh = MeshCreate(Resource);

	if (Mesh)
	{
		// todo: do a proper allocation (factory)
		struct SkinType * Skin = (struct SkinType *)MemoryAllocate(sizeof(struct SkinType));

		if (Skin)
		{
			// Set mesh reference
			Skin->Mesh = Mesh;

			// Set up joint size
			Skin->JointSize = VectorSize(Resource->Joints);

			// Set up frame size
			Skin->FrameSize = VectorSize(Resource->BaseFrame);

			// Initialize skin
			SkinInitialize(Skin);

			// Copy vertex list
			Skin->OutputVertexList = MeshCopyVertexList(Mesh);

			if (Skin->OutputVertexList)
			{
				uinteger i;

				// Set vertex list reference
				Skin->VertexList = Mesh->VertexList;
				
				// Copy parent joints
				for (i = 0; i < Skin->JointSize; ++i)
				{
					struct ModelJointType * Joint = (struct ModelJointType *)VectorAt(Resource->Joints, i);

					// Copy parent
					Skin->ParentJoints[i] = Joint->Parent;
				}

				// Copy base frame list
				for (i = 0; i < Skin->FrameSize; ++i)
				{
					struct Matrix34f * BaseFrame = (struct Matrix34f *)VectorAt(Resource->BaseFrame, i);
					struct Matrix34f * InverseBaseFrame = (struct Matrix34f *)VectorAt(Resource->InverseBaseFrame, i);

					// Copy frames
					Matrix34fCopy(&Skin->BaseFrame[i], BaseFrame);
					Matrix34fCopy(&Skin->InverseBaseFrame[i], InverseBaseFrame);
				}

				// Copy frame list
				Skin->PoseSize = Resource->PoseSize;

				Skin->Frames = (struct Matrix34f *)MemoryAllocate(sizeof(struct Matrix34f) * VectorSize(Resource->Frames));

				for (i = 0; i < VectorSize(Resource->Frames); ++i)
				{
					struct Matrix34f * Frame = (struct Matrix34f *)VectorAt(Resource->Frames, i);

					// Copy frames
					Matrix34fCopy(&Skin->Frames[i], Frame);
				}

				// Copy blend weights and its indexes
				for (i = 0; i < Skin->Mesh->VertexListSize; ++i)
				{
					uinteger Index = i * 4;

					struct Vector4ub * BlendWeight = (struct Vector4ub *)VectorAt(Resource->BlendWeights, i);
					
					struct Vector4ub * BlendIndex = (struct Vector4ub *)VectorAt(Resource->BlendIndexes, i);

					Skin->BlendWeights[Index + 0] = BlendWeight->x;
					Skin->BlendWeights[Index + 1] = BlendWeight->y;
					Skin->BlendWeights[Index + 2] = BlendWeight->z;
					Skin->BlendWeights[Index + 3] = BlendWeight->w;

					Skin->BlendIndexes[Index + 0] = BlendIndex->x;
					Skin->BlendIndexes[Index + 1] = BlendIndex->y;
					Skin->BlendIndexes[Index + 2] = BlendIndex->z;
					Skin->BlendIndexes[Index + 3] = BlendIndex->w;
				}

				return Skin;
			}

			// Destroy skin
			SkinDestroy(Skin);
		}

		// Destroy mesh on error
		MeshDestroy(Mesh);
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Initialize a skinned mesh
////////////////////////////////////////////////////////////
void SkinInitialize(struct SkinType * Skin)
{
	if (Skin)
	{
		// Allocate joints
		Skin->ParentJoints = (integer *)MemoryAllocate(sizeof(integer) * Skin->JointSize);

		// Allocate base frames
		Skin->BaseFrame = (struct Matrix34f *)MemoryAllocate(sizeof(struct Matrix34f) * Skin->JointSize);

		// Allocate inverse base frames
		Skin->InverseBaseFrame = (struct Matrix34f *)MemoryAllocate(sizeof(struct Matrix34f) * Skin->JointSize);

		// Allocate blend weights
		Skin->BlendWeights = (uint8 *)MemoryAllocate(sizeof(uint8) * Skin->Mesh->VertexListSize * 4);

		// Allocate index blend weights
		Skin->BlendIndexes = (uint8 *)MemoryAllocate(sizeof(uint8) * Skin->Mesh->VertexListSize * 4);
	}
}

////////////////////////////////////////////////////////////
/// Clear a skinned mesh
////////////////////////////////////////////////////////////
void SkinClear(struct SkinType * Skin)
{
	if (Skin)
	{
		// Destroy joints
		MemoryDeallocate(Skin->ParentJoints);

		// Destroy base frames
		MemoryDeallocate(Skin->BaseFrame);

		// Destroy inverse base frames
		MemoryDeallocate(Skin->InverseBaseFrame);

		// Destroy frames
		MemoryDeallocate(Skin->Frames);

		// Destroy blend weights
		MemoryDeallocate(Skin->BlendWeights);

		// Destroy index blend weights
		MemoryDeallocate(Skin->BlendIndexes);
	}
}

////////////////////////////////////////////////////////////
/// Destroy a skinned mesh
////////////////////////////////////////////////////////////
void SkinDestroy(struct SkinType * Skin)
{
	if (Skin)
	{
		// Clear skin
		SkinClear(Skin);

		// Restore original vertex list reference
		Skin->Mesh->VertexList = Skin->VertexList;

		// Destroy mesh
		MeshDestroy(Skin->Mesh);

		// Set null the vertex list reference
		Skin->VertexList = NULL;

		// Destroy output vertex list
		MemoryDeallocate(Skin->OutputVertexList);

		// Destroy skin (todo: destroy it in the factory)
		MemoryDeallocate(Skin);
	}
}

////////////////////////////////////////////////////////////
/// Render a skinned mesh
////////////////////////////////////////////////////////////
void SkinRender(struct SkinType * Skin)
{
	if (Skin)
	{
		// todo: review this

		// Set current mesh vertex list
		Skin->Mesh->VertexList = Skin->OutputVertexList;

		// Render mesh
		MeshRender(Skin->Mesh);

		// Restore original mesh vertex list
		Skin->Mesh->VertexList = Skin->VertexList;
	}
}

////////////////////////////////////////////////////////////
/// Animate a skinned mesh
////////////////////////////////////////////////////////////
void SkinAnimate(struct SkinType * Skin, float ElapsedTime)
{
	if (Skin)
	{
		uinteger i;

		uinteger CurrentFrame = (uinteger)floorf(ElapsedTime);
		uinteger NextFrame = (CurrentFrame + 1);
		
		// Get remainder of the floor
		float FrameOffset = ElapsedTime - CurrentFrame;

		struct Matrix34f * CurrentMatrix, * NextMatrix;

		// Restart current frame counter
		if (CurrentFrame >= Skin->FrameSize)
		{
			CurrentFrame -= Skin->FrameSize;
		}

		// Restart next frame counter
		if (NextFrame >= Skin->FrameSize)
		{
			NextFrame -= Skin->FrameSize;
		}

		// Get closest frame matrixes
		CurrentMatrix = &Skin->Frames[CurrentFrame * Skin->PoseSize];
		NextMatrix = &Skin->Frames[NextFrame * Skin->PoseSize];

		// Interpolate matrixes, concatenate with parent if necessary
		// Result will be concatenated with inverse of base pose (todo: animation blending & inter-frame blending with NLerp & shaders)
		for (i = 0; i < Skin->JointSize; ++i)
		{
			struct Matrix34f ScaledCurrentMatrix, ScaledNextMatrix, InterpolatedMatrix;

			// Scale current matrix
			Matrix34fScale(&ScaledCurrentMatrix, &CurrentMatrix[i], (1.0f - FrameOffset));

			// Scale next matrix
			Matrix34fScale(&ScaledNextMatrix, &NextMatrix[i], FrameOffset);

			// Calculate interpolated matrix
			Matrix34fAdd(&InterpolatedMatrix, &ScaledCurrentMatrix, &ScaledNextMatrix);

			if (Skin->ParentJoints[i] >= 0)
			{
				struct Matrix34f Matrix;

				Matrix34fMultiply(&Matrix, &Skin->BaseFrame[Skin->ParentJoints[i]], &InterpolatedMatrix);

				Matrix34fCopy(&Skin->BaseFrame[i], &Matrix);
			}
			else
			{
				Matrix34fCopy(&Skin->BaseFrame[i], &InterpolatedMatrix);
			}
		}

		// Generate vertex list from matrixes (todo: move to shaders)
		for (i = 0; i < Skin->Mesh->VertexListSize; ++i)
		{
			uinteger j;

			// Blend matrixes for this vertex based on blend weights
			// First weight is mandatory and weights are guaranteed to add up to 255
			// If only the first weight is presented, this case can be optimized by
			// skipping any weight multiplies and intermediate storage of a blended matrix
			// There are only at most 4 weights per vertex, and they are in 
			// sorted order from highest weight to lowest weight
			// Weights with 0 values, which are always at the end, are unused
			
			struct Matrix34f Matrix;

			uinteger Index = i * 4;

			// Apply initial weight
			Matrix34fScale(&Matrix, &Skin->BaseFrame[Skin->BlendIndexes[Index]], (float)(Skin->BlendWeights[Index] / 255.0f));

			// Apply remaining weights
			for (j = 1; j < 4 && Skin->BlendWeights[Index + j] > 0; ++j)
			{
				struct Matrix34f BlendMatrix;

				// Apply weight
				Matrix34fScale(&BlendMatrix, &Skin->BaseFrame[Skin->BlendIndexes[Index + j]], (float)(Skin->BlendWeights[Index + j] / 255.0f));

				// Increment to the original matrix
				Matrix34fAdd(&Matrix, &Matrix, &BlendMatrix);
			}

			// Transform vertex attributes by blended matrix
			// Position uses the full 3x4 transformation matrix
			// Normals and tangents only use 3x3 rotation part of the transform matrix
			{
				struct Vector3f Position, BlendPosition;

				// Set position vector
				Vector3fSet(&Position, Skin->VertexList[i].Position[0], Skin->VertexList[i].Position[1], Skin->VertexList[i].Position[2]);

				// Calculate blended position
				Matrix34fTransform(&Matrix, &Position, &BlendPosition);

				// Output vertex list position
				Skin->OutputVertexList[i].Position[0] = BlendPosition.x;
				Skin->OutputVertexList[i].Position[1] = BlendPosition.y;
				Skin->OutputVertexList[i].Position[2] = BlendPosition.z;
			}

			// If matrix includes non-uniform scaling, normal vectors must be transformed
			// by the inverse transpose of the matrix to have a correct relative scale:
			//		invert(mat) = adjoint(mat) / determinant(mat)
			// Since the absolute scale is not important for a vector that will later
			// be renormalized, the adjoint-transpose matrix will work fine, which can be
			// cheaply generated by 3 cross-products
			{
				// If not using joint scaling in models, simply use the upper 3x3 part of
				// the position matrix instead of the adjoint-transpose here
				struct Matrix3f MatrixNormal;

				struct Vector3f Normal, BlendNormal;

				// Calculate adjoint transpose matrix
				Matrix3fFromAdjointTranspose(&MatrixNormal, &Matrix);

				// Set normal vector
				Vector3fSet(&Normal, Skin->VertexList[i].Normal[0], Skin->VertexList[i].Normal[1], Skin->VertexList[i].Normal[2]);

				// Transform normal vector
				Matrix3fTransform(&MatrixNormal, &Normal, &BlendNormal);

				// Output vertex list normal
				Skin->OutputVertexList[i].Normal[0] = BlendNormal.x;
				Skin->OutputVertexList[i].Normal[1] = BlendNormal.y;
				Skin->OutputVertexList[i].Normal[2] = BlendNormal.z;
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Get current mesh of a skinned mesh
////////////////////////////////////////////////////////////
struct MeshType * SkinGetMesh(struct SkinType * Skin)
{
	if (Skin)
	{
		return Skin->Mesh;
	}

	return NULL;
}
