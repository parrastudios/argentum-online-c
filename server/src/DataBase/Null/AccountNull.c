/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2016 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/Account.h>
#include <DataBase/Character.h>
#include <DataBase/Player.h>
#include <DataBase/Query.h>
#include <Game/Account.h>


////////////////////////////////////////////////////////////
///	Initialize all account stored procedures
////////////////////////////////////////////////////////////
bool AccountStoredProcedureInitialize()
{

	return true;
}

////////////////////////////////////////////////////////////
///	Release all account stored procedures
////////////////////////////////////////////////////////////
void AccountStoredProcedureRelease()
{

}

////////////////////////////////////////////////////////////
///	Check if the account exist in the database
////////////////////////////////////////////////////////////
bool DBAccountExist(struct DBAccountData * Account)
{
	return true;
}

////////////////////////////////////////////////////////////
///	Get data from account
////////////////////////////////////////////////////////////
bool DBAccountGetDataByEmail(struct DBAccountData * Account, char * Email)
{
	if (Account)
	{
		// Get account data
		Account->Id = 0;
		strcpy(Account->Name, "Parra");
		strcpy(Account->Password, "1234");
		strcpy(Account->Email, "null@aoc.com");
		//strcpy(Account->Ip, "0.0.0.0");
		Account->Banned = false;
		Account->Online = false;
		Account->Active = true;
		//MemorySet(Account->CodeActive, 0, sizeof(char) * DB_ACCOUNT_CODEACTIVE_MAX_SIZE);

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
///	Get data from account
////////////////////////////////////////////////////////////
bool DBAccountGetDataByName(struct DBAccountData * Account, char * Name)
{
	if (Account)
	{
		// Get account data
		Account->Id = 0;
		strcpy(Account->Name, "Parra");
		strcpy(Account->Password, "1234");
		strcpy(Account->Email, "null@aoc.com");
		//strcpy(Account->Ip, "0.0.0.0");
		Account->Banned = false;
		Account->Online = false;
		Account->Active = true;
		//MemorySet(Account->CodeActive, 0, sizeof(char) * DB_ACCOUNT_CODEACTIVE_MAX_SIZE);

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
///	Set logged field from account
////////////////////////////////////////////////////////////
bool DBAccountSetLogged(struct DBAccountData * Account, bool Logged)
{
	if (Account)
	{
		Account->Online = Logged;

		return true;
	}

	return false;
}


////////////////////////////////////////////////////////////
///	Get logged field from account
////////////////////////////////////////////////////////////
bool DBAccountCheckLogged(struct DBAccountData * Account, bool * Logged)
{
    if (Account)
    {
		*Logged = Account->Online;
    }

	// Set logged flag to false
	*Logged = false;

	return false;
}

////////////////////////////////////////////////////////////
///	Count the number of players associated to account
////////////////////////////////////////////////////////////
uinteger DBAccountGetPlayersSize(struct DBAccountData * Account)
{
	if (Account)
	{
		return UINTEGER_SUFFIX(1);
	}

	return UINTEGER_SUFFIX(0);
}

////////////////////////////////////////////////////////////
///	Query players basic info of an account
////////////////////////////////////////////////////////////
bool DBAccountGetPlayers(struct DBAccountData * Account, Vector DBPlayerList)
{
	if (Account && DBPlayerList)
	{
		struct DBCharacterData * DBCharacter;

		// Insert empty field into list
		VectorPushBackEmpty(DBPlayerList);

		DBCharacter = (struct DBCharacterData*)VectorBack(DBPlayerList);

		// Initialize database object
		DBCharacterInitialize(DBCharacter);

		// Load player by row
		DBPlayerLoadByRow(NULL, DBCharacter);

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
///	Convert database account to gamelogic account
////////////////////////////////////////////////////////////
void DBAccountToAccount(struct DBAccountData * DBAccount, struct AccountData * Account)
{
	Account->Id			= DBAccount->Id;
	Account->Banned		= DBAccount->Banned;
	Account->Logged		= DBAccount->Online;
	Account->Active		= DBAccount->Active;

	strcpy(Account->Name, DBAccount->Name);
	strcpy(Account->Password, DBAccount->Password);
	strcpy(Account->Email, DBAccount->Email);

	Account->PlayerLogged = ACCOUNT_PLAYER_NO_LOGGED;
	Account->PlayersReference = VectorNew(sizeof(uinteger));
}
