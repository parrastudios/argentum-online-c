var connection = require('./Connection');
var options = require('../Options');

var foreignKeys = [];

module.exports = {

  getForeignKeys: function (callback) {

    connection.query('SELECT ke.constraint_name constraintName, ke.table_name parentTableName , ke.column_name parentColumnName, ke.referenced_table_name childTableName , ke.referenced_column_name childColumnName FROM  information_schema.KEY_COLUMN_USAGE ke WHERE  ke.referenced_table_name IS NOT NULL AND ke.CONSTRAINT_SCHEMA = "' + options.database + '" ORDER BY  ke.referenced_table_name;', function (err, rows, fields) {
      if (err) throw err;

      for (var index = 0; index < rows.length; index++) {
        var row = rows[index];

        foreignKeys.push({
          constraintName: row.constraintName,
          parentTableName: row.parentTableName,
          parentColumnName: row.parentColumnName,
          childTableName: row.childTableName,
          childColumnName: row.childColumnName
        });
      }

      callback(foreignKeys);

    });

  }

}
