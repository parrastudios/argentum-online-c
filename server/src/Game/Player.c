/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Character.h>
#include <DataBase/Player.h>

////////////////////////////////////////////////////////////
/// Initialize the player
////////////////////////////////////////////////////////////
void PlayerInitialize(struct CharacterData * Character)
{
	struct PlayerData * Player = &Character->Attributes.Player;

	Player->Name[0]		= NULLCHAR;
	Player->Desc[0]		= NULLCHAR;
	Player->Active		= false;

	// todo ...
}

////////////////////////////////////////////////////////////
/// Destroy the player
////////////////////////////////////////////////////////////
void PlayerDestroy(struct CharacterData * Character)
{
	struct PlayerData * Player = &Character->Attributes.Player;

}

////////////////////////////////////////////////////////////
/// Connect the player
////////////////////////////////////////////////////////////
uint32 PlayerLogin(struct CharacterData * Character)
{
	struct PlayerData * Player = &Character->Attributes.Player;

	if (Player->Ban)
	{
		return PLAYER_LOG_IN_ERROR_BAN;
	}

	if (Player->Active)
	{
		return PLAYER_LOG_IN_ERROR_LOGGED;
	}

	Player->Active = true;

	// Set active flag into database
	#if COMPILE_TYPE != DEBUG_MODE
	{
		struct DBCharacterData DBCharacter;

		PlayerToDBCharacter(Character, &DBCharacter);

		DBPlayerActive(&DBCharacter);
	}
	#endif

	// todo: Insert player into scene

	return PLAYER_LOG_IN_ERROR_NONE;
}

bool PlayerDisconnect(struct CharacterData * Character)
{
	// todo
	struct DBCharacterData DBCharacter;
	struct PlayerData * Player = &Character->Attributes.Player;

	// Check if user can exit or not
	// {

	// Close the player
	Player->Active = false;

	// Convert character to database object
	PlayerToDBCharacter(Character, &DBCharacter);

	// Update player into database
	DBPlayerStore(&DBCharacter);

	// todo: Remove player from scene

	// }

	return true;
}

bool PlayerCheckBan(struct CharacterData * Character)
{
	// todo: checks if a player is banned

	return true;
}
