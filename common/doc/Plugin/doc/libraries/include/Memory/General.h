/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_MEMORY_GENERAL_H
#define SYSTEM_MEMORY_GENERAL_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Memory/Alignment.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define	MEMORY_SYSTEM_DEFAULT	0x00	///< By default
#define MEMORY_SYSTEM_CUSTOM	0x01	///< Memory Pool

#ifndef	MEMORY_SYSTEM_TYPE
#	define MEMORY_SYSTEM_TYPE	MEMORY_SYSTEM_DEFAULT
#endif

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#if MEMORY_SYSTEM_TYPE == MEMORY_SYSTEM_DEFAULT

#	include <stdlib.h>	// malloc, realloc, free
#	include <string.h>	// memcpy, memmove

#	ifndef MemoryAllocate
#		define MemoryAllocate malloc
#	endif
#	ifndef MemoryReallocate
#		define MemoryReallocate realloc
#	endif
#	ifndef MemoryDeallocate
#		define MemoryDeallocate(Ptr) { if (Ptr) { free((void*)Ptr); (Ptr) = NULL; } }
#	endif
#	ifndef MemoryCopy
#		define MemoryCopy(Dest, Src, Size) memcpy(Dest, Src, Size)
#	endif
#	ifndef MemoryMove
#		define MemoryMove(Dest, Src, Size) memmove(Dest, Src, Size)
#	endif
#	ifndef MemorySet
#		define MemorySet(Ptr, Value, Size) memset(Ptr, Value, Size)
#	endif
#elif MEMORY_SYSTEM_TYPE == MEMORY_SYSTEM_CUSTOM
#	include <Memory/Pool.h>
#else
#	error Invalid memory system
#endif

#endif // SYSTEM_MEMORY_GENERAL_H
