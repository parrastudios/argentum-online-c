/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	METADATA_PARAMETER_H
#define METADATA_PARAMETER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/VariableArguments.h>

#include <Preprocessor/Arguments.h>
#include <Preprocessor/Repeat.h>

#include <MetaData/Storage.h>
#include <MetaData/Scope.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define METADATA_PARAMETER_LIST_LENGTH		32									///< Parameter list length number (must be in decimal to allow preprocessor triks)

enum MetadataParameterTypeInfo
{
	METADATA_PARAMETER_TYPE_NULL			= 0x00,								///< Performs no operation
	METADATA_PARAMETER_TYPE_BY_VALUE		= 0x01,								///< Copy the resource in a new instance
	METADATA_PARAMETER_TYPE_BY_REFERENCE	= 0x02,								///< Copy the resource index (the reference)

	METADATA_PARAMETER_TYPE_SIZE,

	METADATA_PARAMETER_TYPE_INVALID			= 0xFFFFFFFFUL
};

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define MetadataParameterSignatureListDecl(Name, Builder) \
	MetadataParameterSignature Name[METADATA_PARAMETER_LIST_LENGTH] = { \
		PREPROCESSOR_REPEAT_COMMA_VARIDIC(METADATA_PARAMETER_LIST_LENGTH, { METADATA_PARAMETER_TYPE_NULL, METADATA_STORAGE_RESOURCE_INVALID, METADATA_STORAGE_INDEX_INVALID, Builder }) \
	}

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct MetadataParameterSignatureType;
typedef struct MetadataParameterSignatureType MetadataParameterSignature;

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef uinteger						    MetadataParameterType;				///< Index refering to implicit array of parameter list
typedef uinteger							MetadataParameterTypeInfo;			///< Index refering to MetadataParameterTypeInfo
typedef char *								MetadataParameterNameType;			///< Parameter name front-end type

typedef uinteger							MetadataParameterListCountType;		///< Counter for parameter list implicit array
typedef struct MetadataParameterListType *	MetadataParameterListTypeImpl;		///< Reference to real type for populating size for compiler
typedef MetadataStorageIndexType			MetadataParameterListType;			///< Parameter list index front-end

typedef MetadataParameterSignature *	    (*MetadataParameterSignatureFunc)(MetadataParameterSignature *, MetadataParameterTypeInfo, MetadataStorageResourceType, MetadataStorageIndexType);

typedef MetadataParameterSignature *	    MetadataParameterSignatureList;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct MetadataParameterSignatureType
{
	MetadataParameterTypeInfo				InfoType;							///< Mode in which parameter is by-passed
	MetadataStorageResourceType				ResourceType;						///< Type of signature's data
	MetadataStorageIndexType				Signature;							///< Resource signature
	MetadataParameterSignatureFunc			Build;								///< Builder pattern to allow curring with signature creation

};

/*
typedef struct MetadataParameterListTypeHolder
{
	MetadataParameterListType Index;

	void (*Create)(MetadataParameterListType, MetadataParameterListCountType, ...);

	void (*Destroy)(MetadataParameterListType);

} MetadataParameterListTypeHolder;
*/

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Set a parameter signature type
////////////////////////////////////////////////////////////
MetadataParameterSignature * MetadataParameterSignatureInitialize(MetadataParameterSignature * ParameterSignature, MetadataParameterTypeInfo InfoType, MetadataStorageResourceType ResourceType, MetadataStorageIndexType Signature);

////////////////////////////////////////////////////////////
/// Create a parameter list
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataParameterListCreate(MetadataScopeType Scope, MetadataParameterListCountType Count);

////////////////////////////////////////////////////////////
/// Copy a parameter list
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataParameterListCopy(MetadataScopeType Scope, MetadataParameterListType ParameterList);

////////////////////////////////////////////////////////////
/// Get size of parameter list
////////////////////////////////////////////////////////////
MetadataParameterListCountType MetadataParameterListSize(MetadataScopeType Scope, MetadataParameterListType ParameterList);

////////////////////////////////////////////////////////////
/// Get the info type of specified parameter
////////////////////////////////////////////////////////////
MetadataParameterTypeInfo MetadataParameterListSignatureInfoType(MetadataScopeType Scope, MetadataParameterListType ParameterList, MetadataParameterType Parameter);

////////////////////////////////////////////////////////////
/// Get the resource type of specified parameter
////////////////////////////////////////////////////////////
MetadataStorageResourceType MetadataParameterListSignatureResourceType(MetadataScopeType Scope, MetadataParameterListType ParameterList, MetadataParameterType Parameter);

////////////////////////////////////////////////////////////
/// Get the index which parameter refers to
////////////////////////////////////////////////////////////
MetadataStorageIndexType MetadataParameterListGetParameterIndex(MetadataScopeType Scope, MetadataParameterListType ParameterList, MetadataParameterType Parameter);

////////////////////////////////////////////////////////////
/// Define a parameter as a signature
////////////////////////////////////////////////////////////
bool MetadataParameterListDefine(MetadataScopeType Scope, MetadataParameterListType Signature, MetadataParameterType Parameter, MetadataParameterTypeInfo InfoType, MetadataStorageResourceType ResourceType, MetadataStorageIndexType Index);

////////////////////////////////////////////////////////////
/// Destroy a parameter (slot is not removed from list)
////////////////////////////////////////////////////////////
bool MetadataParameterListUndefine(MetadataScopeType Scope, MetadataParameterListType ParameterList, MetadataParameterType Parameter);

////////////////////////////////////////////////////////////
/// Creates a signature
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataParameterListBuildSignature(MetadataScopeType Scope, MetadataParameterListCountType Count, MetadataParameterSignature * ParameterSignatureList);

////////////////////////////////////////////////////////////
/// Copy a parameter list filled by given parameters
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataParameterListGenerate(MetadataScopeType Scope, MetadataParameterListType Signature, VariableArgumentList * Arguments);

////////////////////////////////////////////////////////////
/// Destroy a parameter list
////////////////////////////////////////////////////////////
void MetadataParameterListDestroy(MetadataScopeType Scope, MetadataParameterListType ParameterList);

#endif // METADATA_PARAMETER_H
