/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GAME_MOVEMENT_H
#define GAME_MOVEMENT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Vector.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define MOVEMENT_KEY_NONE		0x00
#define MOVEMENT_KEY_UP			0x01 << 0x00
#define MOVEMENT_KEY_DOWN		0x01 << 0x01
#define MOVEMENT_KEY_LEFT		0x01 << 0x02
#define MOVEMENT_KEY_RIGHT		0x01 << 0x03
#define MOVEMENT_KEY_NEAR		0x01 << 0x04
#define MOVEMENT_KEY_FAR		0x01 << 0x05

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get next movement from input and previous movement
////////////////////////////////////////////////////////////
uint32 CharacterMove(uint32 Previous, uint8 Key, bool Pressed);

////////////////////////////////////////////////////////////
/// Obtain entity spatial properties from movement type
////////////////////////////////////////////////////////////
void CharacterMoveApply(uint32 Status, struct Vector3f * Velocity,
						struct Vector3f * Orientation, struct Vector3f * Rotation);

#endif // GAME_MOVEMENT_H
