# Compiler flags...
CPP_COMPILER = g++
C_COMPILER = gcc

# Include paths...
Debug_Include_Path=
Release_Include_Path=
DebugXP_Include_Path=
ReleaseXP_Include_Path=

# Library paths...
Debug_Library_Path=
Release_Library_Path=
DebugXP_Library_Path=
ReleaseXP_Library_Path=

# Additional libraries...
Debug_Libraries=-Wl,--start-group -lOpenGL32 -lGlu32 -lOpenAL32 -llibsndfile-1 -lwsock32 -lfreetype244 -lvfw32 -lpdh  -Wl,--end-group
Release_Libraries=-Wl,--start-group -lOpenGL32 -lGlu32 -lOpenAL32 -llibsndfile-1 -lwsock32 -lfreetype244 -lvfw32 -lpdh  -Wl,--end-group
DebugXP_Libraries=-Wl,--start-group -lOpenGL32 -lGlu32 -lOpenAL32 -llibsndfile-1 -lwsock32 -lfreetype244 -lvfw32 -lpdh  -Wl,--end-group
ReleaseXP_Libraries=-Wl,--start-group -lOpenGL32 -lGlu32 -lOpenAL32 -llibsndfile-1 -lwsock32 -lfreetype244 -lvfw32 -lpdh  -Wl,--end-group

# Preprocessor definitions...
Debug_Preprocessor_Definitions=-D GCC_BUILD -D _DEBUG -D _CONSOLE -D _CRT_SECURE_NO_DEPRECATE -D _CRT_SECURE_NO_WARNINGS 
Release_Preprocessor_Definitions=-D GCC_BUILD -D NDEBUG -D _CONSOLE -D _CRT_SECURE_NO_DEPRECATE -D _CRT_SECURE_NO_WARNINGS 
DebugXP_Preprocessor_Definitions=-D GCC_BUILD -D _DEBUG -D _CONSOLE -D _CRT_SECURE_NO_DEPRECATE -D _CRT_SECURE_NO_WARNINGS 
ReleaseXP_Preprocessor_Definitions=-D GCC_BUILD -D NDEBUG -D _CONSOLE -D _CRT_SECURE_NO_DEPRECATE -D _CRT_SECURE_NO_WARNINGS 

# Implictly linked object files...
Debug_Implicitly_Linked_Objects=
Release_Implicitly_Linked_Objects=
DebugXP_Implicitly_Linked_Objects=
ReleaseXP_Implicitly_Linked_Objects=

# Compiler flags...
Debug_Compiler_Flags=-O0 -g 
Release_Compiler_Flags=-O2 
DebugXP_Compiler_Flags=-O0 -g 
ReleaseXP_Compiler_Flags=-O2 

# Builds all configurations for this project...
.PHONY: build_all_configurations
build_all_configurations: Debug Release DebugXP ReleaseXP 

# Builds the Debug configuration...
.PHONY: Debug
Debug: create_folders ../bin/gccDebug/../src/main.o ../bin/gccDebug/../../common/src/Audio/OpenALHelper.o ../bin/gccDebug/../../common/src/Audio/Sound.o ../bin/gccDebug/../../common/src/Audio/SoundLoader.o ../bin/gccDebug/../../common/src/Audio/SoundManager.o ../bin/gccDebug/../../common/src/Window/Settings.o ../bin/gccDebug/../../common/src/Window/Win32/VideoModeSupport.o ../bin/gccDebug/../../common/src/Window/Win32/Window.o ../bin/gccDebug/../../common/src/System/CPUInfo.o ../bin/gccDebug/../../common/src/System/Dictionary.o ../bin/gccDebug/../../common/src/System/Error.o ../bin/gccDebug/../../common/src/System/MD5.o ../bin/gccDebug/../../common/src/System/Pack.o ../bin/gccDebug/../../common/src/System/Parser.o ../bin/gccDebug/../../common/src/System/Timer.o ../bin/gccDebug/../../common/src/System/TimerManager.o ../bin/gccDebug/../../common/src/System/Win32/IOHelper.o ../bin/gccDebug/../../common/src/System/Win32/Mutex.o ../bin/gccDebug/../../common/src/System/Win32/Platform.o ../bin/gccDebug/../../common/src/System/Win32/Thread.o ../bin/gccDebug/../../common/src/System/String/bstrlib.o ../bin/gccDebug/../../common/src/System/String/Lexer.o ../bin/gccDebug/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccDebug/../../common/src/System/DataTypes/Bitset.o ../bin/gccDebug/../../common/src/System/Memory/Allocator.o ../bin/gccDebug/../../common/src/System/Memory/Pool.o ../bin/gccDebug/../src/Network/Protocol.o ../bin/gccDebug/../../common/src/Network/Socket.o ../bin/gccDebug/../../common/src/Network/Win32/SocketHelper.o ../bin/gccDebug/../src/Game/Application.o ../bin/gccDebug/../src/Game/Character.o ../bin/gccDebug/../src/Game/Configuration.o ../bin/gccDebug/../src/Game/Display.o ../bin/gccDebug/../src/Game/Index.o ../bin/gccDebug/../src/Game/Intro.o ../bin/gccDebug/../src/Game/Language.o ../bin/gccDebug/../src/Game/Login.o ../bin/gccDebug/../src/Game/User.o ../bin/gccDebug/../../common/src/Math/General.o ../bin/gccDebug/../../common/src/Math/Geometry/Matrix.o ../bin/gccDebug/../../common/src/Math/Geometry/Plane.o ../bin/gccDebug/../../common/src/Math/Geometry/Quaternion.o ../bin/gccDebug/../../common/src/Math/Geometry/Rect.o ../bin/gccDebug/../../common/src/Math/Geometry/Vector.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Sphere.o ../bin/gccDebug/../../common/src/Gui/Button.o ../bin/gccDebug/../../common/src/Gui/Clipboard.o ../bin/gccDebug/../../common/src/Gui/EventHandler.o ../bin/gccDebug/../../common/src/Gui/Form.o ../bin/gccDebug/../../common/src/Gui/Label.o ../bin/gccDebug/../../common/src/Gui/List.o ../bin/gccDebug/../../common/src/Gui/TextBox.o ../bin/gccDebug/../../common/src/Gui/Widget.o ../bin/gccDebug/../../common/src/Gui/WindowManager.o ../bin/gccDebug/../../common/src/Gui/WindowObj.o ../bin/gccDebug/../../common/src/Gui/WindowParser.o ../bin/gccDebug/../src/Scene/Camera.o ../bin/gccDebug/../src/Scene/Entity.o ../bin/gccDebug/../src/Scene/EntityManager.o ../bin/gccDebug/../src/Scene/Light.o ../bin/gccDebug/../src/Scene/Model.o ../bin/gccDebug/../src/Scene/ModelLoader.o ../bin/gccDebug/../src/Scene/ModelManager.o ../bin/gccDebug/../src/Scene/Octree.o ../bin/gccDebug/../src/Scene/Particle.o ../bin/gccDebug/../src/Scene/Patch.o ../bin/gccDebug/../src/Scene/Point.o ../bin/gccDebug/../src/Scene/SceneManager.o ../bin/gccDebug/../src/Scene/Sky.o ../bin/gccDebug/../src/Scene/Terrain.o ../bin/gccDebug/../src/Scene/TerrainLoader.o ../bin/gccDebug/../src/Scene/TerrainManager.o ../bin/gccDebug/../../common/src/Graphics/Color.o ../bin/gccDebug/../../common/src/Graphics/Coordinates.o ../bin/gccDebug/../../common/src/Graphics/Device.o ../bin/gccDebug/../../common/src/Graphics/Fog.o ../bin/gccDebug/../../common/src/Graphics/Font.o ../bin/gccDebug/../../common/src/Graphics/IndexBuffer.o ../bin/gccDebug/../../common/src/Graphics/MatrixStack.o ../bin/gccDebug/../../common/src/Graphics/Shader.o ../bin/gccDebug/../../common/src/Graphics/ShaderLoader.o ../bin/gccDebug/../../common/src/Graphics/ShaderManager.o ../bin/gccDebug/../../common/src/Graphics/StatesManager.o ../bin/gccDebug/../../common/src/Graphics/Texture.o ../bin/gccDebug/../../common/src/Graphics/TextureHelper.o ../bin/gccDebug/../../common/src/Graphics/TextureLoader.o ../bin/gccDebug/../../common/src/Graphics/TextureManager.o ../bin/gccDebug/../../common/src/Graphics/TextureRender.o ../bin/gccDebug/../../common/src/Graphics/VertexBuffer.o ../bin/gccDebug/../../common/src/Graphics/VertexFormat.o ../bin/gccDebug/../../common/src/Graphics/VertexTypes.o ../bin/gccDebug/../../common/src/Graphics/View.o ../bin/gccDebug/../../common/src/Graphics/Win32/VideoRender.o 
	g++ ../bin/gccDebug/../src/main.o ../bin/gccDebug/../../common/src/Audio/OpenALHelper.o ../bin/gccDebug/../../common/src/Audio/Sound.o ../bin/gccDebug/../../common/src/Audio/SoundLoader.o ../bin/gccDebug/../../common/src/Audio/SoundManager.o ../bin/gccDebug/../../common/src/Window/Settings.o ../bin/gccDebug/../../common/src/Window/Win32/VideoModeSupport.o ../bin/gccDebug/../../common/src/Window/Win32/Window.o ../bin/gccDebug/../../common/src/System/CPUInfo.o ../bin/gccDebug/../../common/src/System/Dictionary.o ../bin/gccDebug/../../common/src/System/Error.o ../bin/gccDebug/../../common/src/System/MD5.o ../bin/gccDebug/../../common/src/System/Pack.o ../bin/gccDebug/../../common/src/System/Parser.o ../bin/gccDebug/../../common/src/System/Timer.o ../bin/gccDebug/../../common/src/System/TimerManager.o ../bin/gccDebug/../../common/src/System/Win32/IOHelper.o ../bin/gccDebug/../../common/src/System/Win32/Mutex.o ../bin/gccDebug/../../common/src/System/Win32/Platform.o ../bin/gccDebug/../../common/src/System/Win32/Thread.o ../bin/gccDebug/../../common/src/System/String/bstrlib.o ../bin/gccDebug/../../common/src/System/String/Lexer.o ../bin/gccDebug/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccDebug/../../common/src/System/DataTypes/Bitset.o ../bin/gccDebug/../../common/src/System/Memory/Allocator.o ../bin/gccDebug/../../common/src/System/Memory/Pool.o ../bin/gccDebug/../src/Network/Protocol.o ../bin/gccDebug/../../common/src/Network/Socket.o ../bin/gccDebug/../../common/src/Network/Win32/SocketHelper.o ../bin/gccDebug/../src/Game/Application.o ../bin/gccDebug/../src/Game/Character.o ../bin/gccDebug/../src/Game/Configuration.o ../bin/gccDebug/../src/Game/Display.o ../bin/gccDebug/../src/Game/Index.o ../bin/gccDebug/../src/Game/Intro.o ../bin/gccDebug/../src/Game/Language.o ../bin/gccDebug/../src/Game/Login.o ../bin/gccDebug/../src/Game/User.o ../bin/gccDebug/../../common/src/Math/General.o ../bin/gccDebug/../../common/src/Math/Geometry/Matrix.o ../bin/gccDebug/../../common/src/Math/Geometry/Plane.o ../bin/gccDebug/../../common/src/Math/Geometry/Quaternion.o ../bin/gccDebug/../../common/src/Math/Geometry/Rect.o ../bin/gccDebug/../../common/src/Math/Geometry/Vector.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Sphere.o ../bin/gccDebug/../../common/src/Gui/Button.o ../bin/gccDebug/../../common/src/Gui/Clipboard.o ../bin/gccDebug/../../common/src/Gui/EventHandler.o ../bin/gccDebug/../../common/src/Gui/Form.o ../bin/gccDebug/../../common/src/Gui/Label.o ../bin/gccDebug/../../common/src/Gui/List.o ../bin/gccDebug/../../common/src/Gui/TextBox.o ../bin/gccDebug/../../common/src/Gui/Widget.o ../bin/gccDebug/../../common/src/Gui/WindowManager.o ../bin/gccDebug/../../common/src/Gui/WindowObj.o ../bin/gccDebug/../../common/src/Gui/WindowParser.o ../bin/gccDebug/../src/Scene/Camera.o ../bin/gccDebug/../src/Scene/Entity.o ../bin/gccDebug/../src/Scene/EntityManager.o ../bin/gccDebug/../src/Scene/Light.o ../bin/gccDebug/../src/Scene/Model.o ../bin/gccDebug/../src/Scene/ModelLoader.o ../bin/gccDebug/../src/Scene/ModelManager.o ../bin/gccDebug/../src/Scene/Octree.o ../bin/gccDebug/../src/Scene/Particle.o ../bin/gccDebug/../src/Scene/Patch.o ../bin/gccDebug/../src/Scene/Point.o ../bin/gccDebug/../src/Scene/SceneManager.o ../bin/gccDebug/../src/Scene/Sky.o ../bin/gccDebug/../src/Scene/Terrain.o ../bin/gccDebug/../src/Scene/TerrainLoader.o ../bin/gccDebug/../src/Scene/TerrainManager.o ../bin/gccDebug/../../common/src/Graphics/Color.o ../bin/gccDebug/../../common/src/Graphics/Coordinates.o ../bin/gccDebug/../../common/src/Graphics/Device.o ../bin/gccDebug/../../common/src/Graphics/Fog.o ../bin/gccDebug/../../common/src/Graphics/Font.o ../bin/gccDebug/../../common/src/Graphics/IndexBuffer.o ../bin/gccDebug/../../common/src/Graphics/MatrixStack.o ../bin/gccDebug/../../common/src/Graphics/Shader.o ../bin/gccDebug/../../common/src/Graphics/ShaderLoader.o ../bin/gccDebug/../../common/src/Graphics/ShaderManager.o ../bin/gccDebug/../../common/src/Graphics/StatesManager.o ../bin/gccDebug/../../common/src/Graphics/Texture.o ../bin/gccDebug/../../common/src/Graphics/TextureHelper.o ../bin/gccDebug/../../common/src/Graphics/TextureLoader.o ../bin/gccDebug/../../common/src/Graphics/TextureManager.o ../bin/gccDebug/../../common/src/Graphics/TextureRender.o ../bin/gccDebug/../../common/src/Graphics/VertexBuffer.o ../bin/gccDebug/../../common/src/Graphics/VertexFormat.o ../bin/gccDebug/../../common/src/Graphics/VertexTypes.o ../bin/gccDebug/../../common/src/Graphics/View.o ../bin/gccDebug/../../common/src/Graphics/Win32/VideoRender.o  $(Debug_Library_Path) $(Debug_Libraries) -Wl,-rpath,./ -o ../bin/gccDebug/Client.exe

# Compiles file ../src/main.c for the Debug configuration...
-include ../bin/gccDebug/../src/main.d
../bin/gccDebug/../src/main.o: ../src/main.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/main.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/main.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/main.c $(Debug_Include_Path) > ../bin/gccDebug/../src/main.d

# Compiles file ../../common/src/Audio/OpenALHelper.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Audio/OpenALHelper.d
../bin/gccDebug/../../common/src/Audio/OpenALHelper.o: ../../common/src/Audio/OpenALHelper.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Audio/OpenALHelper.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Audio/OpenALHelper.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Audio/OpenALHelper.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Audio/OpenALHelper.d

# Compiles file ../../common/src/Audio/Sound.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Audio/Sound.d
../bin/gccDebug/../../common/src/Audio/Sound.o: ../../common/src/Audio/Sound.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Audio/Sound.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Audio/Sound.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Audio/Sound.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Audio/Sound.d

# Compiles file ../../common/src/Audio/SoundLoader.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Audio/SoundLoader.d
../bin/gccDebug/../../common/src/Audio/SoundLoader.o: ../../common/src/Audio/SoundLoader.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Audio/SoundLoader.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Audio/SoundLoader.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Audio/SoundLoader.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Audio/SoundLoader.d

# Compiles file ../../common/src/Audio/SoundManager.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Audio/SoundManager.d
../bin/gccDebug/../../common/src/Audio/SoundManager.o: ../../common/src/Audio/SoundManager.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Audio/SoundManager.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Audio/SoundManager.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Audio/SoundManager.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Audio/SoundManager.d

# Compiles file ../../common/src/Window/Settings.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Window/Settings.d
../bin/gccDebug/../../common/src/Window/Settings.o: ../../common/src/Window/Settings.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Window/Settings.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Window/Settings.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Window/Settings.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Window/Settings.d

# Compiles file ../../common/src/Window/Win32/VideoModeSupport.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Window/Win32/VideoModeSupport.d
../bin/gccDebug/../../common/src/Window/Win32/VideoModeSupport.o: ../../common/src/Window/Win32/VideoModeSupport.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Window/Win32/VideoModeSupport.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Window/Win32/VideoModeSupport.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Window/Win32/VideoModeSupport.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Window/Win32/VideoModeSupport.d

# Compiles file ../../common/src/Window/Win32/Window.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Window/Win32/Window.d
../bin/gccDebug/../../common/src/Window/Win32/Window.o: ../../common/src/Window/Win32/Window.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Window/Win32/Window.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Window/Win32/Window.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Window/Win32/Window.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Window/Win32/Window.d

# Compiles file ../../common/src/System/CPUInfo.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/CPUInfo.d
../bin/gccDebug/../../common/src/System/CPUInfo.o: ../../common/src/System/CPUInfo.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/CPUInfo.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/CPUInfo.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/CPUInfo.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/CPUInfo.d

# Compiles file ../../common/src/System/Dictionary.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Dictionary.d
../bin/gccDebug/../../common/src/System/Dictionary.o: ../../common/src/System/Dictionary.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Dictionary.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Dictionary.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Dictionary.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Dictionary.d

# Compiles file ../../common/src/System/Error.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Error.d
../bin/gccDebug/../../common/src/System/Error.o: ../../common/src/System/Error.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Error.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Error.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Error.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Error.d

# Compiles file ../../common/src/System/MD5.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/MD5.d
../bin/gccDebug/../../common/src/System/MD5.o: ../../common/src/System/MD5.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/MD5.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/MD5.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/MD5.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/MD5.d

# Compiles file ../../common/src/System/Pack.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Pack.d
../bin/gccDebug/../../common/src/System/Pack.o: ../../common/src/System/Pack.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Pack.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Pack.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Pack.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Pack.d

# Compiles file ../../common/src/System/Parser.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Parser.d
../bin/gccDebug/../../common/src/System/Parser.o: ../../common/src/System/Parser.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Parser.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Parser.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Parser.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Parser.d

# Compiles file ../../common/src/System/Timer.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Timer.d
../bin/gccDebug/../../common/src/System/Timer.o: ../../common/src/System/Timer.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Timer.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Timer.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Timer.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Timer.d

# Compiles file ../../common/src/System/TimerManager.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/TimerManager.d
../bin/gccDebug/../../common/src/System/TimerManager.o: ../../common/src/System/TimerManager.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/TimerManager.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/TimerManager.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/TimerManager.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/TimerManager.d

# Compiles file ../../common/src/System/Win32/IOHelper.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Win32/IOHelper.d
../bin/gccDebug/../../common/src/System/Win32/IOHelper.o: ../../common/src/System/Win32/IOHelper.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Win32/IOHelper.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Win32/IOHelper.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Win32/IOHelper.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Win32/IOHelper.d

# Compiles file ../../common/src/System/Win32/Mutex.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Win32/Mutex.d
../bin/gccDebug/../../common/src/System/Win32/Mutex.o: ../../common/src/System/Win32/Mutex.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Win32/Mutex.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Win32/Mutex.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Win32/Mutex.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Win32/Mutex.d

# Compiles file ../../common/src/System/Win32/Platform.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Win32/Platform.d
../bin/gccDebug/../../common/src/System/Win32/Platform.o: ../../common/src/System/Win32/Platform.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Win32/Platform.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Win32/Platform.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Win32/Platform.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Win32/Platform.d

# Compiles file ../../common/src/System/Win32/Thread.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Win32/Thread.d
../bin/gccDebug/../../common/src/System/Win32/Thread.o: ../../common/src/System/Win32/Thread.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Win32/Thread.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Win32/Thread.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Win32/Thread.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Win32/Thread.d

# Compiles file ../../common/src/System/String/bstrlib.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/String/bstrlib.d
../bin/gccDebug/../../common/src/System/String/bstrlib.o: ../../common/src/System/String/bstrlib.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/String/bstrlib.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/String/bstrlib.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/String/bstrlib.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/String/bstrlib.d

# Compiles file ../../common/src/System/String/Lexer.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/String/Lexer.d
../bin/gccDebug/../../common/src/System/String/Lexer.o: ../../common/src/System/String/Lexer.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/String/Lexer.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/String/Lexer.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/String/Lexer.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/String/Lexer.d

# Compiles file ../../common/src/System/DataTypes/BinaryTree.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/DataTypes/BinaryTree.d
../bin/gccDebug/../../common/src/System/DataTypes/BinaryTree.o: ../../common/src/System/DataTypes/BinaryTree.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/DataTypes/BinaryTree.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/DataTypes/BinaryTree.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/DataTypes/BinaryTree.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/DataTypes/BinaryTree.d

# Compiles file ../../common/src/System/DataTypes/Bitset.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/DataTypes/Bitset.d
../bin/gccDebug/../../common/src/System/DataTypes/Bitset.o: ../../common/src/System/DataTypes/Bitset.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/DataTypes/Bitset.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/DataTypes/Bitset.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/DataTypes/Bitset.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/DataTypes/Bitset.d

# Compiles file ../../common/src/System/Memory/Allocator.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Memory/Allocator.d
../bin/gccDebug/../../common/src/System/Memory/Allocator.o: ../../common/src/System/Memory/Allocator.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Memory/Allocator.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Memory/Allocator.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Memory/Allocator.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Memory/Allocator.d

# Compiles file ../../common/src/System/Memory/Pool.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Memory/Pool.d
../bin/gccDebug/../../common/src/System/Memory/Pool.o: ../../common/src/System/Memory/Pool.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Memory/Pool.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Memory/Pool.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Memory/Pool.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Memory/Pool.d

# Compiles file ../src/Network/Protocol.c for the Debug configuration...
-include ../bin/gccDebug/../src/Network/Protocol.d
../bin/gccDebug/../src/Network/Protocol.o: ../src/Network/Protocol.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Network/Protocol.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Network/Protocol.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Network/Protocol.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Network/Protocol.d

# Compiles file ../../common/src/Network/Socket.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Network/Socket.d
../bin/gccDebug/../../common/src/Network/Socket.o: ../../common/src/Network/Socket.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Network/Socket.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Network/Socket.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Network/Socket.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Network/Socket.d

# Compiles file ../../common/src/Network/Win32/SocketHelper.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Network/Win32/SocketHelper.d
../bin/gccDebug/../../common/src/Network/Win32/SocketHelper.o: ../../common/src/Network/Win32/SocketHelper.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Network/Win32/SocketHelper.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Network/Win32/SocketHelper.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Network/Win32/SocketHelper.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Network/Win32/SocketHelper.d

# Compiles file ../src/Game/Application.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/Application.d
../bin/gccDebug/../src/Game/Application.o: ../src/Game/Application.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/Application.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/Application.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/Application.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/Application.d

# Compiles file ../src/Game/Character.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/Character.d
../bin/gccDebug/../src/Game/Character.o: ../src/Game/Character.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/Character.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/Character.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/Character.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/Character.d

# Compiles file ../src/Game/Configuration.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/Configuration.d
../bin/gccDebug/../src/Game/Configuration.o: ../src/Game/Configuration.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/Configuration.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/Configuration.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/Configuration.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/Configuration.d

# Compiles file ../src/Game/Display.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/Display.d
../bin/gccDebug/../src/Game/Display.o: ../src/Game/Display.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/Display.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/Display.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/Display.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/Display.d

# Compiles file ../src/Game/Index.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/Index.d
../bin/gccDebug/../src/Game/Index.o: ../src/Game/Index.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/Index.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/Index.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/Index.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/Index.d

# Compiles file ../src/Game/Intro.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/Intro.d
../bin/gccDebug/../src/Game/Intro.o: ../src/Game/Intro.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/Intro.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/Intro.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/Intro.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/Intro.d

# Compiles file ../src/Game/Language.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/Language.d
../bin/gccDebug/../src/Game/Language.o: ../src/Game/Language.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/Language.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/Language.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/Language.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/Language.d

# Compiles file ../src/Game/Login.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/Login.d
../bin/gccDebug/../src/Game/Login.o: ../src/Game/Login.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/Login.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/Login.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/Login.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/Login.d

# Compiles file ../src/Game/User.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/User.d
../bin/gccDebug/../src/Game/User.o: ../src/Game/User.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/User.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/User.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/User.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/User.d

# Compiles file ../../common/src/Math/General.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/General.d
../bin/gccDebug/../../common/src/Math/General.o: ../../common/src/Math/General.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/General.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/General.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/General.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/General.d

# Compiles file ../../common/src/Math/Geometry/Matrix.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Matrix.d
../bin/gccDebug/../../common/src/Math/Geometry/Matrix.o: ../../common/src/Math/Geometry/Matrix.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Matrix.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Matrix.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Matrix.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Matrix.d

# Compiles file ../../common/src/Math/Geometry/Plane.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Plane.d
../bin/gccDebug/../../common/src/Math/Geometry/Plane.o: ../../common/src/Math/Geometry/Plane.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Plane.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Plane.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Plane.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Plane.d

# Compiles file ../../common/src/Math/Geometry/Quaternion.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Quaternion.d
../bin/gccDebug/../../common/src/Math/Geometry/Quaternion.o: ../../common/src/Math/Geometry/Quaternion.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Quaternion.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Quaternion.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Quaternion.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Quaternion.d

# Compiles file ../../common/src/Math/Geometry/Rect.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Rect.d
../bin/gccDebug/../../common/src/Math/Geometry/Rect.o: ../../common/src/Math/Geometry/Rect.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Rect.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Rect.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Rect.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Rect.d

# Compiles file ../../common/src/Math/Geometry/Vector.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Vector.d
../bin/gccDebug/../../common/src/Math/Geometry/Vector.o: ../../common/src/Math/Geometry/Vector.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Vector.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Vector.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Vector.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Vector.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Box.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Box.d
../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Box.o: ../../common/src/Math/Geometry/Bounding/Box.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Box.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Box.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Box.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Box.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Frustum.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Frustum.d
../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Frustum.o: ../../common/src/Math/Geometry/Bounding/Frustum.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Frustum.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Frustum.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Frustum.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Frustum.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Quad.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Quad.d
../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Quad.o: ../../common/src/Math/Geometry/Bounding/Quad.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Quad.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Quad.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Quad.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Quad.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Sphere.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Sphere.d
../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Sphere.o: ../../common/src/Math/Geometry/Bounding/Sphere.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Sphere.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Sphere.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Sphere.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Sphere.d

# Compiles file ../../common/src/Gui/Button.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Gui/Button.d
../bin/gccDebug/../../common/src/Gui/Button.o: ../../common/src/Gui/Button.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Gui/Button.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Gui/Button.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Gui/Button.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Gui/Button.d

# Compiles file ../../common/src/Gui/Clipboard.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Gui/Clipboard.d
../bin/gccDebug/../../common/src/Gui/Clipboard.o: ../../common/src/Gui/Clipboard.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Gui/Clipboard.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Gui/Clipboard.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Gui/Clipboard.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Gui/Clipboard.d

# Compiles file ../../common/src/Gui/EventHandler.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Gui/EventHandler.d
../bin/gccDebug/../../common/src/Gui/EventHandler.o: ../../common/src/Gui/EventHandler.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Gui/EventHandler.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Gui/EventHandler.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Gui/EventHandler.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Gui/EventHandler.d

# Compiles file ../../common/src/Gui/Form.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Gui/Form.d
../bin/gccDebug/../../common/src/Gui/Form.o: ../../common/src/Gui/Form.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Gui/Form.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Gui/Form.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Gui/Form.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Gui/Form.d

# Compiles file ../../common/src/Gui/Label.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Gui/Label.d
../bin/gccDebug/../../common/src/Gui/Label.o: ../../common/src/Gui/Label.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Gui/Label.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Gui/Label.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Gui/Label.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Gui/Label.d

# Compiles file ../../common/src/Gui/List.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Gui/List.d
../bin/gccDebug/../../common/src/Gui/List.o: ../../common/src/Gui/List.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Gui/List.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Gui/List.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Gui/List.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Gui/List.d

# Compiles file ../../common/src/Gui/TextBox.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Gui/TextBox.d
../bin/gccDebug/../../common/src/Gui/TextBox.o: ../../common/src/Gui/TextBox.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Gui/TextBox.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Gui/TextBox.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Gui/TextBox.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Gui/TextBox.d

# Compiles file ../../common/src/Gui/Widget.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Gui/Widget.d
../bin/gccDebug/../../common/src/Gui/Widget.o: ../../common/src/Gui/Widget.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Gui/Widget.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Gui/Widget.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Gui/Widget.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Gui/Widget.d

# Compiles file ../../common/src/Gui/WindowManager.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Gui/WindowManager.d
../bin/gccDebug/../../common/src/Gui/WindowManager.o: ../../common/src/Gui/WindowManager.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Gui/WindowManager.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Gui/WindowManager.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Gui/WindowManager.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Gui/WindowManager.d

# Compiles file ../../common/src/Gui/WindowObj.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Gui/WindowObj.d
../bin/gccDebug/../../common/src/Gui/WindowObj.o: ../../common/src/Gui/WindowObj.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Gui/WindowObj.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Gui/WindowObj.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Gui/WindowObj.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Gui/WindowObj.d

# Compiles file ../../common/src/Gui/WindowParser.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Gui/WindowParser.d
../bin/gccDebug/../../common/src/Gui/WindowParser.o: ../../common/src/Gui/WindowParser.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Gui/WindowParser.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Gui/WindowParser.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Gui/WindowParser.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Gui/WindowParser.d

# Compiles file ../src/Scene/Camera.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/Camera.d
../bin/gccDebug/../src/Scene/Camera.o: ../src/Scene/Camera.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/Camera.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/Camera.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/Camera.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/Camera.d

# Compiles file ../src/Scene/Entity.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/Entity.d
../bin/gccDebug/../src/Scene/Entity.o: ../src/Scene/Entity.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/Entity.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/Entity.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/Entity.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/Entity.d

# Compiles file ../src/Scene/EntityManager.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/EntityManager.d
../bin/gccDebug/../src/Scene/EntityManager.o: ../src/Scene/EntityManager.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/EntityManager.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/EntityManager.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/EntityManager.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/EntityManager.d

# Compiles file ../src/Scene/Light.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/Light.d
../bin/gccDebug/../src/Scene/Light.o: ../src/Scene/Light.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/Light.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/Light.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/Light.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/Light.d

# Compiles file ../src/Scene/Model.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/Model.d
../bin/gccDebug/../src/Scene/Model.o: ../src/Scene/Model.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/Model.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/Model.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/Model.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/Model.d

# Compiles file ../src/Scene/ModelLoader.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/ModelLoader.d
../bin/gccDebug/../src/Scene/ModelLoader.o: ../src/Scene/ModelLoader.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/ModelLoader.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/ModelLoader.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/ModelLoader.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/ModelLoader.d

# Compiles file ../src/Scene/ModelManager.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/ModelManager.d
../bin/gccDebug/../src/Scene/ModelManager.o: ../src/Scene/ModelManager.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/ModelManager.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/ModelManager.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/ModelManager.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/ModelManager.d

# Compiles file ../src/Scene/Octree.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/Octree.d
../bin/gccDebug/../src/Scene/Octree.o: ../src/Scene/Octree.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/Octree.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/Octree.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/Octree.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/Octree.d

# Compiles file ../src/Scene/Particle.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/Particle.d
../bin/gccDebug/../src/Scene/Particle.o: ../src/Scene/Particle.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/Particle.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/Particle.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/Particle.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/Particle.d

# Compiles file ../src/Scene/Patch.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/Patch.d
../bin/gccDebug/../src/Scene/Patch.o: ../src/Scene/Patch.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/Patch.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/Patch.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/Patch.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/Patch.d

# Compiles file ../src/Scene/Point.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/Point.d
../bin/gccDebug/../src/Scene/Point.o: ../src/Scene/Point.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/Point.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/Point.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/Point.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/Point.d

# Compiles file ../src/Scene/SceneManager.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/SceneManager.d
../bin/gccDebug/../src/Scene/SceneManager.o: ../src/Scene/SceneManager.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/SceneManager.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/SceneManager.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/SceneManager.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/SceneManager.d

# Compiles file ../src/Scene/Sky.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/Sky.d
../bin/gccDebug/../src/Scene/Sky.o: ../src/Scene/Sky.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/Sky.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/Sky.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/Sky.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/Sky.d

# Compiles file ../src/Scene/Terrain.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/Terrain.d
../bin/gccDebug/../src/Scene/Terrain.o: ../src/Scene/Terrain.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/Terrain.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/Terrain.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/Terrain.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/Terrain.d

# Compiles file ../src/Scene/TerrainLoader.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/TerrainLoader.d
../bin/gccDebug/../src/Scene/TerrainLoader.o: ../src/Scene/TerrainLoader.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/TerrainLoader.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/TerrainLoader.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/TerrainLoader.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/TerrainLoader.d

# Compiles file ../src/Scene/TerrainManager.c for the Debug configuration...
-include ../bin/gccDebug/../src/Scene/TerrainManager.d
../bin/gccDebug/../src/Scene/TerrainManager.o: ../src/Scene/TerrainManager.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Scene/TerrainManager.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Scene/TerrainManager.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Scene/TerrainManager.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Scene/TerrainManager.d

# Compiles file ../../common/src/Graphics/Color.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/Color.d
../bin/gccDebug/../../common/src/Graphics/Color.o: ../../common/src/Graphics/Color.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/Color.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/Color.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/Color.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/Color.d

# Compiles file ../../common/src/Graphics/Coordinates.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/Coordinates.d
../bin/gccDebug/../../common/src/Graphics/Coordinates.o: ../../common/src/Graphics/Coordinates.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/Coordinates.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/Coordinates.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/Coordinates.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/Coordinates.d

# Compiles file ../../common/src/Graphics/Device.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/Device.d
../bin/gccDebug/../../common/src/Graphics/Device.o: ../../common/src/Graphics/Device.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/Device.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/Device.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/Device.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/Device.d

# Compiles file ../../common/src/Graphics/Fog.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/Fog.d
../bin/gccDebug/../../common/src/Graphics/Fog.o: ../../common/src/Graphics/Fog.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/Fog.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/Fog.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/Fog.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/Fog.d

# Compiles file ../../common/src/Graphics/Font.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/Font.d
../bin/gccDebug/../../common/src/Graphics/Font.o: ../../common/src/Graphics/Font.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/Font.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/Font.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/Font.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/Font.d

# Compiles file ../../common/src/Graphics/IndexBuffer.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/IndexBuffer.d
../bin/gccDebug/../../common/src/Graphics/IndexBuffer.o: ../../common/src/Graphics/IndexBuffer.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/IndexBuffer.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/IndexBuffer.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/IndexBuffer.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/IndexBuffer.d

# Compiles file ../../common/src/Graphics/MatrixStack.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/MatrixStack.d
../bin/gccDebug/../../common/src/Graphics/MatrixStack.o: ../../common/src/Graphics/MatrixStack.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/MatrixStack.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/MatrixStack.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/MatrixStack.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/MatrixStack.d

# Compiles file ../../common/src/Graphics/Shader.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/Shader.d
../bin/gccDebug/../../common/src/Graphics/Shader.o: ../../common/src/Graphics/Shader.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/Shader.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/Shader.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/Shader.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/Shader.d

# Compiles file ../../common/src/Graphics/ShaderLoader.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/ShaderLoader.d
../bin/gccDebug/../../common/src/Graphics/ShaderLoader.o: ../../common/src/Graphics/ShaderLoader.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/ShaderLoader.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/ShaderLoader.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/ShaderLoader.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/ShaderLoader.d

# Compiles file ../../common/src/Graphics/ShaderManager.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/ShaderManager.d
../bin/gccDebug/../../common/src/Graphics/ShaderManager.o: ../../common/src/Graphics/ShaderManager.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/ShaderManager.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/ShaderManager.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/ShaderManager.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/ShaderManager.d

# Compiles file ../../common/src/Graphics/StatesManager.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/StatesManager.d
../bin/gccDebug/../../common/src/Graphics/StatesManager.o: ../../common/src/Graphics/StatesManager.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/StatesManager.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/StatesManager.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/StatesManager.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/StatesManager.d

# Compiles file ../../common/src/Graphics/Texture.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/Texture.d
../bin/gccDebug/../../common/src/Graphics/Texture.o: ../../common/src/Graphics/Texture.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/Texture.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/Texture.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/Texture.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/Texture.d

# Compiles file ../../common/src/Graphics/TextureHelper.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/TextureHelper.d
../bin/gccDebug/../../common/src/Graphics/TextureHelper.o: ../../common/src/Graphics/TextureHelper.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/TextureHelper.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/TextureHelper.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/TextureHelper.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/TextureHelper.d

# Compiles file ../../common/src/Graphics/TextureLoader.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/TextureLoader.d
../bin/gccDebug/../../common/src/Graphics/TextureLoader.o: ../../common/src/Graphics/TextureLoader.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/TextureLoader.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/TextureLoader.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/TextureLoader.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/TextureLoader.d

# Compiles file ../../common/src/Graphics/TextureManager.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/TextureManager.d
../bin/gccDebug/../../common/src/Graphics/TextureManager.o: ../../common/src/Graphics/TextureManager.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/TextureManager.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/TextureManager.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/TextureManager.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/TextureManager.d

# Compiles file ../../common/src/Graphics/TextureRender.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/TextureRender.d
../bin/gccDebug/../../common/src/Graphics/TextureRender.o: ../../common/src/Graphics/TextureRender.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/TextureRender.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/TextureRender.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/TextureRender.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/TextureRender.d

# Compiles file ../../common/src/Graphics/VertexBuffer.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/VertexBuffer.d
../bin/gccDebug/../../common/src/Graphics/VertexBuffer.o: ../../common/src/Graphics/VertexBuffer.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/VertexBuffer.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/VertexBuffer.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/VertexBuffer.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/VertexBuffer.d

# Compiles file ../../common/src/Graphics/VertexFormat.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/VertexFormat.d
../bin/gccDebug/../../common/src/Graphics/VertexFormat.o: ../../common/src/Graphics/VertexFormat.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/VertexFormat.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/VertexFormat.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/VertexFormat.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/VertexFormat.d

# Compiles file ../../common/src/Graphics/VertexTypes.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/VertexTypes.d
../bin/gccDebug/../../common/src/Graphics/VertexTypes.o: ../../common/src/Graphics/VertexTypes.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/VertexTypes.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/VertexTypes.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/VertexTypes.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/VertexTypes.d

# Compiles file ../../common/src/Graphics/View.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/View.d
../bin/gccDebug/../../common/src/Graphics/View.o: ../../common/src/Graphics/View.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/View.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/View.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/View.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/View.d

# Compiles file ../../common/src/Graphics/Win32/VideoRender.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Graphics/Win32/VideoRender.d
../bin/gccDebug/../../common/src/Graphics/Win32/VideoRender.o: ../../common/src/Graphics/Win32/VideoRender.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Graphics/Win32/VideoRender.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Graphics/Win32/VideoRender.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Graphics/Win32/VideoRender.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Graphics/Win32/VideoRender.d

# Builds the Release configuration...
.PHONY: Release
Release: create_folders ../bin/gccRelease/../src/main.o ../bin/gccRelease/../../common/src/Audio/OpenALHelper.o ../bin/gccRelease/../../common/src/Audio/Sound.o ../bin/gccRelease/../../common/src/Audio/SoundLoader.o ../bin/gccRelease/../../common/src/Audio/SoundManager.o ../bin/gccRelease/../../common/src/Window/Settings.o ../bin/gccRelease/../../common/src/Window/Win32/VideoModeSupport.o ../bin/gccRelease/../../common/src/Window/Win32/Window.o ../bin/gccRelease/../../common/src/System/CPUInfo.o ../bin/gccRelease/../../common/src/System/Dictionary.o ../bin/gccRelease/../../common/src/System/Error.o ../bin/gccRelease/../../common/src/System/MD5.o ../bin/gccRelease/../../common/src/System/Pack.o ../bin/gccRelease/../../common/src/System/Parser.o ../bin/gccRelease/../../common/src/System/Timer.o ../bin/gccRelease/../../common/src/System/TimerManager.o ../bin/gccRelease/../../common/src/System/Win32/IOHelper.o ../bin/gccRelease/../../common/src/System/Win32/Mutex.o ../bin/gccRelease/../../common/src/System/Win32/Platform.o ../bin/gccRelease/../../common/src/System/Win32/Thread.o ../bin/gccRelease/../../common/src/System/String/bstrlib.o ../bin/gccRelease/../../common/src/System/String/Lexer.o ../bin/gccRelease/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccRelease/../../common/src/System/DataTypes/Bitset.o ../bin/gccRelease/../../common/src/System/Memory/Allocator.o ../bin/gccRelease/../../common/src/System/Memory/Pool.o ../bin/gccRelease/../src/Network/Protocol.o ../bin/gccRelease/../../common/src/Network/Socket.o ../bin/gccRelease/../../common/src/Network/Win32/SocketHelper.o ../bin/gccRelease/../src/Game/Application.o ../bin/gccRelease/../src/Game/Character.o ../bin/gccRelease/../src/Game/Configuration.o ../bin/gccRelease/../src/Game/Display.o ../bin/gccRelease/../src/Game/Index.o ../bin/gccRelease/../src/Game/Intro.o ../bin/gccRelease/../src/Game/Language.o ../bin/gccRelease/../src/Game/Login.o ../bin/gccRelease/../src/Game/User.o ../bin/gccRelease/../../common/src/Math/General.o ../bin/gccRelease/../../common/src/Math/Geometry/Matrix.o ../bin/gccRelease/../../common/src/Math/Geometry/Plane.o ../bin/gccRelease/../../common/src/Math/Geometry/Quaternion.o ../bin/gccRelease/../../common/src/Math/Geometry/Rect.o ../bin/gccRelease/../../common/src/Math/Geometry/Vector.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Sphere.o ../bin/gccRelease/../../common/src/Gui/Button.o ../bin/gccRelease/../../common/src/Gui/Clipboard.o ../bin/gccRelease/../../common/src/Gui/EventHandler.o ../bin/gccRelease/../../common/src/Gui/Form.o ../bin/gccRelease/../../common/src/Gui/Label.o ../bin/gccRelease/../../common/src/Gui/List.o ../bin/gccRelease/../../common/src/Gui/TextBox.o ../bin/gccRelease/../../common/src/Gui/Widget.o ../bin/gccRelease/../../common/src/Gui/WindowManager.o ../bin/gccRelease/../../common/src/Gui/WindowObj.o ../bin/gccRelease/../../common/src/Gui/WindowParser.o ../bin/gccRelease/../src/Scene/Camera.o ../bin/gccRelease/../src/Scene/Entity.o ../bin/gccRelease/../src/Scene/EntityManager.o ../bin/gccRelease/../src/Scene/Light.o ../bin/gccRelease/../src/Scene/Model.o ../bin/gccRelease/../src/Scene/ModelLoader.o ../bin/gccRelease/../src/Scene/ModelManager.o ../bin/gccRelease/../src/Scene/Octree.o ../bin/gccRelease/../src/Scene/Particle.o ../bin/gccRelease/../src/Scene/Patch.o ../bin/gccRelease/../src/Scene/Point.o ../bin/gccRelease/../src/Scene/SceneManager.o ../bin/gccRelease/../src/Scene/Sky.o ../bin/gccRelease/../src/Scene/Terrain.o ../bin/gccRelease/../src/Scene/TerrainLoader.o ../bin/gccRelease/../src/Scene/TerrainManager.o ../bin/gccRelease/../../common/src/Graphics/Color.o ../bin/gccRelease/../../common/src/Graphics/Coordinates.o ../bin/gccRelease/../../common/src/Graphics/Device.o ../bin/gccRelease/../../common/src/Graphics/Fog.o ../bin/gccRelease/../../common/src/Graphics/Font.o ../bin/gccRelease/../../common/src/Graphics/IndexBuffer.o ../bin/gccRelease/../../common/src/Graphics/MatrixStack.o ../bin/gccRelease/../../common/src/Graphics/Shader.o ../bin/gccRelease/../../common/src/Graphics/ShaderLoader.o ../bin/gccRelease/../../common/src/Graphics/ShaderManager.o ../bin/gccRelease/../../common/src/Graphics/StatesManager.o ../bin/gccRelease/../../common/src/Graphics/Texture.o ../bin/gccRelease/../../common/src/Graphics/TextureHelper.o ../bin/gccRelease/../../common/src/Graphics/TextureLoader.o ../bin/gccRelease/../../common/src/Graphics/TextureManager.o ../bin/gccRelease/../../common/src/Graphics/TextureRender.o ../bin/gccRelease/../../common/src/Graphics/VertexBuffer.o ../bin/gccRelease/../../common/src/Graphics/VertexFormat.o ../bin/gccRelease/../../common/src/Graphics/VertexTypes.o ../bin/gccRelease/../../common/src/Graphics/View.o ../bin/gccRelease/../../common/src/Graphics/Win32/VideoRender.o 
	g++ ../bin/gccRelease/../src/main.o ../bin/gccRelease/../../common/src/Audio/OpenALHelper.o ../bin/gccRelease/../../common/src/Audio/Sound.o ../bin/gccRelease/../../common/src/Audio/SoundLoader.o ../bin/gccRelease/../../common/src/Audio/SoundManager.o ../bin/gccRelease/../../common/src/Window/Settings.o ../bin/gccRelease/../../common/src/Window/Win32/VideoModeSupport.o ../bin/gccRelease/../../common/src/Window/Win32/Window.o ../bin/gccRelease/../../common/src/System/CPUInfo.o ../bin/gccRelease/../../common/src/System/Dictionary.o ../bin/gccRelease/../../common/src/System/Error.o ../bin/gccRelease/../../common/src/System/MD5.o ../bin/gccRelease/../../common/src/System/Pack.o ../bin/gccRelease/../../common/src/System/Parser.o ../bin/gccRelease/../../common/src/System/Timer.o ../bin/gccRelease/../../common/src/System/TimerManager.o ../bin/gccRelease/../../common/src/System/Win32/IOHelper.o ../bin/gccRelease/../../common/src/System/Win32/Mutex.o ../bin/gccRelease/../../common/src/System/Win32/Platform.o ../bin/gccRelease/../../common/src/System/Win32/Thread.o ../bin/gccRelease/../../common/src/System/String/bstrlib.o ../bin/gccRelease/../../common/src/System/String/Lexer.o ../bin/gccRelease/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccRelease/../../common/src/System/DataTypes/Bitset.o ../bin/gccRelease/../../common/src/System/Memory/Allocator.o ../bin/gccRelease/../../common/src/System/Memory/Pool.o ../bin/gccRelease/../src/Network/Protocol.o ../bin/gccRelease/../../common/src/Network/Socket.o ../bin/gccRelease/../../common/src/Network/Win32/SocketHelper.o ../bin/gccRelease/../src/Game/Application.o ../bin/gccRelease/../src/Game/Character.o ../bin/gccRelease/../src/Game/Configuration.o ../bin/gccRelease/../src/Game/Display.o ../bin/gccRelease/../src/Game/Index.o ../bin/gccRelease/../src/Game/Intro.o ../bin/gccRelease/../src/Game/Language.o ../bin/gccRelease/../src/Game/Login.o ../bin/gccRelease/../src/Game/User.o ../bin/gccRelease/../../common/src/Math/General.o ../bin/gccRelease/../../common/src/Math/Geometry/Matrix.o ../bin/gccRelease/../../common/src/Math/Geometry/Plane.o ../bin/gccRelease/../../common/src/Math/Geometry/Quaternion.o ../bin/gccRelease/../../common/src/Math/Geometry/Rect.o ../bin/gccRelease/../../common/src/Math/Geometry/Vector.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Sphere.o ../bin/gccRelease/../../common/src/Gui/Button.o ../bin/gccRelease/../../common/src/Gui/Clipboard.o ../bin/gccRelease/../../common/src/Gui/EventHandler.o ../bin/gccRelease/../../common/src/Gui/Form.o ../bin/gccRelease/../../common/src/Gui/Label.o ../bin/gccRelease/../../common/src/Gui/List.o ../bin/gccRelease/../../common/src/Gui/TextBox.o ../bin/gccRelease/../../common/src/Gui/Widget.o ../bin/gccRelease/../../common/src/Gui/WindowManager.o ../bin/gccRelease/../../common/src/Gui/WindowObj.o ../bin/gccRelease/../../common/src/Gui/WindowParser.o ../bin/gccRelease/../src/Scene/Camera.o ../bin/gccRelease/../src/Scene/Entity.o ../bin/gccRelease/../src/Scene/EntityManager.o ../bin/gccRelease/../src/Scene/Light.o ../bin/gccRelease/../src/Scene/Model.o ../bin/gccRelease/../src/Scene/ModelLoader.o ../bin/gccRelease/../src/Scene/ModelManager.o ../bin/gccRelease/../src/Scene/Octree.o ../bin/gccRelease/../src/Scene/Particle.o ../bin/gccRelease/../src/Scene/Patch.o ../bin/gccRelease/../src/Scene/Point.o ../bin/gccRelease/../src/Scene/SceneManager.o ../bin/gccRelease/../src/Scene/Sky.o ../bin/gccRelease/../src/Scene/Terrain.o ../bin/gccRelease/../src/Scene/TerrainLoader.o ../bin/gccRelease/../src/Scene/TerrainManager.o ../bin/gccRelease/../../common/src/Graphics/Color.o ../bin/gccRelease/../../common/src/Graphics/Coordinates.o ../bin/gccRelease/../../common/src/Graphics/Device.o ../bin/gccRelease/../../common/src/Graphics/Fog.o ../bin/gccRelease/../../common/src/Graphics/Font.o ../bin/gccRelease/../../common/src/Graphics/IndexBuffer.o ../bin/gccRelease/../../common/src/Graphics/MatrixStack.o ../bin/gccRelease/../../common/src/Graphics/Shader.o ../bin/gccRelease/../../common/src/Graphics/ShaderLoader.o ../bin/gccRelease/../../common/src/Graphics/ShaderManager.o ../bin/gccRelease/../../common/src/Graphics/StatesManager.o ../bin/gccRelease/../../common/src/Graphics/Texture.o ../bin/gccRelease/../../common/src/Graphics/TextureHelper.o ../bin/gccRelease/../../common/src/Graphics/TextureLoader.o ../bin/gccRelease/../../common/src/Graphics/TextureManager.o ../bin/gccRelease/../../common/src/Graphics/TextureRender.o ../bin/gccRelease/../../common/src/Graphics/VertexBuffer.o ../bin/gccRelease/../../common/src/Graphics/VertexFormat.o ../bin/gccRelease/../../common/src/Graphics/VertexTypes.o ../bin/gccRelease/../../common/src/Graphics/View.o ../bin/gccRelease/../../common/src/Graphics/Win32/VideoRender.o  $(Release_Library_Path) $(Release_Libraries) -Wl,-rpath,./ -o ../bin/gccRelease/Client.exe

# Compiles file ../src/main.c for the Release configuration...
-include ../bin/gccRelease/../src/main.d
../bin/gccRelease/../src/main.o: ../src/main.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/main.c $(Release_Include_Path) -o ../bin/gccRelease/../src/main.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/main.c $(Release_Include_Path) > ../bin/gccRelease/../src/main.d

# Compiles file ../../common/src/Audio/OpenALHelper.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Audio/OpenALHelper.d
../bin/gccRelease/../../common/src/Audio/OpenALHelper.o: ../../common/src/Audio/OpenALHelper.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Audio/OpenALHelper.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Audio/OpenALHelper.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Audio/OpenALHelper.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Audio/OpenALHelper.d

# Compiles file ../../common/src/Audio/Sound.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Audio/Sound.d
../bin/gccRelease/../../common/src/Audio/Sound.o: ../../common/src/Audio/Sound.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Audio/Sound.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Audio/Sound.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Audio/Sound.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Audio/Sound.d

# Compiles file ../../common/src/Audio/SoundLoader.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Audio/SoundLoader.d
../bin/gccRelease/../../common/src/Audio/SoundLoader.o: ../../common/src/Audio/SoundLoader.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Audio/SoundLoader.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Audio/SoundLoader.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Audio/SoundLoader.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Audio/SoundLoader.d

# Compiles file ../../common/src/Audio/SoundManager.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Audio/SoundManager.d
../bin/gccRelease/../../common/src/Audio/SoundManager.o: ../../common/src/Audio/SoundManager.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Audio/SoundManager.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Audio/SoundManager.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Audio/SoundManager.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Audio/SoundManager.d

# Compiles file ../../common/src/Window/Settings.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Window/Settings.d
../bin/gccRelease/../../common/src/Window/Settings.o: ../../common/src/Window/Settings.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Window/Settings.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Window/Settings.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Window/Settings.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Window/Settings.d

# Compiles file ../../common/src/Window/Win32/VideoModeSupport.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Window/Win32/VideoModeSupport.d
../bin/gccRelease/../../common/src/Window/Win32/VideoModeSupport.o: ../../common/src/Window/Win32/VideoModeSupport.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Window/Win32/VideoModeSupport.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Window/Win32/VideoModeSupport.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Window/Win32/VideoModeSupport.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Window/Win32/VideoModeSupport.d

# Compiles file ../../common/src/Window/Win32/Window.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Window/Win32/Window.d
../bin/gccRelease/../../common/src/Window/Win32/Window.o: ../../common/src/Window/Win32/Window.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Window/Win32/Window.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Window/Win32/Window.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Window/Win32/Window.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Window/Win32/Window.d

# Compiles file ../../common/src/System/CPUInfo.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/CPUInfo.d
../bin/gccRelease/../../common/src/System/CPUInfo.o: ../../common/src/System/CPUInfo.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/CPUInfo.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/CPUInfo.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/CPUInfo.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/CPUInfo.d

# Compiles file ../../common/src/System/Dictionary.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Dictionary.d
../bin/gccRelease/../../common/src/System/Dictionary.o: ../../common/src/System/Dictionary.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Dictionary.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Dictionary.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Dictionary.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Dictionary.d

# Compiles file ../../common/src/System/Error.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Error.d
../bin/gccRelease/../../common/src/System/Error.o: ../../common/src/System/Error.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Error.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Error.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Error.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Error.d

# Compiles file ../../common/src/System/MD5.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/MD5.d
../bin/gccRelease/../../common/src/System/MD5.o: ../../common/src/System/MD5.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/MD5.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/MD5.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/MD5.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/MD5.d

# Compiles file ../../common/src/System/Pack.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Pack.d
../bin/gccRelease/../../common/src/System/Pack.o: ../../common/src/System/Pack.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Pack.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Pack.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Pack.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Pack.d

# Compiles file ../../common/src/System/Parser.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Parser.d
../bin/gccRelease/../../common/src/System/Parser.o: ../../common/src/System/Parser.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Parser.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Parser.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Parser.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Parser.d

# Compiles file ../../common/src/System/Timer.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Timer.d
../bin/gccRelease/../../common/src/System/Timer.o: ../../common/src/System/Timer.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Timer.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Timer.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Timer.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Timer.d

# Compiles file ../../common/src/System/TimerManager.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/TimerManager.d
../bin/gccRelease/../../common/src/System/TimerManager.o: ../../common/src/System/TimerManager.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/TimerManager.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/TimerManager.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/TimerManager.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/TimerManager.d

# Compiles file ../../common/src/System/Win32/IOHelper.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Win32/IOHelper.d
../bin/gccRelease/../../common/src/System/Win32/IOHelper.o: ../../common/src/System/Win32/IOHelper.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Win32/IOHelper.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Win32/IOHelper.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Win32/IOHelper.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Win32/IOHelper.d

# Compiles file ../../common/src/System/Win32/Mutex.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Win32/Mutex.d
../bin/gccRelease/../../common/src/System/Win32/Mutex.o: ../../common/src/System/Win32/Mutex.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Win32/Mutex.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Win32/Mutex.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Win32/Mutex.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Win32/Mutex.d

# Compiles file ../../common/src/System/Win32/Platform.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Win32/Platform.d
../bin/gccRelease/../../common/src/System/Win32/Platform.o: ../../common/src/System/Win32/Platform.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Win32/Platform.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Win32/Platform.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Win32/Platform.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Win32/Platform.d

# Compiles file ../../common/src/System/Win32/Thread.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Win32/Thread.d
../bin/gccRelease/../../common/src/System/Win32/Thread.o: ../../common/src/System/Win32/Thread.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Win32/Thread.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Win32/Thread.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Win32/Thread.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Win32/Thread.d

# Compiles file ../../common/src/System/String/bstrlib.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/String/bstrlib.d
../bin/gccRelease/../../common/src/System/String/bstrlib.o: ../../common/src/System/String/bstrlib.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/String/bstrlib.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/String/bstrlib.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/String/bstrlib.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/String/bstrlib.d

# Compiles file ../../common/src/System/String/Lexer.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/String/Lexer.d
../bin/gccRelease/../../common/src/System/String/Lexer.o: ../../common/src/System/String/Lexer.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/String/Lexer.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/String/Lexer.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/String/Lexer.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/String/Lexer.d

# Compiles file ../../common/src/System/DataTypes/BinaryTree.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/DataTypes/BinaryTree.d
../bin/gccRelease/../../common/src/System/DataTypes/BinaryTree.o: ../../common/src/System/DataTypes/BinaryTree.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/DataTypes/BinaryTree.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/DataTypes/BinaryTree.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/DataTypes/BinaryTree.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/DataTypes/BinaryTree.d

# Compiles file ../../common/src/System/DataTypes/Bitset.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/DataTypes/Bitset.d
../bin/gccRelease/../../common/src/System/DataTypes/Bitset.o: ../../common/src/System/DataTypes/Bitset.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/DataTypes/Bitset.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/DataTypes/Bitset.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/DataTypes/Bitset.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/DataTypes/Bitset.d

# Compiles file ../../common/src/System/Memory/Allocator.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Memory/Allocator.d
../bin/gccRelease/../../common/src/System/Memory/Allocator.o: ../../common/src/System/Memory/Allocator.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Memory/Allocator.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Memory/Allocator.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Memory/Allocator.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Memory/Allocator.d

# Compiles file ../../common/src/System/Memory/Pool.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Memory/Pool.d
../bin/gccRelease/../../common/src/System/Memory/Pool.o: ../../common/src/System/Memory/Pool.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Memory/Pool.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Memory/Pool.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Memory/Pool.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Memory/Pool.d

# Compiles file ../src/Network/Protocol.c for the Release configuration...
-include ../bin/gccRelease/../src/Network/Protocol.d
../bin/gccRelease/../src/Network/Protocol.o: ../src/Network/Protocol.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Network/Protocol.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Network/Protocol.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Network/Protocol.c $(Release_Include_Path) > ../bin/gccRelease/../src/Network/Protocol.d

# Compiles file ../../common/src/Network/Socket.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Network/Socket.d
../bin/gccRelease/../../common/src/Network/Socket.o: ../../common/src/Network/Socket.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Network/Socket.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Network/Socket.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Network/Socket.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Network/Socket.d

# Compiles file ../../common/src/Network/Win32/SocketHelper.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Network/Win32/SocketHelper.d
../bin/gccRelease/../../common/src/Network/Win32/SocketHelper.o: ../../common/src/Network/Win32/SocketHelper.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Network/Win32/SocketHelper.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Network/Win32/SocketHelper.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Network/Win32/SocketHelper.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Network/Win32/SocketHelper.d

# Compiles file ../src/Game/Application.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/Application.d
../bin/gccRelease/../src/Game/Application.o: ../src/Game/Application.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/Application.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/Application.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/Application.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/Application.d

# Compiles file ../src/Game/Character.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/Character.d
../bin/gccRelease/../src/Game/Character.o: ../src/Game/Character.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/Character.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/Character.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/Character.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/Character.d

# Compiles file ../src/Game/Configuration.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/Configuration.d
../bin/gccRelease/../src/Game/Configuration.o: ../src/Game/Configuration.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/Configuration.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/Configuration.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/Configuration.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/Configuration.d

# Compiles file ../src/Game/Display.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/Display.d
../bin/gccRelease/../src/Game/Display.o: ../src/Game/Display.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/Display.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/Display.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/Display.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/Display.d

# Compiles file ../src/Game/Index.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/Index.d
../bin/gccRelease/../src/Game/Index.o: ../src/Game/Index.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/Index.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/Index.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/Index.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/Index.d

# Compiles file ../src/Game/Intro.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/Intro.d
../bin/gccRelease/../src/Game/Intro.o: ../src/Game/Intro.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/Intro.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/Intro.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/Intro.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/Intro.d

# Compiles file ../src/Game/Language.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/Language.d
../bin/gccRelease/../src/Game/Language.o: ../src/Game/Language.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/Language.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/Language.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/Language.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/Language.d

# Compiles file ../src/Game/Login.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/Login.d
../bin/gccRelease/../src/Game/Login.o: ../src/Game/Login.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/Login.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/Login.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/Login.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/Login.d

# Compiles file ../src/Game/User.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/User.d
../bin/gccRelease/../src/Game/User.o: ../src/Game/User.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/User.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/User.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/User.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/User.d

# Compiles file ../../common/src/Math/General.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/General.d
../bin/gccRelease/../../common/src/Math/General.o: ../../common/src/Math/General.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/General.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/General.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/General.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/General.d

# Compiles file ../../common/src/Math/Geometry/Matrix.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Matrix.d
../bin/gccRelease/../../common/src/Math/Geometry/Matrix.o: ../../common/src/Math/Geometry/Matrix.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Matrix.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Matrix.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Matrix.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Matrix.d

# Compiles file ../../common/src/Math/Geometry/Plane.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Plane.d
../bin/gccRelease/../../common/src/Math/Geometry/Plane.o: ../../common/src/Math/Geometry/Plane.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Plane.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Plane.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Plane.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Plane.d

# Compiles file ../../common/src/Math/Geometry/Quaternion.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Quaternion.d
../bin/gccRelease/../../common/src/Math/Geometry/Quaternion.o: ../../common/src/Math/Geometry/Quaternion.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Quaternion.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Quaternion.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Quaternion.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Quaternion.d

# Compiles file ../../common/src/Math/Geometry/Rect.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Rect.d
../bin/gccRelease/../../common/src/Math/Geometry/Rect.o: ../../common/src/Math/Geometry/Rect.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Rect.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Rect.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Rect.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Rect.d

# Compiles file ../../common/src/Math/Geometry/Vector.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Vector.d
../bin/gccRelease/../../common/src/Math/Geometry/Vector.o: ../../common/src/Math/Geometry/Vector.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Vector.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Vector.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Vector.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Vector.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Box.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Box.d
../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Box.o: ../../common/src/Math/Geometry/Bounding/Box.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Box.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Box.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Box.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Box.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Frustum.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Frustum.d
../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Frustum.o: ../../common/src/Math/Geometry/Bounding/Frustum.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Frustum.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Frustum.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Frustum.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Frustum.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Quad.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Quad.d
../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Quad.o: ../../common/src/Math/Geometry/Bounding/Quad.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Quad.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Quad.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Quad.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Quad.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Sphere.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Sphere.d
../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Sphere.o: ../../common/src/Math/Geometry/Bounding/Sphere.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Sphere.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Sphere.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Sphere.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Sphere.d

# Compiles file ../../common/src/Gui/Button.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Gui/Button.d
../bin/gccRelease/../../common/src/Gui/Button.o: ../../common/src/Gui/Button.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Gui/Button.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Gui/Button.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Gui/Button.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Gui/Button.d

# Compiles file ../../common/src/Gui/Clipboard.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Gui/Clipboard.d
../bin/gccRelease/../../common/src/Gui/Clipboard.o: ../../common/src/Gui/Clipboard.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Gui/Clipboard.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Gui/Clipboard.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Gui/Clipboard.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Gui/Clipboard.d

# Compiles file ../../common/src/Gui/EventHandler.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Gui/EventHandler.d
../bin/gccRelease/../../common/src/Gui/EventHandler.o: ../../common/src/Gui/EventHandler.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Gui/EventHandler.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Gui/EventHandler.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Gui/EventHandler.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Gui/EventHandler.d

# Compiles file ../../common/src/Gui/Form.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Gui/Form.d
../bin/gccRelease/../../common/src/Gui/Form.o: ../../common/src/Gui/Form.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Gui/Form.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Gui/Form.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Gui/Form.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Gui/Form.d

# Compiles file ../../common/src/Gui/Label.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Gui/Label.d
../bin/gccRelease/../../common/src/Gui/Label.o: ../../common/src/Gui/Label.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Gui/Label.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Gui/Label.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Gui/Label.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Gui/Label.d

# Compiles file ../../common/src/Gui/List.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Gui/List.d
../bin/gccRelease/../../common/src/Gui/List.o: ../../common/src/Gui/List.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Gui/List.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Gui/List.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Gui/List.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Gui/List.d

# Compiles file ../../common/src/Gui/TextBox.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Gui/TextBox.d
../bin/gccRelease/../../common/src/Gui/TextBox.o: ../../common/src/Gui/TextBox.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Gui/TextBox.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Gui/TextBox.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Gui/TextBox.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Gui/TextBox.d

# Compiles file ../../common/src/Gui/Widget.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Gui/Widget.d
../bin/gccRelease/../../common/src/Gui/Widget.o: ../../common/src/Gui/Widget.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Gui/Widget.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Gui/Widget.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Gui/Widget.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Gui/Widget.d

# Compiles file ../../common/src/Gui/WindowManager.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Gui/WindowManager.d
../bin/gccRelease/../../common/src/Gui/WindowManager.o: ../../common/src/Gui/WindowManager.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Gui/WindowManager.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Gui/WindowManager.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Gui/WindowManager.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Gui/WindowManager.d

# Compiles file ../../common/src/Gui/WindowObj.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Gui/WindowObj.d
../bin/gccRelease/../../common/src/Gui/WindowObj.o: ../../common/src/Gui/WindowObj.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Gui/WindowObj.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Gui/WindowObj.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Gui/WindowObj.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Gui/WindowObj.d

# Compiles file ../../common/src/Gui/WindowParser.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Gui/WindowParser.d
../bin/gccRelease/../../common/src/Gui/WindowParser.o: ../../common/src/Gui/WindowParser.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Gui/WindowParser.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Gui/WindowParser.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Gui/WindowParser.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Gui/WindowParser.d

# Compiles file ../src/Scene/Camera.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/Camera.d
../bin/gccRelease/../src/Scene/Camera.o: ../src/Scene/Camera.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/Camera.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/Camera.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/Camera.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/Camera.d

# Compiles file ../src/Scene/Entity.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/Entity.d
../bin/gccRelease/../src/Scene/Entity.o: ../src/Scene/Entity.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/Entity.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/Entity.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/Entity.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/Entity.d

# Compiles file ../src/Scene/EntityManager.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/EntityManager.d
../bin/gccRelease/../src/Scene/EntityManager.o: ../src/Scene/EntityManager.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/EntityManager.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/EntityManager.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/EntityManager.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/EntityManager.d

# Compiles file ../src/Scene/Light.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/Light.d
../bin/gccRelease/../src/Scene/Light.o: ../src/Scene/Light.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/Light.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/Light.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/Light.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/Light.d

# Compiles file ../src/Scene/Model.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/Model.d
../bin/gccRelease/../src/Scene/Model.o: ../src/Scene/Model.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/Model.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/Model.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/Model.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/Model.d

# Compiles file ../src/Scene/ModelLoader.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/ModelLoader.d
../bin/gccRelease/../src/Scene/ModelLoader.o: ../src/Scene/ModelLoader.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/ModelLoader.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/ModelLoader.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/ModelLoader.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/ModelLoader.d

# Compiles file ../src/Scene/ModelManager.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/ModelManager.d
../bin/gccRelease/../src/Scene/ModelManager.o: ../src/Scene/ModelManager.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/ModelManager.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/ModelManager.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/ModelManager.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/ModelManager.d

# Compiles file ../src/Scene/Octree.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/Octree.d
../bin/gccRelease/../src/Scene/Octree.o: ../src/Scene/Octree.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/Octree.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/Octree.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/Octree.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/Octree.d

# Compiles file ../src/Scene/Particle.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/Particle.d
../bin/gccRelease/../src/Scene/Particle.o: ../src/Scene/Particle.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/Particle.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/Particle.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/Particle.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/Particle.d

# Compiles file ../src/Scene/Patch.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/Patch.d
../bin/gccRelease/../src/Scene/Patch.o: ../src/Scene/Patch.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/Patch.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/Patch.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/Patch.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/Patch.d

# Compiles file ../src/Scene/Point.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/Point.d
../bin/gccRelease/../src/Scene/Point.o: ../src/Scene/Point.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/Point.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/Point.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/Point.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/Point.d

# Compiles file ../src/Scene/SceneManager.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/SceneManager.d
../bin/gccRelease/../src/Scene/SceneManager.o: ../src/Scene/SceneManager.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/SceneManager.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/SceneManager.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/SceneManager.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/SceneManager.d

# Compiles file ../src/Scene/Sky.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/Sky.d
../bin/gccRelease/../src/Scene/Sky.o: ../src/Scene/Sky.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/Sky.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/Sky.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/Sky.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/Sky.d

# Compiles file ../src/Scene/Terrain.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/Terrain.d
../bin/gccRelease/../src/Scene/Terrain.o: ../src/Scene/Terrain.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/Terrain.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/Terrain.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/Terrain.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/Terrain.d

# Compiles file ../src/Scene/TerrainLoader.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/TerrainLoader.d
../bin/gccRelease/../src/Scene/TerrainLoader.o: ../src/Scene/TerrainLoader.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/TerrainLoader.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/TerrainLoader.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/TerrainLoader.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/TerrainLoader.d

# Compiles file ../src/Scene/TerrainManager.c for the Release configuration...
-include ../bin/gccRelease/../src/Scene/TerrainManager.d
../bin/gccRelease/../src/Scene/TerrainManager.o: ../src/Scene/TerrainManager.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Scene/TerrainManager.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Scene/TerrainManager.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Scene/TerrainManager.c $(Release_Include_Path) > ../bin/gccRelease/../src/Scene/TerrainManager.d

# Compiles file ../../common/src/Graphics/Color.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/Color.d
../bin/gccRelease/../../common/src/Graphics/Color.o: ../../common/src/Graphics/Color.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/Color.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/Color.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/Color.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/Color.d

# Compiles file ../../common/src/Graphics/Coordinates.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/Coordinates.d
../bin/gccRelease/../../common/src/Graphics/Coordinates.o: ../../common/src/Graphics/Coordinates.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/Coordinates.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/Coordinates.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/Coordinates.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/Coordinates.d

# Compiles file ../../common/src/Graphics/Device.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/Device.d
../bin/gccRelease/../../common/src/Graphics/Device.o: ../../common/src/Graphics/Device.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/Device.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/Device.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/Device.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/Device.d

# Compiles file ../../common/src/Graphics/Fog.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/Fog.d
../bin/gccRelease/../../common/src/Graphics/Fog.o: ../../common/src/Graphics/Fog.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/Fog.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/Fog.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/Fog.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/Fog.d

# Compiles file ../../common/src/Graphics/Font.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/Font.d
../bin/gccRelease/../../common/src/Graphics/Font.o: ../../common/src/Graphics/Font.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/Font.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/Font.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/Font.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/Font.d

# Compiles file ../../common/src/Graphics/IndexBuffer.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/IndexBuffer.d
../bin/gccRelease/../../common/src/Graphics/IndexBuffer.o: ../../common/src/Graphics/IndexBuffer.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/IndexBuffer.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/IndexBuffer.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/IndexBuffer.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/IndexBuffer.d

# Compiles file ../../common/src/Graphics/MatrixStack.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/MatrixStack.d
../bin/gccRelease/../../common/src/Graphics/MatrixStack.o: ../../common/src/Graphics/MatrixStack.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/MatrixStack.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/MatrixStack.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/MatrixStack.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/MatrixStack.d

# Compiles file ../../common/src/Graphics/Shader.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/Shader.d
../bin/gccRelease/../../common/src/Graphics/Shader.o: ../../common/src/Graphics/Shader.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/Shader.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/Shader.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/Shader.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/Shader.d

# Compiles file ../../common/src/Graphics/ShaderLoader.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/ShaderLoader.d
../bin/gccRelease/../../common/src/Graphics/ShaderLoader.o: ../../common/src/Graphics/ShaderLoader.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/ShaderLoader.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/ShaderLoader.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/ShaderLoader.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/ShaderLoader.d

# Compiles file ../../common/src/Graphics/ShaderManager.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/ShaderManager.d
../bin/gccRelease/../../common/src/Graphics/ShaderManager.o: ../../common/src/Graphics/ShaderManager.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/ShaderManager.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/ShaderManager.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/ShaderManager.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/ShaderManager.d

# Compiles file ../../common/src/Graphics/StatesManager.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/StatesManager.d
../bin/gccRelease/../../common/src/Graphics/StatesManager.o: ../../common/src/Graphics/StatesManager.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/StatesManager.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/StatesManager.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/StatesManager.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/StatesManager.d

# Compiles file ../../common/src/Graphics/Texture.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/Texture.d
../bin/gccRelease/../../common/src/Graphics/Texture.o: ../../common/src/Graphics/Texture.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/Texture.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/Texture.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/Texture.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/Texture.d

# Compiles file ../../common/src/Graphics/TextureHelper.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/TextureHelper.d
../bin/gccRelease/../../common/src/Graphics/TextureHelper.o: ../../common/src/Graphics/TextureHelper.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/TextureHelper.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/TextureHelper.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/TextureHelper.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/TextureHelper.d

# Compiles file ../../common/src/Graphics/TextureLoader.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/TextureLoader.d
../bin/gccRelease/../../common/src/Graphics/TextureLoader.o: ../../common/src/Graphics/TextureLoader.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/TextureLoader.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/TextureLoader.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/TextureLoader.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/TextureLoader.d

# Compiles file ../../common/src/Graphics/TextureManager.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/TextureManager.d
../bin/gccRelease/../../common/src/Graphics/TextureManager.o: ../../common/src/Graphics/TextureManager.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/TextureManager.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/TextureManager.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/TextureManager.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/TextureManager.d

# Compiles file ../../common/src/Graphics/TextureRender.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/TextureRender.d
../bin/gccRelease/../../common/src/Graphics/TextureRender.o: ../../common/src/Graphics/TextureRender.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/TextureRender.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/TextureRender.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/TextureRender.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/TextureRender.d

# Compiles file ../../common/src/Graphics/VertexBuffer.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/VertexBuffer.d
../bin/gccRelease/../../common/src/Graphics/VertexBuffer.o: ../../common/src/Graphics/VertexBuffer.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/VertexBuffer.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/VertexBuffer.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/VertexBuffer.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/VertexBuffer.d

# Compiles file ../../common/src/Graphics/VertexFormat.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/VertexFormat.d
../bin/gccRelease/../../common/src/Graphics/VertexFormat.o: ../../common/src/Graphics/VertexFormat.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/VertexFormat.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/VertexFormat.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/VertexFormat.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/VertexFormat.d

# Compiles file ../../common/src/Graphics/VertexTypes.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/VertexTypes.d
../bin/gccRelease/../../common/src/Graphics/VertexTypes.o: ../../common/src/Graphics/VertexTypes.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/VertexTypes.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/VertexTypes.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/VertexTypes.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/VertexTypes.d

# Compiles file ../../common/src/Graphics/View.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/View.d
../bin/gccRelease/../../common/src/Graphics/View.o: ../../common/src/Graphics/View.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/View.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/View.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/View.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/View.d

# Compiles file ../../common/src/Graphics/Win32/VideoRender.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Graphics/Win32/VideoRender.d
../bin/gccRelease/../../common/src/Graphics/Win32/VideoRender.o: ../../common/src/Graphics/Win32/VideoRender.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Graphics/Win32/VideoRender.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Graphics/Win32/VideoRender.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Graphics/Win32/VideoRender.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Graphics/Win32/VideoRender.d

# Builds the DebugXP configuration...
.PHONY: DebugXP
DebugXP: create_folders ../bin/gccDebugXP/../src/main.o ../bin/gccDebugXP/../../common/src/Audio/OpenALHelper.o ../bin/gccDebugXP/../../common/src/Audio/Sound.o ../bin/gccDebugXP/../../common/src/Audio/SoundLoader.o ../bin/gccDebugXP/../../common/src/Audio/SoundManager.o ../bin/gccDebugXP/../../common/src/Window/Settings.o ../bin/gccDebugXP/../../common/src/Window/Win32/VideoModeSupport.o ../bin/gccDebugXP/../../common/src/Window/Win32/Window.o ../bin/gccDebugXP/../../common/src/System/CPUInfo.o ../bin/gccDebugXP/../../common/src/System/Dictionary.o ../bin/gccDebugXP/../../common/src/System/Error.o ../bin/gccDebugXP/../../common/src/System/MD5.o ../bin/gccDebugXP/../../common/src/System/Pack.o ../bin/gccDebugXP/../../common/src/System/Parser.o ../bin/gccDebugXP/../../common/src/System/Timer.o ../bin/gccDebugXP/../../common/src/System/TimerManager.o ../bin/gccDebugXP/../../common/src/System/Win32/IOHelper.o ../bin/gccDebugXP/../../common/src/System/Win32/Mutex.o ../bin/gccDebugXP/../../common/src/System/Win32/Platform.o ../bin/gccDebugXP/../../common/src/System/Win32/Thread.o ../bin/gccDebugXP/../../common/src/System/String/bstrlib.o ../bin/gccDebugXP/../../common/src/System/String/Lexer.o ../bin/gccDebugXP/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccDebugXP/../../common/src/System/DataTypes/Bitset.o ../bin/gccDebugXP/../../common/src/System/Memory/Allocator.o ../bin/gccDebugXP/../../common/src/System/Memory/Pool.o ../bin/gccDebugXP/../src/Network/Protocol.o ../bin/gccDebugXP/../../common/src/Network/Socket.o ../bin/gccDebugXP/../../common/src/Network/Win32/SocketHelper.o ../bin/gccDebugXP/../src/Game/Application.o ../bin/gccDebugXP/../src/Game/Character.o ../bin/gccDebugXP/../src/Game/Configuration.o ../bin/gccDebugXP/../src/Game/Display.o ../bin/gccDebugXP/../src/Game/Index.o ../bin/gccDebugXP/../src/Game/Intro.o ../bin/gccDebugXP/../src/Game/Language.o ../bin/gccDebugXP/../src/Game/Login.o ../bin/gccDebugXP/../src/Game/User.o ../bin/gccDebugXP/../../common/src/Math/General.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Matrix.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Plane.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Quaternion.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Rect.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Vector.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Sphere.o ../bin/gccDebugXP/../../common/src/Gui/Button.o ../bin/gccDebugXP/../../common/src/Gui/Clipboard.o ../bin/gccDebugXP/../../common/src/Gui/EventHandler.o ../bin/gccDebugXP/../../common/src/Gui/Form.o ../bin/gccDebugXP/../../common/src/Gui/Label.o ../bin/gccDebugXP/../../common/src/Gui/List.o ../bin/gccDebugXP/../../common/src/Gui/TextBox.o ../bin/gccDebugXP/../../common/src/Gui/Widget.o ../bin/gccDebugXP/../../common/src/Gui/WindowManager.o ../bin/gccDebugXP/../../common/src/Gui/WindowObj.o ../bin/gccDebugXP/../../common/src/Gui/WindowParser.o ../bin/gccDebugXP/../src/Scene/Camera.o ../bin/gccDebugXP/../src/Scene/Entity.o ../bin/gccDebugXP/../src/Scene/EntityManager.o ../bin/gccDebugXP/../src/Scene/Light.o ../bin/gccDebugXP/../src/Scene/Model.o ../bin/gccDebugXP/../src/Scene/ModelLoader.o ../bin/gccDebugXP/../src/Scene/ModelManager.o ../bin/gccDebugXP/../src/Scene/Octree.o ../bin/gccDebugXP/../src/Scene/Particle.o ../bin/gccDebugXP/../src/Scene/Patch.o ../bin/gccDebugXP/../src/Scene/Point.o ../bin/gccDebugXP/../src/Scene/SceneManager.o ../bin/gccDebugXP/../src/Scene/Sky.o ../bin/gccDebugXP/../src/Scene/Terrain.o ../bin/gccDebugXP/../src/Scene/TerrainLoader.o ../bin/gccDebugXP/../src/Scene/TerrainManager.o ../bin/gccDebugXP/../../common/src/Graphics/Color.o ../bin/gccDebugXP/../../common/src/Graphics/Coordinates.o ../bin/gccDebugXP/../../common/src/Graphics/Device.o ../bin/gccDebugXP/../../common/src/Graphics/Fog.o ../bin/gccDebugXP/../../common/src/Graphics/Font.o ../bin/gccDebugXP/../../common/src/Graphics/IndexBuffer.o ../bin/gccDebugXP/../../common/src/Graphics/MatrixStack.o ../bin/gccDebugXP/../../common/src/Graphics/Shader.o ../bin/gccDebugXP/../../common/src/Graphics/ShaderLoader.o ../bin/gccDebugXP/../../common/src/Graphics/ShaderManager.o ../bin/gccDebugXP/../../common/src/Graphics/StatesManager.o ../bin/gccDebugXP/../../common/src/Graphics/Texture.o ../bin/gccDebugXP/../../common/src/Graphics/TextureHelper.o ../bin/gccDebugXP/../../common/src/Graphics/TextureLoader.o ../bin/gccDebugXP/../../common/src/Graphics/TextureManager.o ../bin/gccDebugXP/../../common/src/Graphics/TextureRender.o ../bin/gccDebugXP/../../common/src/Graphics/VertexBuffer.o ../bin/gccDebugXP/../../common/src/Graphics/VertexFormat.o ../bin/gccDebugXP/../../common/src/Graphics/VertexTypes.o ../bin/gccDebugXP/../../common/src/Graphics/View.o ../bin/gccDebugXP/../../common/src/Graphics/Win32/VideoRender.o 
	g++ ../bin/gccDebugXP/../src/main.o ../bin/gccDebugXP/../../common/src/Audio/OpenALHelper.o ../bin/gccDebugXP/../../common/src/Audio/Sound.o ../bin/gccDebugXP/../../common/src/Audio/SoundLoader.o ../bin/gccDebugXP/../../common/src/Audio/SoundManager.o ../bin/gccDebugXP/../../common/src/Window/Settings.o ../bin/gccDebugXP/../../common/src/Window/Win32/VideoModeSupport.o ../bin/gccDebugXP/../../common/src/Window/Win32/Window.o ../bin/gccDebugXP/../../common/src/System/CPUInfo.o ../bin/gccDebugXP/../../common/src/System/Dictionary.o ../bin/gccDebugXP/../../common/src/System/Error.o ../bin/gccDebugXP/../../common/src/System/MD5.o ../bin/gccDebugXP/../../common/src/System/Pack.o ../bin/gccDebugXP/../../common/src/System/Parser.o ../bin/gccDebugXP/../../common/src/System/Timer.o ../bin/gccDebugXP/../../common/src/System/TimerManager.o ../bin/gccDebugXP/../../common/src/System/Win32/IOHelper.o ../bin/gccDebugXP/../../common/src/System/Win32/Mutex.o ../bin/gccDebugXP/../../common/src/System/Win32/Platform.o ../bin/gccDebugXP/../../common/src/System/Win32/Thread.o ../bin/gccDebugXP/../../common/src/System/String/bstrlib.o ../bin/gccDebugXP/../../common/src/System/String/Lexer.o ../bin/gccDebugXP/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccDebugXP/../../common/src/System/DataTypes/Bitset.o ../bin/gccDebugXP/../../common/src/System/Memory/Allocator.o ../bin/gccDebugXP/../../common/src/System/Memory/Pool.o ../bin/gccDebugXP/../src/Network/Protocol.o ../bin/gccDebugXP/../../common/src/Network/Socket.o ../bin/gccDebugXP/../../common/src/Network/Win32/SocketHelper.o ../bin/gccDebugXP/../src/Game/Application.o ../bin/gccDebugXP/../src/Game/Character.o ../bin/gccDebugXP/../src/Game/Configuration.o ../bin/gccDebugXP/../src/Game/Display.o ../bin/gccDebugXP/../src/Game/Index.o ../bin/gccDebugXP/../src/Game/Intro.o ../bin/gccDebugXP/../src/Game/Language.o ../bin/gccDebugXP/../src/Game/Login.o ../bin/gccDebugXP/../src/Game/User.o ../bin/gccDebugXP/../../common/src/Math/General.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Matrix.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Plane.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Quaternion.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Rect.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Vector.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Sphere.o ../bin/gccDebugXP/../../common/src/Gui/Button.o ../bin/gccDebugXP/../../common/src/Gui/Clipboard.o ../bin/gccDebugXP/../../common/src/Gui/EventHandler.o ../bin/gccDebugXP/../../common/src/Gui/Form.o ../bin/gccDebugXP/../../common/src/Gui/Label.o ../bin/gccDebugXP/../../common/src/Gui/List.o ../bin/gccDebugXP/../../common/src/Gui/TextBox.o ../bin/gccDebugXP/../../common/src/Gui/Widget.o ../bin/gccDebugXP/../../common/src/Gui/WindowManager.o ../bin/gccDebugXP/../../common/src/Gui/WindowObj.o ../bin/gccDebugXP/../../common/src/Gui/WindowParser.o ../bin/gccDebugXP/../src/Scene/Camera.o ../bin/gccDebugXP/../src/Scene/Entity.o ../bin/gccDebugXP/../src/Scene/EntityManager.o ../bin/gccDebugXP/../src/Scene/Light.o ../bin/gccDebugXP/../src/Scene/Model.o ../bin/gccDebugXP/../src/Scene/ModelLoader.o ../bin/gccDebugXP/../src/Scene/ModelManager.o ../bin/gccDebugXP/../src/Scene/Octree.o ../bin/gccDebugXP/../src/Scene/Particle.o ../bin/gccDebugXP/../src/Scene/Patch.o ../bin/gccDebugXP/../src/Scene/Point.o ../bin/gccDebugXP/../src/Scene/SceneManager.o ../bin/gccDebugXP/../src/Scene/Sky.o ../bin/gccDebugXP/../src/Scene/Terrain.o ../bin/gccDebugXP/../src/Scene/TerrainLoader.o ../bin/gccDebugXP/../src/Scene/TerrainManager.o ../bin/gccDebugXP/../../common/src/Graphics/Color.o ../bin/gccDebugXP/../../common/src/Graphics/Coordinates.o ../bin/gccDebugXP/../../common/src/Graphics/Device.o ../bin/gccDebugXP/../../common/src/Graphics/Fog.o ../bin/gccDebugXP/../../common/src/Graphics/Font.o ../bin/gccDebugXP/../../common/src/Graphics/IndexBuffer.o ../bin/gccDebugXP/../../common/src/Graphics/MatrixStack.o ../bin/gccDebugXP/../../common/src/Graphics/Shader.o ../bin/gccDebugXP/../../common/src/Graphics/ShaderLoader.o ../bin/gccDebugXP/../../common/src/Graphics/ShaderManager.o ../bin/gccDebugXP/../../common/src/Graphics/StatesManager.o ../bin/gccDebugXP/../../common/src/Graphics/Texture.o ../bin/gccDebugXP/../../common/src/Graphics/TextureHelper.o ../bin/gccDebugXP/../../common/src/Graphics/TextureLoader.o ../bin/gccDebugXP/../../common/src/Graphics/TextureManager.o ../bin/gccDebugXP/../../common/src/Graphics/TextureRender.o ../bin/gccDebugXP/../../common/src/Graphics/VertexBuffer.o ../bin/gccDebugXP/../../common/src/Graphics/VertexFormat.o ../bin/gccDebugXP/../../common/src/Graphics/VertexTypes.o ../bin/gccDebugXP/../../common/src/Graphics/View.o ../bin/gccDebugXP/../../common/src/Graphics/Win32/VideoRender.o  $(DebugXP_Library_Path) $(DebugXP_Libraries) -Wl,-rpath,./ -o ../bin/gccDebugXP/Client.exe

# Compiles file ../src/main.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/main.d
../bin/gccDebugXP/../src/main.o: ../src/main.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/main.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/main.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/main.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/main.d

# Compiles file ../../common/src/Audio/OpenALHelper.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Audio/OpenALHelper.d
../bin/gccDebugXP/../../common/src/Audio/OpenALHelper.o: ../../common/src/Audio/OpenALHelper.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Audio/OpenALHelper.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Audio/OpenALHelper.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Audio/OpenALHelper.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Audio/OpenALHelper.d

# Compiles file ../../common/src/Audio/Sound.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Audio/Sound.d
../bin/gccDebugXP/../../common/src/Audio/Sound.o: ../../common/src/Audio/Sound.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Audio/Sound.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Audio/Sound.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Audio/Sound.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Audio/Sound.d

# Compiles file ../../common/src/Audio/SoundLoader.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Audio/SoundLoader.d
../bin/gccDebugXP/../../common/src/Audio/SoundLoader.o: ../../common/src/Audio/SoundLoader.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Audio/SoundLoader.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Audio/SoundLoader.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Audio/SoundLoader.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Audio/SoundLoader.d

# Compiles file ../../common/src/Audio/SoundManager.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Audio/SoundManager.d
../bin/gccDebugXP/../../common/src/Audio/SoundManager.o: ../../common/src/Audio/SoundManager.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Audio/SoundManager.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Audio/SoundManager.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Audio/SoundManager.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Audio/SoundManager.d

# Compiles file ../../common/src/Window/Settings.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Window/Settings.d
../bin/gccDebugXP/../../common/src/Window/Settings.o: ../../common/src/Window/Settings.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Window/Settings.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Window/Settings.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Window/Settings.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Window/Settings.d

# Compiles file ../../common/src/Window/Win32/VideoModeSupport.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Window/Win32/VideoModeSupport.d
../bin/gccDebugXP/../../common/src/Window/Win32/VideoModeSupport.o: ../../common/src/Window/Win32/VideoModeSupport.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Window/Win32/VideoModeSupport.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Window/Win32/VideoModeSupport.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Window/Win32/VideoModeSupport.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Window/Win32/VideoModeSupport.d

# Compiles file ../../common/src/Window/Win32/Window.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Window/Win32/Window.d
../bin/gccDebugXP/../../common/src/Window/Win32/Window.o: ../../common/src/Window/Win32/Window.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Window/Win32/Window.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Window/Win32/Window.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Window/Win32/Window.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Window/Win32/Window.d

# Compiles file ../../common/src/System/CPUInfo.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/CPUInfo.d
../bin/gccDebugXP/../../common/src/System/CPUInfo.o: ../../common/src/System/CPUInfo.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/CPUInfo.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/CPUInfo.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/CPUInfo.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/CPUInfo.d

# Compiles file ../../common/src/System/Dictionary.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Dictionary.d
../bin/gccDebugXP/../../common/src/System/Dictionary.o: ../../common/src/System/Dictionary.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Dictionary.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Dictionary.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Dictionary.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Dictionary.d

# Compiles file ../../common/src/System/Error.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Error.d
../bin/gccDebugXP/../../common/src/System/Error.o: ../../common/src/System/Error.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Error.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Error.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Error.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Error.d

# Compiles file ../../common/src/System/MD5.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/MD5.d
../bin/gccDebugXP/../../common/src/System/MD5.o: ../../common/src/System/MD5.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/MD5.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/MD5.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/MD5.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/MD5.d

# Compiles file ../../common/src/System/Pack.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Pack.d
../bin/gccDebugXP/../../common/src/System/Pack.o: ../../common/src/System/Pack.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Pack.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Pack.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Pack.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Pack.d

# Compiles file ../../common/src/System/Parser.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Parser.d
../bin/gccDebugXP/../../common/src/System/Parser.o: ../../common/src/System/Parser.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Parser.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Parser.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Parser.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Parser.d

# Compiles file ../../common/src/System/Timer.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Timer.d
../bin/gccDebugXP/../../common/src/System/Timer.o: ../../common/src/System/Timer.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Timer.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Timer.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Timer.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Timer.d

# Compiles file ../../common/src/System/TimerManager.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/TimerManager.d
../bin/gccDebugXP/../../common/src/System/TimerManager.o: ../../common/src/System/TimerManager.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/TimerManager.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/TimerManager.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/TimerManager.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/TimerManager.d

# Compiles file ../../common/src/System/Win32/IOHelper.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Win32/IOHelper.d
../bin/gccDebugXP/../../common/src/System/Win32/IOHelper.o: ../../common/src/System/Win32/IOHelper.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Win32/IOHelper.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Win32/IOHelper.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Win32/IOHelper.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Win32/IOHelper.d

# Compiles file ../../common/src/System/Win32/Mutex.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Win32/Mutex.d
../bin/gccDebugXP/../../common/src/System/Win32/Mutex.o: ../../common/src/System/Win32/Mutex.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Win32/Mutex.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Win32/Mutex.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Win32/Mutex.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Win32/Mutex.d

# Compiles file ../../common/src/System/Win32/Platform.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Win32/Platform.d
../bin/gccDebugXP/../../common/src/System/Win32/Platform.o: ../../common/src/System/Win32/Platform.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Win32/Platform.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Win32/Platform.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Win32/Platform.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Win32/Platform.d

# Compiles file ../../common/src/System/Win32/Thread.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Win32/Thread.d
../bin/gccDebugXP/../../common/src/System/Win32/Thread.o: ../../common/src/System/Win32/Thread.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Win32/Thread.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Win32/Thread.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Win32/Thread.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Win32/Thread.d

# Compiles file ../../common/src/System/String/bstrlib.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/String/bstrlib.d
../bin/gccDebugXP/../../common/src/System/String/bstrlib.o: ../../common/src/System/String/bstrlib.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/String/bstrlib.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/String/bstrlib.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/String/bstrlib.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/String/bstrlib.d

# Compiles file ../../common/src/System/String/Lexer.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/String/Lexer.d
../bin/gccDebugXP/../../common/src/System/String/Lexer.o: ../../common/src/System/String/Lexer.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/String/Lexer.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/String/Lexer.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/String/Lexer.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/String/Lexer.d

# Compiles file ../../common/src/System/DataTypes/BinaryTree.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/DataTypes/BinaryTree.d
../bin/gccDebugXP/../../common/src/System/DataTypes/BinaryTree.o: ../../common/src/System/DataTypes/BinaryTree.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/DataTypes/BinaryTree.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/DataTypes/BinaryTree.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/DataTypes/BinaryTree.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/DataTypes/BinaryTree.d

# Compiles file ../../common/src/System/DataTypes/Bitset.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/DataTypes/Bitset.d
../bin/gccDebugXP/../../common/src/System/DataTypes/Bitset.o: ../../common/src/System/DataTypes/Bitset.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/DataTypes/Bitset.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/DataTypes/Bitset.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/DataTypes/Bitset.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/DataTypes/Bitset.d

# Compiles file ../../common/src/System/Memory/Allocator.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Memory/Allocator.d
../bin/gccDebugXP/../../common/src/System/Memory/Allocator.o: ../../common/src/System/Memory/Allocator.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Memory/Allocator.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Memory/Allocator.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Memory/Allocator.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Memory/Allocator.d

# Compiles file ../../common/src/System/Memory/Pool.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Memory/Pool.d
../bin/gccDebugXP/../../common/src/System/Memory/Pool.o: ../../common/src/System/Memory/Pool.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Memory/Pool.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Memory/Pool.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Memory/Pool.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Memory/Pool.d

# Compiles file ../src/Network/Protocol.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Network/Protocol.d
../bin/gccDebugXP/../src/Network/Protocol.o: ../src/Network/Protocol.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Network/Protocol.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Network/Protocol.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Network/Protocol.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Network/Protocol.d

# Compiles file ../../common/src/Network/Socket.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Network/Socket.d
../bin/gccDebugXP/../../common/src/Network/Socket.o: ../../common/src/Network/Socket.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Network/Socket.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Network/Socket.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Network/Socket.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Network/Socket.d

# Compiles file ../../common/src/Network/Win32/SocketHelper.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Network/Win32/SocketHelper.d
../bin/gccDebugXP/../../common/src/Network/Win32/SocketHelper.o: ../../common/src/Network/Win32/SocketHelper.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Network/Win32/SocketHelper.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Network/Win32/SocketHelper.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Network/Win32/SocketHelper.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Network/Win32/SocketHelper.d

# Compiles file ../src/Game/Application.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/Application.d
../bin/gccDebugXP/../src/Game/Application.o: ../src/Game/Application.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/Application.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/Application.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/Application.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/Application.d

# Compiles file ../src/Game/Character.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/Character.d
../bin/gccDebugXP/../src/Game/Character.o: ../src/Game/Character.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/Character.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/Character.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/Character.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/Character.d

# Compiles file ../src/Game/Configuration.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/Configuration.d
../bin/gccDebugXP/../src/Game/Configuration.o: ../src/Game/Configuration.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/Configuration.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/Configuration.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/Configuration.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/Configuration.d

# Compiles file ../src/Game/Display.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/Display.d
../bin/gccDebugXP/../src/Game/Display.o: ../src/Game/Display.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/Display.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/Display.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/Display.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/Display.d

# Compiles file ../src/Game/Index.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/Index.d
../bin/gccDebugXP/../src/Game/Index.o: ../src/Game/Index.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/Index.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/Index.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/Index.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/Index.d

# Compiles file ../src/Game/Intro.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/Intro.d
../bin/gccDebugXP/../src/Game/Intro.o: ../src/Game/Intro.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/Intro.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/Intro.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/Intro.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/Intro.d

# Compiles file ../src/Game/Language.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/Language.d
../bin/gccDebugXP/../src/Game/Language.o: ../src/Game/Language.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/Language.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/Language.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/Language.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/Language.d

# Compiles file ../src/Game/Login.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/Login.d
../bin/gccDebugXP/../src/Game/Login.o: ../src/Game/Login.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/Login.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/Login.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/Login.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/Login.d

# Compiles file ../src/Game/User.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/User.d
../bin/gccDebugXP/../src/Game/User.o: ../src/Game/User.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/User.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/User.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/User.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/User.d

# Compiles file ../../common/src/Math/General.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/General.d
../bin/gccDebugXP/../../common/src/Math/General.o: ../../common/src/Math/General.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/General.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/General.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/General.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/General.d

# Compiles file ../../common/src/Math/Geometry/Matrix.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Matrix.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Matrix.o: ../../common/src/Math/Geometry/Matrix.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Matrix.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Matrix.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Matrix.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Matrix.d

# Compiles file ../../common/src/Math/Geometry/Plane.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Plane.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Plane.o: ../../common/src/Math/Geometry/Plane.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Plane.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Plane.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Plane.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Plane.d

# Compiles file ../../common/src/Math/Geometry/Quaternion.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Quaternion.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Quaternion.o: ../../common/src/Math/Geometry/Quaternion.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Quaternion.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Quaternion.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Quaternion.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Quaternion.d

# Compiles file ../../common/src/Math/Geometry/Rect.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Rect.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Rect.o: ../../common/src/Math/Geometry/Rect.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Rect.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Rect.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Rect.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Rect.d

# Compiles file ../../common/src/Math/Geometry/Vector.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Vector.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Vector.o: ../../common/src/Math/Geometry/Vector.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Vector.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Vector.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Vector.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Vector.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Box.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Box.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Box.o: ../../common/src/Math/Geometry/Bounding/Box.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Box.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Box.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Box.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Box.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Frustum.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Frustum.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Frustum.o: ../../common/src/Math/Geometry/Bounding/Frustum.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Frustum.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Frustum.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Frustum.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Frustum.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Quad.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Quad.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Quad.o: ../../common/src/Math/Geometry/Bounding/Quad.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Quad.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Quad.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Quad.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Quad.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Sphere.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Sphere.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Sphere.o: ../../common/src/Math/Geometry/Bounding/Sphere.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Sphere.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Sphere.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Sphere.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Sphere.d

# Compiles file ../../common/src/Gui/Button.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Gui/Button.d
../bin/gccDebugXP/../../common/src/Gui/Button.o: ../../common/src/Gui/Button.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Gui/Button.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Gui/Button.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Gui/Button.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Gui/Button.d

# Compiles file ../../common/src/Gui/Clipboard.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Gui/Clipboard.d
../bin/gccDebugXP/../../common/src/Gui/Clipboard.o: ../../common/src/Gui/Clipboard.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Gui/Clipboard.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Gui/Clipboard.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Gui/Clipboard.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Gui/Clipboard.d

# Compiles file ../../common/src/Gui/EventHandler.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Gui/EventHandler.d
../bin/gccDebugXP/../../common/src/Gui/EventHandler.o: ../../common/src/Gui/EventHandler.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Gui/EventHandler.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Gui/EventHandler.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Gui/EventHandler.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Gui/EventHandler.d

# Compiles file ../../common/src/Gui/Form.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Gui/Form.d
../bin/gccDebugXP/../../common/src/Gui/Form.o: ../../common/src/Gui/Form.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Gui/Form.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Gui/Form.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Gui/Form.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Gui/Form.d

# Compiles file ../../common/src/Gui/Label.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Gui/Label.d
../bin/gccDebugXP/../../common/src/Gui/Label.o: ../../common/src/Gui/Label.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Gui/Label.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Gui/Label.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Gui/Label.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Gui/Label.d

# Compiles file ../../common/src/Gui/List.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Gui/List.d
../bin/gccDebugXP/../../common/src/Gui/List.o: ../../common/src/Gui/List.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Gui/List.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Gui/List.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Gui/List.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Gui/List.d

# Compiles file ../../common/src/Gui/TextBox.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Gui/TextBox.d
../bin/gccDebugXP/../../common/src/Gui/TextBox.o: ../../common/src/Gui/TextBox.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Gui/TextBox.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Gui/TextBox.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Gui/TextBox.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Gui/TextBox.d

# Compiles file ../../common/src/Gui/Widget.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Gui/Widget.d
../bin/gccDebugXP/../../common/src/Gui/Widget.o: ../../common/src/Gui/Widget.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Gui/Widget.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Gui/Widget.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Gui/Widget.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Gui/Widget.d

# Compiles file ../../common/src/Gui/WindowManager.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Gui/WindowManager.d
../bin/gccDebugXP/../../common/src/Gui/WindowManager.o: ../../common/src/Gui/WindowManager.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Gui/WindowManager.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Gui/WindowManager.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Gui/WindowManager.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Gui/WindowManager.d

# Compiles file ../../common/src/Gui/WindowObj.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Gui/WindowObj.d
../bin/gccDebugXP/../../common/src/Gui/WindowObj.o: ../../common/src/Gui/WindowObj.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Gui/WindowObj.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Gui/WindowObj.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Gui/WindowObj.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Gui/WindowObj.d

# Compiles file ../../common/src/Gui/WindowParser.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Gui/WindowParser.d
../bin/gccDebugXP/../../common/src/Gui/WindowParser.o: ../../common/src/Gui/WindowParser.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Gui/WindowParser.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Gui/WindowParser.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Gui/WindowParser.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Gui/WindowParser.d

# Compiles file ../src/Scene/Camera.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/Camera.d
../bin/gccDebugXP/../src/Scene/Camera.o: ../src/Scene/Camera.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/Camera.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/Camera.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/Camera.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/Camera.d

# Compiles file ../src/Scene/Entity.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/Entity.d
../bin/gccDebugXP/../src/Scene/Entity.o: ../src/Scene/Entity.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/Entity.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/Entity.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/Entity.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/Entity.d

# Compiles file ../src/Scene/EntityManager.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/EntityManager.d
../bin/gccDebugXP/../src/Scene/EntityManager.o: ../src/Scene/EntityManager.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/EntityManager.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/EntityManager.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/EntityManager.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/EntityManager.d

# Compiles file ../src/Scene/Light.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/Light.d
../bin/gccDebugXP/../src/Scene/Light.o: ../src/Scene/Light.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/Light.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/Light.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/Light.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/Light.d

# Compiles file ../src/Scene/Model.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/Model.d
../bin/gccDebugXP/../src/Scene/Model.o: ../src/Scene/Model.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/Model.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/Model.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/Model.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/Model.d

# Compiles file ../src/Scene/ModelLoader.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/ModelLoader.d
../bin/gccDebugXP/../src/Scene/ModelLoader.o: ../src/Scene/ModelLoader.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/ModelLoader.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/ModelLoader.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/ModelLoader.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/ModelLoader.d

# Compiles file ../src/Scene/ModelManager.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/ModelManager.d
../bin/gccDebugXP/../src/Scene/ModelManager.o: ../src/Scene/ModelManager.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/ModelManager.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/ModelManager.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/ModelManager.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/ModelManager.d

# Compiles file ../src/Scene/Octree.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/Octree.d
../bin/gccDebugXP/../src/Scene/Octree.o: ../src/Scene/Octree.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/Octree.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/Octree.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/Octree.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/Octree.d

# Compiles file ../src/Scene/Particle.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/Particle.d
../bin/gccDebugXP/../src/Scene/Particle.o: ../src/Scene/Particle.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/Particle.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/Particle.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/Particle.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/Particle.d

# Compiles file ../src/Scene/Patch.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/Patch.d
../bin/gccDebugXP/../src/Scene/Patch.o: ../src/Scene/Patch.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/Patch.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/Patch.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/Patch.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/Patch.d

# Compiles file ../src/Scene/Point.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/Point.d
../bin/gccDebugXP/../src/Scene/Point.o: ../src/Scene/Point.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/Point.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/Point.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/Point.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/Point.d

# Compiles file ../src/Scene/SceneManager.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/SceneManager.d
../bin/gccDebugXP/../src/Scene/SceneManager.o: ../src/Scene/SceneManager.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/SceneManager.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/SceneManager.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/SceneManager.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/SceneManager.d

# Compiles file ../src/Scene/Sky.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/Sky.d
../bin/gccDebugXP/../src/Scene/Sky.o: ../src/Scene/Sky.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/Sky.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/Sky.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/Sky.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/Sky.d

# Compiles file ../src/Scene/Terrain.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/Terrain.d
../bin/gccDebugXP/../src/Scene/Terrain.o: ../src/Scene/Terrain.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/Terrain.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/Terrain.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/Terrain.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/Terrain.d

# Compiles file ../src/Scene/TerrainLoader.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/TerrainLoader.d
../bin/gccDebugXP/../src/Scene/TerrainLoader.o: ../src/Scene/TerrainLoader.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/TerrainLoader.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/TerrainLoader.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/TerrainLoader.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/TerrainLoader.d

# Compiles file ../src/Scene/TerrainManager.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Scene/TerrainManager.d
../bin/gccDebugXP/../src/Scene/TerrainManager.o: ../src/Scene/TerrainManager.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Scene/TerrainManager.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Scene/TerrainManager.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Scene/TerrainManager.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Scene/TerrainManager.d

# Compiles file ../../common/src/Graphics/Color.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/Color.d
../bin/gccDebugXP/../../common/src/Graphics/Color.o: ../../common/src/Graphics/Color.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/Color.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/Color.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/Color.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/Color.d

# Compiles file ../../common/src/Graphics/Coordinates.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/Coordinates.d
../bin/gccDebugXP/../../common/src/Graphics/Coordinates.o: ../../common/src/Graphics/Coordinates.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/Coordinates.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/Coordinates.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/Coordinates.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/Coordinates.d

# Compiles file ../../common/src/Graphics/Device.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/Device.d
../bin/gccDebugXP/../../common/src/Graphics/Device.o: ../../common/src/Graphics/Device.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/Device.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/Device.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/Device.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/Device.d

# Compiles file ../../common/src/Graphics/Fog.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/Fog.d
../bin/gccDebugXP/../../common/src/Graphics/Fog.o: ../../common/src/Graphics/Fog.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/Fog.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/Fog.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/Fog.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/Fog.d

# Compiles file ../../common/src/Graphics/Font.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/Font.d
../bin/gccDebugXP/../../common/src/Graphics/Font.o: ../../common/src/Graphics/Font.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/Font.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/Font.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/Font.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/Font.d

# Compiles file ../../common/src/Graphics/IndexBuffer.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/IndexBuffer.d
../bin/gccDebugXP/../../common/src/Graphics/IndexBuffer.o: ../../common/src/Graphics/IndexBuffer.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/IndexBuffer.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/IndexBuffer.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/IndexBuffer.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/IndexBuffer.d

# Compiles file ../../common/src/Graphics/MatrixStack.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/MatrixStack.d
../bin/gccDebugXP/../../common/src/Graphics/MatrixStack.o: ../../common/src/Graphics/MatrixStack.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/MatrixStack.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/MatrixStack.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/MatrixStack.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/MatrixStack.d

# Compiles file ../../common/src/Graphics/Shader.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/Shader.d
../bin/gccDebugXP/../../common/src/Graphics/Shader.o: ../../common/src/Graphics/Shader.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/Shader.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/Shader.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/Shader.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/Shader.d

# Compiles file ../../common/src/Graphics/ShaderLoader.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/ShaderLoader.d
../bin/gccDebugXP/../../common/src/Graphics/ShaderLoader.o: ../../common/src/Graphics/ShaderLoader.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/ShaderLoader.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/ShaderLoader.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/ShaderLoader.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/ShaderLoader.d

# Compiles file ../../common/src/Graphics/ShaderManager.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/ShaderManager.d
../bin/gccDebugXP/../../common/src/Graphics/ShaderManager.o: ../../common/src/Graphics/ShaderManager.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/ShaderManager.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/ShaderManager.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/ShaderManager.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/ShaderManager.d

# Compiles file ../../common/src/Graphics/StatesManager.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/StatesManager.d
../bin/gccDebugXP/../../common/src/Graphics/StatesManager.o: ../../common/src/Graphics/StatesManager.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/StatesManager.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/StatesManager.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/StatesManager.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/StatesManager.d

# Compiles file ../../common/src/Graphics/Texture.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/Texture.d
../bin/gccDebugXP/../../common/src/Graphics/Texture.o: ../../common/src/Graphics/Texture.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/Texture.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/Texture.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/Texture.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/Texture.d

# Compiles file ../../common/src/Graphics/TextureHelper.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/TextureHelper.d
../bin/gccDebugXP/../../common/src/Graphics/TextureHelper.o: ../../common/src/Graphics/TextureHelper.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/TextureHelper.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/TextureHelper.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/TextureHelper.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/TextureHelper.d

# Compiles file ../../common/src/Graphics/TextureLoader.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/TextureLoader.d
../bin/gccDebugXP/../../common/src/Graphics/TextureLoader.o: ../../common/src/Graphics/TextureLoader.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/TextureLoader.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/TextureLoader.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/TextureLoader.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/TextureLoader.d

# Compiles file ../../common/src/Graphics/TextureManager.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/TextureManager.d
../bin/gccDebugXP/../../common/src/Graphics/TextureManager.o: ../../common/src/Graphics/TextureManager.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/TextureManager.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/TextureManager.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/TextureManager.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/TextureManager.d

# Compiles file ../../common/src/Graphics/TextureRender.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/TextureRender.d
../bin/gccDebugXP/../../common/src/Graphics/TextureRender.o: ../../common/src/Graphics/TextureRender.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/TextureRender.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/TextureRender.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/TextureRender.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/TextureRender.d

# Compiles file ../../common/src/Graphics/VertexBuffer.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/VertexBuffer.d
../bin/gccDebugXP/../../common/src/Graphics/VertexBuffer.o: ../../common/src/Graphics/VertexBuffer.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/VertexBuffer.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/VertexBuffer.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/VertexBuffer.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/VertexBuffer.d

# Compiles file ../../common/src/Graphics/VertexFormat.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/VertexFormat.d
../bin/gccDebugXP/../../common/src/Graphics/VertexFormat.o: ../../common/src/Graphics/VertexFormat.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/VertexFormat.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/VertexFormat.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/VertexFormat.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/VertexFormat.d

# Compiles file ../../common/src/Graphics/VertexTypes.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/VertexTypes.d
../bin/gccDebugXP/../../common/src/Graphics/VertexTypes.o: ../../common/src/Graphics/VertexTypes.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/VertexTypes.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/VertexTypes.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/VertexTypes.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/VertexTypes.d

# Compiles file ../../common/src/Graphics/View.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/View.d
../bin/gccDebugXP/../../common/src/Graphics/View.o: ../../common/src/Graphics/View.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/View.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/View.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/View.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/View.d

# Compiles file ../../common/src/Graphics/Win32/VideoRender.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Graphics/Win32/VideoRender.d
../bin/gccDebugXP/../../common/src/Graphics/Win32/VideoRender.o: ../../common/src/Graphics/Win32/VideoRender.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Graphics/Win32/VideoRender.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Graphics/Win32/VideoRender.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Graphics/Win32/VideoRender.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Graphics/Win32/VideoRender.d

# Builds the ReleaseXP configuration...
.PHONY: ReleaseXP
ReleaseXP: create_folders ../bin/gccReleaseXP/../src/main.o ../bin/gccReleaseXP/../../common/src/Audio/OpenALHelper.o ../bin/gccReleaseXP/../../common/src/Audio/Sound.o ../bin/gccReleaseXP/../../common/src/Audio/SoundLoader.o ../bin/gccReleaseXP/../../common/src/Audio/SoundManager.o ../bin/gccReleaseXP/../../common/src/Window/Settings.o ../bin/gccReleaseXP/../../common/src/Window/Win32/VideoModeSupport.o ../bin/gccReleaseXP/../../common/src/Window/Win32/Window.o ../bin/gccReleaseXP/../../common/src/System/CPUInfo.o ../bin/gccReleaseXP/../../common/src/System/Dictionary.o ../bin/gccReleaseXP/../../common/src/System/Error.o ../bin/gccReleaseXP/../../common/src/System/MD5.o ../bin/gccReleaseXP/../../common/src/System/Pack.o ../bin/gccReleaseXP/../../common/src/System/Parser.o ../bin/gccReleaseXP/../../common/src/System/Timer.o ../bin/gccReleaseXP/../../common/src/System/TimerManager.o ../bin/gccReleaseXP/../../common/src/System/Win32/IOHelper.o ../bin/gccReleaseXP/../../common/src/System/Win32/Mutex.o ../bin/gccReleaseXP/../../common/src/System/Win32/Platform.o ../bin/gccReleaseXP/../../common/src/System/Win32/Thread.o ../bin/gccReleaseXP/../../common/src/System/String/bstrlib.o ../bin/gccReleaseXP/../../common/src/System/String/Lexer.o ../bin/gccReleaseXP/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccReleaseXP/../../common/src/System/DataTypes/Bitset.o ../bin/gccReleaseXP/../../common/src/System/Memory/Allocator.o ../bin/gccReleaseXP/../../common/src/System/Memory/Pool.o ../bin/gccReleaseXP/../src/Network/Protocol.o ../bin/gccReleaseXP/../../common/src/Network/Socket.o ../bin/gccReleaseXP/../../common/src/Network/Win32/SocketHelper.o ../bin/gccReleaseXP/../src/Game/Application.o ../bin/gccReleaseXP/../src/Game/Character.o ../bin/gccReleaseXP/../src/Game/Configuration.o ../bin/gccReleaseXP/../src/Game/Display.o ../bin/gccReleaseXP/../src/Game/Index.o ../bin/gccReleaseXP/../src/Game/Intro.o ../bin/gccReleaseXP/../src/Game/Language.o ../bin/gccReleaseXP/../src/Game/Login.o ../bin/gccReleaseXP/../src/Game/User.o ../bin/gccReleaseXP/../../common/src/Math/General.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Matrix.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Plane.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Quaternion.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Rect.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Vector.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Sphere.o ../bin/gccReleaseXP/../../common/src/Gui/Button.o ../bin/gccReleaseXP/../../common/src/Gui/Clipboard.o ../bin/gccReleaseXP/../../common/src/Gui/EventHandler.o ../bin/gccReleaseXP/../../common/src/Gui/Form.o ../bin/gccReleaseXP/../../common/src/Gui/Label.o ../bin/gccReleaseXP/../../common/src/Gui/List.o ../bin/gccReleaseXP/../../common/src/Gui/TextBox.o ../bin/gccReleaseXP/../../common/src/Gui/Widget.o ../bin/gccReleaseXP/../../common/src/Gui/WindowManager.o ../bin/gccReleaseXP/../../common/src/Gui/WindowObj.o ../bin/gccReleaseXP/../../common/src/Gui/WindowParser.o ../bin/gccReleaseXP/../src/Scene/Camera.o ../bin/gccReleaseXP/../src/Scene/Entity.o ../bin/gccReleaseXP/../src/Scene/EntityManager.o ../bin/gccReleaseXP/../src/Scene/Light.o ../bin/gccReleaseXP/../src/Scene/Model.o ../bin/gccReleaseXP/../src/Scene/ModelLoader.o ../bin/gccReleaseXP/../src/Scene/ModelManager.o ../bin/gccReleaseXP/../src/Scene/Octree.o ../bin/gccReleaseXP/../src/Scene/Particle.o ../bin/gccReleaseXP/../src/Scene/Patch.o ../bin/gccReleaseXP/../src/Scene/Point.o ../bin/gccReleaseXP/../src/Scene/SceneManager.o ../bin/gccReleaseXP/../src/Scene/Sky.o ../bin/gccReleaseXP/../src/Scene/Terrain.o ../bin/gccReleaseXP/../src/Scene/TerrainLoader.o ../bin/gccReleaseXP/../src/Scene/TerrainManager.o ../bin/gccReleaseXP/../../common/src/Graphics/Color.o ../bin/gccReleaseXP/../../common/src/Graphics/Coordinates.o ../bin/gccReleaseXP/../../common/src/Graphics/Device.o ../bin/gccReleaseXP/../../common/src/Graphics/Fog.o ../bin/gccReleaseXP/../../common/src/Graphics/Font.o ../bin/gccReleaseXP/../../common/src/Graphics/IndexBuffer.o ../bin/gccReleaseXP/../../common/src/Graphics/MatrixStack.o ../bin/gccReleaseXP/../../common/src/Graphics/Shader.o ../bin/gccReleaseXP/../../common/src/Graphics/ShaderLoader.o ../bin/gccReleaseXP/../../common/src/Graphics/ShaderManager.o ../bin/gccReleaseXP/../../common/src/Graphics/StatesManager.o ../bin/gccReleaseXP/../../common/src/Graphics/Texture.o ../bin/gccReleaseXP/../../common/src/Graphics/TextureHelper.o ../bin/gccReleaseXP/../../common/src/Graphics/TextureLoader.o ../bin/gccReleaseXP/../../common/src/Graphics/TextureManager.o ../bin/gccReleaseXP/../../common/src/Graphics/TextureRender.o ../bin/gccReleaseXP/../../common/src/Graphics/VertexBuffer.o ../bin/gccReleaseXP/../../common/src/Graphics/VertexFormat.o ../bin/gccReleaseXP/../../common/src/Graphics/VertexTypes.o ../bin/gccReleaseXP/../../common/src/Graphics/View.o ../bin/gccReleaseXP/../../common/src/Graphics/Win32/VideoRender.o 
	g++ ../bin/gccReleaseXP/../src/main.o ../bin/gccReleaseXP/../../common/src/Audio/OpenALHelper.o ../bin/gccReleaseXP/../../common/src/Audio/Sound.o ../bin/gccReleaseXP/../../common/src/Audio/SoundLoader.o ../bin/gccReleaseXP/../../common/src/Audio/SoundManager.o ../bin/gccReleaseXP/../../common/src/Window/Settings.o ../bin/gccReleaseXP/../../common/src/Window/Win32/VideoModeSupport.o ../bin/gccReleaseXP/../../common/src/Window/Win32/Window.o ../bin/gccReleaseXP/../../common/src/System/CPUInfo.o ../bin/gccReleaseXP/../../common/src/System/Dictionary.o ../bin/gccReleaseXP/../../common/src/System/Error.o ../bin/gccReleaseXP/../../common/src/System/MD5.o ../bin/gccReleaseXP/../../common/src/System/Pack.o ../bin/gccReleaseXP/../../common/src/System/Parser.o ../bin/gccReleaseXP/../../common/src/System/Timer.o ../bin/gccReleaseXP/../../common/src/System/TimerManager.o ../bin/gccReleaseXP/../../common/src/System/Win32/IOHelper.o ../bin/gccReleaseXP/../../common/src/System/Win32/Mutex.o ../bin/gccReleaseXP/../../common/src/System/Win32/Platform.o ../bin/gccReleaseXP/../../common/src/System/Win32/Thread.o ../bin/gccReleaseXP/../../common/src/System/String/bstrlib.o ../bin/gccReleaseXP/../../common/src/System/String/Lexer.o ../bin/gccReleaseXP/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccReleaseXP/../../common/src/System/DataTypes/Bitset.o ../bin/gccReleaseXP/../../common/src/System/Memory/Allocator.o ../bin/gccReleaseXP/../../common/src/System/Memory/Pool.o ../bin/gccReleaseXP/../src/Network/Protocol.o ../bin/gccReleaseXP/../../common/src/Network/Socket.o ../bin/gccReleaseXP/../../common/src/Network/Win32/SocketHelper.o ../bin/gccReleaseXP/../src/Game/Application.o ../bin/gccReleaseXP/../src/Game/Character.o ../bin/gccReleaseXP/../src/Game/Configuration.o ../bin/gccReleaseXP/../src/Game/Display.o ../bin/gccReleaseXP/../src/Game/Index.o ../bin/gccReleaseXP/../src/Game/Intro.o ../bin/gccReleaseXP/../src/Game/Language.o ../bin/gccReleaseXP/../src/Game/Login.o ../bin/gccReleaseXP/../src/Game/User.o ../bin/gccReleaseXP/../../common/src/Math/General.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Matrix.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Plane.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Quaternion.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Rect.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Vector.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Sphere.o ../bin/gccReleaseXP/../../common/src/Gui/Button.o ../bin/gccReleaseXP/../../common/src/Gui/Clipboard.o ../bin/gccReleaseXP/../../common/src/Gui/EventHandler.o ../bin/gccReleaseXP/../../common/src/Gui/Form.o ../bin/gccReleaseXP/../../common/src/Gui/Label.o ../bin/gccReleaseXP/../../common/src/Gui/List.o ../bin/gccReleaseXP/../../common/src/Gui/TextBox.o ../bin/gccReleaseXP/../../common/src/Gui/Widget.o ../bin/gccReleaseXP/../../common/src/Gui/WindowManager.o ../bin/gccReleaseXP/../../common/src/Gui/WindowObj.o ../bin/gccReleaseXP/../../common/src/Gui/WindowParser.o ../bin/gccReleaseXP/../src/Scene/Camera.o ../bin/gccReleaseXP/../src/Scene/Entity.o ../bin/gccReleaseXP/../src/Scene/EntityManager.o ../bin/gccReleaseXP/../src/Scene/Light.o ../bin/gccReleaseXP/../src/Scene/Model.o ../bin/gccReleaseXP/../src/Scene/ModelLoader.o ../bin/gccReleaseXP/../src/Scene/ModelManager.o ../bin/gccReleaseXP/../src/Scene/Octree.o ../bin/gccReleaseXP/../src/Scene/Particle.o ../bin/gccReleaseXP/../src/Scene/Patch.o ../bin/gccReleaseXP/../src/Scene/Point.o ../bin/gccReleaseXP/../src/Scene/SceneManager.o ../bin/gccReleaseXP/../src/Scene/Sky.o ../bin/gccReleaseXP/../src/Scene/Terrain.o ../bin/gccReleaseXP/../src/Scene/TerrainLoader.o ../bin/gccReleaseXP/../src/Scene/TerrainManager.o ../bin/gccReleaseXP/../../common/src/Graphics/Color.o ../bin/gccReleaseXP/../../common/src/Graphics/Coordinates.o ../bin/gccReleaseXP/../../common/src/Graphics/Device.o ../bin/gccReleaseXP/../../common/src/Graphics/Fog.o ../bin/gccReleaseXP/../../common/src/Graphics/Font.o ../bin/gccReleaseXP/../../common/src/Graphics/IndexBuffer.o ../bin/gccReleaseXP/../../common/src/Graphics/MatrixStack.o ../bin/gccReleaseXP/../../common/src/Graphics/Shader.o ../bin/gccReleaseXP/../../common/src/Graphics/ShaderLoader.o ../bin/gccReleaseXP/../../common/src/Graphics/ShaderManager.o ../bin/gccReleaseXP/../../common/src/Graphics/StatesManager.o ../bin/gccReleaseXP/../../common/src/Graphics/Texture.o ../bin/gccReleaseXP/../../common/src/Graphics/TextureHelper.o ../bin/gccReleaseXP/../../common/src/Graphics/TextureLoader.o ../bin/gccReleaseXP/../../common/src/Graphics/TextureManager.o ../bin/gccReleaseXP/../../common/src/Graphics/TextureRender.o ../bin/gccReleaseXP/../../common/src/Graphics/VertexBuffer.o ../bin/gccReleaseXP/../../common/src/Graphics/VertexFormat.o ../bin/gccReleaseXP/../../common/src/Graphics/VertexTypes.o ../bin/gccReleaseXP/../../common/src/Graphics/View.o ../bin/gccReleaseXP/../../common/src/Graphics/Win32/VideoRender.o  $(ReleaseXP_Library_Path) $(ReleaseXP_Libraries) -Wl,-rpath,./ -o ../bin/gccReleaseXP/Client.exe

# Compiles file ../src/main.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/main.d
../bin/gccReleaseXP/../src/main.o: ../src/main.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/main.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/main.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/main.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/main.d

# Compiles file ../../common/src/Audio/OpenALHelper.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Audio/OpenALHelper.d
../bin/gccReleaseXP/../../common/src/Audio/OpenALHelper.o: ../../common/src/Audio/OpenALHelper.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Audio/OpenALHelper.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Audio/OpenALHelper.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Audio/OpenALHelper.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Audio/OpenALHelper.d

# Compiles file ../../common/src/Audio/Sound.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Audio/Sound.d
../bin/gccReleaseXP/../../common/src/Audio/Sound.o: ../../common/src/Audio/Sound.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Audio/Sound.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Audio/Sound.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Audio/Sound.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Audio/Sound.d

# Compiles file ../../common/src/Audio/SoundLoader.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Audio/SoundLoader.d
../bin/gccReleaseXP/../../common/src/Audio/SoundLoader.o: ../../common/src/Audio/SoundLoader.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Audio/SoundLoader.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Audio/SoundLoader.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Audio/SoundLoader.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Audio/SoundLoader.d

# Compiles file ../../common/src/Audio/SoundManager.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Audio/SoundManager.d
../bin/gccReleaseXP/../../common/src/Audio/SoundManager.o: ../../common/src/Audio/SoundManager.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Audio/SoundManager.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Audio/SoundManager.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Audio/SoundManager.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Audio/SoundManager.d

# Compiles file ../../common/src/Window/Settings.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Window/Settings.d
../bin/gccReleaseXP/../../common/src/Window/Settings.o: ../../common/src/Window/Settings.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Window/Settings.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Window/Settings.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Window/Settings.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Window/Settings.d

# Compiles file ../../common/src/Window/Win32/VideoModeSupport.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Window/Win32/VideoModeSupport.d
../bin/gccReleaseXP/../../common/src/Window/Win32/VideoModeSupport.o: ../../common/src/Window/Win32/VideoModeSupport.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Window/Win32/VideoModeSupport.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Window/Win32/VideoModeSupport.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Window/Win32/VideoModeSupport.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Window/Win32/VideoModeSupport.d

# Compiles file ../../common/src/Window/Win32/Window.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Window/Win32/Window.d
../bin/gccReleaseXP/../../common/src/Window/Win32/Window.o: ../../common/src/Window/Win32/Window.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Window/Win32/Window.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Window/Win32/Window.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Window/Win32/Window.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Window/Win32/Window.d

# Compiles file ../../common/src/System/CPUInfo.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/CPUInfo.d
../bin/gccReleaseXP/../../common/src/System/CPUInfo.o: ../../common/src/System/CPUInfo.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/CPUInfo.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/CPUInfo.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/CPUInfo.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/CPUInfo.d

# Compiles file ../../common/src/System/Dictionary.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Dictionary.d
../bin/gccReleaseXP/../../common/src/System/Dictionary.o: ../../common/src/System/Dictionary.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Dictionary.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Dictionary.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Dictionary.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Dictionary.d

# Compiles file ../../common/src/System/Error.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Error.d
../bin/gccReleaseXP/../../common/src/System/Error.o: ../../common/src/System/Error.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Error.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Error.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Error.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Error.d

# Compiles file ../../common/src/System/MD5.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/MD5.d
../bin/gccReleaseXP/../../common/src/System/MD5.o: ../../common/src/System/MD5.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/MD5.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/MD5.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/MD5.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/MD5.d

# Compiles file ../../common/src/System/Pack.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Pack.d
../bin/gccReleaseXP/../../common/src/System/Pack.o: ../../common/src/System/Pack.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Pack.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Pack.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Pack.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Pack.d

# Compiles file ../../common/src/System/Parser.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Parser.d
../bin/gccReleaseXP/../../common/src/System/Parser.o: ../../common/src/System/Parser.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Parser.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Parser.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Parser.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Parser.d

# Compiles file ../../common/src/System/Timer.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Timer.d
../bin/gccReleaseXP/../../common/src/System/Timer.o: ../../common/src/System/Timer.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Timer.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Timer.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Timer.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Timer.d

# Compiles file ../../common/src/System/TimerManager.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/TimerManager.d
../bin/gccReleaseXP/../../common/src/System/TimerManager.o: ../../common/src/System/TimerManager.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/TimerManager.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/TimerManager.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/TimerManager.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/TimerManager.d

# Compiles file ../../common/src/System/Win32/IOHelper.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Win32/IOHelper.d
../bin/gccReleaseXP/../../common/src/System/Win32/IOHelper.o: ../../common/src/System/Win32/IOHelper.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Win32/IOHelper.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Win32/IOHelper.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Win32/IOHelper.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Win32/IOHelper.d

# Compiles file ../../common/src/System/Win32/Mutex.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Win32/Mutex.d
../bin/gccReleaseXP/../../common/src/System/Win32/Mutex.o: ../../common/src/System/Win32/Mutex.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Win32/Mutex.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Win32/Mutex.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Win32/Mutex.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Win32/Mutex.d

# Compiles file ../../common/src/System/Win32/Platform.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Win32/Platform.d
../bin/gccReleaseXP/../../common/src/System/Win32/Platform.o: ../../common/src/System/Win32/Platform.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Win32/Platform.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Win32/Platform.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Win32/Platform.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Win32/Platform.d

# Compiles file ../../common/src/System/Win32/Thread.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Win32/Thread.d
../bin/gccReleaseXP/../../common/src/System/Win32/Thread.o: ../../common/src/System/Win32/Thread.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Win32/Thread.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Win32/Thread.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Win32/Thread.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Win32/Thread.d

# Compiles file ../../common/src/System/String/bstrlib.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/String/bstrlib.d
../bin/gccReleaseXP/../../common/src/System/String/bstrlib.o: ../../common/src/System/String/bstrlib.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/String/bstrlib.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/String/bstrlib.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/String/bstrlib.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/String/bstrlib.d

# Compiles file ../../common/src/System/String/Lexer.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/String/Lexer.d
../bin/gccReleaseXP/../../common/src/System/String/Lexer.o: ../../common/src/System/String/Lexer.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/String/Lexer.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/String/Lexer.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/String/Lexer.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/String/Lexer.d

# Compiles file ../../common/src/System/DataTypes/BinaryTree.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/DataTypes/BinaryTree.d
../bin/gccReleaseXP/../../common/src/System/DataTypes/BinaryTree.o: ../../common/src/System/DataTypes/BinaryTree.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/DataTypes/BinaryTree.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/DataTypes/BinaryTree.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/DataTypes/BinaryTree.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/DataTypes/BinaryTree.d

# Compiles file ../../common/src/System/DataTypes/Bitset.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/DataTypes/Bitset.d
../bin/gccReleaseXP/../../common/src/System/DataTypes/Bitset.o: ../../common/src/System/DataTypes/Bitset.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/DataTypes/Bitset.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/DataTypes/Bitset.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/DataTypes/Bitset.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/DataTypes/Bitset.d

# Compiles file ../../common/src/System/Memory/Allocator.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Memory/Allocator.d
../bin/gccReleaseXP/../../common/src/System/Memory/Allocator.o: ../../common/src/System/Memory/Allocator.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Memory/Allocator.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Memory/Allocator.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Memory/Allocator.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Memory/Allocator.d

# Compiles file ../../common/src/System/Memory/Pool.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Memory/Pool.d
../bin/gccReleaseXP/../../common/src/System/Memory/Pool.o: ../../common/src/System/Memory/Pool.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Memory/Pool.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Memory/Pool.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Memory/Pool.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Memory/Pool.d

# Compiles file ../src/Network/Protocol.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Network/Protocol.d
../bin/gccReleaseXP/../src/Network/Protocol.o: ../src/Network/Protocol.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Network/Protocol.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Network/Protocol.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Network/Protocol.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Network/Protocol.d

# Compiles file ../../common/src/Network/Socket.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Network/Socket.d
../bin/gccReleaseXP/../../common/src/Network/Socket.o: ../../common/src/Network/Socket.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Network/Socket.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Network/Socket.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Network/Socket.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Network/Socket.d

# Compiles file ../../common/src/Network/Win32/SocketHelper.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Network/Win32/SocketHelper.d
../bin/gccReleaseXP/../../common/src/Network/Win32/SocketHelper.o: ../../common/src/Network/Win32/SocketHelper.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Network/Win32/SocketHelper.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Network/Win32/SocketHelper.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Network/Win32/SocketHelper.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Network/Win32/SocketHelper.d

# Compiles file ../src/Game/Application.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/Application.d
../bin/gccReleaseXP/../src/Game/Application.o: ../src/Game/Application.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/Application.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/Application.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/Application.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/Application.d

# Compiles file ../src/Game/Character.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/Character.d
../bin/gccReleaseXP/../src/Game/Character.o: ../src/Game/Character.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/Character.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/Character.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/Character.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/Character.d

# Compiles file ../src/Game/Configuration.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/Configuration.d
../bin/gccReleaseXP/../src/Game/Configuration.o: ../src/Game/Configuration.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/Configuration.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/Configuration.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/Configuration.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/Configuration.d

# Compiles file ../src/Game/Display.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/Display.d
../bin/gccReleaseXP/../src/Game/Display.o: ../src/Game/Display.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/Display.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/Display.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/Display.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/Display.d

# Compiles file ../src/Game/Index.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/Index.d
../bin/gccReleaseXP/../src/Game/Index.o: ../src/Game/Index.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/Index.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/Index.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/Index.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/Index.d

# Compiles file ../src/Game/Intro.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/Intro.d
../bin/gccReleaseXP/../src/Game/Intro.o: ../src/Game/Intro.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/Intro.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/Intro.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/Intro.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/Intro.d

# Compiles file ../src/Game/Language.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/Language.d
../bin/gccReleaseXP/../src/Game/Language.o: ../src/Game/Language.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/Language.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/Language.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/Language.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/Language.d

# Compiles file ../src/Game/Login.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/Login.d
../bin/gccReleaseXP/../src/Game/Login.o: ../src/Game/Login.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/Login.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/Login.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/Login.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/Login.d

# Compiles file ../src/Game/User.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/User.d
../bin/gccReleaseXP/../src/Game/User.o: ../src/Game/User.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/User.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/User.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/User.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/User.d

# Compiles file ../../common/src/Math/General.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/General.d
../bin/gccReleaseXP/../../common/src/Math/General.o: ../../common/src/Math/General.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/General.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/General.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/General.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/General.d

# Compiles file ../../common/src/Math/Geometry/Matrix.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Matrix.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Matrix.o: ../../common/src/Math/Geometry/Matrix.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Matrix.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Matrix.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Matrix.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Matrix.d

# Compiles file ../../common/src/Math/Geometry/Plane.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Plane.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Plane.o: ../../common/src/Math/Geometry/Plane.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Plane.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Plane.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Plane.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Plane.d

# Compiles file ../../common/src/Math/Geometry/Quaternion.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Quaternion.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Quaternion.o: ../../common/src/Math/Geometry/Quaternion.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Quaternion.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Quaternion.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Quaternion.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Quaternion.d

# Compiles file ../../common/src/Math/Geometry/Rect.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Rect.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Rect.o: ../../common/src/Math/Geometry/Rect.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Rect.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Rect.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Rect.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Rect.d

# Compiles file ../../common/src/Math/Geometry/Vector.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Vector.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Vector.o: ../../common/src/Math/Geometry/Vector.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Vector.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Vector.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Vector.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Vector.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Box.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Box.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Box.o: ../../common/src/Math/Geometry/Bounding/Box.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Box.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Box.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Box.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Box.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Frustum.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Frustum.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Frustum.o: ../../common/src/Math/Geometry/Bounding/Frustum.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Frustum.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Frustum.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Frustum.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Frustum.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Quad.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Quad.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Quad.o: ../../common/src/Math/Geometry/Bounding/Quad.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Quad.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Quad.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Quad.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Quad.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Sphere.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Sphere.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Sphere.o: ../../common/src/Math/Geometry/Bounding/Sphere.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Sphere.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Sphere.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Sphere.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Sphere.d

# Compiles file ../../common/src/Gui/Button.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Gui/Button.d
../bin/gccReleaseXP/../../common/src/Gui/Button.o: ../../common/src/Gui/Button.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Gui/Button.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Gui/Button.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Gui/Button.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Gui/Button.d

# Compiles file ../../common/src/Gui/Clipboard.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Gui/Clipboard.d
../bin/gccReleaseXP/../../common/src/Gui/Clipboard.o: ../../common/src/Gui/Clipboard.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Gui/Clipboard.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Gui/Clipboard.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Gui/Clipboard.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Gui/Clipboard.d

# Compiles file ../../common/src/Gui/EventHandler.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Gui/EventHandler.d
../bin/gccReleaseXP/../../common/src/Gui/EventHandler.o: ../../common/src/Gui/EventHandler.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Gui/EventHandler.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Gui/EventHandler.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Gui/EventHandler.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Gui/EventHandler.d

# Compiles file ../../common/src/Gui/Form.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Gui/Form.d
../bin/gccReleaseXP/../../common/src/Gui/Form.o: ../../common/src/Gui/Form.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Gui/Form.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Gui/Form.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Gui/Form.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Gui/Form.d

# Compiles file ../../common/src/Gui/Label.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Gui/Label.d
../bin/gccReleaseXP/../../common/src/Gui/Label.o: ../../common/src/Gui/Label.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Gui/Label.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Gui/Label.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Gui/Label.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Gui/Label.d

# Compiles file ../../common/src/Gui/List.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Gui/List.d
../bin/gccReleaseXP/../../common/src/Gui/List.o: ../../common/src/Gui/List.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Gui/List.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Gui/List.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Gui/List.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Gui/List.d

# Compiles file ../../common/src/Gui/TextBox.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Gui/TextBox.d
../bin/gccReleaseXP/../../common/src/Gui/TextBox.o: ../../common/src/Gui/TextBox.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Gui/TextBox.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Gui/TextBox.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Gui/TextBox.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Gui/TextBox.d

# Compiles file ../../common/src/Gui/Widget.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Gui/Widget.d
../bin/gccReleaseXP/../../common/src/Gui/Widget.o: ../../common/src/Gui/Widget.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Gui/Widget.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Gui/Widget.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Gui/Widget.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Gui/Widget.d

# Compiles file ../../common/src/Gui/WindowManager.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Gui/WindowManager.d
../bin/gccReleaseXP/../../common/src/Gui/WindowManager.o: ../../common/src/Gui/WindowManager.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Gui/WindowManager.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Gui/WindowManager.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Gui/WindowManager.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Gui/WindowManager.d

# Compiles file ../../common/src/Gui/WindowObj.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Gui/WindowObj.d
../bin/gccReleaseXP/../../common/src/Gui/WindowObj.o: ../../common/src/Gui/WindowObj.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Gui/WindowObj.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Gui/WindowObj.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Gui/WindowObj.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Gui/WindowObj.d

# Compiles file ../../common/src/Gui/WindowParser.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Gui/WindowParser.d
../bin/gccReleaseXP/../../common/src/Gui/WindowParser.o: ../../common/src/Gui/WindowParser.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Gui/WindowParser.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Gui/WindowParser.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Gui/WindowParser.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Gui/WindowParser.d

# Compiles file ../src/Scene/Camera.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/Camera.d
../bin/gccReleaseXP/../src/Scene/Camera.o: ../src/Scene/Camera.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/Camera.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/Camera.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/Camera.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/Camera.d

# Compiles file ../src/Scene/Entity.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/Entity.d
../bin/gccReleaseXP/../src/Scene/Entity.o: ../src/Scene/Entity.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/Entity.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/Entity.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/Entity.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/Entity.d

# Compiles file ../src/Scene/EntityManager.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/EntityManager.d
../bin/gccReleaseXP/../src/Scene/EntityManager.o: ../src/Scene/EntityManager.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/EntityManager.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/EntityManager.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/EntityManager.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/EntityManager.d

# Compiles file ../src/Scene/Light.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/Light.d
../bin/gccReleaseXP/../src/Scene/Light.o: ../src/Scene/Light.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/Light.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/Light.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/Light.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/Light.d

# Compiles file ../src/Scene/Model.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/Model.d
../bin/gccReleaseXP/../src/Scene/Model.o: ../src/Scene/Model.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/Model.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/Model.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/Model.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/Model.d

# Compiles file ../src/Scene/ModelLoader.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/ModelLoader.d
../bin/gccReleaseXP/../src/Scene/ModelLoader.o: ../src/Scene/ModelLoader.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/ModelLoader.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/ModelLoader.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/ModelLoader.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/ModelLoader.d

# Compiles file ../src/Scene/ModelManager.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/ModelManager.d
../bin/gccReleaseXP/../src/Scene/ModelManager.o: ../src/Scene/ModelManager.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/ModelManager.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/ModelManager.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/ModelManager.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/ModelManager.d

# Compiles file ../src/Scene/Octree.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/Octree.d
../bin/gccReleaseXP/../src/Scene/Octree.o: ../src/Scene/Octree.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/Octree.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/Octree.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/Octree.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/Octree.d

# Compiles file ../src/Scene/Particle.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/Particle.d
../bin/gccReleaseXP/../src/Scene/Particle.o: ../src/Scene/Particle.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/Particle.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/Particle.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/Particle.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/Particle.d

# Compiles file ../src/Scene/Patch.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/Patch.d
../bin/gccReleaseXP/../src/Scene/Patch.o: ../src/Scene/Patch.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/Patch.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/Patch.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/Patch.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/Patch.d

# Compiles file ../src/Scene/Point.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/Point.d
../bin/gccReleaseXP/../src/Scene/Point.o: ../src/Scene/Point.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/Point.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/Point.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/Point.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/Point.d

# Compiles file ../src/Scene/SceneManager.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/SceneManager.d
../bin/gccReleaseXP/../src/Scene/SceneManager.o: ../src/Scene/SceneManager.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/SceneManager.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/SceneManager.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/SceneManager.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/SceneManager.d

# Compiles file ../src/Scene/Sky.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/Sky.d
../bin/gccReleaseXP/../src/Scene/Sky.o: ../src/Scene/Sky.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/Sky.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/Sky.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/Sky.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/Sky.d

# Compiles file ../src/Scene/Terrain.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/Terrain.d
../bin/gccReleaseXP/../src/Scene/Terrain.o: ../src/Scene/Terrain.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/Terrain.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/Terrain.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/Terrain.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/Terrain.d

# Compiles file ../src/Scene/TerrainLoader.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/TerrainLoader.d
../bin/gccReleaseXP/../src/Scene/TerrainLoader.o: ../src/Scene/TerrainLoader.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/TerrainLoader.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/TerrainLoader.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/TerrainLoader.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/TerrainLoader.d

# Compiles file ../src/Scene/TerrainManager.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Scene/TerrainManager.d
../bin/gccReleaseXP/../src/Scene/TerrainManager.o: ../src/Scene/TerrainManager.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Scene/TerrainManager.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Scene/TerrainManager.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Scene/TerrainManager.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Scene/TerrainManager.d

# Compiles file ../../common/src/Graphics/Color.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/Color.d
../bin/gccReleaseXP/../../common/src/Graphics/Color.o: ../../common/src/Graphics/Color.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/Color.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/Color.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/Color.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/Color.d

# Compiles file ../../common/src/Graphics/Coordinates.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/Coordinates.d
../bin/gccReleaseXP/../../common/src/Graphics/Coordinates.o: ../../common/src/Graphics/Coordinates.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/Coordinates.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/Coordinates.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/Coordinates.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/Coordinates.d

# Compiles file ../../common/src/Graphics/Device.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/Device.d
../bin/gccReleaseXP/../../common/src/Graphics/Device.o: ../../common/src/Graphics/Device.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/Device.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/Device.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/Device.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/Device.d

# Compiles file ../../common/src/Graphics/Fog.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/Fog.d
../bin/gccReleaseXP/../../common/src/Graphics/Fog.o: ../../common/src/Graphics/Fog.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/Fog.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/Fog.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/Fog.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/Fog.d

# Compiles file ../../common/src/Graphics/Font.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/Font.d
../bin/gccReleaseXP/../../common/src/Graphics/Font.o: ../../common/src/Graphics/Font.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/Font.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/Font.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/Font.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/Font.d

# Compiles file ../../common/src/Graphics/IndexBuffer.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/IndexBuffer.d
../bin/gccReleaseXP/../../common/src/Graphics/IndexBuffer.o: ../../common/src/Graphics/IndexBuffer.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/IndexBuffer.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/IndexBuffer.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/IndexBuffer.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/IndexBuffer.d

# Compiles file ../../common/src/Graphics/MatrixStack.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/MatrixStack.d
../bin/gccReleaseXP/../../common/src/Graphics/MatrixStack.o: ../../common/src/Graphics/MatrixStack.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/MatrixStack.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/MatrixStack.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/MatrixStack.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/MatrixStack.d

# Compiles file ../../common/src/Graphics/Shader.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/Shader.d
../bin/gccReleaseXP/../../common/src/Graphics/Shader.o: ../../common/src/Graphics/Shader.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/Shader.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/Shader.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/Shader.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/Shader.d

# Compiles file ../../common/src/Graphics/ShaderLoader.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/ShaderLoader.d
../bin/gccReleaseXP/../../common/src/Graphics/ShaderLoader.o: ../../common/src/Graphics/ShaderLoader.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/ShaderLoader.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/ShaderLoader.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/ShaderLoader.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/ShaderLoader.d

# Compiles file ../../common/src/Graphics/ShaderManager.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/ShaderManager.d
../bin/gccReleaseXP/../../common/src/Graphics/ShaderManager.o: ../../common/src/Graphics/ShaderManager.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/ShaderManager.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/ShaderManager.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/ShaderManager.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/ShaderManager.d

# Compiles file ../../common/src/Graphics/StatesManager.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/StatesManager.d
../bin/gccReleaseXP/../../common/src/Graphics/StatesManager.o: ../../common/src/Graphics/StatesManager.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/StatesManager.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/StatesManager.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/StatesManager.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/StatesManager.d

# Compiles file ../../common/src/Graphics/Texture.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/Texture.d
../bin/gccReleaseXP/../../common/src/Graphics/Texture.o: ../../common/src/Graphics/Texture.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/Texture.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/Texture.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/Texture.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/Texture.d

# Compiles file ../../common/src/Graphics/TextureHelper.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/TextureHelper.d
../bin/gccReleaseXP/../../common/src/Graphics/TextureHelper.o: ../../common/src/Graphics/TextureHelper.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/TextureHelper.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/TextureHelper.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/TextureHelper.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/TextureHelper.d

# Compiles file ../../common/src/Graphics/TextureLoader.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/TextureLoader.d
../bin/gccReleaseXP/../../common/src/Graphics/TextureLoader.o: ../../common/src/Graphics/TextureLoader.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/TextureLoader.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/TextureLoader.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/TextureLoader.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/TextureLoader.d

# Compiles file ../../common/src/Graphics/TextureManager.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/TextureManager.d
../bin/gccReleaseXP/../../common/src/Graphics/TextureManager.o: ../../common/src/Graphics/TextureManager.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/TextureManager.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/TextureManager.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/TextureManager.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/TextureManager.d

# Compiles file ../../common/src/Graphics/TextureRender.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/TextureRender.d
../bin/gccReleaseXP/../../common/src/Graphics/TextureRender.o: ../../common/src/Graphics/TextureRender.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/TextureRender.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/TextureRender.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/TextureRender.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/TextureRender.d

# Compiles file ../../common/src/Graphics/VertexBuffer.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/VertexBuffer.d
../bin/gccReleaseXP/../../common/src/Graphics/VertexBuffer.o: ../../common/src/Graphics/VertexBuffer.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/VertexBuffer.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/VertexBuffer.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/VertexBuffer.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/VertexBuffer.d

# Compiles file ../../common/src/Graphics/VertexFormat.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/VertexFormat.d
../bin/gccReleaseXP/../../common/src/Graphics/VertexFormat.o: ../../common/src/Graphics/VertexFormat.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/VertexFormat.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/VertexFormat.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/VertexFormat.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/VertexFormat.d

# Compiles file ../../common/src/Graphics/VertexTypes.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/VertexTypes.d
../bin/gccReleaseXP/../../common/src/Graphics/VertexTypes.o: ../../common/src/Graphics/VertexTypes.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/VertexTypes.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/VertexTypes.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/VertexTypes.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/VertexTypes.d

# Compiles file ../../common/src/Graphics/View.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/View.d
../bin/gccReleaseXP/../../common/src/Graphics/View.o: ../../common/src/Graphics/View.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/View.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/View.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/View.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/View.d

# Compiles file ../../common/src/Graphics/Win32/VideoRender.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Graphics/Win32/VideoRender.d
../bin/gccReleaseXP/../../common/src/Graphics/Win32/VideoRender.o: ../../common/src/Graphics/Win32/VideoRender.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Graphics/Win32/VideoRender.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Graphics/Win32/VideoRender.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Graphics/Win32/VideoRender.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Graphics/Win32/VideoRender.d

# Creates the intermediate and output folders for each configuration...
.PHONY: create_folders
create_folders:
	mkdir -p ../bin/gccDebug/source
	mkdir -p ../bin/gccRelease/source
	mkdir -p ../bin/gccDebugXP/source
	mkdir -p ../bin/gccReleaseXP/source

# Cleans intermediate and output files (objects, libraries, executables)...
.PHONY: clean
clean:
	rm -f ../bin/gccDebug/*.o
	rm -f ../bin/gccDebug/*.d
	rm -f ../bin/gccDebug/*.a
	rm -f ../bin/gccDebug/*.so
	rm -f ../bin/gccDebug/*.dll
	rm -f ../bin/gccDebug/*.exe
	rm -f ../bin/gccRelease/*.o
	rm -f ../bin/gccRelease/*.d
	rm -f ../bin/gccRelease/*.a
	rm -f ../bin/gccRelease/*.so
	rm -f ../bin/gccRelease/*.dll
	rm -f ../bin/gccRelease/*.exe
	rm -f ../bin/gccDebugXP/*.o
	rm -f ../bin/gccDebugXP/*.d
	rm -f ../bin/gccDebugXP/*.a
	rm -f ../bin/gccDebugXP/*.so
	rm -f ../bin/gccDebugXP/*.dll
	rm -f ../bin/gccDebugXP/*.exe
	rm -f ../bin/gccReleaseXP/*.o
	rm -f ../bin/gccReleaseXP/*.d
	rm -f ../bin/gccReleaseXP/*.a
	rm -f ../bin/gccReleaseXP/*.so
	rm -f ../bin/gccReleaseXP/*.dll
	rm -f ../bin/gccReleaseXP/*.exe

