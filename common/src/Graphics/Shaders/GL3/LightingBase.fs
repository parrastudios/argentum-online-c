#version 330

const int MAX_POINT_LIGHTS = 2;
const int MAX_SPOT_LIGHTS = 2;

in vec2 TexCoord0;
in vec3 Normal0;
in vec3 WorldPos0;

out vec4 FragColor;

struct BaseLight
{
    vec3 Color;
    float AmbientIntensity;
    float DiffuseIntensity;
};

struct DirectionalLight
{
    BaseLight Base;
    vec3 Direction;
};

struct Attenuation
{
    float Constant;
    float Linear;
    float Exp;
};

struct PointLight
{
    BaseLight Base;
    vec3 Position;
    Attenuation Atten;
};

struct SpotLight
{
    PointLight Base;
    vec3 Direction;
    float Cutoff;
};

uniform int PointLightsSize;
uniform int SpotLightsSize;
uniform DirectionalLight DirectLight;
uniform PointLight PointLights[MAX_POINT_LIGHTS];
uniform SpotLight SpotLights[MAX_SPOT_LIGHTS];
uniform sampler2D ColorMap;
uniform vec3 EyeWorldPos;
uniform float MatSpecularIntensity;
uniform float SpecularPower;

vec4 CalcLightInternal(BaseLight Light, vec3 LightDirection, vec3 Normal)
{
    vec4 AmbientColor = vec4(Light.Color * Light.AmbientIntensity, 1.0f);
    float DiffuseFactor = dot(Normal, -LightDirection);

    vec4 DiffuseColor  = vec4(0, 0, 0, 0);
    vec4 SpecularColor = vec4(0, 0, 0, 0);

    if (DiffuseFactor > 0)
    {
        DiffuseColor = vec4(Light.Color * Light.DiffuseIntensity * DiffuseFactor, 1.0f);

        vec3 VertexToEye = normalize(gEyeWorldPos - WorldPos0);
        vec3 LightReflect = normalize(reflect(LightDirection, Normal));
        float SpecularFactor = dot(VertexToEye, LightReflect);

        if (SpecularFactor > 0)
        {
            SpecularFactor = pow(SpecularFactor, SpecularPower);
            SpecularColor = vec4(Light.Color * MatSpecularIntensity * SpecularFactor, 1.0f);
        }
    }

    return (AmbientColor + DiffuseColor + SpecularColor);
}

vec4 CalcDirectionalLight(vec3 Normal)
{
    return CalcLightInternal(DirectionalLight.Base, DirectionalLight.Direction, Normal);
}

vec4 CalcPointLight(PointLight Light, vec3 Normal)
{
    vec3 LightDirection = WorldPos0 - Light.Position;
    float Distance = length(LightDirection);
    LightDirection = normalize(LightDirection);

    vec4 Color = CalcLightInternal(Light.Base, LightDirection, Normal);

    float Attenuation =  Light.Atten.Constant +
                         Light.Atten.Linear * Distance +
                         Light.Atten.Exp * Distance * Distance;

    return Color / Attenuation;
}

vec4 CalcSpotLight(SpotLight Light, vec3 Normal)
{
    vec3 LightToPixel = normalize(WorldPos0 - Light.Base.Position);
    float SpotFactor = dot(LightToPixel, Light.Direction);

    if (SpotFactor > Light.Cutoff)
    {
        vec4 Color = CalcPointLight(Light.Base, Normal);
        return Color * (1.0 - (1.0 - SpotFactor) * 1.0 / (1.0 - Light.Cutoff));
    }
    else
    {
        return vec4(0, 0, 0, 0);
    }
}

void main()
{
    vec3 Normal = normalize(Normal0);
    vec4 TotalLight = CalcDirectionalLight(Normal);

    for (int i = 0; i < PointLightsSize; ++i)
    {
        TotalLight += CalcPointLight(PointLights[i], Normal);
    }

    for (int i = 0; i < SpotLightsSize; ++i)
    {
        TotalLight += CalcSpotLight(SpotLights[i], Normal);
    }

    FragColor = texture(ColorMap, TexCoord0.xy) * TotalLight;
}
