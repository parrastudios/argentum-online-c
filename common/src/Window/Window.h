/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WINDOW_HANDLE_H
#define WINDOW_HANDLE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <Window/VideoModeSupport.h>
#include <Window/Settings.h>
#include <Window/Event.h>
#include <Window/Keyboard.h>

////////////////////////////////////////////////////////////
/// Define a low-level window handle type, specific to
/// each platform
////////////////////////////////////////////////////////////
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT

    // Windows defines a void* handle (HWND)
    typedef void * WindowHandle;

#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD

    // Unix - X11 defines an unsigned integer handle (Window)
    typedef unsigned long WindowHandle;

#elif PLATFORM_TYPE == PLATFORM_MACOS

    // Mac OS X defines a void * handle (NSWindow)
	typedef void* WindowHandle;

#endif

////////////////////////////////////////////////////////////
/// Enumeration of window creation styles
////////////////////////////////////////////////////////////
enum Style
{
	NonStyle   = 0,      ///< No border / title bar (this flag and all others are mutually exclusive)
	Titlebar   = 1 << 0, ///< Title bar + fixed border
	Resize     = 1 << 1, ///< Titlebar + resizable border + maximize button
	Close      = 1 << 2, ///< Titlebar + close button
	Fullscreen = 1 << 3  ///< Fullscreen mode (this flag and all others are mutually exclusive)
};

////////////////////////////////////////////////////////////
/// Window is a rendering window ; it can create a new window
/// or connect to an existing one
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create (or recreate) the window
///
/// \param Mode :        Video mode to use
/// \param Title :       Title of the window
/// \param WindowStyle : Window style (Resize | Close by default)
/// \param Params :      Creation parameters (see default constructor for default values)
///
////////////////////////////////////////////////////////////
bool WindowCreate(struct VideoMode * Mode, char * Title, unsigned long WindowStyle, struct WindowSettings * Params);

////////////////////////////////////////////////////////////
/// Close (destroy) the window.
/// The Window instance remains valid and you can call
/// Create to recreate the window
///
////////////////////////////////////////////////////////////
void WindowClose();

////////////////////////////////////////////////////////////
/// Show or hide the mouse cursor
///
/// \param Show : True to show, false to hide
///
////////////////////////////////////////////////////////////
void WindowShowMouseCursor(bool Show);

////////////////////////////////////////////////////////////
/// Change the position of the mouse cursor
///
/// \param Left : Left coordinate of the cursor, relative to the window
/// \param Top :  Top coordinate of the cursor, relative to the window
///
////////////////////////////////////////////////////////////
void WindowSetCursorPosition(uint32 Left, uint32 Top);

////////////////////////////////////////////////////////////
/// Change the position of the window on screen.
/// Only works for top-level windows
///
/// \param Left : Left position
/// \param Top :  Top position
///
////////////////////////////////////////////////////////////
void WindowSetPosition(int32 Left, int32 Top);

////////////////////////////////////////////////////////////
/// Change the size of the rendering region of the window
///
/// \param Width :  New width
/// \param Height : New height
///
////////////////////////////////////////////////////////////
void WindowSetSize(uint32 Width, uint32 Height);

////////////////////////////////////////////////////////////
/// Show or hide the window
///
/// \param State : True to show, false to hide
///
////////////////////////////////////////////////////////////
void WindowShow(bool State);

////////////////////////////////////////////////////////////
/// Change the window's icon
///
/// \param Width :  Icon's width, in pixels
/// \param Height : Icon's height, in pixels
/// \param Pixels : Pointer to the pixels in memory, format must be RGBA 32 bits
///
////////////////////////////////////////////////////////////
void WindowSetIcon(uint32 Width, uint32 Height, uint8* Pixels);

////////////////////////////////////////////////////////////
/// Activate of deactivate the window as the current target
/// for rendering
///
/// \param Active : True to activate, false to deactivate (true by default)
///
/// \return True if operation was successful, false otherwise
///
////////////////////////////////////////////////////////////
void WindowSetActive(bool Active);

////////////////////////////////////////////////////////////
/// Display the window on screen
///
////////////////////////////////////////////////////////////
void WindowDisplay();

////////////////////////////////////////////////////////////
/// Process incoming events from operating system
///
////////////////////////////////////////////////////////////
void WindowProcessEvents();

////////////////////////////////////////////////////////////
/// Get the event struct from the window
////////////////////////////////////////////////////////////
void WindowGetEvent(struct Event * Copy);

////////////////////////////////////////////////////////////
/// Clear the event struct from the window
////////////////////////////////////////////////////////////
void WindowClearEvent();

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
uint32					WindowWidth;			///< Window width size
uint32					WindowHeight;			///< Window height size

#endif // WINDOW_HANDLE_H
