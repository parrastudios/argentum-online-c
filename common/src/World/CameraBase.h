/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_CAMERA_BASE_H
#define WORLD_CAMERA_BASE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Component.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define CAMERA_BASE_NAME_LENGTH			UINTEGER_SUFFIX(0x20)
#define CAMERA_BASE_SPRING_DEFAULT		12.0f
#define CAMERA_BASE_SPRING_DISABLE		0.0f
#define CAMERA_BASE_FOVX_DEFAULT		80.0f
#define CAMERA_BASE_ZNEAR_DEFAULT		1.0f
#define CAMERA_BASE_ZFAR_DEFAULT		12600.0f

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct CameraComponentBaseData
{
	bool 					EnableSpring;
	float 					SpringConstant;
	float					DampingConstant;

	float					FovX;
	float					FovY;
	float					Near;
	float					Far;

	struct Vector3f			AxisX;
	struct Vector3f			AxisY;
	struct Vector3f			AxisZ;

	struct Vector3f			ViewDirection;

	struct Vector3f			Eye;
	struct Vector3f			Target;
	struct Vector3f			TargetAxisY;

	char					Name[CAMERA_BASE_NAME_LENGTH];
};

struct CameraComponentBaseType
{
	//< Initialize base camera properties (spring disabled)
	struct EntityType * (*Initialize)(struct EntityType * Entity, char * Name);

	//< Initialize base camera properties (spring enabled)
	struct EntityType * (*InitializeSpring)(struct EntityType * Entity, char * Name, float Spring);

	//< Set camera spring constant
	void (*SetSpring)(struct EntityType * Entity, float Spring);

	//< Set camera perspective
	void (*Perspective)(struct EntityType * Entity, float FovX, float Aspect, float Near, float Far);

	//< Look camera at a target with eye and up vectors
	void (*LookAt)(struct EntityType * Entity, struct Vector3f * Eye, struct Vector3f * Target, struct Vector3f * Up);
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the camera base implementation attributes
////////////////////////////////////////////////////////////
const void * CameraComponentBaseAttr();

////////////////////////////////////////////////////////////
/// Return the camera base implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * CameraComponentBaseImpl();

////////////////////////////////////////////////////////////
/// Return the camera base derived data size
////////////////////////////////////////////////////////////
uinteger CameraComponentBaseSize();

#endif // WORLD_CAMERA_BASE_H
