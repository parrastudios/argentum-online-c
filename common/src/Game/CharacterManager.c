/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/CharacterManager.h>
#include <Game/Character.h>
#include <DataBase/Character.h>

////////////////////////////////////////////////////////////
/// Gets concrete object type from manager
////////////////////////////////////////////////////////////
uint32 CharacterManagerTypeImpl(struct CharacterManagerType * Manager)
{
	switch (Manager->Type)
	{
		case GAME_CHARACTER_MANAGER_TYPE_PLAYER :
		{
			return CHARACTER_TYPE_PLAYER;
		}

		case GAME_CHARACTER_MANAGER_TYPE_NPC :
		{
			return CHARACTER_TYPE_NPC;
		}
	}

	return CHARACTER_TYPE_INVALID;
}

////////////////////////////////////////////////////////////
/// Initialize character manager
////////////////////////////////////////////////////////////
bool CharacterManagerInitialize(struct CharacterManagerType * Manager, uint32 Type)
{
	// Set manager type
	Manager->Type = Type;

	// Initialize character data
	Manager->CharacterData = VectorNew(CharacterGetSize(CharacterManagerTypeImpl(Manager)));

	// Load all players (sure?)
	if (CharacterManagerLoad(Manager))
	{
		// ...

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Load character manager from persistence
////////////////////////////////////////////////////////////
bool CharacterManagerLoad(struct CharacterManagerType * Manager)
{
	if (Manager->Type == GAME_CHARACTER_MANAGER_TYPE_PLAYER)
	{
		if (VectorEmpty(Manager->CharacterData))
		{
			// If empty load all players
			return DBPlayerLoadList(Manager->CharacterData);
		}
		else
		{
			// If not, reload data
			return DBPlayerReloadList(Manager->CharacterData);
		}
	}
	else if (Manager->Type == GAME_CHARACTER_MANAGER_TYPE_NPC)
	{
		if (VectorEmpty(Manager->CharacterData))
		{
			// If empty load all npcs
			return DBNpcLoadList(Manager->CharacterData);
		}
		else
		{
			// If not, reload data
			return DBNpcReloadList(Manager->CharacterData);
		}
	}

	return false;
}
////////////////////////////////////////////////////////////
/// Stores character manager into persistence
////////////////////////////////////////////////////////////
bool CharacterManagerStore(struct CharacterManagerType * Manager)
{
	if (!VectorEmpty(Manager->CharacterData))
	{
		if (Manager->Type == GAME_CHARACTER_MANAGER_TYPE_PLAYER)
		{
			return DBPlayerStoreList(Manager->CharacterData);
		}
		else if (Manager->Type == GAME_CHARACTER_MANAGER_TYPE_NPC)
		{
			return DBNpcStoreList(Manager->CharacterData);
		}
	}

	return false;
}
////////////////////////////////////////////////////////////
/// Destroy character manager
////////////////////////////////////////////////////////////
void CharacterManagerDestroy(struct CharacterManagerType * Manager)
{
	if (!CharacterManagerStore(Manager))
	{
		// Handle error
	}

	VectorDestroy(Manager->CharacterData);
}

////////////////////////////////////////////////////////////
/// Create a new character
////////////////////////////////////////////////////////////
struct CharacterData * CharacterManagerCreate(struct CharacterManagerType * Manager)
{
	uint32 CharacterType = CharacterManagerTypeImpl(Manager);

	struct CharacterData * Character = CharacterCreate(CharacterType);

	if (Character)
	{
		CharacterInitialize(Character, CharacterType);

		return Character;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Add a new character to the manager
////////////////////////////////////////////////////////////
struct CharacterData * CharacterManagerAdd(struct CharacterManagerType * Manager, struct CharacterData * Character)
{
	if (Character)
	{
		VectorPushBack(Manager->CharacterData, Character);
		
		return (struct CharacterData*)VectorBack(Manager->CharacterData);
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Add a list of characters to the manager
////////////////////////////////////////////////////////////
void CharacterManagerAddList(struct CharacterManagerType * Manager, Vector DBCharacterList, Vector References)
{
	uint32 i;

	for (i = 0; i < VectorSize(DBCharacterList); i++)
	{
		struct DBCharacterData * DBCharacter = (struct DBCharacterData *)VectorAt(DBCharacterList, i);
		uinteger Position = 0;

		// Insert a new empty character
		VectorPushBackEmpty(Manager->CharacterData);

		// Initialize character
		CharacterInitialize((struct CharacterData*)VectorBack(Manager->CharacterData), CharacterManagerTypeImpl(Manager));

		// Copy database character to game ones
		DBCharacterToCharacter(DBCharacter, (struct CharacterData*)VectorBack(Manager->CharacterData), CharacterManagerTypeImpl(Manager));

		// Save reference to the character in the account list
		Position = VectorSize(Manager->CharacterData) - 1;

		VectorPushBack(References, &Position);
	}
}

////////////////////////////////////////////////////////////
/// Get a character by position
////////////////////////////////////////////////////////////
struct CharacterData * CharacterManagerGetByPosition(struct CharacterManagerType * Manager, uint32 Position)
{
	return (struct CharacterData *)VectorAt(Manager->CharacterData, Position);
}


////////////////////////////////////////////////////////////
/// Get a character by id
////////////////////////////////////////////////////////////
struct CharacterData * CharacterManagerGetById(struct CharacterManagerType * Manager, uint32 Id)
{
	uint32 i;

	for (i = 0; i < VectorSize(Manager->CharacterData); i++)
	{
		if ((VectorAtT(Manager->CharacterData, i, struct CharacterData)).Index == Id)
		{
			return VectorAt(Manager->CharacterData, i);
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Remove a character from manager by its id
////////////////////////////////////////////////////////////
void CharacterManagerRemoveById(struct CharacterManagerType * Manager, uint32 Id)
{
	uint32 i;

	for (i = 0; i < VectorSize(Manager->CharacterData); i++)
	{
		struct CharacterData * Character = VectorAt(Manager->CharacterData, i);

		if (Character->Index == Id)
		{
			CharacterDestroy(Character);

			VectorErase(Manager->CharacterData, i);
		}
	}
}

////////////////////////////////////////////////////////////
/// Remove a character from manager
////////////////////////////////////////////////////////////
void CharacterManagerRemove(struct CharacterManagerType * Manager, struct CharacterData * Character)
{
	CharacterManagerRemoveById(Manager, Character->Index);
}
