/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/IndexBuffer.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

struct IndexBuffer * IndexBufferCreate(uint32 Size, bool Dynamic, uint32 IndexSize)
{
	struct IndexBuffer * Buffer = (struct IndexBuffer*)MemoryAllocate(sizeof(struct IndexBuffer));

	if (Buffer)
	{
		Buffer->Size		= Size;
		Buffer->Dynamic		= Dynamic;
		Buffer->Count		= 0;
		Buffer->IndexSize	= IndexSize;

		if (Buffer->IndexSize == sizeof(uint16))
		{
			Buffer->Data = Buffer->Data16 = (uint16*)MemoryAllocate(sizeof(uint16) * Buffer->Size);
		}
		else if (Buffer->IndexSize == sizeof(uint32))
		{
			Buffer->Data = Buffer->Data32 = (uint32*)MemoryAllocate(sizeof(uint32) * Buffer->Size);
		}
		else
		{
			// error
			Buffer->Data = Buffer->Data32 = NULL;
			Buffer->IndexSize = 0;
		}
	}

	return Buffer;
}

void IndexBufferDestroy(struct IndexBuffer * Buffer)
{
	if (Buffer)
	{
		if (Buffer->Data)
		{
			MemoryDeallocate(Buffer->Data);
		}

		MemoryDeallocate(Buffer);
	}
}

void IndexBufferReset(struct IndexBuffer * Buffer)
{
	Buffer->Count = 0;
}

void IndexBufferAdd(struct IndexBuffer * Buffer, uint32 Index)
{
	if (IndexBufferIsAvailable(Buffer, 1))
	{
		if (Buffer->IndexSize == sizeof(uint16))
		{
			Buffer->Data16[Buffer->Count++] = (uint16)Index;
		}
		else if (Buffer->IndexSize == sizeof(uint32))
		{
			Buffer->Data32[Buffer->Count++] = (uint32)Index;
		}
		
	}
}

void IndexBufferAddTriangle(struct IndexBuffer * Buffer, uint32 VertexBase)
{
	if (IndexBufferIsAvailable(Buffer, 3))
	{
		if (Buffer->IndexSize == sizeof(uint16))
		{
			uint16 * Data = &Buffer->Data16[Buffer->Count];

			Data[0] = VertexBase	;
			Data[1] = VertexBase + 1;
			Data[2] = VertexBase + 2;
		}
		else if (Buffer->IndexSize == sizeof(uint32))
		{
			uint32 * Data = &Buffer->Data32[Buffer->Count];

			Data[0] = VertexBase	;
			Data[1] = VertexBase + 1;
			Data[2] = VertexBase + 2;
		}
		
		Buffer->Count += 3;
	}
}

void IndexBufferAddQuad(struct IndexBuffer * Buffer, uint32 VertexBase)
{
    if (IndexBufferIsAvailable(Buffer, 6))
	{
		// v0 -------- v1/v4
		// |		 /	|
		// |	  /		|
		// |   /		|
		// v2/v3 ------ v5

		if (Buffer->IndexSize == sizeof(uint16))
		{
			uint16 * Data = &Buffer->Data16[Buffer->Count];

			// First triangle
			Data[0] = (uint16)VertexBase	;
			Data[1] = (uint16)VertexBase + 1;
			Data[2] = (uint16)VertexBase + 2;

			// Second triangle
			Data[3] = (uint16)VertexBase + 2;
			Data[4] = (uint16)VertexBase + 1;
			Data[5] = (uint16)VertexBase + 3;
		}
		else if (Buffer->IndexSize == sizeof(uint32))
		{
			uint32 * Data = &Buffer->Data32[Buffer->Count];

			// First triangle
			Data[0] = (uint32)VertexBase	;
			Data[1] = (uint32)VertexBase + 1;
			Data[2] = (uint32)VertexBase + 2;

			// Second triangle
			Data[3] = (uint32)VertexBase + 2;
			Data[4] = (uint32)VertexBase + 1;
			Data[5] = (uint32)VertexBase + 3;
		}
		
		Buffer->Count += 6;
	}
}

void IndexBufferAddGrid(struct IndexBuffer * Buffer, uint32 VertexBase, uint32 Length, uint32 Rows, uint32 Cols, bool Outward)
{	
	if (IndexBufferIsAvailable(Buffer, Rows * Cols * 6))
	{
		uint32 i, j;

		for (i = 0; i < Rows; ++i)
		{
			for (j = 0; j < Cols; ++j)
			{
				uint32 Index = VertexBase + i * Length + j;

				// Top-Left
				IndexBufferAdd(Buffer, Index);

				if (Outward)
				{
					// Top-Right
					IndexBufferAdd(Buffer, Index + 1);

					// Bottom-Left
					IndexBufferAdd(Buffer, Index + Length);
				}
				else
				{
					// Bottom-Left
					IndexBufferAdd(Buffer, Index + Length);

					// Top-Right
					IndexBufferAdd(Buffer, Index + 1);
				}

				// Bottom-Left
				IndexBufferAdd(Buffer, Index + Length);

				if (Outward)
				{
					// Top-Right
					IndexBufferAdd(Buffer, Index + 1);

					// Bottom-Right
					IndexBufferAdd(Buffer, Index + Length + 1);
				}
				else
				{
					// Bottom-Right
					IndexBufferAdd(Buffer, Index + Length + 1);

					// Top-Right
					IndexBufferAdd(Buffer, Index + 1);
				}
			}
		}
	}
}

uint32 IndexBufferLength(struct IndexBuffer * Buffer)
{
	return Buffer->Count;
}

uint32 IndexBufferSizeI(struct IndexBuffer * Buffer)
{
	return Buffer->IndexSize;
}

bool IndexBufferIsAvailable(struct IndexBuffer * Buffer, uint32 Count)
{
	return ((Buffer->Count + Count) <= Buffer->Size);
}
