/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	WORLD_TERRAIN_OCCLUSION_H
#define WORLD_TERRAIN_OCCLUSION_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Component.h>
#include <Math/Geometry/Vector.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct TerrainOcclusionNode
{
	struct Vector2i					Start;
	struct Vector2i					Position;
	float							Gradient;
	struct TerrainOcclusionNode *	Next;
};

struct TerrainComponentOcclusionData
{
	uinteger						Sectors;
	uinteger						SubSectors;
	float *							SectorTrigTable;
	float *							SubSectorTrigTable;
	uinteger						MaxPoints;
	integer *						Points;
	integer	*						HorizonPoints;
	uinteger						NodeCount;
	struct TerrainOcclusionNode *	NodeList;
	uinteger						AveragePointsRequired;
	uinteger						MaxPointsRequired;
};

struct TerrainComponentOcclusionType
{
	void * TODO;
};

/*
struct TerrainOcclusionResourceStreamType
{
	bool (*Load)(struct TerrainOcclusionType * Occlusion, uinteger Id);
	bool (*Store)(struct TerrainOcclusionType * Occlusion, uinteger Id);
};
*/

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the terrain occlusion implementation attributes
////////////////////////////////////////////////////////////
const void * TerrainComponentOcclusionAttr();

////////////////////////////////////////////////////////////
/// Return the terrain occlusion implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * TerrainComponentOcclusionImpl();

////////////////////////////////////////////////////////////
/// Return the terrain occlusion derived data size
////////////////////////////////////////////////////////////
uinteger TerrainComponentOcclusionSize();

#endif // WORLD_TERRAIN_OCCLUSION_H
