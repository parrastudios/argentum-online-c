/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GUI_LABEL_H
#define GUI_LABEL_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Widget forward declaration
////////////////////////////////////////////////////////////
struct GuiWidget;

////////////////////////////////////////////////////////////
// Create a label
////////////////////////////////////////////////////////////
struct GuiWidget * GuiLabelCreate(int32 X, int32 Y, uint32 FontSize, char * Text);

////////////////////////////////////////////////////////////
// Create a label by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiLabelCreateDefault();

////////////////////////////////////////////////////////////
// Change the caption of a label
////////////////////////////////////////////////////////////
void GuiLabelChangeCaption(struct GuiWidget * Widget, char * Text);

////////////////////////////////////////////////////////////
// Render a label
////////////////////////////////////////////////////////////
void GuiLabelRender(struct GuiWidget * Form, struct GuiWidget * Widget, struct Color4ub * BackgroundColor, struct Color4ub * FontColor);

#endif // GUI_LABEL_H
