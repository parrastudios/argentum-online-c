/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATABASE_DEFAULT_H
#define DATABASE_DEFAULT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/Connector.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define DATABASE_DEFAULT_SERVER		"localhost"
#define DATABASE_DEFAULT_USER		"root"
#define DATABASE_DEFAULT_PASSWORD	"root"
#define DATABASE_DEFAULT_NAME		"aocdb"
#define DATABASE_DEFAULT_PORT		3306

/////////////////////////////////////////////////////////////
/// Initialize a default database
/////////////////////////////////////////////////////////////
bool DataBaseInitialize();

/////////////////////////////////////////////////////////////
/// Reconnect a default database
/////////////////////////////////////////////////////////////
bool DataBaseReconnect();

/////////////////////////////////////////////////////////////
/// Destroy a default database
/////////////////////////////////////////////////////////////
bool DataBaseDestroy();

/////////////////////////////////////////////////////////////
/// Get default database
/////////////////////////////////////////////////////////////
struct DataBase * DataBaseGetDefault();

#endif // DATABASE_DEFAULT_H
