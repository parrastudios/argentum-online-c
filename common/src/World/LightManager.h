/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_LIGHT_MANAGER_H
#define WORLD_LIGHT_MANAGER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Vector.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct LightEntryType
{
	struct Vector4f Ambient;
	struct Vector4f Diffuse;
	struct Vector4f Specular;
	struct Vector3f Attenuation; // [0] : constant, [1] : linear, [2] : quadratic
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize light manager table
////////////////////////////////////////////////////////////
void LightManagerInitialize(char * Path);

////////////////////////////////////////////////////////////
/// Get properties of a light from the manager table
////////////////////////////////////////////////////////////
struct LightEntryType * LightManagerGet(uinteger Id);

////////////////////////////////////////////////////////////
/// Add a new light entry to the manager
////////////////////////////////////////////////////////////
uinteger LightManagerAdd(struct LightEntryType * LightEntry);

////////////////////////////////////////////////////////////
/// Modify properties of a light entry
////////////////////////////////////////////////////////////
void LightManagerModify(uinteger Id, struct LightEntryType * LightEntry);

////////////////////////////////////////////////////////////
/// Save light table to a file
////////////////////////////////////////////////////////////
void LightManagerSave(char * Path);

////////////////////////////////////////////////////////////
/// Destroy all lights
////////////////////////////////////////////////////////////
void LightManagerDestroy();

#endif // WORLD_LIGHT_MANAGER_H
