/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_MATRIX_STACK_H
#define GRAPHICS_MATRIX_STACK_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Matrix.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define MATRIX_STACK_DEFAULT_SIZE		64
#define MATRIX_STACK_ERROR_OK			0x00
#define MATRIX_STACK_ERROR_OVERFLOW		0x01
#define MATRIX_STACK_ERROR_UNDERFLOW	0x02

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct MatrixStack
{
	struct Matrix4f *	Data;
	uint32				Depth;
	uint32				Pointer;
	uint32				Error;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

struct MatrixStack * StackCreate(uint32 Depth);

void StackDestroy(struct MatrixStack * Stack);

void StackLoadIdentity(struct MatrixStack * Stack);

void StackLoadMatrix(struct MatrixStack * Stack, struct Matrix4f * Matrix);

//void StackLoadTransform(struct MatrixStack * Stack, struct TransformType * Transform);

void StackMultMatrix(struct MatrixStack * Stack, struct Matrix4f * Matrix);

//void StackMultTransform(struct MatrixStack * Stack, struct TransformType * Transform);

void StackPush(struct MatrixStack * Stack);

void StackPop(struct MatrixStack * Stack);

void StackScale(struct MatrixStack * Stack, float x, float y, float z);

void StackTranslate(struct MatrixStack * Stack, float x, float y, float z);

void StackRotate(struct MatrixStack * Stack, float Angle, uint32 Axis);

void StackScale3f(struct MatrixStack * Stack, struct Vector3f * Vector);

void StackTranslate3f(struct MatrixStack * Stack, struct Vector3f * Vector);

void StackPushMatrix(struct MatrixStack * Stack, struct Matrix4f * Matrix);

//void StackPushTransform(struct MatrixStack * Stack, struct TransformType * Transform);

struct Matrix4f * StackGetMatrix(struct MatrixStack * Stack);

void StackCopyMatrix(struct MatrixStack * Stack, struct Matrix4f * Matrix);

uint32 StackGetError(struct MatrixStack * Stack);

#endif // GRAPHICS_MATRIX_STACK_H
