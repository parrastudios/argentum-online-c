/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATATYPE_STRING_LEXER_H
#define DATATYPE_STRING_LEXER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Types.h>
#include <stdio.h>
#include <DataType/String/bstrlib.h> // todo: remove this

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define LEXER_ERROR			0x00
#define LEXER_OK			0x01
#define LEXER_EOF			0x02

#define LEXER_UPPER			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define LEXER_LOWER			"abcdefghijklmnopqrstuvwxyz"
#define LEXER_DIGIT			"0123456789"
#define LEXER_BLANK			"\t\n\r "
#define LEXER_SEPARATOR		"\t "
#define LEXER_ALPHA			LEXER_UPPER LEXER_LOWER
#define LEXER_ALPHANUMERIC	LEXER_UPPER LEXER_LOWER LEXER_DIGIT

////////////////////////////////////////////////////////////
// Data structure
////////////////////////////////////////////////////////////
struct Lexer
{
	FILE *	Input;				///< Input file
	int32	Current;			///< Current character
	bool	HasCurrent;			///< Check if current has a preread character
	bstring Item;				///< Token being read
	bool	OwnItem;			///< Check if the Lexer owns the item reference
	bool	Started, Off;		///< State indicators
};

////////////////////////////////////////////////////////////
/// Lexer initializer (the FILE have to be opened)
////////////////////////////////////////////////////////////
struct Lexer * LexerCreate(FILE * File);

////////////////////////////////////////////////////////////
/// Lexer destructor (it doesn't close the file)
////////////////////////////////////////////////////////////
void LexerDestroy(struct Lexer * LexPtr);

////////////////////////////////////////////////////////////
// Consultants
//
// TODO: side-effect deallocate
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Returns the last string read by the lexer (the bstring
/// have to be released)
////////////////////////////////////////////////////////////
bstring LexerItemGet(struct Lexer * LexPtr);

////////////////////////////////////////////////////////////
/// Check if the lexer began to read a file
////////////////////////////////////////////////////////////
bool LexerStarted(const struct Lexer * LexPtr);

////////////////////////////////////////////////////////////
/// Check if the lexer finished to read a file
////////////////////////////////////////////////////////////
bool LexerOff(const struct Lexer * LexPtr);

////////////////////////////////////////////////////////////
// Operators
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
///	Reads as many characters as possible belonging to Charset
////////////////////////////////////////////////////////////
void LexerNext(struct Lexer * LexPtr, const char * Charset);

////////////////////////////////////////////////////////////
///	Reads as many characters as possible not belonging to Charset
////////////////////////////////////////////////////////////
void LexerNextTo(struct Lexer * LexPtr, const char * Charset);

////////////////////////////////////////////////////////////
///	Reads one character belonging to Charset
////////////////////////////////////////////////////////////
void LexerNextChar(struct Lexer * LexPtr, const char * Charset);

////////////////////////////////////////////////////////////
/// Skip until the end of line
////////////////////////////////////////////////////////////
uint32 LexerNextLine(struct Lexer * LexPtr);

////////////////////////////////////////////////////////////
///	Reads as many characters as possible belonging to
/// Charset (the read characters are lost)
////////////////////////////////////////////////////////////
void LexerSkip(struct Lexer * LexPtr, const char * Charset);

////////////////////////////////////////////////////////////
///	Reads as many characters as possible not belonging to
/// Charset (the read characters are lost)
////////////////////////////////////////////////////////////
void LexerSkipTo(struct Lexer * LexPtr, const char * Charset);

#endif // DATATYPE_STRING_LEXER_H
