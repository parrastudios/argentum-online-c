/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/DataTypes/Set.h>
#include <stdio.h>

////////////////////////////////////////////////////////////
/// Integer hash function implementation for set
////////////////////////////////////////////////////////////
uint32 IntHash(void * Integer)
{
	uint32 Value = (uint32)(*Integer);

    Value = Value ^ (Value >> 4);
    Value = (Value ^ 0xDEADBEEF) + (Value << 5);
    Value = Value ^ (Value >> 11);
	
    return Value;
}

////////////////////////////////////////////////////////////
/// Program entry point
////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
    struct SetType set;
    struct SetIterator it;

	// Create set
    SetCreateHashOnly(&set, &IntHash);

	/*
	Output:
	
	< 0 (1)
	< 1 (1)
	< 2 (1)
	< 1 (0)
	< 3 (1)
	< 1 (0)
	< 4 (1)
	< 5 (1)
	< 6 (1)
	< 7 (1)
	< 8 (1)
	< 9 (1)
	*/
	
    printf("< 0 (%d)\n", (int)SetInsert(&set, (void*)0));
    printf("< 1 (%d)\n", (int)SetInsert(&set, (void*)1));
    printf("< 2 (%d)\n", (int)SetInsert(&set, (void*)2));
    printf("< 1 (%d)\n", (int)SetInsert(&set, (void*)1));
    printf("< 3 (%d)\n", (int)SetInsert(&set, (void*)3));
    printf("< 1 (%d)\n", (int)SetInsert(&set, (void*)1));
    printf("< 4 (%d)\n", (int)SetInsert(&set, (void*)4));
    printf("< 5 (%d)\n", (int)SetInsert(&set, (void*)5));
    printf("< 6 (%d)\n", (int)SetInsert(&set, (void*)6));
    printf("< 7 (%d)\n", (int)SetInsert(&set, (void*)7));
    printf("< 8 (%d)\n", (int)SetInsert(&set, (void*)8));
    printf("< 9 (%d)\n", (int)SetInsert(&set, (void*)95));

	/*
	Output:
	
	> 0
	> 1
	> 2
	> 3
	> 95
	> 4
	> 5
	> 6
	> 7
	> 8
	------------
	*/
	
    for(SetIteratorBegin(&it, &set); !SetIteratorEnd(&it); SetIteratorNext(&it))
    {
        printf("> %d\n", (int)SetIteratorValue(&it));
    }

    printf("------------\n");

    SetClear(&set);
	
	/*
	Output:
	
	< 0 (1)
	< 1 (1)
	< 2 (1)
	< 1 (0)
	*/

    printf("< 0 (%d)\n", (int)SetInsert(&set, (void*)0));
    printf("< 1 (%d)\n", (int)SetInsert(&set, (void*)1));
    printf("< 2 (%d)\n", (int)SetInsert(&set, (void*)2));
    printf("< 1 (%d)\n", (int)SetInsert(&set, (void*)1));

	/*
	Output:
	
	> 0
	> 1
	> 2
	*/
	
    for (SetIteratorBegin(&it, &set); !SetIteratorEnd(&it); SetIteratorNext(&it))
    {
        printf("> %d\n", (int)SetIteratorValue(&it));
    }

	// Destroy set
    SetDestroy(&set);

    return 0;
}
