/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/Value.h>
#include <MetaData/Reference.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef union MetadataValueImpl
{
	#define MetadataValueImplDecl(Type, Name, Id) \
		MetadataTypeDecl(Type) Name;

		// Define metadata type declaration
		METADATA_TYPE_DECL(MetadataValueImplDecl)

	#undef MetadataValueImplDecl

	uint8 Buffer[1];

} * MetadataValueDataImpl;

typedef union MetadataValueImplPtr
{
	#define MetadataValueImplPtrDecl(Type, Name, Id) \
		MetadataTypeDecl(Type) * Name;

		// Define metadata type declaration
		METADATA_TYPE_DECL(MetadataValueImplPtrDecl)

	#undef MetadataValueImplPtrDecl

	void * Buffer;

} * MetadataValueImplPtr;

struct MetadataValueType
{
	MetadataStorageIndexType	Index;		///< Reference to itself
	MetadataType				Type;		///< Type of value
	MetadataReferenceCountType	RefCount;	///< References to value
	MetadataValueCountType		Count;		///< Amount of values contained (0 -> definition, 1 -> value, (>=2) -> array)
	MetadataStorageDataSize		DataSize;	///< Size in bytes of the implementation data
	MetadataValueDataImpl		Data;		///< Data implementation of the value

};

typedef struct MetadataValueArgType
{
	union MetadataValueImpl		Data;
	union MetadataValueImplPtr	Ptr;

} MetadataValueArgType;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a serial value
////////////////////////////////////////////////////////////
MetadataValueType MetadataValueCreate(MetadataScopeType Scope, MetadataType Type, MetadataValueCountType Count, void * Data)
{
	// Calculate the sizes
	MetadataStorageDataSize DataImplSize = Count * MetadataTypeGetSize(Type);
	MetadataStorageDataSize ValueSize = sizeof(struct MetadataValueType);

	// Allocate memory for value
	MetadataValueType Value = MetadataStorageAllocateValue(MetadataScopeGetStorage(Scope), ValueSize + DataImplSize);

	if (Value != METADATA_STORAGE_INDEX_INVALID)
	{
		MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

		if (ValueImpl)
		{
			ValueImpl->Index = Value;
			ValueImpl->Type = Type;
			ValueImpl->Count = Count;
			ValueImpl->DataSize = DataImplSize;
			ValueImpl->RefCount = 0;

			if (ValueImpl->Count > 0)
			{
				// Set impl to the end of value header structure
				ValueImpl->Data = (MetadataValueDataImpl)((uint8*)(ValueImpl) + sizeof(struct MetadataValueType));

				if (Data != NULL)
				{
					// Copy data at the end of value header structure
					MemoryCopy(ValueImpl->Data, Data, ValueImpl->DataSize);
				}
			}
			else
			{
				ValueImpl->Data = NULL;
			}
		}
		else
		{
			MetadataStorageDeallocateValue(MetadataScopeGetStorage(Scope), Value);
		}
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// Bind a serial value
////////////////////////////////////////////////////////////
MetadataValueType MetadataValueBind(MetadataScopeType Scope, MetadataType Type, MetadataValueCountType Count, void * Data)
{
	if (Data != NULL)
	{
		// Calculate the sizes
		MetadataStorageDataSize DataImplSize = Count * MetadataTypeGetSize(Type);
		MetadataStorageDataSize ValueSize = sizeof(struct MetadataValueType);

		// Allocate memory for value (with no size contigously allocated)
		MetadataValueType Value = MetadataStorageAllocateValue(MetadataScopeGetStorage(Scope), ValueSize);

		if (Value != METADATA_STORAGE_INDEX_INVALID)
		{
			MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

			if (ValueImpl)
			{
				ValueImpl->Index = Value;
				ValueImpl->Type = Type;
				ValueImpl->Count = Count;
				ValueImpl->DataSize = DataImplSize;
				ValueImpl->RefCount = 0;

				if (ValueImpl->Count > 0)
				{
					// Bind data to the variable
					ValueImpl->Data = Data;
				}
				else
				{
					ValueImpl->Data = NULL;
				}
			}
			else
			{
				MetadataStorageDeallocateValue(MetadataScopeGetStorage(Scope), Value);
			}
		}

		return Value;
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Create an abstract value
////////////////////////////////////////////////////////////
MetadataValueType MetadataValueSignature(MetadataScopeType Scope, MetadataType Type, MetadataValueCountType Count)
{
	// Allocate memory for value
	MetadataValueType Value = MetadataStorageAllocateValue(MetadataScopeGetStorage(Scope), sizeof(struct MetadataValueType));

	if (Value != METADATA_STORAGE_INDEX_INVALID)
	{
		MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

		if (ValueImpl)
		{
			ValueImpl->Index = Value;
			ValueImpl->Type = Type;
			ValueImpl->Count = Count;
			ValueImpl->DataSize = 0;
			ValueImpl->RefCount = 0;
			ValueImpl->Data = NULL;
		}
		else
		{
			MetadataStorageDeallocateValue(MetadataScopeGetStorage(Scope), Value);
		}
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// Copy a value
////////////////////////////////////////////////////////////
MetadataValueType MetadataValueCopy(MetadataScopeType Scope, MetadataValueType Value)
{
	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Copy data into value from current parameter arg list
////////////////////////////////////////////////////////////
void MetadataValueArgument(MetadataScopeType Scope, MetadataValueType Value, VariableArgumentList * Arguments)
{
	if (MetadataValueInstanced(Scope, Value))
	{
		MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

		if (ValueImpl)
		{
			MetadataValueArgType Argument;

			// Set data
			if (ValueImpl->Count == 1)
			{
				switch (ValueImpl->Type)
				{
					#define MetadataValueArgCastDecl(Type, Name, Id) \
						case Id: \
						{ \
							Argument.Data.Name = MetadataTypeCast(Type) va_arg(Arguments->List, MetadataTypePromotionDecl(Type)); \
						} \
						break;

						// Define metadata type declaration
						METADATA_TYPE_DECL(MetadataValueArgCastDecl)

					#undef MetadataValueArgCastDecl
				}

				// Copy argument parameter to value memory
				MemoryCopy(ValueImpl->Data, &Argument.Data.Buffer[0], ValueImpl->DataSize);
			}
			else if (ValueImpl->Count > 1)
			{
				switch (ValueImpl->Type)
				{
					#define MetadataValueArgCastPtrDecl(Type, Name, Id) \
						case Id: \
						{ \
							Argument.Ptr.Name = va_arg(Arguments->List, MetadataTypeDecl(Type) *); \
						} \
						break;

							// Define metadata type declaration
							METADATA_TYPE_DECL(MetadataValueArgCastPtrDecl)

						#undef MetadataValueArgCastPtrDecl
				}

				// Copy argument parameter to value memory
				MemoryCopy(ValueImpl->Data, Argument.Ptr.Buffer, ValueImpl->DataSize);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Check if it is a valid value
////////////////////////////////////////////////////////////
bool MetadataValueValid(MetadataScopeType Scope, MetadataValueType Value)
{
	MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

	return (ValueImpl != NULL && ValueImpl->Type != METADATA_TYPE_ID_INVALID);
}

////////////////////////////////////////////////////////////
/// Check if it is an abstract value
////////////////////////////////////////////////////////////
bool MetadataValueAbstract(MetadataScopeType Scope, MetadataValueType Value)
{
	MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

	return (ValueImpl != NULL && ValueImpl->Type != METADATA_TYPE_ID_INVALID && (ValueImpl->Data == NULL && ValueImpl->DataSize == 0));
}

////////////////////////////////////////////////////////////
/// Check if a value is an abstract builtin type
////////////////////////////////////////////////////////////
bool MetadataValueAbstractBuiltin(MetadataScopeType Scope, MetadataValueType Value)
{
	MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

	return (ValueImpl != NULL && ValueImpl->Type != METADATA_TYPE_ID_INVALID && ValueImpl->Count == 1 && (ValueImpl->Data == NULL && ValueImpl->DataSize == 0));
}

////////////////////////////////////////////////////////////
/// Check if a value is an abstract array
////////////////////////////////////////////////////////////
bool MetadataValueAbstractArray(MetadataScopeType Scope, MetadataValueType Value)
{
	MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

	return (ValueImpl != NULL && ValueImpl->Type != METADATA_TYPE_ID_INVALID && ValueImpl->Count > 1 && (ValueImpl->Data == NULL && ValueImpl->DataSize == 0));
}

////////////////////////////////////////////////////////////
/// Check if a value is instanced
////////////////////////////////////////////////////////////
bool MetadataValueInstanced(MetadataScopeType Scope, MetadataValueType Value)
{
	MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

	return (ValueImpl != NULL && ValueImpl->Type != METADATA_TYPE_ID_INVALID && ValueImpl->Count > 0 && (ValueImpl->Data != NULL && ValueImpl->DataSize > 0));
}

////////////////////////////////////////////////////////////
/// Check if a value is an builtin type
////////////////////////////////////////////////////////////
bool MetadataValueBuiltin(MetadataScopeType Scope, MetadataValueType Value)
{
	MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

	return (ValueImpl != NULL && ValueImpl->Type != METADATA_TYPE_ID_INVALID && ValueImpl->Count == 1 && (ValueImpl->Data != NULL && ValueImpl->DataSize > 0));
}

////////////////////////////////////////////////////////////
/// Check if a value is an array
////////////////////////////////////////////////////////////
bool MetadataValueArray(MetadataScopeType Scope, MetadataValueType Value)
{
	MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

	return (ValueImpl != NULL && ValueImpl->Type != METADATA_TYPE_ID_INVALID && ValueImpl->Count > 1 && (ValueImpl->Data != NULL && ValueImpl->DataSize > 0));
}

////////////////////////////////////////////////////////////
/// Get the type of a value
////////////////////////////////////////////////////////////
MetadataType MetadataValueTypeOf(MetadataScopeType Scope, MetadataValueType Value)
{
	if (MetadataValueValid(Scope, Value))
	{
		MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

		return ValueImpl->Type;
	}

	return METADATA_TYPE_ID_INVALID;
}

////////////////////////////////////////////////////////////
/// Get the size of value type
////////////////////////////////////////////////////////////
MetadataTypeSize MetadataValueTypeSize(MetadataScopeType Scope, MetadataValueType Value)
{
	if (MetadataValueValid(Scope, Value))
	{
		MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

		return MetadataTypeGetSize(ValueImpl->Type);
	}

	return METADATA_TYPE_SIZE_INVALID;
}

////////////////////////////////////////////////////////////
/// Get the amount of values that contains itself
////////////////////////////////////////////////////////////
MetadataValueCountType MetadataValueCount(MetadataScopeType Scope, MetadataValueType Value)
{
	if (MetadataValueValid(Scope, Value))
	{
		MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

		return ValueImpl->Count;
	}

	return 0;
}

////////////////////////////////////////////////////////////
/// Get the data wraped by a value
////////////////////////////////////////////////////////////
void * MetadataValueGet(MetadataScopeType Scope, MetadataValueType Value)
{
	MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

	if (MetadataValueInstanced(Scope, Value))
	{
		return ValueImpl->Data;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set the internal data of a value
////////////////////////////////////////////////////////////
void MetadataValueSet(MetadataScopeType Scope, MetadataValueType Value, void * Data)
{
	if (MetadataValueInstanced(Scope, Value) && Data)
	{
		MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

		if (ValueImpl->Data)
		{
			MemoryCopy(ValueImpl->Data, Data, MetadataValueTypeSize(Scope, Value));
		}
	}
}

////////////////////////////////////////////////////////////
/// Reference a value
////////////////////////////////////////////////////////////
void MetadataValueReference(MetadataScopeType Scope, MetadataValueType Value)
{
	if (MetadataValueInstanced(Scope, Value))
	{
		MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

		++ValueImpl->RefCount;
	}
}

////////////////////////////////////////////////////////////
/// Check if value is referenced
////////////////////////////////////////////////////////////
bool MetadataValueIsReferenced(MetadataScopeType Scope, MetadataValueType Value)
{
	if (MetadataValueInstanced(Scope, Value))
	{
		MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

		if (ValueImpl)
		{
			return (ValueImpl->RefCount > 0);
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Dereference a value
////////////////////////////////////////////////////////////
void MetadataValueDereference(MetadataScopeType Scope, MetadataValueType Value)
{
	if (MetadataValueInstanced(Scope, Value))
	{
		MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

		--ValueImpl->RefCount;
	}
}

////////////////////////////////////////////////////////////
/// Destroy a value
////////////////////////////////////////////////////////////
void MetadataValueDestroy(MetadataScopeType Scope, MetadataValueType Value)
{
	MetadataValueTypeImpl ValueImpl = MetadataStorageGetValue(MetadataScopeGetStorage(Scope), Value);

	if (ValueImpl)
	{
		ValueImpl->Index = METADATA_STORAGE_INDEX_INVALID;
		ValueImpl->Type = METADATA_TYPE_ID_INVALID;
		ValueImpl->Count = 0;
		ValueImpl->RefCount = 0;
		ValueImpl->DataSize = 0;
		ValueImpl->Data = NULL;
	}

	MetadataStorageDeallocateValue(MetadataScopeGetStorage(Scope), Value);
}
