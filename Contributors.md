![](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2012/Dec/26/argentum-online-c-logo-1554735323-0_avatar.png?raw=true "Argentum Online C")

# __Argentum Online C__
##### Copyright (C) 2009 - 2014
##### Vicente Eduardo Ferrer Garc�a - Parra Studios (<vic798@gmail.com>)


- - -


##### CONTRIBUTORS

* Maximiliano Duthey (<lokomax@gmail.com>)
* Adri�n Garc�a Santiago (<dev@agaman.me>)
* Jos� Antonio Dominguez (<chamot11@gmail.com>)
* Nelson Bermudez-Bassett (<nelsonbassett13@gmail.com>)
* Alejandro Juan P�rez (<tuketelamodelmon@gmail.com>)
* Daniel Buide Ortega (<daniel_buide@hotmail.com>)


- - -

