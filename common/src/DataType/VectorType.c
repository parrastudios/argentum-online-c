/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/Vector.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

// Creates a vector whose elements have size 'TypeSize'
Vector VectorNew(uinteger TypeSize)
{
	Vector v = (Vector)MemoryAllocate(sizeof(struct VectorType));

	if (v == NULL)
	{
		// Handle error
		VectorTypeFatalError("Vector allocation error");
		return NULL;
	}

	v->TypeSize = TypeSize;
	v->Capacity = VECTOR_CAPACITY_MIN;
	v->Size = 0;
	v->Data = MemoryAllocate(v->Capacity * v->TypeSize);

	if (v->Data == NULL)
	{
		// Handle error
		VectorTypeFatalError("Vector allocation error");
		return NULL;
	}

	return v;
}

// Creates a vector with 'Size' elements that have size 'TypeSize'
Vector VectorNewReserve(uinteger Size, uinteger TypeSize)
{
	Vector v = (Vector)MemoryAllocate(sizeof(struct VectorType));

	if (v == NULL)
	{
		// Handle error
		VectorTypeFatalError("Vector allocation error");
		return NULL;
	}

	v->TypeSize = TypeSize;
	v->Capacity = (Size < VECTOR_CAPACITY_MIN) ? VECTOR_CAPACITY_MIN : Size;
	v->Size = Size;
	v->Data = MemoryAllocate(v->Capacity * v->TypeSize);

	if (v->Data == NULL)
	{
		// Handle error

		return NULL;
	}

	return v;
}

// Free all the memory
//   After that te object is unusable
//   Don't apply the function twice over the same object
void VectorDestroy(Vector v)
{
	MemoryDeallocate(v->Data);
	MemoryDeallocate(v);
}

// Request a change in the capacity
void VectorReserve(Vector v, uinteger Capacity)
{
	if (Capacity < v->Size)
		v->Size = Capacity;
	if (Capacity < VECTOR_CAPACITY_MIN)
		Capacity = VECTOR_CAPACITY_MIN;
	if (Capacity != v->Capacity)
	{
		register void *newData = MemoryReallocate(v->Data, Capacity * v->TypeSize);
		if (newData == NULL)
		{
			// Handle error
			VectorTypeFatalError("Vector allocation error");

			return;
		}
		v->Data = newData;
	}
	v->Capacity = Capacity;
}

// Resizes the container so that it contains 'Size' elements
void VectorResize(Vector v, uinteger Size)
{
	if (v->Capacity < Size)
		VectorReserve(v, Size);
	v->Size = Size;
	if (v->Size < v->Capacity / VECTOR_CAPACITY_MIN_USED)
		VectorReserve(v, v->Size * VECTOR_CAPACITY_INCREMENT);
}

// Deletes all the objects and set size to 0
//   (Decrements the allocated memory if necesary)
void VectorClear(Vector v)
{
	VectorResize(v, 0);
}

// Returns the amount of objects that can store with the actual allocated memory
uinteger VectorCapacity(Vector v)
{
	return v->Capacity;
}

// Returns the amount of object in the v
uinteger VectorSize(Vector v)
{
	return v->Size;
}

// Returns the size of the element type
uinteger VectorTypeSize(Vector v)
{
	return v->TypeSize;
}

// Returns true if the vector if empty (size is 0)
bool VectorEmpty(Vector v)
{
	return (bool)(v->Size == 0);
}

// Returns a pointer to the first object
void * VectorFront(Vector v)
{
	return v->Data;
}

// Returns a pointer to the last object
void * VectorBack(Vector v)
{
	return VectorTypeDataPosbytes(v, (v->Size - 1) * v->TypeSize);
}

// Return a pointer to the object in the position 'Position'
//   [ 0 <= Position <= VectorSize(v)-1 ]
void * VectorAt(Vector v, uinteger Position) {
	return VectorTypeDataPosbytes(v, Position * v->TypeSize);
}

// Sets the object in the position 'Position' to the same of the object pointed by 'Element'
void VectorSet(Vector v, uinteger Position, void * Element)
{
	MemoryCopy(VectorTypeDataPosbytes(v, Position * v->TypeSize), Element, v->TypeSize);
}

// Adds an object to the end of the vector without initializing them
//   (Increments the allocated memory if necessary)
//   (The new element could contain garbage)
void VectorPushBackEmpty(Vector v)
{
	if (v->Size == v->Capacity)
		VectorReserve(v, v->Capacity * VECTOR_CAPACITY_INCREMENT);
	++v->Size;
}

// Adds an object to the end of the vector
//   (Increments the allocated memory if necessary)
void VectorPushBack(Vector v, void * Element)
{
	if (v->Size == v->Capacity)
		VectorReserve(v, v->Capacity * VECTOR_CAPACITY_INCREMENT);
	MemoryCopy(VectorTypeDataPosbytes(v, v->Size * v->TypeSize), Element, v->TypeSize);
	++v->Size;
}

// Deletes the last object of the vector
//   (Decrements the allocated memory if necesary)
void VectorPopBack(Vector v)
{
	if (v->Size > 0)
	{
		--v->Size;
		if (v->Capacity / VECTOR_CAPACITY_MIN_USED < v->Size)
			VectorReserve(v, v->Size * VECTOR_CAPACITY_INCREMENT);
	}
}

// Adds an object in the first position whithout initializing them
//   (Increments the allocated memory if necesary)
//   (The new element could contain garbage)
void VectorPushFrontEmpty(Vector v)
{
	if (v->Size == v->Capacity)
		VectorReserve(v, v->Capacity * VECTOR_CAPACITY_INCREMENT);
	MemoryMove(VectorTypeDataPosbytes(v, v->TypeSize), v->Data, v->Size * v->TypeSize);
	++v->Size;
}

// Adds an object in the first position
//   (Increments the allocated memory if necesary)
void VectorPushFront(Vector v, void * Element)
{
	if (v->Size == v->Capacity)
		VectorReserve(v, v->Capacity * VECTOR_CAPACITY_INCREMENT);
	MemoryMove(VectorTypeDataPosbytes(v, v->TypeSize), v->Data, v->Size * v->TypeSize);
	MemoryCopy(v->Data, Element, v->TypeSize);
	++v->Size;
}

// Deletes the first object
//   (Decrements the allocated memory if necesary)
void VectorPopFront(Vector v)
{
	if (v->Size > 0)
	{
		--v->Size;
		MemoryMove(v->Data, VectorTypeDataPosbytes(v, v->TypeSize), v->Size * v->TypeSize);
		if (v->Capacity / VECTOR_CAPACITY_MIN_USED < v->Size)
			VectorReserve(v, v->Size * VECTOR_CAPACITY_INCREMENT);
	}
}

// Adds an object in the position 'Position' whithout initializing them
//   [ 0 <= pos <= VectorSize(v)-1 ]
//   The objects in position 'Position' and upper will be moved
//   (Increments the allocated memory if necesary)
//   (The new element could contain garbage)
void VectorInsertEmpty(Vector v, uinteger Position)
{
	if (v->Size == v->Capacity)
		VectorReserve(v, v->Capacity * VECTOR_CAPACITY_INCREMENT);
	if (Position < v->Size)
	{
		MemoryMove(VectorTypeDataPosbytes(v, Position * v->TypeSize),
			VectorTypeDataPosbytes(v, (Position + 1) * v->TypeSize),
			(v->Size - Position) * v->TypeSize);
	}
	++v->Size;
}

// Adds an object in the position 'Position'
//   [ 0 <= Position <= VectorSize(v)-1 ]
//   The objects in position 'Position' and upper will be moved
//   (Increments the allocated memory if necesary)
void VectorInsert(Vector v, uinteger Position, void * Element)
{
	if (v->Size == v->Capacity)
		VectorReserve(v, v->Capacity * VECTOR_CAPACITY_INCREMENT);
	if (Position >= v->Size)
	{
		MemoryCopy(VectorTypeDataPosbytes(v, v->Size * v->TypeSize), Element, v->TypeSize);
	}
	else
	{
		MemoryMove(VectorTypeDataPosbytes(v, Position * v->TypeSize),
			VectorTypeDataPosbytes(v, (Position + 1) * v->TypeSize),
			(v->Size - Position) * v->TypeSize);
		MemoryCopy(VectorTypeDataPosbytes(v, Position * v->TypeSize), Element, v->TypeSize);
	}
	++v->Size;
}

// Deletes an object in the position Position
//   (Decrements the allocated memory if necesary)
void VectorErase(Vector v, uinteger Position)
{
	if (Position < v->Size)
	{
		if (Position < v->Size - 1)
			MemoryMove(VectorTypeDataPosbytes(v, Position * v->TypeSize),
			VectorTypeDataPosbytes(v, (Position + 1) * v->TypeSize),
			(v->Size - Position - 1) * v->TypeSize);
		--v->Size;
		if (v->Capacity / VECTOR_CAPACITY_MIN_USED < v->Size)
			VectorReserve(v, v->Size * VECTOR_CAPACITY_INCREMENT);
	}
}
