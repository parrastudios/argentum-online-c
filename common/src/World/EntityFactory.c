/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/EntityFactory.h>

#include <World/Terrain.h> // todo: invert dependencies
#include <World/Model.h>
#include <World/Trigger.h>
#include <World/Light.h>
#include <World/Particle.h>
#include <World/Point.h>
#include <World/Camera.h>

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get hash from index of an entity
////////////////////////////////////////////////////////////
SetHashType EntityFactoryHashEntity(void * EntityPtr);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return entity factory by type
////////////////////////////////////////////////////////////
struct EntityFactoryType * EntityFactoryList(uinteger Type)
{
	if (Type < ENTITY_TYPE_COUNT)
	{
		EntityFactorySingleton EntityFactoryListImpl[ENTITY_TYPE_COUNT] = {
			&TerrainFactoryGet,
			&TriggerFactoryGet,
			&ModelFactoryGet,
			&LightFactoryGet,
			&ParticleFactoryGet,
			&PointFactoryGet,
			&CameraFactoryGet
		};

		return EntityFactoryListImpl[Type]();
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Initialize a factory
////////////////////////////////////////////////////////////
void EntityFactoryInitialize(struct EntityFactoryType * EntityFactory)
{
	if (EntityFactory && EntityFactory->InitializeImpl)
	{
		// Create entity pool
		EntityFactory->EntityPool = SetCreateHashOnly(sizeof(struct EntityType), &EntityFactoryHashEntity);

		if (EntityFactory->EntityPool)
		{
			// Create entity free ids list
			EntityFactory->EntityFreeIds = ListNew(sizeof(EntityIdType));

			// Create map of component pools
			if (EntityFactory->ComponentMap == NULL)
			{
				EntityFactory->ComponentMap = HashMapCreate(HashGetCallbackUnsignedIntegerDefault(), CompareGetCallback(COMPARE_FUNC_UINTEGER));
			}

			// Initialize factory
			if (!EntityFactory->InitializeImpl())
			{
				// Error: Invalid factory initialization
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Clear all entities from the factory
////////////////////////////////////////////////////////////
void EntityFactoryClear(struct EntityFactoryType * EntityFactory)
{
	if (EntityFactory && EntityFactory->EntityPool)
	{
		struct SetIteratorType Iterator;

		// Destroy all entities of the factory
		SetForEach(EntityFactory->EntityPool, &Iterator)
		{
			struct EntityType * Entity = (struct EntityType *)SetIteratorValue(&Iterator);

			if (Entity)
			{
				EntityDestroy(Entity->Factory->Type, Entity->Index);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Destroy a factory from memory
////////////////////////////////////////////////////////////
void EntityFactoryDestroy(struct EntityFactoryType * EntityFactory)
{
	if (EntityFactory)
	{
		// Clear entities
		EntityFactoryClear(EntityFactory);

		// Destroy factory impl
		if (EntityFactory->DestroyImpl)
		{
			EntityFactory->DestroyImpl();
		}

		// Destroy entity pool
		if (EntityFactory->EntityPool)
		{
			SetDestroy(EntityFactory->EntityPool);
		}

		// Destroy entity free ids list
		if (EntityFactory->EntityFreeIds)
		{
			ListDestroy(EntityFactory->EntityFreeIds);
		}

		// Destroy map (assume vectors are already deleted)
		if (EntityFactory->ComponentMap)
		{
			HashMapDestroy(EntityFactory->ComponentMap);
		}
	}
}

////////////////////////////////////////////////////////////
/// Create a component vector into a factory
////////////////////////////////////////////////////////////
bool EntityFactoryCreateComponents(struct EntityFactoryType * EntityFactory, uinteger DataType, uinteger DerivedSize)
{
	if (EntityFactory && EntityFactory->ComponentMap)
	{
		// Check if vector component already exists
		struct ComponentPoolType * ComponentPool = (struct ComponentPoolType *)HashMapGet(EntityFactory->ComponentMap, (HashKeyData)&DataType);

		// Create if not exists
		if (ComponentPool == NULL)
		{
			// Create the component
			ComponentPool = (struct ComponentPoolType *)MemoryAllocate(sizeof(struct ComponentPoolType));

			if (ComponentPool)
			{
				// Allocate vector with component + derived data size
				ComponentPool->Components = VectorNew(ComponentGetSize(DataType, DerivedSize));

				// Allocate vector for the index table
				ComponentPool->Table = VectorNew(sizeof(ComponentIdType));

				// Allocate list for the free ids in the index table (static ones)
				ComponentPool->FreeIds = ListNew(sizeof(ComponentIdType));

				// If valid allocation
				if (ComponentPool->Components && ComponentPool->Table && ComponentPool->FreeIds)
				{
					// Set current pool type
					ComponentPool->DataType = DataType;

					// Insert into the map if successful
					HashMapInsert(EntityFactory->ComponentMap, (HashKeyData)&ComponentPool->DataType, (void *)ComponentPool);

					return true;
				}
				else
				{
					// Clear components
					VectorDestroy(ComponentPool->Components);

					// Clear index table
					VectorDestroy(ComponentPool->Table);

					// Clear free id list
					ListDestroy(ComponentPool->FreeIds);

					// Clear the pool
					MemoryDeallocate(ComponentPool);
				}
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Link entity component map to each factory components
////////////////////////////////////////////////////////////
void EntityFactoryBindComponents(struct EntityFactoryType * EntityFactory, struct EntityType * Entity)
{
	uinteger DataType;

	for (DataType = 0; DataType < COMPONENT_DATA_SIZE; ++DataType)
	{
		if (EntityFactoryComponentDataGet(EntityFactory->ComponentDataMask, DataType))
		{
			// Create component
			ComponentIdType ComponentId = EntityCreateComponent(Entity, DataType);

			// ...

		}
	}
}

////////////////////////////////////////////////////////////
/// Get component from entity factory
////////////////////////////////////////////////////////////
struct ComponentType * EntityFactoryGetComponent(struct EntityFactoryType * EntityFactory, uinteger DataType, ComponentIdType ComponentId)
{
	if (EntityFactory && ComponentId != COMPONENT_ID_INVALID)
	{
		// Get pool of desired data type
		struct ComponentPoolType * ComponentPool = (struct ComponentPoolType *)HashMapGet(EntityFactory->ComponentMap, (HashKeyData)&DataType);

		if (ComponentPool)
		{
			// Get dynamic id refering to component vector id
			ComponentIdType * DynamicComponentId = (ComponentIdType *)VectorAt(ComponentPool->Table, ComponentId);

			// Get the component
			return (struct ComponentType *)VectorAt(ComponentPool->Components, *DynamicComponentId);
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy component vector from a factory
////////////////////////////////////////////////////////////
void EntityFactoryDestroyComponents(struct EntityFactoryType * EntityFactory, uinteger DataType)
{
	if (EntityFactory && EntityFactory->ComponentMap)
	{
		// Get component pool
		struct ComponentPoolType * ComponentPool = (struct ComponentPoolType *)HashMapGet(EntityFactory->ComponentMap, (HashKeyData)&DataType);

		if (ComponentPool)
		{
			// Clear components
			VectorDestroy(ComponentPool->Components);

			// Clear index table
			VectorDestroy(ComponentPool->Table);

			// Clear free id list
			ListDestroy(ComponentPool->FreeIds);

			// Clear the pool
			MemoryDeallocate(ComponentPool);
		}
	}
}

////////////////////////////////////////////////////////////
/// Create an entity in a factory
////////////////////////////////////////////////////////////
struct EntityType * EntityFactoryCreateEntity(struct EntityFactoryType * EntityFactory)
{
	struct EntityType EmptyEntity;

	if (EntityFactory && EntityFactory->EntityPool && EntityFactory->EntityFreeIds)
	{
		bool ClearFreeId;

		// Set to null the entity values and find a free index
		EmptyEntity.Factory = NULL;
		EmptyEntity.Components = NULL;

		// Get free entry
		if (!ListEmpty(EntityFactory->EntityFreeIds))
		{
			// Get first free entry in the set
			EmptyEntity.Index = (EntityIdType)ListFront(EntityFactory->EntityFreeIds);

			// Set clear flag
			ClearFreeId = true;
		}
		else
		{
			// Get index from new entry in the set
			EmptyEntity.Index = (EntityIdType)SetGetSize(EntityFactory->EntityPool);

			// Set clear flag
			ClearFreeId = false;
		}

		// Insert new entity
		if (SetInsert(EntityFactory->EntityPool, (void *)&EmptyEntity))
		{
			struct EntityType * Entity;

			if (ClearFreeId)
			{
				// Clear the free id from list
				ListPopFront(EntityFactory->EntityFreeIds);
			}

			// Link entity to the factory
			Entity = SetGetValueByHash(EntityFactory->EntityPool, EmptyEntity.Index);

			if (Entity != NULL)
			{
				Entity->Factory = EntityFactory;

				return Entity;
			}
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Get hash from index of an entity
////////////////////////////////////////////////////////////
SetHashType EntityFactoryHashEntity(void * EntityPtr)
{
	SetHashType Value = ((struct EntityType *)(EntityPtr))->Index;

	return Value;
}

////////////////////////////////////////////////////////////
/// Destroy an entity from a factory
////////////////////////////////////////////////////////////
bool EntityFactoryDestroyEntity(struct EntityType * Entity)
{
	if (Entity && Entity->Factory)
	{
		EntityIdType EntityId = Entity->Index;

		HashMapIterator ComponentsIterator;

		// Remove components of the entity from factory
		if (HashMapIteratorBegin(&ComponentsIterator, Entity->Components))
		{
			do
			{
				uinteger DataType = HashMapIteratorKeyT(&ComponentsIterator, uinteger);

				ComponentIdType ComponentId = HashMapIteratorDataT(&ComponentsIterator, ComponentIdType);

				// Remove component
				if (!EntityDestroyComponent(Entity, DataType, ComponentId))
				{
					// Error: Invalid component
					return false;
				}
			} while (!HashMapIteratorEnd(&ComponentsIterator) && HashMapIteratorNext(&ComponentsIterator));
		}

		// Remove entity by its hash from the pool
		SetRemoveByHash(Entity->Factory->EntityPool, EntityFactoryHashEntity(Entity));

		// Append to free ids list the current removed entity id
		ListPushBack(Entity->Factory->EntityFreeIds, (void *)&EntityId);

		return true;
	}

	// Error: Invalid entity or entity factory
	return false;
}
