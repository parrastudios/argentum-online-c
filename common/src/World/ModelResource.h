/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_MODEL_RESOURCE_H
#define WORLD_MODEL_RESOURCE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/Vector.h>
#include <System/IOHelper.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define MODEL_RESOURCE_HEADER_ID_SIZE			0x10		//< 16 Bytes
#define MODEL_RESOURCE_OLD_POSE_CHANNEL_SIZE	0x09
#define MODEL_RESOURCE_POSE_CHANNEL_SIZE		0x0A

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct ModelResourceType;

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef struct ModelResourceType * ModelResource;			//< Opaque model resource handler type definition

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct ModelResourceHeaderType
{
	uint8 HeaderIdentifier[MODEL_RESOURCE_HEADER_ID_SIZE];
	uint32 Version;
	uint32 FileSize;
	uint32 Flags;
	uint32 TextSize, TextOffset;
	uint32 MeshSize, MeshOffset;
	uint32 VertexArraysSize, VertexSize, VertexArraysOffset;
	uint32 TriangleSize, TriangleOffset, AdjacencyOffset;
	uint32 JointSize, JointOffset;
	uint32 PoseSize, PoseOffset;
	uint32 AnimSize, AnimOffset;
	uint32 FrameSize, FrameChannelSize, FrameOffset, BoundOffset;
	uint32 CommentSize, CommentOffset;
	uint32 ExtensionSize, ExtensionOffset;
};

struct ModelResourceMeshType
{
	uint32 Name;
	uint32 Material;
	uint32 VertexOffset, VertexSize;
	uint32 TriangleOffset, TriangleSize;
};

struct ModelResourceTriangleType
{
	uint32 Vertex[3];
};

struct ModelResourceAdjacencyType
{
	uint32 Triangle[3];
};

struct ModelResourceJointOldType
{
	uint32 Name;
	int32 Parent;
	float32 Translate[3], Rotate[3], Scale[3];
};

struct ModelResourceJointType
{
	uint32 Name;
	int32 Parent;
	float32 Translate[3], Rotate[4], Scale[3];
};

struct ModelResourcePoseOldType
{
	int32 Parent;
	uint32 Mask;
	float32 ChannelOffset[MODEL_RESOURCE_OLD_POSE_CHANNEL_SIZE];
	float32 ChannelScale[MODEL_RESOURCE_OLD_POSE_CHANNEL_SIZE];
};

struct ModelResourcePoseType
{
	int32 Parent;
	uint32 Mask;
	float32 ChannelOffset[MODEL_RESOURCE_POSE_CHANNEL_SIZE];
	float32 ChannelScale[MODEL_RESOURCE_POSE_CHANNEL_SIZE];
};

struct ModelResourceAnimationType
{
	uint32 Name;
	uint32 FrameOffset, FrameSize;
	float32 FrameRate;
	uint32 Flags;
};

struct ModelResourceVertexArrayType
{
	uint32 Type;
	uint32 Flags;
	uint32 Format;
	uint32 Size;
	uint32 Offset;
};

struct ModelResourceSharedVertexType
{
	int32 Index;
	int32 Weld;
};

struct ModelResourceBoundsType
{
	float32 BoundingBoxMin[3], BoundingBoxMax[3];
	float32 XYRadius, Radius;
};

struct ModelResourceExtensionType
{
	uint32 Name;
	uint32 DataSize, DataOffset;
	uint32 ExtensionOffset;
};

struct ModelResourceImplType
{
	uint8 *										Buffer;			//< Buffer containing the model data

	struct
	{
		struct ModelResourceHeaderType *		Header;			//< Model file header
		uint8 *									Text;			//< Model text pointer
		struct ModelResourceMeshType *			Meshes;			//< Model mesh pointer
		struct ModelResourceTriangleType *		Triangles;		//< Model triangle list
		struct ModelResourceAdjacencyType *		Neighbors;		//< Model triangle neighbor list
		struct ModelResourceVertexArrayType *	VertexArrays;	//< Model vertex array reference
		struct ModelResourceAnimationType *		Animations;		//< Model animation list
		struct ModelResourceJointType *			Joints;			//< Model joint list
		struct ModelResourcePoseType *			Poses;			//< Model pose list
		uint16 *								Frames;			//< Model frame list
		struct ModelResourceBoundsType *		Bounds;			//< Model bounding box list
	};

	struct
	{
		float32 * Positions;
		float32 * Normals;
		float32 * Tangents;
		float32 * TextureCoords;
		uint8 * BlendIndexes;
		uint8 * BlendWeights;
		uint8 * Colors;
	} Vertex;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Reset a model resource header
////////////////////////////////////////////////////////////
void ModelResourceHeaderReset(struct ModelResourceHeaderType * ModelResourceHeader);

////////////////////////////////////////////////////////////
/// Check model resource header integrity
////////////////////////////////////////////////////////////
bool ModelResourceHeaderCheck(struct ModelResourceHeaderType * ModelResourceHeader);

////////////////////////////////////////////////////////////
/// Convert model header to proper endian
////////////////////////////////////////////////////////////
void ModelResourceHeaderFormat(struct ModelResourceHeaderType * ModelResourceHeader);

////////////////////////////////////////////////////////////
/// Parse model resource header
////////////////////////////////////////////////////////////
bool ModelResourceHeaderRead(struct ModelResourceHeaderType * ModelResourceHeader, FILE * File);

////////////////////////////////////////////////////////////
/// Create a model resource representation
////////////////////////////////////////////////////////////
void ModelResourceCreate(struct ModelResourceImplType * ModelResource);

////////////////////////////////////////////////////////////
/// Clear a model resource representation
////////////////////////////////////////////////////////////
void ModelResourceClear(struct ModelResourceImplType * ModelResource);

////////////////////////////////////////////////////////////
/// Parse model resource (on success, file handle is closed)
////////////////////////////////////////////////////////////
bool ModelResourceRead(struct ModelResourceImplType * ModelResource, FILE * File);

////////////////////////////////////////////////////////////
/// Destroy a model resource representation
////////////////////////////////////////////////////////////
void ModelResourceDestroy(struct ModelResourceImplType * ModelResource);

////////////////////////////////////////////////////////////
/// Load model resource meshes from buffer
////////////////////////////////////////////////////////////
bool ModelResourceLoadMeshes(struct ModelResourceImplType * ModelResource);

////////////////////////////////////////////////////////////
/// Load model resource animations from buffer
////////////////////////////////////////////////////////////
bool ModelResourceLoadAnims(struct ModelResourceImplType * ModelResource);

#endif // WORLD_MODEL_RESOURCE_H
