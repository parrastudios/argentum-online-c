/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_LIGHTING_BASE_H
#define	GRAPHICS_LIGHTING_BASE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/Device.h>
#include <Graphics/LightPrimitives.h>
#include <Graphics/ShaderProgram.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define LIGHTING_BASE_LIGHT_POINT_MAX		2
#define LIGHTING_BASE_LIGHT_SPOT_MAX		2

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct LightingBaseTechniqueType
{
	GLuint WVPLocation;
	GLuint WorldMatrixLocation;
	GLuint ColorTextureLocation;
	GLuint EyeWorldPosLocation;
	GLuint MatSpecularIntensityLocation;
	GLuint MatSpecularPowerLocation;
	GLuint LightsPointSizeLocation;
	GLuint LightsSpotSizeLocation;

	struct
	{
		GLuint Color;
		GLuint AmbientIntensity;
		GLuint DiffuseIntensity;
		GLuint Direction;
	} LightDirectionalLocation;

	struct {
		GLuint Color;
		GLuint AmbientIntensity;
		GLuint DiffuseIntensity;
		GLuint Position;

		struct
		{
			GLuint Constant;
			GLuint Linear;
			GLuint Exp;
		} Attenuation;
	} LightPointLocations[LIGHTING_BASE_LIGHT_POINT_MAX];

	struct
	{
		GLuint Color;
		GLuint AmbientIntensity;
		GLuint DiffuseIntensity;
		GLuint Position;
		GLuint Direction;
		GLuint Cutoff;

		struct
		{
			GLuint Constant;
			GLuint Linear;
			GLuint Exp;
		} Atten;
	} LightSpotLocations[LIGHTING_BASE_LIGHT_SPOT_MAX];

	struct ShaderProgramType Technique;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

bool LightingBaseGetLocations(struct LightingBaseTechniqueType * LightingBase);

bool LightingBaseInitialize(struct LightingBaseTechniqueType * LightingBase);

void LightingBaseSetWVP(struct LightingBaseTechniqueType * LightingBase, struct Matrix4f * WVP);

void LightingBaseSetWorldMatrix(struct LightingBaseTechniqueType * LightingBase, struct Matrix4f * World);

void LightingBaseSetColorTextureUnit(struct LightingBaseTechniqueType * LightingBase, uinteger TextureUnit);

void LightingBaseSetDirectionalLight(struct LightingBaseTechniqueType * LightingBase, struct LightDirectional * Light);

void LightingBaseSetPointLights(struct LightingBaseTechniqueType * LightingBase, struct LightPoint * Lights, uinteger Size);

void LightingBaseSetSpotLights(struct LightingBaseTechniqueType * LightingBase, struct LightSpot * Lights, uinteger Size);

void LightingBaseSetEyeWorldPos(struct LightingBaseTechniqueType * LightingBase, struct Vector3f * EyeWorldPos);

void LightingBaseSetMatSpecularIntensity(struct LightingBaseTechniqueType * LightingBase, float Intensity);

void LightingBaseSetMatSpecularPower(struct LightingBaseTechniqueType * LightingBase, float Power);

void LightingBaseDestroy(struct LightingBaseTechniqueType * LightingBase);

#endif // GRAPHICS_LIGHTING_BASE_H
