/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2016 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/ModelRenderSystem.h>
#include <Graphics/MeshManager.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct ModelRenderSystemType
{
	struct MeshManagerType MeshManager;
	//struct SkinManagerType SkinManager;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize model render system
////////////////////////////////////////////////////////////
bool ModelRenderSystemInitialize(struct EntitySystemType * EntitySystem)
{
	if (EntitySystem)
	{
		struct ModelRenderSystemType * ModelRenderSystem = NULL;

		// Initialize entity system
		EntityBaseSystemInitialize(EntitySystem, ENTITY_TYPE_MODEL);

		// Initialize entity render implementation
		ModelRenderSystem = (struct ModelRenderSystemType *)MemoryAllocate(sizeof(struct ModelRenderSystemType));

		if (ModelRenderSystem)
		{
			// Initialize mesh manager
			if (MeshManagerInitialize(&ModelRenderSystem->MeshManager))
			{
				// Set model render system implementation
				EntitySystem->Impl = ModelRenderSystem;

				return true;
			}

			/*
			MeshManagerInitialize:

			// Initialize model render vectors
			ModelRenderSystem->ModelMeshList = VectorNewT(EntityIdType);

			if (ModelRenderSystem->ModelMeshList)
			{
				ModelRenderSystem->ModelSkinList = VectorNewT(EntityIdType);

				if (ModelRenderSystem->ModelSkinList)
				{
					return true;
				}

				// Destroy mesh list if skin has not been allocated
				VectorDestroy(ModelRenderSystem->ModelMeshList);
			}
			*/

			// Destroy model render system on vector allocation errors
			MemoryDeallocate(ModelRenderSystem);
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Destroy model render system
////////////////////////////////////////////////////////////
void ModelRenderSystemDestroy(struct EntitySystemType * EntitySystem)
{
	if (EntitySystem)
	{
		// Destroy implementation
		if (EntitySystem->Impl)
		{
			struct ModelRenderSystemType * ModelRenderSystem = EntitySystem->Impl;

			// Destroy mesh manager
			MeshManagerDestroy(&ModelRenderSystem->MeshManager);

			/*
			// Destroy model vectors
			VectorDestroy(ModelRenderSystem->ModelMeshList);
			VectorDestroy(ModelRenderSystem->ModelSkinList);
			*/
		}

		// Destroy entity system
		EntityBaseSystemDestroy(EntitySystem);
	}
}

////////////////////////////////////////////////////////////
/// Insert model into render system
////////////////////////////////////////////////////////////
void ModelRenderSystemInsert(struct EntitySystemType * EntitySystem, EntityIdType EntityId)
{
	if (EntitySystem && EntityId != ENTITY_INDEX_INVALID)
	{
		// Get entity
		struct EntityType * Entity = EntityGet(ENTITY_TYPE_MODEL, EntityId);


	}
}

////////////////////////////////////////////////////////////
/// Remove model from render system
////////////////////////////////////////////////////////////
void ModelRenderSystemRemove(struct EntitySystemType * EntitySystem, EntityIdType EntityId)
{

}

////////////////////////////////////////////////////////////
/// Render model in the render system
////////////////////////////////////////////////////////////
void ModelRenderSystemRender(struct EntitySystemType * EntitySystem, EntityIdType EntityId)
{

}

////////////////////////////////////////////////////////////
/// Render all models in the render system
////////////////////////////////////////////////////////////
void ModelRenderSystemRenderAll(struct EntitySystemType * EntitySystem)
{

}

////////////////////////////////////////////////////////////
/// Commit all model list into render system
////////////////////////////////////////////////////////////
void ModelRenderSystemCommit(struct EntitySystemType * EntitySystem)
{
	if (EntitySystem && EntitySystem->Impl)
	{
		struct ModelRenderSystemType * ModelRenderSystem = EntitySystem->Impl;
	}
}
