/****************************************************************************

  IqeB_GUI_ToolsMeshPartsWin.cpp

  16.02.2015 RR: First editon of this file.


*****************************************************************************
*/

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Button.H>

/************************************************************************************
 * Globals
 *
 */

/************************************************************************************
 * Statics
 */

static  Fl_Window  *pMyToolWin;
static int MyWinPosX = IQE_GUI_NO_WINPOS_X, MyWinPosY = IQE_GUI_NO_WINPOS_Y; // last window position

static Fl_Browser *pGUI_WbenchBrowser;      // workbench browser
static Fl_Button *pGUI_WbenchButAdd;        // Work bench button 'Add'
static Fl_Button *pGUI_WbenchButRemove;     // Work bench button 'Remove'
static Fl_Button *pGUI_WbenchButClear;      // Work bench button 'Clear'
static Fl_Button *pGUI_WbenchButMerge;      // Work bench button 'Merge'

/************************************************************************************
 * update workbench browser
 */

// -- update model container

static void WorkbenchUpdate( int KeepSelection)
{
  int i;
  int LastSelection;
  int TempActive;

  LastSelection = pGUI_WbenchBrowser->value();  // get index of selected item

  pGUI_WbenchBrowser->clear();                  // empty the list box

  // fill the list box

  for( i = 0; i < ModelContainer_n; i++) {

    pGUI_WbenchBrowser->add( ModelContainer[ i].DispName);
  }

  if( KeepSelection && LastSelection > 0 && LastSelection <= pGUI_WbenchBrowser->size()) {

    pGUI_WbenchBrowser->select( LastSelection);         // set to last selected

    pGUI_WbenchBrowser->middleline( ModelContainer_n);
  } else if( ModelContainer_n > 0) {                    // have any

    pGUI_WbenchBrowser->select( ModelContainer_n);      // set to last added

    pGUI_WbenchBrowser->middleline( ModelContainer_n);
  }

  // Update workbench buttons

  TempActive = ModelContainer_n < MODEL_CONTAINER_MAX &&    // have free space
               pDrawModel != NULL &&                        // have a model loaded
               DrawModelSelName[ 0] != '\0';                //
  IqeB_GUI_WidgetActivate( pGUI_WbenchButAdd, TempActive);

  TempActive = pGUI_WbenchBrowser->value() > 0 &&           // selction is in range
               pGUI_WbenchBrowser->value() <= ModelContainer_n;
  IqeB_GUI_WidgetActivate( pGUI_WbenchButRemove, TempActive);

  TempActive = ModelContainer_n > 0;                        // Have any in
  IqeB_GUI_WidgetActivate( pGUI_WbenchButClear, TempActive);

  TempActive = ModelContainer_n > 1;                        // More than one
  IqeB_GUI_WidgetActivate( pGUI_WbenchButMerge, TempActive);

}

/************************************************************************************
 * Callback, workbench entry is selected
 */

static void WorkbenchSelectCallback(Fl_Widget *w, void *data)
{
  int this_item;

  this_item = pGUI_WbenchBrowser->value();  // get index of selected item

  if( this_item > 0 && this_item <= ModelContainer_n) { // indexes is in range

    this_item -= 1;   // adapte index from 1 based to 0 based table access

    IqeB_DispPrepareContainerModel( ModelContainer + this_item);
  }
}

/************************************************************************************
 * Callback, workbench button 'Add' pressed
 */

static void WorkbenchButAddCallback(Fl_Widget *w, void *data)
{

  if( pDrawModel == NULL ||                   // have NO model loaded
      DrawModelSelName[ 0] == '\0') {         //

    IqeB_DispMessage( DRAW_MSG_COL_ERR,
                      (char *)"Add:",
                      (char *)"Please select a file",
                      (char *)"with the file browser");

  } else if( ModelContainer_n >= MODEL_CONTAINER_MAX) {    // container is full

    IqeB_DispMessage( DRAW_MSG_COL_ERR,
                      (char *)"Add:", (char *)"Model container is full", NULL);

  } else if( ModelContainer_n < MODEL_CONTAINER_MAX &&    // have free space
             pDrawModel != NULL &&                        // have a model loaded
             DrawModelSelName[ 0] != '\0') {              //

    // copy model and names

    if( pDrawModel->skel->joint_count == 0 && pDrawModel->anim_poses > 0) { // have NO sekelton but poses from an animation

      sprintf( ModelContainer[ ModelContainer_n].DispName, "M%2d,P%2d,A%2d,%s",
               pDrawModel->mesh->part_count,
               pDrawModel->anim_poses,
               pDrawModel->anim_count,
               DrawModelSelName);
    } else {

      sprintf( ModelContainer[ ModelContainer_n].DispName, "M%2d,J%2d,A%2d,%s",
               pDrawModel->mesh->part_count,
               pDrawModel->skel->joint_count,
               pDrawModel->anim_count,
               DrawModelSelName);
    }

    // copy the model from the file browser

    strcpy( ModelContainer[ ModelContainer_n].FileName, DrawModelSelName);
    strcpy( ModelContainer[ ModelContainer_n].ModelFullName, DrawModelSelFullName);
    IqeB_ModelCopy( &pDrawModel, &(ModelContainer[ ModelContainer_n].pModel), IQE_MODEL_COPY_NEW_ALL);

    // remember the original mesh count

    ModelContainer[ ModelContainer_n].part_count_before_add = pDrawModel->mesh->part_count;

    // If this model ist not the first model in the model container,
    // copy mesh and or sekelton from the first model in the container
    // if we have only an animation

    char AddMessage[ 256];

    strcpy( AddMessage, "");

    if( ModelContainer_n > 0 &&                    // is NOT the first model
        ModelContainer[ ModelContainer_n].pModel->anim_count > 0) { // have an animation

      int CopyFlags;

      CopyFlags = 0;                               // preset nothing to copy

      // Can copy skeleton.
      // On Merge this copied skeleton is skipped
      if( ModelContainer[ ModelContainer_n].pModel->skel->joint_count == 0 &&  // have NO skeleton
          ModelContainer[ 0].pModel->skel->joint_count > 0 &&                  // but first model has one
          ModelContainer[ ModelContainer_n].pModel->anim_poses ==              // and number of poses of this model
            ModelContainer[ 0].pModel->skel->joint_count) {                    // match the number of joints of the first model

        CopyFlags |= IQE_MODEL_COPY_SKELETON;   // copy the skeleton

        strcat( AddMessage, "Copied skeleton");
      }

#ifdef use_again
      if( ModelContainer[ ModelContainer_n].pModel->mesh->part_count == 0 &&    // have NO meshes
          ModelContainer[ ModelContainer_n].pModel->mesh->vertex_count == 0 &&  // and have NO vertex data
          ModelContainer[ 0].pModel->mesh->part_count > 0 &&                    // but first model have a meshes
          ModelContainer[ 0].pModel->mesh->vertex_count > 0 &&                  // and first model has vertex data
          ((ModelContainer[ ModelContainer_n].pModel->anim_poses ==             // and number of poses of this model
             ModelContainer[ 0].pModel->skel->joint_count) ||                   // match the number of joints of the first model
           (ModelContainer[ ModelContainer_n].pModel->skel->joint_count >= 0 && //   or this model has a skeleton
             ModelContainer[ 0].pModel->skel->joint_count == 0))) {             //   and the first model has NO skeleton

        CopyFlags |= COPYMODEL_MESH;       // copy the mesh

        if( (CopyFlags & IQE_MODEL_COPY_SKELETON) != 0) {   // also copy skeleton

          strcat( AddMessage, " and mesh");
        } else {

          strcat( AddMessage, "Copied mesh");
        }
      }
#endif

      if( CopyFlags != 0) {     // have something to copy

        IqeB_ModelCopy( &(ModelContainer[ 0].pModel), &(ModelContainer[ ModelContainer_n].pModel), CopyFlags);

        strcat( AddMessage, " from 1. model");
      }
    }

    // ....

    ModelContainer_n += 1;                         // have one more

    WorkbenchUpdate( false);                       // update model container

    // display the just added model

    IqeB_DispPrepareContainerModel( ModelContainer + ModelContainer_n - 1);

    IqeB_DispMessage( DRAW_MSG_COL_OK,
                        (char *)"Add done", AddMessage, NULL);

    IqeB_GUI_UpdateWidgets();            // Also update the GUI

  } else {

    IqeB_DispMessage( DRAW_MSG_COL_ERR,
                      (char *)"Add failed", NULL, NULL);
  }
}

/************************************************************************************
 * Callback, workbench button 'Remove' pressed
 */

static void WorkbenchButRemoveCallback(Fl_Widget *w, void *data)
{
  int i, this_item;

  this_item = pGUI_WbenchBrowser->value();  // get index of selected item

  if( this_item > 0 && this_item <= ModelContainer_n) { // indexes is in range

    this_item -= 1;   // adapte index from 1 based to 0 based table access

    if( this_item >= ModelContainer_n - 1) {    // is the last one

      // set last one to nil
      strcpy( ModelContainer[ ModelContainer_n - 1].DispName, "");
      strcpy( ModelContainer[ ModelContainer_n - 1].FileName, "");
      strcpy( ModelContainer[ ModelContainer_n - 1].ModelFullName, "");
      IqeB_ModelFreeData( &ModelContainer[ ModelContainer_n - 1].pModel, IQE_MODEL_FREE_ALL);
      ModelContainer[ ModelContainer_n - 1].pModel = NULL;

      ModelContainer_n -= 1;                // have one less
      WorkbenchUpdate( false);              // update model container

      // display the last model in the workbench

      if( ModelContainer_n > 0) {   // have any

        IqeB_DispPrepareContainerModel( ModelContainer + ModelContainer_n - 1);
      }
    } else {

      // copy down

      IqeB_ModelFreeData( &ModelContainer[ this_item].pModel, IQE_MODEL_FREE_ALL);

      for( i = this_item; i <  ModelContainer_n - 1; i++) {

        strcpy( ModelContainer[ i].DispName, ModelContainer[ i + 1].DispName);
        strcpy( ModelContainer[ i].FileName, ModelContainer[ i + 1].FileName);
        strcpy( ModelContainer[ i].ModelFullName, ModelContainer[ i + 1].ModelFullName);
        ModelContainer[ i].pModel = ModelContainer[ i + 1].pModel;
      }

      // set last one to nil
      strcpy( ModelContainer[ ModelContainer_n - 1].DispName, "");
      strcpy( ModelContainer[ ModelContainer_n - 1].FileName, "");
      strcpy( ModelContainer[ ModelContainer_n - 1].ModelFullName, "");
      ModelContainer[ ModelContainer_n - 1].pModel = NULL;

      ModelContainer_n -= 1;                // have one less
      WorkbenchUpdate( true);               // update model container

      // display the model copied down

      IqeB_DispPrepareContainerModel( ModelContainer + this_item);
    }

    IqeB_DispMessage( DRAW_MSG_COL_OK,
                      (char *)"Remove done", NULL, NULL);

    IqeB_GUI_UpdateWidgets();            // Also update the GUI

  } else {

    IqeB_DispMessage( DRAW_MSG_COL_OK,
                      NULL, NULL, NULL);
  }
}

/************************************************************************************
 * Callback, workbench button 'Clear' pressed
 */

static void WorkbenchButClearCallback(Fl_Widget *w, void *data)
{
  int i;

  if( ModelContainer_n > 0) {       // have something to clear

    for( i = 0; i <  ModelContainer_n; i++) {

      strcpy( ModelContainer[ i].DispName, "");
      strcpy( ModelContainer[ i].FileName, "");
      strcpy( ModelContainer[ i].ModelFullName, "");
      IqeB_ModelFreeData( &ModelContainer[ i].pModel, IQE_MODEL_FREE_ALL);
    }

    ModelContainer_n = 0;             // set count to 0

    WorkbenchUpdate( false);          // update model container

    IqeB_DispMessage( DRAW_MSG_COL_OK,
                     (char *)"Clear done", NULL, NULL);

    IqeB_GUI_UpdateWidgets();            // Also update the GUI

  } else {

    IqeB_DispMessage( DRAW_MSG_COL_OK,
                      NULL, NULL, NULL);
  }
}

/************************************************************************************
 * Callback, workbench button 'Merge' pressed
 */

static void WorkbenchButMergeCallback(Fl_Widget *w, void *data)
{
  int i;
  IQE_model *pModel;

  if( ModelContainer_n > 1) {       // have something to merge

    for( i = 1; i <  ModelContainer_n; i++) {

      // merge to first

      IqeB_ModelMergeTo( ModelContainer[ 0].pModel, ModelContainer[ i].pModel);

      // delete

      strcpy( ModelContainer[ i].DispName, "");
      strcpy( ModelContainer[ i].FileName, "");
      strcpy( ModelContainer[ i].ModelFullName, "");
      IqeB_ModelFreeData( &ModelContainer[ i].pModel, IQE_MODEL_FREE_ALL);
    }

    ModelContainer_n = 1;             // set count to

    pModel = ModelContainer[ 0].pModel;    // the merged model

    if( pModel->skel->joint_count == 0 && pModel->anim_poses > 0) { // have NO sekelton but poses from an animation

      sprintf( ModelContainer[ 0].DispName, "M%2d,P%2d,A%2d,%s",
               pModel->mesh->part_count,
               pModel->anim_poses,
               pModel->anim_count,
               ModelContainer[ 0].FileName);
    } else {

      sprintf( ModelContainer[ 0].DispName, "M%2d,J%2d,A%2d,%s",
               pModel->mesh->part_count,
               pModel->skel->joint_count,
               pModel->anim_count,
               ModelContainer[ 0].FileName);
    }

    WorkbenchUpdate( false);          // update model container

    IqeB_DispPrepareContainerModel( ModelContainer + 0);

    IqeB_DispMessage( DRAW_MSG_COL_OK,
                     (char *)"Merge done", NULL, NULL);

    IqeB_GUI_UpdateWidgets();            // Also update the GUI

  } else {

    IqeB_DispMessage( DRAW_MSG_COL_OK,
                      NULL, NULL, NULL);
  }
}

/************************************************************************************
 * IqeB_GUI_UpdateCallback
 *
 * Is called from IqeB_GUI_UpdateWidgets() if something has changed
 * what needs an update (new model displayed, ...).
 */

static void IqeB_GUI_UpdateCallback( int UpdateFlags)
{

  if( pMyToolWin == NULL) {  // security test, has main window

    return;
  }

  WorkbenchUpdate( true);               // update model container
}

/************************************************************************************
 * close_cb, close this window
 *
 * pValueArg is a pointer to the widget. Set this pointer to NULL on deletion.
 */

static void close_cb( Fl_Widget *w, void *pValueArg)
{

  IqeB_GUI_CloseToolWindow( (void **)&pMyToolWin);
}

/************************************************************************************
 * Presets for this tools window
 *
 */

#ifdef use_again // 16.02.2014 RR: Keep this until we need preferences
 static T_GUI_PreferenceEntry MyPreferences[] =
 {

  // Box display

  { PREF_T_INT,   "BoxEnable",      "0", &IqeB_ToolsDisp_BoxEnable},
  { PREF_T_FLOAT, "BoxHeight1",  "32.0", &IqeB_ToolsDisp_BoxHeight1},
  { PREF_T_FLOAT, "BoxHeight2", "-24.0", &IqeB_ToolsDisp_BoxHeight2},
  { PREF_T_FLOAT, "BoxWidth",    "32.0", &IqeB_ToolsDisp_BoxWidth},
};
#endif

// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "ToolsMeshWorkbench", NULL, 0,
                                               (void **)(&pMyToolWin), &MyWinPosX, &MyWinPosY);

/************************************************************************************
 * IqeB_GUI_ToolsMeshWorkbenchWin
 *
 * Open dialog
 */

void IqeB_GUI_ToolsMeshWorkbenchWin( int xLeft, int xRight, int yTop, int yBotton)
{

  //
  // winodow already created --> show it
  //

  if( pMyToolWin != NULL) {         // already have tool window

    // Show invisble window or bring visible window to foreground

    pMyToolWin->show();          // show it

    return;
  }

  //
  // creation of window on first call
  //

  if( pMyToolWin == NULL) {  // no tool window until now

    int xPos, yPos;

    xPos = xRight;
    yPos = yTop;

    if( MyWinPosX != IQE_GUI_NO_WINPOS_X && MyWinPosY != IQE_GUI_NO_WINPOS_Y) { // have last window position

      xPos = MyWinPosX;
      yPos = MyWinPosY;
    }

    pMyToolWin = new Fl_Window( xPos, yPos, IQE_GUI_TOOLS_STD_WITDH, 400, "Mesh/Workbench");

    if( pMyToolWin == NULL) {  // security test

      return;
    }

    IqeB_GUI_RegisterCallback( (void **)&pMyToolWin, IqeB_GUI_UpdateCallback, IQE_GUI_UPDATE_DEFAULT);
  }

  //
  //  GUI things
  //

  int x, x1, xx, xx2, y1, y, yy, yy2;

  x1  = 4;
  xx  = pMyToolWin->w() - 8;
  yy  = 32;

  y1 = 20;
  y = y1;

  // Workbench browser

  yy2 = 320;

  pGUI_WbenchBrowser = new Fl_Browser( x1, y1, xx, yy2, "Workbench");

  pGUI_WbenchBrowser->align( FL_ALIGN_TOP_LEFT);     // align for label

  pGUI_WbenchBrowser->type(FL_HOLD_BROWSER);        // use for single selection
  pGUI_WbenchBrowser->callback( WorkbenchSelectCallback, NULL);

  y += yy2;

  // Workbench buttons

  y += 4;
  yy = 22;

  x = x1;
  xx2 = (xx - 12) / 4;

  pGUI_WbenchButAdd = new Fl_Button( x, y, xx2, yy, "&Add");
  pGUI_WbenchButAdd->callback( WorkbenchButAddCallback, NULL);
  pGUI_WbenchButAdd->tooltip( "Add displayed model to workbench");

  x += xx2 + 4;
  pGUI_WbenchButRemove = new Fl_Button( x, y, xx2, yy, "&Remove");
  pGUI_WbenchButRemove->callback( WorkbenchButRemoveCallback, NULL);
  pGUI_WbenchButRemove->tooltip( "Remove selection from workbench");

  x += xx2 + 4;
  pGUI_WbenchButClear = new Fl_Button( x, y, xx2, yy, "&Clear");
  pGUI_WbenchButClear->callback( WorkbenchButClearCallback, NULL);
  pGUI_WbenchButClear->tooltip( "Empty workbench");

  x += xx2 + 4;
  pGUI_WbenchButMerge = new Fl_Button( x, y, xx2, yy, "&Merge");
  pGUI_WbenchButMerge->callback( WorkbenchButMergeCallback, NULL);
  pGUI_WbenchButMerge->tooltip( "Merge all workbench models");

  y += yy;

  // finish up

  pMyToolWin->size( pMyToolWin->w(), y + 4);  // final size of window

  pMyToolWin->end();
  pMyToolWin->set_non_modal();
  pMyToolWin->callback( close_cb, &pMyToolWin);

  WorkbenchUpdate( false); // Update the GUI

  pMyToolWin->show();
}

/********************************** End Of File **********************************/
