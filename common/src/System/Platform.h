/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_PLATFORM_H
#define SYSTEM_PLATFORM_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
/// Get the current system time
///
/// \return System time, in milliseconds
///
////////////////////////////////////////////////////////////
float SystemGetTime();

////////////////////////////////////////////////////////////
/// Suspend the execution of the current thread for a specified time
///
/// \param Time : Time to sleep, in milliseconds
///
////////////////////////////////////////////////////////////
void SystemSleep(float Milliseconds);

////////////////////////////////////////////////////////////
/// Assign the current thread to one processor. This ensures
/// that timing code runs on only one processor, and will
/// not suffer any ill effects from power management
////////////////////////////////////////////////////////////
void SystemSetProcessorAffinity();

#ifdef SYSTEM_MACOS

#include <CoreFoundation/CoreFoundation.h>
#include <System/IOHelper.h>

#define SYSTEM_PLATFORM_WORKING_DIRECTORY_SIZE 0x1000

////////////////////////////////////////////////////////////
/// Under Mac OS X, when launching an application from the Finder,
/// the default working directory is the user home directory ;
/// when launching from Xcode, the default one is the directory
/// containing the application. In order to produce a uniform behaviour
/// and simplify the use of resources, sets the working directory to
/// the Resources folder of the application bundle.
/// The "constructor" attribute forces the function to be called
/// at library loading time.
////////////////////////////////////////////////////////////
void InitializeWorkingDirectory(void)
{
	uint8 PathBuffer[SYSTEM_PLATFORM_WORKING_DIRECTORY_SIZE];
	bool Encoded = false;

	// Get the application bundle
	CFBundleRef MainBundle = CFBundleGetMainBundle();
	assert(MainBundle != NULL);

	// Get the resource directory URL
	CFURLRef ResourceDirectory = CFBundleCopyResourcesDirectoryURL(MainBundle);
	assert(ResourceDirectory != NULL);

	// Convert it as absolute URL
	CFURLRef AbsoluteURL = CFURLCopyAbsoluteURL(ResourceDirectory);
	assert(AbsoluteURL != NULL);

	// Get the path as C string
	Encoded = CFURLGetFileSystemRepresentation(AbsoluteURL, true, (uint8*)PathBuffer, SYSTEM_PLATFORM_WORKING_DIRECTORY_SIZE);
	assert(Encoded);

	// Set the working directory
	chdir(PathBuffer);

	CFRelease(AbsoluteURL);
	CFRelease(ResourceDirectory);
}

#endif // SYSTEM_MACOS

#endif // SYSTEM_PLATFORM_H
