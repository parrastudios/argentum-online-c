/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/TextureRender.h>
#include <Graphics/Texture.h>
#include <Graphics/TextureManager.h>
#include <Graphics/Device.h>
#include <Math/Geometry/Rect.h>
#include <Window/Window.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
static struct Color4ub ColorDefault = { 255, 255, 255, 255 };

////////////////////////////////////////////////////////////
/// Render a grh from index
/// (without color and rect specification)
////////////////////////////////////////////////////////////
void TextureRenderGrh(int32 GrhID, int32 X, int32 Y)
{
    struct GrhData * GrhPtr = IndexGetGrh(GrhID);
	struct Rect RectData = { GrhPtr->sX, GrhPtr->sY,
							 GrhPtr->sX + GrhPtr->PixelWidth,
							 GrhPtr->sY + GrhPtr->PixelHeight };

	// Render
	TextureRender(GrhID, X, Y, &RectData, &ColorDefault);
}

////////////////////////////////////////////////////////////
/// Render a grh from index with alpha specification
/// (without rect specification)
////////////////////////////////////////////////////////////
void TextureRenderGrhA(int32 GrhID, int32 X, int32 Y, uint8 Alpha)
{
    struct Color4ub Color = { 255, 255, 255, Alpha };
    struct GrhData * GrhPtr = IndexGetGrh(GrhID);
	struct Rect RectData = { GrhPtr->sX, GrhPtr->sY,
							 GrhPtr->sX + GrhPtr->PixelWidth,
							 GrhPtr->sY + GrhPtr->PixelHeight };

	// Render
	TextureRender(GrhID, X, Y, &RectData, &Color);
}

////////////////////////////////////////////////////////////
/// Render a grh from index with color specification
/// (without rect specification)
////////////////////////////////////////////////////////////
void TextureRenderGrhColor(int32 GrhID, int32 X, int32 Y, struct Color4ub * Color)
{
    struct GrhData * GrhPtr = IndexGetGrh(GrhID);
	struct Rect RectData = { GrhPtr->sX, GrhPtr->sY,
							 GrhPtr->sX + GrhPtr->PixelWidth,
							 GrhPtr->sY + GrhPtr->PixelHeight };
	// Render
	TextureRender(GrhID, X, Y, &RectData, Color);
}


////////////////////////////////////////////////////////////
/// Render a grh from index with color specification
/// (repeating it in all rect)
////////////////////////////////////////////////////////////
void TextureRenderGrhColorRepeat(int32 GrhID, int32 Left, int32 Top, int32 Right, int32 Bottom, struct Color4ub * Color)
{
    struct GrhData * GrhPtr = IndexGetGrh(GrhID);
	struct Rect GrhRect = { GrhPtr->sX, GrhPtr->sY,
							GrhPtr->sX + GrhPtr->PixelWidth,
							GrhPtr->sY + GrhPtr->PixelHeight };

	// Load it, if it isn't loaded
	if (TextureLoad(GrhID, NULL, false, &GrhRect))
	{
		struct Texture * Tex = TextureGetData(GrhID);
		float CoordX, CoordY;

		int32 Width = Right - Left;
		int32 Height = Bottom - Top;

		// Bind texture
		TextureBind(Tex);

		// Calculate coordinates
		CoordX = Width / (float)Tex->TextureWidth;
		CoordY = Height / (float)Tex->TextureHeight;

		// Blend
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Set color
		glColor4ub(Color->r, Color->g, Color->b, Color->a);

		// Render
		glBegin(GL_TRIANGLE_STRIP);
			glTexCoord2f(0, 0);				glVertex2i(Left, Top);
			glTexCoord2f(CoordX, 0);		glVertex2i(Left + Width, Top);
			glTexCoord2f(0, CoordY);		glVertex2i(Left, Top + Height);
			glTexCoord2f(CoordX, CoordY);	glVertex2i(Left + Width, Top + Height);
		glEnd();
	}
}

////////////////////////////////////////////////////////////
/// Render a grh from pointer
/// (without color and rect specification)
////////////////////////////////////////////////////////////
void TextureRenderGrhPtr(struct GrhData * Grh, int32 X, int32 Y)
{
	// struct Rect RectData = { Grh->sX, Grh->sY,
	//						 Grh->sX + Grh->PixelWidth,
	//						 Grh->sY + Grh->PixelHeight };
	// Render
	// TextureRender(Grh->FileNumber, X, Y, &RectData, &ColorDefault);
}

////////////////////////////////////////////////////////////
/// Render a circle
/// it starts filling the circle from StartTheta degrees to Alpha (max 360)
/// @param Smothness : Number of segments
////////////////////////////////////////////////////////////
void TextureRenderGrhRound(int32 GrhID, int32 CX, int32 CY, int32 Radio, uinteger Smoothness, float StartTheta, float Alpha, struct Color4ub * Color)
{
	struct GrhData * GrhPtr = IndexGetGrh(GrhID);
	struct Rect GrhRect = { GrhPtr->sX, GrhPtr->sY,
		GrhPtr->sX + GrhPtr->PixelWidth,
		GrhPtr->sY + GrhPtr->PixelHeight };

	if (TextureLoad(GrhID, NULL, false, &GrhRect))
	{
		struct Texture * Tex = TextureGetData(GrhID);
		float AlphaRad, Theta, StartRad;
		uinteger i;
		float x, y;

		glColor4ub(Color->r, Color->g, Color->b, Color->a);

		TextureBind(Tex);

		glEnable(GL_TEXTURE_2D);

		glBegin(GL_TRIANGLE_FAN);

		glTexCoord2f(0.5f, 0.5f);

		// Center vertex
		glVertex2i(CX, CY);

		// Get the current angle  i / Smoothness
		StartRad = StartTheta * 3.1415926f / 180;

		AlphaRad = Alpha * 3.1415926f / 180;

		for (i = 0; i <= Smoothness; i++)
		{
			// Get the current angle
			Theta = StartRad + (AlphaRad * i / Smoothness);

			// Calculate the x component
			x = (float)(Radio * cos(Theta));

			// Calculate the y component
			y = (float)(-Radio * sin(Theta));

			glTexCoord2f(0.5f + ((GLfloat)(cos(Theta)) / 2.0f), 0.5f + ((GLfloat)(-sin(Theta)) / 2.0f));

			// Output vertex
			glVertex2f(x + (float)CX, y + (float)CY);

		}

		glEnd();
	}
}
