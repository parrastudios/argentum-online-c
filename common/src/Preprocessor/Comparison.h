/////////////////////////////////////////////////////////////////////////////
//	Preprocessor Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A cross-platform macro based library to give support
//	for preprocessor metaprogramming.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PREPROCESSOR_COMPARISON_H
#define PREPROCESSOR_COMPARISON_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Preprocessor/If.h>
#include <Preprocessor/Detection.h>
#include <Preprocessor/Concatenation.h>
#include <Preprocessor/Bit.h>
#include <Preprocessor/Empty.h>

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define PREPROCESSOR_COMPARE_IMPL(Left, Right) \
	PREPROCESSOR_DETECT_PARENTHESIS \
	( \
		PREPROCESSOR_COMPARE_ ## Left (PREPROCESSOR_COMPARE_ ## Right) (()) \
	)

#define PREPROCESSOR_NOT_EQUAL(Left, Right) \
	PREPROCESSOR_IF_IMPL( \
		PREPROCESSOR_BIT_AND(PREPROCESSOR_DETECT_COMPARABLE(Left))(PREPROCESSOR_DETECT_COMPARABLE(Right)), \
		PREPROCESSOR_COMPARE_IMPL, \
		1 PREPROCESSOR_EMPTY_EXPANSION_VARIDIC \
	)(Left, Right)


#define PREPROCESSOR_EQUAL(Left, Right) \
	PREPROCESSOR_COMPL(PREPROCESSOR_NOT_EQUAL(Left, Right))

#endif // PREPROCESSOR_COMPARISON_H
