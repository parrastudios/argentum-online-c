/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Language.h>
#include <Game/Configuration.h>
#include <System/IOHelper.h>
#include <System/Error.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
char Messages[LANGUAGE_MESSAGE_SIZE][LANGUAGE_MESSAGE_LENGHT];

////////////////////////////////////////////////////////////
/// Lenguage initializer
////////////////////////////////////////////////////////////
bool LanguageInitialize()
{
	int32 Idiom = ConfigGet()->Language;

	if (LanguageLoad(Idiom))
		return true;

	return false;
}

////////////////////////////////////////////////////////////
/// Message loader
////////////////////////////////////////////////////////////
bool LanguageLoad(int32 Idiom)
{
	FILE * LangFile;
	char Path[0xFF];
	uint32 i;

	switch (Idiom)
	{
		case LANGUAGE_SPANISH :
			strcpy(Path, "data/lang/esp.lng");
			break;

		case LANGUAGE_ENGLISH :
			strcpy(Path, "data/lang/eng.lng");
			break;

		default :
			MessageError("LanguageLoad", "An internal language error : The specified language doesn't exist.");
			return false;
	}

	if (!fopen(&LangFile, Path, "r"))
	{
		MessageError("LanguageLoad", "An internal language error : '%s' not found or corrupted.", Path);
		return false;
	}

	// Read all data
	for (i = 0; i < LANGUAGE_MESSAGE_SIZE; i++)
	{
		// Get the line
		fgets(&Messages[i][0], LANGUAGE_MESSAGE_LENGHT, LangFile);
		
		// Set the last char to null
		Messages[i][strlen(Messages[i]) - 1] = NULLCHAR;
	}

	fclose(LangFile);

	return true;
}

////////////////////////////////////////////////////////////
/// Message method
////////////////////////////////////////////////////////////
char * LanguageMessage(uint32 MessageIndex)
{
	if (MessageIndex < LANGUAGE_MESSAGE_SIZE)
		return Messages[MessageIndex];

	return NULL;
}

////////////////////////////////////////////////////////////
/// Get the message id with a message text
////////////////////////////////////////////////////////////
uint32 LanguageMessageText(char * MessageType)
{
	if		(strcmp(MessageType, "CONNECT") == 0)	return MSG_CONNECT;
	else if (strcmp(MessageType, "EXIT") == 0)		return MSG_EXIT;
	else if (strcmp(MessageType, "USER") == 0)		return MSG_USER;
	else if (strcmp(MessageType, "PASSWORD") == 0)	return MSG_PASSWORD;
	else if (strcmp(MessageType, "ACCOUNT") == 0)	return MSG_ACCOUNT;
	else if (strcmp(MessageType, "LEVEL") == 0)		return MSG_LEVEL;
	else if (strcmp(MessageType, "GOLD") == 0)		return MSG_GOLD;
	else if (strcmp(MessageType, "DEAD") == 0)		return MSG_DEAD;
	else if (strcmp(MessageType, "YES") == 0)		return MSG_YES;
	else if (strcmp(MessageType, "NO") == 0)		return MSG_NO;


	return LANGUAGE_MSG_ERROR;
}

////////////////////////////////////////////////////////////
/// Simplification method
////////////////////////////////////////////////////////////
char * LanguageMessageGet(char * Type)
{
	return LanguageMessage(LanguageMessageText(Type));
}