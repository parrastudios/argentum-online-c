/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef METADATA_RUNTIME_H
#define METADATA_RUNTIME_H

////////////////////////////////////////////////////////////
/// Syntactic sugar for metaprogramming
///
/// Use case:
///
/// Dog.h
///
/// // class DogType {
/// //	Function bark();
///	//	Function run(float x, float y, float z);
///	// };
/// METADATA_RUNTIME_CLASS_DECL(DogType);
///
/// Dog.c
///
/// void DogBark() {
///		printf("wouf!\n");
/// }
///
/// bool DogRun(float x, float y, float z) {
///		printf("running to (%f, %f, %f)\n", x, y, z);
///		return true;
/// }
///
/// METADATA_RUNTIME_CLASS_IMPL_DECL(DogType)
/// {
///		METADATA_RUNTIME_CLASS_OBJECT_DECL(DogType);
///
///		METADATA_RUNTIME_CLASS_OBJECT_METHOD("bark", void, DogBark, METADATA_PARAMETER_LIST_NULL);
///
///		METADATA_RUNTIME_CLASS_OBJECT_METHOD("run", bool, DogRun, ((float, x), (float, y), (float, z)));
///
///		METADATA_RUNTIME_CLASS_OBJECT_COMMIT();
/// }
///
/// Test.c
///
/// int main(int argc, char * argv[]) {
///		MetadataRuntimeClassType * DogRuntimeClass = METADATA_RUNTIME_CLASS(DogType);
///		MetadataRuntimeObjectType * Dog = DogRuntimeClass->CreateObject();
///
///		MetadataObjectExecuteName(Dog->Object, "bark");
///
///		// output: wouf!
///
///		DogRuntimeClass->DestroyObject(Dog);
///
///		return 0;
/// }
///
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/Object.h>
#include <MetaData/Bind.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define METADATA_RUNTIME_CLASS_PREFIX				MetadataRuntimeClassImpl
#define METADATA_RUNTIME_CLASS_CREATE_OBJECT_PREFIX	MetadataRuntimeClassCreateObject
#define METADATA_RUNTIME_CLASS_GET_PREFIX			MetadataRuntimeClassGet

typedef struct MetadataRuntimeClassType		MetadataRuntimeClassType;
typedef struct MetadataRuntimeObjectType	MetadataRuntimeObjectType;
typedef char *								MetadataRuntimeClassNameType;
typedef uinteger							MetadataRuntimeClassSizeType;
typedef MetadataRuntimeObjectType *			(*MetadataRuntimeClassCreateObject)();
typedef void								(*MetadataRuntimeClassDestroyObject)(MetadataRuntimeObjectType *);
typedef MetadataRuntimeClassType *			(*MetadataRuntimeClassGet)();

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define METADATA_RUNTIME_CLASS_STRINGIFY_DECL(Str)		#Str
#define METADATA_RUNTIME_CLASS_STRINGIFY(Str)			METADATA_RUNTIME_CLASS_STRINGIFY_DECL(Str)
#define METADATA_RUNTIME_CLASS_NAME(Type)				METADATA_RUNTIME_CLASS_PREFIX ## Type
#define METADATA_RUNTIME_CLASS_CREATE_OBJECT_NAME(Type)	METADATA_RUNTIME_CLASS_CREATE_OBJECT_PREFIX ## Type
#define METADATA_RUNTIME_CLASS_GET_NAME(Type)			METADATA_RUNTIME_CLASS_GET_PREFIX ## Type

#define METADATA_RUNTIME_CLASS_DECL(Type) \
	MetadataRuntimeObjectType * METADATA_RUNTIME_CLASS_CREATE_OBJECT_NAME(Type) ## (); \
	static MetadataRuntimeClassType METADATA_RUNTIME_CLASS_NAME(Type)

#define METADATA_RUNTIME_CLASS_IMPL_DERIVED_DECL(Type, ParentType) \
	MetadataRuntimeClassType METADATA_RUNTIME_CLASS_NAME(Type) = \
	{ \
		TypeInit(Name, METADATA_RUNTIME_CLASS_STRINGIFY(Type)), \
		TypeInit(Size, 0), /* TypeInit(Size, sizeof(Type)), */ \
		TypeInit(CreateObject, &METADATA_RUNTIME_CLASS_CREATE_OBJECT_NAME(Type)), \
		TypeInit(DestroyObject, &MetadataRuntimeClassDestroyObjectImpl), \
		TypeInit(Parent, ParentType) \
	}; \
	MetadataRuntimeClassType * METADATA_RUNTIME_CLASS_GET_NAME(Type) ## () \
	{ \
		return &METADATA_RUNTIME_CLASS_NAME(Type); \
	} \
	MetadataRuntimeObjectType * METADATA_RUNTIME_CLASS_CREATE_OBJECT_PREFIX ## Type ## ()

#define METADATA_RUNTIME_CLASS_IMPL_DECL(Type) \
	METADATA_RUNTIME_CLASS_IMPL_DERIVED_DECL(Type, NULL)

#define METADATA_RUNTIME_CLASS_OBJECT_DECL(Type) \
	struct MetadataRuntimeObjectType * Handle = MemoryAllocate(sizeof(struct MetadataRuntimeObjectType)); \
	do { \
		if (Handle != NULL) \
		{ \
			Handle->RuntimeClassGet = &METADATA_RUNTIME_CLASS_GET_NAME(Type); \
			Handle->Object = METADATA_STORAGE_INDEX_INVALID; /* todo: creation of object */ \
		} \
		else \
		{ \
			return NULL; \
		} \
	} while (0)

#define METADATA_RUNTIME_CLASS_OBJECT_METHOD(Name, Ptr) \
	MetadataObjectMethodCreate(Handle->Object, Name, Ptr)

#define METADATA_RUNTIME_CLASS_OBJECT_COMMIT() \
	return Handle

#define METADATA_RUNTIME_CLASS(Type) \
	&METADATA_RUNTIME_CLASS_NAME(Type)

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct MetadataRuntimeClassType
{
	MetadataRuntimeClassNameType			Name;
	MetadataRuntimeClassSizeType			Size;
	MetadataRuntimeClassCreateObject		CreateObject;
	MetadataRuntimeClassDestroyObject		DestroyObject;
	MetadataRuntimeClassType *				Parent;
};

struct MetadataRuntimeObjectType
{
	MetadataRuntimeClassGet					RuntimeClassGet;
	MetadataObjectType						Object;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Runtime class object destructor sugar
////////////////////////////////////////////////////////////
void MetadataRuntimeClassDestroyObjectImpl(MetadataRuntimeObjectType * Handle);

#endif // METADATA_RUNTIME_H
