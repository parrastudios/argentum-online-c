/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/Thread.h>
#include <stdio.h>

void * ThreadFunc(void * UserData);

////////////////////////////////////////////////////////////
/// Create and run the thread
////////////////////////////////////////////////////////////
void ThreadLaunch(Thread * ThreadEx, FuncType Function, void * UserData)
{
    int32 Error;

	// Set data from thread
	ThreadEx->Function  = Function;
	ThreadEx->Data      = UserData;

    // Create the thread
    ThreadEx->IsActive = true;
    Error = pthread_create(&ThreadEx->pThread, NULL, &ThreadFunc, ThreadEx);

    // Error ?
    if (Error != 0)
    {
        MessageError("ThreadLaunch", "Failed to create thread");
        ThreadEx->IsActive = false;
    }
}


////////////////////////////////////////////////////////////
/// Wait until the thread finishes
////////////////////////////////////////////////////////////
void ThreadWait(Thread * ThreadEx)
{
    if (ThreadEx->IsActive)
    {
        // Wait for the thread to finish, no timeout
        pthread_join(ThreadEx->pThread, NULL);

        // Reset the thread state
        ThreadEx->IsActive = false;
    }
}


////////////////////////////////////////////////////////////
/// Terminate the thread
/// Terminating a thread with this function is not safe,
/// you should rather try to make the thread function
/// terminate by itself
////////////////////////////////////////////////////////////
void ThreadTerminate(Thread * ThreadEx)
{
    if (ThreadEx->IsActive)
    {
        pthread_cancel(ThreadEx->pThread);
        ThreadEx->IsActive = false;
    }
}


////////////////////////////////////////////////////////////
/// Actual thread entry point, dispatches to instances
////////////////////////////////////////////////////////////
void * ThreadFunc(void * UserData)
{
    // The sfThread instance is stored in the user data
    Thread * ThreadToRun = (Thread*)(UserData);

    // Tell the thread to handle cancel requests immediatly
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

    // Forward to the instance
    if (ThreadToRun->Function)
        ThreadToRun->Function(ThreadToRun->Data);

    return NULL;
}
