/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef PLATFORM_H
#define PLATFORM_H

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Debug.h>

////////////////////////////////////////////////////////////
/// Identify the compiler
////////////////////////////////////////////////////////////
#define COMPILER_UNKNOWN			0x00
#define COMPILER_GCCE				0x01
#define COMPILER_WINSCW				0x02
#define COMPILER_MSVC				0x03
#define COMPILER_MSQC				0x04
#define COMPILER_MINGW				0x05
#define COMPILER_BORLAND			0x06
#define COMPILER_GNUC				0x07
#define COMPILER_WATCOM				0x08
#define COMPILER_POWERC				0x09
#define COMPILER_COMEAU				0x0A
#define COMPILER_CRAY				0x0B
#define COMPILER_CYGWIN				0x0C
#define COMPILER_COMPAQ				0x0D
#define COMPILER_DIAB				0x0E
#define COMPILER_DIGMARS			0x0F
#define COMPILER_HPCC				0x10
#define COMPILER_INTEL				0x11
#define COMPILER_KAY				0x12
#define COMPILER_SGI				0x13
#define COMPILER_MPW				0x14
#define COMPILER_MWERKS				0x15
#define COMPILER_NORCROFT			0x16
#define COMPILER_SCO				0x17
#define COMPILER_SUNPRO				0x18
#define COMPILER_TENDRA				0x19
#define COMPILER_USLC				0x1A
#define COMPILER_XLC				0x1B

#define CompilerVersionBuild(Major, Minor, Micro) (((Major) << 24) + ((Minor) << 16) + (Micro))

#if defined(__GCCE__)
#	define COMPILER_TYPE COMPILER_GCCE
#	define COMPILER_VERSION _MSC_VER
#	include <staticlibinit_gcce.h>
#elif defined(__WINSCW__)
#	define COMPILER_TYPE COMPILER_WINSCW
#	define COMPILER_VERSION _MSC_VER
#	pragma warning (disable: 4996)
#elif defined(_QC)
#	define COMPILER_TYPE COMPILER_MSQC
#	define COMPILER_VERSION _QC
#elif defined(_MSC_VER)
#	define COMPILER_TYPE COMPILER_MSVC
#	if defined(_MSC_FULL_VER)
#		define COMPILER_VERSION CompilerVersionBuild(_MSC_FULL_VER / 1000000, (_MSC_FULL_VER % 1000000) / 10000, _MSC_FULL_VER % 10000)
#	else
#		define COMPILER_VERSION CompilerVersionBuild(_MSC_VER / 100, _MSC_VER % 100, 0)
#	endif
#elif defined(__MINGW32__)
#	define COMPILER_TYPE COMPILER_MINGW
#	define COMPILER_VERSION __MINGW32__
#elif defined (__GNUC__)
#	define COMPILER_TYPE COMPILER_GNUC
#	if defined(__GNUC_PATCHLEVEL__)
#		define COMPILER_VERSION CompilerVersionBuild(__GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__)
#	else
#		define COMPILER_VERSION CompilerVersionBuild(__GNUC__, __GNUC_MINOR__, 0)
#	endif
#elif defined(__BORLANDC__)
#	define COMPILER_TYPE COMPILER_BORLAND
#	define COMPILER_VERSION __BCPLUSPLUS__
#	define __FUNCTION__ __FUNC__
#elif defined(__WATCOMC__)
#	define COMPILER_TYPE COMPILER_WATCOM
#	define COMPILER_VERSION __WATCOMC__
#elif defined(__POWERC)
#	define COMPILER_TYPE COMPILER_POWERC
#	define COMPILER_VERSION __POWERC
#elif defined(__COMO__)
#	define COMPILER_TYPE COMPILER_COMEAU
#	define COMPILER_VERSION __COMO_VERSION__
#elif defined(_CRAYC)
#	define COMPILER_TYPE COMPILER_CRAY
#	define COMPILER_VERSION _REVISION
#elif defined(__CYGWIN__)
#	define COMPILER_TYPE COMPILER_CYGWIN
#	define COMPILER_VERSION 0
#elif (defined(__DECC) || defined(__DECCXX)) || (defined(VAXC) || defined(__VAXC)) || (defined(__osf__) && defined(__LANGUAGE_C__) && !defined(__GNUC__))
#	define COMPILER_TYPE COMPILER_COMPAQ
#	if defined(__DECC_VER)
#		define COMPILER_VERSION __DECC_VER
#	else
#		define COMPILER_VERSION 0
#	endif
#elif ((defined(__DCC__) && defined(__VERSION_NUMBER__)))
#	define COMPILER_TYPE COMPILER_DIAB
#	define COMPILER_VERSION __VERSION_NUMBER__
#elif (defined(__DMC__) || (defined(__SC__) || defined(__ZTC__)))
#	define COMPILER_TYPE COMPILER_DIGMARS
#	if defined(__DMC__)
#		define COMPILER_VERSION __DMC__
#	else
#		define COMPILER_VERSION 0
#	endif
#elif (defined(__HP_aCC) || defined(__HP_cc) || ((__cplusplus - 0 >= 199707L) && defined(__hpux)))
#	define COMPILER_TYPE COMPILER_HPCC
#	if defined(__HP_aCC)
#		if (__HP_aCC == 1)
#			define COMPILER_VERSION 11500
#		else
#			define COMPILER_VERSION __HP_aCC
#		endif
#	elif defined(__HP_cc)
#		define COMPILER_VERSION __HP_cc
#	else
#		define COMPILER_VERSION 0
#	endif
#elif (defined(__INTEL_COMPILER) || defined(__ICC))
#	define COMPILER_TYPE COMPILER_INTEL
#	if defined(__INTEL_COMPILER)
#		define COMPILER_VERSION __INTEL_COMPILER
#	else
#		define COMPILER_VERSION 0
#	endif
#elif defined(__KCC)
#	define COMPILER_TYPE COMPILER_KAY
#	define COMPILER_VERSION __KCC_VERSION
#elif ((defined(sgi) || defined(__sgi)) && defined(_COMPILER_VERSION))
#	define COMPILER_TYPE COMPILER_SGI
#	define COMPILER_VERSION _COMPILER_VERSION
#elif (defined(__MRC__) || defined(MPW_C) || defined(MPW_CPLUS))
#	define COMPILER_TYPE COMPILER_MPW
#	if defined(__MRC__)
#		define COMPILER_VERSION __MRC__
#	else
#		define COMPILER_VERSION 0
#	endif
#elif defined(__MWERKS__)
#	define COMPILER_TYPE COMPILER_MWERKS
#	define COMPILER_VERSION __MWERKS__
#elif defined(__CC_NORCROFT)
#	define COMPILER_TYPE COMPILER_NORCROFT
#	define COMPILER_VERSION 0 // __ARMCC_VERSION is a float
#elif defined(_SCO_DS)
#	define COMPILER_TYPE COMPILER_SCO
#	define COMPILER_VERSION 0
#elif (defined(__SUNPRO_C) || defined(__SUNPRO_CC))
#	define COMPILER_TYPE COMPILER_SUNPRO
#	if defined(__SUNPRO_C)
#		define COMPILER_VERSION __SUNPRO_C
#	elif defined(__SUNPRO_CC)
#		define COMPILER_VERSION __SUNPRO_CC
#	else
#		define COMPILER_VERSION 0
#	endif
#elif defined(__TenDRA__)
#	define COMPILER_TYPE COMPILER_TENDRA
#	define COMPILER_VERSION 0
#elif defined(__USLC__)
#	define COMPILER_TYPE COMPILER_USLC
#	if defined(__SCO_VERSION__)
#		define COMPILER_VERSION __SCO_VERSION__
#	else
#		define COMPILER_VERSION 0
#	endif
#elif (defined(__xlC__) || (defined(__IBMC__) || defined(__IBMCPP__)) || (defined(_AIX) && !defined(__GNUC__)))
#	define COMPILER_TYPE COMPILER_XLC
#	if defined(__xlC__)
#		define COMPILER_VERSION __xlC__
#	else
#		define COMPILER_VERSION 0
#	endif
#else
#	error This compiler is not supported
#endif

#if COMPILER_TYPE == COMPILER_MSVC
#   if COMPILER_VERSION >= 1200
#       define inline __forceinline
#   endif
#	define __func__ __FUNCTION__
#elif COMPILER_TYPE == COMPILER_MINGW
#	if !defined(inline)
#		define inline __inline
#	endif
#elif COMPILER_TYPE == COMPILER_GNUC
#   if !__GNUC_STDC_INLINE__
#       define inline extern inline
#   endif
#elif __STDC_VERSION__ < 199901L
#   if !defined(inline)
#	    define inline static
#   endif
#endif

////////////////////////////////////////////////////////////
/// Identify the operating system
////////////////////////////////////////////////////////////
#define PLATFORM_UNKNOWN			0x00
#define PLATFORM_LINUX				0x01
#define PLATFORM_FREE_BSD			0x02
#define PLATFORM_NET_BSD			0x03
#define PLATFORM_OPEN_BSD			0x04
#define PLATFORM_BSDI				0x05
#define PLATFORM_DRAGONFLY			0x06
#define PLATFORM_WINDOWS_NT			0x07
#define PLATFORM_WINDOWS_CE			0x08
#define PLATFORM_MSDOS				0x09
#define PLATFORM_MACINTOSH			0x0A
#define PLATFORM_MACOSX				0x0B
#define PLATFORM_HPUX				0x0C
#define PLATFORM_SOLARIS			0x0D
#define PLATFORM_SUNOS				0x0E
#define PLATFORM_AMIGAOS			0x0F
#define PLATFORM_HAIKU				0x10
#define PLATFORM_TRU64				0x11
#define PLATFORM_AIX				0x12
#define PLATFORM_IRIX				0x13
#define PLATFORM_HURD				0x14
#define PLATFORM_CRAY				0x15
#define PLATFORM_DGUX				0x16
#define PLATFORM_OS2				0x17
#define PLATFORM_PYRAMID			0x18
#define PLATFORM_QNX				0x19
#define PLATFORM_SCO				0x1A
#define PLATFORM_SEQUENT			0x1B
#define PLATFORM_SINIX				0x1C
#define PLATFORM_ULTRIX				0x1D
#define PLATFORM_CYGWIN				0x1E
#define PLATFORM_SYMBIAN			0x1F
#define PLATFORM_ANDROID			0x20
#define PLATFORM_IOS				0x21
#define PLATFORM_NACL				0x22
#define PLATFORM_VMS				0x23
#define PLATFORM_UNKNOWN_UNIX		0x24
#define PLATFORM_VXWORKS			0x25

#if (defined(linux) || defined(__linux) || defined(__linux__) || defined(__TOS_LINUX__))
#	define PLATFORM_TYPE PLATFORM_LINUX
#	define PLATFORM_FAMILY_UNIX
#elif defined(__FreeBSD__)
#	define PLATFORM_TYPE PLATFORM_FREE_BSD
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif defined(__NetBSD__)
#	define PLATFORM_TYPE PLATFORM_NET_BSD
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif defined(__OpenBSD__)
#	define PLATFORM_TYPE PLATFORM_OPEN_BSD
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif (defined(bsdi) || defined(__bsdi__))
#	define PLATFORM_TYPE PLATFORM_BSDI
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif defined(__DragonFly__)
#	define PLATFORM_TYPE PLATFORM_DRAGONFLY
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif (defined(_WIN32) || defined(__WIN32__) || defined(_WIN64))
#	define PLATFORM_TYPE PLATFORM_WINDOWS_NT
#	define PLATFORM_FAMILY_WINDOWS
#elif defined(_WIN32_WCE)
#	define PLATFORM_TYPE PLATFORM_WINDOWS_CE
#	define PLATFORM_FAMILY_WINDOWS
#elif (defined(MSDOS) || defined(__MSDOS__) || defined(_MSDOS) || defined(__DOS__))
#	define PLATFORM_TYPE PLATFORM_MSDOS
#	define PLATFORM_FAMILY_WINDOWS
#elif (defined(__MACOS__) || defined(macintosh) || defined(Macintosh) || defined(__TOS_MACOS__))
#	define PLATFORM_TYPE PLATFORM_MACINTOSH
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif (defined(__APPLE__) && defined(__MACH__)) || defined(__MACOSX__)
#	define PLATFORM_TYPE PLATFORM_MACOSX
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif (defined(hpux) || defined(_hpux) || defined(__hpux) || defined(_HPUX_SOURCE))
#	define PLATFORM_TYPE PLATFORM_HPUX
#	define PLATFORM_FAMILY_UNIX
#elif (defined(sun) || defined(__sun) || defined(__sun__))
#	if defined(__SVR4) || defined(__svr4__)
#		define PLATFORM_TYPE PLATFORM_SOLARIS
#	else
#		define PLATFORM_TYPE PLATFORM_SUNOS
#	endif
#	define PLATFORM_FAMILY_UNIX
#elif (defined(__amigaos__) || defined(AMIGA))
#	define PLATFORM_TYPE PLATFORM_AMIGAOS
#	if (COMPIPLER_TYPE == COMPILER_GCC)
#		define PLATFORM_FAMILY_UNIX
#	endif
#elif (defined(__HAIKU__) || defined(__BEOS__))
#	define PLATFORM_TYPE PLATFORM_HAIKU
#	define  PLATFORM_FAMILY_UNIX
#elif (defined(__digital__) || defined(__osf__) || defined(__osf) || (COMPILER_TYPE == COMPILER_DECC))
#	define PLATFORM_TYPE PLATFORM_TRU64
#	define PLATFORM_FAMILY_UNIX
#elif (defined(_AIX) || defined(__TOS_AIX__) || (COMPILER_TYPE == COMPILER_XLC))
#	define PLATFORM_TYPE PLATFORM_AIX
#	define PLATFORM_FAMILY_UNIX
#elif (defined(sgi) || defined(__sgi) || defined(mips) || defined(_SGI_SOURCE))
#	define PLATFORM_TYPE PLATFORM_IRIX
#	define PLATFORM_FAMILY_UNIX
#elif defined(__GNU__)
#	define PLATFORM_TYPE PLATFORM_HURD
#	define PLATFORM_FAMILY_UNIX
#elif (defined(_UNICOS) || defined(_CRAY))
#	define PLATFORM_TYPE PLATFORM_CRAY
#	define PLATFORM_FAMILY_UNIX
#elif (defined(DGUX) || defined(__DGUX__) || defined(__dgux__))
#	define PLATFORM_TYPE PLATFORM_DGUX
#	define PLATFORM_FAMILY_UNIX
#elif (defined(OS2) || defined(_OS2) || defined(__OS2__) || defined(__TOS_OS2__))
#	define PLATFORM_TYPE PLATFORM_OS2
#	define PLATFORM_FAMILY_UNIX
#elif defined(pyr)
#	define PLATFORM_TYPE PLATFORM_PYRAMID
#	define PLATFORM_FAMILY_UNIX
#elif (defined(__QNX__) || defined(__QNXNTO__))
#	define PLATFORM_TYPE PLATFORM_QNX
#	define PLATFORM_FAMILY_UNIX
#elif (defined(M_I386) || defined(M_XENIX) || defined(_SCO_C_DIALECT) || (COMPILER_TYPE == COMPILER_SCO))
#	define PLATFORM_TYPE PLATFORM_SCO
#elif (defined(_SEQUENT_) || defined(sequent))
#	define PLATFORM_TYPE PLATFORM_SEQUENT
#elfi defined(sinix)
#	define PLATFORM_TYPE PLATFORM_SINIX
#elif (defined(ultrix) || defined(__ultrix) || defined(__ultrix__))
#	define PLATFORM_TYPE PLATFORM_ULTRIX
#	define PLATFORM_FAMILY_UNIX
#elif defined(__CYGWIN__)
#	define PLATFORM_TYPE PLATFORM_CYGWIN
#	define PLATFORM_FAMILY_UNIX
#elif (defined(VMS) || defined(__VMS))
#	define PLATFORM_TYPE PLATFORM_VMS
#	define PLATFORM_FAMILY_VMS
#elif defined(__SYMBIAN32__)
#	define PLATFORM_TYPE PLATFORM_SYMBIAN
#	define PLATFORM_FAMILY_UNIX
#elif defined(__ANDROID__)
#	define PLATFORM_TYPE PLATFORM_ANDROID
#	define PLATFORM_FAMILY_UNIX
#elif defined(__APPLE_CC__)
#	if __ENVIRONMENT_IPHONE_OS_VERSION_MIN_REQUIRED__ >= 40000 || __IPHONE_OS_VERSION_MIN_REQUIRED >= 40000
#		define PLATFORM_TYPE PLATFORM_IOS
#	endif
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif defined(__native_client__)
#	define PLATFORM_TYPE PLATFORM_NACL
#elif defined(unix) || defined(__unix) || defined(__unix__)
#	define PLATFORM_TYPE PLATFORM_UNKNOWN_UNIX
#	define PLATFORM_FAMILY_UNIX
#elif defined(USING_VXWORKS_PLATFORM) // explicit declaration
#	define PLATFORM_TYPE PLATFORM_VXWORKS
#	define PLATFORM_FAMILY_UNIX
#else
#	error This operating system is not supported
#endif

#if !defined(PLATFORM_FAMILY_BSD) && (defined(_BSD_SOURCE) || defined(_SYSTYPE_BSD))
#	define PLATFORM_FAMILY_BSD
#endif

#if defined(__sysv__) || defined(__SVR4) || defined(__svr4__) || defined(_SVR4_SOURCE) || defined(_SYSTYPE_SVR4)
#	define LATFORM_FAMILY_SVR4
#endif

#if defined(UWIN)
#	define PLATFORM_FAMILY_UWIN
#endif

#if defined(_WINDU_SOURCE)
#	define PLATFORM_FAMILY_WINDU
#endif

////////////////////////////////////////////////////////////
/// Identify the architecture type, family and endianess
////////////////////////////////////////////////////////////
#define ARCH_UNKNOWN				0x00
#define ARCH_ALPHA					0x01
#define ARCH_IA32					0x02
#define ARCH_IA64					0x03
#define ARCH_MIPS					0x04
#define ARCH_HPPA					0x05
#define ARCH_PPC					0x06
#define ARCH_POWER					0x07
#define ARCH_SPARC					0x08
#define ARCH_AMD64					0x09
#define ARCH_ARM					0x0A
#define ARCH_M68K					0x0B
#define ARCH_S390					0x0C
#define ARCH_SH						0x0D
#define ARCH_NIOS2					0x0E

#define ARCHITECTURE_32				0x00
#define ARCHITECTURE_64				0x01

#define ENDIAN_UNKNOWN				0x00
#define ENDIAN_LITTLE				0x01
#define ENDIAN_BIG					0x02
#define ENDIAN_BIG_WORD				0x03 //< Middle-endian, Honeywell 316 style
#define ENDIAN_LITTLE_WORD			0x04 //< Middle-endian, PDP-11 style
#define ENDIAN_UNCHECKED			0xFF

#if defined(alpha) || defined(__ALPHA) || defined(__alpha) || defined(__alpha__) || defined(_M_ALPHA)
#	define ARCH_TYPE ARCH_ALPHA
#	define ARCH_FAMILY ARCHITECTURE_64
#	define ENDIAN_TYPE ENDIAN_LITTLE
#elif defined(__386__) || defined(i386) || defined(__i386) || defined(__i386__) || defined(__X86) || defined(_M_IX86)
#	define ARCH_TYPE ARCH_IA32
#	define ARCH_FAMILY ARCHITECTURE_32
#	define ENDIAN_TYPE ENDIAN_LITTLE
#elif defined(_IA64) || defined(__IA64__) || defined(__ia64__) || defined(__ia64) || defined(_M_IA64)
#	define ARCH_TYPE ARCH_IA64
#	define ARCH_FAMILY ARCHITECTURE_64
#	if defined(hpux) || defined(_hpux)
#		define ENDIAN_TYPE ENDIAN_BIG
#	else
#		define ENDIAN_TYPE ENDIAN_LITTLE
#	endif
#elif defined(__x86_64__) || defined(_M_X64) || defined(_M_AMD64) || defined(__amd64)
#	define ARCH_TYPE ARCH_AMD64
#	define ARCH_FAMILY ARCHITECTURE_64
#	define ENDIAN_TYPE ENDIAN_LITTLE
#elif defined(__mips__) || defined(__mips) || defined(__MIPS__) || defined(_M_MRX000)
#	define ARCH_TYPE ARCH_MIPS
#	define ARCH_FAMILY ARCHITECTURE_32
#	if defined(_MIPSEB) || defined(__MIPSEB) || defined(__MIPSEB__)
#		define ENDIAN_TYPE ENDIAN_BIG
#	elif  defined(_MIPSEL) || defined(__MIPSEL) || defined(__MIPSEL__)
#		define ENDIAN_TYPE ENDIAN_LITTLE
#	endif
#elif defined(__hppa) || defined(__hppa__)
#	define ARCH_TYPE ARCH_HPPA
#	define ARCH_FAMILY ARCHITECTURE_32
#	define ENDIAN_TYPE ENDIAN_BIG
#elif defined(__PPC) || defined(__POWERPC__) || defined(__powerpc) || defined(__powerpc64__) || defined(__PPC__) || \
      defined(__powerpc__) || defined(__ppc__) || defined(__ppc) || defined(_ARCH_PPC) || defined(_M_PPC)
#	define ARCH_TYPE ARCH_PPC
#	define ENDIAN_TYPE ENDIAN_BIG
#	if defined(__powerpc64__)
#		define ARCH_FAMILY ARCHITECTURE_64
#	else
#		define ARCH_FAMILY ARCHITECTURE_32
#	endif
#elif defined(_POWER) || defined(_ARCH_PWR) || defined(_ARCH_PWR2) || defined(_ARCH_PWR3) || \
      defined(_ARCH_PWR4) || defined(__THW_RS6000)
#	define ARCH_TYPE ARCH_POWER
#	define ENDIAN_TYPE ENDIAN_BIG
#	if defined(_ARCH_PPC64)
#		define ARCH_FAMILY ARCHITECTURE_64
#	elif defined(_ARCH_COM) && defined(_ARCH_PPC)
#		define ARCH_FAMILY ARCHITECTURE_32
#	endif
#elif defined(__sparc__) || defined(__sparc) || defined(sparc)
#	define ARCH_TYPE ARCH_SPARC
#	define ENDIAN_TYPE ENDIAN_BIG
#	if defined(__arch64__) || defined(__sparcv9) || defined(__sparc_v9__)
#		define ARCH_FAMILY ARCHITECTURE_64
#	else
#		define ARCH_FAMILY ARCHITECTURE_32
#	endif
#elif defined(__arm__) || defined(__arm) || defined(ARM) || defined(_ARM) || defined(_ARM_) || defined(__ARM__) || defined(_M_ARM)
#	define ARCH_TYPE ARCH_ARM
#	if defined(__ARMEB__)
#		define ENDIAN_TYPE ENDIAN_BIG
#	else
#		define ENDIAN_TYPE ENDIAN_LITTLE
#	endif
#	define ARCH_FAMILY ARCHITECTURE_32
#elif defined(__m68k__) || defined(mc68000) || defined(_M_M68K)
#	define ARCH_TYPE ARCH_M68K
#	define ENDIAN_TYPE ENDIAN_BIG
#	define ARCH_FAMILY ARCHITECTURE_32
#elif defined(__s390__) || defined(__s390x__)
#	define ARCH_TYPE ARCH_S390
#	define ENDIAN_TYPE ENDIAN_BIG
#	define ARCH_FAMILY ARCHITECTURE_64
#elif defined(__sh__) || defined(__sh) || defined(SHx) || defined(_SHX_)
#	define ARCH_TYPE ARCH_SH
#	if defined(__LITTLE_ENDIAN__) || (PLATFORM_TYPE == PLATFORM_WINDOWS_CE)
#		define ENDIAN_TYPE ENDIAN_LITTLE
#	elif defined(__BIG_ENDIAN__)
#		define ENDIAN_TYPE ENDIAN_BIG
#	endif
#	define ARCH_FAMILY ARCHITECTURE_32
#elif defined(nios2) || defined(__nios2) || defined(__nios2__)
#	define ARCH_TYPE ARCH_NIOS2
#	if defined(__nios2_little_endian) || defined(nios2_little_endian) || defined(__nios2_little_endian__)
#		define ENDIAN_TYPE ENDIAN_LITTLE
#	else
#		define ENDIAN_TYPE ENDIAN_BIG
#	endif
#	define ARCH_FAMILY ARCHITECTURE_32
#endif

#if !defined(ENDIAN_TYPE)
#	if defined(__THUMBEB__) || defined(__AARCH64EB__)
#		define ENDIAN_TYPE ENDIAN_BIG
#	elif defined(__THUMBEL__) || defined(__AARCH64EL__) || defined(vax) || \
		 defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || \
		 defined(_WIN32_WCE) || defined(__NT__)
#		define ENDIAN_TYPE ENDIAN_LITTLE
#	elif defined(PLATFORM_FAMILY_UNIX) || defined(PLATFORM_FAMILY_BSD) || COMPIER_TYPE == COMPILER_GNUC
#		include <sys/param.h>
#		if defined(__BYTE_ORDER)
#			if __BYTE_ORDER == __BIG_ENDIAN
#				define ENDIAN_TYPE ENDIAN_BIG
#			elif __BYTE_ORDER == __LITTLE_ENDIAN
#				define ENDIAN_TYPE ENDIAN_LITTLE
#			elif __BYTE_ORDER == __PDP_ENDIAN
#				define ENDIAN_TYPE ENDIAN_LITTLE_WORD
#			else
#				define ENDIAN_TYPE ENDIAN_UNKNOWN
#			endif
#		else
#			define ENDIAN_TYPE ENDIAN_UNKNOWN
#		endif
#	endif
#endif

#if ENDIAN_TYPE == ENDIAN_UNKNOWN
#	error This endianess is not supported
#endif

#if !(defined(ARCH_TYPE) && ARCH_TYPE != ARCH_UNKNOWN && defined(ARCH_FAMILY))
#	error Unknown hardware architecture
#endif

////////////////////////////////////////////////////////////
/// Thread-safety of local static initialization
////////////////////////////////////////////////////////////
#if (__cplusplus >= 201103L) || (__GNUC__ >= 4) || defined(__clang__)
#	ifndef LOCAL_STATIC_THREADSAFE
#		define LOCAL_STATIC_THREADSAFE
#	endif
#endif

////////////////////////////////////////////////////////////
/// Identify supported assembly language
////////////////////////////////////////////////////////////
#define ASM_SYNTAX_UNKNOWN			0x00
#define ASM_SYNTAX_INTEL			0x01
#define ASM_SYNTAX_ATANDT			0x02

#if (ARCH_TYPE == ARCH_IA32) || (ARCH_TYPE == ARCH_AMD64)
#	if (COMPILER_TYPE == COMPILER_GNUC)
		// GCC assembler
#	elif (COMPILER_TYPE == COMPILER_SUNPRO)
		// GCC-compatible assembler
#	endif
#elif (COMPILER_TYPE == COMPILER_GNUC) && (COMPILER_VERSION >= 401)
	// GCC 4.0.1 intrinsic
#else
#	define ASM_SYNTAX_TYPE ASM_SYNTAX_UNKNOWN
#endif

////////////////////////////////////////////////////////////
/// Identify standard ANSI C language version
///////////////////////////////////////////////////////////
#define LANGUAGE_VERSION_UNKNOWN	0L
#define LANGUAGE_VERSION_C89		198900L
#define LANGUAGE_VERSION_C90		199000L
#define LANGUAGE_VERSION_C94		199409L
#define LANGUAGE_VERSION_C99		199901L
#define LANGUAGE_VERSION_C11		201112L
#define LANGUAGE_VERSION_CPP98		199711L
#define LANGUAGE_VERSION_CPP11		201103L
#define LANGUAGE_VERSION_CPP14		201402L
#define LANGUAGE_VERSION_CPP17		2017FFL
#define LANGUAGE_VERSION_CPPCLI		200406L
#define LANGUAGE_VERSION_ECPP		1L

#if defined(__STDC__)
#	if defined(__STDC_VERSION__)
#		if (__STDC_VERSION__ - 0 > 1)
#			if (__STDC_VERSION__ == LANGUAGE_VERSION_C94)
#				define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C94
#			elif (__STDC_VERSION__ == LANGUAGE_VERSION_C99)
#				define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C99
#			elif (__STDC_VERSION__ == LANGUAGE_VERSION_C11)
#				define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C11
#			else
#				define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_UNKNOWN
#			endif
#		else
#			if (COMPILER_TYPE == COMPILER_SUNPRO)
#				if (COMPILER_VERSION >= 0x420)
#					define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C94
#				else
#					define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C90
#				endif
#			else
#				define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_UNKNOWN
#			endif
#		endif
#	else
#		define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C89
#	endif
#elif (defined(_MSC_EXTENSIONS) || (COMPILER_TYPE == COMPILER_BORLAND))
#	define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C89
#elif defined(__cplusplus)
#	if (__cplusplus == 1) || (__cplusplus == LANGUAGE_VERSION_CPP98)
#		define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_CPP98
#	elif (__cplusplus == LANGUAGE_VERSION_CPP11)
#		define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_CPP11
#	elif (__cplusplus == LANGUAGE_VERSION_CPP14)
#		define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_CPP14
#	elif (__cplusplus > LANGUAGE_VERSION_CPP14)
#		define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_CPP17
#	else
#		define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_UNKNOWN
#	endif
#elif defined(__cplusplus_cli)
#	define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_CPPCLI
#elif defined(__embedded_cplusplus)
#	define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_ECPP
#else
#	define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_UNKNOWN
#endif

#if (LANGUAGE_VERSION_TYPE == LANGUAGE_VERSION_UNKNOWN)
#	ifndef const
#		define const
#	endif
#	ifndef volatile
#		define volatile
#	endif
#endif

////////////////////////////////////////////////////////////
/// Function call attribute declarations
////////////////////////////////////////////////////////////
#if COMPILER_TYPE == COMPILER_MSVC
#	ifndef attr_cdecl
#		define attr_cdecl __cdecl
#	endif
#	ifndef attr_stdcall
#		define attr_stdcall __stdcall
#	endif
#	ifndef attr_fastcall
#		define attr_fastcall __fastcall
#	endif
#elif COMPILER_TYPE == COMPILER_GNUC
#	ifndef attr_cdecl
#		define attr_cdecl __attribute__((cdecl))
#	endif
#	ifndef attr_stdcall
#		define attr_stdcall __attribute__((stdcall))
#	endif
#	ifndef attr_fastcall
#		define attr_fastcall __attribute__((fastcall))
#	endif
#else
#	ifndef attr_cdecl
#		define attr_cdecl
#	endif
#	ifndef attr_stdcall
#		define attr_stdcall
#	endif
#	ifndef attr_fastcall
#		define attr_fastcall
#	endif
#endif

#endif // PLATFORM_H
