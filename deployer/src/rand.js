/**
 *	Randomizer
 *
 * @author Vicente Ferrer
 * @lastmodify 120214 by Vicente
 * @brief A module gives suport for random generation.
 */

/**
 * Private
 */

var default_rand_str_length = 10;	///< Default random string size

/**
 * @brief Generates a random string of specified size.
 * @return {String} Resulting string generated.
 */
function randomize_string(length) {
	var strlen	= length || default_rand_str_length;
	var charset = '0123456789abcdef';
	var result  = new Array();
	
	for (var i = 0; i < strlen; ++i) {
		result.push(charset[Math.floor(Math.random() * charset.length)]);
	}

	result.splice(strlen / 2, 0, ['-']);
	
	return result.join('');
}

/**
 * Public
 */
exports.str		= randomize_string;
