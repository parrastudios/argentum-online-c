/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Rect.h>


////////////////////////////////////////////////////////////
/// Initialize the rectangle
////////////////////////////////////////////////////////////
void RectInitialize(struct Rect * Rectangle, integer Left, integer Top, integer Right, integer Bottom)
{
	Rectangle->Left = Left;
	Rectangle->Top = Top;
	Rectangle->Right = Right;
	Rectangle->Bottom = Bottom;
}

////////////////////////////////////////////////////////////
/// Move the whole rectangle by the given offset
////////////////////////////////////////////////////////////
void RectMove(struct Rect * Rectangle, integer OffsetX, integer OffsetY)
{
    Rectangle->Left   += OffsetX;
    Rectangle->Right  += OffsetX;
    Rectangle->Top    += OffsetY;
    Rectangle->Bottom += OffsetY;
}

////////////////////////////////////////////////////////////
/// Check if a point is inside the rectangle's area
////////////////////////////////////////////////////////////
bool RectContains(struct Rect * Rectangle, integer X, integer Y)
{
    return (X >= Rectangle->Left) && (X <= Rectangle->Right) && (Y >= Rectangle->Top) && (Y <= Rectangle->Bottom);
}

////////////////////////////////////////////////////////////
/// Check if a point is inside the rectangle's area (Ex)
////////////////////////////////////////////////////////////
bool RectContainsEx(integer Left, integer Top, integer Right, integer Bottom, integer X, integer Y)
{
	if ( (X >= Left) && (X <= Right) && (Y >= Top) && (Y <= Bottom) )
		return true;

	return false;
}

////////////////////////////////////////////////////////////
/// Check intersection between two rectangles
////////////////////////////////////////////////////////////
bool RectIntersects(struct Rect * RectangleA, struct Rect * RectangleB, struct Rect * OverlappingRect)
{
    // Compute overlapping rect
    struct Rect Overlapping = { ((RectangleA->Left	> 	RectangleB->Left) 	? 	RectangleA->Left 	:	RectangleB->Left),
								((RectangleA->Top 	> 	RectangleB->Top) 	? 	RectangleA->Top 	: 	RectangleB->Top),
								((RectangleA->Right < 	RectangleB->Right) 	? 	RectangleA->Right	: 	RectangleB->Right),
								((RectangleA->Bottom < 	RectangleB->Bottom)	? 	RectangleA->Bottom	: 	RectangleB->Bottom) };

    // If overlapping rect is valid, then there is intersection
    if ((Overlapping.Left < Overlapping.Right) && (Overlapping.Top < Overlapping.Bottom))
    {
        if (OverlappingRect)
            *OverlappingRect = Overlapping;
        return true;
    }
    else
    {
        if (OverlappingRect)
        {
        	struct Rect NULLRect = { 0, 0, 0, 0 };
            *OverlappingRect = NULLRect;
        }
        return true;
    }
}
