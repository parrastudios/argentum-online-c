/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GAME_TICK_H
#define GAME_TICK_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/List.h>
#include <System/TimerManager.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define TICK_TIME_STAMP				0x05

#define TICK_TIMER_INVALID			0xFFFFFFFF

#define TICK_TASK_PRIORITY_LOW		0x00
#define TICK_TASK_PRIORITY_MEDIUM	0x01
#define TICK_TASK_PRIORITY_HIGH		0x02

#define TICK_TASK_INITIALIZER		0x00
#define TICK_TASK_UPDATER			0x01
#define TICK_TASK_DESTROYER			0x02

#define TICK_TASK_SLEEP				0x00
#define TICK_TASK_REALTIME			0x01

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef void * TickData;
typedef void * TickFuncRet;

typedef struct
{
	TickData			Data;				//< Data value (in/out) of the last exectued function
	TickFuncRet			RetValue;			//< Return value of last exectued function
} TickFuncContext;

typedef TickFuncRet (*TickFunc)(TickData);

typedef struct
{
	TickFuncContext		Context;			//< Context context of the function task
	TickFunc			Function;			//< Function to be executed
} TickTaskEvent;

#define TickTaskEventCall(Event) do { Event.Context.RetValue = Event.Function(Event.Context.Data); } while (0)

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct TickTaskType
{
	uint32				Priority;			//< Priority of current task (medium by default)
	TickTaskEvent		Initializer;		//< Function will be executed when task starts
	TickTaskEvent		Updater;			//< Function will be executed when interval expires
	TickTaskEvent		Destroyer;			//< Function will be executed when task deads
	uint32				Interval;			//< Interval in ticks of the task (sleep, realtime, or higher)
	uint32				Current;			//< Current tick of the task (remaining ticks to execution)
};

struct TickManagerType
{
	uint32				TimerId;			//< Id associated to the timer manager
	float				Interval;			//< Interval in milliseconds thread will be launched 
	List				Tasks;				//< List of tasks to be executed synchronously
	bool				Pause;				//< Stop all tasks of tick manager
	CallbackType		TasksUpdaterImpl;	//< Callback holder for updaters
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Returns the default tick manager
////////////////////////////////////////////////////////////
struct TickManagerType * TickManagerDefault();

////////////////////////////////////////////////////////////
/// Register callbacks of a task
////////////////////////////////////////////////////////////
void TickTaskRegister(struct TickTaskType * TickTask, TickTaskEvent Init, TickTaskEvent Update, TickTaskEvent Destroy);

////////////////////////////////////////////////////////////
/// Initialize a tick task
////////////////////////////////////////////////////////////
void TickTaskInitialize(struct TickTaskType * TickTask, uint32 Priority, uint32 Interval);

////////////////////////////////////////////////////////////
/// Get tick task context
////////////////////////////////////////////////////////////
TickFuncContext * TickTaskGetContext(struct TickTaskType * TickTask, uint32 EventType);

////////////////////////////////////////////////////////////
/// Initialize a tick manager
////////////////////////////////////////////////////////////
void TickManagerInitialize(struct TickManagerType * TickManager, float Interval);

////////////////////////////////////////////////////////////
/// Add task to a tick manager
////////////////////////////////////////////////////////////
void TickManagerAddTask(struct TickManagerType * TickManager, TickTaskEvent Init, TickTaskEvent Update, TickTaskEvent Destroy, uint32 Priority, uint32 Interval);

////////////////////////////////////////////////////////////
/// Clear all tasks of a tick manager
////////////////////////////////////////////////////////////
void TickManagerClearTask(struct TickManagerType * TickManager);

////////////////////////////////////////////////////////////
/// Start a tick manager
////////////////////////////////////////////////////////////
void TickManagerStart(struct TickManagerType * TickManager);

////////////////////////////////////////////////////////////
/// Stop a tick manager
////////////////////////////////////////////////////////////
void TickManagerStop(struct TickManagerType * TickManager);

////////////////////////////////////////////////////////////
/// Restart a tick manager
////////////////////////////////////////////////////////////
void TickManagerRestart(struct TickManagerType * TickManager);

////////////////////////////////////////////////////////////
/// Update all tasks synchronously of a tick manager
////////////////////////////////////////////////////////////
void TickManagerUpdate(struct TickManagerType * TickManager);

////////////////////////////////////////////////////////////
/// Destroy a tick manager
////////////////////////////////////////////////////////////
void TickManagerDestroy(struct TickManagerType * TickManager);

#endif // GAME_TICK_H
