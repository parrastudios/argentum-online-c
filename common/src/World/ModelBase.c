/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/ModelBase.h>

////////////////////////////////////////////////////////////
// Member prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a base component of a model
////////////////////////////////////////////////////////////
struct EntityType * ModelBaseCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a model base component
////////////////////////////////////////////////////////////
void ModelBaseDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the model base implementation attributes
////////////////////////////////////////////////////////////
const void * ModelComponentBaseAttr()
{
	static const struct ModelComponentBaseType ModelComponentBaseAttr = {
		TypeInit(TODO, NULL)
	};

	return (const void *)&ModelComponentBaseAttr;
}

////////////////////////////////////////////////////////////
/// Return the model base implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * ModelComponentBaseImpl()
{
	static const struct ComponentImplType ModelComponentBaseTypeImpl = {
		ExprInit(Base, TypeInit(Create, &ModelBaseCreate), TypeInit(Destroy, &ModelBaseDestroy)),
		TypeInit(Type, COMPONENT_TYPE_BASE),
		UnionInit(Attributes, Derived, &ModelComponentBaseAttr)
	};

	return &ModelComponentBaseTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a base component of a model
////////////////////////////////////////////////////////////
struct EntityType * ModelBaseCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct ModelComponentBaseData * BaseData = ComponentDataDerived(Base, struct ModelComponentBaseData);

		// ...

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set base model name
////////////////////////////////////////////////////////////
void ModelBaseSetName(struct EntityType * Entity, char * Name)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct ModelComponentBaseData * BaseData = ComponentDataDerived(Base, struct ModelComponentBaseData);

		// ...

	}
}

////////////////////////////////////////////////////////////
/// Destroy a model base component
////////////////////////////////////////////////////////////
void ModelBaseDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct ModelComponentBaseData * BaseData = ComponentDataDerived(Base, struct ModelComponentBaseData);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the model base derived data size
////////////////////////////////////////////////////////////
uinteger ModelComponentBaseSize()
{
	return sizeof(struct ModelComponentBaseData);
}
