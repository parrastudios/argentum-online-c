/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <stdlib.h> // qsort, bsearch
#include <Config.h>
#include <Game/Console.h>
#include <Memory/General.h>
#include <DataBase/Account.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
bool	ConsoleRun;		///< Console switch
Thread	ConsoleThread;	///< Thread console
Mutex	ConsoleMutex;	///< Mutex console
bool	CmdListSorted;	///< Console command list array sorting value

////////////////////////////////////////////////////////////
// Shutdown command console
////////////////////////////////////////////////////////////
void Shutdown()
{
    MutexLock(&ConsoleMutex);

    ConsoleRun = false;

    MutexUnlock(&ConsoleMutex);
}

////////////////////////////////////////////////////////////
// Initialize command console
////////////////////////////////////////////////////////////
bool ConsoleInitialize()
{
    int32 i, CommandSize;
    char Section[0xFF];

    struct Dictionary * Data = ParserLoad(COMMAND_LIST_PATH);

    if (Data == NULL)
    {
        MessageError("ConsoleInitialize", "Error when the program tried to load the command list: %s", COMMAND_LIST_PATH);
        return false;
    }

    CommandSize = ParserGetInt(Data, "INIT:CmdSize", -1);

    // Load the command list
    for (i = 0; i < CommandSize; i++)
    {
        sprintf(Section, "CMDLST:%d", i);
        CommandList[i].Index = i;
        strcpy(CommandList[i].Data, ParserGetString(Data, Section, NULL));
    }

    ParserFreeDict(Data);

    // Set switch
    ConsoleRun = true;

    // Set sorted value
    CmdListSorted = false;

    // Initialize the mutex
    MutexInitialize(&ConsoleMutex);

    // Run the thread
    ThreadLaunch(&ConsoleThread, (FuncType)&ConsoleRoutine, NULL);

    return true;
}

////////////////////////////////////////////////////////////
// Console command helper comparator
////////////////////////////////////////////////////////////
static int32 ConsoleCommandCompare(const void * p1, const void * p2)
{
    if (p1 == NULL || p2 == NULL)
        return CONSOLE_ERROR;

    return strcmp( (const char*)p1, (const char*)p2 );
}

////////////////////////////////////////////////////////////
// Console command parser
////////////////////////////////////////////////////////////
int32 ConsoleParseCommand(char * BufferCommand)
{
    struct CommandData * Command;

    if (!CmdListSorted)
    {
        qsort(CommandList, CONSOLE_COMMAND_SIZE, sizeof(*CommandList), ConsoleCommandCompare);
        CmdListSorted = true;
    }

    Command = bsearch(BufferCommand, CommandList, CONSOLE_COMMAND_SIZE, sizeof(*CommandList), ConsoleCommandCompare);

    return Command ? Command->Index : CONSOLE_ERROR;
}

////////////////////////////////////////////////////////////
// Console command parser
////////////////////////////////////////////////////////////
void ConsoleParseArguments(struct CommandData * Command, char * Arguments)
{
    uint32 i, j;
    uint32 LastPos = 0;

    for (i = 0; i < CONSOLE_COMMAND_ARG_SIZE; i++)
    {
        for (j = LastPos; j < CONSOLE_BUFFER_SIZE; j++)
        {
            if ( (Arguments[j] == ':') || (Arguments[j] == NULLCHAR) )
            {
                if (LastPos == 0)
                {
                    MemoryCopy(Command->Arguments[i], &Arguments[0], j);
                    Command->Arguments[i][j] = NULLCHAR;
                }
                else
                {
                    MemoryCopy(Command->Arguments[i], &Arguments[LastPos], (j - LastPos));
                    Command->Arguments[i][j] = NULLCHAR;
                }

                LastPos = (j + 1);

                break;
            }
        }
    }
}

////////////////////////////////////////////////////////////
// Input buffer parser
////////////////////////////////////////////////////////////
int32 ConsoleParseBuffer(char * Buffer)
{
    char Command[CONSOLE_BUFFER_SIZE];
    int32 i;

    for (i = 0; i < CONSOLE_BUFFER_SIZE; i++)
    {
        if (Buffer[i] == NULLCHAR)
        {
            return ConsoleParseCommand(Buffer);
        }

        if (Buffer[i] == ':')
        {
            int32 CommandID;

            // Copy command
            MemoryCopy(Command, Buffer, i);

            // Set finally null char
            Command[i] = NULLCHAR;

            // Copy arguments
            MemoryCopy(&Buffer[0], &Buffer[i+1], strlen(Buffer - i));

            // Parse command
            CommandID = ConsoleParseCommand(Command);

            // Parse arguments
            ConsoleParseArguments(&CommandList[CommandID], Buffer);

            // Return ID
            return CommandID;
        }
    }

    return CONSOLE_ERROR;
}

////////////////////////////////////////////////////////////
// Command console method
////////////////////////////////////////////////////////////
void ConsoleRoutine()
{
    char Buffer[CONSOLE_BUFFER_SIZE];

    while (ConsoleRun)
    {
        printf("Server>");

#if PLATFORM_TYPE == PLATFORM_WINDOWS && COMPILER_TYPE == COMPILER_MSVC
        scanf_s("%s", Buffer, CONSOLE_BUFFER_SIZE);
#else
        scanf("%s", Buffer);
#endif

        switch (ConsoleParseBuffer(Buffer))
        {
            case CONSOLE_CMD_HELP :
            {
                printf("Command console help :\n");
                printf("     time	: show info about server time\n");
                printf("     credits	: show credits of server\n");
                printf("     exit	: close the application\n");

                break;
            }

            case CONSOLE_CMD_TIME :
            {
                printf("Server time (todo) : 00:00:00\n");

                break;
            }

            case CONSOLE_CMD_CREDITS :
            {
                printf("Copyright (C) 2009-2016 " APPLICATION_COMPANY "\n");
                printf(APPLICATION_NAME " - Version " APPLICATION_VERSION "\n");

                break;
            }

            case CONSOLE_CMD_TEST :
            {
                // Mock data
                struct DBAccountData Account;

                AccountStoredProcedureInitialize();

                if(DBAccountExist(&Account))
                {
                    printf("Exists\n");
                }

                AccountStoredProcedureRelease();
                break;
            }

            case CONSOLE_CMD_EXIT :
            {
                printf("Closing, wait...\n");
                AppSetRun(false);
                Shutdown();
                break;
            }

            case CONSOLE_ERROR :
            {
                MessageError("ConsoleRoutine", "Error when the program tried to manage the command: %s", Buffer);
                break;
            }

            default :
                break;
        }
    }
}

////////////////////////////////////////////////////////////
// Destroy command console
////////////////////////////////////////////////////////////
void ConsoleDestroy()
{
    Shutdown();

    ThreadWait(&ConsoleThread);

    MutexDestroy(&ConsoleMutex);
}
