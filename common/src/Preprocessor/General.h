/////////////////////////////////////////////////////////////////////////////
//	Preprocessor Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A cross-platform macro based library to give support
//	for preprocessor metaprogramming.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PREPROCESSOR_GENERAL_H
#define PREPROCESSOR_GENERAL_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Preprocessor/Arguments.h>
#include <Preprocessor/Bit.h>
#include <Preprocessor/Boolean.h>
#include <Preprocessor/Comma.h>
#include <Preprocessor/Comparison.h>
#include <Preprocessor/Complement.h>
#include <Preprocessor/Concatenation.h>
#include <Preprocessor/Detection.h>
#include <Preprocessor/Empty.h>
#include <Preprocessor/For.h>
#include <Preprocessor/If.h>
#include <Preprocessor/Recursion.h>
#include <Preprocessor/Repeat.h>
#include <Preprocessor/Serial.h>
#include <Preprocessor/Stringify.h>
#include <Preprocessor/Tuple.h>

#endif // PREPROCESSOR_GENERAL_H
