# Compiler flags...
CPP_COMPILER = g++
C_COMPILER = gcc

# Include paths...
Debug_Include_Path=
Release_Include_Path=
DebugXP_Include_Path=
ReleaseXP_Include_Path=

# Library paths...
Debug_Library_Path=
Release_Library_Path=
DebugXP_Library_Path=
ReleaseXP_Library_Path=

# Additional libraries...
Debug_Libraries=-Wl,--start-group -lwsock32 -lpdh -llibmysql  -Wl,--end-group
Release_Libraries=-Wl,--start-group -lwsock32 -lpdh -llibmysql  -Wl,--end-group
DebugXP_Libraries=-Wl,--start-group -lwsock32 -lpdh -llibmysql  -Wl,--end-group
ReleaseXP_Libraries=-Wl,--start-group -lwsock32 -lpdh -llibmysql  -Wl,--end-group

# Preprocessor definitions...
Debug_Preprocessor_Definitions=-D GCC_BUILD -D _DEBUG -D _CONSOLE -D _CRT_SECURE_NO_DEPRECATE -D _CRT_SECURE_NO_WARNINGS 
Release_Preprocessor_Definitions=-D GCC_BUILD -D NDEBUG -D _CONSOLE -D _CRT_SECURE_NO_DEPRECATE -D _CRT_SECURE_NO_WARNINGS 
DebugXP_Preprocessor_Definitions=-D GCC_BUILD -D _DEBUG -D _CONSOLE -D _CRT_SECURE_NO_DEPRECATE -D _CRT_SECURE_NO_WARNINGS 
ReleaseXP_Preprocessor_Definitions=-D GCC_BUILD -D NDEBUG -D _CONSOLE -D _CRT_SECURE_NO_DEPRECATE -D _CRT_SECURE_NO_WARNINGS 

# Implictly linked object files...
Debug_Implicitly_Linked_Objects=
Release_Implicitly_Linked_Objects=
DebugXP_Implicitly_Linked_Objects=
ReleaseXP_Implicitly_Linked_Objects=

# Compiler flags...
Debug_Compiler_Flags=-O0 -g 
Release_Compiler_Flags=-O2 
DebugXP_Compiler_Flags=-O0 -g 
ReleaseXP_Compiler_Flags=-O2 

# Builds all configurations for this project...
.PHONY: build_all_configurations
build_all_configurations: Debug Release DebugXP ReleaseXP 

# Builds the Debug configuration...
.PHONY: Debug
Debug: create_folders ../bin/gccDebug/../src/main.o ../bin/gccDebug/../src/Database/Account.o ../bin/gccDebug/../../common/src/Database/Connector.o ../bin/gccDebug/../../common/src/Database/Default.o ../bin/gccDebug/../src/Database/Npc.o ../bin/gccDebug/../src/Database/Player.o ../bin/gccDebug/../../common/src/Database/Query.o ../bin/gccDebug/../../common/src/System/CPUInfo.o ../bin/gccDebug/../../common/src/System/Dictionary.o ../bin/gccDebug/../../common/src/System/Error.o ../bin/gccDebug/../../common/src/System/MD5.o ../bin/gccDebug/../../common/src/System/Pack.o ../bin/gccDebug/../../common/src/System/Parser.o ../bin/gccDebug/../../common/src/System/Timer.o ../bin/gccDebug/../../common/src/System/TimerManager.o ../bin/gccDebug/../../common/src/System/Win32/Condition.o ../bin/gccDebug/../../common/src/System/Win32/IOHelper.o ../bin/gccDebug/../../common/src/System/Win32/Mutex.o ../bin/gccDebug/../../common/src/System/Win32/Platform.o ../bin/gccDebug/../../common/src/System/Win32/Thread.o ../bin/gccDebug/../../common/src/System/Win32/ThreadLocal.o ../bin/gccDebug/../../common/src/System/String/bstrlib.o ../bin/gccDebug/../../common/src/System/String/Lexer.o ../bin/gccDebug/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccDebug/../../common/src/System/DataTypes/Bitset.o ../bin/gccDebug/../../common/src/System/Memory/Allocator.o ../bin/gccDebug/../../common/src/System/Memory/Pool.o ../bin/gccDebug/../src/Network/Protocol.o ../bin/gccDebug/../../common/src/Network/Socket.o ../bin/gccDebug/../../common/src/Network/Win32/SocketHelper.o ../bin/gccDebug/../src/Game/Application.o ../bin/gccDebug/../src/Game/Character.o ../bin/gccDebug/../src/Game/Console.o ../bin/gccDebug/../../common/src/Game/RealTime.o ../bin/gccDebug/../src/Game/User.o ../bin/gccDebug/../src/Game/Weather.o ../bin/gccDebug/../../common/src/Math/General.o ../bin/gccDebug/../../common/src/Math/Geometry/Matrix.o ../bin/gccDebug/../../common/src/Math/Geometry/Plane.o ../bin/gccDebug/../../common/src/Math/Geometry/Quaternion.o ../bin/gccDebug/../../common/src/Math/Geometry/Rect.o ../bin/gccDebug/../../common/src/Math/Geometry/Vector.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Sphere.o 
	g++ ../bin/gccDebug/../src/main.o ../bin/gccDebug/../src/Database/Account.o ../bin/gccDebug/../../common/src/Database/Connector.o ../bin/gccDebug/../../common/src/Database/Default.o ../bin/gccDebug/../src/Database/Npc.o ../bin/gccDebug/../src/Database/Player.o ../bin/gccDebug/../../common/src/Database/Query.o ../bin/gccDebug/../../common/src/System/CPUInfo.o ../bin/gccDebug/../../common/src/System/Dictionary.o ../bin/gccDebug/../../common/src/System/Error.o ../bin/gccDebug/../../common/src/System/MD5.o ../bin/gccDebug/../../common/src/System/Pack.o ../bin/gccDebug/../../common/src/System/Parser.o ../bin/gccDebug/../../common/src/System/Timer.o ../bin/gccDebug/../../common/src/System/TimerManager.o ../bin/gccDebug/../../common/src/System/Win32/Condition.o ../bin/gccDebug/../../common/src/System/Win32/IOHelper.o ../bin/gccDebug/../../common/src/System/Win32/Mutex.o ../bin/gccDebug/../../common/src/System/Win32/Platform.o ../bin/gccDebug/../../common/src/System/Win32/Thread.o ../bin/gccDebug/../../common/src/System/Win32/ThreadLocal.o ../bin/gccDebug/../../common/src/System/String/bstrlib.o ../bin/gccDebug/../../common/src/System/String/Lexer.o ../bin/gccDebug/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccDebug/../../common/src/System/DataTypes/Bitset.o ../bin/gccDebug/../../common/src/System/Memory/Allocator.o ../bin/gccDebug/../../common/src/System/Memory/Pool.o ../bin/gccDebug/../src/Network/Protocol.o ../bin/gccDebug/../../common/src/Network/Socket.o ../bin/gccDebug/../../common/src/Network/Win32/SocketHelper.o ../bin/gccDebug/../src/Game/Application.o ../bin/gccDebug/../src/Game/Character.o ../bin/gccDebug/../src/Game/Console.o ../bin/gccDebug/../../common/src/Game/RealTime.o ../bin/gccDebug/../src/Game/User.o ../bin/gccDebug/../src/Game/Weather.o ../bin/gccDebug/../../common/src/Math/General.o ../bin/gccDebug/../../common/src/Math/Geometry/Matrix.o ../bin/gccDebug/../../common/src/Math/Geometry/Plane.o ../bin/gccDebug/../../common/src/Math/Geometry/Quaternion.o ../bin/gccDebug/../../common/src/Math/Geometry/Rect.o ../bin/gccDebug/../../common/src/Math/Geometry/Vector.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Sphere.o  $(Debug_Library_Path) $(Debug_Libraries) -Wl,-rpath,./ -o ../bin/gccDebug/Server.exe

# Compiles file ../src/main.c for the Debug configuration...
-include ../bin/gccDebug/../src/main.d
../bin/gccDebug/../src/main.o: ../src/main.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/main.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/main.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/main.c $(Debug_Include_Path) > ../bin/gccDebug/../src/main.d

# Compiles file ../src/Database/Account.c for the Debug configuration...
-include ../bin/gccDebug/../src/Database/Account.d
../bin/gccDebug/../src/Database/Account.o: ../src/Database/Account.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Database/Account.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Database/Account.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Database/Account.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Database/Account.d

# Compiles file ../../common/src/Database/Connector.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Database/Connector.d
../bin/gccDebug/../../common/src/Database/Connector.o: ../../common/src/Database/Connector.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Database/Connector.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Database/Connector.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Database/Connector.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Database/Connector.d

# Compiles file ../../common/src/Database/Default.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Database/Default.d
../bin/gccDebug/../../common/src/Database/Default.o: ../../common/src/Database/Default.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Database/Default.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Database/Default.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Database/Default.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Database/Default.d

# Compiles file ../src/Database/Npc.c for the Debug configuration...
-include ../bin/gccDebug/../src/Database/Npc.d
../bin/gccDebug/../src/Database/Npc.o: ../src/Database/Npc.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Database/Npc.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Database/Npc.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Database/Npc.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Database/Npc.d

# Compiles file ../src/Database/Player.c for the Debug configuration...
-include ../bin/gccDebug/../src/Database/Player.d
../bin/gccDebug/../src/Database/Player.o: ../src/Database/Player.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Database/Player.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Database/Player.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Database/Player.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Database/Player.d

# Compiles file ../../common/src/Database/Query.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Database/Query.d
../bin/gccDebug/../../common/src/Database/Query.o: ../../common/src/Database/Query.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Database/Query.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Database/Query.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Database/Query.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Database/Query.d

# Compiles file ../../common/src/System/CPUInfo.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/CPUInfo.d
../bin/gccDebug/../../common/src/System/CPUInfo.o: ../../common/src/System/CPUInfo.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/CPUInfo.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/CPUInfo.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/CPUInfo.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/CPUInfo.d

# Compiles file ../../common/src/System/Dictionary.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Dictionary.d
../bin/gccDebug/../../common/src/System/Dictionary.o: ../../common/src/System/Dictionary.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Dictionary.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Dictionary.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Dictionary.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Dictionary.d

# Compiles file ../../common/src/System/Error.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Error.d
../bin/gccDebug/../../common/src/System/Error.o: ../../common/src/System/Error.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Error.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Error.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Error.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Error.d

# Compiles file ../../common/src/System/MD5.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/MD5.d
../bin/gccDebug/../../common/src/System/MD5.o: ../../common/src/System/MD5.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/MD5.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/MD5.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/MD5.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/MD5.d

# Compiles file ../../common/src/System/Pack.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Pack.d
../bin/gccDebug/../../common/src/System/Pack.o: ../../common/src/System/Pack.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Pack.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Pack.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Pack.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Pack.d

# Compiles file ../../common/src/System/Parser.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Parser.d
../bin/gccDebug/../../common/src/System/Parser.o: ../../common/src/System/Parser.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Parser.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Parser.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Parser.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Parser.d

# Compiles file ../../common/src/System/Timer.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Timer.d
../bin/gccDebug/../../common/src/System/Timer.o: ../../common/src/System/Timer.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Timer.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Timer.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Timer.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Timer.d

# Compiles file ../../common/src/System/TimerManager.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/TimerManager.d
../bin/gccDebug/../../common/src/System/TimerManager.o: ../../common/src/System/TimerManager.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/TimerManager.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/TimerManager.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/TimerManager.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/TimerManager.d

# Compiles file ../../common/src/System/Win32/Condition.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Win32/Condition.d
../bin/gccDebug/../../common/src/System/Win32/Condition.o: ../../common/src/System/Win32/Condition.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Win32/Condition.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Win32/Condition.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Win32/Condition.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Win32/Condition.d

# Compiles file ../../common/src/System/Win32/IOHelper.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Win32/IOHelper.d
../bin/gccDebug/../../common/src/System/Win32/IOHelper.o: ../../common/src/System/Win32/IOHelper.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Win32/IOHelper.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Win32/IOHelper.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Win32/IOHelper.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Win32/IOHelper.d

# Compiles file ../../common/src/System/Win32/Mutex.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Win32/Mutex.d
../bin/gccDebug/../../common/src/System/Win32/Mutex.o: ../../common/src/System/Win32/Mutex.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Win32/Mutex.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Win32/Mutex.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Win32/Mutex.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Win32/Mutex.d

# Compiles file ../../common/src/System/Win32/Platform.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Win32/Platform.d
../bin/gccDebug/../../common/src/System/Win32/Platform.o: ../../common/src/System/Win32/Platform.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Win32/Platform.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Win32/Platform.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Win32/Platform.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Win32/Platform.d

# Compiles file ../../common/src/System/Win32/Thread.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Win32/Thread.d
../bin/gccDebug/../../common/src/System/Win32/Thread.o: ../../common/src/System/Win32/Thread.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Win32/Thread.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Win32/Thread.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Win32/Thread.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Win32/Thread.d

# Compiles file ../../common/src/System/Win32/ThreadLocal.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Win32/ThreadLocal.d
../bin/gccDebug/../../common/src/System/Win32/ThreadLocal.o: ../../common/src/System/Win32/ThreadLocal.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Win32/ThreadLocal.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Win32/ThreadLocal.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Win32/ThreadLocal.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Win32/ThreadLocal.d

# Compiles file ../../common/src/System/String/bstrlib.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/String/bstrlib.d
../bin/gccDebug/../../common/src/System/String/bstrlib.o: ../../common/src/System/String/bstrlib.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/String/bstrlib.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/String/bstrlib.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/String/bstrlib.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/String/bstrlib.d

# Compiles file ../../common/src/System/String/Lexer.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/String/Lexer.d
../bin/gccDebug/../../common/src/System/String/Lexer.o: ../../common/src/System/String/Lexer.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/String/Lexer.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/String/Lexer.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/String/Lexer.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/String/Lexer.d

# Compiles file ../../common/src/System/DataTypes/BinaryTree.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/DataTypes/BinaryTree.d
../bin/gccDebug/../../common/src/System/DataTypes/BinaryTree.o: ../../common/src/System/DataTypes/BinaryTree.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/DataTypes/BinaryTree.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/DataTypes/BinaryTree.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/DataTypes/BinaryTree.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/DataTypes/BinaryTree.d

# Compiles file ../../common/src/System/DataTypes/Bitset.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/DataTypes/Bitset.d
../bin/gccDebug/../../common/src/System/DataTypes/Bitset.o: ../../common/src/System/DataTypes/Bitset.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/DataTypes/Bitset.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/DataTypes/Bitset.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/DataTypes/Bitset.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/DataTypes/Bitset.d

# Compiles file ../../common/src/System/Memory/Allocator.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Memory/Allocator.d
../bin/gccDebug/../../common/src/System/Memory/Allocator.o: ../../common/src/System/Memory/Allocator.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Memory/Allocator.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Memory/Allocator.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Memory/Allocator.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Memory/Allocator.d

# Compiles file ../../common/src/System/Memory/Pool.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/System/Memory/Pool.d
../bin/gccDebug/../../common/src/System/Memory/Pool.o: ../../common/src/System/Memory/Pool.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/System/Memory/Pool.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/System/Memory/Pool.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/System/Memory/Pool.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/System/Memory/Pool.d

# Compiles file ../src/Network/Protocol.c for the Debug configuration...
-include ../bin/gccDebug/../src/Network/Protocol.d
../bin/gccDebug/../src/Network/Protocol.o: ../src/Network/Protocol.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Network/Protocol.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Network/Protocol.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Network/Protocol.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Network/Protocol.d

# Compiles file ../../common/src/Network/Socket.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Network/Socket.d
../bin/gccDebug/../../common/src/Network/Socket.o: ../../common/src/Network/Socket.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Network/Socket.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Network/Socket.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Network/Socket.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Network/Socket.d

# Compiles file ../../common/src/Network/Win32/SocketHelper.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Network/Win32/SocketHelper.d
../bin/gccDebug/../../common/src/Network/Win32/SocketHelper.o: ../../common/src/Network/Win32/SocketHelper.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Network/Win32/SocketHelper.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Network/Win32/SocketHelper.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Network/Win32/SocketHelper.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Network/Win32/SocketHelper.d

# Compiles file ../src/Game/Application.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/Application.d
../bin/gccDebug/../src/Game/Application.o: ../src/Game/Application.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/Application.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/Application.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/Application.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/Application.d

# Compiles file ../src/Game/Character.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/Character.d
../bin/gccDebug/../src/Game/Character.o: ../src/Game/Character.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/Character.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/Character.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/Character.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/Character.d

# Compiles file ../src/Game/Console.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/Console.d
../bin/gccDebug/../src/Game/Console.o: ../src/Game/Console.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/Console.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/Console.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/Console.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/Console.d

# Compiles file ../../common/src/Game/RealTime.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Game/RealTime.d
../bin/gccDebug/../../common/src/Game/RealTime.o: ../../common/src/Game/RealTime.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Game/RealTime.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Game/RealTime.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Game/RealTime.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Game/RealTime.d

# Compiles file ../src/Game/User.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/User.d
../bin/gccDebug/../src/Game/User.o: ../src/Game/User.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/User.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/User.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/User.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/User.d

# Compiles file ../src/Game/Weather.c for the Debug configuration...
-include ../bin/gccDebug/../src/Game/Weather.d
../bin/gccDebug/../src/Game/Weather.o: ../src/Game/Weather.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../src/Game/Weather.c $(Debug_Include_Path) -o ../bin/gccDebug/../src/Game/Weather.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../src/Game/Weather.c $(Debug_Include_Path) > ../bin/gccDebug/../src/Game/Weather.d

# Compiles file ../../common/src/Math/General.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/General.d
../bin/gccDebug/../../common/src/Math/General.o: ../../common/src/Math/General.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/General.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/General.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/General.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/General.d

# Compiles file ../../common/src/Math/Geometry/Matrix.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Matrix.d
../bin/gccDebug/../../common/src/Math/Geometry/Matrix.o: ../../common/src/Math/Geometry/Matrix.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Matrix.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Matrix.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Matrix.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Matrix.d

# Compiles file ../../common/src/Math/Geometry/Plane.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Plane.d
../bin/gccDebug/../../common/src/Math/Geometry/Plane.o: ../../common/src/Math/Geometry/Plane.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Plane.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Plane.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Plane.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Plane.d

# Compiles file ../../common/src/Math/Geometry/Quaternion.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Quaternion.d
../bin/gccDebug/../../common/src/Math/Geometry/Quaternion.o: ../../common/src/Math/Geometry/Quaternion.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Quaternion.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Quaternion.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Quaternion.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Quaternion.d

# Compiles file ../../common/src/Math/Geometry/Rect.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Rect.d
../bin/gccDebug/../../common/src/Math/Geometry/Rect.o: ../../common/src/Math/Geometry/Rect.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Rect.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Rect.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Rect.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Rect.d

# Compiles file ../../common/src/Math/Geometry/Vector.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Vector.d
../bin/gccDebug/../../common/src/Math/Geometry/Vector.o: ../../common/src/Math/Geometry/Vector.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Vector.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Vector.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Vector.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Vector.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Box.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Box.d
../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Box.o: ../../common/src/Math/Geometry/Bounding/Box.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Box.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Box.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Box.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Box.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Frustum.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Frustum.d
../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Frustum.o: ../../common/src/Math/Geometry/Bounding/Frustum.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Frustum.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Frustum.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Frustum.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Frustum.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Quad.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Quad.d
../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Quad.o: ../../common/src/Math/Geometry/Bounding/Quad.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Quad.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Quad.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Quad.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Quad.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Sphere.c for the Debug configuration...
-include ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Sphere.d
../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Sphere.o: ../../common/src/Math/Geometry/Bounding/Sphere.c
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Sphere.c $(Debug_Include_Path) -o ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Sphere.o
	$(C_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Sphere.c $(Debug_Include_Path) > ../bin/gccDebug/../../common/src/Math/Geometry/Bounding/Sphere.d

# Builds the Release configuration...
.PHONY: Release
Release: create_folders ../bin/gccRelease/../src/main.o ../bin/gccRelease/../src/Database/Account.o ../bin/gccRelease/../../common/src/Database/Connector.o ../bin/gccRelease/../../common/src/Database/Default.o ../bin/gccRelease/../src/Database/Npc.o ../bin/gccRelease/../src/Database/Player.o ../bin/gccRelease/../../common/src/Database/Query.o ../bin/gccRelease/../../common/src/System/CPUInfo.o ../bin/gccRelease/../../common/src/System/Dictionary.o ../bin/gccRelease/../../common/src/System/Error.o ../bin/gccRelease/../../common/src/System/MD5.o ../bin/gccRelease/../../common/src/System/Pack.o ../bin/gccRelease/../../common/src/System/Parser.o ../bin/gccRelease/../../common/src/System/Timer.o ../bin/gccRelease/../../common/src/System/TimerManager.o ../bin/gccRelease/../../common/src/System/Win32/Condition.o ../bin/gccRelease/../../common/src/System/Win32/IOHelper.o ../bin/gccRelease/../../common/src/System/Win32/Mutex.o ../bin/gccRelease/../../common/src/System/Win32/Platform.o ../bin/gccRelease/../../common/src/System/Win32/Thread.o ../bin/gccRelease/../../common/src/System/Win32/ThreadLocal.o ../bin/gccRelease/../../common/src/System/String/bstrlib.o ../bin/gccRelease/../../common/src/System/String/Lexer.o ../bin/gccRelease/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccRelease/../../common/src/System/DataTypes/Bitset.o ../bin/gccRelease/../../common/src/System/Memory/Allocator.o ../bin/gccRelease/../../common/src/System/Memory/Pool.o ../bin/gccRelease/../src/Network/Protocol.o ../bin/gccRelease/../../common/src/Network/Socket.o ../bin/gccRelease/../../common/src/Network/Win32/SocketHelper.o ../bin/gccRelease/../src/Game/Application.o ../bin/gccRelease/../src/Game/Character.o ../bin/gccRelease/../src/Game/Console.o ../bin/gccRelease/../../common/src/Game/RealTime.o ../bin/gccRelease/../src/Game/User.o ../bin/gccRelease/../src/Game/Weather.o ../bin/gccRelease/../../common/src/Math/General.o ../bin/gccRelease/../../common/src/Math/Geometry/Matrix.o ../bin/gccRelease/../../common/src/Math/Geometry/Plane.o ../bin/gccRelease/../../common/src/Math/Geometry/Quaternion.o ../bin/gccRelease/../../common/src/Math/Geometry/Rect.o ../bin/gccRelease/../../common/src/Math/Geometry/Vector.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Sphere.o 
	g++ ../bin/gccRelease/../src/main.o ../bin/gccRelease/../src/Database/Account.o ../bin/gccRelease/../../common/src/Database/Connector.o ../bin/gccRelease/../../common/src/Database/Default.o ../bin/gccRelease/../src/Database/Npc.o ../bin/gccRelease/../src/Database/Player.o ../bin/gccRelease/../../common/src/Database/Query.o ../bin/gccRelease/../../common/src/System/CPUInfo.o ../bin/gccRelease/../../common/src/System/Dictionary.o ../bin/gccRelease/../../common/src/System/Error.o ../bin/gccRelease/../../common/src/System/MD5.o ../bin/gccRelease/../../common/src/System/Pack.o ../bin/gccRelease/../../common/src/System/Parser.o ../bin/gccRelease/../../common/src/System/Timer.o ../bin/gccRelease/../../common/src/System/TimerManager.o ../bin/gccRelease/../../common/src/System/Win32/Condition.o ../bin/gccRelease/../../common/src/System/Win32/IOHelper.o ../bin/gccRelease/../../common/src/System/Win32/Mutex.o ../bin/gccRelease/../../common/src/System/Win32/Platform.o ../bin/gccRelease/../../common/src/System/Win32/Thread.o ../bin/gccRelease/../../common/src/System/Win32/ThreadLocal.o ../bin/gccRelease/../../common/src/System/String/bstrlib.o ../bin/gccRelease/../../common/src/System/String/Lexer.o ../bin/gccRelease/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccRelease/../../common/src/System/DataTypes/Bitset.o ../bin/gccRelease/../../common/src/System/Memory/Allocator.o ../bin/gccRelease/../../common/src/System/Memory/Pool.o ../bin/gccRelease/../src/Network/Protocol.o ../bin/gccRelease/../../common/src/Network/Socket.o ../bin/gccRelease/../../common/src/Network/Win32/SocketHelper.o ../bin/gccRelease/../src/Game/Application.o ../bin/gccRelease/../src/Game/Character.o ../bin/gccRelease/../src/Game/Console.o ../bin/gccRelease/../../common/src/Game/RealTime.o ../bin/gccRelease/../src/Game/User.o ../bin/gccRelease/../src/Game/Weather.o ../bin/gccRelease/../../common/src/Math/General.o ../bin/gccRelease/../../common/src/Math/Geometry/Matrix.o ../bin/gccRelease/../../common/src/Math/Geometry/Plane.o ../bin/gccRelease/../../common/src/Math/Geometry/Quaternion.o ../bin/gccRelease/../../common/src/Math/Geometry/Rect.o ../bin/gccRelease/../../common/src/Math/Geometry/Vector.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Sphere.o  $(Release_Library_Path) $(Release_Libraries) -Wl,-rpath,./ -o ../bin/gccRelease/Server.exe

# Compiles file ../src/main.c for the Release configuration...
-include ../bin/gccRelease/../src/main.d
../bin/gccRelease/../src/main.o: ../src/main.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/main.c $(Release_Include_Path) -o ../bin/gccRelease/../src/main.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/main.c $(Release_Include_Path) > ../bin/gccRelease/../src/main.d

# Compiles file ../src/Database/Account.c for the Release configuration...
-include ../bin/gccRelease/../src/Database/Account.d
../bin/gccRelease/../src/Database/Account.o: ../src/Database/Account.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Database/Account.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Database/Account.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Database/Account.c $(Release_Include_Path) > ../bin/gccRelease/../src/Database/Account.d

# Compiles file ../../common/src/Database/Connector.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Database/Connector.d
../bin/gccRelease/../../common/src/Database/Connector.o: ../../common/src/Database/Connector.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Database/Connector.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Database/Connector.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Database/Connector.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Database/Connector.d

# Compiles file ../../common/src/Database/Default.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Database/Default.d
../bin/gccRelease/../../common/src/Database/Default.o: ../../common/src/Database/Default.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Database/Default.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Database/Default.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Database/Default.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Database/Default.d

# Compiles file ../src/Database/Npc.c for the Release configuration...
-include ../bin/gccRelease/../src/Database/Npc.d
../bin/gccRelease/../src/Database/Npc.o: ../src/Database/Npc.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Database/Npc.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Database/Npc.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Database/Npc.c $(Release_Include_Path) > ../bin/gccRelease/../src/Database/Npc.d

# Compiles file ../src/Database/Player.c for the Release configuration...
-include ../bin/gccRelease/../src/Database/Player.d
../bin/gccRelease/../src/Database/Player.o: ../src/Database/Player.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Database/Player.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Database/Player.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Database/Player.c $(Release_Include_Path) > ../bin/gccRelease/../src/Database/Player.d

# Compiles file ../../common/src/Database/Query.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Database/Query.d
../bin/gccRelease/../../common/src/Database/Query.o: ../../common/src/Database/Query.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Database/Query.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Database/Query.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Database/Query.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Database/Query.d

# Compiles file ../../common/src/System/CPUInfo.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/CPUInfo.d
../bin/gccRelease/../../common/src/System/CPUInfo.o: ../../common/src/System/CPUInfo.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/CPUInfo.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/CPUInfo.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/CPUInfo.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/CPUInfo.d

# Compiles file ../../common/src/System/Dictionary.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Dictionary.d
../bin/gccRelease/../../common/src/System/Dictionary.o: ../../common/src/System/Dictionary.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Dictionary.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Dictionary.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Dictionary.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Dictionary.d

# Compiles file ../../common/src/System/Error.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Error.d
../bin/gccRelease/../../common/src/System/Error.o: ../../common/src/System/Error.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Error.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Error.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Error.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Error.d

# Compiles file ../../common/src/System/MD5.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/MD5.d
../bin/gccRelease/../../common/src/System/MD5.o: ../../common/src/System/MD5.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/MD5.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/MD5.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/MD5.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/MD5.d

# Compiles file ../../common/src/System/Pack.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Pack.d
../bin/gccRelease/../../common/src/System/Pack.o: ../../common/src/System/Pack.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Pack.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Pack.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Pack.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Pack.d

# Compiles file ../../common/src/System/Parser.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Parser.d
../bin/gccRelease/../../common/src/System/Parser.o: ../../common/src/System/Parser.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Parser.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Parser.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Parser.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Parser.d

# Compiles file ../../common/src/System/Timer.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Timer.d
../bin/gccRelease/../../common/src/System/Timer.o: ../../common/src/System/Timer.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Timer.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Timer.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Timer.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Timer.d

# Compiles file ../../common/src/System/TimerManager.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/TimerManager.d
../bin/gccRelease/../../common/src/System/TimerManager.o: ../../common/src/System/TimerManager.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/TimerManager.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/TimerManager.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/TimerManager.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/TimerManager.d

# Compiles file ../../common/src/System/Win32/Condition.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Win32/Condition.d
../bin/gccRelease/../../common/src/System/Win32/Condition.o: ../../common/src/System/Win32/Condition.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Win32/Condition.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Win32/Condition.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Win32/Condition.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Win32/Condition.d

# Compiles file ../../common/src/System/Win32/IOHelper.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Win32/IOHelper.d
../bin/gccRelease/../../common/src/System/Win32/IOHelper.o: ../../common/src/System/Win32/IOHelper.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Win32/IOHelper.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Win32/IOHelper.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Win32/IOHelper.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Win32/IOHelper.d

# Compiles file ../../common/src/System/Win32/Mutex.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Win32/Mutex.d
../bin/gccRelease/../../common/src/System/Win32/Mutex.o: ../../common/src/System/Win32/Mutex.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Win32/Mutex.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Win32/Mutex.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Win32/Mutex.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Win32/Mutex.d

# Compiles file ../../common/src/System/Win32/Platform.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Win32/Platform.d
../bin/gccRelease/../../common/src/System/Win32/Platform.o: ../../common/src/System/Win32/Platform.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Win32/Platform.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Win32/Platform.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Win32/Platform.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Win32/Platform.d

# Compiles file ../../common/src/System/Win32/Thread.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Win32/Thread.d
../bin/gccRelease/../../common/src/System/Win32/Thread.o: ../../common/src/System/Win32/Thread.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Win32/Thread.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Win32/Thread.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Win32/Thread.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Win32/Thread.d

# Compiles file ../../common/src/System/Win32/ThreadLocal.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Win32/ThreadLocal.d
../bin/gccRelease/../../common/src/System/Win32/ThreadLocal.o: ../../common/src/System/Win32/ThreadLocal.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Win32/ThreadLocal.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Win32/ThreadLocal.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Win32/ThreadLocal.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Win32/ThreadLocal.d

# Compiles file ../../common/src/System/String/bstrlib.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/String/bstrlib.d
../bin/gccRelease/../../common/src/System/String/bstrlib.o: ../../common/src/System/String/bstrlib.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/String/bstrlib.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/String/bstrlib.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/String/bstrlib.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/String/bstrlib.d

# Compiles file ../../common/src/System/String/Lexer.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/String/Lexer.d
../bin/gccRelease/../../common/src/System/String/Lexer.o: ../../common/src/System/String/Lexer.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/String/Lexer.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/String/Lexer.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/String/Lexer.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/String/Lexer.d

# Compiles file ../../common/src/System/DataTypes/BinaryTree.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/DataTypes/BinaryTree.d
../bin/gccRelease/../../common/src/System/DataTypes/BinaryTree.o: ../../common/src/System/DataTypes/BinaryTree.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/DataTypes/BinaryTree.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/DataTypes/BinaryTree.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/DataTypes/BinaryTree.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/DataTypes/BinaryTree.d

# Compiles file ../../common/src/System/DataTypes/Bitset.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/DataTypes/Bitset.d
../bin/gccRelease/../../common/src/System/DataTypes/Bitset.o: ../../common/src/System/DataTypes/Bitset.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/DataTypes/Bitset.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/DataTypes/Bitset.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/DataTypes/Bitset.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/DataTypes/Bitset.d

# Compiles file ../../common/src/System/Memory/Allocator.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Memory/Allocator.d
../bin/gccRelease/../../common/src/System/Memory/Allocator.o: ../../common/src/System/Memory/Allocator.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Memory/Allocator.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Memory/Allocator.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Memory/Allocator.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Memory/Allocator.d

# Compiles file ../../common/src/System/Memory/Pool.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/System/Memory/Pool.d
../bin/gccRelease/../../common/src/System/Memory/Pool.o: ../../common/src/System/Memory/Pool.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/System/Memory/Pool.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/System/Memory/Pool.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/System/Memory/Pool.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/System/Memory/Pool.d

# Compiles file ../src/Network/Protocol.c for the Release configuration...
-include ../bin/gccRelease/../src/Network/Protocol.d
../bin/gccRelease/../src/Network/Protocol.o: ../src/Network/Protocol.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Network/Protocol.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Network/Protocol.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Network/Protocol.c $(Release_Include_Path) > ../bin/gccRelease/../src/Network/Protocol.d

# Compiles file ../../common/src/Network/Socket.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Network/Socket.d
../bin/gccRelease/../../common/src/Network/Socket.o: ../../common/src/Network/Socket.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Network/Socket.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Network/Socket.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Network/Socket.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Network/Socket.d

# Compiles file ../../common/src/Network/Win32/SocketHelper.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Network/Win32/SocketHelper.d
../bin/gccRelease/../../common/src/Network/Win32/SocketHelper.o: ../../common/src/Network/Win32/SocketHelper.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Network/Win32/SocketHelper.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Network/Win32/SocketHelper.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Network/Win32/SocketHelper.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Network/Win32/SocketHelper.d

# Compiles file ../src/Game/Application.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/Application.d
../bin/gccRelease/../src/Game/Application.o: ../src/Game/Application.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/Application.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/Application.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/Application.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/Application.d

# Compiles file ../src/Game/Character.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/Character.d
../bin/gccRelease/../src/Game/Character.o: ../src/Game/Character.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/Character.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/Character.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/Character.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/Character.d

# Compiles file ../src/Game/Console.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/Console.d
../bin/gccRelease/../src/Game/Console.o: ../src/Game/Console.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/Console.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/Console.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/Console.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/Console.d

# Compiles file ../../common/src/Game/RealTime.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Game/RealTime.d
../bin/gccRelease/../../common/src/Game/RealTime.o: ../../common/src/Game/RealTime.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Game/RealTime.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Game/RealTime.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Game/RealTime.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Game/RealTime.d

# Compiles file ../src/Game/User.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/User.d
../bin/gccRelease/../src/Game/User.o: ../src/Game/User.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/User.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/User.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/User.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/User.d

# Compiles file ../src/Game/Weather.c for the Release configuration...
-include ../bin/gccRelease/../src/Game/Weather.d
../bin/gccRelease/../src/Game/Weather.o: ../src/Game/Weather.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../src/Game/Weather.c $(Release_Include_Path) -o ../bin/gccRelease/../src/Game/Weather.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../src/Game/Weather.c $(Release_Include_Path) > ../bin/gccRelease/../src/Game/Weather.d

# Compiles file ../../common/src/Math/General.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/General.d
../bin/gccRelease/../../common/src/Math/General.o: ../../common/src/Math/General.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/General.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/General.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/General.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/General.d

# Compiles file ../../common/src/Math/Geometry/Matrix.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Matrix.d
../bin/gccRelease/../../common/src/Math/Geometry/Matrix.o: ../../common/src/Math/Geometry/Matrix.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Matrix.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Matrix.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Matrix.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Matrix.d

# Compiles file ../../common/src/Math/Geometry/Plane.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Plane.d
../bin/gccRelease/../../common/src/Math/Geometry/Plane.o: ../../common/src/Math/Geometry/Plane.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Plane.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Plane.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Plane.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Plane.d

# Compiles file ../../common/src/Math/Geometry/Quaternion.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Quaternion.d
../bin/gccRelease/../../common/src/Math/Geometry/Quaternion.o: ../../common/src/Math/Geometry/Quaternion.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Quaternion.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Quaternion.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Quaternion.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Quaternion.d

# Compiles file ../../common/src/Math/Geometry/Rect.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Rect.d
../bin/gccRelease/../../common/src/Math/Geometry/Rect.o: ../../common/src/Math/Geometry/Rect.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Rect.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Rect.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Rect.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Rect.d

# Compiles file ../../common/src/Math/Geometry/Vector.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Vector.d
../bin/gccRelease/../../common/src/Math/Geometry/Vector.o: ../../common/src/Math/Geometry/Vector.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Vector.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Vector.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Vector.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Vector.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Box.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Box.d
../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Box.o: ../../common/src/Math/Geometry/Bounding/Box.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Box.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Box.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Box.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Box.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Frustum.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Frustum.d
../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Frustum.o: ../../common/src/Math/Geometry/Bounding/Frustum.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Frustum.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Frustum.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Frustum.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Frustum.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Quad.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Quad.d
../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Quad.o: ../../common/src/Math/Geometry/Bounding/Quad.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Quad.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Quad.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Quad.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Quad.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Sphere.c for the Release configuration...
-include ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Sphere.d
../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Sphere.o: ../../common/src/Math/Geometry/Bounding/Sphere.c
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Sphere.c $(Release_Include_Path) -o ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Sphere.o
	$(C_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Sphere.c $(Release_Include_Path) > ../bin/gccRelease/../../common/src/Math/Geometry/Bounding/Sphere.d

# Builds the DebugXP configuration...
.PHONY: DebugXP
DebugXP: create_folders ../bin/gccDebugXP/../src/main.o ../bin/gccDebugXP/../src/Database/Account.o ../bin/gccDebugXP/../../common/src/Database/Connector.o ../bin/gccDebugXP/../../common/src/Database/Default.o ../bin/gccDebugXP/../src/Database/Npc.o ../bin/gccDebugXP/../src/Database/Player.o ../bin/gccDebugXP/../../common/src/Database/Query.o ../bin/gccDebugXP/../../common/src/System/CPUInfo.o ../bin/gccDebugXP/../../common/src/System/Dictionary.o ../bin/gccDebugXP/../../common/src/System/Error.o ../bin/gccDebugXP/../../common/src/System/MD5.o ../bin/gccDebugXP/../../common/src/System/Pack.o ../bin/gccDebugXP/../../common/src/System/Parser.o ../bin/gccDebugXP/../../common/src/System/Timer.o ../bin/gccDebugXP/../../common/src/System/TimerManager.o ../bin/gccDebugXP/../../common/src/System/Win32/Condition.o ../bin/gccDebugXP/../../common/src/System/Win32/IOHelper.o ../bin/gccDebugXP/../../common/src/System/Win32/Mutex.o ../bin/gccDebugXP/../../common/src/System/Win32/Platform.o ../bin/gccDebugXP/../../common/src/System/Win32/Thread.o ../bin/gccDebugXP/../../common/src/System/Win32/ThreadLocal.o ../bin/gccDebugXP/../../common/src/System/String/bstrlib.o ../bin/gccDebugXP/../../common/src/System/String/Lexer.o ../bin/gccDebugXP/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccDebugXP/../../common/src/System/DataTypes/Bitset.o ../bin/gccDebugXP/../../common/src/System/Memory/Allocator.o ../bin/gccDebugXP/../../common/src/System/Memory/Pool.o ../bin/gccDebugXP/../src/Network/Protocol.o ../bin/gccDebugXP/../../common/src/Network/Socket.o ../bin/gccDebugXP/../../common/src/Network/Win32/SocketHelper.o ../bin/gccDebugXP/../src/Game/Application.o ../bin/gccDebugXP/../src/Game/Character.o ../bin/gccDebugXP/../src/Game/Console.o ../bin/gccDebugXP/../../common/src/Game/RealTime.o ../bin/gccDebugXP/../src/Game/User.o ../bin/gccDebugXP/../src/Game/Weather.o ../bin/gccDebugXP/../../common/src/Math/General.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Matrix.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Plane.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Quaternion.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Rect.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Vector.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Sphere.o 
	g++ ../bin/gccDebugXP/../src/main.o ../bin/gccDebugXP/../src/Database/Account.o ../bin/gccDebugXP/../../common/src/Database/Connector.o ../bin/gccDebugXP/../../common/src/Database/Default.o ../bin/gccDebugXP/../src/Database/Npc.o ../bin/gccDebugXP/../src/Database/Player.o ../bin/gccDebugXP/../../common/src/Database/Query.o ../bin/gccDebugXP/../../common/src/System/CPUInfo.o ../bin/gccDebugXP/../../common/src/System/Dictionary.o ../bin/gccDebugXP/../../common/src/System/Error.o ../bin/gccDebugXP/../../common/src/System/MD5.o ../bin/gccDebugXP/../../common/src/System/Pack.o ../bin/gccDebugXP/../../common/src/System/Parser.o ../bin/gccDebugXP/../../common/src/System/Timer.o ../bin/gccDebugXP/../../common/src/System/TimerManager.o ../bin/gccDebugXP/../../common/src/System/Win32/Condition.o ../bin/gccDebugXP/../../common/src/System/Win32/IOHelper.o ../bin/gccDebugXP/../../common/src/System/Win32/Mutex.o ../bin/gccDebugXP/../../common/src/System/Win32/Platform.o ../bin/gccDebugXP/../../common/src/System/Win32/Thread.o ../bin/gccDebugXP/../../common/src/System/Win32/ThreadLocal.o ../bin/gccDebugXP/../../common/src/System/String/bstrlib.o ../bin/gccDebugXP/../../common/src/System/String/Lexer.o ../bin/gccDebugXP/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccDebugXP/../../common/src/System/DataTypes/Bitset.o ../bin/gccDebugXP/../../common/src/System/Memory/Allocator.o ../bin/gccDebugXP/../../common/src/System/Memory/Pool.o ../bin/gccDebugXP/../src/Network/Protocol.o ../bin/gccDebugXP/../../common/src/Network/Socket.o ../bin/gccDebugXP/../../common/src/Network/Win32/SocketHelper.o ../bin/gccDebugXP/../src/Game/Application.o ../bin/gccDebugXP/../src/Game/Character.o ../bin/gccDebugXP/../src/Game/Console.o ../bin/gccDebugXP/../../common/src/Game/RealTime.o ../bin/gccDebugXP/../src/Game/User.o ../bin/gccDebugXP/../src/Game/Weather.o ../bin/gccDebugXP/../../common/src/Math/General.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Matrix.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Plane.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Quaternion.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Rect.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Vector.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Sphere.o  $(DebugXP_Library_Path) $(DebugXP_Libraries) -Wl,-rpath,./ -o ../bin/gccDebugXP/Server.exe

# Compiles file ../src/main.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/main.d
../bin/gccDebugXP/../src/main.o: ../src/main.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/main.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/main.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/main.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/main.d

# Compiles file ../src/Database/Account.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Database/Account.d
../bin/gccDebugXP/../src/Database/Account.o: ../src/Database/Account.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Database/Account.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Database/Account.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Database/Account.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Database/Account.d

# Compiles file ../../common/src/Database/Connector.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Database/Connector.d
../bin/gccDebugXP/../../common/src/Database/Connector.o: ../../common/src/Database/Connector.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Database/Connector.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Database/Connector.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Database/Connector.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Database/Connector.d

# Compiles file ../../common/src/Database/Default.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Database/Default.d
../bin/gccDebugXP/../../common/src/Database/Default.o: ../../common/src/Database/Default.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Database/Default.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Database/Default.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Database/Default.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Database/Default.d

# Compiles file ../src/Database/Npc.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Database/Npc.d
../bin/gccDebugXP/../src/Database/Npc.o: ../src/Database/Npc.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Database/Npc.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Database/Npc.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Database/Npc.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Database/Npc.d

# Compiles file ../src/Database/Player.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Database/Player.d
../bin/gccDebugXP/../src/Database/Player.o: ../src/Database/Player.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Database/Player.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Database/Player.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Database/Player.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Database/Player.d

# Compiles file ../../common/src/Database/Query.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Database/Query.d
../bin/gccDebugXP/../../common/src/Database/Query.o: ../../common/src/Database/Query.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Database/Query.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Database/Query.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Database/Query.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Database/Query.d

# Compiles file ../../common/src/System/CPUInfo.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/CPUInfo.d
../bin/gccDebugXP/../../common/src/System/CPUInfo.o: ../../common/src/System/CPUInfo.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/CPUInfo.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/CPUInfo.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/CPUInfo.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/CPUInfo.d

# Compiles file ../../common/src/System/Dictionary.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Dictionary.d
../bin/gccDebugXP/../../common/src/System/Dictionary.o: ../../common/src/System/Dictionary.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Dictionary.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Dictionary.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Dictionary.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Dictionary.d

# Compiles file ../../common/src/System/Error.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Error.d
../bin/gccDebugXP/../../common/src/System/Error.o: ../../common/src/System/Error.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Error.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Error.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Error.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Error.d

# Compiles file ../../common/src/System/MD5.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/MD5.d
../bin/gccDebugXP/../../common/src/System/MD5.o: ../../common/src/System/MD5.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/MD5.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/MD5.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/MD5.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/MD5.d

# Compiles file ../../common/src/System/Pack.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Pack.d
../bin/gccDebugXP/../../common/src/System/Pack.o: ../../common/src/System/Pack.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Pack.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Pack.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Pack.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Pack.d

# Compiles file ../../common/src/System/Parser.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Parser.d
../bin/gccDebugXP/../../common/src/System/Parser.o: ../../common/src/System/Parser.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Parser.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Parser.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Parser.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Parser.d

# Compiles file ../../common/src/System/Timer.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Timer.d
../bin/gccDebugXP/../../common/src/System/Timer.o: ../../common/src/System/Timer.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Timer.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Timer.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Timer.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Timer.d

# Compiles file ../../common/src/System/TimerManager.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/TimerManager.d
../bin/gccDebugXP/../../common/src/System/TimerManager.o: ../../common/src/System/TimerManager.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/TimerManager.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/TimerManager.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/TimerManager.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/TimerManager.d

# Compiles file ../../common/src/System/Win32/Condition.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Win32/Condition.d
../bin/gccDebugXP/../../common/src/System/Win32/Condition.o: ../../common/src/System/Win32/Condition.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Win32/Condition.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Win32/Condition.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Win32/Condition.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Win32/Condition.d

# Compiles file ../../common/src/System/Win32/IOHelper.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Win32/IOHelper.d
../bin/gccDebugXP/../../common/src/System/Win32/IOHelper.o: ../../common/src/System/Win32/IOHelper.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Win32/IOHelper.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Win32/IOHelper.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Win32/IOHelper.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Win32/IOHelper.d

# Compiles file ../../common/src/System/Win32/Mutex.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Win32/Mutex.d
../bin/gccDebugXP/../../common/src/System/Win32/Mutex.o: ../../common/src/System/Win32/Mutex.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Win32/Mutex.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Win32/Mutex.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Win32/Mutex.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Win32/Mutex.d

# Compiles file ../../common/src/System/Win32/Platform.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Win32/Platform.d
../bin/gccDebugXP/../../common/src/System/Win32/Platform.o: ../../common/src/System/Win32/Platform.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Win32/Platform.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Win32/Platform.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Win32/Platform.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Win32/Platform.d

# Compiles file ../../common/src/System/Win32/Thread.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Win32/Thread.d
../bin/gccDebugXP/../../common/src/System/Win32/Thread.o: ../../common/src/System/Win32/Thread.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Win32/Thread.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Win32/Thread.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Win32/Thread.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Win32/Thread.d

# Compiles file ../../common/src/System/Win32/ThreadLocal.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Win32/ThreadLocal.d
../bin/gccDebugXP/../../common/src/System/Win32/ThreadLocal.o: ../../common/src/System/Win32/ThreadLocal.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Win32/ThreadLocal.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Win32/ThreadLocal.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Win32/ThreadLocal.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Win32/ThreadLocal.d

# Compiles file ../../common/src/System/String/bstrlib.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/String/bstrlib.d
../bin/gccDebugXP/../../common/src/System/String/bstrlib.o: ../../common/src/System/String/bstrlib.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/String/bstrlib.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/String/bstrlib.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/String/bstrlib.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/String/bstrlib.d

# Compiles file ../../common/src/System/String/Lexer.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/String/Lexer.d
../bin/gccDebugXP/../../common/src/System/String/Lexer.o: ../../common/src/System/String/Lexer.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/String/Lexer.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/String/Lexer.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/String/Lexer.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/String/Lexer.d

# Compiles file ../../common/src/System/DataTypes/BinaryTree.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/DataTypes/BinaryTree.d
../bin/gccDebugXP/../../common/src/System/DataTypes/BinaryTree.o: ../../common/src/System/DataTypes/BinaryTree.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/DataTypes/BinaryTree.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/DataTypes/BinaryTree.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/DataTypes/BinaryTree.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/DataTypes/BinaryTree.d

# Compiles file ../../common/src/System/DataTypes/Bitset.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/DataTypes/Bitset.d
../bin/gccDebugXP/../../common/src/System/DataTypes/Bitset.o: ../../common/src/System/DataTypes/Bitset.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/DataTypes/Bitset.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/DataTypes/Bitset.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/DataTypes/Bitset.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/DataTypes/Bitset.d

# Compiles file ../../common/src/System/Memory/Allocator.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Memory/Allocator.d
../bin/gccDebugXP/../../common/src/System/Memory/Allocator.o: ../../common/src/System/Memory/Allocator.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Memory/Allocator.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Memory/Allocator.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Memory/Allocator.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Memory/Allocator.d

# Compiles file ../../common/src/System/Memory/Pool.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/System/Memory/Pool.d
../bin/gccDebugXP/../../common/src/System/Memory/Pool.o: ../../common/src/System/Memory/Pool.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/System/Memory/Pool.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/System/Memory/Pool.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/System/Memory/Pool.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/System/Memory/Pool.d

# Compiles file ../src/Network/Protocol.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Network/Protocol.d
../bin/gccDebugXP/../src/Network/Protocol.o: ../src/Network/Protocol.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Network/Protocol.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Network/Protocol.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Network/Protocol.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Network/Protocol.d

# Compiles file ../../common/src/Network/Socket.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Network/Socket.d
../bin/gccDebugXP/../../common/src/Network/Socket.o: ../../common/src/Network/Socket.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Network/Socket.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Network/Socket.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Network/Socket.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Network/Socket.d

# Compiles file ../../common/src/Network/Win32/SocketHelper.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Network/Win32/SocketHelper.d
../bin/gccDebugXP/../../common/src/Network/Win32/SocketHelper.o: ../../common/src/Network/Win32/SocketHelper.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Network/Win32/SocketHelper.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Network/Win32/SocketHelper.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Network/Win32/SocketHelper.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Network/Win32/SocketHelper.d

# Compiles file ../src/Game/Application.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/Application.d
../bin/gccDebugXP/../src/Game/Application.o: ../src/Game/Application.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/Application.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/Application.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/Application.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/Application.d

# Compiles file ../src/Game/Character.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/Character.d
../bin/gccDebugXP/../src/Game/Character.o: ../src/Game/Character.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/Character.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/Character.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/Character.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/Character.d

# Compiles file ../src/Game/Console.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/Console.d
../bin/gccDebugXP/../src/Game/Console.o: ../src/Game/Console.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/Console.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/Console.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/Console.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/Console.d

# Compiles file ../../common/src/Game/RealTime.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Game/RealTime.d
../bin/gccDebugXP/../../common/src/Game/RealTime.o: ../../common/src/Game/RealTime.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Game/RealTime.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Game/RealTime.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Game/RealTime.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Game/RealTime.d

# Compiles file ../src/Game/User.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/User.d
../bin/gccDebugXP/../src/Game/User.o: ../src/Game/User.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/User.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/User.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/User.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/User.d

# Compiles file ../src/Game/Weather.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../src/Game/Weather.d
../bin/gccDebugXP/../src/Game/Weather.o: ../src/Game/Weather.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../src/Game/Weather.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../src/Game/Weather.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../src/Game/Weather.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../src/Game/Weather.d

# Compiles file ../../common/src/Math/General.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/General.d
../bin/gccDebugXP/../../common/src/Math/General.o: ../../common/src/Math/General.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/General.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/General.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/General.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/General.d

# Compiles file ../../common/src/Math/Geometry/Matrix.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Matrix.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Matrix.o: ../../common/src/Math/Geometry/Matrix.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Matrix.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Matrix.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Matrix.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Matrix.d

# Compiles file ../../common/src/Math/Geometry/Plane.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Plane.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Plane.o: ../../common/src/Math/Geometry/Plane.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Plane.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Plane.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Plane.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Plane.d

# Compiles file ../../common/src/Math/Geometry/Quaternion.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Quaternion.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Quaternion.o: ../../common/src/Math/Geometry/Quaternion.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Quaternion.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Quaternion.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Quaternion.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Quaternion.d

# Compiles file ../../common/src/Math/Geometry/Rect.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Rect.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Rect.o: ../../common/src/Math/Geometry/Rect.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Rect.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Rect.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Rect.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Rect.d

# Compiles file ../../common/src/Math/Geometry/Vector.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Vector.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Vector.o: ../../common/src/Math/Geometry/Vector.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Vector.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Vector.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Vector.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Vector.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Box.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Box.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Box.o: ../../common/src/Math/Geometry/Bounding/Box.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Box.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Box.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Box.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Box.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Frustum.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Frustum.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Frustum.o: ../../common/src/Math/Geometry/Bounding/Frustum.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Frustum.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Frustum.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Frustum.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Frustum.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Quad.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Quad.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Quad.o: ../../common/src/Math/Geometry/Bounding/Quad.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Quad.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Quad.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Quad.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Quad.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Sphere.c for the DebugXP configuration...
-include ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Sphere.d
../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Sphere.o: ../../common/src/Math/Geometry/Bounding/Sphere.c
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Sphere.c $(DebugXP_Include_Path) -o ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Sphere.o
	$(C_COMPILER) $(DebugXP_Preprocessor_Definitions) $(DebugXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Sphere.c $(DebugXP_Include_Path) > ../bin/gccDebugXP/../../common/src/Math/Geometry/Bounding/Sphere.d

# Builds the ReleaseXP configuration...
.PHONY: ReleaseXP
ReleaseXP: create_folders ../bin/gccReleaseXP/../src/main.o ../bin/gccReleaseXP/../src/Database/Account.o ../bin/gccReleaseXP/../../common/src/Database/Connector.o ../bin/gccReleaseXP/../../common/src/Database/Default.o ../bin/gccReleaseXP/../src/Database/Npc.o ../bin/gccReleaseXP/../src/Database/Player.o ../bin/gccReleaseXP/../../common/src/Database/Query.o ../bin/gccReleaseXP/../../common/src/System/CPUInfo.o ../bin/gccReleaseXP/../../common/src/System/Dictionary.o ../bin/gccReleaseXP/../../common/src/System/Error.o ../bin/gccReleaseXP/../../common/src/System/MD5.o ../bin/gccReleaseXP/../../common/src/System/Pack.o ../bin/gccReleaseXP/../../common/src/System/Parser.o ../bin/gccReleaseXP/../../common/src/System/Timer.o ../bin/gccReleaseXP/../../common/src/System/TimerManager.o ../bin/gccReleaseXP/../../common/src/System/Win32/Condition.o ../bin/gccReleaseXP/../../common/src/System/Win32/IOHelper.o ../bin/gccReleaseXP/../../common/src/System/Win32/Mutex.o ../bin/gccReleaseXP/../../common/src/System/Win32/Platform.o ../bin/gccReleaseXP/../../common/src/System/Win32/Thread.o ../bin/gccReleaseXP/../../common/src/System/Win32/ThreadLocal.o ../bin/gccReleaseXP/../../common/src/System/String/bstrlib.o ../bin/gccReleaseXP/../../common/src/System/String/Lexer.o ../bin/gccReleaseXP/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccReleaseXP/../../common/src/System/DataTypes/Bitset.o ../bin/gccReleaseXP/../../common/src/System/Memory/Allocator.o ../bin/gccReleaseXP/../../common/src/System/Memory/Pool.o ../bin/gccReleaseXP/../src/Network/Protocol.o ../bin/gccReleaseXP/../../common/src/Network/Socket.o ../bin/gccReleaseXP/../../common/src/Network/Win32/SocketHelper.o ../bin/gccReleaseXP/../src/Game/Application.o ../bin/gccReleaseXP/../src/Game/Character.o ../bin/gccReleaseXP/../src/Game/Console.o ../bin/gccReleaseXP/../../common/src/Game/RealTime.o ../bin/gccReleaseXP/../src/Game/User.o ../bin/gccReleaseXP/../src/Game/Weather.o ../bin/gccReleaseXP/../../common/src/Math/General.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Matrix.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Plane.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Quaternion.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Rect.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Vector.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Sphere.o 
	g++ ../bin/gccReleaseXP/../src/main.o ../bin/gccReleaseXP/../src/Database/Account.o ../bin/gccReleaseXP/../../common/src/Database/Connector.o ../bin/gccReleaseXP/../../common/src/Database/Default.o ../bin/gccReleaseXP/../src/Database/Npc.o ../bin/gccReleaseXP/../src/Database/Player.o ../bin/gccReleaseXP/../../common/src/Database/Query.o ../bin/gccReleaseXP/../../common/src/System/CPUInfo.o ../bin/gccReleaseXP/../../common/src/System/Dictionary.o ../bin/gccReleaseXP/../../common/src/System/Error.o ../bin/gccReleaseXP/../../common/src/System/MD5.o ../bin/gccReleaseXP/../../common/src/System/Pack.o ../bin/gccReleaseXP/../../common/src/System/Parser.o ../bin/gccReleaseXP/../../common/src/System/Timer.o ../bin/gccReleaseXP/../../common/src/System/TimerManager.o ../bin/gccReleaseXP/../../common/src/System/Win32/Condition.o ../bin/gccReleaseXP/../../common/src/System/Win32/IOHelper.o ../bin/gccReleaseXP/../../common/src/System/Win32/Mutex.o ../bin/gccReleaseXP/../../common/src/System/Win32/Platform.o ../bin/gccReleaseXP/../../common/src/System/Win32/Thread.o ../bin/gccReleaseXP/../../common/src/System/Win32/ThreadLocal.o ../bin/gccReleaseXP/../../common/src/System/String/bstrlib.o ../bin/gccReleaseXP/../../common/src/System/String/Lexer.o ../bin/gccReleaseXP/../../common/src/System/DataTypes/BinaryTree.o ../bin/gccReleaseXP/../../common/src/System/DataTypes/Bitset.o ../bin/gccReleaseXP/../../common/src/System/Memory/Allocator.o ../bin/gccReleaseXP/../../common/src/System/Memory/Pool.o ../bin/gccReleaseXP/../src/Network/Protocol.o ../bin/gccReleaseXP/../../common/src/Network/Socket.o ../bin/gccReleaseXP/../../common/src/Network/Win32/SocketHelper.o ../bin/gccReleaseXP/../src/Game/Application.o ../bin/gccReleaseXP/../src/Game/Character.o ../bin/gccReleaseXP/../src/Game/Console.o ../bin/gccReleaseXP/../../common/src/Game/RealTime.o ../bin/gccReleaseXP/../src/Game/User.o ../bin/gccReleaseXP/../src/Game/Weather.o ../bin/gccReleaseXP/../../common/src/Math/General.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Matrix.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Plane.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Quaternion.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Rect.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Vector.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Box.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Frustum.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Quad.o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Sphere.o  $(ReleaseXP_Library_Path) $(ReleaseXP_Libraries) -Wl,-rpath,./ -o ../bin/gccReleaseXP/Server.exe

# Compiles file ../src/main.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/main.d
../bin/gccReleaseXP/../src/main.o: ../src/main.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/main.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/main.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/main.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/main.d

# Compiles file ../src/Database/Account.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Database/Account.d
../bin/gccReleaseXP/../src/Database/Account.o: ../src/Database/Account.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Database/Account.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Database/Account.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Database/Account.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Database/Account.d

# Compiles file ../../common/src/Database/Connector.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Database/Connector.d
../bin/gccReleaseXP/../../common/src/Database/Connector.o: ../../common/src/Database/Connector.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Database/Connector.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Database/Connector.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Database/Connector.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Database/Connector.d

# Compiles file ../../common/src/Database/Default.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Database/Default.d
../bin/gccReleaseXP/../../common/src/Database/Default.o: ../../common/src/Database/Default.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Database/Default.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Database/Default.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Database/Default.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Database/Default.d

# Compiles file ../src/Database/Npc.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Database/Npc.d
../bin/gccReleaseXP/../src/Database/Npc.o: ../src/Database/Npc.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Database/Npc.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Database/Npc.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Database/Npc.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Database/Npc.d

# Compiles file ../src/Database/Player.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Database/Player.d
../bin/gccReleaseXP/../src/Database/Player.o: ../src/Database/Player.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Database/Player.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Database/Player.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Database/Player.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Database/Player.d

# Compiles file ../../common/src/Database/Query.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Database/Query.d
../bin/gccReleaseXP/../../common/src/Database/Query.o: ../../common/src/Database/Query.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Database/Query.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Database/Query.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Database/Query.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Database/Query.d

# Compiles file ../../common/src/System/CPUInfo.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/CPUInfo.d
../bin/gccReleaseXP/../../common/src/System/CPUInfo.o: ../../common/src/System/CPUInfo.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/CPUInfo.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/CPUInfo.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/CPUInfo.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/CPUInfo.d

# Compiles file ../../common/src/System/Dictionary.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Dictionary.d
../bin/gccReleaseXP/../../common/src/System/Dictionary.o: ../../common/src/System/Dictionary.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Dictionary.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Dictionary.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Dictionary.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Dictionary.d

# Compiles file ../../common/src/System/Error.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Error.d
../bin/gccReleaseXP/../../common/src/System/Error.o: ../../common/src/System/Error.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Error.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Error.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Error.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Error.d

# Compiles file ../../common/src/System/MD5.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/MD5.d
../bin/gccReleaseXP/../../common/src/System/MD5.o: ../../common/src/System/MD5.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/MD5.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/MD5.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/MD5.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/MD5.d

# Compiles file ../../common/src/System/Pack.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Pack.d
../bin/gccReleaseXP/../../common/src/System/Pack.o: ../../common/src/System/Pack.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Pack.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Pack.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Pack.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Pack.d

# Compiles file ../../common/src/System/Parser.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Parser.d
../bin/gccReleaseXP/../../common/src/System/Parser.o: ../../common/src/System/Parser.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Parser.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Parser.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Parser.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Parser.d

# Compiles file ../../common/src/System/Timer.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Timer.d
../bin/gccReleaseXP/../../common/src/System/Timer.o: ../../common/src/System/Timer.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Timer.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Timer.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Timer.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Timer.d

# Compiles file ../../common/src/System/TimerManager.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/TimerManager.d
../bin/gccReleaseXP/../../common/src/System/TimerManager.o: ../../common/src/System/TimerManager.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/TimerManager.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/TimerManager.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/TimerManager.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/TimerManager.d

# Compiles file ../../common/src/System/Win32/Condition.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Win32/Condition.d
../bin/gccReleaseXP/../../common/src/System/Win32/Condition.o: ../../common/src/System/Win32/Condition.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Win32/Condition.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Win32/Condition.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Win32/Condition.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Win32/Condition.d

# Compiles file ../../common/src/System/Win32/IOHelper.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Win32/IOHelper.d
../bin/gccReleaseXP/../../common/src/System/Win32/IOHelper.o: ../../common/src/System/Win32/IOHelper.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Win32/IOHelper.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Win32/IOHelper.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Win32/IOHelper.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Win32/IOHelper.d

# Compiles file ../../common/src/System/Win32/Mutex.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Win32/Mutex.d
../bin/gccReleaseXP/../../common/src/System/Win32/Mutex.o: ../../common/src/System/Win32/Mutex.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Win32/Mutex.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Win32/Mutex.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Win32/Mutex.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Win32/Mutex.d

# Compiles file ../../common/src/System/Win32/Platform.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Win32/Platform.d
../bin/gccReleaseXP/../../common/src/System/Win32/Platform.o: ../../common/src/System/Win32/Platform.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Win32/Platform.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Win32/Platform.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Win32/Platform.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Win32/Platform.d

# Compiles file ../../common/src/System/Win32/Thread.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Win32/Thread.d
../bin/gccReleaseXP/../../common/src/System/Win32/Thread.o: ../../common/src/System/Win32/Thread.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Win32/Thread.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Win32/Thread.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Win32/Thread.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Win32/Thread.d

# Compiles file ../../common/src/System/Win32/ThreadLocal.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Win32/ThreadLocal.d
../bin/gccReleaseXP/../../common/src/System/Win32/ThreadLocal.o: ../../common/src/System/Win32/ThreadLocal.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Win32/ThreadLocal.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Win32/ThreadLocal.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Win32/ThreadLocal.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Win32/ThreadLocal.d

# Compiles file ../../common/src/System/String/bstrlib.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/String/bstrlib.d
../bin/gccReleaseXP/../../common/src/System/String/bstrlib.o: ../../common/src/System/String/bstrlib.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/String/bstrlib.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/String/bstrlib.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/String/bstrlib.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/String/bstrlib.d

# Compiles file ../../common/src/System/String/Lexer.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/String/Lexer.d
../bin/gccReleaseXP/../../common/src/System/String/Lexer.o: ../../common/src/System/String/Lexer.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/String/Lexer.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/String/Lexer.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/String/Lexer.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/String/Lexer.d

# Compiles file ../../common/src/System/DataTypes/BinaryTree.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/DataTypes/BinaryTree.d
../bin/gccReleaseXP/../../common/src/System/DataTypes/BinaryTree.o: ../../common/src/System/DataTypes/BinaryTree.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/DataTypes/BinaryTree.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/DataTypes/BinaryTree.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/DataTypes/BinaryTree.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/DataTypes/BinaryTree.d

# Compiles file ../../common/src/System/DataTypes/Bitset.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/DataTypes/Bitset.d
../bin/gccReleaseXP/../../common/src/System/DataTypes/Bitset.o: ../../common/src/System/DataTypes/Bitset.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/DataTypes/Bitset.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/DataTypes/Bitset.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/DataTypes/Bitset.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/DataTypes/Bitset.d

# Compiles file ../../common/src/System/Memory/Allocator.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Memory/Allocator.d
../bin/gccReleaseXP/../../common/src/System/Memory/Allocator.o: ../../common/src/System/Memory/Allocator.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Memory/Allocator.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Memory/Allocator.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Memory/Allocator.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Memory/Allocator.d

# Compiles file ../../common/src/System/Memory/Pool.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/System/Memory/Pool.d
../bin/gccReleaseXP/../../common/src/System/Memory/Pool.o: ../../common/src/System/Memory/Pool.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/System/Memory/Pool.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/System/Memory/Pool.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/System/Memory/Pool.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/System/Memory/Pool.d

# Compiles file ../src/Network/Protocol.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Network/Protocol.d
../bin/gccReleaseXP/../src/Network/Protocol.o: ../src/Network/Protocol.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Network/Protocol.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Network/Protocol.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Network/Protocol.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Network/Protocol.d

# Compiles file ../../common/src/Network/Socket.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Network/Socket.d
../bin/gccReleaseXP/../../common/src/Network/Socket.o: ../../common/src/Network/Socket.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Network/Socket.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Network/Socket.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Network/Socket.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Network/Socket.d

# Compiles file ../../common/src/Network/Win32/SocketHelper.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Network/Win32/SocketHelper.d
../bin/gccReleaseXP/../../common/src/Network/Win32/SocketHelper.o: ../../common/src/Network/Win32/SocketHelper.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Network/Win32/SocketHelper.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Network/Win32/SocketHelper.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Network/Win32/SocketHelper.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Network/Win32/SocketHelper.d

# Compiles file ../src/Game/Application.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/Application.d
../bin/gccReleaseXP/../src/Game/Application.o: ../src/Game/Application.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/Application.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/Application.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/Application.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/Application.d

# Compiles file ../src/Game/Character.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/Character.d
../bin/gccReleaseXP/../src/Game/Character.o: ../src/Game/Character.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/Character.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/Character.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/Character.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/Character.d

# Compiles file ../src/Game/Console.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/Console.d
../bin/gccReleaseXP/../src/Game/Console.o: ../src/Game/Console.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/Console.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/Console.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/Console.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/Console.d

# Compiles file ../../common/src/Game/RealTime.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Game/RealTime.d
../bin/gccReleaseXP/../../common/src/Game/RealTime.o: ../../common/src/Game/RealTime.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Game/RealTime.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Game/RealTime.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Game/RealTime.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Game/RealTime.d

# Compiles file ../src/Game/User.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/User.d
../bin/gccReleaseXP/../src/Game/User.o: ../src/Game/User.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/User.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/User.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/User.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/User.d

# Compiles file ../src/Game/Weather.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../src/Game/Weather.d
../bin/gccReleaseXP/../src/Game/Weather.o: ../src/Game/Weather.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../src/Game/Weather.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../src/Game/Weather.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../src/Game/Weather.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../src/Game/Weather.d

# Compiles file ../../common/src/Math/General.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/General.d
../bin/gccReleaseXP/../../common/src/Math/General.o: ../../common/src/Math/General.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/General.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/General.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/General.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/General.d

# Compiles file ../../common/src/Math/Geometry/Matrix.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Matrix.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Matrix.o: ../../common/src/Math/Geometry/Matrix.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Matrix.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Matrix.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Matrix.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Matrix.d

# Compiles file ../../common/src/Math/Geometry/Plane.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Plane.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Plane.o: ../../common/src/Math/Geometry/Plane.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Plane.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Plane.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Plane.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Plane.d

# Compiles file ../../common/src/Math/Geometry/Quaternion.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Quaternion.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Quaternion.o: ../../common/src/Math/Geometry/Quaternion.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Quaternion.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Quaternion.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Quaternion.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Quaternion.d

# Compiles file ../../common/src/Math/Geometry/Rect.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Rect.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Rect.o: ../../common/src/Math/Geometry/Rect.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Rect.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Rect.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Rect.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Rect.d

# Compiles file ../../common/src/Math/Geometry/Vector.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Vector.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Vector.o: ../../common/src/Math/Geometry/Vector.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Vector.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Vector.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Vector.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Vector.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Box.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Box.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Box.o: ../../common/src/Math/Geometry/Bounding/Box.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Box.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Box.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Box.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Box.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Frustum.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Frustum.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Frustum.o: ../../common/src/Math/Geometry/Bounding/Frustum.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Frustum.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Frustum.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Frustum.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Frustum.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Quad.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Quad.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Quad.o: ../../common/src/Math/Geometry/Bounding/Quad.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Quad.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Quad.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Quad.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Quad.d

# Compiles file ../../common/src/Math/Geometry/Bounding/Sphere.c for the ReleaseXP configuration...
-include ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Sphere.d
../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Sphere.o: ../../common/src/Math/Geometry/Bounding/Sphere.c
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -c ../../common/src/Math/Geometry/Bounding/Sphere.c $(ReleaseXP_Include_Path) -o ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Sphere.o
	$(C_COMPILER) $(ReleaseXP_Preprocessor_Definitions) $(ReleaseXP_Compiler_Flags) -MM ../../common/src/Math/Geometry/Bounding/Sphere.c $(ReleaseXP_Include_Path) > ../bin/gccReleaseXP/../../common/src/Math/Geometry/Bounding/Sphere.d

# Creates the intermediate and output folders for each configuration...
.PHONY: create_folders
create_folders:
	mkdir -p ../bin/gccDebug/source
	mkdir -p ../bin/gccRelease/source
	mkdir -p ../bin/gccDebugXP/source
	mkdir -p ../bin/gccReleaseXP/source

# Cleans intermediate and output files (objects, libraries, executables)...
.PHONY: clean
clean:
	rm -f ../bin/gccDebug/*.o
	rm -f ../bin/gccDebug/*.d
	rm -f ../bin/gccDebug/*.a
	rm -f ../bin/gccDebug/*.so
	rm -f ../bin/gccDebug/*.dll
	rm -f ../bin/gccDebug/*.exe
	rm -f ../bin/gccRelease/*.o
	rm -f ../bin/gccRelease/*.d
	rm -f ../bin/gccRelease/*.a
	rm -f ../bin/gccRelease/*.so
	rm -f ../bin/gccRelease/*.dll
	rm -f ../bin/gccRelease/*.exe
	rm -f ../bin/gccDebugXP/*.o
	rm -f ../bin/gccDebugXP/*.d
	rm -f ../bin/gccDebugXP/*.a
	rm -f ../bin/gccDebugXP/*.so
	rm -f ../bin/gccDebugXP/*.dll
	rm -f ../bin/gccDebugXP/*.exe
	rm -f ../bin/gccReleaseXP/*.o
	rm -f ../bin/gccReleaseXP/*.d
	rm -f ../bin/gccReleaseXP/*.a
	rm -f ../bin/gccReleaseXP/*.so
	rm -f ../bin/gccReleaseXP/*.dll
	rm -f ../bin/gccReleaseXP/*.exe

