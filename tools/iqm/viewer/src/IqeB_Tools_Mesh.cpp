/*
***********************************************************************************
 IqeB_Tools_Mesh.cpp

 Tools for working with meshes.

27.11.2014 RR: * First editon of this file.

*/

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/gl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

/************************************************************************************
 * IqeB_MeshFreeAllData()
 *
 * Free all data of a mesh.
 */

void IqeB_MeshFreeAllData( struct mesh *mesh)
{
  int i;

  for( i = 0; i < mesh->part_count; i++) {

    if( mesh->part[i].material > 0) {       // openGL has loaded

      unsigned int TempMaterial;

      TempMaterial = mesh->part[i].material;

      glDeleteTextures( 1, &TempMaterial);
    }

    mesh->part[i].material = -1;            // invalidate loaded texture

    // ...

    free( mesh->part[ i].pMeshName);
    free( mesh->part[ i].pMaterialStr);
    free( mesh->part[ i].pMaterialTags);
    free( mesh->part[ i].pMaterialBasedir);
    if( mesh->part[ i].pMaterialLoadedFile) free( mesh->part[ i].pMaterialLoadedFile);
  }

  // free mesh

	if( mesh->position)    free( mesh->position);
	if( mesh->normal)      free( mesh->normal);
	if( mesh->texcoord)    free( mesh->texcoord);
	if( mesh->color)       free( mesh->color);
	if( mesh->blendindex)  free( mesh->blendindex);
	if( mesh->blendweight) free( mesh->blendweight);
	if( mesh->element)     free( mesh->element);
	if( mesh->part)        free( mesh->part);
	if( mesh->aposition)   free( mesh->aposition);
	if( mesh->anormal)     free( mesh->anormal);
	if( mesh->blendColors) free( mesh->blendColors);
}

/************************************************************************************
 * IqeB_MeshComputeVertexNormals()
 *
 * Compute the vertex normals of the meshes.
 */

void IqeB_MeshComputeVertexNormals( struct mesh *mesh)
{
  int i, j;

  if( mesh->vertex_count <= 0) {  // have no vertexes

    return;
  }

  if( mesh->normal) {     // alredy have normals

    free( mesh->normal);  // free them
    mesh->normal = NULL;  // mark not allocated
  }

  // allocated vertexes and set
  mesh->normal = (float *)malloc( mesh->vertex_count * 3 * sizeof( float));

  if( mesh->normal == NULL) {   // security test, malloc failed

    return;
  }

  // set normals to 0
	for( i = 0; i < mesh->vertex_count; i++) {

    mesh->normal[ i * 3 + 0] = 0.0;
    mesh->normal[ i * 3 + 1] = 0.0;
    mesh->normal[ i * 3 + 2] = 0.0;
  }

  // walk all edges and sum up

	for( i = 0; i < mesh->part_count; i++) {

    for( j = 0; j <mesh->part[i].count; j += 3) {

      int i1, i2, i3;
      vec3 d1, d2, normal;

      i1 = mesh->element[ mesh->part[ i].first + j + 0];
      i2 = mesh->element[ mesh->part[ i].first + j + 1];
      i3 = mesh->element[ mesh->part[ i].first + j + 2];

      vec_sub( d1, mesh->position + i2 * 3, mesh->position + i1 * 3);
      vec_sub( d2, mesh->position + i3 * 3, mesh->position + i1 * 3);
      vec_cross( normal, d1, d2);
      vec_normalize( normal);

      vec_add_to( mesh->normal + i1 * 3, normal);
      vec_add_to( mesh->normal + i2 * 3, normal);
      vec_add_to( mesh->normal + i3 * 3, normal);
    }
	}

  // Normalize the normals
	for( i = 0; i < mesh->vertex_count; i++) {

    vec_normalize( mesh->normal + i * 3);
  }
}

/************************************************************************************
 * IqeB_MeshSmooth()
 *
 * Smooth the meshes.
 * Normals are recalculated
 *
 * mesh:            Mesh to smooth
 *
 * SmootValue:
 *    Smooth factor
 *    0.0  no smoooth, nothing is changed
 *    ...  linear interplation between no smooth and max. smooth
 *    1.0  maximal smooth.
 *
 * KeepPointedness:
 *    Keep pointedness of vertex
 *    0.0  don't test for pointedness, smooth all
 *    ...  keep the pointedness of '#vertexes * Pointedness' vertexes.
 *    1.0  keep all pointedness, nothing is changed
 *
 * PointednessFullSmooth:
 *    Interpolate smooth between NOT and argument SmootValue
 *    Only used for 'KeepPointedness > 0.0 and < 1.0 for the remaining
 *    vertexes (pointedness is modified).
 *    The remaining vertexes are sorted by their pointedness.
 *    0.0  all remaing vertexes get SmootValue
 *    ...  smooth factor is interpolated for 'remaing vertexes' * PointednessFullSmooth
 *    1.0  smooth factor is interpolated for all remaining vertexes
 *
 * Examples:
 *  SmootValue, KeepPointedness, PointednessFullSmooth
 *  1.0, 0.0, 0.0  : All vertexes are maximal smoothed
 *  0.2, 0.0, 0.0  : All vertexes are smoothed a bit
 *  1.0, 0.1, 0.0  : Keep 10 % of the most pointedness vertexes, rest is maximal smoothed
 *  0.5, 0.1, 0.3  : Keep 10 % of the most pointedness vertexes,
 *                   from the remaining vertexes 70 % are smoothed with factor
 *                   0.5 and 30 % are smoothed with factor 0.0 to 0.5 depenting
 *                   from their pointedness.
 */

// Collect some data about vertexes

typedef struct {

  int   iVertex;                // remeber vertex index (after qsort)
  int   nNeighbours;            // number of neighbour points
  vec3  GravCenter;             // Center of gravity of the neigbours
  float Pointedness;            // Value for pointedness of vertex point.
} T_IqeB_MeshSmooth_Vertex;

// help to sort T_IqeB_MeshSmooth_Vertex by pointedness

static int IqeB_MeshSmooth_compare( const void *arg1, const void *arg2 )
{
  T_IqeB_MeshSmooth_Vertex *p1, *p2;

  // sort accorting pointedness
  // highest values first

  p1 = (T_IqeB_MeshSmooth_Vertex *)arg1;
  p2 = (T_IqeB_MeshSmooth_Vertex *)arg2;

  if( p1->Pointedness == p2->Pointedness) {  // is equal

    return( 0);
  }

  if( p1->Pointedness > p2->Pointedness) {   // is greater than

    return( -1);
  }

  return( 1);   // must be less than
}

// ...

void IqeB_MeshSmooth( struct mesh *mesh, float SmootValue, float KeepPointedness,
                      float PointednessFullSmooth)
{
  T_IqeB_MeshSmooth_Vertex *pVertexHelper, *pVH1;
  int i, j;

  if( mesh->vertex_count <= 0) {  // have no vertexes

    return;
  }

  if( SmootValue <= 0.0) {        // argument says no smooth

    return;
  }

  if( SmootValue > 1.0) {         // clip to max. reasonable value

    SmootValue = 1.0;
  }

  if( KeepPointedness >= 1.0) {   // keep all pointedness, no smooth

    return;
  }

  if( mesh->normal == NULL) {     // Nave no normals

    IqeB_MeshComputeVertexNormals( mesh);  // allocate and compute normals
  }

  // allocate memory for vertex helper
  pVertexHelper = (T_IqeB_MeshSmooth_Vertex *)malloc( mesh->vertex_count * sizeof( T_IqeB_MeshSmooth_Vertex));
  if( pVertexHelper == NULL) {    // security test

    return;                       // panic exit
  }

  // Zero vertex helper data

  memset( pVertexHelper, 0, mesh->vertex_count * sizeof( T_IqeB_MeshSmooth_Vertex));

  // walk all edges and sum up the neighbours
  // NOTE: Its enough to sum up only on neighbour of a triangle point.
  //       The other point is a neightbour at the neighboured triangle
  //       (all triangles have the same winding).

	for( i = 0; i < mesh->part_count; i++) {

    for( j = 0; j <mesh->part[i].count; j += 3) {

      int i1, i2, i3;

      i1 = mesh->element[ mesh->part[ i].first + j + 0];
      i2 = mesh->element[ mesh->part[ i].first + j + 1];
      i3 = mesh->element[ mesh->part[ i].first + j + 2];

      pVH1 = pVertexHelper + i1;
      vec_add_to( pVH1->GravCenter, mesh->position + i2 * 3);
      pVH1->nNeighbours += 1;

      pVH1 = pVertexHelper + i2;
      vec_add_to( pVH1->GravCenter, mesh->position + i3 * 3);
      pVH1->nNeighbours += 1;

      pVH1 = pVertexHelper + i3;
      vec_add_to( pVH1->GravCenter, mesh->position + i1 * 3);
      pVH1->nNeighbours += 1;
    }
	}

  // Calculate gravity center

  pVH1 = pVertexHelper;
	for( i = 0; i < mesh->vertex_count; i++, pVH1++) {

    pVH1->iVertex = i;                     // remeber vertex index (after qsort)

    if( pVH1->nNeighbours > 1) {

      vec_scale( pVH1->GravCenter, pVH1->GravCenter, 1.0 / pVH1->nNeighbours );
    }
  }

  // Make the smooth

  if( KeepPointedness <= 0.0) {   // don't test for pointedness

    //
    // don't test for pointedness
    //

    if( SmootValue >= 1.0) {        // argument says maximal smooth

      // simply copy gravity center to point

      pVH1 = pVertexHelper;
  	  for( i = 0; i < mesh->vertex_count; i++, pVH1++) {

        if( pVH1->nNeighbours > 1) {

          vec_copy( mesh->position + i * 3, pVH1->GravCenter);
        }
      }
    } else {

      pVH1 = pVertexHelper;
  	  for( i = 0; i < mesh->vertex_count; i++, pVH1++) {

        // interpolate between vertex an center of gravity

        vec3 a1;

        if( pVH1->nNeighbours > 1) {

          vec_scale( a1, mesh->position + i * 3, 1.0 - SmootValue);
          vec_scale( mesh->position + i * 3, pVH1->GravCenter, SmootValue);
          vec_add_to( mesh->position + i * 3, a1);
        }
      }
    }
  } else {

    vec3 a1;
    float SmoothFactor, InterpolateHelper;
    int  iSmoothStart;

    //
    // take pointedness into account
    //

    pVH1 = pVertexHelper;
  	for( i = 0; i < mesh->vertex_count; i++, pVH1++) {

      if( pVH1->nNeighbours > 1) {

        // Pointedness is the distance between
        // * The vertex.
        // * The nearest point of the 'center of gravity' to a line give by
        //   the vertex and the normal.

        pVH1->Pointedness = PointDistNearestToLineBasePoint( mesh->position + i * 3, mesh->normal + i * 3, pVH1->GravCenter, NULL);
        if( pVH1->Pointedness < 0) {  // wand absolute value
          pVH1->Pointedness = - pVH1->Pointedness;
        }
      }
  	}

  	// sort accorting pointedness, highest values first

    qsort( pVertexHelper, mesh->vertex_count, sizeof( T_IqeB_MeshSmooth_Vertex),
           IqeB_MeshSmooth_compare);

    // where to start with smooth calculations

    iSmoothStart = (int)(mesh->vertex_count * KeepPointedness + 0.5);

    if( iSmoothStart >= mesh->vertex_count) {    // clipping
      iSmoothStart = mesh->vertex_count - 1;
    }

    if( iSmoothStart < 0) {    // clipping
      iSmoothStart = 0;
    }

    // where to start with full smooth

    j = (int)((mesh->vertex_count - iSmoothStart) * PointednessFullSmooth + 0.5); // factor of remainder
    j += iSmoothStart;                           // at start index


    if( j >= mesh->vertex_count) {    // clipping
      j = mesh->vertex_count - 1;
    }

    if( j < iSmoothStart) {    // clipping
      j = iSmoothStart;
    }

    if( j > iSmoothStart) {

      InterpolateHelper = 1.0 / (float)(j - iSmoothStart);
    } else {

      InterpolateHelper = 1.0;
    }

    // to the smoothing

    pVH1 = pVertexHelper + iSmoothStart;
  	for( i = iSmoothStart; i < mesh->vertex_count; i++, pVH1++) {

      if( pVH1->nNeighbours > 1) {

        // interpolate between vertex an center of gravity

        if( pVH1->nNeighbours > 1) {

          if( i >= j) {    // do full smoothing

            SmoothFactor = SmootValue;
          } else {

            // interpolate smoothing
            SmoothFactor = SmootValue * (i - iSmoothStart) * InterpolateHelper;
          }

          vec_scale( a1, mesh->position + pVH1->iVertex * 3, 1.0 - SmoothFactor);
          vec_scale( mesh->position + pVH1->iVertex * 3, pVH1->GravCenter, SmoothFactor);
          vec_add_to( mesh->position + pVH1->iVertex * 3, a1);
        }
      }
  	}
  }

  // free allocated data

  free( pVertexHelper);

  // must recompute vertex normals

  IqeB_MeshComputeVertexNormals( mesh);  // allocate and compute normals
}

/************************************************************************************
 * IqeB_MeshMeasureSize()
 *
 * Calculate min/max box of a mesh and returns
 * size and center of this box.
 *
 * return:   0: Done OK
 *         < 0: error
 *          -1: No vertex data
 */

int IqeB_MeshMeasureSize( struct mesh *pMesh, float center[3], float size[3])
{
	float minbox[3], maxbox[3];
	int i, nMinMax;

  // get min max box

  minbox[0] = minbox[1] = minbox[2] = 0;
  maxbox[0] = maxbox[1] = maxbox[2] = 0;
  nMinMax = 0;

  // vertex

	for (i = 0; i < pMesh->vertex_count; i++) {

    if( nMinMax == 0) {

  		vec_copy( minbox, pMesh->position + i * 3);
  		vec_copy( maxbox, pMesh->position + i * 3);
    } else {

      vec_minmax( minbox, maxbox, pMesh->position + i * 3);
    }

    nMinMax += 1;
  }

	center[0] = (minbox[0] + maxbox[0]) * 0.5;
	center[1] = (minbox[1] + maxbox[1]) * 0.5;
	center[2] = (minbox[2] + maxbox[2]) * 0.5;

	size[0] = maxbox[0] - minbox[0];
	size[1] = maxbox[1] - minbox[1];
	size[2] = maxbox[2] - minbox[2];

  return( nMinMax > 0 ? 0 : -1);  // Got some vertexes ?
}

/****************************** End Of File ******************************/
