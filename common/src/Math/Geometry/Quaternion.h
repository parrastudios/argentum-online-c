/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	MATH_GEOMETRY_QUATERNION_H
#define MATH_GEOMETRY_QUATERNION_H

////////////////////////////////////////////////////////////
/// References:
///  [1] GPWiki - OpenGL Tutorials: Using Quaternions
///  [2] A.J.'s Computer Science Page
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Vector.h>
#include <Math/Geometry/Matrix.h>

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct Matrix4f;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct Quaternion
{
	float w, x, y, z;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////
void  QuaternionSet(struct Quaternion * Quat, float x, float y, float z, float w);
void  QuaternionCopy(struct Quaternion * Dest, struct Quaternion * Source);
bool  QuaternionIsEqual(struct Quaternion * Left, struct Quaternion * Right);
void  QuaternionIdentity(struct Quaternion * Quat);
void  QuaternionConjugate(struct Quaternion * Src, struct Quaternion * Dest);
void  QuaternionInverse(struct Quaternion * Quat);
float QuaternionDotProduct(struct Quaternion * Q1, struct Quaternion * Q2);
float QuaternionMagnitude(struct Quaternion * Quat);
void  QuaternionComputeW(struct Quaternion * Quat);
void  QuaternionNormalize(struct Quaternion * Quat);
void  QuaternionMultQuaternion(struct Quaternion * Q1, struct Quaternion * Q2, struct Quaternion * Result);
void  QuaternionMultQuaternionEx(struct Quaternion * Q1, struct Quaternion * Q2, struct Quaternion * Result);
void  QuaternionMultVector3f(struct Quaternion * Quat, struct Vector3f * Vec, struct Quaternion * Result);
void  QuaternionRotatePoint(struct Quaternion * Quat, struct Vector3f * Src, struct Vector3f * Dest);
void  QuaternionSlerp(struct Quaternion * Q1, struct Quaternion * Q2, float Delta, struct Quaternion * Result);
void  QuaternionNlerp(struct Quaternion * Q1, struct Quaternion * Q2, float Delta, struct Quaternion * Result);
void  QuaternionIntegrate(struct Quaternion * Quat, struct Vector3f * Omega, float Delta, struct Quaternion * Result);
void  QuaternionToMatrix(struct Quaternion * Quat, struct Matrix4f * Matrix);
void  QuaternionFromMatrix(struct Quaternion * Quat, struct Matrix4f * Matrix);
void  QuaternionToAxisAngle(struct Quaternion * Quat, struct Vector3f * Axis, float * Angle);
void  QuaternionFromAxisAngle(struct Quaternion * Quat, struct Vector3f * Axis, float Angle);
void  QuaternionFromEuler(struct Quaternion * Quat, float Pitch, float Yaw, float Roll);
void  QuaternionToEuler(struct Quaternion * Quat, struct Vector3f * Euler);
void  QuaternionFromEulerEx(struct Quaternion * Quat, struct Matrix4f * Matrix, struct Vector3f * EulerRotation, bool Constrained);

#endif // MATH_GEOMETRY_QUATERNION_H
