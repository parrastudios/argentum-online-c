/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2016 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/Default.h>
#include <DataBase/StoredProcedure.h>

#include <System/IOHelper.h> // todo: remove this

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define HELLO_WORLD_NAME_LENGTH		UINTEGER_SUFFIX(0xFF)
#define HELLO_WORLD_RESULT_LENGTH	UINTEGER_SUFFIX(0xFF)
#define ACCOUNT_NAME_LENGTH			UINTEGER_SUFFIX(0xFF)
#define PLAYER_NAME_LENGTH			UINTEGER_SUFFIX(0xFF)
#define PLAYER_DESC_LENGTH			UINTEGER_SUFFIX(0xFF)

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct StoredProcedureHelloWorldArgType
{
	char Name[HELLO_WORLD_NAME_LENGTH];
	char Result[HELLO_WORLD_RESULT_LENGTH];
};

struct StoredProcedureTestArgType
{
	int32 In;
	int32 Out;
	int32 InOut;
};

struct StoredProcedureAccountArgType
{
	char Name[ACCOUNT_NAME_LENGTH];
};

struct StoredProcedureResultPlayerType
{
	char Name[PLAYER_NAME_LENGTH];
	char Desc[PLAYER_DESC_LENGTH];
	int64 Level;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Test in, out and inout parameters
////////////////////////////////////////////////////////////
void TestInOutInOutParameters()
{
	char const ProcedureName[] = "TestInOutInOutParameters";

	printf("\nTest official doc function\n");

	// Destroy a stored procedure from the database
	if (StoredProcedureDestroy(ProcedureName))
	{
		printf("Stored procedure %s already exists, destroyed successfully\n", ProcedureName);
	}

	// Create a stored procedure in the database
	if (StoredProcedureCreate(ProcedureName, "IN p_in INT, OUT p_out INT, INOUT p_inout INT",
		"SELECT p_in, p_out, p_inout; "
		"SET p_in = 100, p_out = 200, p_inout = 300; "
		"SELECT p_in, p_out, p_inout; "))
	{
		struct StoredProcedureType StoredProcedure;

		printf("Stored procedure %s created successfully\n", ProcedureName);

		if (StoredProcedureInitialize(&StoredProcedure, ProcedureName, 3, NULL))
		{
			struct StoredProcedureTestArgType Arguments;

			// Set values
			Arguments.In = 123;
			Arguments.Out = 0;
			Arguments.InOut = 456;

			// Bind stored procedure arguments
			StoredProcedurePrototypeBind(&StoredProcedure,
				(STORED_PROCEDURE_LONG, &Arguments.In),
				(STORED_PROCEDURE_LONG, &Arguments.Out),
				(STORED_PROCEDURE_LONG, &Arguments.InOut));

			// Make the call
			if (StoredProcedureCall(&StoredProcedure))
			{
				printf("Stored procedure %s called successfully\n", ProcedureName);

				// Print arguments
				printf("In: %d, InOut: %d, Out: %d\n", Arguments.In, Arguments.Out, Arguments.InOut);
			}

			// Release the stored procedure
			StoredProcedureRelease(&StoredProcedure);
		}

		// Destroy a stored procedure from the database
		if (StoredProcedureDestroy(ProcedureName))
		{
			printf("Stored procedure %s destroyed successfully\n", ProcedureName);
		}
	}
}

////////////////////////////////////////////////////////////
/// Test multiple fetch results
////////////////////////////////////////////////////////////
void TestMultipleFetch()
{
	char const ProcedureName[] = "TestMultipleFetchResults";

	printf("\nTest multiple fetch results function\n");

	// Destroy a stored procedure from the database
	if (StoredProcedureDestroy(ProcedureName))
	{
		printf("Stored procedure %s already exists, destroyed successfully\n", ProcedureName);
	}

	// Create a stored procedure in the database
	if (StoredProcedureCreate(ProcedureName, "IN p_in INT, OUT p_out INT, INOUT p_inout INT",
		"SELECT 1 as one, 2 as two, 3 as three "
		"UNION ALL SELECT 4 as four, 5 as five, 6 as six "
		"UNION ALL SELECT 7 as seven, 8 as eight, 9 as nine;"))
	{
		struct StoredProcedureType StoredProcedure;

		printf("Stored procedure %s created successfully\n", ProcedureName);

		if (StoredProcedureInitialize(&StoredProcedure, ProcedureName, 3, NULL))
		{
			struct StoredProcedureTestArgType Arguments;

			// Set values
			Arguments.In = 123;
			Arguments.Out = 0;
			Arguments.InOut = 456;

			// Bind stored procedure arguments
			StoredProcedurePrototypeBind(&StoredProcedure,
				(STORED_PROCEDURE_LONG, &Arguments.In),
				(STORED_PROCEDURE_LONG, &Arguments.Out),
				(STORED_PROCEDURE_LONG, &Arguments.InOut));

			// Make the call
			if (StoredProcedureCall(&StoredProcedure))
			{
				printf("Stored procedure %s called successfully\n", ProcedureName);

				// Print arguments
				printf("In: %d, InOut: %d, Out: %d\n", Arguments.In, Arguments.Out, Arguments.InOut);
			}

			// Release the stored procedure
			StoredProcedureRelease(&StoredProcedure);
		}

		// Destroy a stored procedure from the database
		if (StoredProcedureDestroy(ProcedureName))
		{
			printf("Stored procedure %s destroyed successfully\n", ProcedureName);
		}
	}
}

////////////////////////////////////////////////////////////
/// Test string results
////////////////////////////////////////////////////////////
void TestStringResultOnly()
{
	char const ProcedureName[] = "TestStringResultOnly";

	printf("\nTest string function\n");

	// Destroy a stored procedure from the database
	if (StoredProcedureDestroy(ProcedureName))
	{
		printf("Stored procedure %s already exists, destroyed successfully\n", ProcedureName);
	}

	// Create a stored procedure in the database
	if (StoredProcedureCreate(ProcedureName, "IN number INT",
		"DECLARE result CHAR(255); "
		"SELECT CONCAT('Hello ', number) INTO result; "
		"SELECT result; "))
	{
		struct StoredProcedureType StoredProcedure;

		printf("Stored procedure %s created successfully\n", ProcedureName);

		if (StoredProcedureInitialize(&StoredProcedure, ProcedureName, 1, NULL))
		{
			// Set value
			int32 Number = 1234;

			// Bind stored procedure arguments
			StoredProcedurePrototypeBind(&StoredProcedure,
				(STORED_PROCEDURE_LONG, &Number));

			// Make the call
			if (StoredProcedureCall(&StoredProcedure))
			{
				printf("Stored procedure %s called successfully\n", ProcedureName);

				// Print arguments
				printf("Number: %d\n", Number);
			}

			// Set new value for prev binding
			Number = 4321;

			// Make a second call
			if (StoredProcedureCall(&StoredProcedure))
			{
				printf("Stored procedure %s called successfully\n", ProcedureName);

				// Print arguments
				printf("Number: %d\n", Number);
			}

			// Release the stored procedure
			StoredProcedureRelease(&StoredProcedure);
		}

		// Destroy a stored procedure from the database
		if (StoredProcedureDestroy(ProcedureName))
		{
			printf("Stored procedure %s destroyed successfully\n", ProcedureName);
		}
	}
}

////////////////////////////////////////////////////////////
/// Test string arguments and results
////////////////////////////////////////////////////////////
void TestStrings()
{
	char const ProcedureName[] = "TestStrings";

	printf("\nTest string function\n");

	// Destroy a stored procedure from the database
	if (StoredProcedureDestroy(ProcedureName))
	{
		printf("Stored procedure %s already exists, destroyed successfully\n", ProcedureName);
	}

	// Create a stored procedure in the database
	if (StoredProcedureCreate(ProcedureName, "IN name CHAR(255), INOUT result CHAR(255)",
		"SELECT name INTO result; "
		"SELECT result; "))
	{
		struct StoredProcedureType StoredProcedure;

		printf("Stored procedure %s created successfully\n", ProcedureName);

		if (StoredProcedureInitialize(&StoredProcedure, ProcedureName, 2, NULL))
		{
			struct StoredProcedureHelloWorldArgType Arguments;

			// Copy name
			strcpy(Arguments.Name, "Pepito");
			strcpy(Arguments.Result, "Nothing");

			// Bind stored procedure arguments
			StoredProcedurePrototypeBind(&StoredProcedure,
				(STORED_PROCEDURE_STRING, Arguments.Name),
				(STORED_PROCEDURE_STRING, Arguments.Result));

			// Make the call
			if (StoredProcedureCall(&StoredProcedure))
			{
				printf("Stored procedure %s called successfully\n", ProcedureName);

				// Print arguments
				printf("Name: %s, Result: %s\n", Arguments.Name, Arguments.Result);
			}

			// Release the stored procedure
			StoredProcedureRelease(&StoredProcedure);
		}

		// Destroy a stored procedure from the database
		if (StoredProcedureDestroy(ProcedureName))
		{
			printf("Stored procedure %s destroyed successfully\n", ProcedureName);
		}
	}
}

////////////////////////////////////////////////////////////
/// Test a simple but complete example player create
////////////////////////////////////////////////////////////
void StoredProcedureResultPlayerCreate(struct StoredProcedureResultPlayerType * Player, char * Name, char * Desc, int64 Level)
{
	if (Player && Name && Desc)
	{
		// Copy player data
		strcpy(Player->Name, Name);
		strcpy(Player->Desc, Desc);
		Player->Level = Level;
	}
}

////////////////////////////////////////////////////////////
/// Test a simple but complete example player print
////////////////////////////////////////////////////////////
void StoredProcedureResultPlayerPrint(struct StoredProcedureResultPlayerType * Player)
{
	if (Player)
	{
		// Print the player
		printf("Player %s [Description: %s] - Level %d\n", Player->Name, Player->Desc, Player->Level);
	}
}

////////////////////////////////////////////////////////////
/// Test a simple but complete example fetch callback
////////////////////////////////////////////////////////////
void TestSimpleCompleteExampleFetchCallback(struct StoredProcedureArgumentBuilder * BuilderList, uinteger Size)
{
	if (BuilderList && Size > UINTEGER_SUFFIX(0))
	{
		uinteger Result;

		for (Result = 0; Result < Size; ++Result)
		{
			char * Name; uinteger NameLength;
			char * Desc; uinteger DescLength;
			int64 Level;

			struct StoredProcedureResultPlayerType Player;

			StoredProcedureArgumentList Arguments = BuilderList[Result].Arguments;

			// Retreive result set
			StoredProcedureArgumentGetBuildValue(Arguments,
				(STORED_PROCEDURE_STRING, Name, NameLength),
				(STORED_PROCEDURE_STRING, Desc, DescLength),
				(STORED_PROCEDURE_LONG_LONG, Level)
			);

			// Create the player
			StoredProcedureResultPlayerCreate(&Player, Name, Desc, Level);

			// Print player
			StoredProcedureResultPlayerPrint(&Player);

			// Insert into player manager
			// VectorPushBack(PlayerList, &Player);
		}
	}
}

////////////////////////////////////////////////////////////
/// Test a simple but complete example
////////////////////////////////////////////////////////////
void TestSimpleCompleteExample()
{
	char const ProcedureName[] = "TestSimpleCompleteExample";

	printf("\nTest a simple but complete example function\n");

	// Destroy a stored procedure from the database
	if (StoredProcedureDestroy(ProcedureName))
	{
		printf("Stored procedure %s already exists, destroyed successfully\n", ProcedureName);
	}

	// Create a stored procedure in the database
	if (StoredProcedureCreate(ProcedureName, "IN account_name CHAR(255)",
		"SELECT 'Parra' as player_name, 'They are fucking with me subliminally' as player_desc, 23 as player_level "
		"UNION ALL SELECT 'Gil' as player_name, 'Gonzalo�s Meme Emulator' as player_desc, 29 as player_level "
		"UNION ALL SELECT 'Hell0wner' as player_name, 'Get rekt' as player_desc, 1 as player_level;"))
	{
		struct StoredProcedureType StoredProcedure;

		printf("Stored procedure %s created successfully\n", ProcedureName);

		if (StoredProcedureInitialize(&StoredProcedure, ProcedureName, 1, &TestSimpleCompleteExampleFetchCallback))
		{
			struct StoredProcedureAccountArgType Account;

			// Set account name
			strcpy(Account.Name, "noob_legion");

			// Bind stored procedure arguments
			StoredProcedurePrototypeBind(&StoredProcedure,
				(STORED_PROCEDURE_STRING, Account.Name));

			// Make the call
			if (StoredProcedureCall(&StoredProcedure))
			{
				printf("Stored procedure %s called successfully\n", ProcedureName);
			}

			// Release the stored procedure
			StoredProcedureRelease(&StoredProcedure);
		}

		// Destroy a stored procedure from the database
		if (StoredProcedureDestroy(ProcedureName))
		{
			printf("Stored procedure %s destroyed successfully\n", ProcedureName);
		}
	}
}

////////////////////////////////////////////////////////////
/// Main entry point
////////////////////////////////////////////////////////////
int main(int argc, char * argv[])
{
	printf("Stored Procedure Test\n\n");

	if (DataBaseInitialize())
    {
		printf("Connected to database successfully\n\n");

		// Test in, out, inout parameters
		TestInOutInOutParameters();

		// Test multiple fetch results
		TestMultipleFetch();

		// Test string results
		TestStringResultOnly();

		// Test string arguments and results
		TestStrings();

		// Test a simple but complete example
		TestSimpleCompleteExample();

        if (!DataBaseDestroy())
        {
            printf("Error when destroying database\n");
        }
    }

	return 0;
}
