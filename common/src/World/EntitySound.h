/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_ENTITY_SOUND_H
#define WORLD_ENTITY_SOUND_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Component.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define ENTITY_SOUND_INDEX_INVALID		UINTEGER_MAX_RANGE

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct EntityType;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Sound component base data
////////////////////////////////////////////////////////////
struct ComponentSoundData
{
	uinteger					Index;
	bool						Loop;
	struct Vector3f				Origin;
	char						Path[0xFF]; // todo: refactor this
};

////////////////////////////////////////////////////////////
/// Sound base component type
////////////////////////////////////////////////////////////
struct ComponentSoundType
{
	//< Play sound of component
	void (*Play)(struct EntityType * Entity);

	//< Stop sound of component
	void (*Stop)(struct EntityType * Entity);

	//< Set a new stream sound to the component
	void (*Stream)(struct EntityType * Entity, uinteger Index);

	//< Set loop property of sound component
	void (*Loop)(struct EntityType * Entity, bool Loop);

	//< Set origin of sound component
	void (*Origin)(struct EntityType * Entity, struct Vector3f * Position);
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a sound component
////////////////////////////////////////////////////////////
struct EntityType * EntitySoundCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a sound component
////////////////////////////////////////////////////////////
void EntitySoundDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Return the entity sound implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentSoundType * EntityComponentSoundAttr();

////////////////////////////////////////////////////////////
/// Return the entity sound implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentSoundImpl();

#endif // WORLD_ENTITY_SOUND_H
