/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GUI_WINDOW_MANAGER_H
#define GUI_WINDOW_MANAGER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <GUI/WindowObj.h>
#include <GUI/EventHandler.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct GuiWindowList
{
	struct GuiWindow * First;
	struct GuiWindow * Last;
	struct GuiWindow * Focused;
};

////////////////////////////////////////////////////////////
/// Initialize the windows
////////////////////////////////////////////////////////////
void GuiInitialize(uinteger Template, char * Path);

////////////////////////////////////////////////////////////
/// Check if keyboard is being used on GUI
////////////////////////////////////////////////////////////
bool GuiKeyboardAvailable();

////////////////////////////////////////////////////////////
// Add window to the list
////////////////////////////////////////////////////////////
void GuiAddWindow(struct GuiWindow * Window);

////////////////////////////////////////////////////////////
/// Get a window pointer by id
////////////////////////////////////////////////////////////
struct GuiWindow * GuiGetWindow(char * WindowID);

////////////////////////////////////////////////////////////
// Remove window from the list
////////////////////////////////////////////////////////////
void GuiRemoveWindow(struct GuiWindow * Window);

////////////////////////////////////////////////////////////
/// Open the window
////////////////////////////////////////////////////////////
void GuiOpenWindow(struct GuiWindow * Window);

////////////////////////////////////////////////////////////
/// Load the window on the list
////////////////////////////////////////////////////////////
void GuiLoadWindow(struct GuiWindow * Window);

////////////////////////////////////////////////////////////
/// Close the window
////////////////////////////////////////////////////////////
void GuiCloseWindow(struct GuiWindow * Window);

////////////////////////////////////////////////////////////
/// Lost focus of a window
////////////////////////////////////////////////////////////
void GuiLostFocusWindow(struct GuiWindow * Window);

////////////////////////////////////////////////////////////
/// Lost focus of all window
////////////////////////////////////////////////////////////
void GuiLostFocusAll();

////////////////////////////////////////////////////////////
// Render all gui (renderer)
////////////////////////////////////////////////////////////
void GuiRender();

////////////////////////////////////////////////////////////
// Update all gui (event handler)
////////////////////////////////////////////////////////////
void GuiUpdate(struct Event * EventData);

////////////////////////////////////////////////////////////
/// Destroy all windows
////////////////////////////////////////////////////////////
void GuiDestroy();

#endif // GUI_WINDOW_MANAGER_H
