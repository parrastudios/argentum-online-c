Include files are distributed as it would be distributed under AGLP v3 License.

This is only applied for the game Argentum Online C, which nowadays it's also released under Apache 2.0 License.

The game engine is private and it's not distributed, but headers are automatically included when code is developed.

In the future, the game enigne will be separated from game and it will be distributed under private license.
