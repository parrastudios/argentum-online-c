/****************************************************************************

  IqeB_GUI_ToolsMeshTransform.cpp

  07.02.2015 RR: First editon of this file.


*****************************************************************************
*/

#include <math.h>

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Button.H>

/************************************************************************************
 * Globals
 *
 */

static float IqeB_ToolsMesh_ScaleFactor =  1.0;   // scale factor
static float IqeB_ToolsMesh_ScaleToSize = 10.0;   // scale to this size

static float IqeB_ToolsMesh_AlignX =  0.0;        // Align X coordiante to this
static float IqeB_ToolsMesh_AlignY =  0.0;        // Align Y coordiante to this
static float IqeB_ToolsMesh_AlignZ =  0.0;        // Align Z coordiante to this

static float IqeB_ToolsMesh_RotateX = 90.0;        // Rotate around X axis
static float IqeB_ToolsMesh_RotateY = 90.0;        // Rotate around Y axis
static float IqeB_ToolsMesh_RotateZ = 90.0;        // Rotate around T axis

/************************************************************************************
 * Statics
 */

static  Fl_Window *pMyToolWin;
static int MyWinPosX = IQE_GUI_NO_WINPOS_X, MyWinPosY = IQE_GUI_NO_WINPOS_Y; // last window position

/************************************************************************************
 * IqeB_GUI_Button_Callback
 */

#define ACTION_CODE_SCALE_FACTOR       0  // scale model by factor
#define ACTION_CODE_SCALE_TO_SIZE      1  // scale model to given size

#define ACTION_CODE_ALIGN_MESH_X       2  // Allign mesh X coordinate
#define ACTION_CODE_ALIGN_MESH_Y       5  // Allign mesh Y coordinate
#define ACTION_CODE_ALIGN_MESH_Z       8  // Allign mesh Z coordinate
#define ACTION_CODE_ALIGN_MESH_MIN     0  // to mesh minimum, add to ACTION_CODE_ALIGN_MESH_X/Y/Z
#define ACTION_CODE_ALIGN_MESH_CENTER  1  // to mesh center, add to ACTION_CODE_ALIGN_MESH_X/Y/Z
#define ACTION_CODE_ALIGN_MESH_MAX     2  // to mesh maximum, add to ACTION_CODE_ALIGN_MESH_X/Y/Z

#define ACTION_CODE_ROTATE_MESH_X     11  // Rotate around X axis
#define ACTION_CODE_ROTATE_MESH_Y     13  // Rotate around Y axis
#define ACTION_CODE_ROTATE_MESH_Z     15  // Rotate around Z axis
#define ACTION_CODE_ROTATE_DIR_NEG     0  // rotate negative direction, add to ACTION_CODE_ROTATE_MESH_X/Y/Z
#define ACTION_CODE_ROTATE_DIR_POS     1  // rotate positive direction, add to ACTION_CODE_ROTATE_MESH_X/Y/Z

static void IqeB_GUI_Button_Callback( Fl_Widget *w, void *ActionCodeArg)
{
  int ActionCode;

  if( pDrawModel == NULL) {              // have no draw model

    return;                              // nothing to do
  }

  ActionCode = (int)ActionCodeArg;       // get action code

  switch( ActionCode) {
  case ACTION_CODE_SCALE_FACTOR:  // scale model by factor

    IqeB_ModelSizeScale( pDrawModel, IqeB_ToolsMesh_ScaleFactor);

    IqeB_DispViewAdaptFromModel( pDrawModel);   // update
    break;

  case ACTION_CODE_SCALE_TO_SIZE:  // scale model to given size
    {                     // have a model
      vec3 ObjSize;
      float ScaleFactor;

      IqeB_DispModelGetInfo( ObjSize, NULL);

      if( ObjSize[ 2] < 0.0001) {      // security test, keep a reasonable minimum

        ObjSize[ 2] = 0.0001;
      }

      ScaleFactor = IqeB_ToolsMesh_ScaleToSize / ObjSize[ 2];

      IqeB_ModelSizeScale( pDrawModel, ScaleFactor);

      IqeB_DispViewAdaptFromModel( pDrawModel);   // update
    }
    break;

  case ACTION_CODE_ALIGN_MESH_X + ACTION_CODE_ALIGN_MESH_MIN:    // align mesh
  case ACTION_CODE_ALIGN_MESH_X + ACTION_CODE_ALIGN_MESH_CENTER:
  case ACTION_CODE_ALIGN_MESH_X + ACTION_CODE_ALIGN_MESH_MAX:
  case ACTION_CODE_ALIGN_MESH_Y + ACTION_CODE_ALIGN_MESH_MIN:
  case ACTION_CODE_ALIGN_MESH_Y + ACTION_CODE_ALIGN_MESH_CENTER:
  case ACTION_CODE_ALIGN_MESH_Y + ACTION_CODE_ALIGN_MESH_MAX:
  case ACTION_CODE_ALIGN_MESH_Z + ACTION_CODE_ALIGN_MESH_MIN:
  case ACTION_CODE_ALIGN_MESH_Z + ACTION_CODE_ALIGN_MESH_CENTER:
  case ACTION_CODE_ALIGN_MESH_Z + ACTION_CODE_ALIGN_MESH_MAX:
    {                     // have a model
      int iCoord;
      vec3 ObjSize, ObjCenter, Translate, AddTo;

      if( ActionCode >= ACTION_CODE_ALIGN_MESH_Z) {

        iCoord = 2;  // is Z coordinate

      } else if( ActionCode >= ACTION_CODE_ALIGN_MESH_Y) {

        iCoord = 1;  // is Y coordinate

      } else {

        iCoord = 0;  // must be X coordinate
      }

      IqeB_DispModelGetInfo( ObjSize, ObjCenter);

      vec_set( Translate, IqeB_ToolsMesh_AlignX, IqeB_ToolsMesh_AlignY, IqeB_ToolsMesh_AlignZ);
      vec_set( AddTo, 0.0, 0.0, 0.0);

      if( (ActionCode - ACTION_CODE_ALIGN_MESH_X) % 3 == ACTION_CODE_ALIGN_MESH_MIN) {

        AddTo[ iCoord] = Translate[ iCoord] - ObjCenter[ iCoord] + ObjSize[ iCoord] * 0.5;

      } else if( (ActionCode - ACTION_CODE_ALIGN_MESH_X) % 3 == ACTION_CODE_ALIGN_MESH_CENTER) {

        AddTo[ iCoord] = Translate[ iCoord] - ObjCenter[ iCoord];

      } else {  // must be ACTION_CODE_ALIGN_MESH_MAX

        AddTo[ iCoord] = Translate[ iCoord] - ObjCenter[ iCoord] - ObjSize[ iCoord] * 0.5;
      }

      IqeB_ModelTranslate( pDrawModel, AddTo);

      IqeB_DispViewAdaptFromModel( pDrawModel);   // update
    }
    break;

  case ACTION_CODE_ROTATE_MESH_X + ACTION_CODE_ROTATE_DIR_NEG:    // rotate around axis
  case ACTION_CODE_ROTATE_MESH_X + ACTION_CODE_ROTATE_DIR_POS:
  case ACTION_CODE_ROTATE_MESH_Y + ACTION_CODE_ROTATE_DIR_NEG:
  case ACTION_CODE_ROTATE_MESH_Y + ACTION_CODE_ROTATE_DIR_POS:
  case ACTION_CODE_ROTATE_MESH_Z + ACTION_CODE_ROTATE_DIR_NEG:
  case ACTION_CODE_ROTATE_MESH_Z + ACTION_CODE_ROTATE_DIR_POS:
    {                     // have a model
      int iCoord;
      vec3 RotateArg, RotateBy;
      vec4 RotateQuad;

      if( ActionCode >= ACTION_CODE_ROTATE_MESH_Z) {

        iCoord = 2;  // is Z coordinate

      } else if( ActionCode >= ACTION_CODE_ROTATE_MESH_Y) {

        iCoord = 1;  // is Y coordinate

      } else {

        iCoord = 0;  // must be X coordinate
      }

      vec_set( RotateArg, IqeB_ToolsMesh_RotateX, IqeB_ToolsMesh_RotateY, IqeB_ToolsMesh_RotateZ);
      vec_set( RotateBy, 0.0, 0.0, 0.0);

      if( (ActionCode - ACTION_CODE_ROTATE_MESH_X) % 2 == ACTION_CODE_ROTATE_DIR_NEG) {

        RotateBy[ iCoord] = - RotateArg[ iCoord] * M_PI / 180.0;   // degree to radians

      } else {  // must be ACTION_CODE_ROTATE_DIR_POS

        RotateBy[ iCoord] = RotateArg[ iCoord] * M_PI / 180.0;   // degree to radians
      }

      quat_from_rotation( RotateQuad, RotateBy);

      IqeB_ModelRotate( pDrawModel, RotateQuad);

      IqeB_DispViewAdaptFromModel( pDrawModel);   // update
    }
    break;

  } // switch

  // Animation data has changed, some GUI's must update

  IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_JOINT_CHANGED | IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
}

/************************************************************************************
 * IqeB_GUI_CBox_SetValue_Callback
 */

static void IqeB_GUI_Float_SetValue_Callback( Fl_Widget *w, void *pValueArg)
{
  float *pValue;
  Fl_Float_Input *pThis;

  // ...

  pThis  = (Fl_Float_Input *)w;
  pValue = (float *)pValueArg;             // get pointer to associated variable

  *pValue = atof( pThis->value());         // update the variable
}

/************************************************************************************
 * close_cb, close this window
 *
 * pValueArg is a pointer to the widget. Set this pointer to NULL on deletion.
 */

static void close_cb( Fl_Widget *w, void *pValueArg)
{

  IqeB_GUI_CloseToolWindow( (void **)&pMyToolWin);
}

/************************************************************************************
 * Presets for this tools window
 *
 */

 static T_GUI_PreferenceEntry MyPreferences[] =
 {

  // Cube display

  { PREF_T_FLOAT, "MeshScaleFactor",  "1.0", &IqeB_ToolsMesh_ScaleFactor},
  { PREF_T_FLOAT, "MeshScaleToSize", "20.0", &IqeB_ToolsMesh_ScaleToSize},
  { PREF_T_FLOAT, "MeshAlignX",       "0.0", &IqeB_ToolsMesh_AlignX},
  { PREF_T_FLOAT, "MeshAlignY",       "0.0", &IqeB_ToolsMesh_AlignY},
  { PREF_T_FLOAT, "MeshAlignZ",       "0.0", &IqeB_ToolsMesh_AlignZ},
  { PREF_T_FLOAT, "MeshRotateX",     "90.0", &IqeB_ToolsMesh_RotateX},
  { PREF_T_FLOAT, "MeshRotateY",     "90.0", &IqeB_ToolsMesh_RotateY},
  { PREF_T_FLOAT, "MeshRotateZ",     "90.0", &IqeB_ToolsMesh_RotateZ},
};

// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "ToolsMeshTransform", MyPreferences, sizeof( MyPreferences) / sizeof( T_GUI_PreferenceEntry),
                                               (void **)(&pMyToolWin), &MyWinPosX, &MyWinPosY);

/************************************************************************************
 * IqeB_GUI_ToolsMeshTransformWin
 *
 * Open dialog
 */

void IqeB_GUI_ToolsMeshTransformWin( int xLeft, int xRight, int yTop, int yBotton)
{

  //
  // winodow already created --> show it
  //

  if( pMyToolWin != NULL) {         // already have tool window

    // Show invisble window or bring visible window to foreground

    pMyToolWin->show();          // show it

    return;
  }

  //
  // creation of window on first call
  //

  if( pMyToolWin == NULL) {  // no tool window until now

    int xPos, yPos;

    xPos = xRight;
    yPos = yTop;

    if( MyWinPosX != IQE_GUI_NO_WINPOS_X && MyWinPosY != IQE_GUI_NO_WINPOS_Y) { // have last window position

      xPos = MyWinPosX;
      yPos = MyWinPosY;
    }

    pMyToolWin = new Fl_Window( xPos, yPos, IQE_GUI_TOOLS_STD_WITDH, 160, "Mesh/Transform");

    if( pMyToolWin == NULL) {  // security test

      return;
    }
  }

  //
  //  GUI things
  //

  int x1, x2, xc, xx1, xx2, xxTemp, y, yy, yGroup;
  char TempBuffer[ 256];

  //x/Fl_Check_Button *pTemp_Check_Button;
  Fl_Float_Input  *pTemp_Float_Input;
  Fl_Box          *pTemp_Box;
  Fl_Tabs         *pTemp_Tabs;
  Fl_Group        *pTemp_Group;
  Fl_Button       *pTemp_Button;

  x1  = 4;
  x2  = 8;
  xx1 = pMyToolWin->w() - 16;
  xx2 = xx1 / 2;
  xc  = pMyToolWin->w() / 2;          // x center
  yy  = 30;

  y = 4;

  //
  // Tabs
  //

  pTemp_Tabs = new Fl_Tabs( x1, y, pMyToolWin->w() - x1 - 4, pMyToolWin->h() - y - 4);
  pTemp_Tabs->selection_color( Fl::get_color( FL_SELECTION_COLOR));

  y += 26;

  //
  // Group size
  //

  yGroup = y;

  pTemp_Group = new Fl_Group( x1, y, pMyToolWin->w() - x1 - 4, pMyToolWin->h() - y - 4, "Size");

    // Size change by factor

    y += 4;
    y += 4;

    pTemp_Box = new Fl_Box( x2, y, xx1, 2, "Scale by factor");
    pTemp_Box->align( FL_ALIGN_TOP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
    pTemp_Box->box( FL_DOWN_FRAME);

    y += 20;

    pTemp_Float_Input = new Fl_Float_Input( x2, y, xx2, yy);
    pTemp_Float_Input->type( FL_FLOAT_INPUT);
    pTemp_Float_Input->tooltip( "Change model sizes by this factor");
    sprintf( TempBuffer, "%.2f", IqeB_ToolsMesh_ScaleFactor);
    pTemp_Float_Input->value( TempBuffer);
    pTemp_Float_Input->callback( IqeB_GUI_Float_SetValue_Callback, &IqeB_ToolsMesh_ScaleFactor);

    pTemp_Button = new Fl_Button( xc + 8, y, xx2 - 8, yy, "Do it");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)ACTION_CODE_SCALE_FACTOR);
    pTemp_Button->tooltip( "Do the size change");

    y += yy;

    // Size change to given size

    y += 4;

    pTemp_Box = new Fl_Box( x2, y, xx1, 2, "Scale to size");
    pTemp_Box->align( FL_ALIGN_TOP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
    pTemp_Box->box( FL_DOWN_FRAME);

    y += 20;

    pTemp_Float_Input = new Fl_Float_Input( x2, y, xx2, yy);
    pTemp_Float_Input->type( FL_FLOAT_INPUT);
    pTemp_Float_Input->tooltip( "Change the model height (Zaxis size)\nto the specified value.");
    sprintf( TempBuffer, "%.2f", IqeB_ToolsMesh_ScaleToSize);
    pTemp_Float_Input->value( TempBuffer);
    pTemp_Float_Input->callback( IqeB_GUI_Float_SetValue_Callback, &IqeB_ToolsMesh_ScaleToSize);

    pTemp_Button = new Fl_Button( xc + 8, y, xx2 - 8, yy, "Do it");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)ACTION_CODE_SCALE_TO_SIZE);
    pTemp_Button->tooltip( "Do the size change");

    y += yy;

    // Finish things for this group

    //pTemp_Group->size( pTemp_Group->w(), y - pTemp_Group->y());
    pTemp_Group->end();

  //
  // Group translate
  //

  y = yGroup;

  pTemp_Group = new Fl_Group( x1, y, pMyToolWin->w() - x1 - 4, pMyToolWin->h() - y - 4, "Translate");

    // translate to min, center or max side

    y += 4;
    y += 4;

    xxTemp = (xx2 - 8) / 3;

    pTemp_Box = new Fl_Box( x2, y, xx1, 2, "Align mesh min., center or max. to");
    pTemp_Box->align( FL_ALIGN_TOP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
    pTemp_Box->box( FL_DOWN_FRAME);

    y += 20;

    pTemp_Float_Input = new Fl_Float_Input( x2 + 16, y, xx2 - 16, yy, "X");
    pTemp_Float_Input->type( FL_FLOAT_INPUT);
    pTemp_Float_Input->tooltip( "Align X coordinate");
    sprintf( TempBuffer, "%.2f", IqeB_ToolsMesh_AlignX);
    pTemp_Float_Input->value( TempBuffer);
    pTemp_Float_Input->callback( IqeB_GUI_Float_SetValue_Callback, &IqeB_ToolsMesh_AlignX);

    pTemp_Button = new Fl_Button( xc + 8, y, xxTemp, yy, "@|<");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ALIGN_MESH_X + ACTION_CODE_ALIGN_MESH_MIN));
    pTemp_Button->tooltip( "to mesh minimum");

    pTemp_Button = new Fl_Button( xc + 8 + xxTemp, y, xxTemp, yy, "@2line");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ALIGN_MESH_X + ACTION_CODE_ALIGN_MESH_CENTER));
    pTemp_Button->tooltip( "to mesh center");

    pTemp_Button = new Fl_Button( xc + 8 + xxTemp * 2, y, xxTemp, yy, "@>|");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ALIGN_MESH_X + ACTION_CODE_ALIGN_MESH_MAX));
    pTemp_Button->tooltip( "to mesh maximum");

    y += yy;

    pTemp_Float_Input = new Fl_Float_Input( x2 + 16, y, xx2 - 16, yy, "Y");
    pTemp_Float_Input->type( FL_FLOAT_INPUT);
    pTemp_Float_Input->tooltip( "Align Y coordinate");
    sprintf( TempBuffer, "%.2f", IqeB_ToolsMesh_AlignY);
    pTemp_Float_Input->value( TempBuffer);
    pTemp_Float_Input->callback( IqeB_GUI_Float_SetValue_Callback, &IqeB_ToolsMesh_AlignY);

    pTemp_Button = new Fl_Button( xc + 8, y, xxTemp, yy, "@|<");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ALIGN_MESH_Y + ACTION_CODE_ALIGN_MESH_MIN));
    pTemp_Button->tooltip( "to mesh minimum");

    pTemp_Button = new Fl_Button( xc + 8 + xxTemp, y, xxTemp, yy, "@2line");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ALIGN_MESH_Y + ACTION_CODE_ALIGN_MESH_CENTER));
    pTemp_Button->tooltip( "to mesh center");

    pTemp_Button = new Fl_Button( xc + 8 + xxTemp * 2, y, xxTemp, yy, "@>|");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ALIGN_MESH_Y + ACTION_CODE_ALIGN_MESH_MAX));
    pTemp_Button->tooltip( "to mesh maximum");

    y += yy;

    pTemp_Float_Input = new Fl_Float_Input( x2 + 16, y, xx2 - 16, yy, "Z");
    pTemp_Float_Input->type( FL_FLOAT_INPUT);
    pTemp_Float_Input->tooltip( "Align Z coordinate");
    sprintf( TempBuffer, "%.2f", IqeB_ToolsMesh_AlignZ);
    pTemp_Float_Input->value( TempBuffer);
    pTemp_Float_Input->callback( IqeB_GUI_Float_SetValue_Callback, &IqeB_ToolsMesh_AlignZ);

    pTemp_Button = new Fl_Button( xc + 8, y, xxTemp, yy, "@|<");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ALIGN_MESH_Z + ACTION_CODE_ALIGN_MESH_MIN));
    pTemp_Button->tooltip( "to mesh minimum");

    pTemp_Button = new Fl_Button( xc + 8 + xxTemp, y, xxTemp, yy, "@2line");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ALIGN_MESH_Z + ACTION_CODE_ALIGN_MESH_CENTER));
    pTemp_Button->tooltip( "to mesh center");

    pTemp_Button = new Fl_Button( xc + 8 + xxTemp * 2, y, xxTemp, yy, "@>|");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ALIGN_MESH_Z + ACTION_CODE_ALIGN_MESH_MAX));
    pTemp_Button->tooltip( "to mesh maximum");

    y += yy;

    // Finish things for this group

    //pTemp_Group->size( pTemp_Group->w(), y - pTemp_Group->y());
    pTemp_Group->end();

  //
  // Group rotate
  //

  y = yGroup;

  pTemp_Group = new Fl_Group( x1, y, pMyToolWin->w() - x1 - 4, pMyToolWin->h() - y - 4, "Rotate");

    // rotate around axis

    y += 4;
    y += 4;

    xxTemp = (xx2 - 8) / 2;

    pTemp_Box = new Fl_Box( x2, y, xx1, 2, "Rotate around axis [degree]");
    pTemp_Box->align( FL_ALIGN_TOP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
    pTemp_Box->box( FL_DOWN_FRAME);

    y += 20;

    pTemp_Float_Input = new Fl_Float_Input( x2 + 16, y, xx2 - 16, yy, "X");
    pTemp_Float_Input->type( FL_FLOAT_INPUT);
    pTemp_Float_Input->tooltip( "Rotate around X axis");
    sprintf( TempBuffer, "%.2f", IqeB_ToolsMesh_RotateX);
    pTemp_Float_Input->value( TempBuffer);
    pTemp_Float_Input->callback( IqeB_GUI_Float_SetValue_Callback, &IqeB_ToolsMesh_RotateX);

    pTemp_Button = new Fl_Button( xc + 8, y, xxTemp, yy, "@<");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ROTATE_MESH_X + ACTION_CODE_ROTATE_DIR_NEG));
    pTemp_Button->tooltip( "negative direction");

    pTemp_Button = new Fl_Button( xc + 8 + xxTemp, y, xxTemp, yy, "@>");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ROTATE_MESH_X + ACTION_CODE_ROTATE_DIR_POS));
    pTemp_Button->tooltip( "positive direction");

    y += yy;

    pTemp_Float_Input = new Fl_Float_Input( x2 + 16, y, xx2 - 16, yy, "Y");
    pTemp_Float_Input->type( FL_FLOAT_INPUT);
    pTemp_Float_Input->tooltip( "Rotate around Y axis");
    sprintf( TempBuffer, "%.2f", IqeB_ToolsMesh_RotateY);
    pTemp_Float_Input->value( TempBuffer);
    pTemp_Float_Input->callback( IqeB_GUI_Float_SetValue_Callback, &IqeB_ToolsMesh_RotateY);

    pTemp_Button = new Fl_Button( xc + 8, y, xxTemp, yy, "@<");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ROTATE_MESH_Y + ACTION_CODE_ROTATE_DIR_NEG));
    pTemp_Button->tooltip( "negative direction");

    pTemp_Button = new Fl_Button( xc + 8 + xxTemp, y, xxTemp, yy, "@>");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ROTATE_MESH_Y + ACTION_CODE_ROTATE_DIR_POS));
    pTemp_Button->tooltip( "positive direction");

    y += yy;

    pTemp_Float_Input = new Fl_Float_Input( x2 + 16, y, xx2 - 16, yy, "Z");
    pTemp_Float_Input->type( FL_FLOAT_INPUT);
    pTemp_Float_Input->tooltip( "Rotate around Z axis");
    sprintf( TempBuffer, "%.2f", IqeB_ToolsMesh_RotateZ);
    pTemp_Float_Input->value( TempBuffer);
    pTemp_Float_Input->callback( IqeB_GUI_Float_SetValue_Callback, &IqeB_ToolsMesh_RotateZ);

    pTemp_Button = new Fl_Button( xc + 8, y, xxTemp, yy, "@<");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ROTATE_MESH_Z + ACTION_CODE_ROTATE_DIR_NEG));
    pTemp_Button->tooltip( "negative direction");

    pTemp_Button = new Fl_Button( xc + 8 + xxTemp, y, xxTemp, yy, "@>");
    pTemp_Button->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_ROTATE_MESH_Z + ACTION_CODE_ROTATE_DIR_POS));
    pTemp_Button->tooltip( "positive direction");

    y += yy;

    // Finish things for this group

    //pTemp_Group->size( pTemp_Group->w(), y - pTemp_Group->y());
    pTemp_Group->end();

  // finish up

  pTemp_Tabs->end();
  //pMyToolWin->size( pMyToolWin->w(), pTemp_Tabs->y() + pTemp_Tabs->h() + 4);  // final size of window

  pMyToolWin->end();
  pMyToolWin->set_non_modal();
  pMyToolWin->callback( close_cb, &pMyToolWin);
  pMyToolWin->show();
}

/********************************** End Of File **********************************/
