/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GUI_BUTTON_H
#define GUI_BUTTON_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define GUI_BUTTON_SOUND_1				"data/audio/ui/click_1.ogg"
#define GUI_BUTTON_SOUND_2				"data/audio/ui/click_2.ogg"
#define GUI_BUTTON_NORMAL				0x00
#define GUI_BUTTON_PRESSED				0x01
#define GUI_BUTTON_HOVERED				0x02
#define GUI_BUTTON_NORMAL_TEXTURE		0x5C7F
#define GUI_BUTTON_PRESSED_TEXTURE		0x5C80
#define GUI_BUTTON_HOVERED_TEXTURE		0x5C81
#define GUI_BUTTON_NOT_TEXTURED			-1

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct GuiButton
{
	uint32 ActionStatus;		///< Button event status
};

////////////////////////////////////////////////////////////
// Widget forward declaration
////////////////////////////////////////////////////////////
struct GuiWidget;

////////////////////////////////////////////////////////////
// Create a button
////////////////////////////////////////////////////////////
struct GuiWidget * GuiButtonCreate(int32 X, int32 Y, uint32 FontSize, char * Text);

////////////////////////////////////////////////////////////
// Create a button by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiButtonCreateDefault();

////////////////////////////////////////////////////////////
// Set state of the button
////////////////////////////////////////////////////////////
void GuiButtonSetState(struct GuiButton * Button, int32 State);

////////////////////////////////////////////////////////////
// Render a button
////////////////////////////////////////////////////////////
void GuiButtonRender(struct GuiWidget * Form, struct GuiWidget * Widget, struct Color4ub * BackgroundColor, struct Color4ub * FontColor);

#endif // GUI_BUTTON_H
