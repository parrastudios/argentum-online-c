/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATATYPE_SET_H
#define DATATYPE_SET_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Memory/General.h>

#include <DataType/Operator.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define SET_HASH_INVALID	UINTEGER_MAX_RANGE

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct SetNodeType;
struct SetType;
struct SetIteratorType;

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef uinteger SetHashType;
typedef SetHashType (*SetHashCallback)(void *);

typedef struct SetNodeType * SetNode;
typedef struct SetType * Set;
typedef struct SetIteratorType * SetIterator;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct SetIteratorType
{
	Set SetData;
	uinteger Bucket;
	SetNode Node;
};

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define SetForEach(SetData, Iterator) \
	if (SetGetSize(SetData) > 0) \
		for (SetIteratorBegin(SetData, Iterator); (Iterator)->Node; SetIteratorNext(Iterator))

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a set
////////////////////////////////////////////////////////////
Set SetCreate(uinteger TypeSize, SetHashCallback HashFunc, CompareCallback CompareFunc);

////////////////////////////////////////////////////////////
/// Create a set without comparing function
////////////////////////////////////////////////////////////
Set SetCreateHashOnly(uinteger TypeSize, SetHashCallback HashFunc);

////////////////////////////////////////////////////////////
/// Destroy a set
////////////////////////////////////////////////////////////
void SetDestroy(Set SetData);

////////////////////////////////////////////////////////////
/// Get number of elements a set contains
////////////////////////////////////////////////////////////
uinteger SetGetSize(Set SetData);

////////////////////////////////////////////////////////////
/// Insert a new element into a set
////////////////////////////////////////////////////////////
bool SetInsert(Set SetData, void * Value);

////////////////////////////////////////////////////////////
/// Remove an element by its hash index
////////////////////////////////////////////////////////////
void SetRemoveByHash(Set SetData, SetHashType Hash);

////////////////////////////////////////////////////////////
/// Clear all elements of a set
////////////////////////////////////////////////////////////
void SetClear(Set SetData);

////////////////////////////////////////////////////////////
/// Check if set contains a value
////////////////////////////////////////////////////////////
bool SetHasValue(Set SetData, void * Value);

////////////////////////////////////////////////////////////
/// Returns the value of set by its hash
////////////////////////////////////////////////////////////
void * SetGetValueByHash(Set SetData, SetHashType Hash);

////////////////////////////////////////////////////////////
/// Reallocate if need and reindex all buckets
////////////////////////////////////////////////////////////
void SetRehash(Set SetData);

////////////////////////////////////////////////////////////
/// Set iterator begin
////////////////////////////////////////////////////////////
SetNode SetIteratorBegin(Set SetData, SetIterator Iterator);

////////////////////////////////////////////////////////////
/// Set iterator end
////////////////////////////////////////////////////////////
SetNode SetIteratorEnd(Set SetData, SetIterator Iterator);

////////////////////////////////////////////////////////////
/// Set iterator retreive next
////////////////////////////////////////////////////////////
SetNode SetIteratorNext(SetIterator Iterator);

////////////////////////////////////////////////////////////
/// Set iterator retreive previous
////////////////////////////////////////////////////////////
SetNode SetIteratorPrev(SetIterator Iterator);

////////////////////////////////////////////////////////////
/// Set iterator retreive value
////////////////////////////////////////////////////////////
void * SetIteratorValue(SetIterator Iterator);

#endif // DATATYPE_SET_H
