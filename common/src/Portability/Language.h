/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PORTABILITY_LANGUAGE_H
#define PORTABILITY_LANGUAGE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Compiler.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define LANGUAGE_VERSION_UNKNOWN	0L
#define LANGUAGE_VERSION_C89		198900L
#define LANGUAGE_VERSION_C90		199000L
#define LANGUAGE_VERSION_C94		199409L
#define LANGUAGE_VERSION_C99		199901L
#define LANGUAGE_VERSION_C11		201112L
#define LANGUAGE_VERSION_CPP98		199711L
#define LANGUAGE_VERSION_CPP11		201103L
#define LANGUAGE_VERSION_CPP14		201402L
#define LANGUAGE_VERSION_CPP17		2017FFL
#define LANGUAGE_VERSION_CPPCLI		200406L
#define LANGUAGE_VERSION_ECPP		1L

////////////////////////////////////////////////////////////
// Detection
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Identify standard C / C++ language version
///////////////////////////////////////////////////////////
#if defined(__STDC__)
#	if defined(__STDC_VERSION__)
#		if (__STDC_VERSION__ - 0 > 1)
#			if (__STDC_VERSION__ == LANGUAGE_VERSION_C94)
#				define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C94
#			elif (__STDC_VERSION__ == LANGUAGE_VERSION_C99)
#				define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C99
#			elif (__STDC_VERSION__ == LANGUAGE_VERSION_C11)
#				define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C11
#			else
#				define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_UNKNOWN
#			endif
#		else
#			if (COMPILER_TYPE == COMPILER_SUNPRO)
#				if (COMPILER_VERSION >= 0x420)
#					define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C94
#				else
#					define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C90
#				endif
#			else
#				define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_UNKNOWN
#			endif
#		endif
#	else
#		define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C89
#	endif
#elif (defined(_MSC_EXTENSIONS) || (COMPILER_TYPE == COMPILER_BORLAND))
#	define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_C89
#elif defined(__cplusplus)
#	if (__cplusplus == 1) || (__cplusplus == LANGUAGE_VERSION_CPP98)
#		define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_CPP98
#	elif (__cplusplus == LANGUAGE_VERSION_CPP11)
#		define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_CPP11
#	elif (__cplusplus == LANGUAGE_VERSION_CPP14)
#		define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_CPP14
#	elif (__cplusplus > LANGUAGE_VERSION_CPP14)
#		define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_CPP17
#	else
#		define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_UNKNOWN
#	endif
#elif defined(__cplusplus_cli)
#	define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_CPPCLI
#elif defined(__embedded_cplusplus)
#	define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_ECPP
#else
#	define LANGUAGE_VERSION_TYPE LANGUAGE_VERSION_UNKNOWN
#endif

#endif // PORTABILITY_LANGUAGE_H
