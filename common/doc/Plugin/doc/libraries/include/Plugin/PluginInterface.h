/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	PLUGIN_INTERFACE_H
#define PLUGIN_INTERFACE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Plugin/Provider.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PLUGIN_DESCRIPTOR_FILE_LENGTH	0xFFUL
#define PLUGIN_DESCRIPTOR_NAME_LENGTH	0xFFUL

typedef char		PluginDescriptorFile[PLUGIN_DESCRIPTOR_FILE_LENGTH];
typedef char		PluginDescriptorName[PLUGIN_DESCRIPTOR_NAME_LENGTH];

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
enum PluginProgrammingLanguageInfo
{
	PLUGIN_PROGRAMMING_LANGUAGE_C,
	PLUGIN_PROGRAMMING_LANGUAGE_CPP,
	PLUGIN_PROGRAMMING_LANGUAGE_JS,
	PLUGIN_PROGRAMMING_LANGUAGE_PYTHON,
	PLUGIN_PROGRAMMING_LANGUAGE_LUA
};

typedef uint32 PluginProgrammingLanguageType;
typedef uint32 PluginProgrammingLanguageVersion;

typedef struct PluginDescriptorType PluginDescriptorType;

struct PluginInterfaceType
{
	PluginDescriptorType * (*Descriptor)();		//< Plugin information
	void * (*Initialize)(void *);				//< Initialize plugin callback
	void (*Run)();								//< Main function of a plugin
	void (*Stop)();								//< Temporally stop functionality of the plugin
	void (*Destroy)();							//< Destroy plugin callback
	PluginProviderType * (*Provider)();			//< Plugin data provider (wrapper to metadata modules)
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initializes a custom descriptor for a plugin
////////////////////////////////////////////////////////////
PluginDescriptorType * PluginInterfaceDescriptorCreate(PluginDescriptorType * Descriptor,
	uint32 API, PluginDescriptorFile File, PluginProgrammingLanguageType LanguageType,
	PluginProgrammingLanguageVersion LanguageVersion, PluginDescriptorName Name, uint32 Version);

#endif // PLUGIN_INTERFACE_H
