/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Audio/SoundLoader.h>
#include <Audio/OpenALHelper.h>
#include <System/Error.h>
#include <System/Platform.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// SND File library Headers
////////////////////////////////////////////////////////////
#include <sndfile.h>

////////////////////////////////////////////////////////////
/// Open a sound file for reading
////////////////////////////////////////////////////////////
bool SoundFileOpenReadFromFile(struct Sound * SoundData, char * Filename)
{
	// File descriptor
    SNDFILE* FileDescriptor;

	// Size from buffer
	ALsizei Size;

    // Open the sound file
    SF_INFO FileInfo;
    FileDescriptor = sf_open(Filename, SFM_READ, &FileInfo);

    if (!FileDescriptor)
    {
    	MessageError("SoundFileOpenReadFromFile", "Failed to read sound file \" %s \" (%s)", Filename, sf_strerror(FileDescriptor));
        return false;
    }

    // Generate OpenAL Buffers
    ALCheck(alGenBuffers(1, &SoundData->Buffer));

    // Set the sound parameters
    SoundData->ChannelsCount	= FileInfo.channels;
    SoundData->SampleRate		= FileInfo.samplerate;
    SoundData->SamplesCount 	= (int32)(FileInfo.frames) * SoundData->ChannelsCount;
    SoundData->Time				= SystemGetTime();

	strcpy(SoundData->File, Filename);

	// Read the samples from the opened file
	SoundData->Samples = (int16*)(MemoryAllocate(sizeof(int16) * SoundData->SamplesCount));

	if (sf_read_short(FileDescriptor, &SoundData->Samples[0], SoundData->SamplesCount) == SoundData->SamplesCount)
	{
		// Find the good format according to the number of channels
		ALenum SoundFormat = AudioGetFormatFromChannelsCount(SoundData->ChannelsCount);

		// Check if the format is valid
		if (SoundFormat == 0)
		{
			MessageError("SoundFileOpenReadFromFile", "Unsupported number of channels (%d).", SoundData->ChannelsCount);

			// Close file
			sf_close(FileDescriptor);

			// Return error
			return false;
		}

		// Fill the buffer
		Size = (ALsizei)((SoundData->SamplesCount) * sizeof(int16));
		ALCheck(alBufferData(SoundData->Buffer, SoundFormat, &SoundData->Samples[0], Size, SoundData->SampleRate));

		// Generate OpenAL Sources
		ALCheck(alGenSources(1, &SoundData->Source));

		// Assign and use the new buffer
		ALCheck(alSourcei(SoundData->Source, AL_BUFFER, SoundData->Buffer));
	}
	else
	{
		// Close file
		sf_close(FileDescriptor);

		// Return error
		return false;
	}

    // Close file
    sf_close(FileDescriptor);

    return true;
}

////////////////////////////////////////////////////////////
/// Open a sound file in memory for reading
////////////////////////////////////////////////////////////
bool SoundFileOpenReadFromMemory(struct Sound * SoundData, void* Data, int32 SizeInBytes)
{
	// File descriptor
    /*SNDFILE* SoundFile;

    // Prepare the memory I/O structure
    struct SF_VIRTUAL_IO IO = myMemoryIO.Prepare(Data, SizeInBytes);

    // Open the sound file
    SF_INFO FileInfo;
    SoundFile = sf_open_virtual(&IO, SFM_READ, &FileInfo, &myMemoryIO);

    if (!SoundFile)
    {
    	MessageError("SoundFileOpenReadFromMemory", "Failed to read sound file from memory (%s).", sf_strerror(SoundFile));
        return NULL;
    }

    // Set the sound parameters
    //ChannelsCount = FileInfo.channels;
    //SampleRate    = FileInfo.samplerate;
    //NbSamples     = (int32)(FileInfo.frames) * ChannelsCount;

    return SoundFile;*/

    return true;
}
