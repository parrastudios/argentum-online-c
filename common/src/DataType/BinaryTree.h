/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATATYPE_BINARY_TREE_H
#define DATATYPE_BINARY_TREE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <DataType/Operator.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define	BINARY_TREE_SEARCH_ABORT	0x00
#define	BINARY_TREE_SEARCH_CONTINUE 0x01

typedef uinteger BinaryTreeSearchType;
typedef BinaryTreeSearchType (*BinaryTreeSearch)(void *, uinteger);

////////////////////////////////////////////////////////////
// Data structure
////////////////////////////////////////////////////////////
struct BinaryTreeNode
{
	struct BinaryTreeNode * Left;
	struct BinaryTreeNode * Right;
	struct BinaryTreeNode * Parent;
	void				  * Data;
};

struct BinaryTree
{
	struct BinaryTreeNode * Root;
	uinteger				Size;

	uinteger				RightDepth;
	uinteger				RightElemCount;
	uinteger				RightLeafCount;

	uinteger				LeftDepth;
	uinteger				LeftElemCount;
	uinteger				LeftLeafCount;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

bool BinaryTreeCreate(struct BinaryTree * Tree);

void BinaryTreeDestroy(struct BinaryTree * Tree);

void BinaryTreeAdd(struct BinaryTree * Tree, struct BinaryTreeNode * Node, CompareCallback CompareFunc);

void BinaryTreeRemove(struct BinaryTree * Tree, struct BinaryTreeNode * Node, CompareCallback CompareFunc);

bool BinaryTreeIsMember(struct BinaryTree * Tree, struct BinaryTreeNode * Node);

void * BinaryTreeCloset(struct BinaryTree * Tree, uinteger Size, BinaryTreeSearch SearchFunc, CompareCallback CompareFunc);

void * BinaryTreeBigger(struct BinaryTree * Tree, uinteger Size, BinaryTreeSearch SearchFunc, CompareCallback CompareFunc);

#endif // DATATYPE_BINARY_TREE_H
