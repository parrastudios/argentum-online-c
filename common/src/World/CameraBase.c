/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/CameraBase.h>
#include <World/CameraTransform.h>

#include <System/IOHelper.h> // todo: refactor this

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define CAMERA_BASE_DAMPING_DEFAULT		(CAMERA_BASE_SPRING_DEFAULT / 2.0f)

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a base component of a camera
////////////////////////////////////////////////////////////
struct EntityType * CameraBaseCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Initialize base camera properties (spring disabled)
////////////////////////////////////////////////////////////
struct EntityType * CameraBaseInitialize(struct EntityType * Entity, char * Name);

////////////////////////////////////////////////////////////
/// Initialize base camera properties (spring enabled)
////////////////////////////////////////////////////////////
struct EntityType * CameraBaseInitializeSpring(struct EntityType * Entity, char * Name, float Spring);

////////////////////////////////////////////////////////////
/// Destroy a camera base component
////////////////////////////////////////////////////////////
void CameraBaseDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Set camera spring constant
////////////////////////////////////////////////////////////
void CameraBaseSetSpring(struct EntityType * Entity, float Spring);

////////////////////////////////////////////////////////////
/// Set camera perspective
////////////////////////////////////////////////////////////
void CameraBasePerspective(struct EntityType * Entity, float FovX, float Aspect, float Near, float Far);

////////////////////////////////////////////////////////////
/// Look camera at a target with eye and up vectors
////////////////////////////////////////////////////////////
void CameraBaseLookAt(struct EntityType * Entity, struct Vector3f * Eye, struct Vector3f * Target, struct Vector3f * Up);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the camera base implementation attributes
////////////////////////////////////////////////////////////
const void * CameraComponentBaseAttr()
{
	static const struct CameraComponentBaseType CameraComponentBaseAttr = {
		TypeInit(Initialize, &CameraBaseInitialize),
		TypeInit(InitializeSpring, &CameraBaseInitializeSpring),
		TypeInit(SetSpring, &CameraBaseSetSpring),
		TypeInit(Perspective, &CameraBasePerspective),
		TypeInit(LookAt, &CameraBaseLookAt)
	};

	return (const void *)&CameraComponentBaseAttr;
}

////////////////////////////////////////////////////////////
/// Return the camera base implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * CameraComponentBaseImpl()
{
	static const struct ComponentImplType CameraComponentBaseTypeImpl = {
		ExprInit(Base, TypeInit(Create, &CameraBaseCreate), TypeInit(Destroy, &CameraBaseDestroy)),
		TypeInit(Type, COMPONENT_TYPE_BASE),
		UnionInit(Attributes, Derived, &CameraComponentBaseAttr)
	};

	return &CameraComponentBaseTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a base component of a camera
////////////////////////////////////////////////////////////
struct EntityType * CameraBaseCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct CameraComponentBaseData * BaseData = ComponentDataDerived(Base, struct CameraComponentBaseData);

		// Set default base name for camera
		sprintf(BaseData->Name, "Camera <%d>", Entity->Index);

		// Set constants to default
		BaseData->EnableSpring = false;
		BaseData->SpringConstant = CAMERA_BASE_DAMPING_DEFAULT;
		BaseData->DampingConstant = CAMERA_BASE_SPRING_DEFAULT;

		// Initialize perspective properties
		BaseData->FovX = CAMERA_BASE_FOVX_DEFAULT;
		BaseData->FovY = 0.0f;
		BaseData->Near = CAMERA_BASE_ZNEAR_DEFAULT;
		BaseData->Far = CAMERA_BASE_ZFAR_DEFAULT;

		// Initialize axis
		Vector3fSet(&BaseData->AxisX, 1.0f, 0.0f, 0.0f);
		Vector3fSet(&BaseData->AxisY, 0.0f, 1.0f, 0.0f);
		Vector3fSet(&BaseData->AxisZ, 0.0f, 0.0f, 1.0f);

		// Initialize view direction
		Vector3fSet(&BaseData->ViewDirection, 0.0f, 0.0f, -1.0f);

		// Initialize target related vectors
		Vector3fSetNull(&BaseData->Eye);
		Vector3fSetNull(&BaseData->Target);
		Vector3fSet(&BaseData->TargetAxisY, 0.0f, 1.0f, 0.0f);

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Initialize base camera properties (spring disabled)
////////////////////////////////////////////////////////////
struct EntityType * CameraBaseInitialize(struct EntityType * Entity, char * Name)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct CameraComponentBaseData * BaseData = ComponentDataDerived(Base, struct CameraComponentBaseData);

		if (Name && Name[0] != NULLCHAR)
		{
			// Copy predefined name
			strncpy(BaseData->Name, Name, CAMERA_BASE_NAME_LENGTH);
		}
		else
		{
			// Set default base name for camera
			sprintf(BaseData->Name, "Camera <%d>", Entity->Index);
		}

		// Set constants to default
		BaseData->EnableSpring = false;
		BaseData->SpringConstant = CAMERA_BASE_DAMPING_DEFAULT;
		BaseData->DampingConstant = CAMERA_BASE_SPRING_DEFAULT;

		// Initialize perspective properties
		BaseData->FovX = CAMERA_BASE_FOVX_DEFAULT;
		BaseData->FovY = 0.0f;
		BaseData->Near = CAMERA_BASE_ZNEAR_DEFAULT;
		BaseData->Far = CAMERA_BASE_ZFAR_DEFAULT;

		// Initialize axis
		Vector3fSet(&BaseData->AxisX, 1.0f, 0.0f, 0.0f);
		Vector3fSet(&BaseData->AxisY, 0.0f, 1.0f, 0.0f);
		Vector3fSet(&BaseData->AxisZ, 0.0f, 0.0f, 1.0f);

		// Initialize view direction
		Vector3fSet(&BaseData->ViewDirection, 0.0f, 0.0f, -1.0f);

		// Initialize target related vectors
		Vector3fSetNull(&BaseData->Eye);
		Vector3fSetNull(&BaseData->Target);
		Vector3fSet(&BaseData->TargetAxisY, 0.0f, 1.0f, 0.0f);

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Initialize base camera properties (spring enabled)
////////////////////////////////////////////////////////////
struct EntityType * CameraBaseInitializeSpring(struct EntityType * Entity, char * Name, float Spring)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct CameraComponentBaseData * BaseData = ComponentDataDerived(Base, struct CameraComponentBaseData);

		if (Name && Name[0] != NULLCHAR)
		{
			// Copy predefined name
			strncpy(BaseData->Name, Name, CAMERA_BASE_NAME_LENGTH);
		}
		else
		{
			// Set default base name for camera
			sprintf(BaseData->Name, "Camera <%d>", Entity->Index);
		}

		// Set enable spring
		BaseData->EnableSpring = true;

		// Set spring and damping constant
		if (Spring > CAMERA_BASE_SPRING_DISABLE)
		{
			CameraBaseSetSpring(Entity, Spring);
		}
		else
		{
			BaseData->SpringConstant = CAMERA_BASE_DAMPING_DEFAULT;
			BaseData->DampingConstant = CAMERA_BASE_SPRING_DEFAULT;
		}

		// Initialize perspective properties
		BaseData->FovX = CAMERA_BASE_FOVX_DEFAULT;
		BaseData->FovY = 0.0f;
		BaseData->Near = CAMERA_BASE_ZNEAR_DEFAULT;
		BaseData->Far = CAMERA_BASE_ZFAR_DEFAULT;

		// Initialize axis
		Vector3fSet(&BaseData->AxisX, 1.0f, 0.0f, 0.0f);
		Vector3fSet(&BaseData->AxisY, 0.0f, 1.0f, 0.0f);
		Vector3fSet(&BaseData->AxisZ, 0.0f, 0.0f, 1.0f);

		// Initialize view direction
		Vector3fSet(&BaseData->ViewDirection, 0.0f, 0.0f, -1.0f);

		// Initialize target related vectors
		Vector3fSetNull(&BaseData->Eye);
		Vector3fSetNull(&BaseData->Target);
		Vector3fSet(&BaseData->TargetAxisY, 0.0f, 1.0f, 0.0f);

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a camera base component
////////////////////////////////////////////////////////////
void CameraBaseDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct PointComponentBaseData * BaseData = ComponentDataDerived(Base, struct PointComponentBaseData);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Set camera spring constant
////////////////////////////////////////////////////////////
void CameraBaseSetSpring(struct EntityType * Entity, float Spring)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct CameraComponentBaseData * BaseData = ComponentDataDerived(Base, struct CameraComponentBaseData);

		if (Spring > CAMERA_BASE_SPRING_DISABLE)
		{
			// Using a critically damped spring system where the damping ratio
			// is equal to one :
			//
			// DampingRatio = DampingConstant / (2.0f * sqrtf(SpringConstant))

			BaseData->SpringConstant = Spring;
			BaseData->DampingConstant = 2.0f * sqrtf(BaseData->SpringConstant);
			BaseData->SpringConstant = true;
		}
		else
		{
			BaseData->SpringConstant = false;
		}
	}
}

////////////////////////////////////////////////////////////
/// Set camera perspective
////////////////////////////////////////////////////////////
void CameraBasePerspective(struct EntityType * Entity, float FovX, float Aspect, float Near, float Far)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct CameraComponentBaseData * BaseData = ComponentDataDerived(Base, struct CameraComponentBaseData);

		// Obtain base component data reference
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct CameraComponentTransformType * TransformType = ComponentTypeDerived(Transform, struct CameraComponentTransformType);

		// Calculate fov y and scale
		float AspectInv = 1.0f / Aspect;
		float FovY = 2.0f * atanf(AspectInv / (1.0f / tanf(degtorad(FovX) / 2.0f)));

		// Update perspective properties
		BaseData->FovX = FovX;
		BaseData->FovY = FovY;
		BaseData->Near = Near;
		BaseData->Far = Far;

		// If transform is available
		if (TransformType)
		{
			float ScaleX = 1.0f / tanf(0.5f * FovY);
			float ScaleY = ScaleX / AspectInv;

			// Set projection matrix
			TransformType->Projection(Entity, ScaleX, ScaleY, Near, Far);
		}
	}
}

////////////////////////////////////////////////////////////
/// Look camera at a target with eye and up vectors
////////////////////////////////////////////////////////////
void CameraBaseLookAt(struct EntityType * Entity, struct Vector3f * Eye, struct Vector3f * Target, struct Vector3f * Up)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct CameraComponentBaseData * BaseData = ComponentDataDerived(Base, struct CameraComponentBaseData);

		// Obtain base component data reference
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct CameraComponentTransformType * TransformType = ComponentTypeDerived(Transform, struct CameraComponentTransformType);

		// Get eye, target and target y axis
		Vector3fCopy(&BaseData->Eye, Eye);
		Vector3fCopy(&BaseData->Target, Target);
		Vector3fCopy(&BaseData->TargetAxisY, Up);

		// Calculate z axis from eye minus target
		Vector3fSubtractEx(&BaseData->AxisZ, Eye, Target);
		Vector3fNormalize(&BaseData->AxisZ);

		// Get view direction, oposite to z axis
		Vector3fOpposite(&BaseData->ViewDirection, &BaseData->AxisZ);

		// Calculate x axis from up and z axis cross product
		Vector3fCross(Up, &BaseData->AxisZ, &BaseData->AxisX);
		Vector3fNormalize(&BaseData->AxisX);

		// Calculate y axis from z and x axis cross product
		Vector3fCross(&BaseData->AxisZ, &BaseData->AxisX, &BaseData->AxisY);
		Vector3fNormalize(&BaseData->AxisY);
		Vector3fNormalize(&BaseData->AxisX);

		// If transform is available
		if (TransformType)
		{
			// Set projection matrix
			TransformType->View(Entity);
		}
	}
}

////////////////////////////////////////////////////////////
/// Return the camera base derived data size
////////////////////////////////////////////////////////////
uinteger CameraComponentBaseSize()
{
	return sizeof(struct CameraComponentBaseData);
}
