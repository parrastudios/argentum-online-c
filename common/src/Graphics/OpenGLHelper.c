/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/OpenGLHelper.h>

////////////////////////////////////////////////////////////
/// Window includes
////////////////////////////////////////////////////////////
#include <Window/Window.h>

////////////////////////////////////////////////////////////
/// Standard library includes
////////////////////////////////////////////////////////////
#include <System/IOHelper.h>
#include <System/Error.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
//bool	DepthEvenFlag;
char  * GLExtensions;
uint32  RenderingStates[GL_STATES_COUNT];

////////////////////////////////////////////////////////////
/// Configure OpenGL states for rendering
////////////////////////////////////////////////////////////
bool OpenGLInitialize()
{
	#if PLATFORM_TYPE == PLATFORM_WINDOWS
		// Initialize wgl function pointers
		wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)(wglGetProcAddress("wglSwapIntervalEXT"));
        wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)(wglGetProcAddress("wglChoosePixelFormatARB"));
	#endif

	// Initialize the OpenGL extensions
	GLExtensions = (char*)glGetString(GL_EXTENSIONS);

	// Check if the pc can use npot textures
	SupportsTextureNPOT  	= OpenGLCheckExtension("GL_ARB_texture_non_power_of_two");
	SupportsVBO				= OpenGLCheckExtension("GL_ARB_vertex_buffer_object");
	SupportsEdgeClamp 		= OpenGLCheckExtension("GL_EXT_texture_edge_clamp");
	SupportsTextureCombine	= OpenGLCheckExtension("GL_ARB_texture_env_combine");
	SupportsCVA				= OpenGLCheckExtension("GL_EXT_compiled_vertex_array");
	SupportsVARFence		= (OpenGLCheckExtension("GL_NV_vertex_array_range") && OpenGLCheckExtension("GL_NV_Fence"));
	SupportsPBuffer 		= (OpenGLCheckExtension("WGL_ARB_pixel_format") && OpenGLCheckExtension("WGL_ARB_pbuffer"));
	SupportsMakeCurrentRead	= OpenGLCheckExtension("WGL_ARB_make_current_read");

	OpenGLCheck(glGetIntegerv(GL_MAX_TEXTURES_UNITS_ARB, &TextureUnitsCounter));
	OpenGLCheck(glGetIntegerv(GL_MAX_TEXTURE_SIZE, &TextureMaxSize));

	// Shade model
	glShadeModel(GL_SMOOTH); // GL_FLAT

	// Clear color
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);

	// Initialize depth test
	/*if (GL_HELPER_CLEAR_DEPTH)
	{
		glClearDepth(1.0f);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glDepthFunc(GL_LEQUAL);
		
		if (GL_HELPER_RENDER_2D)
		{
			glClear(GL_DEPTH_BUFFER_BIT);
			glDepthRange(0.0f, 0.5f);
			glDepthFunc(GL_LESS);

			DepthEvenFlag = true;
		}
	}*/

	// Set perspective calculations
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);

	OpenGLSetState(GL_POLYGON_TYPE, GL_POLYGON_FILL);
	OpenGLSetState(GL_ALPHA_MODE, GL_ALPHA_ENABLED);
	OpenGLSetState(GL_COLOR_MODE, GL_COLOR_ENABLE);

	// Resize the OpenGL scene
	// OpenGLResizeScene();

	// Print features
	#if COMPILE_TYPE == DEBUG_MODE
		OpenGLPrintFeatures();
	#endif

	return true;
}

////////////////////////////////////////////////////////////
/// Clear OpenGL scene
////////////////////////////////////////////////////////////
void OpenGLClear()
{
	// Reset the current viewport
	OpenGLResetViewPort();

	// Clear Screen And Depth Buffer
/*	if (GL_HELPER_CLEAR_DEPTH)
	{
		if (GL_HELPER_RENDER_2D)
		{
			if (DepthEvenFlag)
			{
				glDepthFunc( GL_LESS );
				glDepthRange( 0.0, 0.5 );
			}
			else
			{
				glDepthFunc( GL_GREATER );
				glDepthRange( 1.0, 0.5 );
			}

			DepthEvenFlag = !DepthEvenFlag;
		}

		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	}
	else
	{
		glClear( GL_COLOR_BUFFER_BIT );
	}
*/
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

////////////////////////////////////////////////////////////
/// Reset the view port
////////////////////////////////////////////////////////////
void OpenGLResetViewPort()
{
	glViewport(0, 0, WindowWidth, WindowHeight);
}

////////////////////////////////////////////////////////////
/// Set rendering states
////////////////////////////////////////////////////////////
void OpenGLSetState(uint32 State, uint32 Value)
{
	RenderingStates[State] = Value;
}

////////////////////////////////////////////////////////////
/// Begin rendering states
////////////////////////////////////////////////////////////
void OpenGLBegin()
{
	if (RenderingStates[GL_STATES_MODE] == GL_STATES_SAVE)
	{
		// Save render states
		glPushAttrib(GL_ALL_ATTRIB_BITS);

		// Save matrices
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
	}

	if (RenderingStates[GL_POLYGON_TYPE] == GL_POLYGON_WIREFRAME)
	{
		// Set the width of the lines
		glLineWidth(1.0f);

		// Polygon mode
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else if (RenderingStates[GL_POLYGON_TYPE] == GL_POLYGON_FILL)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	if (RenderingStates[GL_RENDERING_MODE] == GL_RENDERING_2D)
	{
		GLint Viewport[4];

		glGetIntegerv(GL_VIEWPORT, Viewport);

        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();

		glOrtho(Viewport[0], Viewport[0] + Viewport[2], Viewport[1] + Viewport[3], Viewport[1], -1.0f, 1.0f);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();

        glPushAttrib(GL_DEPTH_BUFFER_BIT | GL_LIGHTING_BIT);

        glDisable(GL_DEPTH_TEST);
        glDisable(GL_LIGHTING);
		glDisable(GL_DITHER);

		glEnable(GL_TEXTURE_2D);

		if (RenderingStates[GL_ALPHA_MODE] == GL_ALPHA_DISABLED)
		{
			glBlendFunc(GL_ONE,GL_ZERO);
			glDisable(GL_BLEND);
		}
		else if (RenderingStates[GL_ALPHA_MODE] == GL_ALPHA_ENABLED)
		{
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
			glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
		}
	}
	else if (RenderingStates[GL_RENDERING_MODE] == GL_RENDERING_3D)
	{
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		glEnable(GL_LIGHTING);
		glEnable(GL_DITHER);
		glEnable(GL_CULL_FACE);

		glDisable(GL_TEXTURE_2D);

		if (RenderingStates[GL_ALPHA_MODE] == GL_ALPHA_DISABLED)
		{
			//glDisable(GL_DEPTH_ALPHA_TEST);
		}
		else if (RenderingStates[GL_ALPHA_MODE] == GL_ALPHA_ENABLED)
		{
			//glEnable(GL_DEPTH_ALPHA_TEST);
			//glAlphaFunc(GL_GREATER, 0.1f);
		}
	}

	if (RenderingStates[GL_COLOR_MODE] == GL_COLOR_ENABLE)
	{
		glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
		glEnable(GL_COLOR_MATERIAL);
	}
}

////////////////////////////////////////////////////////////
/// End rendering states
////////////////////////////////////////////////////////////
void OpenGLEnd()
{
	if (RenderingStates[GL_STATES_MODE] == GL_STATES_SAVE)
	{
		// Restore render states
		glPopAttrib();

		// Restore matrices
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
	}

	if (RenderingStates[GL_RENDERING_MODE] == GL_RENDERING_2D)
	{
        glPopAttrib();

		glMatrixMode(GL_PROJECTION);
		glPopMatrix();

		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();

		glDisable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);

		glEnable(GL_DEPTH_TEST);
        glEnable(GL_LIGHTING);
		glEnable(GL_DITHER);
	}
	else if (RenderingStates[GL_RENDERING_MODE] == GL_RENDERING_3D)
	{
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		glDisable(GL_DITHER);
		glDisable(GL_CULL_FACE);
		
		glEnable(GL_TEXTURE_2D);

		if (RenderingStates[GL_ALPHA_MODE] == GL_ALPHA_ENABLED)
		{
			//glDisable(GL_DEPTH_ALPHA_TEST);
		}
	}

	if (RenderingStates[GL_COLOR_MODE] == GL_COLOR_ENABLE)
	{
		glDisable(GL_COLOR_MATERIAL);
	}
}

////////////////////////////////////////////////////////////
/// Checks the OpenGL extensions
////////////////////////////////////////////////////////////
bool OpenGLCheckExtension(char * Ext)
{
	char * Extensions = &GLExtensions[0];
	char * End = Extensions + strlen(Extensions);
	uint32 ExtLength = strlen(Ext);

	while (Extensions < End)
	{
		uint32 Length = strcspn(Extensions, " ");

		if ((ExtLength == Length) && (strncmp(Ext, Extensions, Length) == 0))
			return true;

		Extensions += (Length + 1);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Check the last OpenGL error
////////////////////////////////////////////////////////////
void OpenGLCheckError(char * File, unsigned int Line)
{
    // Get the last error
    GLenum ErrorCode = glGetError();

    if (ErrorCode != GL_NO_ERROR)
    {
        char * Error = "unknown error";
        char * Desc  = "no description";

        // Decode the error code
        switch (ErrorCode)
        {
            case GL_INVALID_ENUM :
            {
                Error = "GL_INVALID_ENUM";
                Desc  = "an unacceptable value has been specified for an enumerated argument";
                break;
            }

            case GL_INVALID_VALUE :
            {
                Error = "GL_INVALID_VALUE";
                Desc  = "a numeric argument is out of range";
                break;
            }

            case GL_INVALID_OPERATION :
            {
                Error = "GL_INVALID_OPERATION";
                Desc  = "the specified operation is not allowed in the current state";
                break;
            }

            case GL_STACK_OVERFLOW :
            {
                Error = "GL_STACK_OVERFLOW";
                Desc  = "this command would cause a stack overflow";
                break;
            }

            case GL_STACK_UNDERFLOW :
            {
                Error = "GL_STACK_UNDERFLOW";
                Desc  = "this command would cause a stack underflow";
                break;
            }

            case GL_OUT_OF_MEMORY :
            {
                Error = "GL_OUT_OF_MEMORY";
                Desc  = "there is not enough memory left to execute the command";
                break;
            }

            case GL_INVALID_FRAMEBUFFER_OPERATION_EXT :
            {
                Error = "GL_INVALID_FRAMEBUFFER_OPERATION_EXT";
                Desc  = "the object bound to FRAMEBUFFER_BINDING_EXT is not \"framebuffer complete\"";
                break;
            }
        }

        // Log the error
		MessageError("GLCheckError", "An internal OpenGL call failed in %s ( %d ) : %s, %s.", File, Line, Error, Desc);
    }
}

////////////////////////////////////////////////////////////
/// Print graphic device and OpenGL features
////////////////////////////////////////////////////////////
void OpenGLPrintFeatures()
{
	printf("Vendor:         %s\nRenderer: 	%s\nOpenGL Version: %s\n",
		   (char*)(glGetString(GL_VENDOR)),
		   (char*)(glGetString(GL_RENDERER)),
		   (char*)(glGetString(GL_VERSION)));
}