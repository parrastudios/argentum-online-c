/****************************************************************************

  IqeB_GUI_ToolsAnimKinect.cpp

  26.04.2015 RR: First editon of this file.

  Interface to a kinect device.

*****************************************************************************
*/

#include <math.h>

#include "windows.h"

// -----------------------------------------------------------
// 27.04.2015 RR: Things to make the "NuiApi.h" include work
//
// Need a header directory to the kinect API
// (like C:\Program Files\Microsoft SDKs\Kinect\v1.8\inc).
//
// Also ignore the following compiler warnings including "NuiApi.h".
//   warning: ignoring #pragma warning  [-Wunknown-pragmas]
//   warning: extra tokens at end of #endif directive [enabled by default]
//
#define _In_            // This is a microsoft source-code annotation language (SAL) thing. Expands to nothing
#define _Out_           // This is a microsoft source-code annotation language (SAL) thing. Expands to nothing
#define _Inout_         // This is a microsoft source-code annotation language (SAL) thing. Expands to nothing
#define _In_opt_        // This is a microsoft source-code annotation language (SAL) thing. Expands to nothing
#define _Check_return_  // This is a microsoft source-code annotation language (SAL) thing. Expands to nothing
#define _Deref_out_     // This is a microsoft source-code annotation language (SAL) thing. Expands to nothing
#define _In_count_c_( count)       // This is a microsoft source-code annotation language (SAL) thing. Expands to nothing
#define _Out_cap_c_( count)        // This is a microsoft source-code annotation language (SAL) thing. Expands to nothing
#define _Out_opt_cap_post_count_(cap,count)  // This is a microsoft source-code annotation language (SAL) thing. Expands to nothing

#define static_assert( test, messsage) // Something else special

#include "NuiApi.h"

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_draw.H>
#include <FL/fl_ask.H>
#include <FL/Fl_Int_Input.H>

/************************************************************************************
 * Globals
 *
 */

#define IQE_ANIM_KINECT_POSE_LIVE_NAME   (char *)"Kinect-Live"    // Name for kinect live pose
#define IQE_ANIM_KINECT_POSE_RECORD_NAME (char *)"Kinect-Record"  // Name for recording kinect data
#define IQE_ANIM_KINECT_POSE_RECORD_FPS          15.0             // Recording, this frame per seconds

#define ANIM_DRAW_FONT_NR           0      // Use this fond drawings
#define ANIM_DRAW_FONT_SIZE        14      // Use this font size for drawings

// Main enables
static int   IqeB_ToolsAnimKinectEnable     = 0;   // Must be != 0 to search for a connected kinect device
static int   IqeB_ToolsAnimKinectDoRetarget = 0;   // Must be != 0 to retarget the kinect skeleton to the model skeleton.

// Options
static int   IqeB_ToolsAnimKinectOnlyYaw    = 1;   // If 0, use all object angles from kinect, else use only the yaw angle
static int   IqeB_ToolsAnimKinectCenterXY   = 1;   // If 0, use all object positions from kinect, else use Z only

// Record times
static int    RecordWaitStartTime = 5;             // Seconds to wait before start recording
static int    RecordRecordTime    = 10;            // Seconds to record

/************************************************************************************
 * Window to draw graphics.
 *
 */

// Forwards

static void MyGraphicWindowDisplay( int x, int y, int w, int h);
static void IqeB_GUI_ToolsAnimKinectIdleAction( void *);

// The OpenGL Window class

class MyGraphicWindow : public Fl_Box {

    void draw() {

      // Draw my OpenGL view
      MyGraphicWindowDisplay( x(), y(), w(), h());
    }

public:
    // OPENGL WINDOW CONSTRUCTOR
    MyGraphicWindow(int X,int Y,int W,int H,const char*L=0) : Fl_Box(X,Y,W,H,L) {

    }
};

/************************************************************************************
 * Statics
 */

static  Fl_Double_Window *pMyToolWin;
static int MyWinPosX = IQE_GUI_NO_WINPOS_X, MyWinPosY = IQE_GUI_NO_WINPOS_Y; // last window position

static MyGraphicWindow *pGUI_GraphicWin;        // My graphic window
static Fl_Check_Button *pCBox_Connect;          // Chechbox 'Connect to kinect'
static Fl_Check_Button *pCBox_DoRetarget;       // Checkbox 'Do retarget'

static Fl_Button *pGUI_ButRecordAnim;           // Record an animation
static Fl_Int_Input *pInt_InputWaitStart;       // Input field for 'Start in [sec]'
static Fl_Int_Input *pInt_InputRecordTime;      // Input field for 'Record [sec]'

// Kinect connect state

#define KINECT_STATE_DISABLED       0     // Disabled connect to kinect
#define KINECT_STATE_CONNECTING     1     // Wating for connection to kinect
#define KINECT_STATE_CONNECTED      2     // Connected to kinect

static int Kinect_Connect_state = KINECT_STATE_DISABLED;

static INuiSensor *m_pNuiSensor = NULL;          // if != NULL, current kinect
static HANDLE      m_hNextSkeletonEvent = NULL;  // if != NULL, used for kinect message handling

// Last data read from kinect
static NUI_SKELETON_DATA Kinect_skeleton_data;   // Skeleton data from kinect
static Vector4 Kinect_vFloorClipPlane;           // Kinect Floor

// 'Do retarget' state.

#define KINECT_DO_RETARGET_OFF     0       // 'Do retarget' is off
#define KINECT_DO_RETARGET_ERROR   1       // 'Do retarget' has an error
#define KINECT_DO_RETARGET_ON      2       // 'Do retarget' is on

static char DoRetargetErrorString[ 256];              // Any problem, show this error text
static int  DoRetargetState = KINECT_DO_RETARGET_OFF; // 'Do retarget' state
static struct pose SkelPosePreprocessed[ MAXBONE];    // Preprocessed skeleton poses

// Record sequencer

#define KINECT_RECORD_OFF           0       // Record is off
#define KINECT_RECORD_WAIT_START    1       // Record waits for start time gone
#define KINECT_RECORD_DO            2       // Recording
#define KINECT_RECORD_ACTION_TEST  -1       // Test for state changes
#define KINECT_RECORD_ACTION_ABORT -2       // Abort any recording
#define KINECT_RECORD_ACTION_STOP  -3       // Legal stop of recording

static int   RecordState = KINECT_RECORD_OFF;       // 'Record' state

static float RecordTimeRemain = 0.0;                // Remaining time in seconds

static struct anim *pRecordAnim;                    // Recording to this animation
static int   RecordFramesNum = 0;                   // Number of frames to record
static int   RecordFrameDone = 0;                   // Number of frames already recorded

static unsigned int RecordFrameTimeDesired = 0;     // Time of a frame in ms
static unsigned int RecordFrameTimeAccumulated = 0; // Time of a frame accumulated in ms

static void IqeB_Tools_Anim_RecordSequencer( int StateAction); // Need this as forward

// Kinect data frames per second (fps)

static char KinectFramesPerSecondString[ 256];      // Kinect fps, string to display

/************************************************************************************
 * update GUI of this tool window
 */

static void MyWinUpdate()
{

  IqeB_GUI_WidgetActivate( pGUI_ButRecordAnim, Kinect_Connect_state == KINECT_STATE_CONNECTED && // Test for a connected kinect
                                               pDrawModel != NULL &&                             // have a model loaded
                                               (DoRetargetState == KINECT_DO_RETARGET_OFF || DoRetargetState == KINECT_DO_RETARGET_ON)); // Retarget must be on or off (no error)

  IqeB_GUI_WidgetActivate(  pInt_InputWaitStart, RecordState <= KINECT_RECORD_OFF);        // Recording is off
  IqeB_GUI_WidgetActivate( pInt_InputRecordTime, RecordState <= KINECT_RECORD_OFF);        // Recording is off
}

/************************************************************************************
 * IqeB_Tools_Anim_RecordSetup
 *
 * * Create the named animation 'Kinect-Record' (if not already created).
 *   This will be used as 'live' recording animation (of the kinect)
 * * If this is called, we have a cinnect connected and 'Do retarget'
 *   is on (there is a proper model loaded).
 *   --> Store the live animation in the 'Kinect-Record' animation.
 *
 */
static int IqeB_Tools_Anim_RecordSetup( char *pAnimName, char *pErrString,
                                        int ResetForNewRecord, int SecondsToRecord)
{
  int iAnimation, iFrame, UpdateGUI;
  IQE_model *pModel;

  // Setup

  pModel = pDrawModel;                      // Use the current displayed model
  UpdateGUI = false;                        // Preset, no update of the GUI

  if( pModel->anim_count > 0 &&             // joint_count must be equal
      pModel->anim_poses != pModel->skel->joint_count) {

    if( pErrString != NULL) strcpy( pErrString, "Anim-/Skel. # joints mismatch");

    pRecordAnim = NULL;   // Reset animation
    return( -10);
  }

  // Test for an animation with the given name, create one if not existing

  iAnimation = IqeB_AnimFindByName( pModel, pAnimName);  // Does we already have the named animation

  if( ! ResetForNewRecord) {    // Monitor recording.

    if( iAnimation < 0) {       // Kinect live is gone

      if( pErrString != NULL) sprintf( pErrString, "Animation '%s' is gone", pAnimName);

      pRecordAnim = NULL;   // Reset animation
      return( -11);
    }

    return( 0);    // Return OK
  }

  //
  // Have to setup a new animation
  //

  // Calculate the number of frames we need

  RecordFramesNum = (int)(SecondsToRecord * IQE_ANIM_KINECT_POSE_RECORD_FPS + 0.5);
  if( RecordFramesNum < 1) {    // Security test

    RecordFramesNum = 1;
  }

  // Some other preparations for new record session

  RecordFrameTimeDesired = (int)( 1000.0 / IQE_ANIM_KINECT_POSE_RECORD_FPS + 0.5); // Time for one frame in milli seconds
  if( RecordFrameTimeDesired < 30) {   // Keep some minimum time
    RecordFrameTimeDesired = 30;
  }

  RecordFrameTimeAccumulated = RecordFrameTimeDesired;  // Reset frame time accumulated

  RecordFrameDone = 0;          // Reset number of frames already recorded

  // Enshure we have the animation created

  if( iAnimation < 0) {    // Got none

    struct anim *pAnim;

    // create new animation
    pAnim = pushanim( pModel, pAnimName);

    pAnim->framerate = IQE_ANIM_KINECT_POSE_RECORD_FPS;  // Set fps

    if( pModel->anim_count == 1) {    // Is this the first created animation

      pModel->anim_poses = pModel->skel->joint_count;
    }

    // New animation is the last one

    iAnimation = pModel->anim_count - 1;

    UpdateGUI = true;                         // New animation, update of the GUI

  } else {   // Already have a frame

    // security test, joint count and poses match

    if( pModel->anim_poses != pModel->skel->joint_count) {

      if( pErrString != NULL) strcpy( pErrString, "Anim-/Skel. # joints mismatch");

      pRecordAnim = NULL;   // Reset animation
      return( -12);
    }
  }

  // This is our animation data

  pRecordAnim = pModel->anim_data[ iAnimation];

  // Keep fps updated

  if( pRecordAnim->framerate != IQE_ANIM_KINECT_POSE_RECORD_FPS) {  // Has changed

    pRecordAnim->framerate = IQE_ANIM_KINECT_POSE_RECORD_FPS;  // Update fps
    UpdateGUI = true;                                   // Fps has changed, update of the GUI
  }

  // Adapt the number of frames

  if( RecordFramesNum < pRecordAnim->len) {  // Delete frames

    UpdateGUI = true;                        // # frames has changed

    // NOTE: We keep minimum one animation
    while( pRecordAnim->data != NULL && pRecordAnim->len > 1 && RecordFramesNum < pRecordAnim->len) {  // Still have to delete

      free( pRecordAnim->data[ pRecordAnim->len - 1]);  // Delete last

      pRecordAnim->data[ pRecordAnim->len - 1] = NULL;

      pRecordAnim->len -= 1; // one less
    }

  } else if( RecordFramesNum > pRecordAnim->len) {  // Add frames

    UpdateGUI = true;                               // # frames has changed

    while( RecordFramesNum > pRecordAnim->len) {    // Still have to add

      // Add one frame
      pushframe( pRecordAnim, pModel->skel->joint_count);
    }
  }

  // Preset the all animation frames with the skeleton

  for( iFrame = 0; iFrame < pRecordAnim->len; iFrame++) {

    // Copy the skeleton to the frame
    memcpy( pRecordAnim->data[ iFrame], pModel->skel->pose, sizeof( struct pose) * pModel->skel->joint_count);
  }

  // Preset KeyFlags

  IqeB_AnimKeyFlagsPresetAnimation( pRecordAnim, pDrawModel->anim_poses);

  // Update GUI's

  if( UpdateGUI) {    // Update the GUI ?

    IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
  }

  // A message that we started recording

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    (char *)"Recording animation ...", NULL, NULL);

  return( 0);   // return OK
}

/************************************************************************************
 * IqeB_Tools_Anim_RecordAddFrame
 *
 * Got a new frame from the kinect.
 * Copy it to 'Kinect-Record' animation.
 *
 */
static int IqeB_Tools_Anim_RecordAddFrame( struct anim *pLiveAnim, unsigned int FrameTimeDelta, char *pErrString)
{
  IQE_model *pModel;

  // Setup

  pModel = pDrawModel;                      // Use the current displayed model

  // Pause ?

  if( pLiveAnim == NULL && FrameTimeDelta == 0) {

    RecordFrameTimeAccumulated = RecordFrameTimeDesired;  // Reset frame time accumulated

    return( 0);
  }

  // Security tests

  if( pModel == NULL) {  // Security test, have pointer

    if( pErrString != NULL) strcpy( pErrString, "RecordAddFrame: Lost drawing model");

    return( -10);
  }

  if( pRecordAnim == NULL) {  // Security test, have pointer

    if( pErrString != NULL) strcpy( pErrString, "RecordAddFrame: Lost 'Kinect-Record' animation");

    return( -11);
  }

  if( pLiveAnim == NULL) {  // Security test, have pointer

    if( pErrString != NULL) strcpy( pErrString, "RecordAddFrame: Lost 'Kinect-Live' animation");

    return( -12);
  }

  if( pLiveAnim->len < 1 || pLiveAnim->data[ 0] == NULL) {  // Security test, have a frame

    if( pErrString != NULL) strcpy( pErrString, "RecordAddFrame: Lost 'Kinect-Live' frame");

    return( -13);
  }

  // Time management

  if( RecordFrameDone < RecordFramesNum) {            // Still have to record

    if( FrameTimeDelta > RecordFrameTimeDesired) {    // Big delta

      FrameTimeDelta = RecordFrameTimeDesired;        // Clip to transfer rate (fps of recording)
    }

    RecordFrameTimeAccumulated += FrameTimeDelta;     // Add accumulated time

    if( RecordFrameTimeAccumulated >= RecordFrameTimeDesired) {  // time to record

      memcpy( pRecordAnim->data[ RecordFrameDone], pLiveAnim->data[ 0], sizeof( struct pose) * pModel->skel->joint_count);

      RecordFrameDone += 1;      // One more frame acquired

      RecordFrameTimeAccumulated -= RecordFrameTimeDesired;      // Subtract
    }
  }

  if( RecordFrameDone >= RecordFramesNum) {            // Recording is done

    IqeB_Tools_Anim_RecordSequencer( KINECT_RECORD_ACTION_STOP); // Stop recording
  }

  // Update remaining record time

  RecordTimeRemain = (RecordFramesNum - RecordFrameDone) * RecordFrameTimeDesired  * 0.001;    // Remaining time in seconds
  if( RecordTimeRemain < 0.0) {  // Clipping
    RecordTimeRemain = 0.0;
  }

  return( 0);  // return OK
}

/************************************************************************************
 * IqeB_Tools_Anim_RecordStop
 *
 * Stop recording
 *
 */
static int IqeB_Tools_Anim_RecordStop( char *pErrString)
{

  if( pRecordAnim == NULL) {  // Security test, have pointer

    if( pErrString != NULL) strcpy( pErrString, "RecordStop: Lost 'Kinect-Record'");

    return( -12);
  }

  // Preset KeyFlags

  IqeB_AnimKeyFlagsPresetAnimation( pRecordAnim, pDrawModel->anim_poses);

  return( 0);  // return OK
}

/************************************************************************************
 * IqeB_Tools_Anim_RecordSequencer
 *
 * Animation recored sequencer.
 * Manage delays and other things.
 *
 */
static void IqeB_Tools_Anim_RecordSequencer( int StateAction)
{
  unsigned int TimeStampThis;
  static unsigned int TimeStampStart;       // MUST BE STATIC, start of last state change
  int RecordStateOnEntry, OkToRecord;

	TimeStampThis = IqeB_Disp_GetTickCount(); // current time

	RecordStateOnEntry = RecordState;         // Latch this value on entry

	// Is it OK to recored
  OkToRecord = Kinect_Connect_state == KINECT_STATE_CONNECTED && // Test for a connected kinect
               DoRetargetState == KINECT_DO_RETARGET_ON;         // Retarget must be on too

	//
	// Test for state changes depenting on calling argument
	//

  switch( StateAction) {

  case KINECT_RECORD_ACTION_TEST:  // Test for state changes

    if( RecordState > KINECT_RECORD_OFF &&                        // Start delay or recording
        ! OkToRecord) {                                           // an NOT ok to record

      RecordState = KINECT_RECORD_OFF;
    }

    if( RecordState == KINECT_RECORD_WAIT_START) {                // Waiting until start delay is gone

      RecordTimeRemain = RecordWaitStartTime - (TimeStampThis - TimeStampStart) * 0.001;    // Remaining time in seconds
      if( RecordTimeRemain < 0.0) {  // Clipping
        RecordTimeRemain = 0.0;
      }

      if( TimeStampThis - TimeStampStart >= (unsigned)RecordWaitStartTime * 1000) {  // and start delay is gone

        RecordState = KINECT_RECORD_DO;
        RecordTimeRemain = RecordRecordTime;    // Remaining time in seconds
    	  TimeStampStart = TimeStampThis; //
      }
    }

    if( RecordState == KINECT_RECORD_DO) {                            // Recording

#ifdef use_again  // 04.06.2015 RR: Remaining time is caculated in IqeB_Tools_Anim_RecordAddFrame()
      RecordTimeRemain = RecordRecordTime - (TimeStampThis - TimeStampStart) * 0.001;    // Remaining time in seconds
      if( RecordTimeRemain < 0.0) {  // Clipping
        RecordTimeRemain = 0.0;
      }

      if( TimeStampThis - TimeStampStart >= (unsigned)RecordRecordTime * 1000) {  // and record time is gone

        RecordState = KINECT_RECORD_OFF;
      }
#endif
    }

    break;

  case KINECT_RECORD_ACTION_ABORT:  // Abort any recording
  case KINECT_RECORD_ACTION_STOP:   // Legal stop of recording
  case KINECT_RECORD_OFF:           // Record is off

    if( RecordState > KINECT_RECORD_OFF) {      // On

      RecordState = KINECT_RECORD_OFF;
    }

    break;

  case KINECT_RECORD_WAIT_START:   // Record waits for start time gone

    if( OkToRecord && RecordState < KINECT_RECORD_WAIT_START) {  // Off

      if( RecordWaitStartTime <= 0) {           // No start delay
        RecordState = KINECT_RECORD_DO;
        RecordTimeRemain = RecordRecordTime;    // Remaining time in seconds
      } else {                                  // Have to do start delay
        RecordState = KINECT_RECORD_WAIT_START;
        RecordTimeRemain = RecordWaitStartTime; // Remaining time in seconds
      }
  	  TimeStampStart = TimeStampThis; //
    }

    break;

  case KINECT_RECORD_DO:           // Recording

    if( OkToRecord && RecordState < KINECT_RECORD_DO) {     // Off

      RecordState = KINECT_RECORD_DO;
      RecordTimeRemain = RecordRecordTime;    // Remaining time in seconds
  	  TimeStampStart = TimeStampThis; //
    }

    break;
  }  // end switch()

	//
	// Prepare recording thing
	//

	if( RecordState >= KINECT_RECORD_WAIT_START) {     // Recording

    int ierr;
    char TempErrorString[ 256];

    // Switched from off to on
	  if( RecordState >= KINECT_RECORD_WAIT_START && RecordStateOnEntry < KINECT_RECORD_WAIT_START) {

      ierr = IqeB_Tools_Anim_RecordSetup( IQE_ANIM_KINECT_POSE_RECORD_NAME, TempErrorString,
                                          true, RecordRecordTime);

	  } else {

      ierr = IqeB_Tools_Anim_RecordSetup( IQE_ANIM_KINECT_POSE_RECORD_NAME, TempErrorString,
                                          false, RecordRecordTime);
	  }

    if( ierr != 0) {  // Something not OK

      // Some kind of feedback to the user
  	  fl_beep( FL_BEEP_ERROR);

      IqeB_DispMessage( DRAW_MSG_COL_ERR,
                        (char *)"Record animation setup:", TempErrorString, NULL);

      RecordState = KINECT_RECORD_OFF;
    }
  }

	//
	// State changing actions
	//

	if( RecordStateOnEntry != RecordState) {   // Record state has changed

    if( RecordState == KINECT_RECORD_WAIT_START) {

	    fl_beep( FL_BEEP_DEFAULT);

    } else if( RecordState == KINECT_RECORD_DO) {

  	  fl_beep( FL_BEEP_MESSAGE);

    } else {

  	  fl_beep( FL_BEEP_ERROR);
    }

	  // Switched from off to on
	  if( RecordState >= KINECT_RECORD_WAIT_START && RecordStateOnEntry < KINECT_RECORD_WAIT_START) {

      // Modify 'Record' button text
      pGUI_ButRecordAnim->label( "Stop");
	  }

	  // Switched from on to off
	  if( RecordState < KINECT_RECORD_WAIT_START && RecordStateOnEntry >= KINECT_RECORD_WAIT_START) {

      // Modify 'Record' button text
      pGUI_ButRecordAnim->label( "Go");

      // End animation processing
      IqeB_Tools_Anim_RecordStop( NULL);

      // Legal stop of recording
      if( StateAction == KINECT_RECORD_ACTION_STOP && // Legal stop of recording
          pRecordAnim != NULL &&                      // Still have this
          RecordFrameDone > 0) {                      // Have recorded one frame

        // Switch retarget off

        if( IqeB_ToolsAnimKinectDoRetarget) {   // 'Do retarget' is on

          IqeB_ToolsAnimKinectDoRetarget = false;  // Switch to off

          if( pCBox_DoRetarget->value() != IqeB_ToolsAnimKinectDoRetarget) {
            pCBox_DoRetarget->value( IqeB_ToolsAnimKinectDoRetarget);
          }

          DoRetargetState = KINECT_DO_RETARGET_OFF; // 'Do retarget' state to off
          strcpy( DoRetargetErrorString, "Checkbox 'Do retarget' is off");  // Error String to off
        }

        // Ensure play animation is on

        if( ! IqeB_DispStyle_doplay) {   // Animation play is off

          IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doplay, 1); // Update variable and GUI
        }

        // Ensure 'play once' checkbox is off

        if( IqeB_DispStyle_doPlayOnce) {   // Animation 'play once' checkbox is on

          IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doPlayOnce, 0); // Update variable and GUI
        }

        // Switch to the 'Kinect-Record' animation

        pDrawModelCurAnim = pRecordAnim;

        DrawModelCurframe = 0;  // Always switch to first frame

        IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI

        IqeB_DispMessage( DRAW_MSG_COL_OK,
                          (char *)"Playback recorded animation", NULL, NULL);
	    }
	  }
	}
}

/************************************************************************************
 * IqeB_Tools_Retarget_TestKinectSkeleton
 *
 * * Quick test that the models skeleton match the criterias of
 *   the kinect skeleton.
 *
 * Return:   0: OK
 *         < 0: No mach
 *
 */
static int IqeB_Tools_Retarget_TestKinectSkeleton( IQE_model *pModel, char *pErrString)
{
  int iJoint;
  struct skel *pSkel;

  // Test basic requirements

  if( pModel == NULL) {                     // No model

    if( pErrString != NULL) {

      strcpy( pErrString, "No model");
    }

    return( -1);
  }

  if( pModel->skel == NULL ||              // NO skeleton
      pModel->skel->joint_count <= 0) {

    if( pErrString != NULL) {

      strcpy( pErrString, "Model has no skeleton");
    }

    return( -2);
  }

  pSkel = pModel->skel;

  // Number of joints must match

  if( pSkel->joint_count != NUI_SKELETON_POSITION_COUNT) {   // Must have this amount

    if( pErrString != NULL) {

      sprintf( pErrString, "Skeleton need %d joints", NUI_SKELETON_POSITION_COUNT);
    }

    return( -3);
  }

  // Skeleton must be normalized

  if( pSkel->IsNormalized == false) {   // Skeleton is not normalized

    if( pErrString != NULL) {

      strcpy( pErrString, "Skeleton must be 'normalized'");
    }

    return( -4);
  }

  // Test for skin bindung

  if( pModel->mesh && pModel->mesh->part_count > 0 &&      // Model has some parts
      pModel->mesh && pModel->mesh->vertex_count > 0 &&    // and model has some vertices
      pModel->mesh && pModel->mesh->blendweight == NULL) { // NO skin blending weights

    if( pErrString != NULL) {

      strcpy( pErrString, "Model must be rigged");
    }

    return( -5);
  }

  // Test jont mismatch

  for( iJoint = 0; iJoint < pSkel->joint_count; iJoint++) {

    switch( iJoint) {
    case NUI_SKELETON_POSITION_HIP_CENTER:
      if( pSkel->j[ iJoint].parent >= 0) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_SPINE:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_HIP_CENTER) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_SHOULDER_CENTER:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_SPINE) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_HEAD:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_SHOULDER_CENTER) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_SHOULDER_LEFT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_SPINE) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_ELBOW_LEFT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_SHOULDER_LEFT) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_WRIST_LEFT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_ELBOW_LEFT) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_HAND_LEFT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_WRIST_LEFT) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_SHOULDER_RIGHT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_SPINE) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_ELBOW_RIGHT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_SHOULDER_RIGHT) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_WRIST_RIGHT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_ELBOW_RIGHT) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_HAND_RIGHT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_WRIST_RIGHT) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_HIP_LEFT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_HIP_CENTER) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_KNEE_LEFT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_HIP_LEFT) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_ANKLE_LEFT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_KNEE_LEFT) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_FOOT_LEFT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_ANKLE_LEFT) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_HIP_RIGHT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_HIP_CENTER) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_KNEE_RIGHT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_HIP_RIGHT) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_ANKLE_RIGHT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_KNEE_RIGHT) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    case NUI_SKELETON_POSITION_FOOT_RIGHT:
      if( pSkel->j[ iJoint].parent != NUI_SKELETON_POSITION_ANKLE_RIGHT) {

        if( pErrString) strcpy( pErrString, "Joint hierarchy mismatch");
        return( -6);
      }
      break;
    } // End switch()
  }

  return( 0 );   // return OK
}

/************************************************************************************
 * IqeB_Tools_Retarget_PoseSetup
 *
 * * Create the named animation 'Kinect-Live' (if not already created).
 *   This will be used for 'live' retarget (of the kinect)
 * * This is called after IqeB_Tools_Retarget_TestKinectSkeleton()
 *   says OK to the model and skeleton.
 *   --> NO need to test this things here again.
 *
 */
static int IqeB_Tools_Retarget_PoseSetup( char *pAnimName, char *pErrString)
{
  int iAnimation, iParent, iChild, UpdateGUI;
  IQE_model *pModel;

  // Setup

  pModel = pDrawModel;                      // Use the current displayed model
  UpdateGUI = false;                        // Preset, no update of the GUI

  if( pModel->anim_count > 0 &&             // joint_count must be equal
      pModel->anim_poses != pModel->skel->joint_count) {

    if( pErrString != NULL) {

      strcpy( pErrString, "Anim-/Skel. # joints mismatch");
    }

    return( -10);
  }

  // Test for an animation with the given name, create one if not existing

  iAnimation = IqeB_AnimFindByName( pModel, pAnimName);  // Does we already have the named animation

  if( iAnimation < 0) {    // Got none

    struct anim *pAnim;
    struct pose *pPose;

    // create new animation
    pAnim = pushanim( pModel, pAnimName);

    // Add one frame
    pPose = pushframe( pAnim, pModel->skel->joint_count);

    // And preset the pose with the skeleton

    memcpy( pPose, pModel->skel->pose, sizeof( struct pose) * pModel->skel->joint_count);

    if( pModel->anim_count == 1) {    // Is this the first created animation

      pModel->anim_poses = pModel->skel->joint_count;
    }

    // New animation is the last one

    iAnimation = pModel->anim_count - 1;

    // HACK: enshure we setup a skeleton poses later
    DoRetargetState = KINECT_DO_RETARGET_OFF; // 'Do retarget' state

    UpdateGUI = true;                         // Set, update of the GUI
  }

  // Switch to the current animation

  if( pDrawModelCurAnim != pModel->anim_data[ iAnimation]) {  // Other animation is selected

    pDrawModelCurAnim = pModel->anim_data[ iAnimation];
    IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);

    // HACK: enshure we setup a skeleton poses later
    DoRetargetState = KINECT_DO_RETARGET_OFF; // 'Do retarget' state

    UpdateGUI = true;                         // Set, update of the GUI
  }

  DrawModelCurframe = 0;  // Always switch to first frame

  // Enshure animation play is anabled and on

  if( ! IqeB_DispStyle_doAnimations) {   // Do animations checkbox should be set

    IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doAnimations, 1); // Update variable and GUI
  }

  if( ! IqeB_DispStyle_doplay) {   // Animation play is off

    IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doplay, 1); // Update variable and GUI
  }

  if( IqeB_DispStyle_doPlayOnce) {   // Animation 'play once' checkbox is on

    IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doPlayOnce, 0); // Update variable and GUI
  }

  // Setup the skeleton poses

  if( DoRetargetState != KINECT_DO_RETARGET_ON) {   // NOT on here

    int i;

    memcpy( SkelPosePreprocessed, pModel->skel->pose, sizeof( SkelPosePreprocessed));  // Get skeleton

    // NOTE: Reuse 'KeyFlags' as index for the one and only child

    for( int i = 0; i < NUI_SKELETON_POSITION_COUNT; i++) {

      // NOTE: Reuse 'KeyFlags' as index for the one and only child
      SkelPosePreprocessed[ i].KeyFlags = -1;      // Not valid
    }

#ifdef use_again  //02.06.2015 RR: don't use this. Bone is to short for kinect to give reliable results
    SkelPosePreprocessed[ NUI_SKELETON_POSITION_HIP_CENTER].KeyFlags = NUI_SKELETON_POSITION_SPINE;
    SkelPosePreprocessed[ NUI_SKELETON_POSITION_SPINE].KeyFlags = NUI_SKELETON_POSITION_SHOULDER_CENTER;
#else
    SkelPosePreprocessed[ NUI_SKELETON_POSITION_HIP_CENTER].KeyFlags = NUI_SKELETON_POSITION_SHOULDER_CENTER;
#endif
    SkelPosePreprocessed[ NUI_SKELETON_POSITION_SHOULDER_CENTER].KeyFlags = NUI_SKELETON_POSITION_HEAD;

    SkelPosePreprocessed[ NUI_SKELETON_POSITION_SHOULDER_LEFT].KeyFlags = NUI_SKELETON_POSITION_ELBOW_LEFT;
    SkelPosePreprocessed[ NUI_SKELETON_POSITION_ELBOW_LEFT].KeyFlags = NUI_SKELETON_POSITION_WRIST_LEFT;
#ifdef use_again  //02.06.2015 RR: don't use this. Bone is to short for kinect to give reliable results
    SkelPosePreprocessed[ NUI_SKELETON_POSITION_WRIST_LEFT].KeyFlags = NUI_SKELETON_POSITION_HAND_LEFT;
#endif

    SkelPosePreprocessed[ NUI_SKELETON_POSITION_SHOULDER_RIGHT].KeyFlags = NUI_SKELETON_POSITION_ELBOW_RIGHT;
    SkelPosePreprocessed[ NUI_SKELETON_POSITION_ELBOW_RIGHT].KeyFlags = NUI_SKELETON_POSITION_WRIST_RIGHT;
#ifdef use_again  //02.06.2015 RR: don't use this. Bone is to short for kinect to give reliable results
    SkelPosePreprocessed[ NUI_SKELETON_POSITION_WRIST_RIGHT].KeyFlags = NUI_SKELETON_POSITION_HAND_RIGHT;
#endif

    SkelPosePreprocessed[ NUI_SKELETON_POSITION_HIP_LEFT].KeyFlags = NUI_SKELETON_POSITION_KNEE_LEFT;
    SkelPosePreprocessed[ NUI_SKELETON_POSITION_KNEE_LEFT].KeyFlags = NUI_SKELETON_POSITION_ANKLE_LEFT;
#ifdef use_again  //02.06.2015 RR: don't use this. Bone is to short for kinect to give reliable results
    SkelPosePreprocessed[ NUI_SKELETON_POSITION_ANKLE_LEFT].KeyFlags = NUI_SKELETON_POSITION_FOOT_LEFT;
#endif

    SkelPosePreprocessed[ NUI_SKELETON_POSITION_HIP_RIGHT].KeyFlags = NUI_SKELETON_POSITION_KNEE_RIGHT;
    SkelPosePreprocessed[ NUI_SKELETON_POSITION_KNEE_RIGHT].KeyFlags = NUI_SKELETON_POSITION_ANKLE_RIGHT;
#ifdef use_again  //02.06.2015 RR: don't use this. Bone is to short for kinect to give reliable results
    SkelPosePreprocessed[ NUI_SKELETON_POSITION_ANKLE_RIGHT].KeyFlags = NUI_SKELETON_POSITION_FOOT_RIGHT;
#endif

    for( i = 0; i < NUI_SKELETON_POSITION_COUNT; i++) {

      iParent = pModel->skel->j[ i].parent;        // Parent of this joint
      iChild  = SkelPosePreprocessed[ i].KeyFlags;   // Index of the one and anly child

      if( iParent >= 0 &&    // has a parent
          iChild >= 0) {     // Has a child (needed to get direction)

        // NOTE: Reuse 'angles' as normalized direction vector
        vec_copy( SkelPosePreprocessed[ i].angles, SkelPosePreprocessed[ iChild].location);  // Get delta location

        vec_normalize( SkelPosePreprocessed[ i].angles);   // Normalize direction
      }
    }

    // Switch 'Do retarget' to on
    DoRetargetState = KINECT_DO_RETARGET_ON;   // 'Do retarget' to on
  }

  // Update GUI's

  if( UpdateGUI) {    // Update the GUI ?

    MyWinUpdate();   // Update the GUI

    IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
  }

  return( 0);   // return OK
}

/************************************************************************************
 * IqeB_Tools_Retarget_KinectToDrawModel
 *
 * A Skeleton from the kinect was just read on.
 * Retarget the data to the draw model.
 *
 * DoRetarget   true: do the retarget
 *             false: only test the model/skeleton (and prepare it if OK)
 */
static int IqeB_Tools_Retarget_KinectToDrawModel( int DoRetarget)
{
  int iParent, iChild, ierr;
  IQE_model *pModel;
  struct skel *pSkel;

  // Setup

  pModel = pDrawModel;                      // Use the current displayed model

  // Switch to off ?

  if( ! IqeB_ToolsAnimKinectDoRetarget) {

    DoRetargetState = KINECT_DO_RETARGET_OFF; // 'Do retarget' state to off
    strcpy( DoRetargetErrorString, "Checkbox 'Do retarget' is off");  // Error String to off

    return( 0);   // return OK
  }

  // Test basic requirements

  ierr = IqeB_Tools_Retarget_TestKinectSkeleton( pModel, DoRetargetErrorString);
  if( ierr != 0) {  // Something not OK

    // NOTE: 'DoRetargetErrorString' is set by IqeB_Tools_Retarget_TestKinectSkeleton()
    DoRetargetState = KINECT_DO_RETARGET_ERROR; // 'Do retarget' state to err

    return( ierr);   // return error
  }

  // Prepare retarget and the pose

  ierr = IqeB_Tools_Retarget_PoseSetup( IQE_ANIM_KINECT_POSE_LIVE_NAME, DoRetargetErrorString);
  if( ierr != 0) {  // Something not OK

    // NOTE: 'DoRetargetErrorString' is set by IqeB_Tools_Retarget_PoseSetup()
    DoRetargetState = KINECT_DO_RETARGET_ERROR; // 'Do retarget' state to err

    return( ierr);   // return error
  }

  // If we come to here, 'Do retarget' state must be on.
  // Make a security test

  if( DoRetargetState != KINECT_DO_RETARGET_ON) { // 'Do retarget' state is not on

    strcpy( DoRetargetErrorString, "'Do retarget' is NOT on");  // Error String to off
    return( -20);
  }

  // Also the current animation must have a length of 1

#ifdef use_again  // 02.06.2015 RR: this test is true, but who cares
  if( pDrawModelCurAnim->len != 1) {  // length mismatch


    return( -3);
  }
#endif

  // To we have to do the retarget?

  if( ! DoRetarget) {   // NO request to do the retarget

    return( 0);
  }

  //
  // OK, retarget ....
  //

  // Get object and size

  struct pose *pPose;
  vec3 ObjSize, ObjCenter;
  float SizeScale;

  IqeB_DispModelGetInfo( ObjSize, ObjCenter);
  SizeScale = ObjSize[ 2] / 2.0; //1.8;     // Scale factor for size

  // Get the hierarchical bone orientations from the kinect data

	NUI_SKELETON_BONE_ORIENTATION boneOrientations[NUI_SKELETON_POSITION_COUNT];

	NuiSkeletonCalculateBoneOrientations( &Kinect_skeleton_data, boneOrientations);

  // ...

  pSkel = pModel->skel;
  pPose = pDrawModelCurAnim->data[ 0];  // Point to the first frame of the animation

  // Retarget via angles
  // NOTE: Reuse struct pose 'KeyFlags' as index for the one and only child
  // NOTE: Reuse struct pose 'angles' as normalized direction vector


  // Retarget, Step 1:
  // Remap the positions from kinect coordinate space
  // to coordinate space used by IqeBrowser

	vec3 IqeB_SkelPos[ NUI_SKELETON_POSITION_COUNT];

  for( int i = 0; i < NUI_SKELETON_POSITION_COUNT; i++) {

    IqeB_SkelPos[ i][ 0] = - Kinect_skeleton_data.SkeletonPositions[ i].z;
    IqeB_SkelPos[ i][ 1] = - Kinect_skeleton_data.SkeletonPositions[ i].x;
    IqeB_SkelPos[ i][ 2] =   Kinect_skeleton_data.SkeletonPositions[ i].y;
  }

  // Retarget, Step 2:
  // Get the model orientation from the kinect root bone

  vec4 Kinect_Base_Orient_Quat,   IqeB_Base_Orient_Quat,   IqeB_Base_Orient_Neg_Quat;
  vec3 Kinect_Base_Orient_Angles, IqeB_Base_Orient_Angles, IqeB_Base_Orient_Neg_Angles;

  // Get the base orientation as quaterion
  Kinect_Base_Orient_Quat[ 0] = boneOrientations[ NUI_SKELETON_POSITION_HIP_CENTER].hierarchicalRotation.rotationQuaternion.x;
  Kinect_Base_Orient_Quat[ 1] = boneOrientations[ NUI_SKELETON_POSITION_HIP_CENTER].hierarchicalRotation.rotationQuaternion.y;
  Kinect_Base_Orient_Quat[ 2] = boneOrientations[ NUI_SKELETON_POSITION_HIP_CENTER].hierarchicalRotation.rotationQuaternion.z;
  Kinect_Base_Orient_Quat[ 3] = boneOrientations[ NUI_SKELETON_POSITION_HIP_CENTER].hierarchicalRotation.rotationQuaternion.w;

  // Convert quat rotation to angles
  quat_to_rotation( Kinect_Base_Orient_Quat, Kinect_Base_Orient_Angles);

  // Remap the orientation from kinect coordinate space to coordinate space used by IqeBrowser
  if( IqeB_ToolsAnimKinectOnlyYaw) {   // use only the yaw angle

    vec_set( IqeB_Base_Orient_Angles, 0.0, 0.0, M_PI + Kinect_Base_Orient_Angles[ 1]);

  } else {   // use all object angles from kinect

    vec_set( IqeB_Base_Orient_Angles, Kinect_Base_Orient_Angles[ 2], Kinect_Base_Orient_Angles[ 0], M_PI + Kinect_Base_Orient_Angles[ 1]);
  }

  while( IqeB_Base_Orient_Angles[ 2] > M_PI) IqeB_Base_Orient_Angles[ 2] -= 2.0 * M_PI;

  // Make a quaterion from the angels
  quat_from_rotation( IqeB_Base_Orient_Quat, IqeB_Base_Orient_Angles);

  // Negate the direction
  vec_set( IqeB_Base_Orient_Neg_Angles, - IqeB_Base_Orient_Angles[ 0], - IqeB_Base_Orient_Angles[ 1], - IqeB_Base_Orient_Angles[ 2]);

  quat_from_rotation( IqeB_Base_Orient_Neg_Quat, IqeB_Base_Orient_Neg_Angles);

  // Remove the rotation from the skeleton points
  for( int i = 0; i < NUI_SKELETON_POSITION_COUNT; i++) {

    quat_transfrom_vec3( IqeB_Base_Orient_Neg_Quat, IqeB_SkelPos[ i], IqeB_SkelPos[ i]);
  }

#ifdef _DEBUG

  if( RecordState == KINECT_RECORD_OFF) {    // No recording of animations

#ifdef use_again
    char TempStringDebug1[ 256], TempStringDebug2[ 256];

    sprintf( TempStringDebug1, " Kinect: yaw %3d, pitch %3d, roll %3d",
                                (int)(RAD2DEG( Kinect_Base_Orient_Angles[ 1])),
                                (int)(RAD2DEG( Kinect_Base_Orient_Angles[ 0])),
                                (int)(RAD2DEG( Kinect_Base_Orient_Angles[ 2])));
    sprintf( TempStringDebug2, " World : yaw %3d, pitch %3d, roll %3d",
                                (int)(RAD2DEG( IqeB_Base_Orient_Angles[ 2])),
                                (int)(RAD2DEG( IqeB_Base_Orient_Angles[ 1])),
                                (int)(RAD2DEG( IqeB_Base_Orient_Angles[ 0])));

    IqeB_DispMessage( 0.0, 1.0, 1.0,
                      (char *)"Models base angles:", TempStringDebug1, TempStringDebug2);
#else
    char TempStringDebug1[ 256], TempStringDebug2[ 256], TempStringDebug3[ 256];

    sprintf( TempStringDebug1, "Angles yaw/pitch/roll Kinect: %3d/%3d/%3d   world: %3d/%3d/%3d",
                                (int)(RAD2DEG( Kinect_Base_Orient_Angles[ 1])),
                                (int)(RAD2DEG( Kinect_Base_Orient_Angles[ 0])),
                                (int)(RAD2DEG( Kinect_Base_Orient_Angles[ 2])),
                                (int)(RAD2DEG( IqeB_Base_Orient_Angles[ 2])),
                                (int)(RAD2DEG( IqeB_Base_Orient_Angles[ 1])),
                                (int)(RAD2DEG( IqeB_Base_Orient_Angles[ 0])));

    sprintf( TempStringDebug2, "Kinect hip: %4.1f %4.1f %4.1f   world: %4.1f %4.1f %4.1f",
                                Kinect_skeleton_data.SkeletonPositions[ 0].x,
                                Kinect_skeleton_data.SkeletonPositions[ 0].y,
                                Kinect_skeleton_data.SkeletonPositions[ 0].z,
                                IqeB_SkelPos[ 0][ 0],
                                IqeB_SkelPos[ 0][ 1],
                                IqeB_SkelPos[ 0][ 2]);

    sprintf( TempStringDebug3, "Kinect pos: %4.1f %4.1f %4.1f   floor: %4.1f %4.1f %4.1f %4.1f",
                                Kinect_skeleton_data.Position.x,
                                Kinect_skeleton_data.Position.y,
                                Kinect_skeleton_data.Position.z,
                                Kinect_vFloorClipPlane.x,
                                Kinect_vFloorClipPlane.y,
                                Kinect_vFloorClipPlane.z,
                                Kinect_vFloorClipPlane.w);

    IqeB_DispMessage( 0.0, 1.0, 1.0,
                      TempStringDebug1, TempStringDebug2, TempStringDebug3);
#endif
  }
#endif

  // Retarget, Step 3:
  // Get angle difference between Kinect bone
  // and the skeleton bone.

  for( int i = 0; i < NUI_SKELETON_POSITION_COUNT; i++) {

    iParent = pSkel->j[ i].parent;

    iChild = SkelPosePreprocessed[ i].KeyFlags;   // Index of the one and only child

    vec_copy( pPose[ i].location, SkelPosePreprocessed[ i].location);

    // Handle position

    if( iParent < 0) {    // is a root joint

      if( IqeB_ToolsAnimKinectCenterXY) {   // use Z only

        // Position X/Y is from skeletons center (the location of the root joint)
        pPose[ i].location[ 0] = SkelPosePreprocessed[ i].location[ 0];
        pPose[ i].location[ 1] = SkelPosePreprocessed[ i].location[ 1];

      } else {   // use all object positions from kinect

        // Position X/Y is relative to kinect position
        pPose[ i].location[ 0] = (3.0 - Kinect_skeleton_data.Position.z) * SizeScale;
        pPose[ i].location[ 1] = - Kinect_skeleton_data.Position.x * SizeScale;
      }

      // Relative to the ground plane
      pPose[ i].location[ 2] = SkelPosePreprocessed[ i].location[ 2] + Kinect_skeleton_data.SkeletonPositions[ 0].y * SizeScale;

    } else {

      vec_copy( pPose[ i].location, SkelPosePreprocessed[ i].location);
    }

    // handle rotations

    if( iParent >= 0 &&    // has a parent
        iChild >= 0) {     // Has a child (needed to get direction)

      vec3 JointDir;

      // Get angle difference between Kinect bone and the skeleton bone.

      JointDir[ 0] = IqeB_SkelPos[ iChild][ 0] - IqeB_SkelPos[ i][ 0];
      JointDir[ 1] = IqeB_SkelPos[ iChild][ 1] - IqeB_SkelPos[ i][ 1];
      JointDir[ 2] = IqeB_SkelPos[ iChild][ 2] - IqeB_SkelPos[ i][ 2];

      vec_normalize( JointDir);                 // Normalize direction

      quat_from_two_normalized_vectors( pPose[ i].rotation, SkelPosePreprocessed[ i].angles, JointDir);

    } else {

      if( iParent < 0) {    // is the root joint

        // get the rotation from base orientation

        quat_copy( pPose[ i].rotation, IqeB_Base_Orient_Quat);

      } else {

        // No rotation
        quat_set_identity( pPose[ i].rotation);
      }
    }
  }

  // Retarget, Step 4:
  // Subtraction angle differences calculated in pass1
  // from the childs.
  // NOTE: Childs are located always after it's parent.

  for( int i = 0; i < NUI_SKELETON_POSITION_COUNT; i++) {

    iParent = pSkel->j[ i].parent;

    iChild = SkelPosePreprocessed[ i].KeyFlags;   // Index of the one and only child

    if( iParent >= 0 &&    // has a parent
        iChild >= 0 &&     // Has a child (needed to get direction)
        SkelPosePreprocessed[ iChild].KeyFlags >= 0) {  // The child is also flaged for processing

      // If this one has a rotation set, it can be subtracted from the child's rotation

      vec4 QuatInv;

      quat_copy( QuatInv, pPose[ i].rotation);    // Rotation from this

      quat_inverse( QuatInv);                     // Inverse direction

      quat_mul( pPose[ iChild].rotation, QuatInv, pPose[ iChild].rotation); // Subtract from child
    }
  }

  // Retarget, Step 5:
  // For the animation editor display, have to change 'rotation' quaterion to 'angle'

  IqeB_AnimRotToAnglesFrame( pPose, NUI_SKELETON_POSITION_COUNT);

  // And update the draw model to show the updated poses

  DrawModelCurframe = 0;    // To get shure, reset to first frame always

  IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);

  // Data of selected joint has changed
  IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI

  return( 0 );   // return OK
}

/************************************************************************************
 * IqeB_GUI_ToolsAnimKinectCreateFirstConnected
 *
 * Get a first connection to the kinect
 *
 * return: indicates success or failure
 */

static HRESULT IqeB_GUI_ToolsAnimKinectCreateFirstConnected()
{
    INuiSensor * pNuiSensor;

    int iSensorCount = 0;

    HRESULT hr = NuiGetSensorCount(&iSensorCount);
    if (FAILED(hr))
    {
        return hr;
    }

    // Look at each Kinect sensor
    for (int i = 0; i < iSensorCount; ++i)
    {
        // Create the sensor so we can check status, if we can't create it, move on to the next
        hr = NuiCreateSensorByIndex(i, &pNuiSensor);
        if (FAILED(hr))
        {
            continue;
        }

        // Get the status of the sensor, and if connected, then we can initialize it
        hr = pNuiSensor->NuiStatus();
        if (S_OK == hr)
        {
            m_pNuiSensor = pNuiSensor;
            break;
        }

        // This sensor wasn't OK, so release it since we're not using it
        pNuiSensor->Release();
    }

    if (NULL != m_pNuiSensor)
    {
        // Initialize the Kinect and specify that we'll be using skeleton
        hr = m_pNuiSensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_SKELETON);
        if (SUCCEEDED(hr))
        {
            // Create an event that will be signaled when skeleton data is available
            m_hNextSkeletonEvent = CreateEventW(NULL, TRUE, FALSE, NULL);

            // Open a skeleton stream to receive skeleton data
            hr = m_pNuiSensor->NuiSkeletonTrackingEnable(m_hNextSkeletonEvent, 0);
        }
    }

    if (NULL == m_pNuiSensor || FAILED(hr))
    {
        //x/ SetStatusMessage(L"No ready Kinect found!");
        return E_FAIL;
    }

    return hr;
}

/************************************************************************************
 * IqeB_GUI_ToolsAnimKinectCloseConnected
 *
 * Close the connection to a kinect.
 *
 */

static void IqeB_GUI_ToolsAnimKinectCloseConnected()
{
  if( m_pNuiSensor) {

    m_pNuiSensor->NuiShutdown();
    m_pNuiSensor->Release();
    m_pNuiSensor = NULL;
  }

  if( m_hNextSkeletonEvent && (m_hNextSkeletonEvent != INVALID_HANDLE_VALUE)) {

    CloseHandle( m_hNextSkeletonEvent);
    m_hNextSkeletonEvent = NULL;          // Flag closed
  }

  // Clear kinect skeleton data
  memset( &Kinect_skeleton_data, 0, sizeof( Kinect_skeleton_data));
  memset( &Kinect_vFloorClipPlane, 0, sizeof( Kinect_vFloorClipPlane));
}

/************************************************************************************
 * IqeB_GUI_ToolsAnimKinectGetSkeleton
 *
 * Get skeleton data from the kinect.
 *
 * return: 0: no new skeleton data
 *         1: got new skeleton data
 */

static int IqeB_GUI_ToolsAnimKinectGetSkeleton()
{
  int i;
  HRESULT hr;
  NUI_SKELETON_FRAME skeletonFrame = {};

  hr = m_pNuiSensor->NuiSkeletonGetNextFrame( 0, &skeletonFrame);
  if ( FAILED(hr) ) {

    return( 0);  // no new skeleton data
  }

  // smooth out the skeleton data
  m_pNuiSensor->NuiTransformSmooth( &skeletonFrame, NULL);

  for( i = 0 ; i < NUI_SKELETON_COUNT; i++) {

    // We catch the first tracked skeleton

    if( skeletonFrame.SkeletonData[i].eTrackingState == NUI_SKELETON_TRACKED) {

      // Copy to kinect skeleton data
      memcpy( &Kinect_skeleton_data, skeletonFrame.SkeletonData + i, sizeof( Kinect_skeleton_data));
      memcpy( &Kinect_vFloorClipPlane, &skeletonFrame.vFloorClipPlane, sizeof( Kinect_vFloorClipPlane));

      // return to caller
      return( 1);  // got new skeleton data
    }
  }

  if( i >= NUI_SKELETON_COUNT) {    // Found none

    // Clear kinect skeleton data
    memset( &Kinect_skeleton_data, 0, sizeof( Kinect_skeleton_data));
    memset( &Kinect_vFloorClipPlane, 0, sizeof( Kinect_vFloorClipPlane));
  }

  return( 0);  // no new skeleton data
}

/************************************************************************************
 * IqeB_GUI_ToolsAnimKinectDrawSkeleton
 *
 * If this is called, the argument skel holds data of a tracked skeleton.
 * Draw it to the box widget.
 *
 */

#define KINECT_COL_TRACKED   fl_rgb_color(   0,  96,   0) // Bone/Joint was tracked
//x/#define KINECT_COL_INVERRED  fl_rgb_color(  96,  96,  96) // Bone/Joint track state Inferred
#define KINECT_COL_INVERRED  fl_rgb_color( 192,  0,  0) // Bone/Joint track state Inferred

// Hold points in 2D space on drawing the skeleton
static int SkelPointsX[ NUI_SKELETON_POSITION_COUNT], SkelPointsY[ NUI_SKELETON_POSITION_COUNT];

// Draw a skeleton bone to the 2D window
static void SkelDrawBone( const NUI_SKELETON_DATA &skel, NUI_SKELETON_POSITION_INDEX joint0, NUI_SKELETON_POSITION_INDEX joint1)
{
  NUI_SKELETON_POSITION_TRACKING_STATE joint0State = skel.eSkeletonPositionTrackingState[joint0];
  NUI_SKELETON_POSITION_TRACKING_STATE joint1State = skel.eSkeletonPositionTrackingState[joint1];

  // If we can't find either of these joints, exit
  if( joint0State == NUI_SKELETON_POSITION_NOT_TRACKED || joint1State == NUI_SKELETON_POSITION_NOT_TRACKED) {

    return;
  }

  // Don't draw if both points are inferred
  if( joint0State == NUI_SKELETON_POSITION_INFERRED && joint1State == NUI_SKELETON_POSITION_INFERRED) {

    return;
  }

  // We assume all drawn bones are inferred unless BOTH joints are tracked
  if( joint0State == NUI_SKELETON_POSITION_TRACKED && joint1State == NUI_SKELETON_POSITION_TRACKED) {

    fl_color( KINECT_COL_TRACKED);
    fl_line_style( 0, 3);           // line with of 3

    fl_line( SkelPointsX[joint0], SkelPointsY[joint0], SkelPointsX[joint1], SkelPointsY[joint1]);

  } else {

    fl_color( KINECT_COL_INVERRED);
    fl_line_style( 0);

    fl_line( SkelPointsX[joint0], SkelPointsY[joint0], SkelPointsX[joint1], SkelPointsY[joint1]);
  }
}

// Draw the complete skeleton
static void IqeB_GUI_ToolsAnimKinectDrawSkeleton( const NUI_SKELETON_DATA & skel, int windowWidth, int windowHeight)
{
  int i;

  for( i = 0; i < NUI_SKELETON_POSITION_COUNT; i++) {

    LONG x, y;
    USHORT depth;

    NuiTransformSkeletonToDepthImage( skel.SkeletonPositions[ i], &x, &y, &depth, NUI_IMAGE_RESOLUTION_1280x960);

    SkelPointsX[ i] = (int)( x *  windowWidth) / 1280;
    SkelPointsY[ i] = (int)( y * windowHeight) /  960;
  }

  // Render Torso
  SkelDrawBone(skel, NUI_SKELETON_POSITION_HEAD, NUI_SKELETON_POSITION_SHOULDER_CENTER);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SHOULDER_LEFT);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SHOULDER_RIGHT);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_SHOULDER_CENTER, NUI_SKELETON_POSITION_SPINE);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_SPINE, NUI_SKELETON_POSITION_HIP_CENTER);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_HIP_CENTER, NUI_SKELETON_POSITION_HIP_LEFT);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_HIP_CENTER, NUI_SKELETON_POSITION_HIP_RIGHT);

  // Left Arm
  SkelDrawBone(skel, NUI_SKELETON_POSITION_SHOULDER_LEFT, NUI_SKELETON_POSITION_ELBOW_LEFT);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_ELBOW_LEFT, NUI_SKELETON_POSITION_WRIST_LEFT);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_WRIST_LEFT, NUI_SKELETON_POSITION_HAND_LEFT);

  // Right Arm
  SkelDrawBone(skel, NUI_SKELETON_POSITION_SHOULDER_RIGHT, NUI_SKELETON_POSITION_ELBOW_RIGHT);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_ELBOW_RIGHT, NUI_SKELETON_POSITION_WRIST_RIGHT);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_WRIST_RIGHT, NUI_SKELETON_POSITION_HAND_RIGHT);

  // Left Leg
  SkelDrawBone(skel, NUI_SKELETON_POSITION_HIP_LEFT, NUI_SKELETON_POSITION_KNEE_LEFT);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_KNEE_LEFT, NUI_SKELETON_POSITION_ANKLE_LEFT);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_ANKLE_LEFT, NUI_SKELETON_POSITION_FOOT_LEFT);

  // Right Leg
  SkelDrawBone(skel, NUI_SKELETON_POSITION_HIP_RIGHT, NUI_SKELETON_POSITION_KNEE_RIGHT);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_KNEE_RIGHT, NUI_SKELETON_POSITION_ANKLE_RIGHT);
  SkelDrawBone(skel, NUI_SKELETON_POSITION_ANKLE_RIGHT, NUI_SKELETON_POSITION_FOOT_RIGHT);

  // Draw the joints in a different color

  for( i = 0; i < NUI_SKELETON_POSITION_COUNT; i++) {

    if( skel.eSkeletonPositionTrackingState[i] == NUI_SKELETON_POSITION_INFERRED ) {

      fl_color( KINECT_COL_INVERRED);
      fl_line_style( FL_CAP_ROUND, 7);    // joint diameter of 5

      fl_line( SkelPointsX[ i], SkelPointsY[ i], SkelPointsX[ i], SkelPointsY[ i]);

    } else if ( skel.eSkeletonPositionTrackingState[i] == NUI_SKELETON_POSITION_TRACKED ) {

      fl_color( KINECT_COL_TRACKED);
      fl_line_style( FL_CAP_ROUND, 7);    // joint diameter of 5

      fl_line( SkelPointsX[ i], SkelPointsY[ i], SkelPointsX[ i], SkelPointsY[ i]);
    }

  }

  // MUST reset line style to default.
  // Drawing system default was changed before!

  fl_line_style( 0);
}

/************************************************************************************
 * IqeB_GUI_CBox_SetValue_Callback
 */

static void IqeB_GUI_CBox_SetValue_Callback( Fl_Widget *w, void *pValueArg)
{
  int *pValue;
  Fl_Check_Button *pThis;

  // ...

  pThis  = (Fl_Check_Button *)w;
  pValue = (int *)pValueArg;             // get pointer to associated variable

  *pValue = pThis->value();              // update the variable

  if( pValueArg == &IqeB_ToolsAnimKinectEnable) {  // changed the 'Connect to kinect' checkbox

    if( pMyToolWin != NULL) {

      // Test for redraw

      IqeB_GUI_ToolsAnimKinectIdleAction( NULL);
    }

  } else if( pValueArg == &IqeB_ToolsAnimKinectDoRetarget) {  // changed the 'Do retarget' checkbox

    if( pMyToolWin != NULL) {

      // Test for redraw

      IqeB_GUI_ToolsAnimKinectIdleAction( NULL);
    }
  }

  MyWinUpdate();   // Update the GUI
}

/************************************************************************************
 * IqeB_GUI_Button_Callback
 */

#define ACTION_CODE_DO_RECORD         0  // Record an animation

static void IqeB_GUI_Button_Callback( Fl_Widget *w, void *ActionCodeArg)
{
  int ActionCode;

  if( pDrawModel == NULL) {              // have no draw model

    // Some kind of feedback to the user
	  fl_beep( FL_BEEP_ERROR);

    IqeB_DispMessage( DRAW_MSG_COL_ERR,
                      (char *)"No model loaded", NULL, NULL);

    return;                              // nothing to do
  }

  ActionCode = (int)ActionCodeArg;       // get action code

  switch( ActionCode) {
  case ACTION_CODE_DO_RECORD:   // Do skin binding, no smooth

    if( RecordState == KINECT_RECORD_OFF) {    //  Record is off

      // Enshure retarget is switched on

      if( ! IqeB_ToolsAnimKinectDoRetarget) {   // 'Do retarget' is off

        IqeB_ToolsAnimKinectDoRetarget = true;  // Switch to on

        if( pCBox_DoRetarget->value() != IqeB_ToolsAnimKinectDoRetarget) {
           pCBox_DoRetarget->value( IqeB_ToolsAnimKinectDoRetarget);
        }

        // Retarget service, this prepare retarget
        IqeB_Tools_Retarget_KinectToDrawModel( false);

        MyWinUpdate();
      }

      // Check that 'Do retarget' is on
      if( DoRetargetState != KINECT_DO_RETARGET_ON) {   // is not on

        // Problem starting recording

        // Some kind of feedback to the user
    	  fl_beep( FL_BEEP_ERROR);

        return;
      }

      // Start recording

      if( RecordWaitStartTime <= 0) {           // No start delay

        IqeB_Tools_Anim_RecordSequencer( KINECT_RECORD_DO);  // Go recording

      } else {   // Have to do start delay

        IqeB_Tools_Anim_RecordSequencer( KINECT_RECORD_WAIT_START);  // Start delay and recording
      }
    } else {                                   // Record is on

      IqeB_Tools_Anim_RecordSequencer( KINECT_RECORD_ACTION_STOP); // Stop recording
    }
    break;

  } // switch
}

/************************************************************************************
 * IqeB_GUI_Int_SetValue_Callback
 */

static void IqeB_GUI_Int_SetValue_Callback( Fl_Widget *w, void *pValueArg)
{
  int *pValue, Value, ValueBeforeClip;
  Fl_Int_Input *pThis;
  char TempBuffer[ 256];

  // ...

  pThis  = (Fl_Int_Input *)w;
  pValue = (int *)pValueArg;               // get pointer to associated variable

  Value = atoi( pThis->value());           // get the value
  ValueBeforeClip = Value;                 // Latch it

  if( pValue == &RecordWaitStartTime) {    // Range check for 'Start in [sec]'

    // Clipping
    if( Value <  0) Value =  0;
    if( Value > 30) Value = 30;
  }

  if( pValue == &RecordRecordTime) {       // Range check for 'Record [sec]'

    // Clipping
    if( Value <  1) Value =  1;
    if( Value > 60) Value = 60;
  }

  if( ValueBeforeClip != Value) {   // Was clipped

    // update GUI
    sprintf( TempBuffer, "%d", Value);    // Updat on GUI
    pThis->value( TempBuffer);
  }

  *pValue = Value;         // update the variable
}

/************************************************************************************
 * Presets for this tools window
 *
 */

 static T_GUI_PreferenceEntry MyPreferences[] =
 {

  // Kinect dialog

  { PREF_T_INT,   "KinectEnable",        "0", &IqeB_ToolsAnimKinectEnable},
  { PREF_T_INT,   "KinectDoRetarget",    "0", &IqeB_ToolsAnimKinectDoRetarget},
  { PREF_T_INT,   "KinectOnlyYaw",       "1", &IqeB_ToolsAnimKinectOnlyYaw},
  { PREF_T_INT,   "KinectCenterXY",      "1", &IqeB_ToolsAnimKinectCenterXY},
  { PREF_T_INT,   "RecordWaitStartTime", "5", &RecordWaitStartTime},
  { PREF_T_INT,   "RecordRecordTime",   "10", &RecordRecordTime},
};

// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "ToolsAnimKinect", MyPreferences, sizeof( MyPreferences) / sizeof( T_GUI_PreferenceEntry),
                                               (void **)(&pMyToolWin), &MyWinPosX, &MyWinPosY);


/************************************************************************************
 * Draw into the graphic window
 *
 */

static void MyGraphicWindowDisplay( int xArg, int yArg, int w, int h)
{
  int x, y;
  unsigned int TimeStampThis;
  Fl_Font CurrFont;
  Fl_Fontsize CurrFontSize;
  char *p, TempString[ 256];

  TimeStampThis = IqeB_Disp_GetTickCount();    // Current system time since startup in ms

  CurrFont = fl_font();                        // Current font
  CurrFontSize = fl_size();

  // Draw background

#ifdef use_again
  fl_color( IqeB_DispCol_BACKGROUND);
#else
  fl_color( FL_BACKGROUND2_COLOR);
#endif

  fl_rectf( xArg, yArg, w, h);
  fl_draw_box( FL_DOWN_FRAME, xArg - 2, yArg - 2, w + 4, h + 4, pGUI_GraphicWin->color());

  x = xArg;
  y = yArg;

  // Draw state of kinect connection

  x += 4;
  y += 4;

  fl_color( FL_BLACK);

  // Drawings depting from Kinect_Connect_state

  switch( Kinect_Connect_state) {

  case KINECT_STATE_DISABLED:    // Disabled connect to kinect

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE * 3 / 2);
    fl_color( FL_DARK_RED);

    y += ANIM_DRAW_FONT_SIZE * 3 / 2;
    fl_draw( "Off", x, y);

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore default font
    fl_color( FL_BLACK);

    y += ANIM_DRAW_FONT_SIZE;
    fl_draw( "Connect a kinect and set the", x, y);
    y += ANIM_DRAW_FONT_SIZE;
    fl_draw( "'Connect to kinect' checkbox.", x, y);

    break;

  case KINECT_STATE_CONNECTING:  // Wating for connection to kinect

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE * 3 / 2);
    fl_color( FL_DARK_RED);

    switch( (TimeStampThis >> 9) & 0x07) {
    case 0:
      p = (char *)"Connecting";
      break;
    case 1:
      p = (char *)"Connecting .";
      break;
    case 2:
      p = (char *)"Connecting ..";
      break;
    case 3:
      p = (char *)"Connecting ...";
      break;
    case 4:
      p = (char *)"Connecting ....";
      break;
    case 5:
      p = (char *)"Connecting .....";
      break;
    case 6:
      p = (char *)"Connecting ......";
      break;
    case 7:
      p = (char *)"Connecting .......";
      break;
    }

    y += ANIM_DRAW_FONT_SIZE * 3 / 2;
    fl_draw( p, x, y);

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore default font
    fl_color( FL_BLACK);
    y += ANIM_DRAW_FONT_SIZE;
    fl_draw( "Wait for a kinect to connect.", x, y);

    break;

  case KINECT_STATE_CONNECTED:   // Connected to kinect

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE * 3 / 2);
    fl_color( FL_DARK_RED);

    y += ANIM_DRAW_FONT_SIZE * 3 / 2;

    if( Kinect_skeleton_data.eTrackingState == NUI_SKELETON_TRACKED) {  // Got skeleton data

      fl_draw( "Connected", x, y);

      y += ANIM_DRAW_FONT_SIZE;
      y += ANIM_DRAW_FONT_SIZE;

    } else {

      fl_draw( "Connected", x, y);

      y += ANIM_DRAW_FONT_SIZE * 3 / 2;
      fl_draw( "NO skeleton data", x, y);

      fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore default font
      fl_color( FL_BLACK);
      y += ANIM_DRAW_FONT_SIZE;
      y += ANIM_DRAW_FONT_SIZE;
      fl_draw( "Place a human before the kinect.", x, y);
      y += ANIM_DRAW_FONT_SIZE;
    }

    // Display 'Do retarget' errors

    if( DoRetargetState == KINECT_DO_RETARGET_ERROR &&   // Has an error
        DoRetargetErrorString[ 0] != '\0') {             // snd error string is set

      y += ANIM_DRAW_FONT_SIZE;

      fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore default font
      fl_color( fl_rgb_color( 192,  0,  0));
      fl_draw( "'Do retarget' error:", x, y);
      y += ANIM_DRAW_FONT_SIZE;
      fl_draw( DoRetargetErrorString, x, y);

    } else if( DoRetargetState == KINECT_DO_RETARGET_ON) {  // Setup is OK

      y += ANIM_DRAW_FONT_SIZE;

      fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore default font
      fl_color( fl_rgb_color(   0, 192,   0));
      fl_draw( "'Do retarget': on", x, y);

    } else if( DoRetargetState == KINECT_DO_RETARGET_OFF) {  // Setup is OK

      y += ANIM_DRAW_FONT_SIZE;

      fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore default font
      fl_color( fl_rgb_color( 192,  0,  0));
      fl_draw( "'Do retarget': off", x, y);
    }

    // Display frame per seconds

    if( KinectFramesPerSecondString[0] != '\0') {    // Have something to display

      int dx, dy, wo, ho;

      fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore default font
      fl_color( FL_BLACK);

      fl_text_extents( KinectFramesPerSecondString, dx, dy, wo, ho);   // measure extents
      fl_draw( KinectFramesPerSecondString, xArg + w - wo - 3, yArg + ANIM_DRAW_FONT_SIZE);
    }

    // Display 'Recored' timers'

    if( RecordState == KINECT_RECORD_WAIT_START) {       // Record waits for start time gone

      sprintf( TempString, "Wait  %3d sec.", (int)(RecordTimeRemain + 0.5));

      fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE * 2);
      fl_color( FL_BLACK);
      fl_draw( TempString, x, h - 2);

    } else if( RecordState == KINECT_RECORD_DO) {        // Recording

      sprintf( TempString, "Record %3d sec.", (int)(RecordTimeRemain + 0.5));

      fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE * 2);
      fl_color( FL_RED);
      fl_draw( TempString, x, h - 2);
    }

    // Draw the skeleton

    if( Kinect_skeleton_data.eTrackingState == NUI_SKELETON_TRACKED) {  // Got skeleton data

      IqeB_GUI_ToolsAnimKinectDrawSkeleton( Kinect_skeleton_data, w, h);
    }

    break;

  default:                       // Not expected state

    break;
  }

  // ...

  fl_font( CurrFont, CurrFontSize);  // Restore default font

}

/************************************************************************************
 * IqeB_GUI_ToolsAnimKinectIdleAction
 */

static void IqeB_GUI_ToolsAnimKinectIdleAction( void *)
{
  static unsigned int TimeStampDoRedraw;       // Must be static
  static unsigned int TimeStampTryConnect;     // Must be static
  unsigned int TimeStampThis;
  int DoRedraw, Kinect_Connect_state_Last;
  // Variables to measure kinect data frames per second (fps)
  static unsigned int TimeStampLastFrame;      // Must be static
  static unsigned int TimeStampFrameMeas;      // must be static
  static unsigned int nFrames;                 // must be static
  static unsigned int FrameTimeSum;            // must be static
  unsigned int FrameTimeDelta;

  // ....

  if( pMyToolWin == NULL) {     // Security testm window must exist

    return;
  }

  //
  // update preferences all 20 ms
  //

  DoRedraw = false;
  Kinect_Connect_state_Last = Kinect_Connect_state;  // remember last state value

  TimeStampThis = IqeB_Disp_GetTickCount();
  if( TimeStampThis == 0) {   // May not be 0, timer value is used as flag in this function somewhere
    TimeStampThis = 0;
  }

  if( TimeStampThis - TimeStampDoRedraw >= 200) {  // some time gone since last call

    DoRedraw = true;                               // Flag redraw
    TimeStampDoRedraw = TimeStampThis;             // Rememember last tick

    // This is a good moment to keep the GUI updated
    MyWinUpdate();   // Update the GUI
  }

  //
  // Manage State changes
  //

  switch( Kinect_Connect_state) {

  case KINECT_STATE_DISABLED:    // Disabled connect to kinect

    if( IqeB_ToolsAnimKinectEnable) {                  // Enabled checkbox, connection to kinect

      Kinect_Connect_state = KINECT_STATE_CONNECTING;  // Go connecting

      TimeStampTryConnect = TimeStampThis - 9999;      // Reset time stamp
    }

    break;

  case KINECT_STATE_CONNECTING:  // Wating for connection to kinect

    if( ! IqeB_ToolsAnimKinectEnable) {                // Disnabled checkbox, connection to kinect

      Kinect_Connect_state = KINECT_STATE_DISABLED;    // Go connection disabled

      // Close any connection
      IqeB_GUI_ToolsAnimKinectCloseConnected();
      break;
    }

    // Try connect

    if( TimeStampThis - TimeStampTryConnect >= 500) {   // 500 ms gone since last call

      TimeStampTryConnect = TimeStampThis;             // Rememember last tick

      // Try connect to kinect
      IqeB_GUI_ToolsAnimKinectCreateFirstConnected();

      if( m_pNuiSensor != NULL) {    // Got connection

        Kinect_Connect_state = KINECT_STATE_CONNECTED;    // We connected now
        break;
      }
    }

    break;

  case KINECT_STATE_CONNECTED:   // Connected to kinect

    if( ! IqeB_ToolsAnimKinectEnable) {                // Disnabled checkbox, connection to kinect

      Kinect_Connect_state = KINECT_STATE_DISABLED;    // Go connection disabled

      // Close any connection
      IqeB_GUI_ToolsAnimKinectCloseConnected();
      break;
    }

    // Lost connection to kinect ?

    if( m_pNuiSensor == NULL ||        // Lost connection or something other is not OK
        m_hNextSkeletonEvent == NULL) {

      // Close any connection to kinect
      IqeB_GUI_ToolsAnimKinectCloseConnected();

      Kinect_Connect_state = KINECT_STATE_DISABLED;    // Go connection disabled

      break;
    }

    // Poll the kinect for data

    while( WaitForSingleObject( m_hNextSkeletonEvent, 0) == WAIT_OBJECT_0) {

      if( TimeStampLastFrame != 0) {    // Was a frame before (have a time delta)

        FrameTimeDelta = TimeStampThis - TimeStampLastFrame;
        if( FrameTimeDelta < 1) {  // Max not be 0
          FrameTimeDelta = 1;
        }
      } else {

        FrameTimeDelta = 0;
      }

      if( IqeB_GUI_ToolsAnimKinectGetSkeleton() == 1) {  // Got skeleton data

        // Retarget kinect skeleton to current drawn model
        IqeB_Tools_Retarget_KinectToDrawModel( true);

        // Record animation
        if( RecordState == KINECT_RECORD_DO &&                // Still recording
            (RecordFrameDone == 0 || FrameTimeDelta > 0)) {   // First frame or any time delta

          IqeB_Tools_Anim_RecordAddFrame( pDrawModelCurAnim, FrameTimeDelta, NULL);
        }
      } else {

        // Retarget service
        IqeB_Tools_Retarget_KinectToDrawModel( false);

        // Pause animation
        if( RecordState == KINECT_RECORD_DO) {

          IqeB_Tools_Anim_RecordAddFrame( NULL, 0, NULL);
        }
      }

      // Measure frame time

      if( TimeStampLastFrame != 0) {    // Was a frame before (have a time delta)

        if( TimeStampThis + 2000 < TimeStampFrameMeas) {
		      TimeStampFrameMeas = TimeStampThis + 1000;
          nFrames = 0;
          FrameTimeSum = 0;
        }

        nFrames += 1;
        FrameTimeSum += FrameTimeDelta;   // Sum up frame delta

	      if( TimeStampThis > TimeStampFrameMeas) {

          sprintf( KinectFramesPerSecondString, "%3d fps", (int)((1000.0 * (float)nFrames) / (float)FrameTimeSum + 0.5));

		      TimeStampFrameMeas = TimeStampThis + 1000;
          nFrames = 0;
          FrameTimeSum = 0;
        }
      } else {

        // Started measuring
        strcpy( KinectFramesPerSecondString, "... fps");
      }

      TimeStampLastFrame = TimeStampThis;

      // ...

      DoRedraw = true;                           // Flag redraw
    }

    break;

  default:                       // Not expected state

    Kinect_Connect_state = KINECT_STATE_DISABLED;  // Switch to sane state

    // Close any connection
    IqeB_GUI_ToolsAnimKinectCloseConnected();

    break;
  }

  //
  // Reset kinect 'frame per seconds measure' if not connected to the kinect
  //

  if( Kinect_Connect_state != KINECT_STATE_CONNECTED) {

    TimeStampLastFrame = 0;    // Was a frame before (have a time delta)

    TimeStampFrameMeas = TimeStampThis;
    nFrames = 0;
    FrameTimeSum = 0;
    KinectFramesPerSecondString[ 0] = '\0';  // Set string to empty
  }

  //
  // Switch 'Do retarget' state to off if not connected to the kinect
  //

  if( Kinect_Connect_state != KINECT_STATE_CONNECTED) {

    DoRetargetState = KINECT_DO_RETARGET_OFF; // 'Do retarget' state to off
    strcpy( DoRetargetErrorString, "");       // Error String to off
  }

  //
  // Recored sequencer service
  //

  IqeB_Tools_Anim_RecordSequencer( KINECT_RECORD_ACTION_TEST);

  //
  // have to redraw the window
  //

  if( Kinect_Connect_state_Last != Kinect_Connect_state) {  // state has changed since entry

    DoRedraw = true;                       // Flag redraw
  }

  if( DoRedraw) {                          // Have to redraw and window is existing

    pGUI_GraphicWin->redraw();             // Redraw the graphic window
  }
}

/************************************************************************************
 * IqeB_GUI_UpdateCallback
 *
 * Is called from IqeB_GUI_UpdateWidgets() if something has changed
 * what needs an update (new model displayed, ...).
 */

static void IqeB_GUI_UpdateCallback( int UpdateFlags)
{

  if( pMyToolWin == NULL) {  // security test, has main window

    return;
  }

  if( UpdateFlags & IQE_GUI_UPDATE_MODEL_CHANGE) {    // The model has changed

    // After model change, always switch retarget things to off
    DoRetargetState = KINECT_DO_RETARGET_OFF; // 'Do retarget' state to off
    strcpy( DoRetargetErrorString, "");       // Error String to off
  }

  MyWinUpdate();               // Update the GUI
}

/************************************************************************************
 * close_cb, close this window
 *
 * pValueArg is a pointer to the widget. Set this pointer to NULL on deletion.
 */

static void close_cb( Fl_Widget *w, void *pValueArg)
{

  // Remove window idle action

  Fl::remove_idle( IqeB_GUI_ToolsAnimKinectIdleAction);   // Remove idle function

  // Abort any active records
  IqeB_Tools_Anim_RecordSequencer( KINECT_RECORD_ACTION_ABORT);

  // Close any connection to kinect
  IqeB_GUI_ToolsAnimKinectCloseConnected();

  Kinect_Connect_state = KINECT_STATE_DISABLED;    // Go connection disabled

  // Always switch retarget things to off
  DoRetargetState = KINECT_DO_RETARGET_OFF; // 'Do retarget' state to off
  strcpy( DoRetargetErrorString, "");       // Error String to off

  // ...

  IqeB_GUI_CloseToolWindow( (void **)&pMyToolWin);
}

/************************************************************************************
 * IqeB_GUI_ToolsAnimKinectWin
 *
 * Open dialog
 */

void IqeB_GUI_ToolsAnimKinectWin( int xLeft, int xRight, int yTop, int yBotton)
{

  //
  // winodow already created --> show it
  //

  if( pMyToolWin != NULL) {         // already have tool window

    // Show invisble window or bring visible window to foreground

    pMyToolWin->show();          // show it

    return;
  }

  //
  // creation of window on first call
  //

  if( pMyToolWin == NULL) {  // no tool window until now

    int xPos, yPos;

    xPos = xRight;
    yPos = yTop;

    if( MyWinPosX != IQE_GUI_NO_WINPOS_X && MyWinPosY != IQE_GUI_NO_WINPOS_Y) { // have last window position

      xPos = MyWinPosX;
      yPos = MyWinPosY;
    }

    // NOTE: Kinect-Window needs a double buffered window to get no flicker on updates.
    pMyToolWin = new Fl_Double_Window( xPos, yPos, IQE_GUI_TOOLS_STD_WITDH, 400, "Animation/Kinect");

    if( pMyToolWin == NULL) {  // security test

      return;
    }

    IqeB_GUI_RegisterCallback( (void **)&pMyToolWin, IqeB_GUI_UpdateCallback, IQE_GUI_UPDATE_MODEL_CHANGE,
                               0, (void(*)(void *, void *))close_cb);
  }

  // Add idle action for this window

  Fl::add_idle( IqeB_GUI_ToolsAnimKinectIdleAction);      // Redraw window during idle

  //
  //  GUI things
  //

  int x1, xx, xx3, y, yy;
  char TempBuffer[ 256];

  Fl_Check_Button *pTemp_Check_Button;
  Fl_Box          *pTemp_Box;

  x1  = 4;

  xx  = (pMyToolWin->w() - 8);
  xx3 = (xx + 2) / 3;
  //x/xx4 = (xx + 3) / 4;
  //x/x2  = x1 + xx2 / 2;
  yy  = 30;

  y = 4;

  // Graphic drawing window is created now

  pGUI_GraphicWin = new MyGraphicWindow( x1, y, xx, xx);

  y += xx;
  y += 4;

  // checkbox 'Connect to kinect'

  yy  = 24;

  pCBox_Connect = new Fl_Check_Button( x1, y, xx, yy, "Connect to kinect");
  pCBox_Connect->value( IqeB_ToolsAnimKinectEnable);
  pCBox_Connect->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_ToolsAnimKinectEnable);
  pCBox_Connect->tooltip( "Check this to search for a connected kinect device.\n"
                          "The connection state is shown on the drawing area.\n"
                          "If connected to the kinect and there is a human\n"
                          "before the kinect, a 'live' skeleton is shown.");

  y += yy;

  // checkbox 'Do retarget'

  pCBox_DoRetarget = new Fl_Check_Button( x1, y, xx, yy, "Do retarget");
  pCBox_DoRetarget->value( IqeB_ToolsAnimKinectDoRetarget);
  pCBox_DoRetarget->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_ToolsAnimKinectDoRetarget);
  pCBox_DoRetarget->tooltip( "Check this to pose the model shown."
                             "Retarget the kinect skeleton to the current shown model.\n"
                             "On Retarget, bone angels are transfered from the kinect\n"
                             "skeleton to the model skeleton.\n"
                             "The model must match this criterias:\n"
                             "* The skeleton must be structured like the Kinect skeleton.\n"
                             "  --> Same number of joints, same hierarchy.\n"
                             "* The skeleton must be normalised.\n"
                             "  --> See 'Normalize' button elsewhere.\n"
                             "* The model and the skeleton must be oriented towards\n"
                             "  positive X-axis.\n"
                             "* The model must be rigged (must have a skin binding).\n"
                             "NOTE:  An animation 'Kinect-Live' is created\n"
                             "to pass the poses to the model.");

  y += yy;

  // Seperator line

  y += 2;

  pTemp_Box = new Fl_Box( x1, y, xx, 2, "Options");
  pTemp_Box->align( FL_ALIGN_TOP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
  pTemp_Box->box( FL_DOWN_FRAME);
  pTemp_Box->labelsize( 12);

  y += 16;

  // checkbox 'Yaw only'

  pTemp_Check_Button = new Fl_Check_Button( x1, y, xx / 2 - 2, yy, "Yaw only");
  pTemp_Check_Button->value( IqeB_ToolsAnimKinectOnlyYaw);
  pTemp_Check_Button->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_ToolsAnimKinectOnlyYaw);
  pTemp_Check_Button->tooltip( "If set, only the yaw angle is used to rotate the model.\n"
                               "If unset, pitch and roll is used too.");

  // checkbox 'Yaw only'

  pTemp_Check_Button = new Fl_Check_Button( x1 + xx / 2, y, xx / 2, yy, "Center XY");
  pTemp_Check_Button->value( IqeB_ToolsAnimKinectCenterXY);
  pTemp_Check_Button->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_ToolsAnimKinectCenterXY);
  pTemp_Check_Button->tooltip( "If set, only the Z position (height) is applied to the model.\n"
                               "If unset, the X and Y postion is applied too.");

  y += yy;

  // Seperator line

  y += 2;

  pTemp_Box = new Fl_Box( x1, y, xx, 2, "Record");
  pTemp_Box->align( FL_ALIGN_TOP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
  pTemp_Box->box( FL_DOWN_FRAME);
  pTemp_Box->labelsize( 12);

  y += 18;

  // Record button

  pGUI_ButRecordAnim = new Fl_Button( x1, y, xx3 - 2, yy, "Go");
  pGUI_ButRecordAnim->callback( IqeB_GUI_Button_Callback, (void*)(ACTION_CODE_DO_RECORD));
  pGUI_ButRecordAnim->tooltip( "Record an animation.\n"
                               "* Checkboxes 'Connect to kinect' and\n"
                               "  'Do retarget' must be set.\n"
                               "* A kinect must be connected.\n"
                               "* A suitable model must have been loaded."
                               "NOTE: An animation 'Kinect-Record' is created\n"
                               "and used to store the session.\n");

  // int input 'Start in [sec]'

  pInt_InputWaitStart = new Fl_Int_Input( x1 + xx3 * 1 + 4, y + 2, xx3 - 4, yy - 2, "Start in [sec]");
  pInt_InputWaitStart->align( FL_ALIGN_TOP_LEFT);     // align for label
  pInt_InputWaitStart->labelsize( 10);
  sprintf( TempBuffer, "%d", RecordWaitStartTime);
  pInt_InputWaitStart->value( TempBuffer);
  pInt_InputWaitStart->callback( IqeB_GUI_Int_SetValue_Callback, &RecordWaitStartTime);
  pInt_InputWaitStart->tooltip( "Delay (in seconds) until recording starts.");

  // int input 'Start in [sec]'

  pInt_InputRecordTime = new Fl_Int_Input( x1 + xx3 * 2 + 4, y + 2, xx3 - 4, yy - 2, "Record [sec]");
  pInt_InputRecordTime->align( FL_ALIGN_TOP_LEFT);     // align for label
  pInt_InputRecordTime->labelsize( 10);
  sprintf( TempBuffer, "%d", RecordRecordTime);
  pInt_InputRecordTime->value( TempBuffer);
  pInt_InputRecordTime->callback( IqeB_GUI_Int_SetValue_Callback, &RecordRecordTime);
  pInt_InputRecordTime->tooltip( "Time to record (in seconds).");

  y += yy;

  // finish up

  pMyToolWin->size( pMyToolWin->w(), y + 4);  // final size of window

  pMyToolWin->end();
  pMyToolWin->set_non_modal();
  pMyToolWin->callback( close_cb, &pMyToolWin);

  // After windows open, always switch retarget things to off
  DoRetargetState = KINECT_DO_RETARGET_OFF; // 'Do retarget' state to off
  strcpy( DoRetargetErrorString, "");       // Error String to off

  // Set record state to off
  RecordState = KINECT_RECORD_OFF;          // 'Record' state

  // ...

  MyWinUpdate(); // Update the GUI

  pMyToolWin->show();
}

/********************************** End Of File **********************************/
