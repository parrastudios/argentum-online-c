/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_THREAD_LOCAL_H
#define SYSTEM_THREAD_LOCAL_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <System/Mutex.h>

#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
#	define WIN32_LEAN_AND_MEAN
#	include <windows.h>
#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_MACOS || PLATFORM_TYPE == PLATFORM_BSD
#	include <pthread.h>
#endif

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
	typedef DWORD ThreadLocalHandle;
#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_MACOS || PLATFORM_TYPE == PLATFORM_BSD
	typedef pthread_key_t ThreadLocalHandle;
#endif

typedef ThreadLocalHandle ThreadLocal;

////////////////////////////////////////////////////////////
/// Create thread local
////////////////////////////////////////////////////////////
void ThreadLocalInitialize(ThreadLocal * Local);

////////////////////////////////////////////////////////////
/// Destroy thread local
////////////////////////////////////////////////////////////
void ThreadLocalDestroy(ThreadLocal * Local);

////////////////////////////////////////////////////////////
/// Get current value
////////////////////////////////////////////////////////////
void * ThreadLocalGetValue(ThreadLocal * Local);

////////////////////////////////////////////////////////////
/// Set current value
////////////////////////////////////////////////////////////
void ThreadLocalSetValue(ThreadLocal * Local, void * Value);

#endif // SYSTEM_THREAD_LOCAL_H
