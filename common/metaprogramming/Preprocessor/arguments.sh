#!/bin/bash

# script includes
include_path=$(dirname $0)/..

source $include_path/module.sh

# arguments output path
file_name=Arguments.h

# arguments module name
module_name="PREPROCESSOR_ARGUMENTS"

# script variables
arg_count_size=64
generated_seq="	"
line_align=10

# remove old file
if [ -f ${file_name} ]; then
	rm ${file_name}
	echo "removed ${file_name}"
fi

# write copyright
module_copyright ${file_name}

# write header
module_header ${file_name} ${module_name}

# write definition section
module_section ${file_name} "Definitions"

# write argument definitions
echo "#define ${module_name}_COUNT_SIZE	${arg_count_size}" >> ${file_name}
echo >> ${file_name}
echo "#define ${module_name}_COUNT_SEQ_IMPL() \\" >> ${file_name}

for ((i = ${arg_count_size} - 1; i > 0; --i)); do

	if ((${i} % ${line_align} == 0)); then
		echo "${generated_seq}${i}, \\" >> ${file_name}
		generated_seq=""
	elif ((${i} % ${line_align} == ((${line_align} - 1)) )); then
		generated_seq="${generated_seq}	${i}, "
	else
		generated_seq="${generated_seq}${i}, "
	fi

done

echo "${generated_seq}0" >> ${file_name}

echo >> ${file_name}

# write macro section
module_section ${file_name} "Macros"

echo "#define ${module_name}_STRINGIFY_IMPL(...) # __VA_ARGS__" >> ${file_name}
echo "#define ${module_name}_STRINGIFY(...) ${module_name}_STRINGIFY_IMPL(__VA_ARGS__)" >> ${file_name}

echo >> ${file_name}

echo "#define ${module_name}_N_IMPL( \\" >> ${file_name}

generated_seq=""

for ((i = 1; i < ${arg_count_size}; ++i)); do

	if ((${i} % ${line_align} == 0)); then
		echo "${generated_seq}_${i}, \\" >> ${file_name}
		generated_seq=""
	elif ((${i} % ${line_align} == 1)); then
		generated_seq="${generated_seq}	_${i}, "
	else
		generated_seq="${generated_seq}_${i}, "
	fi

done

echo "${generated_seq}N, ...) N" >> ${file_name}

echo >> ${file_name}

echo "#define ${module_name}_COUNT_IMPL(...) \\" >> ${file_name}
echo "	${module_name}_N_IMPL(__VA_ARGS__)" >> ${file_name}

echo >> ${file_name}

echo "#define ${module_name}_COUNT(...) \\" >> ${file_name}
echo "	(${module_name}_COUNT_IMPL(__VA_ARGS__, ${module_name}_COUNT_SEQ_IMPL()) - \\" >> ${file_name}
echo "	(sizeof(${module_name}_STRINGIFY(__VA_ARGS__)) == 1))" >> ${file_name}

echo >> ${file_name}

# write footer
module_footer ${file_name} ${module_name}
