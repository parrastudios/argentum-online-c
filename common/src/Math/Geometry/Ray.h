/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef MATH_GEOMETRY_RAY_H
#define MATH_GEOMETRY_RAY_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Vector.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define RAY_DEFAULT_LENGTH		2.0f

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct RayType
{
	struct Vector3f		Origin;
	struct Vector3f		Direction;
	float				Length;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

struct RayType * RayInitialize(struct RayType * Ray, struct Vector3f * Origin, struct Vector3f * Direction);

struct RayType * RaySet(struct RayType * Ray, struct Vector3f * PositionA, struct Vector3f * PositionB);

struct Vector3f * RaySetOrigin(struct RayType * Ray, struct Vector3f * Origin);

struct Vector3f * RaySetDirection(struct RayType * Ray, struct Vector3f * Direction);

float RaySetLength(struct RayType * Ray, float Length);

#endif // MATH_GEOMETRY_RAY_H
