/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	PLUGIN_LOADER_IMPL_H
#define PLUGIN_LOADER_IMPL_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Plugin/PluginLoader.h>
#include <DataType/HashMap.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
enum PluginLoaderTypeInfo
{
	PLUGIN_LOADER_TYPE_LIBRARY = 0x00,
	PLUGIN_LOADER_TYPE_SCRIPT = 0x01,

	PLUGIN_LOADER_TYPE_SIZE,
	PLUGIN_LOADER_TYPE_INVALID
};

typedef uinteger PluginLoaderTypeInfo;

typedef void * PluginLoaderImplResourceType;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct PluginLoaderImplType
{
	bool (*Initialize)(struct PluginLoaderImplType *);
	void (*Destroy)(struct PluginLoaderImplType *);
	bool (*Load)(struct PluginLoaderImplType *, PluginId, PluginLoaderImplResourceType *);
	bool (*LoadPath)(struct PluginLoaderImplType *, const char *, List);
	bool (*Register)(struct PluginLoaderImplType *, PluginLoaderImplResourceType, PluginType *);
	bool (*Unload)(struct PluginLoaderImplType *, PluginLoaderImplResourceType);
	void (*Clear)(struct PluginLoaderImplType *);

	bool Initialized;

	HashMap ResourceList;
};

#endif // PLUGIN_LOADER_IMPL
