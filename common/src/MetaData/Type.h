/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	METADATA_TYPE_H
#define METADATA_TYPE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <Preprocessor/Concatenation.h>
#include <Preprocessor/For.h>

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define MetadataTypeDecl(Type) \
	PREPROCESSOR_CONCAT(METADATA_TYPE_, Type)

#define MetadataTypePromotionDecl(Type) \
	PREPROCESSOR_CONCAT(METADATA_TYPE_PROMOTION_, Type)

#define MetadataTypeNull(Type) \
	PREPROCESSOR_CONCAT(METADATA_TYPE_NULL_, Type)

#define MetadataTypeDeclPtr(Type) \
	MetadataTypeDecl(Type) *

#define MetadataTypeId(Type) \
	PREPROCESSOR_CONCAT(METADATA_TYPE_ID_, Type)

#define MetadataTypeSizeOf(Type) \
	PREPROCESSOR_CONCAT(METADATA_TYPE_SIZE_, Type)

#define MetadataTypeCast(Type) \
	(MetadataTypeDecl(Type))

#define MetadataTypeCastPtr(Type) \
	(MetadataTypeDeclPtr(Type))

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Metadata type x macro
////////////////////////////////////////////////////////////
#define METADATA_TYPE_DECL(Entry) \
	Entry(BOOLEAN, Boolean, 0x00) \
	Entry(UINT8, UInt8, 0x01) \
	Entry(INT8, Int8, 0x02) \
	Entry(UINT16, UInt16, 0x03) \
	Entry(INT16, Int16, 0x04) \
	Entry(UINT32, UInt32, 0x05) \
	Entry(INT32, Int32, 0x06) \
	Entry(UINT64, UInt64, 0x07) \
	Entry(INT64, Int64, 0x08) \
	Entry(FLOAT, Float, 0x09) \
	Entry(DOUBLE, Double, 0x0A)

////////////////////////////////////////////////////////////
/// Metadata implicit types
////////////////////////////////////////////////////////////
#define METADATA_TYPE_BOOLEAN				bool
#define METADATA_TYPE_UINT8					uint8
#define METADATA_TYPE_INT8					int8
#define METADATA_TYPE_UINT16				uint16
#define METADATA_TYPE_INT16					int16
#define METADATA_TYPE_UINT32				uint32
#define METADATA_TYPE_INT32					int32
#define METADATA_TYPE_UINT64				uint64
#define METADATA_TYPE_INT64					int64
#define METADATA_TYPE_FLOAT					float32
#define METADATA_TYPE_DOUBLE				float64


////////////////////////////////////////////////////////////
/// Metadata implicit promotion types
////////////////////////////////////////////////////////////
#define METADATA_TYPE_PROMOTION_BOOLEAN			uinteger
#define METADATA_TYPE_PROMOTION_UINT8			uinteger
#define METADATA_TYPE_PROMOTION_INT8			integer
#define METADATA_TYPE_PROMOTION_UINT16			uinteger
#define METADATA_TYPE_PROMOTION_INT16			integer
#define METADATA_TYPE_PROMOTION_UINT32			uinteger
#define METADATA_TYPE_PROMOTION_INT32			integer
#define METADATA_TYPE_PROMOTION_UINT64			uinteger
#define METADATA_TYPE_PROMOTION_INT64			integer
#define METADATA_TYPE_PROMOTION_FLOAT			float64
#define METADATA_TYPE_PROMOTION_DOUBLE			float64

////////////////////////////////////////////////////////////
/// Metadata implicit null types
////////////////////////////////////////////////////////////
#define METADATA_TYPE_NULL_BOOLEAN				false
#define METADATA_TYPE_NULL_UINT8				UINT8_SUFFIX(0x00)
#define METADATA_TYPE_NULL_INT8					INT8_SUFFIX(0x00)
#define METADATA_TYPE_NULL_UINT16				UINT16_SUFFIX(0x0000)
#define METADATA_TYPE_NULL_INT16				INT16_SUFFIX(0x0000)
#define METADATA_TYPE_NULL_UINT32				UINT32_SUFFIX(0x00000000)
#define METADATA_TYPE_NULL_INT32				INT32_SUFFIX(0x00000000)
#define METADATA_TYPE_NULL_UINT64				UINT64_SUFFIX(0x0000000000000000)
#define METADATA_TYPE_NULL_INT64				INT64_SUFFIX(0x0000000000000000)
#define METADATA_TYPE_NULL_FLOAT				FLOAT_SUFFIX(0.0)
#define METADATA_TYPE_NULL_DOUBLE				DOUBLE_SUFFIX(0.0)

////////////////////////////////////////////////////////////
/// Metadata type sizes
////////////////////////////////////////////////////////////
#define METADATA_TYPE_SIZE_BOOLEAN			sizeof(bool)
#define METADATA_TYPE_SIZE_UINT8			sizeof(uint8)
#define METADATA_TYPE_SIZE_INT8				sizeof(int8)
#define METADATA_TYPE_SIZE_UINT16			sizeof(uint16)
#define METADATA_TYPE_SIZE_INT16			sizeof(int16)
#define METADATA_TYPE_SIZE_UINT32			sizeof(uint32)
#define METADATA_TYPE_SIZE_INT32			sizeof(int32)
#define METADATA_TYPE_SIZE_UINT64			sizeof(uint64)
#define METADATA_TYPE_SIZE_INT64			sizeof(int64)
#define METADATA_TYPE_SIZE_FLOAT			sizeof(float32)
#define METADATA_TYPE_SIZE_DOUBLE			sizeof(float64)
#define METADATA_TYPE_SIZE_INVALID			0xFFFFFFFFUL

////////////////////////////////////////////////////////////
/// Metadata type id declaration
////////////////////////////////////////////////////////////
enum MetadataTypeNameInfo
{
	#define MetadataTypeIdDecl(Type, Name, Id) \
		MetadataTypeId(Type) = Id,

	METADATA_TYPE_DECL(MetadataTypeIdDecl)

	#undef MetadataTypeIdDecl

	METADATA_TYPE_ID_SIZE,

	METADATA_TYPE_ID_INVALID = UINTEGER_MAX_RANGE
};

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef uinteger MetadataType;
typedef uinteger MetadataTypeSize;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get size of type name
////////////////////////////////////////////////////////////
MetadataTypeSize MetadataTypeGetSize(MetadataType Type);

#endif // METADATA_TYPE_H
