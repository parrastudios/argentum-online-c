/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GUI_WIDGET_H
#define GUI_WIDGET_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Window/Event.h>

#include <GUI/Form.h>
#include <GUI/Button.h>
#include <GUI/TextBox.h>
#include <GUI/Label.h>
#include <GUI/Console.h>
#include <GUI/List.h>
#include <GUI/Shape.h>
#include <GUI/Table.h>

////////////////////////////////////////////////////////////
// Widget type definitions
////////////////////////////////////////////////////////////
#define GUI_WIDGET_DEFAULT				0x00

#define GUI_WIDGET_ERROR				0x00
#define GUI_WIDGET_FORM					0x01
#define GUI_WIDGET_BUTTON				0x02
#define GUI_WIDGET_TEXTBOX				0x03
#define GUI_WIDGET_CONSOLE				0x04
#define GUI_WIDGET_SCROLL				0x05
#define GUI_WIDGET_LABEL				0x06
#define GUI_WIDGET_LIST					0x07
#define GUI_WIDGET_IMAGE				0x08
#define GUI_WIDGET_SHAPE				0x09
#define GUI_WIDGET_TABLE				0x0A

#define GUI_WIDGET_NAME_LENGTH			0x40
#define GUI_WIDGET_TEXT_LENGTH			0x40

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct GuiWidget
{
	char		Name[GUI_WIDGET_NAME_LENGTH];	///< Name
	char		Text[GUI_WIDGET_TEXT_LENGTH];	///< Text
	uinteger	Length;							///< Length of characters in the text
	uinteger	FontSize;						///< Font size

	integer		X, Y;							///< Position
	integer		Width, Height;					///< Size

	bool		Enabled;						///< If it's available or not
	bool		Visible;						///< If it's visible or not
	bool		Border;							///< If it has border or not

	void (*KeyPressed)(struct KeyEvent*);
	void (*KeyReleased)(struct KeyEvent*);
	void (*TextEntered)(struct TextEvent*);
	void (*ButtonPressed)(struct MouseButtonEvent*);
	void (*ButtonReleased)(struct MouseButtonEvent*);
	void (*MouseMove)(struct MouseMoveEvent*);
	void (*Open)();
	void (*Close)();

	union										///< Widget data
	{
		struct GuiForm		Form;
		struct GuiButton		Button;
		struct GuiTextBox	TextBox;
		struct GuiConsole		Console;
		//struct GuiScroll		Scroll;			// TODO: We have to finish all widgets
		//struct GuiCheckBox	CheckBox;
		//struct GuiImage		Image;
		struct GuiList			List;
		struct GuiShape		Shape;
		struct GuiTable		Table;
	} Attributes;

	uinteger Type;								///< Widget attribute type

	struct GuiWidget * Next;					///< Next widget data
	struct GuiWidget * Previous;				///< Previous widget data
};

////////////////////////////////////////////////////////////
// Allocate memory widget
////////////////////////////////////////////////////////////
struct GuiWidget * GuiWidgetCreate(uinteger Type, integer X, integer Y, uinteger FontSize, char * Text);

////////////////////////////////////////////////////////////
// Create widget without allocating memory by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiWidgetCreateDefault(uinteger Type);

////////////////////////////////////////////////////////////
// Copy the events of the widget
////////////////////////////////////////////////////////////
void GuiWidgetCopyEvents(struct GuiWidget * Dest, struct GuiWidget * Src);

////////////////////////////////////////////////////////////
// Toggle visibility
////////////////////////////////////////////////////////////
struct GuiWidget * GuiWidgetToggleVisibility(struct GuiWidget * Widget);

////////////////////////////////////////////////////////////
// Destroy widget and memory
////////////////////////////////////////////////////////////
void GuiWidgetDestroy(struct GuiWidget * Widget);

#endif // GUI_WIDGET_H
