/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Memory/General.h>
#include <World/Octree.h>
#include <World/Entity.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define OCTREE_MIN_ENTITY_SIZE			0x03
#define OCTREE_MAX_ENTITY_SIZE			0x10
#define OCTREE_MAX_DEPTH				0x0A

#define OCTREE_TOP_LEFT_FRONT			0x00
#define OCTREE_TOP_LEFT_BACK			0x01
#define OCTREE_TOP_RIGHT_BACK			0x02
#define OCTREE_TOP_RIGHT_FRONT			0x03
#define OCTREE_BOTTOM_LEFT_FRONT		0x04
#define OCTREE_BOTTOM_LEFT_BACK			0x05
#define OCTREE_BOTTOM_RIGHT_BACK		0x06
#define OCTREE_BOTTOM_RIGHT_FRONT		0x07

#define OCTREE_X_BIT					0x01 << 0x00
#define OCTREE_Y_BIT					0x01 << 0x01
#define OCTREE_Z_BIT					0x01 << 0x02

#define BOUNDING_AREA_VIEW_RADIUS		30.0f

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create an octree structure
////////////////////////////////////////////////////////////
struct OctreeType * OctreeCreate()
{
	struct OctreeType * Octree = (struct OctreeType*)MemoryAllocate(sizeof(struct OctreeType));

	if (Octree)
	{
		// Create the bounding box
		Octree->Box = BoundingBoxCreate();

		// If invalid box allocation
		if (!Octree->Box)
		{
			OctreeDestroy(Octree);

			return NULL;
		}

		// Set to null the reference list
		Octree->EntityIdRefList = NULL;
		
		return OctreeReset(Octree);
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Reset the octree
////////////////////////////////////////////////////////////
struct OctreeType * OctreeReset(struct OctreeType * Octree)
{
	if (Octree)
	{
		uinteger i;

		for (i = 0; i < OCTREE_CHILD_COUNT; ++i)
		{
			Octree->Child[i] = NULL;
		}

		Octree->Type = OCTREE_NODE_TYPE_NULL;

		Octree->Depth = 0;

		Octree->Parent = NULL;

		BoundingBoxSetNull(Octree->Box);

		if (Octree->EntityIdRefList != NULL)
		{
			ListClear(Octree->EntityIdRefList);
		}

		return Octree;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Initialize an octree specifieng it's properties
////////////////////////////////////////////////////////////
struct OctreeType * OctreeInitialize(struct OctreeType * Octree, uinteger Id, struct OctreeType * Parent,
									 struct BoundingBox * Box, uinteger Depth, uinteger Type)
{
	if (Octree)
	{
		Octree->Id = Id;

		Octree->Parent = Parent;

		BoundingBoxCopy(Octree->Box, Box);

		Octree->Depth = Depth;

		Octree->Type = Type;

		if (Type == OCTREE_NODE_TYPE_LEAF || Type == OCTREE_NODE_TYPE_ROOT)
		{
			if (!Octree->EntityIdRefList)
			{
				Octree->EntityIdRefList = ListNew(sizeof(struct EntityIdPairType));

				if (!Octree->EntityIdRefList)
				{
					OctreeDestroy(Octree);

					return NULL;
				}
			}

			ListClear(Octree->EntityIdRefList);
		}

		return Octree;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy octree structure
////////////////////////////////////////////////////////////
void OctreeDestroy(struct OctreeType * Octree)
{
	if (Octree)
	{
		if (OctreeHasChildren(Octree))
		{
			uinteger i;

			for (i = 0; i < OCTREE_CHILD_COUNT; ++i)
			{
				OctreeDestroy(Octree->Child[i]);
			}
		}
		else
		{
			ListDestroy(Octree->EntityIdRefList);
		}

		BoundingBoxDestroy(Octree->Box);

		if (OctreeHasParent(Octree))
		{
			Octree->Parent->Child[Octree->Id] = NULL;
		}

		MemoryDeallocate(Octree);
	}
}

////////////////////////////////////////////////////////////
/// Update entities contained in the octree
////////////////////////////////////////////////////////////
void OctreeUpdate(struct OctreeType * Octree, float ElapsedTime)
{
	if (Octree)
	{
		if (Octree->Type != OCTREE_NODE_TYPE_LEAF && Octree->Type != OCTREE_NODE_TYPE_NULL)
		{
			uinteger i;

			for (i = 0; i < OCTREE_CHILD_COUNT; ++i)
			{
				if (Octree->Child[i])
				{
					OctreeUpdate(Octree->Child[i], ElapsedTime);
				}
			}
		}

		// Update entities
		if (Octree->EntityIdRefList)
		{
			ListIterator Iterator;

			ListForEach(Octree->EntityIdRefList, Iterator)
			{
				struct EntityIdPairType * EntityPairRef = ListItDataPtr(Iterator, struct EntityIdPairType);
				struct EntityType * EntityRef = EntityGet(EntityPairRef->Type, EntityPairRef->Id);
				struct ComponentType * Collision = EntityComponentCollision(EntityRef);
				struct ComponentCollisionType * CollisionType = ComponentTypeCollision(Collision);

				// Update entities
				//CollisionType->Update(EntityRef, ElapsedTime);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Check if octree node has children nodes
////////////////////////////////////////////////////////////
bool OctreeHasChildren(struct OctreeType * Octree)
{
	return (bool)(Octree &&
		(Octree->Type != OCTREE_NODE_TYPE_NULL) &&
		(Octree->Type == OCTREE_NODE_TYPE_NODE ||
		(Octree->Type == OCTREE_NODE_TYPE_ROOT && Octree->Child[0] != NULL)));
}

////////////////////////////////////////////////////////////
/// Check if octree node has a parent node
////////////////////////////////////////////////////////////
bool OctreeHasParent(struct OctreeType * Octree)
{
	return (bool)(Octree && 
		((Octree->Type != OCTREE_NODE_TYPE_NULL) &&
		(Octree->Type != OCTREE_NODE_TYPE_ROOT)) &&
		Octree->Parent);
}

////////////////////////////////////////////////////////////
/// Moves all entities into its children
////////////////////////////////////////////////////////////
void OctreeSpawnEntity(struct OctreeType * Octree)
{
	if (Octree)
	{
		if (OctreeHasChildren(Octree))
		{
			ListIterator Iterator;

			ListForEach(Octree->EntityIdRefList, Iterator)
			{
				struct EntityIdPairType * EntityPairRef = ListItDataPtr(Iterator, struct EntityIdPairType);
				struct EntityType * EntityRef = EntityGet(EntityPairRef->Type, EntityPairRef->Id);
				uinteger i;

				for (i = 0; i < OCTREE_CHILD_COUNT; ++i)
				{
					if (EntityRef && Octree->Child[i])
					{
						if (EntityCollisionComponentCollide(EntityRef, (struct ComponentCollisionData *)Octree->Child[i]->Box, COMPONENT_DATA_COLLISION_BOX))
						{
							ListPushBack(Octree->Child[i]->EntityIdRefList, (void *)EntityPairRef);
							break;
						}
					}
				}
			}

			ListClear(Octree->EntityIdRefList);
		}
	}
}

////////////////////////////////////////////////////////////
/// Collect all entities of an octree
////////////////////////////////////////////////////////////
void OctreeCollectEntity(struct OctreeType * Octree, List EntityIdRefList)
{
	if (Octree)
	{
		if (Octree && EntityIdRefList)
		{
			if (OctreeHasChildren(Octree))
			{
				uinteger i;

				for (i = 0; i < OCTREE_CHILD_COUNT; ++i)
				{
					if (Octree->Child[i])
					{
						OctreeCollectEntity(Octree->Child[i], EntityIdRefList);
					}
				}
			}
			else
			{
				if (Octree->EntityIdRefList)
				{
					ListIterator Iterator;

					ListForEach(Octree->EntityIdRefList, Iterator)
					{
						struct EntityIdPairType * EntityPairRef = ListItDataPtr(Iterator, struct EntityIdPairType);

						ListPushBack(EntityIdRefList, (void *)EntityPairRef);
					}

					ListClear(Octree->EntityIdRefList);
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Count all entities of an octree
////////////////////////////////////////////////////////////
uinteger OctreeCountEntity(struct OctreeType * Octree, uinteger Count)
{
	if (Octree)
	{
		if (OctreeHasChildren(Octree))
		{
			uinteger i;

			for (i = 0; i < OCTREE_CHILD_COUNT; ++i)
			{
				if (Octree->Child[i])
				{
					Count += OctreeCountEntity(Octree->Child[i], Count);
				}
			}

			return Count;
		}
		else
		{
			if (Octree->EntityIdRefList)
			{
				return ListSize(Octree->EntityIdRefList);
			}
			else
			{
				return 0;
			}
		}
	}

	return 0;
}

////////////////////////////////////////////////////////////
/// Create new children in an octree
////////////////////////////////////////////////////////////
void OctreeCreateChildren(struct OctreeType * Octree)
{
	if (Octree)
	{
		if (!OctreeHasChildren(Octree))
		{
			float HalfRadius = (float)BoundingBoxAxisDistance(Octree->Box, 0) * 0.25f;
			uinteger i;

			for (i = 0; i < OCTREE_CHILD_COUNT; ++i)
			{
				struct BoundingBox ChildBox;
				struct Vector3f ChildCenter;

				Vector3fSet(&ChildCenter,
							(i & OCTREE_X_BIT) ? Octree->Box->Center.x - HalfRadius : Octree->Box->Center.x + HalfRadius,
							(i & OCTREE_Y_BIT) ? Octree->Box->Center.y - HalfRadius : Octree->Box->Center.y + HalfRadius,
							(i & OCTREE_Z_BIT) ? Octree->Box->Center.z - HalfRadius : Octree->Box->Center.z + HalfRadius);

				BoundingBoxSetRadius(&ChildBox, &ChildCenter, HalfRadius);

				Octree->Child[i] = OctreeInitialize(OctreeCreate(), i, Octree,
													&ChildBox, (Octree->Depth + 1),
													OCTREE_NODE_TYPE_LEAF);
			}

			if (Octree->Type != OCTREE_NODE_TYPE_ROOT)
			{
				Octree->Type = OCTREE_NODE_TYPE_NODE;
			}

			// Spawn entity list to child nodes
			OctreeSpawnEntity(Octree);
		}
	}
}

////////////////////////////////////////////////////////////
/// Destroy recursively all children of an octree
////////////////////////////////////////////////////////////
void OctreeDestroyChildren(struct OctreeType * Octree)
{
	if (Octree)
	{
		if (OctreeHasChildren(Octree))
		{
			uinteger i;

			if (!Octree->EntityIdRefList)
			{
				Octree->EntityIdRefList = ListNew(sizeof(struct EntityIdPairType));

				if (!Octree->EntityIdRefList)
				{
					return;
				}
			}

			OctreeCollectEntity(Octree, Octree->EntityIdRefList);

			for (i = 0; i < OCTREE_CHILD_COUNT; ++i)
			{
				OctreeDestroy(Octree->Child[i]);
			}

			if (Octree->Type != OCTREE_NODE_TYPE_ROOT)
			{
				Octree->Type = OCTREE_NODE_TYPE_LEAF;
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Add entity into an octree
////////////////////////////////////////////////////////////
void OctreeAddEntity(struct OctreeType * Octree, struct EntityIdPairType * EntityIdPair)
{
	if (Octree)
	{
		if (!OctreeHasChildren(Octree) &&
			Octree->Depth < OCTREE_MAX_DEPTH &&
			ListSize(Octree->EntityIdRefList) > OCTREE_MAX_ENTITY_SIZE)
		{
			OctreeCreateChildren(Octree);
		}

		if (OctreeHasChildren(Octree))
		{
			struct EntityType * Entity = EntityGet(EntityIdPair->Type, EntityIdPair->Id);

			uinteger i;

			for (i = 0; i < OCTREE_CHILD_COUNT; ++i)
			{
				if (Entity && Octree->Child[i])
				{
					if (EntityCollisionComponentCollide(Entity, (struct ComponentCollisionData *)Octree->Child[i]->Box, COMPONENT_DATA_COLLISION_BOX))
					{
						OctreeAddEntity(Octree->Child[i], EntityIdPair);
						break;
					}
				}
			}
		}
		else
		{
			if (Octree->EntityIdRefList)
			{
				ListIterator Iterator;

				ListForEach(Octree->EntityIdRefList, Iterator)
				{
					struct EntityIdPairType * EntityPairRef = ListItDataPtr(Iterator, struct EntityIdPairType);

					if (EntityPairRef && EntityPairRef->Type == EntityIdPair->Type && EntityPairRef->Id == EntityIdPair->Id)
					{
						return;
					}
				}

				// Add entity
				ListPushBack(Octree->EntityIdRefList, (void *)EntityIdPair);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Remove entity from an octree
////////////////////////////////////////////////////////////
void OctreeRemoveEntity(struct OctreeType * Octree, struct EntityIdPairType * EntityIdPair)
{
	if (Octree)
	{
		struct EntityType * Entity = EntityGet(EntityIdPair->Type, EntityIdPair->Id);

		if (OctreeHasChildren(Octree))
		{
			uinteger i;

			for (i = 0; i < OCTREE_CHILD_COUNT; ++i)
			{
				if (Octree->Child[i])
				{
					if (EntityCollisionComponentCollide(Entity, (struct ComponentCollisionData *)Octree->Child[i]->Box, COMPONENT_DATA_COLLISION_BOX))
					{
						OctreeRemoveEntity(Octree->Child[i], EntityIdPair);

						break;
					}
				}
			}
		}
		else
		{
			if (Octree->EntityIdRefList)
			{
				ListIterator Iterator, EraseIt = NULL;

				ListForEach(Octree->EntityIdRefList, Iterator)
				{
					struct EntityIdPairType * IteratorEntityPairRef = ListItDataPtr(Iterator, struct EntityIdPairType);

					if (IteratorEntityPairRef && IteratorEntityPairRef->Type == EntityIdPair->Type && IteratorEntityPairRef->Id == EntityIdPair->Id)
					{
						EraseIt = Iterator;

						break;
					}
				}

				if (EraseIt)
				{
					// Remove entity from list
					ListEraseIt(Octree->EntityIdRefList, EraseIt);

					if (Octree->Type != OCTREE_NODE_TYPE_ROOT)
					{
						uinteger EntitySize = OctreeCountEntity(Octree, 0);

						if (EntitySize < OCTREE_MIN_ENTITY_SIZE)
						{
							OctreeDestroyChildren(Octree);

							OctreeDestroy(Octree);

							/*
							if (OctreeCountEntity(Octree, 0) < OCTREE_MIN_ENTITY_SIZE)
							{
								uinteger i;

								for (i = 0; i < OCTREE_CHILD_COUNT; ++i)
								{
									if (Octree->Parent->Child[i] == Octree)
									{
										ListIterator Iterator;

										// Reset reference
										Octree->Parent->Child[i] = NULL;

										// Insert all entities into parent list
										ListForEach(Octree->EntityIdRefList, Iterator)
										{
											struct EntityIdPairType * IteratorEntityPairRef = ListItDataPtr(Iterator, struct EntityIdPairType);

											ListPushBack(Octree->Parent->EntityIdRefList, (void *)IteratorEntityPairRef);
										}

										// Destroy itself
										OctreeDestroy(Octree);

										return;
									}
								}
							}
							*/
						}
					}
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Get potential collisions related to entity in octree
////////////////////////////////////////////////////////////
void OctreeGetPotentialCollisions(struct OctreeType * Octree, List EntityIdRefList, struct EntityIdPairType * EntityIdPair)
{
	if (Octree && EntityIdRefList)
	{
		struct EntityType * Entity = EntityGet(EntityIdPair->Type, EntityIdPair->Id);

		if (Entity)
		{
			if (OctreeHasChildren(Octree))
			{
				uinteger i;

				struct ComponentTransformData * Transform = ComponentDataTransform(EntityComponentTransform(Entity));

				for (i = 0; i < OCTREE_CHILD_COUNT; ++i)
				{
					if (Octree->Child[i])
					{
						struct BoundingSphere Sphere;

						// Copy radius
						Sphere.Radius = BOUNDING_AREA_VIEW_RADIUS;

						// Copy center
						Vector3fCopy(&Sphere.Center, &Transform->Position);

						// Check collision
						if (EntityCollisionComponentCollide(Entity, (struct ComponentCollisionData *)(&Sphere), COMPONENT_DATA_COLLISION_SPHERE))
						{
							OctreeGetPotentialCollisions(Octree->Child[i], EntityIdRefList, EntityIdPair);
						}
					}
				}
			}
			else
			{
				if (Octree->EntityIdRefList)
				{
					ListIterator Iterator;

					ListForEach(Octree->EntityIdRefList, Iterator)
					{
						struct EntityIdPairType * IteratorEntityPairRef = ListItDataPtr(Iterator, struct EntityIdPairType);

						ListPushBack(EntityIdRefList, (void *)IteratorEntityPairRef);
					}
				}
			}
		}
	}
}
