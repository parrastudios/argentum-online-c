/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/CameraBase.h>
#include <World/CameraTransform.h>

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a transform component of a camera
////////////////////////////////////////////////////////////
struct EntityType * CameraTransformCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a camera transform component
////////////////////////////////////////////////////////////
void CameraTransformDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Look camera at a target
////////////////////////////////////////////////////////////
void CameraTransformLookAtTarget(struct EntityType * Entity, struct Vector3f * Target);

////////////////////////////////////////////////////////////
/// Rotate camera
////////////////////////////////////////////////////////////
void CameraTransformRotate(struct EntityType * Entity, float Longitude, float Latitude);

////////////////////////////////////////////////////////////
///	Set projection matrix
////////////////////////////////////////////////////////////
void CameraTransformProjection(struct EntityType * Entity, float ScaleX, float ScaleY, float Near, float Far);

////////////////////////////////////////////////////////////
///	Set view matrix
////////////////////////////////////////////////////////////
void CameraTransformView(struct EntityType * Entity);

////////////////////////////////////////////////////////////
///	Update view matrix
////////////////////////////////////////////////////////////
void CameraTransformViewUpdate(struct CameraComponentBaseData * BaseData, struct CameraComponentTransformData * TransformData);

////////////////////////////////////////////////////////////
///	Update camera view matrix with elapsed time
////////////////////////////////////////////////////////////
void CameraTransformViewUpdateElapsed(struct CameraComponentBaseData * BaseData,
	struct CameraComponentTransformData * TransformData, struct ComponentPhysicsData * PhysicsData, float ElapsedTime);

////////////////////////////////////////////////////////////
/// Update camera orientation
////////////////////////////////////////////////////////////
void CameraTransformOrientationUpdate(struct CameraComponentBaseData * BaseData, struct CameraComponentTransformData * TransformData, float ElapsedTime);

////////////////////////////////////////////////////////////
/// Update camera
////////////////////////////////////////////////////////////
void CameraTransformUpdate(struct EntityType * Entity, float ElapsedTime);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the camera transform implementation attributes
////////////////////////////////////////////////////////////
const void * CameraComponentTransformAttr()
{
	static const struct CameraComponentTransformType CameraComponentTransformAttr = {
		TypeInit(Move, &CameraTransformLookAtTarget),
		TypeInit(Rotate, &CameraTransformRotate),
		TypeInit(Projection, &CameraTransformProjection),
		TypeInit(View, &CameraTransformView),
		TypeInit(Update, &CameraTransformUpdate)
	};

	return (const void *)&CameraComponentTransformAttr;
}

////////////////////////////////////////////////////////////
/// Return the camera transform implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * CameraComponentTransformImpl()
{
	static const struct ComponentImplType CameraComponentTransformTypeImpl = {
		ExprInit(Base, TypeInit(Create, &CameraTransformCreate), TypeInit(Destroy, &CameraTransformDestroy)),
		TypeInit(Type, COMPONENT_TYPE_TRANSFORM),
		UnionInit(Attributes, Derived, &CameraComponentTransformAttr)
	};

	return &CameraComponentTransformTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a transform component of a camera
////////////////////////////////////////////////////////////
struct EntityType * CameraTransformCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain transform component data reference
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct CameraComponentTransformData * TransformData = ComponentDataDerived(Transform, struct CameraComponentTransformData);

		// Initialize rotation properties
		TransformData->OffsetDistance = 0.0f;
		TransformData->Heading = 0.0f;
		TransformData->Pitch = 0.0f;

		// Initialize matrix
		Matrix4fIdentity(&TransformData->ViewMatrix);
		Matrix4fIdentity(&TransformData->ProjectionMatrix);
		Matrix4fIdentity(&TransformData->ViewProjectionMatrix);

		// Initialize orientation
		QuaternionIdentity(&TransformData->Orientation);

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a camera transform component
////////////////////////////////////////////////////////////
void CameraTransformDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get transform component
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct CameraComponentTransformData * TransformData = ComponentDataDerived(Transform, struct CameraComponentTransformData);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Look camera at a target
////////////////////////////////////////////////////////////
void CameraTransformLookAtTarget(struct EntityType * Entity, struct Vector3f * Target)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct CameraComponentBaseData * BaseData = ComponentDataDerived(Base, struct CameraComponentBaseData);

		// Copy new target
		Vector3fCopy(&BaseData->Target, Target);
	}
}

////////////////////////////////////////////////////////////
/// Rotate camera
////////////////////////////////////////////////////////////
void CameraTransformRotate(struct EntityType * Entity, float Longitude, float Latitude)
{
	if (Entity)
	{
		// Get transform component
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct CameraComponentTransformData * TransformData = ComponentDataDerived(Transform, struct CameraComponentTransformData);

		TransformData->Heading = -Longitude;
		TransformData->Pitch = -Latitude;
	}
}

////////////////////////////////////////////////////////////
///	Set projection matrix
////////////////////////////////////////////////////////////
void CameraTransformProjection(struct EntityType * Entity, float ScaleX, float ScaleY, float Near, float Far)
{
	if (Entity)
	{
		// Get transform component
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct CameraComponentTransformData * TransformData = ComponentDataDerived(Transform, struct CameraComponentTransformData);

		// Calculate projection matrix
		TransformData->ProjectionMatrix.m[0][0] = ScaleX;
		TransformData->ProjectionMatrix.m[0][1] = 0.0f;
		TransformData->ProjectionMatrix.m[0][2] = 0.0f;
		TransformData->ProjectionMatrix.m[0][3] = 0.0f;

		TransformData->ProjectionMatrix.m[1][0] = 0.0f;
		TransformData->ProjectionMatrix.m[1][1] = ScaleY;
		TransformData->ProjectionMatrix.m[1][2] = 0.0f;
		TransformData->ProjectionMatrix.m[1][3] = 0.0f;

		TransformData->ProjectionMatrix.m[2][0] = 0.0f;
		TransformData->ProjectionMatrix.m[2][1] = 0.0f;
		TransformData->ProjectionMatrix.m[2][2] = (Far + Near) / (Near - Far);
		TransformData->ProjectionMatrix.m[2][3] = -1.0f;

		TransformData->ProjectionMatrix.m[3][0] = 0.0f;
		TransformData->ProjectionMatrix.m[3][1] = 0.0f;
		TransformData->ProjectionMatrix.m[3][2] = (2.0f * Far * Near) / (Near - Far);
		TransformData->ProjectionMatrix.m[3][3] = 0.0f;
	}
}

////////////////////////////////////////////////////////////
///	Set view matrix
////////////////////////////////////////////////////////////
void CameraTransformView(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct CameraComponentBaseData * BaseData = ComponentDataDerived(Base, struct CameraComponentBaseData);

		// Get transform component
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct CameraComponentTransformData * TransformData = ComponentDataDerived(Transform, struct CameraComponentTransformData);

		struct Vector3f Offset;

		// Update view matrix
		TransformData->ViewMatrix.m[0][0] = BaseData->AxisX.x;
		TransformData->ViewMatrix.m[1][0] = BaseData->AxisX.y;
		TransformData->ViewMatrix.m[2][0] = BaseData->AxisX.z;
		TransformData->ViewMatrix.m[3][0] = -Vector3fDot(&BaseData->AxisX, &BaseData->Eye);

		TransformData->ViewMatrix.m[0][1] = BaseData->AxisY.x;
		TransformData->ViewMatrix.m[1][1] = BaseData->AxisY.y;
		TransformData->ViewMatrix.m[2][1] = BaseData->AxisY.z;
		TransformData->ViewMatrix.m[3][1] = -Vector3fDot(&BaseData->AxisY, &BaseData->Eye);

		TransformData->ViewMatrix.m[0][2] = BaseData->AxisZ.x;
		TransformData->ViewMatrix.m[1][2] = BaseData->AxisZ.y;
		TransformData->ViewMatrix.m[2][2] = BaseData->AxisZ.z;
		TransformData->ViewMatrix.m[3][2] = -Vector3fDot(&BaseData->AxisZ, &BaseData->Eye);

		// Get orientation from view matrix
		QuaternionFromMatrix(&TransformData->Orientation, &TransformData->ViewMatrix);

		// Calculate offset distance
		Vector3fSubtractEx(&Offset, &BaseData->Target, &BaseData->Eye);

		// Calculate offset distance
		TransformData->OffsetDistance = Vector3fLength(&Offset);
	}
}

////////////////////////////////////////////////////////////
///	Update view matrix
////////////////////////////////////////////////////////////
void CameraTransformViewUpdate(struct CameraComponentBaseData * BaseData, struct CameraComponentTransformData * TransformData)
{
	if (BaseData && TransformData)
	{
		struct Vector3f ScaledAxisZ;

		// Get view matrix from orientation
		QuaternionToMatrix(&TransformData->Orientation, &TransformData->ViewMatrix);

		// Get axis from view matrix
		Vector3fSet(&BaseData->AxisX, TransformData->ViewMatrix.m[0][0], TransformData->ViewMatrix.m[1][0], TransformData->ViewMatrix.m[2][0]);
		Vector3fSet(&BaseData->AxisY, TransformData->ViewMatrix.m[0][1], TransformData->ViewMatrix.m[1][1], TransformData->ViewMatrix.m[2][1]);
		Vector3fSet(&BaseData->AxisZ, TransformData->ViewMatrix.m[0][2], TransformData->ViewMatrix.m[1][2], TransformData->ViewMatrix.m[2][2]);

		// Calculate view direction
		Vector3fOpposite(&BaseData->ViewDirection, &BaseData->AxisZ);

		// Calculate eye vector
		Vector3fMultiplyEx(&ScaledAxisZ, &BaseData->AxisZ, TransformData->OffsetDistance);
		Vector3fAddEx(&BaseData->Eye, &BaseData->Target, &ScaledAxisZ);

		// Update view matrix
		TransformData->ViewMatrix.m[3][0] = -Vector3fDot(&BaseData->AxisX, &BaseData->Eye);
		TransformData->ViewMatrix.m[3][1] = -Vector3fDot(&BaseData->AxisY, &BaseData->Eye);
		TransformData->ViewMatrix.m[3][2] = -Vector3fDot(&BaseData->AxisZ, &BaseData->Eye);

		// Obtain view projection matrix
		Matrix4fMultiply(&TransformData->ViewProjectionMatrix, &TransformData->ViewMatrix, &TransformData->ProjectionMatrix);
	}
}

////////////////////////////////////////////////////////////
///	Update camera view matrix with elapsed time
////////////////////////////////////////////////////////////
void CameraTransformViewUpdateElapsed(struct CameraComponentBaseData * BaseData,
	struct CameraComponentTransformData * TransformData, struct ComponentPhysicsData * PhysicsData, float ElapsedTime)
{
	if (BaseData && TransformData && PhysicsData)
	{
		struct Vector3f OffsetZAxis, IdealPosition, Displacement, SpringAcceleration;
		struct Vector3f SpringDisplacement, DampingVelocity, EyeIncrement;

		// Get view matrix from orientation
		QuaternionToMatrix(&TransformData->Orientation, &TransformData->ViewMatrix);

		// Get axis from view matrix
		Vector3fSet(&BaseData->AxisX, TransformData->ViewMatrix.m[0][0], TransformData->ViewMatrix.m[1][0], TransformData->ViewMatrix.m[2][0]);
		Vector3fSet(&BaseData->AxisY, TransformData->ViewMatrix.m[0][1], TransformData->ViewMatrix.m[1][1], TransformData->ViewMatrix.m[2][1]);
		Vector3fSet(&BaseData->AxisZ, TransformData->ViewMatrix.m[0][2], TransformData->ViewMatrix.m[1][2], TransformData->ViewMatrix.m[2][2]);

		// Calculate the new camera position. The 'IdealPosition' is where the
		// camera should be position. The camera should be positioned directly
		// behind the target at the required offset distance. What we're doing here
		// is rather than have the camera immediately snap to the 'IdealPosition'
		// we slowly move the camera towards the 'IdealPosition' using a spring
		// system.
		//
		// References:
		//   Stone, Jonathan, "Third-Person Camera Navigation," Game Programming
		//     Gems 4, Andrew Kirmse, Editor, Charles River Media, Inc., 2004.

		Vector3fMultiplyEx(&OffsetZAxis, &BaseData->AxisZ, TransformData->OffsetDistance);
		Vector3fAddEx(&IdealPosition, &BaseData->Target, &OffsetZAxis);

		Vector3fSubtractEx(&Displacement, &BaseData->Eye, &IdealPosition);

		Vector3fMultiplyEx(&SpringDisplacement, &Displacement, -BaseData->SpringConstant);
		Vector3fMultiplyEx(&DampingVelocity, &PhysicsData->Velocity, BaseData->DampingConstant);
		Vector3fSubtractEx(&SpringAcceleration, &SpringDisplacement, &DampingVelocity);

		Vector3fMultiply(&SpringAcceleration, ElapsedTime);
		Vector3fAdd(&PhysicsData->Velocity, &SpringAcceleration);

		Vector3fMultiplyEx(&EyeIncrement, &PhysicsData->Velocity, ElapsedTime);
		Vector3fAdd(&BaseData->Eye, &EyeIncrement);

		// The view matrix is always relative to the camera's current position
		// 'Eye'. Since a spring system is being used here 'Eye' will be
		// relative to 'IdealPosition'. When the camera is no longer being
		// moved 'Eye' will become the same as 'IdealPosition'. The local
		// x, y, and z axes that were extracted from the camera's orientation
		// 'Orientation' is correct for the 'IdealPosition' only. We need
		// to recompute these axes so that they're relative to 'Eye'. Once
		// that's done we can use those axes to reconstruct the view matrix.

		Vector3fSubtractEx(&BaseData->AxisZ, &BaseData->Eye, &BaseData->Target);
		Vector3fNormalize(&BaseData->AxisZ);

		Vector3fCross(&BaseData->TargetAxisY, &BaseData->AxisZ, &BaseData->AxisX);
		Vector3fNormalize(&BaseData->AxisX);

		Vector3fCross(&BaseData->AxisZ, &BaseData->AxisX, &BaseData->AxisY);
		Vector3fNormalize(&BaseData->AxisY);

		// Update view matrix
		Matrix4fIdentity(&TransformData->ViewMatrix);

		TransformData->ViewMatrix.m[0][0] = BaseData->AxisX.x;
		TransformData->ViewMatrix.m[1][0] = BaseData->AxisX.y;
		TransformData->ViewMatrix.m[2][0] = BaseData->AxisX.z;
		TransformData->ViewMatrix.m[3][0] = -Vector3fDot(&BaseData->AxisX, &BaseData->Eye);

		TransformData->ViewMatrix.m[0][1] = BaseData->AxisY.x;
		TransformData->ViewMatrix.m[1][1] = BaseData->AxisY.y;
		TransformData->ViewMatrix.m[2][1] = BaseData->AxisY.z;
		TransformData->ViewMatrix.m[3][1] = -Vector3fDot(&BaseData->AxisY, &BaseData->Eye);

		TransformData->ViewMatrix.m[0][2] = BaseData->AxisZ.x;
		TransformData->ViewMatrix.m[1][2] = BaseData->AxisZ.y;
		TransformData->ViewMatrix.m[2][2] = BaseData->AxisZ.z;
		TransformData->ViewMatrix.m[3][2] = -Vector3fDot(&BaseData->AxisZ, &BaseData->Eye);

		// Calculate view direction
		Vector3fOpposite(&BaseData->ViewDirection, &BaseData->AxisZ);

		// Obtain view projection matrix
		Matrix4fMultiply(&TransformData->ViewProjectionMatrix, &TransformData->ViewMatrix, &TransformData->ProjectionMatrix);
	}
}

////////////////////////////////////////////////////////////
/// Update camera orientation
////////////////////////////////////////////////////////////
void CameraTransformOrientationUpdate(struct CameraComponentBaseData * BaseData, struct CameraComponentTransformData * TransformData, float ElapsedTime)
{
	if (BaseData && TransformData)
	{
		struct Quaternion Rotation;

		TransformData->Pitch *= ElapsedTime;
		TransformData->Heading *= ElapsedTime;

		if (TransformData->Heading != 0.0f)
		{
			struct Quaternion Result;

			QuaternionIdentity(&Rotation);

			QuaternionFromAxisAngle(&Rotation, &BaseData->TargetAxisY, TransformData->Heading);
			QuaternionMultQuaternion(&Rotation, &TransformData->Orientation, &Result);
			QuaternionCopy(&TransformData->Orientation, &Result);
		}

		if (TransformData->Pitch != 0.0f)
		{
			struct Quaternion Result;
			struct Vector3f VectorAxisX;

			QuaternionIdentity(&Rotation);

			Vector3fAxisX(&VectorAxisX);

			QuaternionFromAxisAngle(&Rotation, &VectorAxisX, TransformData->Pitch);
			QuaternionMultQuaternion(&TransformData->Orientation, &Rotation, &Result);
			QuaternionCopy(&TransformData->Orientation, &Result);
		}
	}
}

////////////////////////////////////////////////////////////
/// Update camera
////////////////////////////////////////////////////////////
void CameraTransformUpdate(struct EntityType * Entity, float ElapsedTime)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct CameraComponentBaseData * BaseData = ComponentDataDerived(Base, struct CameraComponentBaseData);

		// Get transform component
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct CameraComponentTransformData * TransformData = ComponentDataDerived(Transform, struct CameraComponentTransformData);

		// Get collision component
		struct ComponentType * Collision = EntityComponentCollisionFrustum(Entity);
		struct ComponentCollisionFrustumType * CollisionType = ComponentTypeCollisionFrustum(Collision);

		// Update orientation
		CameraTransformOrientationUpdate(BaseData, TransformData, ElapsedTime);

		// Update view matrix
		if (BaseData->EnableSpring)
		{
			struct ComponentType * Physics = EntityComponentPhysics(Entity);
			struct ComponentPhysicsData * PhysicsData = ComponentDataPhysics(Physics);

			CameraTransformViewUpdateElapsed(BaseData, TransformData, PhysicsData, ElapsedTime);
		}
		else
		{
			CameraTransformViewUpdate(BaseData, TransformData);
		}

		// Update frustum
		CollisionType->Update(Entity, &TransformData->ViewProjectionMatrix);
	}
}

////////////////////////////////////////////////////////////
/// Return the camera transform data size
////////////////////////////////////////////////////////////
uinteger CameraComponentTransformSize()
{
    return sizeof(struct CameraComponentTransformData);
}
