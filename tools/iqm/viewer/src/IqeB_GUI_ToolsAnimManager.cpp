/****************************************************************************

  IqeB_GUI_ToolsAnimManager.cpp

  29.04.2015 RR: First editon of this file.


*****************************************************************************
*/

#include "IqeBrowser.h"

#include <math.h>

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Menu.H>
#include <FL/fl_ask.H>

/************************************************************************************
 * Globals
 *
 */


/************************************************************************************
 * Statics
 */

static  Fl_Window  *pMyToolWin;
static int MyWinPosX = IQE_GUI_NO_WINPOS_X, MyWinPosY = IQE_GUI_NO_WINPOS_Y; // last window position

static Fl_Browser *pAnimationBrowser;        // Animation browser
static Fl_Input   *pInputName;

static Fl_Float_Input  *pFloat_FrameRate;    // Selected animation: Framerate
static Fl_Check_Button *pCheck_Looped;       // Selected animation: looped flag
static Fl_Int_Input    *pInt_NumFrames;      // Selected animation: Number of frames

static Fl_Button *pGUI_ButDelete;    // Delete selected animation
static Fl_Button *pGUI_ButDelAll;    // Delete all animations
static Fl_Button *pGUI_ButShiftUp;   // Shift up selected animation
static Fl_Button *pGUI_ButShiftDown; // Shift down selected animation
static Fl_Button *pGUI_ButNew;       // Create new animation
static Fl_Button *pGUI_ButNorm;      // Normalize the animations

/************************************************************************************
 * IqeB_GUI_ToolsAnimManagerUpdateSelected
 *
 * Updates the GUI things for single animation
 *
 * iAnimation: >= 0  Update for this animation
 *             < 0   No animation to update
 */

static void IqeB_GUI_ToolsAnimManagerUpdateSelected( int iAnimation)
{
  char TempString[ 256];

  if( pDrawModel == NULL ||
      pDrawModel->anim_count <= 0 ||
      iAnimation < 0 || iAnimation >= pDrawModel->anim_count) {  // NOT in range

    if( pInputName) {  // TEST, have GUI elements
      pInputName->value( "");
      IqeB_GUI_WidgetActivate( pInputName, false); // Set item activated/inactive

      pFloat_FrameRate->value( "");
      IqeB_GUI_WidgetActivate( pFloat_FrameRate, false); // Set item activated/inactive

      pCheck_Looped->value( 0);
      IqeB_GUI_WidgetActivate( pCheck_Looped, false); // Set item activated/inactive

      pInt_NumFrames->value( "");
      IqeB_GUI_WidgetActivate( pInt_NumFrames, false); // Set item activated/inactive
    }

    // Update the buttons
    goto UpdateButtonEnables;
  }

  // Set selection

  if( pDrawModelCurAnim != pDrawModel->anim_data[ iAnimation] ) {  // This is not the selected animation

    IqeB_AnimAction( IQE_ANIMATE_ACTION_SET, iAnimation); // Change to other animation

    IqeB_GUI_UpdateWidgets(); // Also update the GUI
  }

  // update input fields

  if( pInputName) {  // TEST, have GUI elements

    pInputName->value( pDrawModel->anim_data[ iAnimation]->name);
    IqeB_GUI_WidgetActivate( pInputName, true); // Set item activated/inactive

    sprintf( TempString, "%.1f", pDrawModel->anim_data[ iAnimation]->framerate);
    pFloat_FrameRate->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_FrameRate, true); // Set item activated/inactive

    pCheck_Looped->value( pDrawModel->anim_data[ iAnimation]->looped);
    IqeB_GUI_WidgetActivate( pCheck_Looped, true); // Set item activated/inactive

    sprintf( TempString, "%d", pDrawModel->anim_data[ iAnimation]->len);
    pInt_NumFrames->value( TempString);
    IqeB_GUI_WidgetActivate( pInt_NumFrames, true); // Set item activated/inactive
  }

  // Update the buttons

UpdateButtonEnables:

  IqeB_GUI_WidgetActivate( pGUI_ButNew , pDrawModel != NULL &&               // Have a model displayed
                                         (pDrawModel->skel != NULL && pDrawModel->skel->joint_count > 0) &&  // need a skeleton
                                         (pDrawModel->anim_count == 0 || pDrawModel->anim_poses == pDrawModel->skel->joint_count)); // joint_count must be equal

  IqeB_GUI_WidgetActivate( pGUI_ButNorm , pDrawModel != NULL &&               // Have a model displayed
                                          (pDrawModel->skel != NULL && pDrawModel->skel->joint_count > 0) &&  // need a skeleton
                                          pDrawModel->skel->IsNormalized == false && // and skeleton is not normalized
                                          (pDrawModel->anim_count == 0 || pDrawModel->anim_poses == pDrawModel->skel->joint_count) && // joint_count must be equal
                                          pDrawModel->anim_count > 0);     // Have animations

  IqeB_GUI_WidgetActivate( pGUI_ButDelete , pDrawModel != NULL &&            // Have a model displayed
                                            pDrawModel->anim_count > 0 &&    // Have animations
                                            pAnimationBrowser->size() > 0 && // The browser shows something
                                            pAnimationBrowser->value() > 0); // An item is selected

  IqeB_GUI_WidgetActivate( pGUI_ButDelAll , pDrawModel != NULL &&            // Have a model displayed
                                            pDrawModel->anim_count > 0);     // Have animations


  IqeB_GUI_WidgetActivate( pGUI_ButShiftUp , pDrawModel != NULL &&            // Have a model displayed
                                             pDrawModel->anim_count > 0 &&    // Have animations
                                             pAnimationBrowser->size() > 1 && // The browser shows something
                                             pAnimationBrowser->value() > 0); // An item is selected

  IqeB_GUI_WidgetActivate( pGUI_ButShiftDown , pDrawModel != NULL &&            // Have a model displayed
                                               pDrawModel->anim_count > 0 &&    // Have animations
                                               pAnimationBrowser->size() > 1 && // The browser shows something
                                               pAnimationBrowser->value() > 0); // An item is selected
}

/************************************************************************************
 * IqeB_GUI_ToolsAnimManagerUpdate
 *
 * Updates the animation view
 */

static void IqeB_GUI_ToolsAnimManagerUpdate( int KeepSelection)
{
  int i;
  int LastSelection;

  if( pAnimationBrowser == NULL) {   // security test, need this pointer

    return;
  }

  pAnimationBrowser->clear();                  // empty the list box

  // test for animations to display

  if( pDrawModel == NULL ||
      pDrawModel->anim_count <= 0) {

    IqeB_GUI_ToolsAnimManagerUpdateSelected( -1);  // deselect
    pAnimationBrowser->redraw();                   // Redraw it

    return;
  }

  LastSelection = pAnimationBrowser->value();  // get index of selected item

  if( ! KeepSelection) {  // Get current selected animation

    LastSelection = 0;
    for( i = 0; i < pDrawModel->anim_count; i++) {

      if( pDrawModel->anim_data[ i] == pDrawModelCurAnim) {

        LastSelection = i + 1;
        break;
      }
    }

    KeepSelection = true;   // Set this as selected
  }

  // Add animations

  for( i = 0; i < pDrawModel->anim_count; i++) {

    pAnimationBrowser->add( pDrawModel->anim_data[ i]->name);
  }

  if( KeepSelection && LastSelection > 0 && LastSelection <= pAnimationBrowser->size()) {

    pAnimationBrowser->select( LastSelection);         // set to last selected

    pAnimationBrowser->middleline( LastSelection);

    IqeB_GUI_ToolsAnimManagerUpdateSelected( LastSelection - 1);

  } else if( pDrawModel->anim_count > 0) {              // have any

    pAnimationBrowser->topline( 1);           // position to top line
    IqeB_GUI_ToolsAnimManagerUpdateSelected( -1);       // deselect
  }

  pAnimationBrowser->redraw();     // Redraw it
}

/************************************************************************************
 * IqeB_GUI_UpdateCallback
 *
 * Is called from IqeB_GUI_UpdateWidgets() if something has changed
 * what needs an update (new model displayed, ...).
 */

static void IqeB_GUI_UpdateCallback( int UpdateFlags)
{
  if( pMyToolWin == NULL) {  // security test, has main window

    return;
  }

  IqeB_GUI_ToolsAnimManagerUpdate( false);
}

/************************************************************************************
 * IqeB_AnimManagerBrowser_Callback
 */

static void IqeB_AnimManagerBrowser_Callback( Fl_Widget *w, void *data)
{
  int this_item;

  if( pDrawModel == NULL ||
      pDrawModel->anim_count <= 0) {

    IqeB_GUI_ToolsAnimManagerUpdateSelected( -1);   // deselect

    return;
  }

  // Select item

  this_item = pAnimationBrowser->value();  // get index of selected item

  if( this_item > 0 && this_item <= pDrawModel->anim_count) { // index is in range

    // Have a selection
    this_item -= 1;   // adapt index from 1 based to 0 based table access

    IqeB_GUI_ToolsAnimManagerUpdateSelected( this_item);

  } else {

    // selected the background

    IqeB_GUI_ToolsAnimManagerUpdateSelected( -1);   // deselect
  }
}

/************************************************************************************
 * IqeB_GUI_Input_Name_SetValue_Callback
 */

static void IqeB_GUI_Input_Name_SetValue_Callback( Fl_Widget *w, void *data)
{
  Fl_Input *pInput;
  int i, iAnimation;

  pInput = (Fl_Input *)w;

  if( pInput == NULL) {   // security test

    return;
  }

  // Get current animation index

  iAnimation = 0;
  for( i = 0; i < pDrawModel->anim_count; i++) {

    if( pDrawModel->anim_data[ i] == pDrawModelCurAnim) {

      iAnimation = i;
      break;
    }
  }

  // security tests

  if( pDrawModel == NULL ||
      pDrawModel->anim_count <= 0 ||
      iAnimation < 0 || iAnimation >= pDrawModel->anim_count) {  // NOT in range

    return;
  }

  // NOTE: iAnimation and pDrawModelCurAnim must match

  if( pInput == pInputName) {       // Was name input field

    if( strcmp( pDrawModelCurAnim->name, pInput->value()) != 0) { // string is different

      // set changed string

      if( pDrawModelCurAnim->name) {

        free( pDrawModelCurAnim->name);
      }

      pDrawModelCurAnim->name = strdup( pInput->value());

      // Also update browser

      pAnimationBrowser->text( iAnimation + 1, pInput->value());
    }

    return;
  }
}

/************************************************************************************
 * IqeB_GUI_Float_SetValue_Callback
 */

static void IqeB_GUI_Float_SetValue_Callback( Fl_Widget *w, void *data)
{
  Fl_Float_Input *pInput;
  int i, iAnimation;
  float Value;
  char TempString[ 256];

  pInput = (Fl_Float_Input *)w;

  if( pInput == NULL) {   // security test

    return;
  }

  // get value

  sscanf( pInput->value(), "%f", &Value);

  // Get current animation index

  iAnimation = 0;
  for( i = 0; i < pDrawModel->anim_count; i++) {

    if( pDrawModel->anim_data[ i] == pDrawModelCurAnim) {

      iAnimation = i;
      break;
    }
  }

  // security tests

  if( pDrawModel == NULL ||
      pDrawModel->anim_count <= 0 ||
      iAnimation < 0 || iAnimation >= pDrawModel->anim_count) {  // NOT in range

    return;
  }

  // NOTE: iAnimation and pDrawModelCurAnim must match

  if( pInput == pFloat_FrameRate) {       // Was framerate input field

    if( Value < 0.01) {   // security test

      Value = 0.01;
      sprintf( TempString, "%.1f", Value);
      pInput->value( TempString);
    }
    if( Value > 100.0) {  // security test

      Value = 100.0;
      sprintf( TempString, "%.1f", Value);
      pInput->value( TempString);
    }

    pDrawModelCurAnim->framerate = Value; // set new framerate

    // and update ...

    IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI

    return;
  }
}

/************************************************************************************
 * IqeB_GUI_Int_SetValue_Callback
 */

static void IqeB_GUI_Int_SetValue_Callback( Fl_Widget *w, void *data)
{
  Fl_Int_Input *pInput;
  int i, iAnimation;
  int Value;
  char TempString[ 256];

  pInput = (Fl_Int_Input *)w;

  if( pInput == NULL) {   // security test

    return;
  }

  // get value

  sscanf( pInput->value(), "%d", &Value);

  // Get current animation index

  iAnimation = 0;
  for( i = 0; i < pDrawModel->anim_count; i++) {

    if( pDrawModel->anim_data[ i] == pDrawModelCurAnim) {

      iAnimation = i;
      break;
    }
  }

  // security tests

  if( pDrawModel == NULL ||
      pDrawModel->anim_count <= 0 ||
      iAnimation < 0 || iAnimation >= pDrawModel->anim_count) {  // NOT in range

    return;
  }

  // NOTE: iAnimation and pDrawModelCurAnim must match

  if( pInput == pInt_NumFrames) {       // Was # frames input field

    if( Value < 0) {   // security test

      Value = 0;
      sprintf( TempString, "%d", Value);
      pInput->value( TempString);
    }

    if( Value > MAX_ANIM_FRAMES) {  // security test

      Value = MAX_ANIM_FRAMES;
      sprintf( TempString, "%d", Value);
      pInput->value( TempString);
    }

    // Must reallocate # animations

    IqeB_AnimAction( IQE_ANIMATE_ACTION_NUM_FRAMES, Value); // Change number of frames of current animation (use argument)

    // and update ...

    IqeB_GUI_ToolsAnimManagerUpdate( true);            // update Browser

    IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI

    return;
  }
}

/************************************************************************************
 * IqeB_GUI_CBox_SetValue_Callback
 */

static void IqeB_GUI_CBox_SetValue_Callback( Fl_Widget *w, void *data)
{
  int Value;
  Fl_Check_Button *pThis;
  int i, iAnimation;

  // ...

  pThis  = (Fl_Check_Button *)w;

  if( pThis == NULL) {   // security test

    return;
  }

  Value = pThis->value();              // update the variable

  // Get current animation index

  iAnimation = 0;
  for( i = 0; i < pDrawModel->anim_count; i++) {

    if( pDrawModel->anim_data[ i] == pDrawModelCurAnim) {

      iAnimation = i;
      break;
    }
  }

  // security tests

  if( pDrawModel == NULL ||
      pDrawModel->anim_count <= 0 ||
      iAnimation < 0 || iAnimation >= pDrawModel->anim_count) {  // NOT in range

    return;
  }

  // NOTE: iAnimation and pDrawModelCurAnim must match

  if( pThis == pCheck_Looped) {         // Was looped flag

    pDrawModelCurAnim->looped = Value;  // Set changed value
  }
}

/************************************************************************************
 * Callback, button 'New' pressed
 */

static void ButtonNewCallback(Fl_Widget *w, void *data)
{
  int iAnimation;

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  if( pDrawModel != NULL &&
      (pDrawModel->skel != NULL && pDrawModel->skel->joint_count > 0) &&  // need a skeleton
      (pDrawModel->anim_count == 0 || pDrawModel->anim_poses == pDrawModel->skel->joint_count)) { // joint_count must be equal

    // OK
  } else {

    // Not OK
    return;
  }

  // ...

  iAnimation = pAnimationBrowser->value() - 1;  // get index of selected item

  IqeB_AnimAction( IQE_ANIMATE_ACTION_NEW_ARG, iAnimation); // Create new animation (after named argument nr.)

  // and update ...

  IqeB_GUI_ToolsAnimManagerUpdate( true);            // update Browser

  IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
}

/************************************************************************************
 * Callback, button 'Normalize' pressed
 */

static void ButtonNormalizeCallback(Fl_Widget *w, void *data)
{

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  if( pDrawModel != NULL &&
      (pDrawModel->skel != NULL && pDrawModel->skel->joint_count > 0) &&  // need a skeleton
      (pDrawModel->anim_count == 0 || pDrawModel->anim_poses == pDrawModel->skel->joint_count) && // joint_count must be equal
      pDrawModel->anim_count > 0) {     // Have animations

    // OK
  } else {

    // Not OK
    return;
  }

  // ...

  IqeB_AnimNormalizeModel( pDrawModel);

  // And update for display
  if( pDrawModelCurAnim != NULL) {

    IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);
  }

  // and update ...

  IqeB_GUI_ToolsAnimManagerUpdate( true);            // update Browser

  IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_MODEL_CHANGE | IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
}

/************************************************************************************
 * Callback, button 'Delete' pressed
 */

static void ButtonDeleteCallback(Fl_Widget *w, void *data)
{
  int iAnimation;

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  if( pDrawModel == NULL ||
      pDrawModel->anim_count <= 0) {

    return;
  }

  // ...

  iAnimation = pAnimationBrowser->value() - 1;  // get index of selected item

  IqeB_AnimAction( IQE_ANIMATE_ACTION_DELETE_ARG, iAnimation); // Delete animation named in argument

  // and update ...

  IqeB_GUI_ToolsAnimManagerUpdate( true);            // update Browser

  IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
}

/************************************************************************************
 * Callback, button 'Delete all' pressed
 */

static void ButtonDelAllCallback(Fl_Widget *w, void *data)
{

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  if( pDrawModel == NULL ||
      pDrawModel->anim_count <= 0) {

    return;
  }

  //

  fl_message_title( "Delete all animations");
  if( fl_choice( "Delete all animations?",
                 "Cancel", "Yes", NULL) == 0) {
    return;
  }

  // ...

  IqeB_AnimAction( IQE_ANIMATE_ACTION_DELETE_ALL, 0); // Delete all animations

  // and update ...

  IqeB_GUI_ToolsAnimManagerUpdate( true);            // update Browser

  IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
}

/************************************************************************************
 * Callback, button 'Shift up' pressed
 */

static void ButtonShiftUpCallback(Fl_Widget *w, void *data)
{
  int iAnimation;

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  if( pDrawModel == NULL ||
      pDrawModel->anim_count <= 0) {

    return;
  }

  // ...

  iAnimation = pAnimationBrowser->value() - 1;  // get index of selected item

  IqeB_AnimAction( IQE_ANIMATE_ACTION_SHIFT_UP, iAnimation); // Shift named animation up

  // and update ...

  IqeB_GUI_ToolsAnimManagerUpdate( true);            // update Browser

  IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
}

/************************************************************************************
 * Callback, button 'Shift down' pressed
 */

static void ButtonShiftDownCallback(Fl_Widget *w, void *data)
{
  int iAnimation;

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  if( pDrawModel == NULL ||
      pDrawModel->anim_count <= 0) {

    return;
  }

  // ...

  iAnimation = pAnimationBrowser->value() - 1;  // get index of selected item

  IqeB_AnimAction( IQE_ANIMATE_ACTION_SHIFT_DOWN, iAnimation); // Shift named animation down

  // and update ...

  IqeB_GUI_ToolsAnimManagerUpdate( true);            // update Browser

  IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
}

/************************************************************************************
 * close_cb, close this window
 *
 * pValueArg is a pointer to the widget. Set this pointer to NULL on deletion.
 */

static void close_cb( Fl_Widget *w, void *pValueArg)
{

  IqeB_GUI_CloseToolWindow( (void **)&pMyToolWin);
}

/************************************************************************************
 * Presets for this tools window
 *
 */

 #ifdef use_again // 29.04.2015 RR: Maybe reused later
static T_GUI_PreferenceEntry MyPreferences[] =
 {

  // Move Symmetric Joints

  { PREF_T_INT,   "MoveSymmetricJoints",   "0", &IqeB_ToolsAnimManager_MoveSymmetricJoints},
};

// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "ToolsAnimManager", MyPreferences, sizeof( MyPreferences) / sizeof( T_GUI_PreferenceEntry),
                                               (void **)(&pMyToolWin), &MyWinPosX, &MyWinPosY);
#else
// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "ToolsAnimManager", NULL, 0,
                                               (void **)(&pMyToolWin), &MyWinPosX, &MyWinPosY);
#endif

/************************************************************************************
 * IqeB_GUI_ToolsAnimManagerWin
 *
 * Open dialog
 */

void IqeB_GUI_ToolsAnimManagerWin( int xLeft, int xRight, int yTop, int yBotton)
{

  //
  // winodow already created --> show it
  //

  if( pMyToolWin != NULL) {         // already have tool window

    // Show invisble window or bring visible window to foreground

    pMyToolWin->show();          // show it

    return;
  }

  //
  // creation of window on first call
  //

  if( pMyToolWin == NULL) {  // no tool window until now

    int xPos, yPos;

    xPos = xRight;
    yPos = yTop;

    if( MyWinPosX != IQE_GUI_NO_WINPOS_X && MyWinPosY != IQE_GUI_NO_WINPOS_Y) { // have last window position

      xPos = MyWinPosX;
      yPos = MyWinPosY;
    }

    pMyToolWin = new Fl_Window( xPos, yPos, IQE_GUI_TOOLS_STD_WITDH, 400, "Animation/Manager");

    if( pMyToolWin == NULL) {  // security test

      return;
    }

    IqeB_GUI_RegisterCallback( (void **)&pMyToolWin, IqeB_GUI_UpdateCallback,
                               IQE_GUI_UPDATE_MODEL_CHANGE | IQE_GUI_UPDATE_ANIMATION);
  }

  //
  //  GUI things
  //

  int x, x1, xx, xx2, y1, y, yy, yy2;


  x1  = 4;
  xx  = pMyToolWin->w() - 8;
  yy  = 20;

  y1 = 20;
  y = y1;

  // Animations browser

  yy2 = 160; //340;

  pAnimationBrowser = new Fl_Browser( x1, y1, xx, yy2, "Animations");

  pAnimationBrowser->align( FL_ALIGN_TOP_LEFT);     // align for label

  pAnimationBrowser->type(FL_HOLD_BROWSER);        // use for single selection
  pAnimationBrowser->callback( IqeB_AnimManagerBrowser_Callback, NULL);
  pAnimationBrowser->tooltip( "Select animaton");

  y += yy2;

  // Name

  y += 12;

  pInputName = new Fl_Input( x1, y, xx, yy, "Name");
  pInputName->align( FL_ALIGN_TOP_LEFT);     // align for label
  pInputName->labelsize( 10);
  pInputName->tooltip( "Name of animation");
  pInputName->callback( IqeB_GUI_Input_Name_SetValue_Callback);

  y += yy;
  y += 12;

  // framerate

  x = x1;
  xx2 = (xx - 8) / 3;

  pFloat_FrameRate = new Fl_Float_Input( x, y, xx2, yy, "Framerate");
  pFloat_FrameRate->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_FrameRate->labelsize( 10);
  pFloat_FrameRate->tooltip( "Framerate of the current animation (frames per second)");
  pFloat_FrameRate->callback( IqeB_GUI_Float_SetValue_Callback);

  // enable checkbox

  x += xx2 + 4;
  pCheck_Looped = new Fl_Check_Button( x, y, xx2, yy, "Looped");
  pCheck_Looped->tooltip( "Looping flag of the current animation.");
  pCheck_Looped->callback( IqeB_GUI_CBox_SetValue_Callback);

  // framerate

  x += xx2 + 4;
  pInt_NumFrames = new Fl_Int_Input( x, y, xx2, yy, "# Frames");
  pInt_NumFrames->align( FL_ALIGN_TOP_LEFT);     // align for label
  pInt_NumFrames->labelsize( 10);
  pInt_NumFrames->tooltip( "Number of frames of the current animation.\n"
                           "Changing this value removes last frames or add\n"
                           "new frames to this animation.\n");
  pInt_NumFrames->callback( IqeB_GUI_Int_SetValue_Callback);

  y += yy;

  // Buttons

  y += 4;
  yy = 22;

  x = x1;
  xx2 = (xx - 8) / 3;

  pGUI_ButDelete = new Fl_Button( x, y, xx2, yy, "&Delete");
  pGUI_ButDelete->callback( ButtonDeleteCallback, NULL);
  pGUI_ButDelete->tooltip( "Delete selected animation");

  x += xx2 + 4;
  pGUI_ButDelAll = new Fl_Button( x, y, xx2, yy, "Del. &all");
  pGUI_ButDelAll->callback( ButtonDelAllCallback, NULL);
  pGUI_ButDelAll->tooltip( "Delete all animations");

  x += xx2 + 4;
  pGUI_ButShiftUp = new Fl_Button( x, y, xx2 / 2, yy, "@#+32<");
  pGUI_ButShiftUp->callback( ButtonShiftUpCallback, NULL);
  pGUI_ButShiftUp->tooltip( "Shift up selected animation");

  pGUI_ButShiftDown = new Fl_Button( x + xx2 / 2, y, xx2 / 2, yy, "@#+32>");
  pGUI_ButShiftDown->callback( ButtonShiftDownCallback, NULL);
  pGUI_ButShiftDown->tooltip( "Shift down selected animation");

  y += yy + 4;
  x = x1;

  pGUI_ButNew = new Fl_Button( x, y, xx2, yy, "&New");
  pGUI_ButNew->callback( ButtonNewCallback, NULL);
  pGUI_ButNew->tooltip( "Create a new animation."
                        "Deselect any animation to add the new animation"
                        "at the end of the list."
                        "If an animation is selected, the new one is"
                        "created at this position.");

  x += xx2 + 4;
  pGUI_ButNorm = new Fl_Button( x, y, xx2, yy, "Nor&malize");
  pGUI_ButNorm->callback( ButtonNormalizeCallback, NULL);
  pGUI_ButNorm->tooltip( "If this button is enabled your skeleton is not normalized!\n"
                         "  A normalized skeleton has zeroed joint rotation angles and\n"
                         "  joint sizes of 1.0.\n"
                         "* Recalculate joint location information and reset joint\n"
                         "  rotation angles to zero and joint sizes to 1.\n"
                         "  IqeBrowser needs 'normalized' skeletons to edit the\n"
                         "  skeletons joint locations with the mouse.\n"
                         "* Also recalculates the animations.");


  y += yy + 4;
  x = x1;

  // finish up

  pMyToolWin->size( pMyToolWin->w(), y + 4);  // final size of window

  pMyToolWin->end();
  pMyToolWin->set_non_modal();
  pMyToolWin->callback( close_cb, &pMyToolWin);

  IqeB_GUI_ToolsAnimManagerUpdate( false); // Update the GUI

  pMyToolWin->show();
}

/********************************** End Of File **********************************/
