/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Doc:
//		This file saves the features of the world map and
//	the position of each terrain in it. Next is described
//	the format:
//
//	[	ID				] [Size in bytes]	[Description]
//
//	[	HEADER			] [4 + 255 bytes]	[Header of the file]
//	[	WORLD_WIDTH		] [4 bytes]			[Width of the table]
//	[	WORLD_HEIGHT	] [4 bytes]			[Height of the table]
//	[	WORLD_TABLE		] [4 bytes * N]		[N = number of maps]
//
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define HEADER_VERSION					0x00010B // v0.1b
#define HEADER_DESCRIPTION				"Argentum Online C by Parra Studios"
#define WORLD_WIDTH						4
#define WORLD_HEIGHT					4
#define WORLD_TABLE						0,  1,  2,  3,	\
										4,  5,  6,  7,	\
										8,  9,  10, 11,	\
										12, 13, 14, 15
#define TERRAIN_SIZE					1024
#define TERRAIN_PATCH_COUNT				16
#define TERRAIN_PATCH_FRAME_VARIANCE	50
#define TERRAIN_PATCH_DESIRED_TRIANGLES	10000
#define TERRAIN_PATCH_POOL_SIZE			25000

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct header_type {
	unsigned int version;		//< File version
	char description[0xff];		//< File description
};

struct terrain_descriptor {
	unsigned int width;
	unsigned int height;
	unsigned int *buffer;
};

struct terrain_chunk {
	unsigned int size;
	unsigned int patch_count;
	unsigned int frame_variance;
	unsigned int desired_tris;
	unsigned int pool_size;
	unsigned char *heightmap;
	// ...
};

////////////////////////////////////////////////////////////
// Application entry point
////////////////////////////////////////////////////////////
int main(int argc, char *argv[]) {
	FILE * file;
	unsigned int i;
	unsigned int table_size = WORLD_WIDTH * WORLD_HEIGHT;
	unsigned int heightmap_size = TERRAIN_SIZE * TERRAIN_SIZE;

	struct header_type header	= { HEADER_VERSION,
									HEADER_DESCRIPTION };

	unsigned int width			=	WORLD_WIDTH;
	unsigned int height			=	WORLD_HEIGHT;
	unsigned int buffer[]		= { WORLD_TABLE };

	unsigned int size			=	TERRAIN_SIZE;
	unsigned int patch_count	=	TERRAIN_PATCH_COUNT;
	unsigned int frame_variance =	TERRAIN_PATCH_FRAME_VARIANCE;
	unsigned int desired_tris	=	TERRAIN_PATCH_DESIRED_TRIANGLES;
	unsigned int pool_size		=	TERRAIN_PATCH_POOL_SIZE;
	unsigned char * heightmap	=	0;

	file = fopen("terrain.descriptor", "wb+");
	
	if (file) {
		
		// write header
		fwrite(&header, sizeof(struct header_type), 1, file);

		// write table width
		fwrite(&width, sizeof(unsigned int), 1, file);

		// write table height
		fwrite(&height, sizeof(unsigned int), 1, file);

		// write table of maps
		fwrite(buffer, sizeof(unsigned int), width * height, file);

		// close file
		fclose(file);
	}

	// initialize heightmap by default
	heightmap = (unsigned char*)malloc(heightmap_size);

	memset(heightmap, 0, heightmap_size);

	// write table of maps by default
	for (i = 0; i < table_size; ++i) {
		char path[0xff];

		sprintf(path, "%d.terrain", buffer[i]); // get id from the table

		file = fopen(path, "wb+");

		if (file) {
			// write header
			fwrite(&header, sizeof(struct header_type), 1, file);

			// write terrain size
			fwrite(&size, sizeof(unsigned int), 1, file);

			// write terrain patch count
			fwrite(&patch_count, sizeof(unsigned int), 1, file);

			// write terrain frame variance
			fwrite(&frame_variance, sizeof(unsigned int), 1, file);

			// write terrain desired triangles
			fwrite(&desired_tris, sizeof(unsigned int), 1, file);

			// write terrain triangle pool size
			fwrite(&pool_size, sizeof(unsigned int), 1, file);

			// write terrain heightmap
			fwrite(&heightmap, sizeof(unsigned char), heightmap_size, file);

			// write list of elements
			// ...

			fclose(file);
		}
	}

	// delete heightmap
	free(heightmap);

	return 0;
}