/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef MEMORY_ALIGNMENT_H
#define MEMORY_ALIGNMENT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Compiler.h>
#include <Portability/Type.h>
#include <Preprocessor/Concatenation.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Memory pack alignment macros
////////////////////////////////////////////////////////////
#if COMPILER_TYPE == COMPILER_MSVC

#	define MEMORY_PACK_ENABLE(Align)	__pragma(pack(push)) \
											__pragma(pack(Align))
#	define MEMORY_PACK_DISABLE()		__pragma(pack(pop))
#	define MEMORY_PACK

#elif COMPILER_TYPE == COMPILER_GNUC || COMPILER_TYPE == COMPILER_MINGW

#	define MEMORY_PACK_ENABLE(Align)
#	define MEMORY_PACK_DISABLE()
#	define MEMORY_PACK				__attribute__((packed))

#else
//	todo: add implementations for more compilers
#	error This compiler does not support explicit memory alignment
#endif

////////////////////////////////////////////////////////////
/// Memory size alignment macros
////////////////////////////////////////////////////////////
#define MEMORY_ALIGN_SIZE_TEMPLATE(Expr) \
	Expr(integer) \
	Expr(uinteger)

////////////////////////////////////////////////////////////
/// Memory size alignment macros for method declaration
////////////////////////////////////////////////////////////
#define MEMORY_ALIGN_SIZE_DECL(Type) \
	Type PREPROCESSOR_CONCAT(MemoryAlginSize, Type)(Type Size, PREPROCESSOR_CONCAT(Type, ptr) Algin);

	// Declare memory align size functions
	MEMORY_ALIGN_SIZE_TEMPLATE(MEMORY_ALIGN_SIZE_DECL)

#undef MEMORY_ALIGN_SIZE_DECL

////////////////////////////////////////////////////////////
/// Memory size alignment macro wrapper
////////////////////////////////////////////////////////////
#define MemoryAlignSize(Type, Size, Align) \
	PREPROCESSOR_CONCAT(MemoryAlginSize, Type)(Size, Align)

////////////////////////////////////////////////////////////
/// Memory pointer alignment wrapper macro
////////////////////////////////////////////////////////////
#define MemoryAlginPointer(Type, Ptr, Align) \
	(Type *)MemoryAlignPointerImpl(Ptr, Align)

#endif // MEMORY_ALIGNMENT_H
