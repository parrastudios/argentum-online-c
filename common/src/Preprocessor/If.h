/////////////////////////////////////////////////////////////////////////////
//	Preprocessor Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A cross-platform macro based library to give support
//	for preprocessor metaprogramming.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PREPROCESSOR_IF_H
#define PREPROCESSOR_IF_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Preprocessor/Boolean.h>

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define PREPROCESSOR_IIF_IMPL_0(True, False) False
#define PREPROCESSOR_IIF_IMPL_1(True, False) True

#if COMPILER_TYPE == COMPILER_MSVC

#	define PREPROCESSOR_IIF_IMPL_I(Expr) Expr

#	define PREPROCESSOR_IIF_IMPL(Value, True, False) PREPROCESSOR_IIF_IMPL_I(PREPROCESSOR_IIF_IMPL_ ## Value(True, False))

#else

#	define PREPROCESSOR_IIF_IMPL(Value, True, False) PREPROCESSOR_IIF_IMPL_ ## Value(True, False)

#endif

#if COMPILER_TYPE == COMPILER_MWERKS

#	define PREPROCESSOR_IIF_EXPAND(Expr) PREPROCESSOR_IIF_IMPL ## Expr

#	define PREPROCESSOR_IIF(Value, True, False) PREPROCESSOR_IIF_EXPAND((Value, True, False))

#else

#	define PREPROCESSOR_IIF(Value, True, False) PREPROCESSOR_IIF_IMPL(Value, True, False)

#endif

#if COMPILER_TYPE == COMPILER_EDG

#	define PREPROCESSOR_IF_IMPL(Condition, True, False) PREPROCESSOR_IIF(PREPROCESSOR_BOOL(Condition), True, False)

#	define PREPROCESSOR_IF(Condition, True, False) PREPROCESSOR_IF_IMPL(Condition, True, False)

#else

#	define PREPROCESSOR_IF(Condition, True, False) PREPROCESSOR_IIF(PREPROCESSOR_BOOL(Condition), True, False)

#endif


/*
#define PREPROCESSOR_IF_IMPL_0(True, ...)
#define PREPROCESSOR_IF_IMPL_1(True, ...) True

#define PREPROCESSOR_IF_IMPL(Expr) \
	PREPROCESSOR_CONCAT(PREPROCESSOR_IF_IMPL_, Expr)

#define PREPROCESSOR_IF(Expr) \
	PREPROCESSOR_IF_IMPL(PREPROCESSOR_BOOL(Expr))

#define PREPROCESSOR_IF_ELSE_IMPL_0(True, ...) __VA_ARGS__
#define PREPROCESSOR_IF_ELSE_IMPL_1(True, ...) True

#define PREPROCESSOR_IF_ELSE_IMPL(Expr) \
	PREPROCESSOR_CONCAT(PREPROCESSOR_IF_ELSE_IMPL_, Expr)

#define PREPROCESSOR_IF_ELSE(Expr) \
	PREPROCESSOR_IF_ELSE_IMPL(PREPROCESSOR_BOOL(Expr))
	*/

#endif // PREPROCESSOR_IF_H
