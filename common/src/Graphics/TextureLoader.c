/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/TextureLoader.h>

#ifndef TEXTURE_HELPER_READ_IMPL
#	define TEXTURE_HELPER_READ_IMPL
#	include <Graphics/TextureHelper.h>
#endif
#include <System/Error.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
/// Load pixels from an image file
////////////////////////////////////////////////////////////
bool TextureLoadFromFile(char * Filename, struct Texture * TextureData)
{
    // Load the image and get a pointer to the pixels in memory
    int32 Width, Height, Channels;
    uint8 * PixelsPtr = stbi_load(Filename, &Width, &Height, &Channels, TEXTURE_LOAD_RGBA);

    if (PixelsPtr)
    {
        // Copy the loaded pixels to the pixel buffer
        TextureData->Pixels = PixelsPtr;
        TextureData->Width = Width;
        TextureData->Height = Height;
        TextureData->Channels = Channels;

        return true;
    }
    else
    {
        // Error, failed to load the image
		MessageError("TextureLoadFromFile", "Failed to load image %s. Reason: %s.", Filename, stbi_failure_reason());
        return false;
    }
}

////////////////////////////////////////////////////////////
/// Load pixels from an image file
////////////////////////////////////////////////////////////
bool TextureLoadFromFileEx(char * Filename, struct Texture * TextureData, struct Rect * SubTexture)
{
    // Load the image and get a pointer to the pixels in memory
    int32 Width, Height, Channels;
	uint8 * PixelsPtr = stbi_load(Filename, &Width, &Height, &Channels, TEXTURE_LOAD_RGBA);

    if (PixelsPtr)
    {
		integer x, y;
		uinteger SubWidth = (uinteger)abs(SubTexture->Right - SubTexture->Left);
		uinteger SubHeight = (uinteger)abs(SubTexture->Bottom - SubTexture->Top);

        // Copy the loaded pixels to the pixel buffer
        uint8 * SubPixels = MemoryAllocate(SubWidth * SubHeight * Channels * sizeof(uint8));

        for (x = SubTexture->Left; x < SubTexture->Right; ++x)
        {
            for (y = SubTexture->Top; y < SubTexture->Bottom; ++y)
            {
                uinteger PixelPos = (x + y * Width) * Channels;
                uinteger SubPixelPos = ((x - SubTexture->Left) + (y - SubTexture->Top) * SubWidth) * Channels;
                integer ChannelCount;

                for (ChannelCount = 0; ChannelCount < Channels; ++ChannelCount)
                {
                    SubPixels[SubPixelPos + ChannelCount] = PixelsPtr[PixelPos + ChannelCount];
                }
            }
        }

        TextureData->Pixels = SubPixels;
        TextureData->Width = SubWidth;
        TextureData->Height = SubHeight;
        TextureData->Channels = Channels;

        // Free the loaded pixels (they are now in our own pixel buffer)
        MemoryDeallocate(PixelsPtr);

        return true;
    }
    else
    {
        // Error, failed to load the image
		MessageError("TextureLoadFromFile", "Failed to load image %s. Reason: %s.", Filename, stbi_failure_reason());
        return false;
    }
}

////////////////////////////////////////////////////////////
/// Load pixels from an image file in memory
////////////////////////////////////////////////////////////
bool TextureLoadFromMemory(uint8 * Data, uinteger SizeInBytes, struct Texture * TextureData)
{
    // Load the image and get a pointer to the pixels in memory
    int32 Width, Height, Channels;
    uint8 * PixelsPtr = stbi_load_from_memory(Data, SizeInBytes, &Width, &Height, &Channels, TEXTURE_LOAD_RGBA);

    if (PixelsPtr)
    {
        // Copy the loaded pixels to the pixel buffer
        TextureData->Pixels = PixelsPtr;
        TextureData->Width = Width;
        TextureData->Height = Height;
        TextureData->Channels = Channels;

        return true;
    }
    else
    {
        // Error, failed to load the image
		MessageError("TextureLoadFromFile", "Failed to load image from memory. Reason: %s", stbi_failure_reason());
        return false;
    }
}
