/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/ModelAnimation.h>

////////////////////////////////////////////////////////////
// Member prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a animation component of a model
////////////////////////////////////////////////////////////
struct EntityType * ModelAnimationCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a model animation component
////////////////////////////////////////////////////////////
void ModelAnimationDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the model animation implementation attributes
////////////////////////////////////////////////////////////
const void * ModelComponentAnimationAttr()
{
	static const struct ModelComponentAnimationType ModelComponentAnimationAttr = {
		TypeInit(TODO, NULL)
	};

	return (const void *)&ModelComponentAnimationAttr;
}

////////////////////////////////////////////////////////////
/// Return the model animation implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * ModelComponentAnimationImpl()
{
	static const struct ComponentImplType ModelComponentAnimationTypeImpl = {
		ExprInit(Base, TypeInit(Create, &ModelAnimationCreate), TypeInit(Destroy, &ModelAnimationDestroy)),
		TypeInit(Type, COMPONENT_TYPE_ANIMATION),
		UnionInit(Attributes, Derived, &ModelComponentAnimationAttr)
	};

	return &ModelComponentAnimationTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a animation component of a model
////////////////////////////////////////////////////////////
struct EntityType * ModelAnimationCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain animation component data reference
		struct ComponentType * Animation = EntityComponentAnimation(Entity);
		struct ModelComponentAnimationData * AnimationData = ComponentDataDerived(Animation, struct ModelComponentAnimationData);

		// ...

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set animation model name
////////////////////////////////////////////////////////////
void ModelAnimationSetName(struct EntityType * Entity, char * Name)
{
	if (Entity)
	{
		// Obtain animation component data reference
		struct ComponentType * Animation = EntityComponentAnimation(Entity);
		struct ModelComponentAnimationData * AnimationData = ComponentDataDerived(Animation, struct ModelComponentAnimationData);

		// ...

	}
}

////////////////////////////////////////////////////////////
/// Destroy a model animation component
////////////////////////////////////////////////////////////
void ModelAnimationDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain animation component data reference
		struct ComponentType * Animation = EntityComponentAnimation(Entity);
		struct ModelComponentAnimationData * AnimationData = ComponentDataDerived(Animation, struct ModelComponentAnimationData);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the model animation derived data size
////////////////////////////////////////////////////////////
uinteger ModelComponentAnimationSize()
{
	return sizeof(struct ModelComponentAnimationData);
}
