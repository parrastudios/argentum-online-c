/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Vector.h>
#include <Math/Geometry/Matrix.h>

void Vector2fCopy(struct Vector2f * Dest, struct Vector2f * Src)
{
	if (Dest && Src)
	{
		Dest->x = Src->x; Dest->y = Src->y;
	}
}

void Vector2fSet(struct Vector2f * Vector, float x, float y)
{
	if (Vector)
	{
		Vector->x = x;
		Vector->y = y;
	}
}

void Vector2iSet(struct Vector2i * Vector, integer x, integer y)
{
	if (Vector)
	{
		Vector->x = x;
		Vector->y = y;
	}
}

void Vector2iSetNull(struct Vector2i * Vector)
{
	if (Vector)
	{
		Vector->x = 0;
		Vector->y = 0;
	}
}

struct Vector2f * Vector2fSetImpl(struct Vector2f * Vector, float x, float y)
{
	if (Vector)
	{
		Vector->x = x;
		Vector->y = y;
	}

	return Vector;
}

void Vector2fSetNull(struct Vector2f * Vector)
{
	if (Vector)
	{
		Vector->x = Vector->y = 0.0f;
	}
}

void Vector3fIndentity(struct Vector3f * Vector)
{
	if (Vector)
	{
		Vector->x = Vector->y = Vector->z = 1.0f;
	}
}

void Vector3fAxisX(struct Vector3f * Vector)
{
	if (Vector)
	{
		Vector->x = 1.0f;
		Vector->y = 0.0f;
		Vector->z = 0.0f;
	}
}

void Vector3fAxisY(struct Vector3f * Vector)
{
	if (Vector)
	{
		Vector->x = 0.0f;
		Vector->y = 1.0f;
		Vector->z = 0.0f;
	}
}

void Vector3fAxisZ(struct Vector3f * Vector)
{
	if (Vector)
	{
		Vector->x = 0.0f;
		Vector->y = 0.0f;
		Vector->z = 1.0f;
	}
}

void Vector3fCopy(struct Vector3f * Dest, struct Vector3f * Src)
{
	if (Dest && Src)
	{
		Dest->x = Src->x; Dest->y = Src->y; Dest->z = Src->z;
	}
}

void Vector3fSetNull(struct Vector3f * Vector)
{
	if (Vector)
	{
		Vector->x = Vector->y = Vector->z = 0.0f;
	}
}

bool Vector3fIsNull(struct Vector3f * Vector)
{
	if (Vector)
	{
		return (bool)(Vector->x == 0.0f && Vector->y == 0.0f && Vector->z == 0.0f);
	}

	return false;
}

bool Vector3fIsEqual(struct Vector3f * Left, struct Vector3f * Right)
{
	if (Left && Right)
	{
		const float Epsilon = 0.00001f;
		struct Vector3f Vector;

		Vector3fSubtractEx(&Vector, Left, Right);

		return (Vector3fLengthSquared(&Vector) < Epsilon);
	}

	return false;
}

void Vector3fSet(struct Vector3f * Vector, float x, float y, float z)
{
	if (Vector)
	{
		Vector->x = x;
		Vector->y = y;
		Vector->z = z;
	}
}

void Vector3iSet(struct Vector3i * Vector, integer x, integer y, integer z)
{
	if (Vector)
	{
		Vector->x = x;
		Vector->y = y;
		Vector->z = z;
	}
}

struct Vector3f * Vector3fSetImpl(struct Vector3f * Vector, float x, float y, float z)
{
	if (Vector)
	{
		Vector->x = x;
		Vector->y = y;
		Vector->z = z;
	}

	return Vector;
}

void Vector3fAdd(struct Vector3f * Dest, struct Vector3f * Src)
{
	if (Dest && Src)
	{
		Dest->x += Src->x;
		Dest->y += Src->y;
		Dest->z += Src->z;
	}
}

void Vector3fAddEx(struct Vector3f * Dest, struct Vector3f * Left, struct Vector3f * Right)
{
	if (Dest && Left && Right)
	{
		Dest->x = Left->x + Right->x;
		Dest->y = Left->y + Right->y;
		Dest->z = Left->z + Right->z;
	}
}

void Vector3fSubtract(struct Vector3f * Dest, struct Vector3f * Src)
{
	if (Dest && Src)
	{
		Dest->x -= Src->x;
		Dest->y -= Src->y;
		Dest->z -= Src->z;
	}
}

void Vector3fSubtractEx(struct Vector3f * Dest, struct Vector3f * Left, struct Vector3f * Right)
{
	if (Dest && Left && Right)
	{
		Dest->x = Left->x - Right->x;
		Dest->y = Left->y - Right->y;
		Dest->z = Left->z - Right->z;
	}
}

void Vector3fIncrement(struct Vector3f * Vector, float x, float y, float z)
{
	if (Vector)
	{
		Vector->x += x;
		Vector->y += y;
		Vector->z += z;
	}
}

void Vector3fDecrement(struct Vector3f * Vector, float x, float y, float z)
{
	if (Vector)
	{
		Vector->x -= x;
		Vector->y -= y;
		Vector->z -= z;
	}
}

void Vector3fMultiply(struct Vector3f * Vector, float Scale)
{
	if (Vector)
	{
		Vector->x *= Scale;
		Vector->y *= Scale;
		Vector->z *= Scale;
	}
}

void Vector3fMultiplyEx(struct Vector3f * Dest, struct Vector3f * Source, float Scale)
{
	if (Source && Dest)
	{
		Dest->x = Source->x * Scale;
		Dest->y = Source->y * Scale;
		Dest->z = Source->z * Scale;
	}
}

void Vector3fMultiplyVectorEx(struct Vector3f * Dest, struct Vector3f * Source, struct Vector3f * Scale)
{
	if (Source && Dest && Scale)
	{
		Dest->x = Source->x * Scale->x;
		Dest->y = Source->y * Scale->y;
		Dest->z = Source->z * Scale->z;
	}
}

void Vector3fDivide(struct Vector3f * Vector, float Scale)
{
	if (Vector)
	{
		Vector->x /= Scale;
		Vector->y /= Scale;
		Vector->z /= Scale;
	}
}

void Vector3fDivideVectorEx(struct Vector3f * Dest, struct Vector3f * Source, struct Vector3f * Scale)
{
	if (Source && Dest && Scale)
	{
		Dest->x = Source->x / Scale->x;
		Dest->y = Source->y / Scale->y;
		Dest->z = Source->z / Scale->z;
	}
}

void Vector3fOpposite(struct Vector3f * Dest, struct Vector3f * Src)
{
	if (Dest && Src)
	{
		Dest->x = -Src->x;
		Dest->y = -Src->y;
		Dest->z = -Src->z;
	}
}

float Vector3fLength(struct Vector3f * Vector)
{
	if (Vector)
	{
		return (float)sqrt(Vector->x * Vector->x + Vector->y * Vector->y + Vector->z * Vector->z);
	}

	return 0.0f;
}

float Vector3fLengthSquared(struct Vector3f * Vector)
{
	if (Vector)
	{
		return Vector->x * Vector->x + Vector->y * Vector->y + Vector->z * Vector->z;
	}

	return 0.0f;
}

void Vector3fNormalize(struct Vector3f * Vector)
{
	if (Vector)
	{
		float Length = Vector3fLength(Vector);

		// Protect zero division
		if (Length > 0.0f)
		{
			// Normalize it
			float InverseLength = 1.0f / (float)Length;

			Vector3fMultiply(Vector, InverseLength);
		}
	}
}

void Vector3fRotate(struct Vector3f * Vector, float Degres, uinteger Coord)
{
	if (Vector && Degres != 0.0f)
	{
		float TempCoord;
		Degres = degtorad(Degres);

		switch (Coord)
		{
			case VECTOR_COORD_X :
			{
				TempCoord = Vector->y;
				Vector->y = (float)(cos(Degres) * Vector->y + sin(Degres) * Vector->z);
				Vector->z = (float)(-sin(Degres) * TempCoord + cos(Degres) * Vector->z);
				break;
			}

			case VECTOR_COORD_Y :
			{
				TempCoord = Vector->x;
				Vector->x = (float)(cos(Degres) * Vector->x - sin(Degres) * Vector->z);
				Vector->z = (float)(sin(Degres) * TempCoord - cos(Degres) * Vector->z);
				break;
			}

			case VECTOR_COORD_Z :
			{
				TempCoord = Vector->x;
				Vector->x = (float)(cos(Degres) * Vector->x + sin(Degres) * Vector->y);
				Vector->y = (float)(-sin(Degres) * TempCoord + cos(Degres) * Vector->y);
				break;
			}
		}
	}
}

float Vector3fDot(struct Vector3f * v1, struct Vector3f * v2)
{
	if (v1 && v2)
	{
		return v1->x * v2->x + v1->y * v2->y + v1->z * v2->z;
	}

	return 0.0f;
}

void Vector3fCross(struct Vector3f * v1, struct Vector3f * v2, struct Vector3f * Result)
{
	if (v1 && v2 && Result)
	{
		Result->x = v1->y * v2->z - v1->z * v2->y;
		Result->y = v1->z * v2->x - v1->x * v2->z;
		Result->z = v1->x * v2->y - v1->y * v2->x;
	}
}

void Vector3fToArray(struct Vector3f * Vector, float * Array)
{
	if (Vector && Array)
	{
		Array[0] = Vector->x;
		Array[1] = Vector->y;
		Array[2] = Vector->z;
	}
}

float Vector3fDistance(struct Vector3f * v1, struct Vector3f * v2)
{
	float x = v1->x - v2->x;
	float y = v1->y - v2->y;
	float z = v1->z - v2->z;

	return (float)sqrt(x * x + y * y + z * z);
}

void Vector3fGetDimensions(struct Vector3f * Vertices, float VertsSize, struct Vector3f * Center, float * Size)
{
	float MaxWidth = 0.0f, MaxHeight = 0.0f, MaxDepth = 0.0f;
	uinteger i;

	if (!Vertices || VertsSize <= 0 || !Center || !Size)
		return;

	Vector3fSetNull(Center);

	for (i = 0; i < VertsSize; i++)
	{
		Vector3fAdd(Center, &Vertices[i]);
	}

	Vector3fDivide(Center, VertsSize);

	for (i = 0; i < VertsSize; i++)
	{
		float Width  = fabsf(Vertices[i].x - Center->x);
		float Height = fabsf(Vertices[i].y - Center->y);
		float Depth	 = fabsf(Vertices[i].z - Center->z);

		if (Width > MaxWidth)
			MaxWidth = Width;

		if (Height > MaxHeight)
			MaxHeight = Height;

		if (Depth > MaxDepth)
			MaxDepth = Depth;
	}

	MaxWidth *= 2; MaxHeight *= 2; MaxDepth *= 2;

	if (MaxWidth > MaxHeight && MaxWidth > MaxDepth)
		*Size = MaxWidth;
	else if (MaxHeight > MaxWidth && MaxHeight > MaxDepth)
		*Size = MaxHeight;
	else
		*Size = MaxDepth;

}

void Vector3fLerp(struct Vector3f * Dest, struct Vector3f * Src, float Factor)
{
	Vector3fLerpEx(Dest, Src, Factor, Dest);
}

void Vector3fLerpEx(struct Vector3f * Left, struct Vector3f * Right, float Factor, struct Vector3f * Dest)
{
	float t = 1.0f - Factor;

	Dest->x = Left->x * Factor + Right->x * t;
	Dest->y = Left->y * Factor + Right->y * t;
	Dest->z = Left->z * Factor + Right->z * t;
}

float Vector3fGetPhi(struct Vector3f * Vector)
{
	float x = Vector3fDot(Vector, Vector);

	if (x < MATH_EPSILON)
	{
		return 0.0f;
	}

	x = Vector->z / (float)sqrt(x);

	if (x < -1.0f)
	{
		x = -1.0f;
	}
	else if (x > 1.0f)
	{
		x = 1.0f;
	}

	return (float)acos(x);
}

float Vector3fGetTheta(struct Vector3f * Vector)
{
	float Theta;
	float x = Vector->x * Vector->x + Vector->y * Vector->y;

	if (x < MATH_EPSILON)
	{
		return 0.0f;
	}

	x = Vector->x / (float)sqrt(x);

	if (x < -1.0f)
	{
		x = -1.0f;
	}
	else if (x > 1.0f)
	{
		x = 1.0f;
	}

	Theta = (float)acos(x);

	if (Vector->y < 0.0f)
		Theta = 2.0f * MATH_PI - Theta;

	return Theta;
}

void Vector3fSetPhi(struct Vector3f * Vector, float Phi)
{
	float Length;
	float Theta;

	while (Phi >= 2.0f * MATH_PI)
	{
		Phi -= 2.0f * MATH_PI;
	}

	while (Phi < 0.0f)
	{
		Phi += 2.0f * MATH_PI;
	}

	Theta = Vector3fGetTheta(Vector);
	Length = Vector3fLength(Vector);

	Vector->x = (float)(Length * sin(Phi) * cos(Theta));
	Vector->y = (float)(Length * sin(Phi) * sin(Theta));
	Vector->z = (float)(Length * cos(Phi));
}

void Vector3fSetTheta(struct Vector3f * Vector, float Theta)
{
	float Length;
	float Phi;

	while (Theta >= 2.0f * MATH_PI)
	{
		Theta -= 2.0f * MATH_PI;
	}

	while (Theta < 0.0f)
	{
		Theta += 2.0f * MATH_PI;
	}

	Phi = Vector3fGetPhi(Vector);
	Length = Vector3fLength(Vector);

	Vector->x = (float)(Length * sin(Phi) * cos(Theta));
	Vector->y = (float)(Length * sin(Phi) * sin(Theta));
}

void Vector3fRotateSpherical(struct Vector3f * Vector, float ThetaOffset, float PhiOffset)
{
	float Theta = Vector3fGetTheta(Vector) + ThetaOffset;
	float Phi = Vector3fGetPhi(Vector) + PhiOffset;
	float Length = Vector3fLength(Vector);

	while (Theta >= 2.0f * MATH_PI)
	{
		Theta -= 2.0f * MATH_PI;
	}

	while (Theta < 0.0f)
	{
		Theta += 2.0f * MATH_PI;
	}

	while (Phi >= 2.0f * MATH_PI)
	{
		Phi -= 2.0f * MATH_PI;
	}

	while (Phi < 0.0f)
	{
		Phi += 2.0f * MATH_PI;
	}

	Vector->x = (float)(Length * sin(Phi) * cos(Theta));
	Vector->y = (float)(Length * sin(Phi) * sin(Theta));
	Vector->z = (float)(Length * cos(Phi));
}

void Vector3fRotateAxis(struct Vector3f * Axis, struct Vector3f * Target, float Radians)
{
	struct Matrix3f Matrix;
	struct Vector3f OriginTarget;

	Vector3fCopy(&OriginTarget, Target);

	Matrix3fRotationVector(&Matrix, Axis, Radians);

	Target->x = OriginTarget.x * Matrix.m[0][0] + OriginTarget.y * Matrix.m[1][0] + OriginTarget.z * Matrix.m[2][0];
	Target->y = OriginTarget.x * Matrix.m[0][1] + OriginTarget.y * Matrix.m[1][1] + OriginTarget.z * Matrix.m[2][1];
	Target->z = OriginTarget.x * Matrix.m[0][2] + OriginTarget.y * Matrix.m[1][2] + OriginTarget.z * Matrix.m[2][2];
}

void Vector4fSet(struct Vector4f * Vector, float x, float y, float z, float w)
{
	if (Vector)
	{
		Vector->x = x;
		Vector->y = y;
		Vector->z = z;
		Vector->w = w;
	}
}
