/**
 * Create a C struct using the parameters
 */
function structBuilder(structName, columnAttributes, fks) {

  var struct = 'struct '
    + structName
    + ' \n{';

  var parentFKs = [];
  var childFKs = [];

  for (var i = 0; i < columnAttributes.length; i++) {

    struct += '\n\t'
    + mysqlTypeToC(columnAttributes[i].columnType)
    + '\t\t'
    + columnAttributes[i].columnName + ';';

    var parentFK = getParentForeignKeysByColumnName(columnAttributes[i].columnName, structName, fks);
    var childFK = getChildForeignKeysByColumnName(columnAttributes[i].columnName, structName, fks);

    if (parentFK) {
      parentFKs.push(parentFK);
    }

    if (childFK) {
      childFKs.push(childFK);
    }
  }
  struct += '\n\n';

  parentFKs.forEach(function (element) {
    struct += '\tForeignKey\t'
    + element
    + '; ///< Foreign Key\n'
  }, this);


  childFKs.forEach(function (element) {
    struct += '\tForeignKey\t'
    + element
    + '; ///< Foreign Key\n'
  }, this);

  struct += '};\n';

  return struct;
}

/**
 * Convert MySQL types to C types
 */
function mysqlTypeToC(type) {

  var map = [];

  map['int'] = 'int';
  map['tinyint'] = 'int8';
  map['short'] = 'int16';
  map['long'] = 'int32';
  map['longlong'] = 'int64';
  map['float'] = 'float32';
  map['double'] = 'float64';
  map['char'] = 'String';
  map['varchar'] = 'String';
  map['blob'] = 'Blob';

  return typeof (map[type]) === 'undefined' ? 'void *' : map[type];
}

/**
 * Gets a parent foreign key by table name
 */
function getParentForeignKeysByColumnName(columnName, structName, fks) {

  var map = [];

  for (var foreignKey in fks) {

    for (var parentTableName in fks[foreignKey]) {

      var childTableName = fks[foreignKey].childTableName;
      var childColumnName = fks[foreignKey].childColumnName;
      var parentColumnName = fks[foreignKey].parentColumnName;
      var parentTableName = fks[foreignKey].parentTableName;

      if (structName == parentTableName)
        map[parentColumnName] = childTableName;

    }
  }

  return typeof (map[columnName]) === 'undefined' ? false : map[columnName];
}

/**
 * Gets a child foreign key by table name
 */
function getChildForeignKeysByColumnName(columnName, structName, fks) {

  var map = [];

  for (var foreignKey in fks) {

    for (var parentTableName in fks[foreignKey]) {

      var childTableName = fks[foreignKey].childTableName;
      var childColumnName = fks[foreignKey].childColumnName;
      var parentColumnName = fks[foreignKey].parentColumnName;
      var parentTableName = fks[foreignKey].parentTableName;

      if (structName == childTableName)
        map[childColumnName] = parentTableName;

    }
  }

  return typeof (map[columnName]) === 'undefined' ? false : map[columnName];
}



module.exports = {
  structBuilder: structBuilder
}
