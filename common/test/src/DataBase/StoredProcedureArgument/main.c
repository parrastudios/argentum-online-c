/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2016 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/StoredProcedureArgument.h>

#include <Memory/General.h>
#include <System/IOHelper.h> // todo: remove this

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define ARG_TEST_STRING_LENGTH UINTEGER_SUFFIX(0x10)

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Test stored procedure argument by value
////////////////////////////////////////////////////////////
void StoredProcedureArgumentValueTest()
{
	struct StoredProcedureArgumentListType Arguments;

	StoredProcedureArgumentDataValueType Value[5];

	const char String[ARG_TEST_STRING_LENGTH] = "0123456789ABCDF";
	char LocalString[ARG_TEST_STRING_LENGTH] = "0123456789ABCDF";

	int8 MyTiny;
	float32 MyFloat;
	int32 MyLong;
	char * MyStringPtr = NULL;
	uinteger MyStringLength = UINTEGER_SUFFIX(0);
	char * MyLocalStringPtr = NULL;

	// Create stored procedure argument list
	StoredProcedureArgumentCreate(&Arguments, STORED_PROCEDURE_BY_VALUE, 5);

	// Set values
	{
		// Initialize parameter 0
		Value[0].Tiny = '@';

		StoredProcedureArgumentSetValue(&Arguments, 0, STORED_PROCEDURE_TINY, &Value[0]);

		// Initialize parameter 1
		Value[1].Float = 360.0f;

		StoredProcedureArgumentSetValue(&Arguments, 1, STORED_PROCEDURE_FLOAT, &Value[1]);

		// Initialize parameter 2
		Value[2].Long = 34235983;

		StoredProcedureArgumentSetValue(&Arguments, 2, STORED_PROCEDURE_LONG, &Value[2]);

		// Initialize parameter 3 (string destruction must be managed)
		Value[3].String.Ptr = strdup(String);
		Value[3].String.Length = ARG_TEST_STRING_LENGTH;

		StoredProcedureArgumentSetValue(&Arguments, 3, STORED_PROCEDURE_STRING, &Value[3]);

		// Initialize parameter 4 (string must not be destroyed)
		Value[4].String.Ptr = LocalString;
		Value[4].String.Length = ARG_TEST_STRING_LENGTH;

		StoredProcedureArgumentSetValue(&Arguments, 4, STORED_PROCEDURE_STRING, &Value[4]);
	}

	// Now data is copied (by value) into the stored procedure
	// It can be retreived by value too, and local modifications will not modify the internal data

	// Get values
	{
		// Retreive parameter 0
		MyTiny = StoredProcedureArgumentGetValue(&Arguments, 0, STORED_PROCEDURE_TINY)->Tiny;

		// Retreive parameter 1
		MyFloat = StoredProcedureArgumentGetValue(&Arguments, 1, STORED_PROCEDURE_FLOAT)->Float;

		// Retreive parameter 2
		MyLong = StoredProcedureArgumentGetValue(&Arguments, 2, STORED_PROCEDURE_LONG)->Long;

		// Retreive parameter 3
		MyStringPtr = StoredProcedureArgumentGetValue(&Arguments, 3, STORED_PROCEDURE_STRING)->String.Ptr;
		MyStringLength = StoredProcedureArgumentGetValue(&Arguments, 3, STORED_PROCEDURE_STRING)->String.Length;

		// Retreive parameter 4
		MyLocalStringPtr = StoredProcedureArgumentGetValue(&Arguments, 4, STORED_PROCEDURE_STRING)->String.Ptr;
	}

	// Now internal values are copied to local variables
	// We can modify them and this will not affect the internal data (except from String and Blob which copy the reference, like Java)

	// Modify local values
	{
		MyTiny = '#';
		MyFloat = 180.0f;
		MyLong = 1000000;
		MyStringPtr[0] = '@';
		MyLocalStringPtr[0] = '?';
	}

	// Debug block
	{
		// Print local values
		printf("Local values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s, %d },\n\tLocal String { %s }\n\n",
			MyTiny, MyFloat, MyLong, MyStringPtr, MyStringLength, MyLocalStringPtr);

		// Print argument values (all will be different, except from string/blob which copies the pointer)
		printf("Argument list values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s, %d },\n\tLocal String { %s }\n\n",
			StoredProcedureArgumentGetValue(&Arguments, 0, STORED_PROCEDURE_TINY)->Tiny,
			StoredProcedureArgumentGetValue(&Arguments, 1, STORED_PROCEDURE_FLOAT)->Float,
			StoredProcedureArgumentGetValue(&Arguments, 2, STORED_PROCEDURE_LONG)->Long,
			StoredProcedureArgumentGetValue(&Arguments, 3, STORED_PROCEDURE_STRING)->String.Ptr,
			StoredProcedureArgumentGetValue(&Arguments, 3, STORED_PROCEDURE_STRING)->String.Length,
			StoredProcedureArgumentGetValue(&Arguments, 4, STORED_PROCEDURE_STRING)->String.Ptr);

		// Print pointers of strings (reference copy! like in Java)
		printf("String (allocated) pointers (local vs arg list):\n\t{ %p == %p }\n\n",
			MyStringPtr,
			StoredProcedureArgumentGetValue(&Arguments, 3, STORED_PROCEDURE_STRING)->String.Ptr);

		// Print pointers of strings (reference copy! like in Java)
		printf("String (local) pointers (local vs arg list):\n\t{ %p == %p }\n\n",
			MyLocalStringPtr,
			StoredProcedureArgumentGetValue(&Arguments, 4, STORED_PROCEDURE_STRING)->String.Ptr);
	}
	
	// Destruction block
	{
		// Destroy argument list
		StoredProcedureArgumentDestroy(&Arguments);

		// Destroy string (it must be delted manually as it has been allocated manually)
		MemoryDeallocate(MyStringPtr);
	}
}

////////////////////////////////////////////////////////////
/// Test stored procedure argument by value with builder
////////////////////////////////////////////////////////////
void StoredProcedureArgumentValueBuilderTest()
{
	struct StoredProcedureArgumentListType Arguments;

	const char String[ARG_TEST_STRING_LENGTH] = "0123456789ABCDF";
	char LocalString[ARG_TEST_STRING_LENGTH] = "0123456789ABCDF";

	int8 MyTiny = '@';
	float32 MyFloat = 360.0f;
	int32 MyLong = 34235983;
	char * MyStringPtr = strdup(String);
	uinteger MyStringLength = UINTEGER_SUFFIX(0);
	char * MyLocalStringPtr = NULL;

	// Create stored procedure argument list
	StoredProcedureArgumentCreate(&Arguments, STORED_PROCEDURE_BY_VALUE, 5);

	// Copy variables
	StoredProcedureArgumentSetBuildValue(&Arguments,
		(STORED_PROCEDURE_TINY, MyTiny),
		(STORED_PROCEDURE_FLOAT, 360.0f), // Testing: literals also work
		(STORED_PROCEDURE_LONG, MyLong),
		(STORED_PROCEDURE_STRING, MyStringPtr, ARG_TEST_STRING_LENGTH),
		(STORED_PROCEDURE_STRING, LocalString, ARG_TEST_STRING_LENGTH));

	// At this point arguments are binded by reference to local variables
	// Modifying any parameter will modify the local variables
	// References can be retreived and data which them point to modified

	// Debug block (print current data before modification)
	{
		// Print local values
		printf("Local values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s },\n\tLocal String { %s }\n\n",
			MyTiny, MyFloat, MyLong, MyStringPtr, LocalString);
	}

	// Now data is copied (by value) into the stored procedure
	// It can be retreived by value too, and local modifications will not modify the internal data

	// Get values
	{
		// Retreive parameter 0
		MyTiny = StoredProcedureArgumentGetValue(&Arguments, 0, STORED_PROCEDURE_TINY)->Tiny;

		// Retreive parameter 1
		MyFloat = StoredProcedureArgumentGetValue(&Arguments, 1, STORED_PROCEDURE_FLOAT)->Float;

		// Retreive parameter 2
		MyLong = StoredProcedureArgumentGetValue(&Arguments, 2, STORED_PROCEDURE_LONG)->Long;

		// Retreive parameter 3
		MyStringPtr = StoredProcedureArgumentGetValue(&Arguments, 3, STORED_PROCEDURE_STRING)->String.Ptr;
		MyStringLength = StoredProcedureArgumentGetValue(&Arguments, 3, STORED_PROCEDURE_STRING)->String.Length;

		// Retreive parameter 4
		MyLocalStringPtr = StoredProcedureArgumentGetValue(&Arguments, 4, STORED_PROCEDURE_STRING)->String.Ptr;
	}

	// Now internal values are copied to local variables
	// We can modify them and this will not affect the internal data (except from String and Blob which copy the reference, like Java)

	// Modify local values
	{
		MyTiny = '#';
		MyFloat = 180.0f;
		MyLong = 1000000;
		MyStringPtr[0] = '@';
		MyLocalStringPtr[0] = '?';
	}

	// Debug block
	{
		// Print local values
		printf("Local values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s, %d },\n\tLocal String { %s }\n\n",
			MyTiny, MyFloat, MyLong, MyStringPtr, MyStringLength, MyLocalStringPtr);

		// Print argument values (all will be different, except from string/blob which copies the pointer)
		printf("Argument list values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s, %d },\n\tLocal String { %s }\n\n",
			StoredProcedureArgumentGetValue(&Arguments, 0, STORED_PROCEDURE_TINY)->Tiny,
			StoredProcedureArgumentGetValue(&Arguments, 1, STORED_PROCEDURE_FLOAT)->Float,
			StoredProcedureArgumentGetValue(&Arguments, 2, STORED_PROCEDURE_LONG)->Long,
			StoredProcedureArgumentGetValue(&Arguments, 3, STORED_PROCEDURE_STRING)->String.Ptr,
			StoredProcedureArgumentGetValue(&Arguments, 3, STORED_PROCEDURE_STRING)->String.Length,
			StoredProcedureArgumentGetValue(&Arguments, 4, STORED_PROCEDURE_STRING)->String.Ptr);

		// Print pointers of strings (reference copy! like in Java)
		printf("String (allocated) pointers (local vs arg list):\n\t{ %p == %p }\n\n",
			MyStringPtr,
			StoredProcedureArgumentGetValue(&Arguments, 3, STORED_PROCEDURE_STRING)->String.Ptr);

		// Print pointers of strings (reference copy! like in Java)
		printf("String (local) pointers (local vs arg list):\n\t{ %p == %p }\n\n",
			MyLocalStringPtr,
			StoredProcedureArgumentGetValue(&Arguments, 4, STORED_PROCEDURE_STRING)->String.Ptr);
	}

	// Destruction block
	{
		// Destroy argument list
		StoredProcedureArgumentDestroy(&Arguments);

		// Destroy string (it must be delted manually as it has been allocated manually)
		MemoryDeallocate(MyStringPtr);
	}
}

////////////////////////////////////////////////////////////
/// Test stored procedure argument by value with builder ex
////////////////////////////////////////////////////////////
void StoredProcedureArgumentValueBuilderExTest()
{
	struct StoredProcedureArgumentListType Arguments;

	const char String[ARG_TEST_STRING_LENGTH] = "0123456789ABCDF";
	char LocalString[ARG_TEST_STRING_LENGTH] = "0123456789ABCDF";

	int8 MyTiny = '@';
	float32 MyFloat = 360.0f;
	int32 MyLong = 34235983;
	char * MyStringPtr = strdup(String);
	uinteger MyStringLength = UINTEGER_SUFFIX(0);
	char * MyLocalStringPtr = NULL;
	uinteger MyLocalStringLength = UINTEGER_SUFFIX(0);

	// Create stored procedure argument list
	StoredProcedureArgumentCreate(&Arguments, STORED_PROCEDURE_BY_VALUE, 5);

	// Copy variables
	StoredProcedureArgumentSetBuildValue(&Arguments,
		(STORED_PROCEDURE_TINY, MyTiny),
		(STORED_PROCEDURE_FLOAT, 360.0f), // Testing: literals also work
		(STORED_PROCEDURE_LONG, MyLong),
		(STORED_PROCEDURE_STRING, MyStringPtr, ARG_TEST_STRING_LENGTH),
		(STORED_PROCEDURE_STRING, LocalString, ARG_TEST_STRING_LENGTH));

	// At this point arguments are binded by reference to local variables
	// Modifying any parameter will modify the local variables
	// References can be retreived and data which them point to modified

	// Debug block (print current data before modification)
	{
		// Print local values
		printf("Local values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s },\n\tLocal String { %s }\n\n",
			MyTiny, MyFloat, MyLong, MyStringPtr, LocalString);
	}

	// Now data is copied (by value) into the stored procedure
	// It can be retreived by value too, and local modifications will not modify the internal data

	// Get values
	{
		StoredProcedureArgumentGetBuildValue(&Arguments,
			(STORED_PROCEDURE_TINY, MyTiny),
			(STORED_PROCEDURE_FLOAT, MyFloat),
			(STORED_PROCEDURE_LONG, MyLong),
			(STORED_PROCEDURE_STRING, MyStringPtr, MyStringLength),
			(STORED_PROCEDURE_STRING, MyLocalStringPtr, MyLocalStringLength));
	}

	// Now internal values are copied to local variables
	// We can modify them and this will not affect the internal data (except from String and Blob which copy the reference, like Java)

	// Modify local values
	{
		MyTiny = '#';
		MyFloat = 180.0f;
		MyLong = 1000000;
		MyStringPtr[0] = '@';
		MyLocalStringPtr[0] = '?';
	}

	// Debug block
	{
		// Print local values
		printf("Local values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s, %d },\n\tLocal String { %s }\n\n",
			MyTiny, MyFloat, MyLong, MyStringPtr, MyStringLength, MyLocalStringPtr);

		// Print argument values (all will be different, except from string/blob which copies the pointer)
		printf("Argument list values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s, %d },\n\tLocal String { %s }\n\n",
			StoredProcedureArgumentGetValue(&Arguments, 0, STORED_PROCEDURE_TINY)->Tiny,
			StoredProcedureArgumentGetValue(&Arguments, 1, STORED_PROCEDURE_FLOAT)->Float,
			StoredProcedureArgumentGetValue(&Arguments, 2, STORED_PROCEDURE_LONG)->Long,
			StoredProcedureArgumentGetValue(&Arguments, 3, STORED_PROCEDURE_STRING)->String.Ptr,
			StoredProcedureArgumentGetValue(&Arguments, 3, STORED_PROCEDURE_STRING)->String.Length,
			StoredProcedureArgumentGetValue(&Arguments, 4, STORED_PROCEDURE_STRING)->String.Ptr);

		// Print pointers of strings (reference copy! like in Java)
		printf("String (allocated) pointers (local vs arg list):\n\t{ %p == %p }\n\n",
			MyStringPtr,
			StoredProcedureArgumentGetValue(&Arguments, 3, STORED_PROCEDURE_STRING)->String.Ptr);

		// Print pointers of strings (reference copy! like in Java)
		printf("String (local) pointers (local vs arg list):\n\t{ %p == %p }\n\n",
			MyLocalStringPtr,
			StoredProcedureArgumentGetValue(&Arguments, 4, STORED_PROCEDURE_STRING)->String.Ptr);
	}

	// Destruction block
	{
		// Destroy argument list
		StoredProcedureArgumentDestroy(&Arguments);

		// Destroy string (it must be delted manually as it has been allocated manually)
		MemoryDeallocate(MyStringPtr);
	}
}

////////////////////////////////////////////////////////////
/// Test stored procedure argument by reference
////////////////////////////////////////////////////////////
void StoredProcedureArgumentReferenceTest()
{
	struct StoredProcedureArgumentListType Arguments;

	// Const string cannot be referenced directly, instead of we duplicate it as in the value example
	const char String[ARG_TEST_STRING_LENGTH] = "0123456789ABCDF";
	char LocalString[ARG_TEST_STRING_LENGTH] = "0123456789ABCDF";

	int8 MyTiny;
	float32 MyFloat;
	int32 MyLong;
	char * MyString = strdup(String);

	// Create stored procedure argument list
	StoredProcedureArgumentCreate(&Arguments, STORED_PROCEDURE_BY_REFERENCE, 5);

	// Set references
	{
		// Initialize parameter 0
		MyTiny = '@';

		StoredProcedureArgumentSetRef(&Arguments, 0, STORED_PROCEDURE_TINY, (StoredProcedureArgumentDataRefType)&MyTiny);

		// Initialize parameter 1
		MyFloat = 360.0f;

		StoredProcedureArgumentSetRef(&Arguments, 1, STORED_PROCEDURE_FLOAT, (StoredProcedureArgumentDataRefType)&MyFloat);

		// Initialize parameter 2
		MyLong = 34235983;

		StoredProcedureArgumentSetRef(&Arguments, 2, STORED_PROCEDURE_LONG, (StoredProcedureArgumentDataRefType)&MyLong);

		// Initialize parameter 3 (string destruction must be managed)
		StoredProcedureArgumentSetRef(&Arguments, 3, STORED_PROCEDURE_STRING, (StoredProcedureArgumentDataRefType)MyString);

		// Initialize parameter 4 (string must not be destroyed)
		StoredProcedureArgumentSetRef(&Arguments, 4, STORED_PROCEDURE_STRING, (StoredProcedureArgumentDataRefType)LocalString);
	}

	// At this point arguments are binded by reference to local variables
	// Modifying any parameter will modify the local variables
	// References can be retreived and data which them point to modified

	// Debug block (print current data before modification)
	{
		// Print local values
		printf("Local values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s },\n\tLocal String { %s }\n\n",
			MyTiny, MyFloat, MyLong, MyString, LocalString);
	}

	// Get references
	{
		int8 * MyTinyPtr;
		float32 * MyFloatPtr;
		int32 * MyLongPtr;
		char * MyStringPtr = NULL;
		char * MyLocalStringPtr = NULL;

		// Retreive parameter 0
		MyTinyPtr = (int8 *)StoredProcedureArgumentGetRef(&Arguments, 0, STORED_PROCEDURE_TINY);

		*MyTinyPtr = '#';

		// Retreive parameter 1
		MyFloatPtr = StoredProcedureArgumentGetRef(&Arguments, 1, STORED_PROCEDURE_FLOAT);

		*MyFloatPtr = 180.0f;

		// Retreive parameter 2
		MyLongPtr = StoredProcedureArgumentGetRef(&Arguments, 2, STORED_PROCEDURE_LONG);

		*MyLongPtr = 1000000;

		// Retreive parameter 3
		MyStringPtr = StoredProcedureArgumentGetRef(&Arguments, 3, STORED_PROCEDURE_STRING);

		MyStringPtr[0] = '@';

		// Retreive parameter 4
		MyLocalStringPtr = StoredProcedureArgumentGetRef(&Arguments, 4, STORED_PROCEDURE_STRING);

		MyLocalStringPtr[0] = '?';
	}

	// At this point, data has been modified by reference,
	// so contents of bound local values will be modified too

	// Debug block (print current data after modification)
	{
		// Print local values
		printf("Local values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s },\n\tLocal String { %s }\n\n",
			MyTiny, MyFloat, MyLong, MyString, LocalString);
	}

	// Destruction block
	{
		// Destroy argument list
		StoredProcedureArgumentDestroy(&Arguments);

		// Destroy string (it must be delted manually as it has been allocated manually)
		MemoryDeallocate(MyString);
	}
}

////////////////////////////////////////////////////////////
/// Test stored procedure argument by reference with builder
////////////////////////////////////////////////////////////
void StoredProcedureArgumentReferenceBuilderTest()
{
	struct StoredProcedureArgumentListType Arguments;

	// Const string cannot be referenced directly, instead of we duplicate it as in the value example
	const char String[ARG_TEST_STRING_LENGTH] = "0123456789ABCDF";
	char LocalString[ARG_TEST_STRING_LENGTH] = "0123456789ABCDF";

	int8 MyTiny = '@';
	float32 MyFloat = 360.0f;
	int32 MyLong = 34235983;
	char * MyString = strdup(String);

	// Create stored procedure argument list
	StoredProcedureArgumentCreate(&Arguments, STORED_PROCEDURE_BY_REFERENCE, 5);

	// Reference variables
	StoredProcedureArgumentSetBuildRef(&Arguments,
		(STORED_PROCEDURE_TINY, &MyTiny),
		(STORED_PROCEDURE_FLOAT, &MyFloat),
		(STORED_PROCEDURE_LONG, &MyLong),
		(STORED_PROCEDURE_STRING, MyString),
		(STORED_PROCEDURE_STRING, LocalString));

	// At this point arguments are binded by reference to local variables
	// Modifying any parameter will modify the local variables
	// References can be retreived and data which them point to modified

	// Debug block (print current data before modification)
	{
		// Print local values
		printf("Local values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s },\n\tLocal String { %s }\n\n",
			MyTiny, MyFloat, MyLong, MyString, LocalString);
	}

	// Get references
	{
		int8 * MyTinyPtr;
		float32 * MyFloatPtr;
		int32 * MyLongPtr;
		char * MyStringPtr = NULL;
		char * MyLocalStringPtr = NULL;

		// Retreive parameter 0
		MyTinyPtr = (int8 *)StoredProcedureArgumentGetRef(&Arguments, 0, STORED_PROCEDURE_TINY);

		*MyTinyPtr = '#';

		// Retreive parameter 1
		MyFloatPtr = StoredProcedureArgumentGetRef(&Arguments, 1, STORED_PROCEDURE_FLOAT);

		*MyFloatPtr = 180.0f;

		// Retreive parameter 2
		MyLongPtr = StoredProcedureArgumentGetRef(&Arguments, 2, STORED_PROCEDURE_LONG);

		*MyLongPtr = 1000000;

		// Retreive parameter 3
		MyStringPtr = StoredProcedureArgumentGetRef(&Arguments, 3, STORED_PROCEDURE_STRING);

		MyStringPtr[0] = '@';

		// Retreive parameter 4
		MyLocalStringPtr = StoredProcedureArgumentGetRef(&Arguments, 4, STORED_PROCEDURE_STRING);

		MyLocalStringPtr[0] = '?';
	}

	// At this point, data has been modified by reference,
	// so contents of bound local values will be modified too

	// Debug block (print current data after modification)
	{
		// Print local values
		printf("Local values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s },\n\tLocal String { %s }\n\n",
			MyTiny, MyFloat, MyLong, MyString, LocalString);
	}

	// Destruction block
	{
		// Destroy argument list
		StoredProcedureArgumentDestroy(&Arguments);

		// Destroy string (it must be delted manually as it has been allocated manually)
		MemoryDeallocate(MyString);
	}
}

////////////////////////////////////////////////////////////
/// Test stored procedure argument by reference with builder ex
////////////////////////////////////////////////////////////
void StoredProcedureArgumentReferenceBuilderExTest()
{
	struct StoredProcedureArgumentListType Arguments;

	// Const string cannot be referenced directly, instead of we duplicate it as in the value example
	const char String[ARG_TEST_STRING_LENGTH] = "0123456789ABCDF";
	char LocalString[ARG_TEST_STRING_LENGTH] = "0123456789ABCDF";

	int8 MyTiny = '@';
	float32 MyFloat = 360.0f;
	int32 MyLong = 34235983;
	char * MyString = strdup(String);

	// Create stored procedure argument list
	StoredProcedureArgumentCreate(&Arguments, STORED_PROCEDURE_BY_REFERENCE, 5);

	// Reference variables
	StoredProcedureArgumentSetBuildRef(&Arguments,
		(STORED_PROCEDURE_TINY, &MyTiny),
		(STORED_PROCEDURE_FLOAT, &MyFloat),
		(STORED_PROCEDURE_LONG, &MyLong),
		(STORED_PROCEDURE_STRING, MyString),
		(STORED_PROCEDURE_STRING, LocalString));

	// At this point arguments are binded by reference to local variables
	// Modifying any parameter will modify the local variables
	// References can be retreived and data which them point to modified

	// Debug block (print current data before modification)
	{
		// Print local values
		printf("Local values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s },\n\tLocal String { %s }\n\n",
			MyTiny, MyFloat, MyLong, MyString, LocalString);
	}

	// Get references
	{
		int8 * MyTinyPtr;
		float32 * MyFloatPtr;
		int32 * MyLongPtr;
		char * MyStringPtr = NULL;
		char * MyLocalStringPtr = NULL;

		StoredProcedureArgumentGetBuildRef(&Arguments,
			(STORED_PROCEDURE_TINY, MyTinyPtr),
			(STORED_PROCEDURE_FLOAT, MyFloatPtr),
			(STORED_PROCEDURE_LONG, MyLongPtr),
			(STORED_PROCEDURE_STRING, MyStringPtr),
			(STORED_PROCEDURE_STRING, MyLocalStringPtr));

		// Modify them
		*MyTinyPtr = '#';
		*MyFloatPtr = 180.0f;
		*MyLongPtr = 1000000;
		MyStringPtr[0] = '@';
		MyLocalStringPtr[0] = '?';
	}

	// At this point, data has been modified by reference,
	// so contents of bound local values will be modified too

	// Debug block (print current data after modification)
	{
		// Print local values
		printf("Local values:\n\tTiny { %c },\n\tFloat { %f },\n\tLong { %d },\n\tAllocated String { %s },\n\tLocal String { %s }\n\n",
			MyTiny, MyFloat, MyLong, MyString, LocalString);
	}

	// Destruction block
	{
		// Destroy argument list
		StoredProcedureArgumentDestroy(&Arguments);

		// Destroy string (it must be delted manually as it has been allocated manually)
		MemoryDeallocate(MyString);
	}
}


////////////////////////////////////////////////////////////
/// Main entry point
////////////////////////////////////////////////////////////
int main(int argc, char * argv[])
{
	printf("Stored Procedure Argument Test\n\n");

	printf("--------------- Value ---------------\n");

	// Test argument list by value
	StoredProcedureArgumentValueTest();

	printf("----------- Value Builder -----------\n");

	// Test argument list by value
	StoredProcedureArgumentValueBuilderTest();

	printf("---------- Value Builder Ex ----------\n");

	// Test argument list by value ex
	StoredProcedureArgumentValueBuilderExTest();

	printf("------------- Reference -------------\n");

	// Test argument list by reference
	StoredProcedureArgumentReferenceTest();

	printf("--------- Reference Builder ---------\n");

	// Test argument list by reference
	StoredProcedureArgumentReferenceBuilderTest();

	printf("-------- Reference Builder Ex --------\n");
	// Test argument list by reference ex
	StoredProcedureArgumentReferenceBuilderExTest();

	return 0;
}