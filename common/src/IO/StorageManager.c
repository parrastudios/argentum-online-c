/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <IO/StorageManager.h>
#include <DataType/List.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef struct SerialStorageType
{
	Set		ResourceList;					///< Set containing the data
	List	FreeList;						///< List referencing to free ids of the set
} SerialStorageType;

struct StorageManagerType
{
	SerialStorageType ReferenceStorage;		///< Managed as (index -> reference data (all elements with same size))

	SerialStorageType ValueStorage;			///< Managed as (index -> pointer to a variant size value)
	SerialStorageType ObjectStorage;		///< Managed as (index -> pointer to a variant size object)
	SerialStorageType FunctionStorage;		///< Managed as (index -> pointer to a variant size function)
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////
