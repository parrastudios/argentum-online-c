/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	METADATA_REFERENCE_H
#define METADATA_REFERENCE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/Value.h>
#include <MetaData/Storage.h>
#include <MetaData/Scope.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
enum MetadataReferenceTypeInfo
{
	METADATA_REFERENCE_TYPE_VALUE		= 0x00,
	METADATA_REFERENCE_TYPE_REFERENCE	= 0x01,
	METADATA_REFERENCE_TYPE_OBJECT		= 0x02,
	METADATA_REFERENCE_TYPE_FUNCTION	= 0x03,

	METADATA_REFERENCE_TYPE_SIZE,

	METADATA_REFERENCE_TYPE_INVALID		= 0xFFFFFFFFUL
};

typedef MetadataStorageIndexType		MetadataReferenceType;
typedef char *							MetadataReferenceNameType;
typedef uinteger						MetadataReferenceTypeInfo;
typedef uinteger						MetadataReferenceCountType;
typedef struct MetadataReferenceType *	MetadataReferenceTypeImpl;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a reference
////////////////////////////////////////////////////////////
MetadataReferenceType MetadataReferenceCreate(MetadataScopeType Scope, MetadataReferenceTypeInfo Type, MetadataStorageIndexType Index);

////////////////////////////////////////////////////////////
/// Create a reference signature
////////////////////////////////////////////////////////////
MetadataReferenceType MetadataReferenceSignature(MetadataScopeType Scope, MetadataReferenceTypeInfo Type);

////////////////////////////////////////////////////////////
/// Copy a reference
////////////////////////////////////////////////////////////
MetadataReferenceType MetadataReferenceCopy(MetadataScopeType Scope, MetadataReferenceType Reference);

////////////////////////////////////////////////////////////
/// Set a reference to a resource
////////////////////////////////////////////////////////////
void MetadataReferenceSetPointer(MetadataScopeType Scope, MetadataReferenceType Reference, MetadataStorageIndexType Index);

////////////////////////////////////////////////////////////
/// Get reference to a resource
////////////////////////////////////////////////////////////
MetadataStorageIndexType MetadataReferenceGetPointer(MetadataScopeType Scope, MetadataReferenceType Reference);

////////////////////////////////////////////////////////////
/// Get the type of pointer (type-safety)
////////////////////////////////////////////////////////////
MetadataReferenceTypeInfo MetadataReferenceTypeOf(MetadataScopeType Scope, MetadataReferenceType Reference);

////////////////////////////////////////////////////////////
/// Reference a reference
////////////////////////////////////////////////////////////
void MetadataReferenceReference(MetadataScopeType Scope, MetadataReferenceType Reference);

////////////////////////////////////////////////////////////
/// Check if reference is referenced
////////////////////////////////////////////////////////////
bool MetadataReferenceIsReferenced(MetadataScopeType Scope, MetadataReferenceType Reference);

////////////////////////////////////////////////////////////
/// Dereference a reference
////////////////////////////////////////////////////////////
void MetadataReferenceDereference(MetadataScopeType Scope, MetadataReferenceType Reference);

////////////////////////////////////////////////////////////
/// Destroy a reference
////////////////////////////////////////////////////////////
void MetadataReferenceDestroy(MetadataScopeType Scope, MetadataReferenceType Reference);

#endif // METADATA_REFERENCE_H
