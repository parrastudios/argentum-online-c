/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef AUDIO_OPENAL_HELPER_H
#define AUDIO_OPENAL_HELPER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

#if PLATFORM_TYPE == PLATFORM_MACOS
    #include <OpenAL/al.h>
    #include <OpenAL/alc.h>
#else
    #include <AL/al.h>
    #include <AL/alc.h>
#endif

////////////////////////////////////////////////////////////
/// Let's define a macro to quickly check every OpenAL
/// API calls
////////////////////////////////////////////////////////////
#if COMPILE_TYPE == DEBUG_MODE

    // If in debug mode, perform a test on every call
    #define ALCheck(Func) ((Func), ALCheckError(__FILE__, __LINE__))

#else

    // Else, we don't add any overhead
    #define ALCheck(Func) (Func)

#endif

////////////////////////////////////////////////////////////
/// Check the last OpenAL error
///
/// \param File : Source file where the call is located
/// \param Line : Line number of the source file where the call is located
///
////////////////////////////////////////////////////////////
void ALCheckError(char * File, uint32 Line);

////////////////////////////////////////////////////////////
/// Set up the audio device
///
/// \return Nothing
////////////////////////////////////////////////////////////
void AudioInitializeDevice();

////////////////////////////////////////////////////////////
/// Destroy the audio device
///
/// \return Nothing
////////////////////////////////////////////////////////////
void AudioDestroyDevice();

////////////////////////////////////////////////////////////
/// Check OpenAL extensions
///
/// \param Extension : The extension that is going to check
///
/// \return true If the extension is supported
////////////////////////////////////////////////////////////
bool AudioIsExtensionSupported(char * Extension);

////////////////////////////////////////////////////////////
/// Get format from chanels count
///
/// \param ChanelsCount : Number of chanels
///
/// \return Format depending of the number of chanels
////////////////////////////////////////////////////////////
int AudioGetFormatFromChannelsCount(uint32 ChannelsCount);

////////////////////////////////////////////////////////////
/// Check if audio device is available
///
////////////////////////////////////////////////////////////
bool AudioIsAvailable();

////////////////////////////////////////////////////////////
/// Print audio device and OpenAL features
///
////////////////////////////////////////////////////////////
void OpenALPrintFeatures();

#endif // AUDIO_OPENAL_HELPER_H
