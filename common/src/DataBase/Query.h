/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATABASE_QUERY_H
#define DATABASE_QUERY_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/Connector.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define QUERY_FIELD_TYPE_STR		0x00
#define QUERY_FIELD_TYPE_INT32		0x01
#define QUERY_FIELD_TYPE_UINT32		0x02
#define QUERY_FIELD_TYPE_BOOL		0x03
#define QUERY_FIELD_TYPE_FLOAT		0x04

#define QUERY_STRING_LENGTH			UINTEGER_SUFFIX(0x03FF)

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef void * QueryResultType;
typedef void * QueryRowType;
typedef void * QueryFieldType;

/////////////////////////////////////////////////////////////
/// Makes a query to the database
/////////////////////////////////////////////////////////////
bool Query(QueryResultType * Result, char * Body, ...);

/////////////////////////////////////////////////////////////
/// Get the next result of a query and releases the previous
///	in case of multiple queries
/////////////////////////////////////////////////////////////
QueryResultType QueryGetNextResult(QueryResultType Result);

/////////////////////////////////////////////////////////////
/// Get the number of rows of the last query result
/////////////////////////////////////////////////////////////
uinteger QueryRowCount(QueryResultType Result);

/////////////////////////////////////////////////////////////
/// Get a row from the last query result
/////////////////////////////////////////////////////////////
QueryRowType QueryGetRow(QueryResultType Result);

// todo: create a function to fetch multiple rows in a single call

/////////////////////////////////////////////////////////////
/// Get the number of colums of the last query result
/////////////////////////////////////////////////////////////
uinteger QueryFieldCount(QueryResultType Result);

/////////////////////////////////////////////////////////////
/// Get the number of colums of the last query result
/////////////////////////////////////////////////////////////
void QueryGetField(QueryRowType Row, uinteger Field, uinteger Type, QueryFieldType Dest);

/////////////////////////////////////////////////////////////
/// Release a query
/////////////////////////////////////////////////////////////
void QueryRelease(QueryResultType Result);

#endif // DATABASE_QUERY_H
