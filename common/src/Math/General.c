/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Assembly.h>
#include <Math/General.h>
#include <System/CPUInfo.h>

////////////////////////////////////////////////////////////
/// Standard undefinitons to initialize the library
////////////////////////////////////////////////////////////
#undef cos
#undef sin
#undef sqrt

////////////////////////////////////////////////////////////
/// Standard math header
////////////////////////////////////////////////////////////
#include <math.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define ANGLE_CIRCLE_SIZE			512
#define ANGLE_CIRCLE_HALF_SIZE		(ANGLE_CIRCLE_SIZE / 2)
#define ANGLE_CIRCLE_QUARTER_SIZE	(ANGLE_CIRCLE_SIZE / 4)
#define ANGLE_CIRCLE_MASK			(ANGLE_CIRCLE_SIZE - 1)

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
float SinCosAngleTable[ANGLE_CIRCLE_SIZE]; ///< Fast sine and cosine table

////////////////////////////////////////////////////////////
/// Square root SSE function
////////////////////////////////////////////////////////////
float MathSqrtSSE(float Value)
{
#   if (ASM_SYNTAX_TYPE == ASM_SYNTAX_INTEL)
    __asm
    {
        movss xmm0, Value
        rsqrtss xmm0, xmm0
        rcpss xmm0, xmm0
        movss Value, xmm0
    }
#   elif (ASM_SYNTAX_TYPE == ASM_SYNTAX_ATANDT)
    union
    {
        int32   IntegerValue;
        float32 FloatValue;
    } Data;

    Data.FloatValue = Value;

    Data.IntegerValue = (1 << 29) + (Data.IntegerValue >> 1) - (1 << 22);

    // Two Babylonian Steps
    Data.FloatValue += Value / Data.FloatValue;

    Data.FloatValue = 0.25f * Data.FloatValue + Value / Data.FloatValue;

    return Data.FloatValue;

#   endif

    return Value;
}

////////////////////////////////////////////////////////////
/// Converts a float to int (by NVidia web site)
////////////////////////////////////////////////////////////
void ftoi(int * intptr, float f)
{
#   if (ASM_SYNTAX_TYPE == ASM_SYNTAX_INTEL)
    __asm
    {
		fld f
		mov edx, intptr
		FRNDINT
		fistp dword ptr [edx];
	}
#   elif (ASM_SYNTAX_TYPE == ASM_SYNTAX_ATANDT)
    *intptr = lrintf(f);
#   endif
}

////////////////////////////////////////////////////////////
/// Initialize the library
////////////////////////////////////////////////////////////
void MathInitialize()
{
	uint32 i;

	if (CPUFeatureCheck(FEATURE_SSEEXT))
	{
		MathSqrt = &MathSqrtSSE;
	}
	else
	{
		MathSqrt = (float(*)(float))&sqrt;
	}

	// Create de sine and cosine table
	for (i = 0; i < ANGLE_CIRCLE_SIZE; i++)
	{
		SinCosAngleTable[i] = (float)sin((float)i * MATH_PI / ANGLE_CIRCLE_HALF_SIZE);
	}

}

////////////////////////////////////////////////////////////
/// Sine fast function
////////////////////////////////////////////////////////////
float MathSin(float Radians)
{
	int Value;

	ftoi(&Value, Radians * ANGLE_CIRCLE_HALF_SIZE / MATH_PI);

	if (Value < 0)
	{
		return SinCosAngleTable[(-((-Value) & ANGLE_CIRCLE_MASK)) + ANGLE_CIRCLE_SIZE];
	}
	else
	{
		return SinCosAngleTable[Value & ANGLE_CIRCLE_MASK];
	}
}

////////////////////////////////////////////////////////////
/// Cosine fast function
////////////////////////////////////////////////////////////
float MathCos(float Radians)
{
	int Value;

	ftoi(&Value, Radians * ANGLE_CIRCLE_HALF_SIZE / MATH_PI);

	if (Value < 0)
	{
		return SinCosAngleTable[((-Value) + ANGLE_CIRCLE_QUARTER_SIZE) & ANGLE_CIRCLE_MASK];
	}
	else
	{
		return SinCosAngleTable[(Value + ANGLE_CIRCLE_QUARTER_SIZE) & ANGLE_CIRCLE_MASK];
	}
}

////////////////////////////////////////////////////////////
/// Calculate the next power of two
////////////////////////////////////////////////////////////
uinteger MathPowerOfTwo(uinteger Value)
{
	register uinteger Result;

	for (Result = Value & (~Value + 1); Result < Value; Result <<= 1);

	return Result;
}

////////////////////////////////////////////////////////////
/// Determines if two floating-point are close enough
////////////////////////////////////////////////////////////
bool MathCloseEnough(float f1, float f2)
{
	return (bool)(fabsf((f1 - f2) / ((f2 == 0.0f) ? 1.0f : f2)) < MATH_EPSILON);
}

////////////////////////////////////////////////////////////
/// Returns a gradual transition of x ranging [0 1]
////////////////////////////////////////////////////////////
float MathSmoothStep(float a, float b, float x)
{
	if (x < a)
	{
		return 0.0f;
	}
	else if (x >= b)
	{
		return 1.0f;
	}
	else
	{
		x = (x - a) / (b - a);
		return sqr(x) * (3.0f - 2.0f * x);
	}
}

////////////////////////////////////////////////////////////
/// Determines if a number is in the range [Min Max]
////////////////////////////////////////////////////////////
bool MathCheckBoundsi(int32 * Value, int32 Min, int32 Max)
{
	bool IsInBounds = true;

	if (*Value < Min)
	{
		*Value = Min;
		IsInBounds = false;
	}

	if (*Value > Max)
	{
		*Value = Max;
		IsInBounds = false;
	}

	return IsInBounds;
}

////////////////////////////////////////////////////////////
/// Determines if a number is in the float range [Min Max]
////////////////////////////////////////////////////////////
bool MathCheckBoundsf(float * Value, float Min, float Max)
{
	bool IsInBounds = true;

	if (*Value < Min)
	{
		*Value = Min;
		IsInBounds = false;
	}

	if (*Value > Max)
	{
		*Value = Max;
		IsInBounds = false;
	}

	return IsInBounds;
}

////////////////////////////////////////////////////////////
/// Discover the orientation of a triangle's points
/// "Programming Principles in Computer Graphics", L. Ammeraal (Wiley)
////////////////////////////////////////////////////////////
int32 MathTriangleOrientation(int32 pX, int32 pY, int32 qX, int32 qY, int32 rX, int32 rY)
{
	int32 aX, aY, bX, bY;
	float d;

	aX = qX - pX;
	aY = qY - pY;

	bX = rX - pX;
	bY = rY - pY;

	d = (float)aX * (float)bY - (float)aY * (float)bX;
	return (d < 0) ? false : (bool)(d > 0);
}
