/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <GUI/WindowObj.h>
#include <Graphics/TextureRender.h>
#include <Graphics/Font.h>
#include <Audio/SoundManager.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Internal data
////////////////////////////////////////////////////////////
static struct Color4ub ColorDisabled	= { 127, 127, 127, 255 };
static struct Color4ub ColorNormal		= { 255, 255, 255, 255 };
static struct Color4ub ColorBlack		= { 0, 0, 0, 255 };

////////////////////////////////////////////////////////////
// Add widget to window
////////////////////////////////////////////////////////////
void GuiWindowAddWidget(struct GuiWindow * Window, struct GuiWidget * Widget)
{
	if (Window->First == NULL)
	{
		Window->FocusWidget = Window->First = Widget;
		Widget->Previous = NULL;
	}
	else
	{
		Window->Last->Next = Widget;
		Widget->Previous = Window->Last;
	}

	Window->Last = Widget;
	Widget->Next = NULL;
}

////////////////////////////////////////////////////////////
/// Remove widget to window
////////////////////////////////////////////////////////////
void GuiWindowRemoveWidget(struct GuiWindow * Window, struct GuiWidget * Widget)
{
	if (Widget->Previous == NULL)
		Window->First = Widget->Next;
	else
		Widget->Previous->Next = Widget->Next;

	if (Widget->Next == NULL)
		Window->Last = Widget->Previous;
	else
		Widget->Next->Previous = Widget->Previous;

	GuiWidgetDestroy(Widget);
}

////////////////////////////////////////////////////////////
/// Create the window
////////////////////////////////////////////////////////////
struct GuiWindow * GuiWindowCreate()
{
	// Set status and pointers
	struct GuiWindow * Window = (struct GuiWindow *)MemoryAllocate(sizeof(struct GuiWindow));

	if (Window)
	{
		Window->hStatus		= GUI_WINDOW_NOTLOADED;
		Window->First		= NULL;
		Window->Last		= NULL;
		Window->FocusWidget	= NULL;

		return Window;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy the window
////////////////////////////////////////////////////////////
void GuiWindowDestroy(struct GuiWindow * Window)
{
	if (Window)
	{
		struct GuiWidget * Iterator = Window->First;

		while (Iterator != NULL)
		{
			struct GuiWidget * Next = Iterator->Next;

			// Destroy it
			GuiWidgetDestroy(Iterator);

			// Go to the next
			Iterator = Next;
		}

		// Set focused to null
		Window->FocusWidget = NULL;

		// Set status
		MemoryDeallocate(Window);
	}
}

////////////////////////////////////////////////////////////
/// Render a form
////////////////////////////////////////////////////////////
void GuiWindowRender(struct GuiWindow * Window, bool Focused)
{
	struct GuiWidget * Iterator;
	struct Color4ub Color;

	for (Iterator = Window->First; Iterator != NULL; Iterator = Iterator->Next)
	{
		if (Iterator->Visible)
		{
			// Update color
			if (!Iterator->Enabled)
			{
				MemoryCopy(&Color, &ColorDisabled, sizeof(struct Color4ub));
			}
			else
			{
				MemoryCopy(&Color, &ColorNormal, sizeof(struct Color4ub));
			}

			if (Focused)
			{
				Color.a = 255;
			}
			else
			{
				Color.a = 200;
			}

			// Render widget
			switch (Iterator->Type)
			{
				case GUI_WIDGET_FORM :
					// Render the background repeating the texture
					GuiFormRender(Iterator, &Color, &ColorBlack);
					break;

				case GUI_WIDGET_BUTTON :
					// Render the texture, render the text centred with color
					GuiButtonRender(Window->First, Iterator, &Color, &ColorBlack);
					break;

				case GUI_WIDGET_TEXTBOX :
					// Render text
					GuiTextBoxRender(Window->First, Iterator, &Color, &ColorBlack, (Iterator == Window->FocusWidget));
					break;

				case GUI_WIDGET_CONSOLE :
					// Render
					GuiConsoleRender(Window->First, Iterator, &Color, &ColorBlack);
					break;

				case GUI_WIDGET_SCROLL :
					// Render

					break;

				case GUI_WIDGET_LABEL :
					// Render label
					GuiLabelRender(Window->First, Iterator, &Color, &ColorBlack);
					break;

				case GUI_WIDGET_LIST :
					// Render the list
					GuiListRender(Window->First, Iterator, &Color, &ColorBlack);
					break;

				case GUI_WIDGET_SHAPE :
					// Render shape
					GuiShapeRender(Window->First, Iterator, &Color, &ColorBlack);
					break;

				case GUI_WIDGET_TABLE :
					// Render the list
					GuiTableRender(Window->First, Iterator, &Color, &ColorBlack);
					break;

				default : break;
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Return the state
////////////////////////////////////////////////////////////
uint32 GuiWindowStatus(struct GuiWindow * Window)
{
	if (!Window)
		return GUI_WINDOW_NOTLOADED;

	return Window->hStatus;
}

////////////////////////////////////////////////////////////
/// Get the specified widget by name
////////////////////////////////////////////////////////////
struct GuiWidget * GuiWindowGetWidget(struct GuiWindow * Window, char * Name)
{
    if (Window && Name)
    {
        struct GuiWidget * Iterator;

        // Iterate through widget list starting by the second one
        for (Iterator = Window->First->Next; Iterator != NULL; Iterator = Iterator->Next)
        {
            if (strcmp(Iterator->Name, Name) == 0)
            {
                return Iterator;
            }
        }
    }
	return NULL;
}

////////////////////////////////////////////////////////////
/// Show a widget of a window (assumed that belongs to it)
////////////////////////////////////////////////////////////
void GuiWindowShowWidget(struct GuiWindow * Window,  struct GuiWidget * Widget, bool Show)
{
	Widget->Visible = Show;

	if (Widget->Visible)
	{
		Window->FocusWidget = Widget;
	}
	else
	{
		Window->FocusWidget = Window->First;
	}
}
