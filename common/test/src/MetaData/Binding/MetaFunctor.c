/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaFunctor.h>
#include <Functor.h>
#include <MetaData/General.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Auto-generated stub for float-int function
////////////////////////////////////////////////////////////
void MetadataFloatIntFunctionStub(MetadataParameterListType ParameterList)
{
	// Get metadata manager instance
	MetadataManagerType MetaManager = MetadataManagerGlobalInstance();

	// Retreive first argument
	MetadataValueType FirstValue = MetaManager->ParameterRetrieve(ParameterList, 0);

	// Retreive second argument
	MetadataValueType SecondValue = MetaManager->ParameterRetrieve(ParameterList, 1);

	// Retreive third argument
	MetadataValueType ThirdValue = MetaManager->ParameterRetrieve(ParameterList, 2);

	// Call real function
	VoidFuncWithArgsFloatFloatIntArgs(
		MetaManager->ValueGetFloat(FirstValue),
		MetaManager->ValueGetFloat(SecondValue),
		MetaManager->ValueGetUInt32(ThirdValue));
}

////////////////////////////////////////////////////////////
/// Register float array into metadata system
////////////////////////////////////////////////////////////
void MetaFunctorRegister()
{
	// Get metadata manager instance
	MetadataManagerType MetaManager = MetadataManagerGlobalInstance();

	// Create a signature
	MetadataParameterSignatureListDecl(FloatIntFuncSignature, MetaManager->ParameterSignature);

	MetadataFunctionType FloatIntFunction = METADATA_STORAGE_INDEX_INVALID;

	// Define function signature
	FloatIntFuncSignature[0].
		Build(&FloatIntFuncSignature[0], METADATA_PARAMETER_TYPE_BY_VALUE, METADATA_STORAGE_RESOURCE_VALUE, MetaManager->ValueSignature("first_float", FLOAT, 1))->
		Build(&FloatIntFuncSignature[1], METADATA_PARAMETER_TYPE_BY_VALUE, METADATA_STORAGE_RESOURCE_VALUE, MetaManager->ValueSignature("second_float", FLOAT, 1))->
		Build(&FloatIntFuncSignature[2], METADATA_PARAMETER_TYPE_BY_VALUE, METADATA_STORAGE_RESOURCE_VALUE, MetaManager->ValueSignature("third_uint", UINT32, 1));

	// Create signature on the system and pass it to the function
	FloatIntFunction = MetaManager->FunctionBind("float_int_function", MetaManager->ParameterListSignature(3, FloatIntFuncSignature), &MetadataFloatIntFunctionStub);

/*
	// Create a void parameter list without return
	MetadataParameterListType ParameterList = MetaManager->ParameterListCreate(("return", METADATA_PARAMETER_TYPE_NULL));

	// Bind float array into a meta value
	MetaManager->FunctionBind("my_void_func", ParameterList, (void *)&VoidFuncWithoutArgs);

	// Create a new parameter list, with integer return and arguments
	ParameterList = MetaManager->ParameterListCreate(("return", METADATA_PARAMETER_TYPE_VALUE), ("First", METADATA_PARAMETER_TYPE_VALUE), ("Second", METADATA_PARAMETER_TYPE_VALUE));

	// Create a meta float array at runtime
	MetaManager->FunctionBind("my_integer_func", ParameterList, (void *)&IntegerFuncWithArgs);
*/

/*
#define BuildSignatureImpl(FunctionSignature, ...) \
	(FunctionSignature)->Build((FunctionSignature), __VA_ARGS__)

#define BuildSignatureExpand(InfoType, ResourceType, ResourceSignature) \
	PREPROCESSOR_CONCAT(METADATA_PARAMETER_TYPE_, InfoType), \
	PREPROCESSOR_CONCAT(METADATA_STORAGE_RESOURCE_, ResourceType), \
	ResourceSignature

#define BuildSignature(Signature, Count, ...) \
	BuildSignatureImpl(&Signature[Count - 1], BuildSignatureExpand PREPROCESSOR_ARGS_FIRST(__VA_ARGS__))

#define BuildSignatureListExpand(InfoType, ResourceType, ResourceSignature) \
	BuildSignatureImpl(&Signature[Count - 1], \
	PREPROCESSOR_CONCAT(METADATA_PARAMETER_TYPE_, InfoType), \
	PREPROCESSOR_CONCAT(METADATA_STORAGE_RESOURCE_, ResourceType), \
	ResourceSignature

#define BuildSingatureList(Signature, Count, ...) \
	PREPROCESSOR_FOR_EACH(BuildSignatureListExpand PREPROCESSOR_ARGS_FIRST, __VA_ARGS__)

	BuildSingatureList(FloatIntFuncSignature, 3,
	(BY_VALUE, VALUE, MetaManager->ValueSignature("first_float", FLOAT, 1)),
	(BY_VALUE, VALUE, MetaManager->ValueSignature("second_float", FLOAT, 1)),
	(BY_VALUE, VALUE, MetaManager->ValueSignature("third_uint", UINT32, 1)));

	//

	BuildSignature(FloatIntFuncSignature, 1, (BY_VALUE, VALUE, MetaManager->ValueSignature("first_float", FLOAT, 1))); 
*/
}
