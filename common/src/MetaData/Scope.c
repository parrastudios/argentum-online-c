/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/Scope.h>
#include <DataType/HashMap.h>

#include <System/IOHelper.h> // todo: refactor

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define METADATA_SCOPE_NAME_LENGTH		0xFFUL
#define METADATA_SCOPE_GLOBAL_NAME		"metadata_scope_global"

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef char						MetadataScopeNameType[METADATA_SCOPE_NAME_LENGTH];

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef struct MetadataScopeNamingType
{

	MetadataScopeNameType		Name;			//< Key of the resource
	MetadataScopeType			Scope;			//< Scope which belongs to
	MetadataStorageResourceType	ResourceType;	//< Resource type (regarding to storage)
	MetadataStorageIndexType	Index;			//< Index of the resource (regarding to storage)

} MetadataScopeNamingType;

struct MetadataScopeType
{
	MetadataScopeNameType		Name;			//< Namespace
	MetadataStorageType			Storage;		//< Storage reference
	HashMap						Naming;			//< Hash map for name indexing (MetadataScopeNameType -> MetadataScopeNamingType)
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get global scope instance
////////////////////////////////////////////////////////////
MetadataScopeType MetadataScopeGlobalInstance()
{
	static MetadataScopeType GlobalScopeInstance = NULL;

	if (GlobalScopeInstance == NULL)
	{
		GlobalScopeInstance = MetadataScopeCreate(METADATA_SCOPE_GLOBAL_NAME, MetadataStorageGlobalInstance());
	}

	return GlobalScopeInstance;
}

////////////////////////////////////////////////////////////
/// Create a new scope
////////////////////////////////////////////////////////////
MetadataScopeType MetadataScopeCreate(MetadataScopeName Name, MetadataStorageType Storage)
{
	if (Storage && Name)
	{
		// Allocate memory for scope
		MetadataScopeType Scope = (MetadataScopeType)MemoryAllocate(sizeof(struct MetadataScopeType));

		if (Scope != NULL)
		{
			// Set scope name
			MetadataScopeSetName(Scope, Name);

			// Set scope storage
			MetadataScopeSetStorage(Scope, Storage);

			// Initialize scope naming map
			Scope->Naming = HashMapCreate(HashGetCallbackStringDefault(), CompareGetCallback(COMPARE_FUNC_CSTRING));
			
			// Initialize naming map
			if (Scope->Naming != NULL)
			{
				// Return scope pointer
				return Scope;
			}

			// Clear scope memory on set allocation error
			MemoryDeallocate(Scope);
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Retreive name asociated to a scope
////////////////////////////////////////////////////////////
MetadataScopeName MetadataScopeGetName(MetadataScopeType Scope)
{
	if (Scope)
	{
		return Scope->Name;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set name asociated to a scope
////////////////////////////////////////////////////////////
void MetadataScopeSetName(MetadataScopeType Scope, MetadataScopeName Name)
{
	if (Scope && Name)
	{
		strcpy(Scope->Name, Name);
	}
}

////////////////////////////////////////////////////////////
/// Retreive storage asociated to a scope
////////////////////////////////////////////////////////////
MetadataStorageType MetadataScopeGetStorage(MetadataScopeType Scope)
{
	if (Scope)
	{
		return Scope->Storage;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set storage asociated to a scope
////////////////////////////////////////////////////////////
void MetadataScopeSetStorage(MetadataScopeType Scope, MetadataStorageType Storage)
{
	if (Scope && Storage)
	{
		Scope->Storage = Storage;
	}
}

////////////////////////////////////////////////////////////
/// Create a keyword in the system and link it to storage
////////////////////////////////////////////////////////////
bool MetadataScopeKeywordCreate(MetadataScopeType Scope, MetadataScopeKey Keyword, MetadataStorageIndexType Index, MetadataStorageResourceType ResourceType)
{
	if (Scope && Keyword && Index != METADATA_STORAGE_INDEX_INVALID && ResourceType < METADATA_STORAGE_RESOURCE_SIZE)
	{
		MetadataScopeNamingType * NamingType = (MetadataScopeNamingType *)MemoryAllocate(sizeof(struct MetadataScopeNamingType));

		if (NamingType)
		{
			// Set naming bucket
			strcpy(NamingType->Name, Keyword);
			NamingType->Scope = Scope;
			NamingType->Index = Index;
			NamingType->ResourceType = ResourceType;

			// Insert into naming list
			if (HashMapInsert(Scope->Naming, (HashKeyData)NamingType->Name, (void *)NamingType))
			{
				return true;
			}
			else
			{
				// Clear naming bucket
				MemoryDeallocate(NamingType);
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Get storage index of the keyword
////////////////////////////////////////////////////////////
MetadataStorageIndexType MetadataScopeKeywordGetIndex(MetadataScopeType Scope, MetadataScopeKey Keyword)
{
	if (Scope && Keyword)
	{
		// Get keyword from naming list
		MetadataScopeNamingType * NamingType = (MetadataScopeNamingType *)HashMapGet(Scope->Naming, (HashKeyData)Keyword);

		if (NamingType)
		{
			// Return resource index
			return NamingType->Index;
		}
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Get storage type of the keyword
////////////////////////////////////////////////////////////
MetadataStorageResourceType MetadataScopeKeywordGetType(MetadataScopeType Scope, MetadataScopeKey Keyword)
{
	if (Scope && Keyword)
	{
		// Get keyword from naming list
		MetadataScopeNamingType * NamingType = (MetadataScopeNamingType *)HashMapGet(Scope->Naming, (HashKeyData)Keyword);

		if (NamingType)
		{
			// Return resource type
			return NamingType->ResourceType;
		}
	}

	return METADATA_STORAGE_RESOURCE_INVALID;
}

////////////////////////////////////////////////////////////
/// Get name of keyword by storage index and type
////////////////////////////////////////////////////////////
MetadataScopeKey MetadataScopeKeywordGetName(MetadataScopeType Scope, MetadataStorageIndexType Index, MetadataStorageResourceType Type)
{
	if (Scope && Index != METADATA_STORAGE_RESOURCE_INVALID && Type != METADATA_STORAGE_RESOURCE_INVALID)
	{
		HashMapIterator Iterator;

		// Iterate through map and remove all naming buckets
		if (HashMapIteratorBegin(&Iterator, Scope->Naming))
		{
			do
			{
				MetadataScopeNamingType * NamingType = (MetadataScopeNamingType *)HashMapIteratorData(&Iterator);

				if (NamingType)
				{
					if (NamingType->Index == NamingType->Index && NamingType->ResourceType == Type)
					{
						return NamingType->Name;
					}
				}
			} while (!HashMapIteratorEnd(&Iterator) && HashMapIteratorNext(&Iterator));
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a keyword in the system and link it to storage
////////////////////////////////////////////////////////////
void MetadataScopeKeywordDestroy(MetadataScopeType Scope, MetadataScopeKey Keyword)
{
	if (Scope && Keyword)
	{
		// Get keyword from naming list
		MetadataScopeNamingType * NamingType = (MetadataScopeNamingType *)HashMapGet(Scope->Naming, (HashKeyData)Keyword);

		if (NamingType && HashMapRemove(Scope->Naming, (HashKeyData)Keyword))
		{
			// Remove naming type
			MemoryDeallocate(NamingType);
		}
	}
}

////////////////////////////////////////////////////////////
/// Destroy a scope
////////////////////////////////////////////////////////////
void MetadataScopeDestroy(MetadataScopeType Scope)
{
	if (Scope)
	{
		HashMapIterator Iterator;

		// Iterate through map and remove all naming buckets
		if (HashMapIteratorBegin(&Iterator, Scope->Naming))
		{
			do
			{
				MetadataScopeNamingType * NamingType = (MetadataScopeNamingType *)HashMapIteratorData(&Iterator);

				if (NamingType)
				{
					// Clear naming bucket memory
					MemoryDeallocate(NamingType);
				}
			} while (!HashMapIteratorEnd(&Iterator) && HashMapIteratorNext(&Iterator));
		}

		// Clear naming map
		HashMapDestroy(Scope->Naming);

		// Clear scope memory
		MemoryDeallocate(Scope);
	}
}
