
IqeBrowser   V2.10 June 7 2015
------------------------------

IqeBrowser is a program to browse IQE, IQM (Inter-Quake Model format) and
many other 3D models. Any 3D model can be saved as IQM (a binary format)
or IQE (a text format) file.
Some model processing tools are integrated to resize, shift, rotate,
reskin ... the model and also to create/ madify skeletons or animations.
IqeBrowser is specially tailored for the Ryzom Assets (see below).

IqeBrowser is ready to use for a windows system.
Source is included.

IqeBrowser is created by Robert Reinhard (see
http://www.moddb.com/mods/r-reinhard).
Please send feedback or feature requests to me: robert@ya3dag.de

Features
--------

  * Browse through the file system and shows IQE and IQM files as 3D models.
    Open Asset Import Library (short name: Assimp) used to view more
    then 40 other 3d model formats.
    see: http://assimp.sourceforge.net/main_features_formats.html
  * Each viewed model can be exported as IQM (a binary format) or
    IQE (a text format) file.
  * Tools to
    * Resize, shift or rotate models.
    * Reskin models
    * Merge multiple models/animations to one model
    * Skeleton modification, creation, load, save
    * Animation modification, creation
      * Timline editor with keyflags
      * Plots of position, angles or sizes
      * Kinect V1 support with realtime retarget
        and recording of animations
  * The settings of checkboxes, values, ...
    are remembered.
  * Positions and size of main and tools windows are remembered and reused
    on next oben.

Ryzom Assets
------------

All these assets were created for the commercial MMORPG Ryzom.
They are all free to use under Creative Commons License Attribution-Share
Alike 3.0 license.
See media.ryzom.com for the original source.

The files in this repository have been converted from the original 3ds Max
files using various scripts and some manual labor by Tor Andersson.

Get it from: https://bitbucket.org/ccxvii/ryzom-assets/overview

How to use
----------

Tip: Link the IQE/IQM file extension with IqeBrowser.
     Using the explorer application, double-clicking an IQE/IQM file the
     IqeBrowser starts up with this file.

Tip: Undo
     IqeBrowser has no undo feature.
     * Use copy/paste (Ctrl+C, Ctrl+V) to save / restore a model
       from / to the 'model view'.
     * Use the workbench to hold multiple copies of models.

Revision History
----------------

V1.00 2014-04-12  First release.
V1.10 2014-05-02  * Added IQM support.
                  * For animation speed use the framerate from the model data.
                    Also display the framerate.
                  * Added display of joint names.
                  * Display size of model.
                  * Added scale for output mesh.
                  * Model container holds up to 64 models (was 16 before).
                  * 'Add' model to model container
                    * If the added model is not the first model in the model container and
                      the added model has an animation,
                      copy skeleton and/or mesh of the first model to the added model
                      if the joint and poses count match.
                      ==> Pure animation files are displayed with skeleton and/or mesh.
                    * display the added model.
                    ==> Show animations with sekelton and/or mesh.

V2.00 2015-03-01  * Reworked GUI
                    Switched to FLTK (Fast Light Toolkit) for GUI.
                    Reworked main window. Replaced cryptic keyboard inputs
                    by checkboxes, buttons, ...
                    Additional tool windows are opened if needed.
                  * More than 40 3d model formats can be viewed.

V2.10 2015-06-07  * 4 or 1 view(s) of the model.
                  * Skeleton modification/creation
                  * Animation modification/creation
                  * Kinect V1 support with live retarget and
                    animation recording
                  * Manual

ToDo
----
an unordered wishlist for future expansions:

* 'Comment Data' in model
  Own dialog to show/modify/add it.
* Managing grips (or tags)
  Grips are used to attach weapons, helmets or other accessories to the model.
* Model Info gets its own dialog.
* A possibility to display additional models. Is good for size comparisons of
  models.
* Write file to same directory as shown by filebrowser
  --> Refresh the filebrowser
* Tools/Mesh/Parts
  * Button 'Remove'     --> Remove selected mesh part
  * Button 'Clear mesh' --> Removes all mesh data so we have an empty model.
                            Skeleton and animations are not cleared.
  * Button 'Create'     --> Creats an empty model, no mesh, no skeleton, no
                            animations

Credits
-------

* Ryzom Assets from the commercial MMORPG Ryzom.
  http://media.ryzom.com/

* Ryzom Assets converted to IQE model format by Tor Andersson.
  https://bitbucket.org/ccxvii/ryzom-assets/overview

* IQM Developer Kit from Lee Salzman,
  The iqm.cpp file is used as by IeqBrowser.
  https://github.com/lsalzman/iqm

* 3d Asset Tools from Tor Andersson.
  Some sourced are used by IqeBrowser.
  https://github.com/ccxvii/asstools

* Open Asset Import Library (short name: Assimp) is a portable Open Source library
  to import various well-known 3D model formats in a uniform manner.
  http://assimp.sourceforge.net/index.html

* 3d Asset Tools
  A set of tools for manipulating 3d assets in IQE/IQM format.
  The interface to 'Open Asset Import Library' was taken from here.
  https://github.com/ccxvii/asstools

* FLTK, Fast Light Toolkit
  FLTK is a cross-platform C++ GUI toolkit for UNIX�/Linux� (X11),
  Microsoft� Windows�, and MacOS� X.
  FLTK provides modern GUI functionality without the bloat and supports
  3D graphics via OpenGL� and its built-in GLUT emulation.
  http://www.fltk.org/index.php

* Pinocchio library for automatic character rigging.
  http://www.mit.edu/~ibaran/autorig/
  http://www.mit.edu/~ibaran/autorig/pinocchio.html

* VCG Library
  The Visualization and Computer Graphics Library (VCG for short) is a open
  source portable C++ templated library for manipulation, processing and
  displaying with OpenGL of triangle and tetrahedral meshes.
  http://vcg.isti.cnr.it/vcglib/

* Icon for IqeBrowser
  The background is made by Robert, www.ya3dag.de.
  The human shape was created by Lorc, http://game-icons.net/

Compilation
-----------

IqeBrowser was compiled with codebloccks 13.12 and MinGW compiler.
Compilation was done for Windows only.
Workspaces and project files for codeblocks are included.

License Info
------------

The licenses of all included compenents like '3d Asset Tools', 'IQM Developer Kit',
'FLTK', 'Open Asset Import Library', ... are still valid.
The IqeBrowser part is licensed as public domain.



