/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATABASE_MYSQL_GENERAL_H
#define DATABASE_MYSQL_GENERAL_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

#ifdef inline
#	undef inline
#endif

#ifdef __func__
#	undef __func__
#endif

#ifndef HAVE_INT8
#   define HAVE_INT8
#endif

#ifndef HAVE_UINT8
#   define HAVE_UINT8
#endif

#ifndef HAVE_INT16
#   define HAVE_INT16
#endif

#ifndef HAVE_UINT16
#   define HAVE_UINT16
#endif

#ifndef HAVE_INT32
#   define HAVE_INT32
#endif

#ifndef HAVE_UINT32
#   define HAVE_UINT32
#endif

#ifndef HAVE_INT64
#   define HAVE_INT64
#endif

#ifndef HAVE_UINT64
#   define HAVE_UINT64
#endif

#ifndef HAVE_INT_8_16_32
#   define HAVE_INT_8_16_32
#endif

//#include <my_global.h>		///< MySQL includes
#include <mysql.h>

#endif // DATABASE_MYSQL_GENERAL_H
