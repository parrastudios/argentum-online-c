/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_MUTEX_H
#define SYSTEM_MUTEX_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Platform.h>
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
/// Mutex defines a mutex (MUTual EXclusion) object,
/// that allows a thread to lock critical instructions
/// to avoid simultaneous access with other threads.
/// The Win32 version uses critical sections, as it is
/// faster than mutexes.
////////////////////////////////////////////////////////////

#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
#	define WIN32_LEAN_AND_MEAN
#	include <windows.h>

	typedef CRITICAL_SECTION Mutex;
#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_MACOS || PLATFORM_TYPE == PLATFORM_BSD
#	include <pthread.h>

#	ifdef THREADING_USE_MUTEX
		typedef pthread_mutex_t Mutex;
#		define MUTEX_INITIALIZE PTHREAD_MUTEX_INITIALIZER
#	else
		typedef pthread_spinlock_t Mutex;
#		define MUTEX_INITIALIZE PTHREAD_SPINLOCK_INITIALIZER
#	endif
#elif PLATFORM_TYPE == PLATFORM_MACOS
	typedef OSSpinLock Mutex;
#endif

////////////////////////////////////////////////////////////
/// Mutex initializer
////////////////////////////////////////////////////////////
void MutexInitialize(Mutex * MutexEx);

////////////////////////////////////////////////////////////
/// Mutex destructor
////////////////////////////////////////////////////////////
void MutexDestroy(Mutex * MutexEx);

////////////////////////////////////////////////////////////
/// Lock the mutex
///
////////////////////////////////////////////////////////////
void MutexLock(Mutex * MutexEx);

////////////////////////////////////////////////////////////
/// Unlock the mutex
///
////////////////////////////////////////////////////////////
void MutexUnlock(Mutex * MutexEx);

#endif // SYSTEM_MUTEX_H
