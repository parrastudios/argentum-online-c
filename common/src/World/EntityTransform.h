/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_ENTITY_TRANSFORM_H
#define WORLD_ENTITY_TRANSFORM_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Matrix.h>
#include <Math/Geometry/Vector.h>
#include <Math/Geometry/Quaternion.h>

#include <World/Component.h>

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct EntityType;

////////////////////////////////////////////////////////////
// Enumerations
////////////////////////////////////////////////////////////
enum ComponentTransformMovement
{
	COMPONENT_TRANSFORM_MOVEMENT_RELATIVE = 0x00,
	COMPONENT_TRANSFORM_MOVEMENT_ABSOLUTE = 0x01,

	COMPONENT_TRANSFORM_MOVEMENT_SIZE
};

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Transform component base data
////////////////////////////////////////////////////////////
struct ComponentTransformData
{
	struct Vector3f					Position;
	struct Vector3f					Heading;
	struct Quaternion				Rotation;
	struct Quaternion				RotationConstraint;
	struct Quaternion				Orientation;
	struct Quaternion				OrientationConstraint;
	struct Vector3f					Scale;

	struct Vector3f					Right;
	struct Vector3f					Up;
	struct Vector3f					Forward;

	struct Vector3f					EulerOrient;
	struct Vector3f					EulerRotate;

	bool							ConstrainedToWorldYAxis;
	enum ComponentTransformMovement	Movement;

	struct Matrix4f					WorldMatrix;
};


////////////////////////////////////////////////////////////
/// Transform base component type
////////////////////////////////////////////////////////////
struct ComponentTransformType
{
	//< Update transform component data of an entity
	void (*Update)(struct EntityType * Entity, float ElapsedTime);

	//< Change position of a component
	void (*Move)(struct EntityType * Entity, struct Vector3f * Position);

	//< Change scale of a component
	void (*Scale)(struct EntityType * Entity, struct Vector3f * Scale);

	//< Change orientation of a component (heading, pitch, roll)
	void (*Orient)(struct EntityType * Entity, struct Vector3f * Orientation);

	//< Change rotation of a component (heading, pitch, roll)
	void (*Rotate)(struct EntityType * Entity, struct Vector3f * Rotation);

	//< Modify world matrix of a component
	void (*Matrix)(struct EntityType * Entity, struct Matrix4f * WorldMatrix);
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a transform component
////////////////////////////////////////////////////////////
struct EntityType * EntityTransformCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a transform component of an entity
////////////////////////////////////////////////////////////
void EntityTransformDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Return the entity transform implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentTransformType * EntityComponentTransformAttr();

////////////////////////////////////////////////////////////
/// Return the entity transform implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentTransformImpl();

#endif // WORLD_ENTITY_TRANSFORM_H
