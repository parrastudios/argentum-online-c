/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATABASE_CHARACTER_H
#define DATABASE_CHARACTER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/Player.h>
#include <DataBase/Npc.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define	CHARACTER_TABLE_DEFAULT		"character"

#define DB_PLAYER_OBJECT_TYPE		0x00
#define DB_NPC_OBJECT_TYPE			0x01

////////////////////////////////////////////////////////////
// Member Data
////////////////////////////////////////////////////////////
struct DBCharacterData
{
	uint32						Id;
	uint32						Algin;

	struct DBCharacterStatus
	{
		uint32	Level;
		uint32	Experience;
		uint32	Head;
		uint32	Body;
		uint32	MaxHp;
		uint32	MinHp;
		uint32	MaxMana;
		uint32	MinMana;
		uint32	MaxHit;
		uint32	MinHit;
		uint32	MaxCombatDefense;
		uint32	MinCombatDefense;
		uint32	MaxMagicDefense;
		uint32	MinMagicDefense;
		bool	Poisoned;
		bool	Paralized;
		bool	Magic; // todo: Remove, can be derived from (MaxMana == 0)
	}							Status;

	struct DBCharacterEquipedItems
	{
		uint32	Helmet;
		uint32	Armor;
		uint32	Weapon;
		uint32	Shield;
		uint32	Footwear;
	}							EquipedItems;

	union
	{
		struct DBPlayerData		Player;
		struct DBNpcData		Npc;
	} Attributes;
};

////////////////////////////////////////////////////////////
/// Initialize character database object
////////////////////////////////////////////////////////////
#define DBCharacterInitialize(DBCharacter) MemorySet(DBCharacter, 0, sizeof(struct DBCharacterData))

////////////////////////////////////////////////////////////
/// Load the character from database
////////////////////////////////////////////////////////////
bool DBCharacterGetDataById(struct DBCharacterData * Character, uint32 Index);

////////////////////////////////////////////////////////////
/// Disconnect the character (store it in database)
////////////////////////////////////////////////////////////
bool DBCharacterStoreData(struct DBCharacterData * Character);

////////////////////////////////////////////////////////////
/// Check if a character exist in the database
////////////////////////////////////////////////////////////
bool DBCharacterExist(struct DBCharacterData * Character);

////////////////////////////////////////////////////////////
/// Create a character in the database
////////////////////////////////////////////////////////////
bool DBCharacterCreate(struct DBCharacterData * Character);

////////////////////////////////////////////////////////////
/// Delete a character from the database
////////////////////////////////////////////////////////////
bool DBCharacterDelete(struct DBCharacterData * Character);

#endif // DATABASE_CHARACTER_H
