/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	WORLD_TERRAIN_FACTORY_H
#define WORLD_TERRAIN_FACTORY_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/EntityFactory.h>

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_BASE)
#	include <World/TerrainBase.h>
#endif

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_TRANSFORM)
#	include <World/TerrainTransform.h>
#endif

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_COLLISION)
#	include <World/TerrainCollision.h>
#endif

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_PHYSICS)
#	include <World/TerrainPhysics.h>
#endif

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_RENDER)
#	include <World/TerrainRender.h>
#endif

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_OCCLUSION)
#	include <World/TerrainOcclusion.h>
#endif

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_SOUND)
#	include <World/TerrainSound.h>
#endif

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize component pool of terrain factory
////////////////////////////////////////////////////////////
bool TerrainFactoryInitialize();

////////////////////////////////////////////////////////////
/// Get terrain factory instance
////////////////////////////////////////////////////////////
struct EntityFactoryType * TerrainFactoryGet();

////////////////////////////////////////////////////////////
/// Destroy component pool of terrain factory
////////////////////////////////////////////////////////////
void TerrainFactoryDestroy();

#endif // WORLD_TERRAIN_FACTORY_H
