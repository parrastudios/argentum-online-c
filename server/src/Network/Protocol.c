/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Network/Protocol.h>
#include <Memory/General.h>
#include <DataType/List.h>

#include <World/EntityManager.h>

// todo: HandleOutgoingData
//			a function executed by a timer of timer manager
//			with a specified delay (i.e. 30ms)
//			making a loop over all users and sending its packets
//			after sending, clear packets and restore buffer to 0
//
//			posible problem: more than one packet at a time
//			solution: make a buffer of packets, instead using just one
//					  (do this dinamically, otherwise it will be too overhead)
//
//			posible issue: will the parser handle properly multiple packets in just
//						   one incoming buffer?
//
//						   check it out.
//
//
//			for implementing it:
//				- create a counter of packets (out/in) and the buffers in
//				the Connection structure of the user
//				- remove protocol send data arguments (Data, Size), it will be
//				handled in the proc by a switch
//				- make the proc with a switch calculating the size and appending
//				the data to a buffer (not sending it) accumulating the whole size
//
//				- when we want to send a packet, it will not be sent at that moment,
//				it will be appended to the buffer and periodically the whole buffer
//				will be sent in a batch
//

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct SocketData	SocketListen;		///< Client socket struct
Mutex				ListenerMutex;		///< Socket listen mutex
Thread				ListenerThread;		///< Socket listen thread

////////////////////////////////////////////////////////////
// Packet Handlers
////////////////////////////////////////////////////////////
bool PacketHandleLoginRequest(struct UserData * User);

bool PacketHandleLoginAccount(struct UserData * User);

bool PacketHandleLoginPlayer(struct UserData * User);

bool PacketHandleLogoutPlayer(struct UserData * User);

bool PacketHandleLogoutAccount(struct UserData * User);

bool PacketHandleWorldCheck(struct UserData * User);

bool PacketHandleWorldLoad(struct UserData * User);

bool PacketHandleMovementWalk(struct UserData * User);

bool PacketHandleMovementRun(struct UserData * User);

bool PacketHandleMovementJump(struct UserData * User);

bool PacketHandleMovementFly(struct UserData * User);

bool PacketHandleMovementRide(struct UserData * User);

bool PacketHandleTalkPublic(struct UserData * User);

bool PacketHandleTalkPlayer(struct UserData * User);

bool PacketHandleTalkClan(struct UserData * User);

bool PacketHandleTalkBroadcast(struct UserData * User);

bool PacketHandleTalkGMs(struct UserData * User);

////////////////////////////////////////////////////////////
/// Protocol initializer
////////////////////////////////////////////////////////////
bool ProtocolInitialize(uint16 Port)
{
	// Initialize socket helper (Win32)
	SocketHelperInitialize();

	// Initialize Socket mutex
	MutexInitialize(&ListenerMutex);

	// Initialize socket
	SocketListen.Type = InvalidSocket();

	// Bind the listen socket
	if (!SocketListenPort(&SocketListen, Port))
		return false;

	// Launch the thread, start listening
	ThreadLaunch(&ListenerThread, (FuncType)&ProtocolListener, NULL);

	return true;
}

////////////////////////////////////////////////////////////
/// Protocol destroyer
////////////////////////////////////////////////////////////
bool ProtocolDestroy()
{
	// Close socket
	if (!SocketClose(&SocketListen))
		return false;

	// Destroy mutex
	MutexDestroy(&ListenerMutex);

	// Destroy socket helper
	SocketHelperCleanup();

	return true;
}

////////////////////////////////////////////////////////////
/// Socket listener proc
////////////////////////////////////////////////////////////
void ProtocolListener()
{
	do
	{
		struct SocketData Socket;

		// Accept incoming connection
		if (SocketAccept(&SocketListen, &Socket) == Done)
		{
			struct UserData * User;

			// Create it
			User = UserCreate();

			// Set socket data
			MemoryCopy(&User->Connection.Socket, &Socket, sizeof(struct SocketData));

			// Set parser status
			User->Connection.ParserStatus = NETWORK_PARSER_WAITING_PREAMBLE;

			// Add to the user list
			UsersAdd(User);

			// Launch user thread
			ThreadLaunch(&User->ThreadData, (FuncType)&UserParseData, (void*)User);
		}
	} while (SocketIsValid(&SocketListen));
}

////////////////////////////////////////////////////////////
/// Get properly the size of an outgoing packet
////////////////////////////////////////////////////////////
uint32 ProtocolPacketGetSize(struct UserData * User)
{
	// todo

	switch (User->Connection.Outgoing.Header.Type)
	{
		case NETWORK_TYPE_ERROR :
		{
			return NETWORK_PACKET_DATA_EMPTY;
		}

		case NETWORK_TYPE_LOG_IN :
		{
			switch (User->Connection.Outgoing.Header.Code)
			{
				case NETWORK_CODE_LOG_IN_REQUEST :
				{
					return NETWORK_PACKET_DATA_EMPTY;
				}

				case NETWORK_CODE_LOG_IN_ACCOUNT :
				{
					// Players * Count
					return sizeof(struct AccountPlayerInfo) * User->Connection.Outgoing.Packet.LoginAccountAck.Count;
				}

				case NETWORK_CODE_LOG_IN_PLAYER :
				{
					uint32 MotdLength = strlen((char*)&User->Connection.Outgoing.Packet.LoginPlayerAck.Motd[0]);

					return (sizeof(struct PacketLoginPlayerAck) - 1 + MotdLength);
				}
			}

			break;
		}

		case NETWORK_TYPE_LOG_OUT :
		{
			switch (User->Connection.Outgoing.Header.Code)
			{
				case NETWORK_CODE_LOG_OUT_ACCOUNT :
				{
					return NETWORK_PACKET_DATA_EMPTY;
				}

				case NETWORK_CODE_LOG_OUT_PLAYER :
				{
					return NETWORK_PACKET_DATA_EMPTY;
				}
			}

			break;
		}


		case NETWORK_TYPE_ENVIRONMENT :
		{
			switch (User->Connection.Outgoing.Header.Code)
			{
				case NETWORK_CODE_ENVIRONMENT_AUDIO :
				{
					return sizeof(struct PacketEnvironmentAudio);
				}

				case NETWORK_CODE_ENVIRONMENT_SOUND :
				{
					return sizeof(struct PacketEnvironmentSound);
				}

				case NETWORK_CODE_ENVIRONMENT_FOG :
				{
					return (User->Connection.Outgoing.Packet.EnvironmentFog.Activated ?
							sizeof(struct PacketEnvironmentFog) :
							sizeof(bool));
				}
			}

			break;
		}
	}
	

	// If invalid type or code
	return NETWORK_PACKET_INVALID_SIZE;
}

////////////////////////////////////////////////////////////
/// Check properly the size of an incoming packet
////////////////////////////////////////////////////////////
bool ProtocolPacketCheckSize(struct UserData * User)
{
	uint32 Size = User->Connection.Incoming.Header.DataSize;

	switch (User->Connection.Incoming.Header.Type)
	{
		case NETWORK_TYPE_ERROR :
		{
			return (Size == NETWORK_PACKET_DATA_EMPTY);
		}

		case NETWORK_TYPE_LOG_IN :
		{
			switch (User->Connection.Incoming.Header.Code)
			{
				case NETWORK_CODE_LOG_IN_REQUEST :
				{
					return (Size == sizeof(struct PacketLoginRequest));
				}

				case NETWORK_CODE_LOG_IN_ACCOUNT :
				{
					return (Size == sizeof(struct PacketLoginAccount));
				}

				case NETWORK_CODE_LOG_IN_PLAYER :
				{
					return (Size == sizeof(struct PacketLoginPlayer));
				}
			}

			break;
		}

		case NETWORK_TYPE_LOG_OUT :
		{
			switch (User->Connection.Incoming.Header.Code)
			{
				case NETWORK_CODE_LOG_OUT_ACCOUNT :
				{
					return (Size == NETWORK_PACKET_DATA_EMPTY);
				}

				case NETWORK_CODE_LOG_OUT_PLAYER :
				{
					return (Size == NETWORK_PACKET_DATA_EMPTY);
				}
			}

			break;
		}

		case NETWORK_TYPE_WORLD :
		{
			switch (User->Connection.Incoming.Header.Code)
			{
				case NETWORK_CODE_WORLD_CHECK :
				{
					return (Size == sizeof(struct PacketWorldCheck));
				}

				case NETWORK_CODE_WORLD_LOAD :
				{
					return (Size == NETWORK_PACKET_DATA_EMPTY);
				}
			}

			break;
		}

		case NETWORK_TYPE_MOVEMENT :
		{
			switch (User->Connection.Incoming.Header.Code)
			{
				case NETWORK_CODE_MOVEMENT_WALK :
				{
					return (Size == sizeof(struct PacketMovementWalk));
				}

				case NETWORK_CODE_MOVEMENT_RUN :
				{
					return false;
				}

				case NETWORK_CODE_MOVEMENT_JUMP :
				{
					return false;
				}

				case NETWORK_CODE_MOVEMENT_FLY :
				{
					return false;
				}

				case NETWORK_CODE_MOVEMENT_RIDE :
				{
					return false;
				}

				default :
				{
					ProtocolSendError(User, NETWORK_CODE_ERR_CODE_INVALID);
					return false;
				}
			}

			break;
		}

		case NETWORK_TYPE_TALK :
		{
			switch (User->Connection.Incoming.Header.Code)
			{
				case NETWORK_CODE_TALK_PUBLIC :
				{
					uint32 Length = strlen((char*)&User->Connection.Incoming.Packet.TalkDefault.Text[0]);

					return (Size == (Length + 1));
				}
			}

			break;
		}
	}



	// Invalid size
	return false;
}

////////////////////////////////////////////////////////////
/// Protocol parser
////////////////////////////////////////////////////////////
bool ProtocolParser(struct UserData * User, uint8 * Buffer, uint32 BufferSize)
{
	uint32 i;

	for (i = 0; i < BufferSize; ++i)
	{
		switch (User->Connection.ParserStatus)
		{
			case NETWORK_PARSER_WAITING_PREAMBLE :
			{
				if (Buffer[i] ==  NETWORK_PARSER_PREAMBLE)
				{
					// Get the preamble token
					User->Connection.Incoming.Header.Preamble = Buffer[i];

					// Start parsing the packet
					User->Connection.ParserStatus = NETWORK_PARSER_WAITING_TYPE;
				}
				break;
			}

			case NETWORK_PARSER_WAITING_TYPE :
			{
				// Update packet type
				User->Connection.Incoming.Header.Type = Buffer[i];

				// Wait for packet code
				User->Connection.ParserStatus = NETWORK_PARSER_WAITING_CODE;
				break;
			}

			case NETWORK_PARSER_WAITING_CODE :
			{
				// Update packet code
				User->Connection.Incoming.Header.Code = Buffer[i];

				// Wait for packet length
				User->Connection.ParserStatus = NETWORK_PARSER_WAITING_LENGTH;
				break;
			}

			case NETWORK_PARSER_WAITING_LENGTH :
			{
				// Update packet data size
				User->Connection.Incoming.Header.DataSize = Buffer[i];

				// Wait for checksum
				User->Connection.ParserStatus = NETWORK_PARSER_WAITING_CLENGTH;
				break;
			}

			case NETWORK_PARSER_WAITING_CLENGTH :
			{
				// Check for packet integrity
				if ((User->Connection.Incoming.Header.DataSize + Buffer[i]) != NETWORK_PACKET_LENGTH)
				{
					// Error, wait for next packet
					User->Connection.ParserStatus = NETWORK_PARSER_WAITING_PREAMBLE;
	                
					break;
				}

				// If no data attached, handle user directly
				if (User->Connection.Incoming.Header.DataSize == 0)
				{
					// Wait for next packet
					User->Connection.ParserStatus = NETWORK_PARSER_WAITING_PREAMBLE;

					// Handle packet without data
					return (ProtocolPacketCheckSize(User) ? ProtocolHandleIncomingData(User) : false);
				}

				// If attached data, parse it
				if (User->Connection.Incoming.Header.DataSize > 0)
				{
					// Set current data counter to first byte of the structure
					User->Connection.CurrentLength = NETWORK_PACKET_HEADER_LENGTH;

					// Start parsing data
					User->Connection.ParserStatus = NETWORK_PARSER_WAITING_DATA;
					
					break;
				}
			}

			case NETWORK_PARSER_WAITING_DATA :
			{
				// Get the data
				User->Connection.Incoming.Buffer[User->Connection.CurrentLength++] = Buffer[i];

				// If data is read completely
				if ((User->Connection.CurrentLength - NETWORK_PACKET_HEADER_LENGTH) ==
					 User->Connection.Incoming.Header.DataSize)
				{
					// Wait for the next packet
					User->Connection.ParserStatus = NETWORK_PARSER_WAITING_PREAMBLE;

					// Return false if the data is wrong
					return (ProtocolPacketCheckSize(User) ? ProtocolHandleIncomingData(User) : false);
				}

				break;
			}
		}
	}

	return true;
}

////////////////////////////////////////////////////////////
/// Handles incoming data socket
////////////////////////////////////////////////////////////
bool ProtocolHandleIncomingData(struct UserData * User)
{
	switch (User->Connection.Incoming.Header.Type)
	{
		// Error packet

		case NETWORK_TYPE_ERROR :
		{
			// Handle error

			switch (User->Connection.Incoming.Header.Code)
			{
				case NETWORK_CODE_ERR_TYPE_INVALID :
				{
					
					break;
				}

				case NETWORK_CODE_ERR_CODE_INVALID :
				{
					
					break;
				}

				default :
				{
					ProtocolSendError(User, NETWORK_CODE_ERR_CODE_INVALID);
					return false;
				}
			}

			break;
		}

		// Login packet

		case NETWORK_TYPE_LOG_IN :
		{
			switch (User->Connection.Incoming.Header.Code)
			{
				case NETWORK_CODE_LOG_IN_REQUEST :
				{
					return PacketHandleLoginRequest(User);
				}

				case NETWORK_CODE_LOG_IN_ACCOUNT :
				{
					return PacketHandleLoginAccount(User);
				}

				case NETWORK_CODE_LOG_IN_PLAYER :
				{
					return PacketHandleLoginPlayer(User);
				}

				default :
				{
					ProtocolSendError(User, NETWORK_CODE_ERR_CODE_INVALID);
					return false;
				}
			}

			break;
		}

		// Logout packet

		case NETWORK_TYPE_LOG_OUT :
		{
			switch (User->Connection.Incoming.Header.Code)
			{
				case NETWORK_CODE_LOG_OUT_ACCOUNT :
				{
					return PacketHandleLogoutAccount(User);
				}

				case NETWORK_CODE_LOG_OUT_PLAYER :
				{
					return PacketHandleLogoutPlayer(User);
				}

				default :
				{
					ProtocolSendError(User, NETWORK_CODE_ERR_CODE_INVALID);
					return false;
				}
			}

			break;
		}

		// World packet

		case NETWORK_TYPE_WORLD :
		{
			switch (User->Connection.Incoming.Header.Code)
			{
				case NETWORK_CODE_WORLD_CHECK :
				{
					return PacketHandleWorldCheck(User);
				}

				case NETWORK_CODE_WORLD_LOAD :
				{
					return PacketHandleWorldLoad(User);
				}

				default :
				{
					ProtocolSendError(User, NETWORK_CODE_ERR_CODE_INVALID);
					return false;
				}
			}

			break;
		}

		// Movement packet

		case NETWORK_TYPE_MOVEMENT :
		{
			switch (User->Connection.Incoming.Header.Code)
			{
				case NETWORK_CODE_MOVEMENT_WALK :
				{
					return PacketHandleMovementWalk(User);
				}

				case NETWORK_CODE_MOVEMENT_RUN :
				{
					return PacketHandleMovementRun(User);
				}

				case NETWORK_CODE_MOVEMENT_JUMP :
				{
					return PacketHandleMovementJump(User);
				}

				case NETWORK_CODE_MOVEMENT_FLY :
				{
					return PacketHandleMovementFly(User);
				}

				case NETWORK_CODE_MOVEMENT_RIDE :
				{
					return PacketHandleMovementRide(User);
				}

				default :
				{
					ProtocolSendError(User, NETWORK_CODE_ERR_CODE_INVALID);
					return false;
				}
			}

			break;
		}

		case NETWORK_TYPE_TALK :
		{
			switch (User->Connection.Incoming.Header.Code)
			{
				case NETWORK_CODE_TALK_PUBLIC :
				{
					return PacketHandleTalkPublic(User);
				}

				case NETWORK_CODE_TALK_PLAYER :
				{
					return PacketHandleTalkPlayer(User);
				}

				case NETWORK_CODE_TALK_CLAN :
				{
					return PacketHandleTalkClan(User);
				}

				case NETWORK_CODE_TALK_BROADCAST :
				{
					return PacketHandleTalkBroadcast(User);
				}

				case NETWORK_CODE_TALK_GMS :
				{
					return PacketHandleTalkGMs(User);
				}

				default :
				{
					ProtocolSendError(User, NETWORK_CODE_ERR_CODE_INVALID);
					return false;
				}
			}
		}

		// If packet type is another else
		default :
		{
			ProtocolSendError(User, NETWORK_CODE_ERR_TYPE_INVALID);
			return false;
		}
	}

    return false;
}

////////////////////////////////////////////////////////////
/// Protocol send data procediment
////////////////////////////////////////////////////////////
void ProtocolSendData(struct UserData * User, uint8 Type, uint8 Code)
{
	uint32 Size;

	if (!SocketIsValid(&User->Connection.Socket)) return;

	// Set buffer initial data
	User->Connection.Outgoing.Header.Preamble	= NETWORK_PARSER_PREAMBLE;
	User->Connection.Outgoing.Header.Type		= Type;
	User->Connection.Outgoing.Header.Code		= Code;
	
	Size = ProtocolPacketGetSize(User);

	if (Size != NETWORK_PACKET_INVALID_SIZE && Size <= NETWORK_PACKET_DATA_LENGTH)
	{
		// Update size
		User->Connection.Outgoing.Header.DataSize = Size;
		User->Connection.Outgoing.Header.Checksum = ~Size;

		// Send data to user
		SocketSend(&User->Connection.Socket,
				   &User->Connection.Outgoing.Buffer[0],
				   User->Connection.Outgoing.Header.DataSize + NETWORK_PACKET_HEADER_LENGTH);
	}
}

////////////////////////////////////////////////////////////
/// Protocol send error
////////////////////////////////////////////////////////////
void ProtocolSendError(struct UserData * User, uint32 Code)
{
	ProtocolSendData(User, NETWORK_TYPE_ERROR, Code);
}

////////////////////////////////////////////////////////////
/// Protocol packet request handler
////////////////////////////////////////////////////////////
bool PacketHandleLoginRequest(struct UserData * User)
{
	#if (COMPILE_TYPE == DEBUG_MODE)
		// Use a predefined md5 code for debug
		char MD5Code[NETWORK_TYPE_LOGIN_REQUEST_LENGTH] = "abcdefghijklmnop";
	#else
		// Get md5 code (todo)
		char MD5Code[NETWORK_TYPE_LOGIN_REQUEST_LENGTH] = "0000000000000000";
	#endif

	// Check client integrity
	if (APPLICATION_VERSION_CHECK(User->Connection.Incoming.Packet.Request.Version.Major,
								  User->Connection.Incoming.Packet.Request.Version.Minor,
								  User->Connection.Incoming.Packet.Request.Version.Revision) &&
	    strncmp(User->Connection.Incoming.Packet.Request.MD5, MD5Code, NETWORK_TYPE_LOGIN_REQUEST_LENGTH) == 0)
	{
		// Send ack to the user
		ProtocolSendData(User, NETWORK_TYPE_LOG_IN, NETWORK_CODE_LOG_IN_REQUEST);
		return true;
	}
	else
	{
		// Send error to the user
		ProtocolSendError(User, NETWORK_CODE_ERR_REQ_INVALID);

		// Critical error (disconnect)
		return false;
	}
}

////////////////////////////////////////////////////////////
/// Protocol packet login account handler
////////////////////////////////////////////////////////////
bool PacketHandleLoginAccount(struct UserData * User)
{
	// Login the account
	uint32 Result = AccountLogin(&User->Account,
								 User->Connection.Incoming.Packet.LoginAccount.Name,
								 User->Connection.Incoming.Packet.LoginAccount.Password);

	// Handle the result
	switch (Result)
	{
		case ACCOUNT_LOG_IN_ERROR_NONE :
		{
			// Send result to the user
			PacketSendLoginAccountAck(User);
			break;
		}

		case ACCOUNT_LOG_IN_ERROR_LOGGED :
		{
			ProtocolSendError(User, NETWORK_CODE_ERR_ACC_LOGGED);
			break;
		}

		case ACCOUNT_LOG_IN_ERROR_BAN :
		{
			ProtocolSendError(User, NETWORK_CODE_ERR_ACC_BAN);
			break;
		}

		case ACCOUNT_LOG_IN_ERROR_PASSWORD :
		{
			ProtocolSendError(User, NETWORK_CODE_ERR_ACC_PASSW);
			break;
		}

		case ACCOUNT_LOG_IN_ERROR_NAME :
		{
			ProtocolSendError(User, NETWORK_CODE_ERR_ACC_INVALID);
			break;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////
/// Protocol packet player login handler
////////////////////////////////////////////////////////////
bool PacketHandleLoginPlayer(struct UserData * User)
{
	// Login the player
	uint32 Result = AccountPlayerLogin(&User->Account, User->Connection.Incoming.Packet.LoginPlayer.Index);

	// Handle the result
	switch (Result)
	{
		case PLAYER_LOG_IN_ERROR_NONE :
		{
			// Send result to the user
			PacketSendLoginPlayerAck(User);
			break;
		}

		case PLAYER_LOG_IN_ERROR_INVALID :
		{
			ProtocolSendError(User, NETWORK_CODE_ERR_PLY_INVALID);

			// Critical error (disconnect)
			return false;
		}

		case PLAYER_LOG_IN_ERROR_LOGGED :
		{
			ProtocolSendError(User, NETWORK_CODE_ERR_PLY_LOGGED);
			break;
		}

		case PLAYER_LOG_IN_ERROR_BAN :
		{
			ProtocolSendError(User, NETWORK_CODE_ERR_PLY_BAN);
			break;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////
/// Protocol packet player logout handler
////////////////////////////////////////////////////////////
bool PacketHandleLogoutPlayer(struct UserData * User)
{
	uint32 Result = AccountPlayerLogout(&User->Account);

	// Handle the result
	switch (Result)
	{
		case PLAYER_LOG_OUT_ERROR_NONE :
		{
			// Send ack to user
			ProtocolSendData(User, NETWORK_TYPE_LOG_OUT, NETWORK_CODE_LOG_OUT_PLAYER);
			break;
		}

		case PLAYER_LOG_OUT_ERROR_NOT_EXIT :
		{
			// Player cannot exit
			ProtocolSendError(User, NETWORK_CODE_ERR_PLY_NOT_EXIT);
			break;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////
/// Protocol packet account logout handler
////////////////////////////////////////////////////////////
bool PacketHandleLogoutAccount(struct UserData * User)
{
	uint32 Result = AccountLogout(&User->Account);

	// Handle the result
	switch (Result)
	{
		case ACCOUNT_LOG_OUT_ERROR_NONE :
		{
			// Send ack to user
			ProtocolSendData(User, NETWORK_TYPE_LOG_OUT, NETWORK_CODE_LOG_OUT_ACCOUNT);
			break;
		}

		case ACCOUNT_LOG_OUT_ERROR_NOT_EXIT :
		{
			// Player cannot exit
			ProtocolSendError(User, NETWORK_CODE_ERR_ACC_NOT_EXIT);
			break;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////
/// Protocol packet world check
////////////////////////////////////////////////////////////
bool PacketHandleWorldCheck(struct UserData * User)
{
	// todo

	//if (WorldCheckIntegrity(User->Connection.Incoming.Packet.WorldCheck.SceneIndex,
	//						User->Connection.Incoming.Packet.WorldCheck.CheckSum))
	{
		// Get entities updated near to the user
		// WorldGetUpdatedEntityList(


		ProtocolSendData(User, NETWORK_TYPE_LOG_OUT, NETWORK_CODE_LOG_OUT_ACCOUNT);
	}
	//else
	{
		// if valid, send world data (update)
		// if not, send download and then send update
	}
	
	return true;
}

////////////////////////////////////////////////////////////
/// Protocol packet world load ack
////////////////////////////////////////////////////////////
bool PacketHandleWorldLoad(struct UserData * User)
{
	// Send world create packet
	PacketSendWorldCreate(User);

	return true;
}

////////////////////////////////////////////////////////////
/// Protocol packet movement walk handler
////////////////////////////////////////////////////////////
bool PacketHandleMovementWalk(struct UserData * User)
{
	return UserCharacterMove(	User,
								User->Connection.Incoming.Packet.MovementWalk.Key,
								User->Connection.Incoming.Packet.MovementWalk.Pressed);
}

////////////////////////////////////////////////////////////
/// Protocol packet movement run handler
////////////////////////////////////////////////////////////
bool PacketHandleMovementRun(struct UserData * User)
{
	// todo

	// move this to gamelogic
	/*
	if (User->Character.Attributes.Player.Flags.Running)
	{
		// Movement decreased X
		 User->Character.Attributes.Player.Flags.Running = false;
	}
	else
	{
		// Movement increased X
		User->Character.Attributes.Player.Flags.Running = true;
	}
	*/
	return true;
}

////////////////////////////////////////////////////////////
/// Protocol packet movement jump handler
////////////////////////////////////////////////////////////
bool PacketHandleMovementJump(struct UserData * User)
{
	// todo

	// move this to gamelogic
	/*
	if (User->Character.Attributes.Player.Flags.Jumping)
	{
		// Character jumps and then falls
		 User->Character.Attributes.Player.Flags.Jumping = false;
	}
	*/

	return true;
}
////////////////////////////////////////////////////////////
/// Protocol packet movement fly handler
////////////////////////////////////////////////////////////
bool PacketHandleMovementFly(struct UserData * User)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Protocol packet movement ride handler
////////////////////////////////////////////////////////////
bool PacketHandleMovementRide(struct UserData * User)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Protocol packet talk public handler
////////////////////////////////////////////////////////////
bool PacketHandleTalkPublic(struct UserData * User)
{
	// Send text to all users
	UsersBroadcastMessage(&User->Connection.Incoming.Header);

	return true;
}

////////////////////////////////////////////////////////////
/// Protocol packet talk player handler
////////////////////////////////////////////////////////////
bool PacketHandleTalkPlayer(struct UserData * User)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Protocol packet talk clan handler
////////////////////////////////////////////////////////////
bool PacketHandleTalkClan(struct UserData * User)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Protocol packet talk broadcast handler
////////////////////////////////////////////////////////////
bool PacketHandleTalkBroadcast(struct UserData * User)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Protocol packet talk GMs handler
////////////////////////////////////////////////////////////
bool PacketHandleTalkGMs(struct UserData * User)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Protocol packet login account ack sender
////////////////////////////////////////////////////////////
void PacketSendLoginAccountAck(struct UserData * User)
{
	// Get players info of account
	AccountGetPlayersInfo(&User->Account,
						  User->Connection.Outgoing.Packet.LoginAccountAck.Players,
						  &User->Connection.Outgoing.Packet.LoginAccountAck.Count);

	// Send players basic info
	ProtocolSendData(User, NETWORK_TYPE_LOG_IN, NETWORK_CODE_LOG_IN_ACCOUNT);
}

////////////////////////////////////////////////////////////
/// Protocol packet player stats sender
////////////////////////////////////////////////////////////
void PacketSendStatsPlayer(struct UserData * User)
{

}

////////////////////////////////////////////////////////////
/// Protocol packet player inventory sender
////////////////////////////////////////////////////////////
void PacketSendInventoryPlayer(struct UserData * User)
{

}

////////////////////////////////////////////////////////////
/// Protocol packet player spells sender
////////////////////////////////////////////////////////////
void PacketSendSpellsPlayer(struct UserData * User)
{

}

////////////////////////////////////////////////////////////
/// Protocol packet player equipped items sender
////////////////////////////////////////////////////////////
void PacketSendEquippedPlayer(struct UserData * User)
{

}

////////////////////////////////////////////////////////////
/// Protocol packet login player ack sender implementation
////////////////////////////////////////////////////////////
void PacketSendLoginPlayerAckImpl(struct UserData * User)
{
	// Obtain player data from account
	struct CharacterData * Character = UserGetCharacter(User);

	// Copy data into packet
	User->Connection.Outgoing.Packet.LoginPlayerAck.Player.Index = Character->Index;
	strcpy((char*)&User->Connection.Outgoing.Packet.LoginPlayerAck.Motd[0],
			"Welcome to " APPLICATION_NAME " v" APPLICATION_VERSION);

	// Send player data
	ProtocolSendData(User, NETWORK_TYPE_LOG_IN, NETWORK_CODE_LOG_IN_PLAYER);
}

////////////////////////////////////////////////////////////
/// Protocol packet login player ack sender
////////////////////////////////////////////////////////////
void PacketSendLoginPlayerAck(struct UserData * User)
{
	// Send world load packet
	PacketSendPacketWorldLoad(User);

	// Send player stats
	PacketSendStatsPlayer(User);

	// Send inventory data
	PacketSendInventoryPlayer(User);

	// Send spells data
	PacketSendSpellsPlayer(User);

	// Send equipped items
	PacketSendEquippedPlayer(User);

	// Send player login related data
	PacketSendLoginPlayerAckImpl(User);
}

////////////////////////////////////////////////////////////
/// Protocol packet world load sender
////////////////////////////////////////////////////////////
void PacketSendPacketWorldLoad(struct UserData * User)
{
	struct CharacterData * Character = UserGetCharacter(User);

	User->Connection.Outgoing.Packet.WorldLoad.Index = Character->World;

	ProtocolSendData(User, NETWORK_TYPE_WORLD, NETWORK_CODE_WORLD_LOAD);
}

////////////////////////////////////////////////////////////
/// Protocol packet world create sender
////////////////////////////////////////////////////////////
void PacketSendWorldCreate(struct UserData * User)
{
	// todo:

	/*
	// Send create entity list
	struct PacketWorldCreate * WorldCreate = &User->Connection.Outgoing.Packet.WorldCreate;

	// Get create list of the entity
	List EntityList = SceneManagerGetCreateEntityList(EntityBaseCharacter(User->Character));

	ListIterator Iterator;

	// Initialize packet list counter
	WorldCreate->EntityCount = 0;

	// Iterate in create world entity list
	ListForEach(EntityList, Iterator)
	{
		struct EntityType * Entity = ListItData(Iterator, struct EntityType *);

		// Copy entity index
		WorldCreate->EntityList[WorldCreate->EntityCount].Index = Entity->Index;

		// ..

		// Increment packet list counter
		++WorldCreate->EntityCount;

		// Send a portion of the entity list
		if (WorldCreate->EntityCount == NETWORK_TYPE_WORLD_CREATE_LENGTH)
		{
			// Send list
			ProtocolSendData(User, NETWORK_TYPE_WORLD, NETWORK_CODE_WORLD_CREATE);

			// Restart list counter
			WorldCreate->EntityCount = 0;
		}
	}

	// Clear entity list
	ListDestroy(EntityList);
	*/
}

////////////////////////////////////////////////////////////
/// Protocol packet world update sender
////////////////////////////////////////////////////////////
void PacketSendWorldUpdate(struct UserData * User)
{
	// todo:

	/*
	// Send update entity list
	struct PacketWorldUpdate * WorldUpdate = &User->Connection.Outgoing.Packet.WorldUpdate;

	// Get upate list of the entity
	List EntityList = SceneManagerGetUpdateEntityList(EntityBaseCharacter(User->Character));

	ListIterator Iterator;

	// Initialize packet list counter
	WorldUpdate->EntityCount = 0;

	// Iterate in update world entity list
	ListForEach(EntityList, Iterator)
	{
		struct EntityType * Entity = ListItData(Iterator, struct EntityType *);

		// Copy entity index
		WorldUpdate->EntityList[WorldUpdate->EntityCount].Index = Entity->Index;

		// Copy entity position
		Vector3fCopy(&WorldUpdate->EntityList[WorldUpdate->EntityCount].Position, &Entity->Position);

		// Copy entity rotation
		QuaternionCopy(&WorldUpdate->EntityList[WorldUpdate->EntityCount].Rotation, &Entity->Rotation);

		// Increment packet list counter
		++WorldUpdate->EntityCount;

		// Send a portion of the entity list
		if (WorldUpdate->EntityCount == NETWORK_TYPE_WORLD_UPDATE_LENGTH)
		{
			// Send list
			ProtocolSendData(User, NETWORK_TYPE_WORLD, NETWORK_CODE_WORLD_UPDATE);

			// Restart list counter
			WorldUpdate->EntityCount = 0;
		}
	}
	*/
}

////////////////////////////////////////////////////////////
/// Protocol packet world modify sender
////////////////////////////////////////////////////////////
void PacketSendWorldModify(struct UserData * User)
{

}

////////////////////////////////////////////////////////////
/// Protocol packet world destroy sender
////////////////////////////////////////////////////////////
void PacketSendWorldDestroy(struct UserData * User)
{
	// todo:

	/*
	// Send destroy entity list
	struct PacketWorldDestroy * WorldDestroy = &User->Connection.Outgoing.Packet.WorldDestroy;

	// Get destroy list of the entity
	List EntityList = SceneManagerGetDestroyEntityList(EntityBaseCharacter(User->Character));

	struct EntityData * Iterator;

	// Initialize packet list counter
	WorldDestroy->EntityCount = 0;

	// Iterate in destroy world entity list
	ListForEach(EntityList, Iterator)
	{
		struct EntityType * Entity = ListItData(Iterator, struct EntityType *);

		// Copy entity index
		WorldDestroy->EntityList[WorldDestroy->EntityCount].Index = Entity->Index;

		// Increment packet list counter
		++WorldDestroy->EntityCount;

		// Send a portion of the entity list
		if (WorldDestroy->EntityCount == NETWORK_TYPE_WORLD_DESTROY_LENGTH)
		{
			// Send list
			ProtocolSendData(User, NETWORK_TYPE_WORLD, NETWORK_CODE_WORLD_DESTROY);

			// Restart list counter
			WorldDestroy->EntityCount = 0;
		}
	}
	*/
}

////////////////////////////////////////////////////////////
/// Protocol packet environment audio sender
////////////////////////////////////////////////////////////
void PacketSendEnvironmentAudio(struct UserData * User)
{
	// todo

	// Get audio values
  	//  EnvironmentGetAudio(User, &User->Connection.Outgoing.Packet.EnvironmentAudio);

 	// Send audio data
 	ProtocolSendData(User, NETWORK_TYPE_ENVIRONMENT, NETWORK_CODE_ENVIRONMENT_AUDIO);
}

////////////////////////////////////////////////////////////
/// Protocol packet environment sound sender
////////////////////////////////////////////////////////////
void PacketSendEnvironmentSound(struct UserData * User)
{
	// todo

	// Get sound values
  	// EnvironmentGetSound(User, &User->Connection.Outgoing.Packet.EnvironmentSound);

 	// Send sound data
 	ProtocolSendData(User, NETWORK_TYPE_ENVIRONMENT, NETWORK_CODE_ENVIRONMENT_SOUND);
}

////////////////////////////////////////////////////////////
/// Protocol packet environment fog sender
////////////////////////////////////////////////////////////
void PacketSendEnvironmentFog(struct UserData * User)
{
	// todo

	// Check if user has fog in region where he is
	//User->Connection.Outgoing.Packet.EnvironmentFog.Activated = EnvironmentFogActivated(User);

	// Get fog values
	//if (User->Connection.Outgoing.Packet.EnvironmentFog.Activated)
	{
	//	EnvironmentGetFogProperties(User, &User->Connection.Outgoing.Packet.EnvironmentFog.Properties);
	}

	// Send fog data
	ProtocolSendData(User, NETWORK_TYPE_ENVIRONMENT, NETWORK_CODE_ENVIRONMENT_FOG);
}
