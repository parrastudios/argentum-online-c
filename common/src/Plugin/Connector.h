/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	PLUGIN_CONNECTOR_H
#define PLUGIN_CONNECTOR_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Preprocessor/Stringify.h>
#include <Preprocessor/Concatenation.h>

#include <DataType/HashMap.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PLUGIN_CONNECTOR_NAME									plugin_connect_func			//< Connection functor header
#define PLUGIN_CONNECTOR_VERSION								0x0000010BUL				//< 0.1.b
#define PLUGIN_CONNECTOR_NAME_DEFINITION_GLUE(Name, Version)	Name ## _ ## Version
#define PLUGIN_CONNECTOR_NAME_DEFINITION_DECL(Name, Version)	PLUGIN_CONNECTOR_NAME_DEFINITION_GLUE(Name, Version)
#define PLUGIN_CONNECTOR_NAME_DEFINITION						PLUGIN_CONNECTOR_NAME_DEFINITION_DECL(PLUGIN_CONNECTOR_NAME, PLUGIN_CONNECTOR_VERSION)
#define PLUGIN_CONNECTOR_FUNC_DECL								void PLUGIN_CONNECTOR_NAME_DEFINITION(PluginConnectorInfoType *)
#define PLUGIN_CONNECTOR_INFO_PARAM								ConnectorInfoParam
#define PLUGIN_CONNECTOR_FUNC_IMPL								void PLUGIN_CONNECTOR_NAME_DEFINITION(PluginConnectorInfoType * PLUGIN_CONNECTOR_INFO_PARAM)
#define PLUGIN_CONNECTOR_NAME_STR								PREPROCESSOR_STRINGIFY(PLUGIN_CONNECTOR_NAME_DEFINITION)
#define PLUGIN_CONNECTOR_HOOK_NAME_LENGTH						0xFFUL
#define PLUGIN_CONNECTOR_INFO_NAME								PluginConnectorInfo
#define PLUGIN_CONNECTOR_IMPL_DECL(Name)						uint32 Name()

#define PluginConnectorInfoGet()								PLUGIN_CONNECTOR_INFO_NAME

typedef void *	ConnectorAddress;
typedef char	ConnectorHookName[PLUGIN_CONNECTOR_HOOK_NAME_LENGTH];
typedef void *	ConnectorHookAddress;
typedef uint32(*ConnectorRegisterHandler)(); // todo: this body only for test..

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef struct PluginType PluginType;

typedef struct PluginConnectorInfoType
{
	const char * Name;
	ConnectorRegisterHandler Register;
} * PluginConnectorInfoType;

typedef struct PluginConnectorType
{
	const char * (*Name)();
	bool (*Bind) (PluginType *, ConnectorAddress);
	bool (*Invoke) (PluginType *);

	ConnectorAddress		Address;	//< Connector address in the plugin
	HashMap					HookMap;	//< Map of hooks (ConnectorHookName -> ConnectorHookAddress)
	PluginConnectorInfoType	Info;		//< Info holding the custom connector pointer
} PluginConnectorType;


/*
////////////////////////////////////////////////////////////

//#define PLUGIN_DEFINE_CONNECTION(Name) 

typedef SetHashType ConnectorHookId;
typedef void * ConnectorHookAddress;
typedef char ConnectorHookName[CONNECTOR_HOOK_NAME_LENGTH];

struct PluginConnectorSlotType
{
	ConnectorHookName		Name;							//< Hook name for a connection slot
	ConnectorHookAddress	Address;						//< Hook function address
};

typedef ConnectorHookId(*ConnectEventHandler)(const char *, ConnectorHookAddress)

struct PluginConnectorHookMapType
{
	ConnectEventHandler	Connect;							//< Event handler to connect function
	Set					HookSet;							//< Map of ConnectorHookId to PluginConnectorSlotType
};

typedef void(*RegisterEventHandler)(struct PluginInterfaceType *);

struct PluginConnectorType
{
	RegisterEventHandler				Register;			//< Pointer to register event
	struct PluginInterfaceType *		Interface;			//< Interface of the plugin
	struct PluginConnectorHookMapType	PluginHookTable;	//< Table holding raw address of plugin to the 
};

////////////////////////////////////////////////////////////
*/

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize a connector type
////////////////////////////////////////////////////////////
PluginConnectorType * ConnectorInitialize(PluginConnectorType * Connector);

////////////////////////////////////////////////////////////
/// Release a connector type
////////////////////////////////////////////////////////////
void ConnectorDestroy(PluginConnectorType * Connector);

#endif // PLUGIN_CONNECTOR_H
