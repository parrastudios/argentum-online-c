/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize all factories
////////////////////////////////////////////////////////////
void EntitySystemInitialize()
{
	uinteger i;

	// Initialize all factories
	for (i = 0; i < ENTITY_TYPE_COUNT; ++i)
	{
		EntityFactoryInitialize(EntityFactoryList(i));
	}
}

////////////////////////////////////////////////////////////
/// Destroy all factories
////////////////////////////////////////////////////////////
void EntitySystemDestroy()
{
	uinteger i;

	// Destroy all factories
	for (i = 0; i < ENTITY_TYPE_COUNT; ++i)
	{
		EntityFactoryDestroy(EntityFactoryList(i));
	}
}

////////////////////////////////////////////////////////////
/// Create an entity
////////////////////////////////////////////////////////////
EntityIdType EntityCreate(uinteger Type)
{
	if (Type < ENTITY_TYPE_COUNT && EntityFactoryList(Type) != NULL)
	{
		struct EntityType * Entity = EntityFactoryCreateEntity(EntityFactoryList(Type));

		if (Entity)
		{
			Entity->Components = HashMapCreate(HashGetCallbackUnsignedIntegerDefault(), CompareGetCallback(COMPARE_FUNC_UINTEGER));

            if (Entity->Components)
            {
                EntityFactoryBindComponents(EntityFactoryList(Type), Entity);

                Entity = Entity->Factory->Create(Entity);

                return Entity->Index;
            }
		}
	}

	return ENTITY_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Get entity component
////////////////////////////////////////////////////////////
struct ComponentType * EntityComponent(struct EntityType * Entity, uinteger DataTypeId)
{
	struct EntityComponentRefType * ComponentRef = HashMapGet(Entity->Components, (HashKeyData)&DataTypeId);

	if (ComponentRef)
	{
		if (ComponentRef->Managed)
		{
			return EntityFactoryGetComponent(Entity->Factory, DataTypeId, ComponentRef->Component.Id);
		}
		else
		{
			return ComponentRef->Component.Ptr;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Get entity from factory
////////////////////////////////////////////////////////////
struct EntityType * EntityGet(uinteger Type, EntityIdType EntityId)
{
	if (Type < ENTITY_TYPE_COUNT && EntityFactoryList(Type) != NULL)
	{
		struct EntityType * Entity = SetGetValueByHash(EntityFactoryList(Type)->EntityPool, EntityId);

		if (Entity && Entity->Index != ENTITY_INDEX_INVALID)
		{
			return Entity;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy an entity
////////////////////////////////////////////////////////////
void EntityDestroy(uinteger Type, EntityIdType EntityId)
{
	struct EntityType * Entity = EntityGet(Type, EntityId);

	if (Entity)
	{
		if (Entity->Factory)
		{
			Entity->Factory->Destroy(Entity);
		}

		if (Entity->Factory)
		{
			if (EntityFactoryDestroyEntity(Entity))
			{
				// Destroy components
				HashMapDestroy(Entity->Components);
			}
			else
			{
				// Error: Invalid destruction
			}
		}
		else
		{
			// Destroy components
			HashMapDestroy(Entity->Components);

			// Deallocate entity
			MemoryDeallocate(Entity);
		}
	}
}

////////////////////////////////////////////////////////////
/// Creates a component in the factory
////////////////////////////////////////////////////////////
ComponentIdType EntityCreateComponent(struct EntityType * Entity, uinteger DataType)
{
	ComponentIdType ComponentId = COMPONENT_ID_INVALID;

	if (Entity->Factory)
	{
		// Get pool of desired data type
		struct ComponentPoolType * ComponentPool = (struct ComponentPoolType *)HashMapGet(Entity->Factory->ComponentMap, (HashKeyData)&DataType);

		if (ComponentPool)
		{
			bool ClearFreeId;

			// Check if there are free ids
			if (!ListEmpty(ComponentPool->FreeIds))
			{
				// Get first free entry in the set
				ComponentId = (ComponentIdType)ListFront(ComponentPool->FreeIds);

				// Set clear flag
				ClearFreeId = true;
			}
			else
			{
				// Get index from new entry in the set
				ComponentId = (ComponentIdType)VectorSize(ComponentPool->Table);

				// Reserve a slot in the table
				VectorPushBackEmpty(ComponentPool->Table);

				// Set clear flag
				ClearFreeId = false;
			}

			if (ComponentId != COMPONENT_ID_INVALID)
			{
				struct ComponentType * Component;

				// Get dynamic id referring to component vector id
				ComponentIdType * DynamicComponentId = (ComponentIdType *)VectorAt(ComponentPool->Table, ComponentId);

				// Insert the new component in the pool and update its dynamic id reference
				*DynamicComponentId = VectorSize(ComponentPool->Components);

				// Push the new component in the vector
				VectorPushBackEmpty(ComponentPool->Components);

				// Get the component
				Component = (struct ComponentType *)VectorBack(ComponentPool->Components);

				// Auto-reference with the static id
				Component->Index = ComponentId;

				// Link data to component data type holder
				Component->DataImpl.Type = DataType;
				Component->DataImpl.Size = VectorTypeSize(ComponentPool->Components);

				// Set attributes to null because pointer will be recalculated each time
				Component->DataImpl.Attributes = NULL; //ComponentGetDataImpl(Component);

				// Link data type to component and component to entity
				Component = ComponentInitialize(Component, Entity, &Component->DataImpl, NULL);

				// If successful creation
				if (Component)
				{
					if (ClearFreeId)
					{
						// Clear index from free list
						ListPopFront(ComponentPool->FreeIds);
					}

					// Return static reference (to table)
					return Component->Index;
				}
			}
		}
	}

	return COMPONENT_ID_INVALID;
}

////////////////////////////////////////////////////////////
/// Destroy a component of the factory
////////////////////////////////////////////////////////////
bool EntityDestroyComponent(struct EntityType * Entity, uinteger DataType, ComponentIdType ComponentId)
{
	if (Entity->Factory)
	{
		// Get pool of desired data type
		struct ComponentPoolType * ComponentPool = (struct ComponentPoolType *)HashMapGet(Entity->Factory->ComponentMap, (HashKeyData)&DataType);

		if (ComponentPool)
		{
			struct ComponentType * Component = EntityComponent(Entity, DataType);

			if (Component)
			{
				// Get dynamic id refering to component vector id
				ComponentIdType * DynamicComponentId = (ComponentIdType *)VectorAt(ComponentPool->Table, ComponentId);

				// Delete component
				ComponentDestroy(Component);

				if (DynamicComponentId && *DynamicComponentId != COMPONENT_ID_INVALID)
				{
					uinteger Id;

					// Remove from component pool
					VectorErase(ComponentPool->Components, *DynamicComponentId);

					// Reindex table of dynamic ids
					for (Id = *DynamicComponentId; Id < VectorSize(ComponentPool->Components); ++Id)
					{
						struct ComponentType * Component = (struct ComponentType *)VectorAt(ComponentPool->Components, Id);

						ComponentIdType * Iterator = (ComponentIdType *)VectorAt(ComponentPool->Table, Component->Index);

						*Iterator = Id;
					}

					// Insert static id into free id list
					ListPushBack(ComponentPool->FreeIds, (void *)&ComponentId);

					// Set component id to invalid
					*DynamicComponentId = COMPONENT_ID_INVALID;
				}
			}
		}
	}

	// Invalid component
	return false;
}

////////////////////////////////////////////////////////////
/// Attach component to an entity
////////////////////////////////////////////////////////////
void EntityAttachComponent(struct EntityType * Entity, struct ComponentType * Component)
{
	if (Entity && Component)
	{
		struct EntityComponentRefType * ComponentRef = (struct EntityComponentRefType *)MemoryAllocate(sizeof(struct EntityComponentRefType));

		if (ComponentRef)
		{
			// Set entity reference
			Component->EntityId = Entity->Index;

			// Set entity factory reference
			Component->EntityFactory = Entity->Factory->Type;

			// Set component reference memory behavior
			ComponentRef->Managed = Component->Managed;

			// Set component reference type
			ComponentRef->Type = Component->DataImpl.Type;

			if (Component->Managed)
			{
				// Set component reference index
				ComponentRef->Component.Id = Component->Index;
			}
			else
			{
				// Set component reference pointer
				ComponentRef->Component.Ptr = Component;
			}

			// Append to component map
			HashMapInsert(Entity->Components, (HashKeyData)&ComponentRef->Type, (void *)ComponentRef);
		}
	}
}

////////////////////////////////////////////////////////////
/// Detach component to an entity
////////////////////////////////////////////////////////////
void EntityDetachComponent(struct EntityType * Entity, struct ComponentType * Component)
{
    if (Entity && Component->EntityId != ENTITY_INDEX_INVALID &&
        Entity->Index == Component->EntityId &&
        Entity->Factory && Entity->Factory->Type == Component->EntityFactory)
    {
		struct EntityComponentRefType * ComponentRef = HashMapGet(Entity->Components, (HashKeyData)&Component->DataImpl.Type);

		if (ComponentRef)
		{
			// Remove from component map
			HashMapRemove(Entity->Components, (HashKeyData)&Component->DataImpl.Type);

			// Destroy component reference
			MemoryDeallocate(ComponentRef);
		}

		// Set entity reference
		Component->EntityId = ENTITY_INDEX_INVALID;

		// Set entity factory reference
        Component->EntityFactory = ENTITY_TYPE_INVALID;
	}
}
