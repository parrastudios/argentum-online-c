/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATABASE_STORED_PROCEDURE_ARGUMENT_H
#define DATABASE_STORED_PROCEDURE_ARGUMENT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <Preprocessor/For.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define STORED_PROCEDURE_ARGUMENT_SIZE			UINTEGER_SUFFIX(0xFF)
#define STORED_PROCEDURE_ARGUMENT_STRING_SIZE	UINTEGER_SUFFIX(0xFF)
#define STORED_PROCEDURE_ARGUMENT_BLOB_SIZE		UINTEGER_SUFFIX(0xFF)

////////////////////////////////////////////////////////////
// Enumerations
////////////////////////////////////////////////////////////
enum StoredProcedureArgumentPassing
{
	STORED_PROCEDURE_BY_VALUE		= 0x00,
	STORED_PROCEDURE_BY_REFERENCE	= 0x01,
	STORED_PROCEDURE_BY_NULL		= 0x02
};

enum StoredProcedureArgumentTypeId
{
	STORED_PROCEDURE_TINY			= 0x00,
	STORED_PROCEDURE_SHORT			= 0x01,
	STORED_PROCEDURE_LONG			= 0x02,
	STORED_PROCEDURE_LONG_LONG		= 0x03,
	STORED_PROCEDURE_FLOAT			= 0x04,
	STORED_PROCEDURE_DOUBLE			= 0x05,
	STORED_PROCEDURE_DATE			= 0x06,
	STORED_PROCEDURE_TIME			= 0x07,
	STORED_PROCEDURE_STRING			= 0x08,
	STORED_PROCEDURE_BLOB			= 0x09,
	STORED_PROCEDURE_NULL			= 0x0A,

	STORED_PROCEDURE_COUNT,

	STORED_PROCEDURE_INVALID
};

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Set by value implementation macro
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentSetValueOperator(Value, Name, Data) \
	do \
	{ \
		Value.Name = Data; \
	} while (0)

////////////////////////////////////////////////////////////
/// Get by value implementation macro
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentGetValueOperator(Value, Name, Data) \
	do \
	{ \
		Data = Value->Name; \
	} while (0)

////////////////////////////////////////////////////////////
/// Property of value implementation macro (string)
////////////////////////////////////////////////////////////
#define StoredProcedureStringPropertyImpl(Operator, Value, Data, Len) \
	do \
	{ \
		Operator(Value, String.Ptr, Data); \
		Operator(Value, String.Length, Len); \
	} while (0)

////////////////////////////////////////////////////////////
/// Property of value implementation macro (tiny)
////////////////////////////////////////////////////////////
#define STORED_PROCEDURE_TINY_PROPERTY(Operator, Value, Data) \
	Operator(Value, Tiny, Data)

////////////////////////////////////////////////////////////
/// Property of value implementation macro (short)
////////////////////////////////////////////////////////////
#define STORED_PROCEDURE_SHORT_PROPERTY(Operator, Value, Data) \
	Operator(Value, Short, Data)

////////////////////////////////////////////////////////////
/// Property of value implementation macro (long)
////////////////////////////////////////////////////////////
#define STORED_PROCEDURE_LONG_PROPERTY(Operator, Value, Data) \
	Operator(Value, Long, Data)

////////////////////////////////////////////////////////////
/// Property of value implementation macro (long long)
////////////////////////////////////////////////////////////
#define STORED_PROCEDURE_LONG_LONG_PROPERTY(Operator, Value, Data) \
	Operator(Value, LongLong, Data)

////////////////////////////////////////////////////////////
/// Property of value implementation macro (float)
////////////////////////////////////////////////////////////
#define STORED_PROCEDURE_FLOAT_PROPERTY(Operator, Value, Data) \
	Operator(Value, Float, Data)

////////////////////////////////////////////////////////////
/// Property of value implementation macro (double)
////////////////////////////////////////////////////////////
#define STORED_PROCEDURE_DOUBLE_PROPERTY(Operator, Value, Data) \
	Operator(Value, Double, Data)

////////////////////////////////////////////////////////////
/// Property of value implementation macro (date)
////////////////////////////////////////////////////////////
#define STORED_PROCEDURE_DATE_PROPERTY(Operator, Value, Data, Length) \
	StoredProcedureStringPropertyImpl(Operator, Value, Data, Length)

////////////////////////////////////////////////////////////
/// Property of value implementation macro (time)
////////////////////////////////////////////////////////////
#define STORED_PROCEDURE_TIME_PROPERTY(Operator, Value, Data) \
	StoredProcedureStringPropertyImpl(Operator, Value, Data, Length)

////////////////////////////////////////////////////////////
/// Property of value implementation macro (string)
////////////////////////////////////////////////////////////
#define STORED_PROCEDURE_STRING_PROPERTY(Operator, Value, Data, Length) \
	StoredProcedureStringPropertyImpl(Operator, Value, Data, Length)

////////////////////////////////////////////////////////////
/// Property of value implementation macro (blob)
////////////////////////////////////////////////////////////
#define STORED_PROCEDURE_BLOB_PROPERTY(Operator, Value, Data, Bytes) \
	do \
	{ \
		Operator(Value, Blob.Ptr, Data); \
		Operator(Value, Blob.Size, Bytes); \
	} while (0)

////////////////////////////////////////////////////////////
/// Property of value implementation macro (null)
////////////////////////////////////////////////////////////
#define STORED_PROCEDURE_NULL_PROPERTY(Operator, Value, ...) do { __VA_ARGS__ } while (0)

////////////////////////////////////////////////////////////
/// Property of an argument by value (implementation)
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentPropertyValueImpl(Property, Tuple) \
	Property Tuple

////////////////////////////////////////////////////////////
/// Property of an argument by value
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentPropertyValue(Operator, Value, Type, ...) \
	StoredProcedureArgumentPropertyValueImpl(PREPROCESSOR_CONCAT(Type, _PROPERTY), (Operator, Value, __VA_ARGS__))

////////////////////////////////////////////////////////////
/// Set an argument by value
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentSetValueImpl(Value, Type, ...) \
	StoredProcedureArgumentPropertyValue(StoredProcedureArgumentSetValueOperator, Value, Type, __VA_ARGS__)

////////////////////////////////////////////////////////////
/// Get an argument by value
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentGetValueImpl(Value, Type, ...) \
	StoredProcedureArgumentPropertyValue(StoredProcedureArgumentGetValueOperator, Value, Type, __VA_ARGS__)

////////////////////////////////////////////////////////////
/// Callback implementation of an argument list by tuples (ref)
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentSetBuildRefImpl(Builder, Type, Data) \
	StoredProcedureArgumentSetBuildRefCallback(Builder, Type, (StoredProcedureArgumentDataRefType)(Data));

////////////////////////////////////////////////////////////
/// Callback implementation of an argument list by tuples (val)
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentSetBuildValueImpl(Builder, Type, ...) \
	do \
	{ \
		union StoredProcedureArgumentDataValueType Value; \
		\
		StoredProcedureArgumentSetValueImpl(Value, Type, __VA_ARGS__); \
		\
		StoredProcedureArgumentSetBuildValueCallback(Builder, Type, &Value); \
		\
	} while (0);

////////////////////////////////////////////////////////////
/// Implementation to construct an argument list by tuples
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentBuildImpl(Function, Arguments, ...) \
	do \
	{ \
		struct StoredProcedureArgumentBuilder Builder; \
		\
		StoredProcedureArgumentBuildCreate(&Builder, Arguments); \
		\
		PREPROCESSOR_FOR_EACH_TUPLE_PREPEND(Function, &Builder, __VA_ARGS__) \
		\
	} while (0)

////////////////////////////////////////////////////////////
/// Macro for construct an argument list by tuples (ref)
///
/// StoredProcedureArgumentSetBuildRef(ArgumentList,
///		(STORED_PROCEDURE_FLOAT, &MyFloat),
///		(STORED_PROCEDURE_LONG, &MyLong));
///
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentSetBuildRef(Arguments, ...) \
	StoredProcedureArgumentBuildImpl(StoredProcedureArgumentSetBuildRefImpl, Arguments, __VA_ARGS__)

////////////////////////////////////////////////////////////
/// Macro for construct an argument list by tuples (value)
///
/// StoredProcedureArgumentSetBuildValue(ArgumentList,
///		(STORED_PROCEDURE_FLOAT, MyFloat),
///		(STORED_PROCEDURE_LONG, MyLong),
///		(STORED_PROCEDURE_FLOAT, 344.0f),
///		(STORED_PROCEDURE_TINY, '@'),
///		(STORED_PROCEDURE_STRING, MyString, MyStringLength),
///		(STORED_PROCEDURE_BLOB, MyBlob, MyBlobSize));
///
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentSetBuildValue(Arguments, ...) \
	StoredProcedureArgumentBuildImpl(StoredProcedureArgumentSetBuildValueImpl, Arguments, __VA_ARGS__)

////////////////////////////////////////////////////////////
/// Callback implementation of an argument list by tuples (ref)
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentGetBuildRefImpl(Builder, Type, Data) \
	StoredProcedureArgumentGetBuildRefCallback(Builder, Type, (StoredProcedureArgumentDataRefType *)&Data);

////////////////////////////////////////////////////////////
/// Callback implementation of an argument list by tuples (val)
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentGetBuildValueImpl(Builder, Type, ...) \
	do \
	{ \
		union StoredProcedureArgumentDataValueType * Value; \
		\
		StoredProcedureArgumentGetBuildValueCallback(Builder, Type, &Value); \
		\
		StoredProcedureArgumentGetValueImpl(Value, Type, __VA_ARGS__); \
		\
	} while (0);

////////////////////////////////////////////////////////////
/// Macro for retrieve an argument list by tuples (ref)
///
/// StoredProcedureArgumentGetBuildValue(ArgumentList,
///		(STORED_PROCEDURE_FLOAT, MyFloatPtr),
///		(STORED_PROCEDURE_LONG, MyLongPtr),
///		(STORED_PROCEDURE_STRING, MyStringPtr));
///
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentGetBuildRef(Arguments, ...) \
	StoredProcedureArgumentBuildImpl(StoredProcedureArgumentGetBuildRefImpl, Arguments, __VA_ARGS__)

////////////////////////////////////////////////////////////
/// Macro for retrieve an argument list by tuples (value)
///
/// StoredProcedureArgumentGetBuildRef(ArgumentList,
///		(STORED_PROCEDURE_FLOAT, MyFloat),
///		(STORED_PROCEDURE_LONG, MyLong)
///		(STORED_PROCEDURE_STRING, MyStringPtr, Length));
///
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentGetBuildValue(Arguments, ...) \
	StoredProcedureArgumentBuildImpl(StoredProcedureArgumentGetBuildValueImpl, Arguments, __VA_ARGS__)

////////////////////////////////////////////////////////////
/// Macro for retrieve passing type of a stored procedure arg
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentGetBuildValueTypeOf()	STORED_PROCEDURE_BY_VALUE
#define StoredProcedureArgumentSetBuildValueTypeOf()	STORED_PROCEDURE_BY_VALUE
#define StoredProcedureArgumentGetBuildRefTypeOf()		STORED_PROCEDURE_BY_REFERENCE
#define StoredProcedureArgumentSetBuildRefTypeOf()		STORED_PROCEDURE_BY_REFERENCE

////////////////////////////////////////////////////////////
/// Macro for retrieve an argument list by tuples (value)
///
/// StoredProcedureArgumentBuild(
///		StoredProcedureArgument{ Get | Set }Build{ Value | Ref }
///		ArgumentList,
///		(STORED_PROCEDURE_FLOAT, MyFloat),
///		(STORED_PROCEDURE_LONG, MyLong)
///		(STORED_PROCEDURE_STRING, MyStringPtr, Length));
///
////////////////////////////////////////////////////////////
#define StoredProcedureArgumentBuild(Build, Arguments, ...) \
	do \
	{ \
		Arguments = StoredProcedureArgumentCreate(PREPROCESSOR_CONCAT(Build, TypeOf)(), PREPROCESSOR_ARGS_COUNT(__VA_ARGS__)); \
		\
		Build(&Arguments, __VA_ARGS__); \
	} while (0)

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
union StoredProcedureArgumentDataValueType;
struct StoredProcedureArgumentType;
struct StoredProcedureArgumentListType;

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef void * StoredProcedureArgumentDataRefType;
typedef union StoredProcedureArgumentDataValueType StoredProcedureArgumentDataValueType;
typedef unsigned long StoredProcedureArgumentSizeType;
typedef unsigned long StoredProcedureArgumentLengthType;
typedef struct StoredProcedureArgumentType * StoredProcedureArgument;
typedef struct StoredProcedureArgumentListType * StoredProcedureArgumentList;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct StoredProcedureArgumentDataStringType
{
	StoredProcedureArgumentLengthType Length;
	char * Ptr;
};

struct StoredProcedureArgumentDataBlobType
{
	StoredProcedureArgumentLengthType Size;
	int8 * Ptr;
};

union StoredProcedureArgumentDataValueType
{
	int8 Tiny;
	int16 Short;
	int32 Long;
	int64 LongLong;
	float32 Float;
	float64 Double;
	struct StoredProcedureArgumentDataStringType String;
	struct StoredProcedureArgumentDataBlobType Blob;
};

struct StoredProcedureArgumentBuilder
{
	StoredProcedureArgumentList					Arguments;		//< Argument list pointer
	uinteger									Count;			//< Argument count iterator
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
/// Initialize a stored procedure argument value
/////////////////////////////////////////////////////////////
void * StoredProcedureArgumentValueInitialize(union StoredProcedureArgumentDataValueType * Value, enum StoredProcedureArgumentTypeId Type);

/////////////////////////////////////////////////////////////
/// Return size of an argument type
/////////////////////////////////////////////////////////////
StoredProcedureArgumentSizeType StoredProcedureArgumentSizeOf(enum StoredProcedureArgumentTypeId ArgumentType);

/////////////////////////////////////////////////////////////
/// Return length of an argument type if any
/////////////////////////////////////////////////////////////
StoredProcedureArgumentLengthType * StoredProcedureArgumenValueLength(union StoredProcedureArgumentDataValueType * Value, enum StoredProcedureArgumentTypeId Type);

/////////////////////////////////////////////////////////////
/// Create stored procedure argument list
/////////////////////////////////////////////////////////////
StoredProcedureArgumentList StoredProcedureArgumentCreate(enum StoredProcedureArgumentPassing Passing, uinteger Size);

/////////////////////////////////////////////////////////////
/// Copy stored procedure argument list
/////////////////////////////////////////////////////////////
StoredProcedureArgumentList StoredProcedureArgumentCopy(StoredProcedureArgumentList Arguments);

/////////////////////////////////////////////////////////////
/// Get procedure argument list size
/////////////////////////////////////////////////////////////
uinteger StoredProcedureArgumentSize(StoredProcedureArgumentList Arguments);

/////////////////////////////////////////////////////////////
/// Get argument type from procedure argument list
/////////////////////////////////////////////////////////////
enum StoredProcedureArgumentTypeId StoredProcedureArgumentListGetType(StoredProcedureArgumentList Arguments, uinteger Parameter);

/////////////////////////////////////////////////////////////
/// Set argument type from procedure argument list
/////////////////////////////////////////////////////////////
void StoredProcedureArgumentListSetType(StoredProcedureArgumentList Arguments, uinteger Parameter, enum StoredProcedureArgumentTypeId Type);

/////////////////////////////////////////////////////////////
/// Destroy stored procedure argument list
/////////////////////////////////////////////////////////////
void StoredProcedureArgumentDestroy(StoredProcedureArgumentList Arguments);

/////////////////////////////////////////////////////////////
/// Set stored procedure argument list by reference
/////////////////////////////////////////////////////////////
void StoredProcedureArgumentSetRef(StoredProcedureArgumentList Arguments, uinteger Parameter, enum StoredProcedureArgumentTypeId Type, StoredProcedureArgumentDataRefType Data);

/////////////////////////////////////////////////////////////
/// Set stored procedure argument list by value
/////////////////////////////////////////////////////////////
void StoredProcedureArgumentSetValue(StoredProcedureArgumentList Arguments, uinteger Parameter, enum StoredProcedureArgumentTypeId Type, StoredProcedureArgumentDataValueType * Data);

/////////////////////////////////////////////////////////////
/// Get stored procedure argument list by reference
/////////////////////////////////////////////////////////////
StoredProcedureArgumentDataRefType StoredProcedureArgumentGetRef(StoredProcedureArgumentList Arguments, uinteger Parameter, enum StoredProcedureArgumentTypeId Type);

/////////////////////////////////////////////////////////////
/// Get stored procedure argument list by value
/////////////////////////////////////////////////////////////
StoredProcedureArgumentDataValueType * StoredProcedureArgumentGetValue(StoredProcedureArgumentList Arguments, uinteger Parameter, enum StoredProcedureArgumentTypeId Type);

/////////////////////////////////////////////////////////////
/// Create builder for stored procedure argument list
/////////////////////////////////////////////////////////////
void StoredProcedureArgumentBuildCreate(struct StoredProcedureArgumentBuilder * Builder, StoredProcedureArgumentList Arguments);

/////////////////////////////////////////////////////////////
/// Build set callback for stored procedure argument list by ref
/////////////////////////////////////////////////////////////
struct StoredProcedureArgumentBuilder * StoredProcedureArgumentSetBuildRefCallback(struct StoredProcedureArgumentBuilder * Builder, enum StoredProcedureArgumentTypeId Type, StoredProcedureArgumentDataRefType Data);

/////////////////////////////////////////////////////////////
/// Build set callback for stored procedure argument list by val
/////////////////////////////////////////////////////////////
struct StoredProcedureArgumentBuilder * StoredProcedureArgumentSetBuildValueCallback(struct StoredProcedureArgumentBuilder * Builder, enum StoredProcedureArgumentTypeId Type, StoredProcedureArgumentDataValueType * Data);

/////////////////////////////////////////////////////////////
/// Build get callback for stored procedure argument list by ref
/////////////////////////////////////////////////////////////
struct StoredProcedureArgumentBuilder * StoredProcedureArgumentGetBuildRefCallback(struct StoredProcedureArgumentBuilder * Builder, enum StoredProcedureArgumentTypeId Type, StoredProcedureArgumentDataRefType * Data);

/////////////////////////////////////////////////////////////
/// Build get callback for stored procedure argument list by val
/////////////////////////////////////////////////////////////
struct StoredProcedureArgumentBuilder * StoredProcedureArgumentGetBuildValueCallback(struct StoredProcedureArgumentBuilder * Builder, enum StoredProcedureArgumentTypeId Type, StoredProcedureArgumentDataValueType ** Data);

#endif // DATABASE_STORED_PROCEDURE_ARGUMENT_H
