/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Plugin/PluginLoader.h>
#include <Plugin/PluginLoaderImplLibrary.h>
#include <Plugin/PluginLoaderImplScript.h>

#include <System/IOHelper.h> // todo: refactor into String module

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PLUGIN_LOADER_POOL_SIZE			0xFFUL

typedef struct PluginLoaderImplType * (*PluginLoaderImplGet)();

PluginLoaderImplGet PluginLoaderList[PLUGIN_LOADER_TYPE_SIZE] =
{
	ArrayInit(PLUGIN_LOADER_TYPE_LIBRARY, &PluginLoaderImplLibraryGet),
	ArrayInit(PLUGIN_LOADER_TYPE_SCRIPT, &PluginLoaderImplScriptGet)
};

typedef uinteger PluginLoaderResourceIndex;

#define PluginLoaderResourceRelease(Resource) \
	do { \
		Resource->Impl = NULL; \
		Resource->Id[0] = NULLCHAR; \
		Resource->Type = PLUGIN_LOADER_TYPE_INVALID; \
	} while (0)

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct PluginLoaderResourceType
{
	PluginId						Id;				///< Pointer to id of the plugin
	PluginLoaderTypeInfo			Type;			///< Type of the plugin
	PluginLoaderImplResourceType	Impl;			///< Handle of the implementation resource
};

struct
{
	struct PluginLoaderResourceType	Pool[PLUGIN_LOADER_POOL_SIZE];		///< Pool of resources (pointed by the resource list in implementation loader)
	uinteger						Count;								///< Number of pool ids ocupated
	List							FreeResourceList;					///< List of references to free ids in the pool

} PluginLoaderResourceTable;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderInitialize();

////////////////////////////////////////////////////////////
/// Destroy plugin loader
////////////////////////////////////////////////////////////
void PluginLoaderDestroy();

////////////////////////////////////////////////////////////
/// Load a plugin from plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderLoad(PluginId Id, struct PluginType * Plugin);

////////////////////////////////////////////////////////////
/// Load a whole path of plugins from plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderLoadPath(const char * Path, List LoadedIdList);

////////////////////////////////////////////////////////////
/// Unload a plugin from plugin loader
////////////////////////////////////////////////////////////
void PluginLoaderUnload(PluginId Id);

////////////////////////////////////////////////////////////
/// Clear all plugins from plugin loader
////////////////////////////////////////////////////////////
void PluginLoaderClear();

////////////////////////////////////////////////////////////
/// Clear plugin loader resource pool
////////////////////////////////////////////////////////////
void PluginLoaderResourcePoolClear()
{
	uinteger i;

	// Clear list
	ListClear(PluginLoaderResourceTable.FreeResourceList);

	// Clear pool
	PluginLoaderResourceTable.Count = 0;

	for (i = 0; i < PLUGIN_LOADER_POOL_SIZE; ++i)
	{
		struct PluginLoaderResourceType * Resource = &PluginLoaderResourceTable.Pool[i];

		PluginLoaderResourceRelease(Resource);
	}
}

////////////////////////////////////////////////////////////
/// Initialize plugin loader resource pool
////////////////////////////////////////////////////////////
bool PluginLoaderResourcePoolInitialize()
{
	// Initialize free list
	PluginLoaderResourceTable.FreeResourceList = ListNew(sizeof(PluginLoaderResourceIndex));

	if (PluginLoaderResourceTable.FreeResourceList)
	{
		// Initialize the pool
		PluginLoaderResourcePoolClear();

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Destroy plugin loader resource pool
////////////////////////////////////////////////////////////
void PluginLoaderResourcePoolDestroy()
{
	// Clear pool
	PluginLoaderResourcePoolClear();

	// Destroy free list
	ListDestroy(PluginLoaderResourceTable.FreeResourceList);
}

////////////////////////////////////////////////////////////
/// Initialize plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderInitialize()
{
	PluginLoaderTypeInfo Type;
	uinteger InvalidLoaderCount = 0;

	for (Type = 0; Type < PLUGIN_LOADER_TYPE_SIZE; ++Type)
	{
		struct PluginLoaderImplType * Loader = PluginLoaderList[Type]();

		// Create resource map
		Loader->ResourceList = HashMapCreate(HashGetCallbackStringDefault(), CompareGetCallback(COMPARE_FUNC_CSTRING));

		if (Loader->ResourceList)
		{
			// Create loader implementation
			if (!Loader->Initialize(Loader))
			{
				// Handle error
				// ..
				++InvalidLoaderCount;

				Loader->Initialized = false;

				// Clear resource list
				HashMapDestroy(Loader->ResourceList);
			}
			else
			{
				Loader->Initialized = true;
			}
		}
		else
		{
			// Handle error
			// ..
			++InvalidLoaderCount;

			Loader->Initialized = false;
		}
	}

	// At least some loader valid
	if (InvalidLoaderCount < PLUGIN_LOADER_TYPE_SIZE)
	{
		return PluginLoaderResourcePoolInitialize();
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Destroy plugin loader
////////////////////////////////////////////////////////////
void PluginLoaderDestroy()
{
	PluginLoaderTypeInfo Type;

	// Clear all plugins
	PluginLoaderClear();

	// Destroy implementations
	for (Type = 0; Type < PLUGIN_LOADER_TYPE_SIZE; ++Type)
	{
		struct PluginLoaderImplType * Loader = PluginLoaderList[Type]();

		if (Loader->Initialized)
		{
			// Destroy loader implementation
			Loader->Destroy(Loader);

			Loader->Initialized = false;

			// Clear resource list
			HashMapDestroy(Loader->ResourceList);
		}
	}

	// Destroy the resource pool
	PluginLoaderResourcePoolDestroy();
}

////////////////////////////////////////////////////////////
/// Get resource from the pool
////////////////////////////////////////////////////////////
struct PluginLoaderResourceType * PluginLoaderResourceGet(PluginId Id)
{
	PluginLoaderTypeInfo Type;
	struct PluginLoaderResourceType * Resource = NULL;

	// Check if it is already loaded
	for (Type = 0; Type < PLUGIN_LOADER_TYPE_SIZE; ++Type)
	{
		struct PluginLoaderImplType * Loader = PluginLoaderList[Type]();

		if (Loader->Initialized)
		{
			// Get from resource list
			Resource = HashMapGet(Loader->ResourceList, (HashKeyData)Id);

			// If already loaded
			if (Resource)
			{
				// Check if resource is valid
				if ((strcmp(Resource->Id, Id) == 0) && Resource->Type == Type && Resource->Impl != NULL)
				{
					return Resource;
				}

				Resource = NULL;
			}
		}
	}

	return Resource;
}

////////////////////////////////////////////////////////////
/// Register resource library into the plugin
////////////////////////////////////////////////////////////
bool PluginLoaderRegister(struct PluginLoaderResourceType * Resource, struct PluginType * Plugin)
{
	struct PluginLoaderImplType * Loader = PluginLoaderList[Resource->Type]();

	// Hold a reference to the resource id
	// strncpy(Plugin->Id, Resource->Id, PLUGIN_ID_LENGTH);
	strcpy(Plugin->Id, Resource->Id);

	// ..

	// Register the plugin
	return Loader->Register(Loader, Resource->Impl, Plugin);
}

////////////////////////////////////////////////////////////
/// Load a plugin from plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderLoad(PluginId Id, struct PluginType * Plugin)
{
	PluginLoaderResourceIndex Index;
	PluginLoaderTypeInfo Type;

	// Get resoure if it is already loaded
	struct PluginLoaderResourceType * Resource = PluginLoaderResourceGet(Id);

	// Return if resource is already loaded
	if (Resource)
	{
		// ..

		return true;
	}

	// Check if the pool is full
	if (PluginLoaderResourceTable.Count == (PLUGIN_LOADER_POOL_SIZE - 1) && ListEmpty(PluginLoaderResourceTable.FreeResourceList))
	{
		// ..

		return false;
	}

	// Resource not loaded, get one from the pool
	if (!ListEmpty(PluginLoaderResourceTable.FreeResourceList))
	{
		// Get from the free list
		Index = (PluginLoaderResourceIndex)ListFront(PluginLoaderResourceTable.FreeResourceList);

		ListPopFront(PluginLoaderResourceTable.FreeResourceList);

		if (Index < PLUGIN_LOADER_POOL_SIZE)
		{
			// Get from the pool
			Resource = &PluginLoaderResourceTable.Pool[Index];
		}
	}
	else
	{
		// Get directly from the pool
		Index = PluginLoaderResourceTable.Count;

		++PluginLoaderResourceTable.Count;

		Resource = &PluginLoaderResourceTable.Pool[Index];
	}

	// Check if resource has been allocated
	if (Resource == NULL)
	{
		// ..

		return false;
	}

	// Load the plugin
	for (Type = 0; Type < PLUGIN_LOADER_TYPE_SIZE; ++Type)
	{
		struct PluginLoaderImplType * Loader = PluginLoaderList[Type]();

		// Try to load it
		if (Loader->Load(Loader, Id, &Resource->Impl))
		{
			// Set values properly
			// strncpy(Resource->Id, Id, PLUGIN_ID_LENGTH);
			strcpy(Resource->Id, Id);
			Resource->Type = Type;

			// Insert to the map and register library into the plugin
			if (HashMapInsert(Loader->ResourceList, (HashKeyData)Resource->Id, (void *)Resource))
			{
				// Register plugin
				if (PluginLoaderRegister(Resource, Plugin))
				{
					return true;
				}
				else
				{
					// Remove from resource list
					HashMapRemove(Loader->ResourceList, (HashKeyData)Resource->Id);

					// Invalid insertion, delete implementation
					Loader->Unload(Loader, Resource->Impl);

					// Release resource
					PluginLoaderResourceRelease(Resource);
				}

			}
			else
			{
				// Invalid insertion, delete implementation
				Loader->Unload(Loader, Resource->Impl);

				// Release resource
				PluginLoaderResourceRelease(Resource);
			}
		}
	}

	// Resource has not been loaded, remove from the pool
	if (Index == (PluginLoaderResourceTable.Count - 1))
	{
		--PluginLoaderResourceTable.Count;
	}
	else
	{
		ListPushBack(PluginLoaderResourceTable.FreeResourceList, (void *)&Index);
	}

	Resource = &PluginLoaderResourceTable.Pool[Index];

	// Release resource
	PluginLoaderResourceRelease(Resource);

	return false;
}

////////////////////////////////////////////////////////////
/// Load a whole path of plugins from plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderLoadPath(const char * Path, List LoadedIdList)
{
	// ..

	return false;
}

////////////////////////////////////////////////////////////
/// Unload a plugin from plugin loader
////////////////////////////////////////////////////////////
void PluginLoaderUnload(PluginId Id)
{
	// Get resoure if it is already loaded
	struct PluginLoaderResourceType * Resource = PluginLoaderResourceGet(Id);

	if (Resource)
	{
		struct PluginLoaderImplType * Loader = PluginLoaderList[Resource->Type]();

		// Unload implementation
		if (Loader->Unload(Loader, Resource->Impl))
		{
			// Remove from resource list
			if (HashMapRemove(Loader->ResourceList, (HashKeyData)Resource->Id))
			{
				// Get the index from resource pointer
				PluginLoaderResourceIndex Index = ((uinteger)(Resource) - (uinteger)&PluginLoaderResourceTable.Pool[0]) / sizeof(struct PluginLoaderResourceType);

				if (Resource == &PluginLoaderResourceTable.Pool[Index])
				{
					// Remove from pool
					if (Index == (PluginLoaderResourceTable.Count - 1))
					{
						--PluginLoaderResourceTable.Count;
					}
					else
					{
						ListPushBack(PluginLoaderResourceTable.FreeResourceList, (void *)&Index);
					}

					// Release resource
					PluginLoaderResourceRelease(Resource);
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Clear all plugins from plugin loader
////////////////////////////////////////////////////////////
void PluginLoaderClear()
{
	uinteger i;
	PluginLoaderTypeInfo Type;

	// Unload the pool
	for (i = 0; i < PLUGIN_LOADER_POOL_SIZE; ++i)
	{
		struct PluginLoaderResourceType * Resource = &PluginLoaderResourceTable.Pool[i];

		if (Resource->Impl != NULL)
		{
			struct PluginLoaderImplType * Loader = PluginLoaderList[Resource->Type]();

			// Unload resource implementation
			if (!Loader->Unload(Loader, Resource->Impl))
			{
				// Handle error
			}
		}

		// Release resource
		PluginLoaderResourceRelease(Resource);
	}

	// Clear list
	ListClear(PluginLoaderResourceTable.FreeResourceList);

	// For each loader
	for (Type = 0; Type < PLUGIN_LOADER_TYPE_SIZE; ++Type)
	{
		struct PluginLoaderImplType * Loader = PluginLoaderList[Type]();

		if (Loader->Initialized)
		{
			// Clear implementation
			Loader->Clear(Loader);

			// Clear resource list
			HashMapClear(Loader->ResourceList);
		}
	}
}

////////////////////////////////////////////////////////////
/// Get instance of plugin loader singleton
////////////////////////////////////////////////////////////
PluginLoaderType * PluginLoaderGetInstance()
{
	static PluginLoaderType PluginLoaderTypeInstance = {
		TypeInit(Initialize, &PluginLoaderInitialize),
		TypeInit(Destroy, &PluginLoaderDestroy),
		TypeInit(Load, &PluginLoaderLoad),
		TypeInit(LoadPath, &PluginLoaderLoadPath),
		TypeInit(Unload, &PluginLoaderUnload),
		TypeInit(Clear, &PluginLoaderClear)
	};

	return &PluginLoaderTypeInstance;
}
