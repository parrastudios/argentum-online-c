/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Quaternion.h>

void QuaternionSet(struct Quaternion * Quat, float x, float y, float z, float w)
{
	Quat->x = x;
	Quat->y = y;
	Quat->z = z;
	Quat->w = w;
}

void QuaternionCopy(struct Quaternion * Dest, struct Quaternion * Source)
{
	if (Dest && Source)
	{
		Dest->x = Source->x;
		Dest->y = Source->y;
		Dest->z = Source->z;
		Dest->w = Source->w;
	}
}

bool QuaternionIsEqual(struct Quaternion * Left, struct Quaternion * Right)
{
	if (Left && Right)
	{
		float Distance = QuaternionDotProduct(Left, Right);
		const float Epsilon = 0.000000001f;

		return (fabsf(Distance - 1.0f) < Epsilon);
	}

	return false;
}

void QuaternionIdentity(struct Quaternion * Quat)
{
	Quat->w = 1.0f, Quat->x = Quat->y = Quat->z = 0.0f;
}

void QuaternionConjugate(struct Quaternion * Src, struct Quaternion * Dest)
{
	Dest->x = -Src->x;
	Dest->y = -Src->y;
	Dest->z = -Src->z;
	Dest->w = Src->w;
}

void QuaternionInverse(struct Quaternion * Quat)
{
	float Magnitude = QuaternionMagnitude(Quat);

	if (Magnitude > 0.0f)
	{
		float InverseMagnitude = -1.0f / Magnitude;
		
		Quat->x *= InverseMagnitude;
		Quat->y *= InverseMagnitude;
		Quat->z *= InverseMagnitude;
		Quat->w *= -InverseMagnitude;
	}
}

float QuaternionDotProduct(struct Quaternion * Q1, struct Quaternion * Q2)
{
	return (float)((Q1->x * Q2->x) + (Q1->y * Q2->y) + (Q1->z * Q2->z) + (Q1->w * Q2->w));
}

float QuaternionMagnitude(struct Quaternion * Quat)
{
	return (float)sqrt(QuaternionDotProduct(Quat, Quat));
}

void QuaternionComputeW(struct Quaternion * Quat)
{
	float t = 1.0f - (Quat->x * Quat->x) - (Quat->y * Quat->y) - (Quat->z * Quat->z);

	if (t < 0.0f)
	{
		Quat->w = 0.0f;
	}
	else
	{
		Quat->w = (float)-sqrt(t);
	}
}

void QuaternionNormalize(struct Quaternion * Quat)
{
	float Magnitude = QuaternionMagnitude(Quat);

	// Protect zero division
	if (Magnitude > 0.0f)
	{
		// Normalize it
		float MagInverted = 1.0f / (float)Magnitude;

		Quat->x *= MagInverted;
		Quat->y *= MagInverted;
		Quat->z *= MagInverted;
		Quat->w *= MagInverted;
	}
}

void QuaternionMultQuaternion(struct Quaternion * Q1, struct Quaternion * Q2, struct Quaternion * Result)
{
    // Multiply so that rotations are applied in a right to left order
	Result->x = (Q1->x * Q2->w) + (Q1->w * Q2->x) + (Q1->y * Q2->z) - (Q1->z * Q2->y);
	Result->y = (Q1->y * Q2->w) + (Q1->w * Q2->y) + (Q1->z * Q2->x) - (Q1->x * Q2->z);
	Result->z = (Q1->z * Q2->w) + (Q1->w * Q2->z) + (Q1->x * Q2->y) - (Q1->y * Q2->x);
	Result->w = (Q1->w * Q2->w) - (Q1->x * Q2->x) - (Q1->y * Q2->y) - (Q1->z * Q2->z);
}

void QuaternionMultQuaternionEx(struct Quaternion * Q1, struct Quaternion * Q2, struct Quaternion * Result)
{
    // Multiply so that rotations are applied in a left to right order
	Result->x = (Q1->w * Q2->w) - (Q1->x * Q2->x) - (Q1->y * Q2->y) - (Q1->z * Q2->z);
	Result->y = (Q1->w * Q2->x) + (Q1->x * Q2->w) - (Q1->y * Q2->z) + (Q1->z * Q2->y);
	Result->z = (Q1->w * Q2->y) + (Q1->x * Q2->z) + (Q1->y * Q2->w) - (Q1->z * Q2->x);
	Result->w = (Q1->w * Q2->z) - (Q1->x * Q2->y) + (Q1->y * Q2->x) + (Q1->z * Q2->w);
}

void QuaternionMultVector3f(struct Quaternion * Quat, struct Vector3f * Vec, struct Quaternion * Result)
{
	Result->w = - (Quat->x * Vec->x) - (Quat->y * Vec->y) - (Quat->z * Vec->z);
	Result->x =   (Quat->w * Vec->x) + (Quat->y * Vec->z) - (Quat->z * Vec->y);
	Result->y =   (Quat->w * Vec->y) + (Quat->z * Vec->x) - (Quat->x * Vec->z);
	Result->z =   (Quat->w * Vec->z) + (Quat->x * Vec->y) - (Quat->y * Vec->x);
}

void QuaternionRotatePoint(struct Quaternion * Quat, struct Vector3f * Src, struct Vector3f * Dest)
{
	struct Quaternion Tmp, Inv, Final;

	QuaternionConjugate(Quat, &Inv);

	QuaternionNormalize(&Inv);

	QuaternionMultVector3f(Quat, Src, &Tmp);

	QuaternionMultQuaternion(&Tmp, &Inv, &Final);

	Dest->x = Final.x;
	Dest->y = Final.y;
	Dest->z = Final.z;
}

void QuaternionSlerp(struct Quaternion * Q1, struct Quaternion * Q2, float Delta, struct Quaternion * Result)
{
	float CosOmega;
	struct Quaternion QTemp;
	float k0, k1;

	// Check for out-of range parameter and return edge points if so
	if (Delta <= 0.0f)
	{
		QuaternionCopy(Result, Q1);
		return;
	}
	else if (Delta >= 1.0f)
	{
		QuaternionCopy(Result, Q2);
		return;
	}

	// Compute cosine of angle between quaternions using dot product
	CosOmega = QuaternionDotProduct(Q1, Q2);

	// If negative dot, use -QTemp. Two quaternions q and -q
	// represent the same rotation, but may produce different slerp.
	// We chose q or -q to rotate using the acute angle.
	QTemp.w = Q2->w;
	QTemp.x = Q2->x;
	QTemp.y = Q2->y;
	QTemp.z = Q2->z;

	if (CosOmega < 0.0f)
	{
		QTemp.w = -QTemp.w;
		QTemp.x = -QTemp.x;
		QTemp.y = -QTemp.y;
		QTemp.z = -QTemp.z;
		CosOmega = -CosOmega;
	}

	// We should have two unit quaternions, so dot should be <= 1.0f
	assert(CosOmega < 1.1f);

	// Compute interpolation fraction, checking for quaternions almost exactly the same
	if (CosOmega > 0.9999f)
	{
		// Very close - just use linear interpolation, which will protect again a divide by zero
		k0 = 1.0f - Delta;
		k1 = Delta;
	}
	else
	{
		// Compute the sin of the angle using the
		// trig identity sin^2(omega) + cos^2(omega) = 1
		float SinOmega = (float)sqrt(1.0f - (CosOmega * CosOmega));

		// Compute the angle from its sin and cosine
		float Omega = (float)atan2(SinOmega, CosOmega);

		// Compute inverse of denominator, so we only have to divide once
		float OneOverSinOmega = 1.0f / SinOmega;

		// Compute interpolation parameters
		k0 = (float)sin((1.0f - Delta) * Omega) * OneOverSinOmega;
		k1 = (float)sin(Delta * Omega) * OneOverSinOmega;
	}

	// Interpolate and return new quaternion
	Result->w = (k0 * Q1->w) + (k1 * QTemp.w);
	Result->x = (k0 * Q1->x) + (k1 * QTemp.x);
	Result->y = (k0 * Q1->y) + (k1 * QTemp.y);
	Result->z = (k0 * Q1->z) + (k1 * QTemp.z);
}

void QuaternionNlerp(struct Quaternion * Q1, struct Quaternion * Q2, float Delta, struct Quaternion * Result)
{
	if (Q1 && Q2 && Result)
	{
		float DeltaInverse = 1.0f - Delta;
		float CosOmega;

		// Check for out-of range parameter and return edge points if so
		if (Delta <= 0.0f)
		{
			QuaternionCopy(Result, Q1);
			return;
		}
		else if (Delta >= 1.0f)
		{
			QuaternionCopy(Result, Q2);
			return;
		}

		// Compute cosine of angle between quaternions using dot product
		CosOmega = QuaternionDotProduct(Q1, Q2);

		// Calculate interpolated quaternion
		if (CosOmega < 0.0f)
		{
			struct Quaternion Quat;

			QuaternionSet(&Quat, -Q2->x, -Q2->y, -Q2->z, -Q2->w);

			QuaternionSet(Result,
				DeltaInverse * Q1->x + Delta * Quat.x,
				DeltaInverse * Q1->y + Delta * Quat.y,
				DeltaInverse * Q1->z + Delta * Quat.z,
				DeltaInverse * Q1->w + Delta * Quat.w);
		}
		else
		{

			QuaternionSet(Result,
				DeltaInverse * Q1->x + Delta * Q2->x,
				DeltaInverse * Q1->y + Delta * Q2->y,
				DeltaInverse * Q1->z + Delta * Q2->z,
				DeltaInverse * Q1->w + Delta * Q2->w);
		}
		
		QuaternionNormalize(Result);
	}
}

void QuaternionIntegrate(struct Quaternion * Quat, struct Vector3f * Omega, float Delta, struct Quaternion * Result)
{
	if (Quat && Omega && Result)
	{
		struct Quaternion DeltaQuat;
		struct Vector3f Theta;
		float ThetaMagnitudeSqr, Scale;

		// Scale omega by a half delta
		Vector3fMultiplyEx(&Theta, Omega, Delta * 0.5f);

		// Calculate theta magnitude
		ThetaMagnitudeSqr = Vector3fLengthSquared(&Theta);

		if (ThetaMagnitudeSqr * ThetaMagnitudeSqr / 24.0f < 0.000001f)
		{
			DeltaQuat.w = 1.0f - ThetaMagnitudeSqr / 2.0f;

			Scale = 1.0f - ThetaMagnitudeSqr / 6.0f;
		}
		else
		{
			float ThetaMagnitude = sqrtf(ThetaMagnitudeSqr);

			DeltaQuat.w = cosf(ThetaMagnitude);

			if (ThetaMagnitude > 0.0f)
			{
				Scale = sinf(ThetaMagnitude) / ThetaMagnitude;
			}
			else
			{
				Scale = 0.0f;
			}
		}

		DeltaQuat.x = Theta.x * Scale;
		DeltaQuat.y = Theta.y * Scale;
		DeltaQuat.z = Theta.z * Scale;

		QuaternionMultQuaternion(&DeltaQuat, Quat, Result);
	}
}

void QuaternionFromEuler(struct Quaternion * Quat, float Pitch, float Yaw, float Roll)
{
	if (Quat)
	{
		float PitchRad = degtorad(Pitch) * 0.5f;
		float YawRad = degtorad(Yaw) * 0.5f;
		float RollRad = degtorad(Roll) * 0.5f;

		float SinPitch = (float)sinf(PitchRad);
		float SinYaw = (float)sinf(YawRad);
		float SinRoll = (float)sinf(RollRad);

		float CosPitch = (float)cosf(PitchRad);
		float CosYaw = (float)cosf(YawRad);
		float CosRoll = (float)cosf(RollRad);

		float CosOmega = CosPitch * CosYaw;
		float SinOmega = SinPitch * SinYaw;

		float CosTheta = CosPitch * SinYaw;
		float SinTheta = SinPitch * CosYaw;

		Quat->x = SinRoll * CosOmega - CosRoll * SinOmega;
		Quat->y = CosRoll * SinTheta + SinRoll * CosTheta;
		Quat->z = CosRoll * CosTheta - SinRoll * SinTheta;
		Quat->w = CosRoll * CosOmega + SinRoll * SinOmega;

		QuaternionNormalize(Quat);
	}
}

void QuaternionToEuler(struct Quaternion * Quat, struct Vector3f * Euler)
{
	float xx = sqr(Quat->x);
	float yy = sqr(Quat->y);
	float zz = sqr(Quat->z);
	float ww = sqr(Quat->w);

	float x = atan2f(2.0f * (Quat->y * Quat->z + Quat->x * Quat->w), - xx - yy + zz + ww);
	float y = asinf(-2.0f * (Quat->x * Quat->z - Quat->y * Quat->w));
	float z = atan2f(2.0f * (Quat->x * Quat->y + Quat->z * Quat->w), xx - yy - zz + ww);
	
	Euler->x = radtodeg(x);
	Euler->y = radtodeg(y);
	Euler->z = radtodeg(z);
}

void QuaternionFromAxis(struct Quaternion * Quat, struct Vector3f * Vector, float Angle)
{
	float Sin;
	struct Vector3f Vect;

	Angle *= 0.5f;

	Vector3fCopy(&Vect, Vector);
	Vector3fNormalize(&Vect);

	Sin = (float)sin(Angle);

	Quat->x = (Vect.x * Sin);
	Quat->y = (Vect.y * Sin);
	Quat->z = (Vect.z * Sin);
	Quat->w = (float)cos(Angle);
}

void QuaternionToMatrix(struct Quaternion * Quat, struct Matrix4f * Matrix)
{
	// Converts this quaternion to a rotation matrix.
	//
	//  | 1 - 2(y^2 + z^2)	2(xy + wz)			2(xz - wy)			0  |
	//  | 2(xy - wz)		1 - 2(x^2 + z^2)	2(yz + wx)			0  |
	//  | 2(xz + wy)		2(yz - wx)			1 - 2(x^2 + y^2)	0  |
	//  | 0					0					0					1  |

	float x2 = Quat->x + Quat->x;
	float y2 = Quat->y + Quat->y;
	float z2 = Quat->z + Quat->z;
	float xx = Quat->x * x2;
	float xy = Quat->x * y2;
	float xz = Quat->x * z2;
	float yy = Quat->y * y2;
	float yz = Quat->y * z2;
	float zz = Quat->z * z2;
	float wx = Quat->w * x2;
	float wy = Quat->w * y2;
	float wz = Quat->w * z2;

	Matrix->m[0][0] = 1.0f - (yy + zz);
	Matrix->m[0][1] = xy + wz;
	Matrix->m[0][2] = xz - wy;
	Matrix->m[0][3] = 0.0f;

	Matrix->m[1][0] = xy - wz;
	Matrix->m[1][1] = 1.0f - (xx + zz);
	Matrix->m[1][2] = yz + wx;
	Matrix->m[1][3] = 0.0f;

	Matrix->m[2][0] = xz + wy;
	Matrix->m[2][1] = yz - wx;
	Matrix->m[2][2] = 1.0f - (xx + yy);
	Matrix->m[2][3] = 0.0f;

	Matrix->m[3][0] = 0.0f;
	Matrix->m[3][1] = 0.0f;
	Matrix->m[3][2] = 0.0f;
	Matrix->m[3][3] = 1.0f;
}

void QuaternionFromMatrix(struct Quaternion * Quat, struct Matrix4f * Matrix)
{
    // Creates a quaternion from a rotation matrix.
    // The algorithm used is from Allan and Mark Watt's "Advanced
    // Animation and Rendering Techniques" (ACM Press 1992).

	float s = 0.0f;
    float q[4] = { 0.0f };
    float Trace = Matrix->m[0][0] + Matrix->m[1][1] + Matrix->m[2][2];

    if (Trace > 0.0f)
    {
        s = sqrtf(Trace + 1.0f);
        q[3] = s * 0.5f;
        s = 0.5f / s;
        q[0] = (Matrix->m[1][2] - Matrix->m[2][1]) * s;
        q[1] = (Matrix->m[2][0] - Matrix->m[0][2]) * s;
        q[2] = (Matrix->m[0][1] - Matrix->m[1][0]) * s;
    }
    else
    {
        int nxt[3] = {1, 2, 0};
        int i = 0, j = 0, k = 0;

        if (Matrix->m[1][1] > Matrix->m[0][0])
            i = 1;

        if (Matrix->m[2][2] > Matrix->m[i][i])
            i = 2;

        j = nxt[i];
        k = nxt[j];
        s = sqrtf((Matrix->m[i][i] - (Matrix->m[j][j] + Matrix->m[k][k])) + 1.0f);

        q[i] = s * 0.5f;
        s = 0.5f / s;
        q[3] = (Matrix->m[j][k] - Matrix->m[k][j]) * s;
        q[j] = (Matrix->m[i][j] + Matrix->m[j][i]) * s;
        q[k] = (Matrix->m[i][k] + Matrix->m[k][i]) * s;
    }

    Quat->x = q[0];
	Quat->y = q[1];
	Quat->z = q[2];
	Quat->w = q[3];
}

void QuaternionToAxisAngle(struct Quaternion * Quat, struct Vector3f * Axis, float * Angle)
{
	if (Quat)
	{
		if (Axis)
		{
			float Scale = (float)sqrt(Quat->x * Quat->x + Quat->y * Quat->y + Quat->z * Quat->z);

			if (Scale != 0.0f)
			{
				Axis->x = Quat->x / Scale;
				Axis->y = Quat->y / Scale;
				Axis->z = Quat->z / Scale;
			}
			else
			{
				Vector3fSetNull(Axis);
			}
		}

		if (Angle)
		{
			*Angle = (float)acos(Quat->w) * 2.0f;
		}
	}
}

void QuaternionFromAxisAngle(struct Quaternion * Quat, struct Vector3f * Axis, float Angle)
{
    float HalfTheta = degtorad(Angle) * 0.5f;
    float s = sinf(HalfTheta);

	Quat->w = cosf(HalfTheta);

    Quat->x = Axis->x * s;
    Quat->y = Axis->y * s;
    Quat->z = Axis->z * s;
}

//void QuaternionFromEulerEx(struct Quaternion * Result, struct Matrix4f * Matrix, float Heading, float Pitch, float Roll, bool Constrained)
void QuaternionFromEulerEx(struct Quaternion * Quat, struct Matrix4f * Matrix, struct Vector3f * EulerRotation, bool Constrained)
{
    // Construct a quaternion from an euler transformation. We do this rather
    // than use QuaternionFromHeadPitchRoll() to support constraining heading
    // changes to the world Y axis

	// x: Pitch
	// y: Heading
	// z: Roll

	struct Quaternion Rotation;
	struct Vector3f LocalXAxis, LocalYAxis, LocalZAxis;

	QuaternionIdentity(Quat);
	QuaternionIdentity(&Rotation);

	Vector3fSet(&LocalXAxis, Matrix->m[0][0], Matrix->m[0][1], Matrix->m[0][2]);
	Vector3fSet(&LocalYAxis, Matrix->m[1][0], Matrix->m[1][1], Matrix->m[1][2]);
	Vector3fSet(&LocalZAxis, Matrix->m[2][0], Matrix->m[2][1], Matrix->m[2][2]);

    // Calculate heading degrees
	if (EulerRotation->y != 0.0f)
	{
		struct Quaternion Result;

		if (Constrained)
		{
			struct Vector3f Axis;

			Vector3fSet(&Axis, 0.0f, 1.0f, 0.0f);

			QuaternionFromAxisAngle(&Rotation, &Axis, EulerRotation->y);
		}
		else
		{
			QuaternionFromAxisAngle(&Rotation, &LocalYAxis, EulerRotation->y);
		}

		QuaternionMultQuaternion(Quat, &Rotation, &Result);

		QuaternionCopy(Quat, &Result);
	}

    // Calculate pitch degrees
	if (EulerRotation->x != 0.0f)
	{
		struct Quaternion Result;

		QuaternionFromAxisAngle(&Rotation, &LocalXAxis, EulerRotation->x);

		QuaternionMultQuaternion(Quat, &Rotation, &Result);

		QuaternionCopy(Quat, &Result);
	}

    // Calculate roll degrees
	if (EulerRotation->z != 0.0f)
	{
		struct Quaternion Result;

		QuaternionFromAxisAngle(&Rotation, &LocalZAxis, EulerRotation->z);

		QuaternionMultQuaternion(Quat, &Rotation, &Result);

		QuaternionCopy(Quat, &Result);
	}
}
