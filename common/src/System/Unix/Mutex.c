/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/Mutex.h>

////////////////////////////////////////////////////////////
/// Default initializer
////////////////////////////////////////////////////////////
void MutexInitialize(Mutex * MutexEx)
{
#	ifdef THREADING_USE_MUTEX
		pthread_mutex_init(MutexEx, NULL);
#	else
		pthread_spin_init(MutexEx, PTHREAD_PROCESS_PRIVATE);
#	endif
}


////////////////////////////////////////////////////////////
/// Mutex destructor
////////////////////////////////////////////////////////////
void MutexDestroy(Mutex * MutexEx)
{
#	ifdef THREADING_USE_MUTEX
		pthread_mutex_destroy(MutexEx);
#	else
		pthread_spin_destroy(MutexEx);
#	endif
}


////////////////////////////////////////////////////////////
/// Lock the mutex
////////////////////////////////////////////////////////////
void MutexLock(Mutex * MutexEx)
{
#	ifdef THREADING_USE_MUTEX
		pthread_mutex_lock(MutexEx);
#	else
		pthread_spin_lock(MutexEx);
#	endif
}


////////////////////////////////////////////////////////////
/// Unlock the mutex
////////////////////////////////////////////////////////////
void MutexUnlock(Mutex * MutexEx)
{
#	ifdef THREADING_USE_MUTEX
		pthread_mutex_unlock(MutexEx);
#	else
		pthread_spin_unlock(MutexEx);
#	endif
}
