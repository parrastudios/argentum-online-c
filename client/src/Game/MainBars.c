/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/MainBars.h>

#include <Game/Language.h>
#include <Game/User.h>

#include <GUI/WindowManager.h>
#include <GUI/EventHandler.h>

#include <Network/Protocol.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define SOME_EVENT_FX_SOUND				"data/audio/1.ogg"

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct GuiWindow * MainBarsWindow;
struct GuiWidget * HealthBar, * PowerBar;
struct GuiWidget * FrontLayer, *TTable;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// HP button released event
////////////////////////////////////////////////////////////
void HP_ButtonReleased(struct MouseButtonEvent * ButtonEvent)
{
	if (PowerBar->Attributes.Shape.Percentage > 0.0f)
	{
		GuiShapeAppendPercent(&PowerBar->Attributes.Shape, -0.05f);
	}
	else
	{
		GuiShapeSetPercent(&PowerBar->Attributes.Shape, 1.0f);
	}

	if (HealthBar->Attributes.Shape.Percentage > 0.0f)
	{
		GuiShapeAppendPercent(&HealthBar->Attributes.Shape, -0.10f);
	}
	else
	{
		GuiShapeSetPercent(&HealthBar->Attributes.Shape, 1.0f);
	}
}

////////////////////////////////////////////////////////////
/// Register the MainBars
////////////////////////////////////////////////////////////
void MainBarsRegister()
{
	// MainBars window
	GuiEventHandlerAdd("HP_ButtonReleased", (void*)&HP_ButtonReleased);
}

////////////////////////////////////////////////////////////
/// Initialize the MainBars
////////////////////////////////////////////////////////////
void MainBarsInitialize()
{
	// Obtain window references
	MainBarsWindow = GuiGetWindow("frmMainBars");

	// Create frontLayer
	FrontLayer = GuiWindowGetWidget(MainBarsWindow, "frontLayer");

	// todo: StyleType loads correctly in linux, but not in windows (possibly a problem of \n encoding?)

	GuiShapeSetDisplacementDirection(&FrontLayer->Attributes.Shape, GUI_SHAPE_DIRECTION_RIGTH_TO_LEFT);
	GuiShapeSetTexture(&FrontLayer->Attributes.Shape, 23683);
	GuiShapeSetStyleType(&FrontLayer->Attributes.Shape, GUI_SHAPE_TYPE_SQUARE);

	// Create Bars
	HealthBar = GuiWindowGetWidget(MainBarsWindow, "Health");

	GuiShapeSetDisplacementDirection(&HealthBar->Attributes.Shape, GUI_SHAPE_DIRECTION_RIGTH_TO_LEFT);
	GuiShapeSetTexture(&HealthBar->Attributes.Shape, MAINBARS_GRH_BARS);
	GuiShapeSetStyleType(&HealthBar->Attributes.Shape, GUI_SHAPE_TYPE_CIRCUMFERENCE);

	PowerBar = GuiWindowGetWidget(MainBarsWindow, "Power");

	GuiShapeSetDisplacementDirection(&PowerBar->Attributes.Shape, GUI_SHAPE_DIRECTION_LEFT_TO_RIGTH);
	GuiShapeSetTexture(&PowerBar->Attributes.Shape, MAINBARS_GRH_BARS);
	GuiShapeSetStyleType(&PowerBar->Attributes.Shape, GUI_SHAPE_TYPE_CIRCUMFERENCE);
}

////////////////////////////////////////////////////////////
/// Open the MainBars
////////////////////////////////////////////////////////////
void MainBarsOpen()
{
	if (MainBarsWindow->hStatus == GUI_WINDOW_CLOSED)
	{
		// Open MainBars
		GuiOpenWindow(MainBarsWindow);
	}
}

////////////////////////////////////////////////////////////
/// Renders the background of the MainBars
////////////////////////////////////////////////////////////
void MainBarsRender()
{
	// Render front Layer
	//TextureRenderGrhColorRepeat(MAINBARS_GRH_FRONT_LAYER, 500, 500, 210, 210, &ColorNormal);
}

////////////////////////////////////////////////////////////
/// Close the MainBars
////////////////////////////////////////////////////////////
void MainBarsClose()
{
	if (MainBarsWindow->hStatus == GUI_WINDOW_OPENED)
	{
		// Close the window
		GuiCloseWindow(MainBarsWindow);
	}
}
