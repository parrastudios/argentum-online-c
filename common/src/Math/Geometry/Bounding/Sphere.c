/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Memory/General.h>
#include <Math/General.h>
#include <Math/Geometry/Bounding/Sphere.h>

////////////////////////////////////////////////////////////
/// Create a bounding sphere
////////////////////////////////////////////////////////////
struct BoundingSphere * BoundingSphereCreate()
{
	struct BoundingSphere * SpherePtr = (struct BoundingSphere*)MemoryAllocate(sizeof(struct BoundingSphere));

	if (SpherePtr)
	{
		BoundingSphereSetNull(SpherePtr);

		return SpherePtr;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Initialize a bounding sphere
////////////////////////////////////////////////////////////
struct BoundingSphere * BoundingSphereInitialize(struct BoundingSphere * Sphere)
{
	if (Sphere)
	{
		BoundingSphereSetNull(Sphere);
	}

	return Sphere;
}

////////////////////////////////////////////////////////////
/// Destroy a bounding sphere
////////////////////////////////////////////////////////////
void BoundingSphereDestroy(struct BoundingSphere * Sphere)
{
	MemoryDeallocate(Sphere);
}

////////////////////////////////////////////////////////////
/// Copy a bounding sphere
////////////////////////////////////////////////////////////
void BoundingSphereCopy(struct BoundingSphere * Dest, struct BoundingSphere * Source)
{
	if (Dest && Source)
	{
		Vector3fCopy(&Dest->Center, &Source->Center);
		Dest->Radius = Source->Radius;
	}
}

////////////////////////////////////////////////////////////
/// Set to null a bounding sphere
////////////////////////////////////////////////////////////
void BoundingSphereSetNull(struct BoundingSphere * Sphere)
{
	if (Sphere)
	{
		Sphere->Radius = 0.0f;
		Vector3fSetNull(&Sphere->Center);
	}
}

////////////////////////////////////////////////////////////
/// Set the data of a bounding sphere
////////////////////////////////////////////////////////////
void BoundingSphereSet(struct BoundingSphere * Sphere, struct Vector3f * Center, float Radius)
{
	if (Sphere)
	{
		if (!Center)
		{
			Vector3fSetNull(&Sphere->Center);
		}
		else
		{
			Vector3fCopy(&Sphere->Center, Center);
		}

		if (Radius < 0.0f)
		{
			Sphere->Radius = 0.0f;
		}
		else
		{
			Sphere->Radius = Radius;
		}
	}
}

////////////////////////////////////////////////////////////
/// Set the radius of a bounding sphere
////////////////////////////////////////////////////////////
void BoundingSphereSetRadius(struct BoundingSphere * Sphere, float Radius)
{
	if (Sphere)
	{
		if (Radius < 0.0f)
		{
			Sphere->Radius = 0.0f;
		}
		else
		{
			Sphere->Radius = Radius;
		}
	}
}

////////////////////////////////////////////////////////////
/// Move bounding sphere into specified position as a center
////////////////////////////////////////////////////////////
void BoundingSphereMove(struct BoundingSphere * Sphere, struct Vector3f * Center)
{
    // Update the current center to the new one
    Vector3fCopy(&Sphere->Center, Center);
}

////////////////////////////////////////////////////////////
/// Check if two bounding box are colliding
////////////////////////////////////////////////////////////
bool BoundingSphereCollision(struct BoundingSphere * Sphere, struct BoundingSphere * Other)
{
	struct Vector3f CenterDistance;
	float SquaredDistance;

	Vector3fSubtractEx(&CenterDistance, &Other->Center, &Sphere->Center);

	SquaredDistance = Vector3fLengthSquared(&CenterDistance);

	if (SquaredDistance <= (Sphere->Radius + Other->Radius) * (Sphere->Radius + Other->Radius))
		return true;

	return false;
}

////////////////////////////////////////////////////////////
/// Check if a bounding sphere is colliding with a box
////////////////////////////////////////////////////////////
uinteger BoundingSphereCollisionBox(struct BoundingSphere * Sphere, struct BoundingBox * Box)
{
	static uinteger CollisionMap[3] =
	{
		ArrayInit(BOUNDING_BOX_OUTSIDE, BOUNDING_SPHERE_OUTSIDE),
		ArrayInit(BOUNDING_BOX_INSIDE, BOUNDING_SPHERE_INSIDE),
		ArrayInit(BOUNDING_BOX_OVERLAP, BOUNDING_SPHERE_OVERLAP),
	};

	uinteger Result = BoundingBoxCollisionSphere(Box, Sphere);

	return CollisionMap[Result];
}

////////////////////////////////////////////////////////////
/// Check if a bounding sphere is colliding with a point
////////////////////////////////////////////////////////////
bool BoundingSphereCollisionPoint(struct BoundingSphere * Sphere, struct Vector3f * Point)
{
	struct Vector3f CenterDistance;
	float SquaredDistance;

	Vector3fSubtractEx(&CenterDistance, Point, &Sphere->Center);

	SquaredDistance = Vector3fLengthSquared(&CenterDistance);

	if (SquaredDistance <= Sphere->Radius * Sphere->Radius)
		return true;

	return false;
}

////////////////////////////////////////////////////////////
/// Check if a bounding sphere is colliding with a polygon
////////////////////////////////////////////////////////////
bool BoundingSphereCollisionPolygon(struct BoundingSphere * Sphere, struct PlaneType * PlaneEquation, struct Vector3f * Vertices, uinteger Count)
{
	// ..

	return false;
}
