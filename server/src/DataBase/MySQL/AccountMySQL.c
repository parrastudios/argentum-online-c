/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer García (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/Account.h>
#include <Game/Account.h>
#include <DataBase/Player.h>
#include <DataBase/StoredProcedure.h>

#include <System/IOHelper.h> // todo: remove this

////////////////////////////////////////////////////////////
///	Get an account stored procedure
////////////////////////////////////////////////////////////
struct StoredProcedureType * AccountStoredProcedureGet(enum AccountStoredProcedureId StoredProcedureId)
{
    if (StoredProcedureId < ACCOUNT_STORED_PROCEDURE_SIZE)
    {
        static struct StoredProcedureType StoredProcedureArray[ACCOUNT_STORED_PROCEDURE_SIZE] = {{0}};

        return &StoredProcedureArray[StoredProcedureId];
    }

    return NULL;
}

////////////////////////////////////////////////////////////
/// DBAccountExist stored procedure callback
////////////////////////////////////////////////////////////
void DBAccountExistFetchCallback(struct StoredProcedureArgumentBuilder * BuilderList, uinteger Size)
{

    if (BuilderList && Size > UINTEGER_SUFFIX(0))
    {
        int64 Exists;

        StoredProcedureArgumentList Arguments = BuilderList[0].Arguments;

        StoredProcedureArgumentGetBuildValue(Arguments,
                                            (STORED_PROCEDURE_LONG_LONG, Exists)
                                            );

        printf("Account exists ? Zero or One ? -> %d\n", Exists);

    }
}

////////////////////////////////////////////////////////////
/// DBAccountGetDataByEmail stored procedure callback
////////////////////////////////////////////////////////////
void DBAccountGetDataByEmailFetchCallback(struct StoredProcedureArgumentBuilder * BuilderList, uinteger Size)
{
    if (BuilderList && Size > UINTEGER_SUFFIX(0))
    {
        uinteger Result;

        for (Result = 0; Result < Size; ++Result)
        {

        }
    }

}

////////////////////////////////////////////////////////////
///	Initialize all account stored procedures
////////////////////////////////////////////////////////////
bool AccountStoredProcedureInitialize()
{
    if(!StoredProcedureInitialize(AccountStoredProcedureGet(ACCOUNT_STORED_PROCEDURE_EXISTS), "AccountExists", 1, &DBAccountExistFetchCallback))
    {
        MessageError("AccountStoredProcedure", "Error initializing the procedure: %s", "AccountExists");
        return false;
    }

    return true;
}

////////////////////////////////////////////////////////////
///	Release all account stored procedures
////////////////////////////////////////////////////////////
void AccountStoredProcedureRelease()
{
    uinteger Procedure;

    for (Procedure = 0; Procedure < ACCOUNT_STORED_PROCEDURE_SIZE; ++Procedure)
    {
        StoredProcedureRelease(AccountStoredProcedureGet(Procedure));
    }
}

////////////////////////////////////////////////////////////
///	Check if the account exists in the database
////////////////////////////////////////////////////////////
bool DBAccountExist(struct DBAccountData * Account)
{
    uint64 AccountId = 1;

    StoredProcedurePrototypeBind(AccountStoredProcedureGet(ACCOUNT_STORED_PROCEDURE_EXISTS),
                                 (STORED_PROCEDURE_LONG_LONG, &AccountId));

    if (StoredProcedureCall(AccountStoredProcedureGet(ACCOUNT_STORED_PROCEDURE_EXISTS)))
    {
        return true;
    }

    return false;
}

////////////////////////////////////////////////////////////
///	Get data from account
////////////////////////////////////////////////////////////
bool DBAccountGetDataByEmail(struct DBAccountData * Account, char * Email)
{

    StoredProcedurePrototypeBind(AccountStoredProcedureGet(ACCOUNT_STORED_PROCEDURE_GETBYEMAIL),
                                 (STORED_PROCEDURE_STRING, &Email));
    return true;
}

////////////////////////////////////////////////////////////
///	Set logged field from account
////////////////////////////////////////////////////////////
bool DBAccountSetLogged(struct DBAccountData * Account, bool Logged)
{
    StoredProcedurePrototypeBind(AccountStoredProcedureGet(ACCOUNT_STORED_PROCEDURE_SETCLIENTONLINE),
                                 (STORED_PROCEDURE_TINY, &Logged));

    return false;
}

////////////////////////////////////////////////////////////
///	Get logged field from account
////////////////////////////////////////////////////////////
bool DBAccountCheckLogged(struct DBAccountData * Account, bool * Logged)
{
    StoredProcedurePrototypeBind(AccountStoredProcedureGet(ACCOUNT_STORED_PROCEDURE_GETCLIENTONLINE),
                                 (STORED_PROCEDURE_LONG_LONG, &Account->Id));

    // Set logged flag to false
    *Logged = false;

    return false;
}

////////////////////////////////////////////////////////////
///	Count the number of players associated to account
////////////////////////////////////////////////////////////
uinteger DBAccountGetPlayersSize(struct DBAccountData * Account)
{
    uinteger PlayerSize = 0;

    StoredProcedurePrototypeBind(AccountStoredProcedureGet(ACCOUNT_STORED_PROCEDURE_GETCLIENTONLINE),
                                 (STORED_PROCEDURE_LONG_LONG, &Account->Id));
    return PlayerSize;
}

////////////////////////////////////////////////////////////
///	Query players basic info of an account
////////////////////////////////////////////////////////////
bool DBAccountGetPlayers(struct DBAccountData * Account, Vector DBPlayerList)
{
    QueryResultType Result;

    if (Query(&Result, "SELECT * FROM `%s` WHERE account_id='%d'", DB_PLAYER_TABLE_DEFAULT, Account->Id))
    {
        QueryRowType Row = QueryGetRow(Result);

        while (Row != NULL)
        {
            struct DBCharacterData * DBCharacter;

            // Insert empty field into list
            VectorPushBackEmpty(DBPlayerList);

            DBCharacter = (struct DBCharacterData*)VectorBack(DBPlayerList);

            // Initialize database object
            DBCharacterInitialize(DBCharacter);

            // Load player by row
            DBPlayerLoadByRow(Row, DBCharacter);

            // Go to next row
            Row = QueryGetRow(Result);
        }

        // Release query
        QueryRelease(Result);

        return true;
    }

    return false;
}

////////////////////////////////////////////////////////////
///	Convert database account to gamelogic account
////////////////////////////////////////////////////////////
void DBAccountToAccount(struct DBAccountData * DBAccount, struct AccountData * Account)
{
    Account->Id			= DBAccount->Id;
    Account->Banned		= DBAccount->Banned;
    Account->Logged		= DBAccount->Online;
    Account->Active		= DBAccount->Active;

    strcpy(Account->Name, DBAccount->Name);
    strcpy(Account->Password, DBAccount->Password);
    strcpy(Account->Email, DBAccount->Email);

    Account->PlayerLogged = ACCOUNT_PLAYER_NO_LOGGED;
    Account->PlayersReference = VectorNew(sizeof(uinteger));
}
