/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATABASE_PLAYER_H
#define DATABASE_PLAYER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/Vector.h>
#include <Math/Geometry/Vector.h>
#include <DataBase/Query.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define	DB_PLAYER_TABLE_DEFAULT			"player"

// todo: update field ids

// old field ids
#define DB_PLAYER_FIELD_ID				0x00
#define DB_PLAYER_FIELD_NAME			0x01
#define DB_PLAYER_FIELD_ACCOUNT_ID		0x02
#define DB_PLAYER_FIELD_ACTIVE			0x03
#define DB_PLAYER_FIELD_BAN				0x04

#define DB_PLAYER_FIELD_CLASS			0x05
#define DB_PLAYER_FIELD_GENDER			0x06
#define DB_PLAYER_FIELD_RACE			0x07
#define DB_PLAYER_FIELD_HOME			0x08
#define DB_PLAYER_FIELD_ALIGN			0x09

#define DB_PLAYER_FIELD_HEAD			0x0A
#define DB_PLAYER_FIELD_BODY			0x0B

#define DB_PLAYER_FIELD_STRENGTH		0x0C
#define DB_PLAYER_FIELD_AGILITY			0x0D
#define DB_PLAYER_FIELD_INTELIGENCE		0x0E
#define DB_PLAYER_FIELD_CHARISMA		0x0F
#define DB_PLAYER_FIELD_CONSTITUTION	0x10

#define DB_PLAYER_FIELD_LEVEL			0x11
#define DB_PLAYER_FIELD_EXP				0x12
#define DB_PLAYER_FIELD_GOLD			0x13

#define DB_PLAYER_FIELD_WEAPON			0x14
#define DB_PLAYER_FIELD_SHIELD			0x15
#define DB_PLAYER_FIELD_HELMET			0x16
#define DB_PLAYER_FIELD_FOOTWEAR		0x17

#define DB_PLAYER_FIELD_MAXHP			0x18
#define DB_PLAYER_FIELD_MINHP			0x19
#define DB_PLAYER_FIELD_MAXSTA			0x1A
#define DB_PLAYER_FIELD_MINSTA			0x1B
#define DB_PLAYER_FIELD_MAXMAN			0x1C
#define DB_PLAYER_FIELD_MINMAN			0x1D
#define DB_PLAYER_FIELD_MAXHUN			0x1E
#define DB_PLAYER_FIELD_MINHUM			0x1F
#define DB_PLAYER_FIELD_MAXTHI			0x20
#define DB_PLAYER_FIELD_MINTHI			0x21

#define DB_PLAYER_FIELD_WORLD			0x22
#define DB_PLAYER_FIELD_POS_X			0x23
#define DB_PLAYER_FIELD_POS_Y			0x24
#define DB_PLAYER_FIELD_POS_Z			0x25

// new definitions
#define DB_PLAYER_NAME_MAX_SIZE			0x20
#define DB_PLAYER_DESC_MAX_SIZE			0x2D

/*
#define DB_PLAYER_FIELD_ID				0x00
#define DB_PLAYER_FIELD_NAME			0x01
#define DB_PLAYER_FIELD_ACCOUNT_ID		0x02
#define DB_PLAYER_FIELD_ACTIVE			0x03

#define DB_PLAYER_FIELD_CLASS			0x04
#define DB_PLAYER_FIELD_GENDER			0x05
#define DB_PLAYER_FIELD_RACE			0x06
#define DB_PLAYER_FIELD_HOME			0x07
#define DB_PLAYER_FIELD_ALIGN			0x08

#define DB_PLAYER_FIELD_HEAD			0x09
#define DB_PLAYER_FIELD_BODY			0x0A

#define DB_PLAYER_FIELD_STRENGTH		0x0B
#define DB_PLAYER_FIELD_AGILITY			0x0C
#define DB_PLAYER_FIELD_INTELIGENCE		0x0D
#define DB_PLAYER_FIELD_CHARISMA		0x0E
#define DB_PLAYER_FIELD_CONSTITUTION	0x0F

#define DB_PLAYER_FIELD_LEVEL			0x10
#define DB_PLAYER_FIELD_EXP				0x11
#define DB_PLAYER_FIELD_GOLD			0x12

#define DB_PLAYER_FIELD_WEAPON			0x13
#define DB_PLAYER_FIELD_SHIELD			0x14
#define DB_PLAYER_FIELD_HELMET			0x15
#define DB_PLAYER_FIELD_FOOTWEAR		0x16

#define DB_PLAYER_FIELD_MAXHP			0x17
#define DB_PLAYER_FIELD_MINHP			0x18
#define DB_PLAYER_FIELD_MAXSTA			0x19
#define DB_PLAYER_FIELD_MINSTA			0x1A
#define DB_PLAYER_FIELD_MAXMAN			0x1B
#define DB_PLAYER_FIELD_MINMAN			0x1C
#define DB_PLAYER_FIELD_MAXHUN			0x1D
#define DB_PLAYER_FIELD_MINHUM			0x1E
#define DB_PLAYER_FIELD_MAXTHI			0x1F
#define DB_PLAYER_FIELD_MINTHI			0x20
*/


////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct DBCharacterData;

////////////////////////////////////////////////////////////
// Member Data
////////////////////////////////////////////////////////////
struct DBPlayerData
{
	uint32	Id;
	char	Name[DB_PLAYER_NAME_MAX_SIZE];
	uint32	AccountId;
	bool	Active;
	char	Desc[DB_PLAYER_DESC_MAX_SIZE];
	bool	Ban;

	// Old table (todo: improve design..)
	uint32	Class;
	uint32	Gender;
	uint32	Race;
	uint32	Home;
	uint32	Head;
	uint32	Body;

	uint32	Strength;
	uint32	Agility;
	uint32	Inteligence;
	uint32	Charisma;
	uint32	Constitution;

	uint32	Level;
	uint32	Exp;
	uint32	Gold;

	uint32	Weapon;
	uint32	Shield;
	uint32	Helmet;
	uint32	Footwear;

	uint32	MaxHp,	MinHp;
	uint32	MaxSta, MinSta;
	uint32	MaxMan, MinMan;
	uint32	MaxHun, MinHun;
	uint32	MaxThi, MinThi;

	// todo new table
/*
	// insert BAN field
  `id_character` INT NOT NULL,
  `name` VARCHAR(32) NULL,
  `id_account` INT NULL,
  `active` TINYINT NULL,
  `desc` VARCHAR(45) NULL,
  `id_class` INT NULL,
  `id_race` INT NULL,
  `gender` TINYINT NULL,
  `id_city` INT NULL,
  `gold` INT NULL,
  `max_stamina` INT NULL,
  `min_stamina` INT NULL,
  `max_hunger` INT NULL,
  `min_hunger` INT NULL,
  `max_thirst` INT NULL,
  `min_thirst` INT NULL,
*/


	uint32 World;
	struct Vector3f Position;
};

////////////////////////////////////////////////////////////
// Character Includes
////////////////////////////////////////////////////////////
#include <DataBase/Character.h>

////////////////////////////////////////////////////////////
/// Load all players from database
////////////////////////////////////////////////////////////
bool DBPlayerLoadList(Vector DBPlayerList);

////////////////////////////////////////////////////////////
/// Reload all players from database
////////////////////////////////////////////////////////////
bool DBPlayerReloadList(Vector DBPlayerList);

////////////////////////////////////////////////////////////
/// Store players to database
////////////////////////////////////////////////////////////
bool DBPlayerStoreList(Vector DBPlayerList);

////////////////////////////////////////////////////////////
/// Load the player from database
////////////////////////////////////////////////////////////
bool DBPlayerLoadById(struct DBCharacterData * Character, uinteger Index);

////////////////////////////////////////////////////////////
/// Load the player from row of database
////////////////////////////////////////////////////////////
bool DBPlayerLoadByRow(QueryRowType Row, struct DBCharacterData * Character);

////////////////////////////////////////////////////////////
/// Disconnect the player (store it in database)
////////////////////////////////////////////////////////////
bool DBPlayerStore(struct DBCharacterData * Character);

////////////////////////////////////////////////////////////
/// Set activate flag in database
////////////////////////////////////////////////////////////
void DBPlayerActive(struct DBCharacterData * Character);

////////////////////////////////////////////////////////////
/// Check if a player exists in the database
////////////////////////////////////////////////////////////
bool DBPlayerExist(struct DBCharacterData * Character);

////////////////////////////////////////////////////////////
/// Create a player in the database
////////////////////////////////////////////////////////////
bool DBPlayerCreate(struct DBCharacterData * Character);

////////////////////////////////////////////////////////////
/// Delete a player from the database
////////////////////////////////////////////////////////////
bool DBPlayerDelete(struct DBCharacterData * Character);

#endif // DATABASE_PLAYER_H

