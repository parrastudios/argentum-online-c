/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garcia (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Application.h>

#include <Network/Protocol.h>		///< Network includes (must be first because of Win32)

#include <System/Platform.h>		///< Platform SO includes
#include <System/Thread.h>
#include <System/Mutex.h>
#include <System/CPUInfo.h>

#include <Math/General.h>			///< Math includes

#include <Window/Window.h>			///< Window includes

#include <Graphics/Device.h> 		///< Graphic includes
#include <Graphics/StatesManager.h>
#include <Graphics/Font.h>
#include <Graphics/Texture.h>
#include <Graphics/TextureManager.h>
#include <Graphics/VideoRender.h>
#include <Graphics/Index.h>

#include <Audio/SoundManager.h>     ///< Audio includes

#include <Game/Configuration.h>		///< Game includes
#include <Game/Language.h>
#include <Game/Intro.h>
#include <Game/Login.h>
#include <Game/MainMenu.h>
#include <Game/Display.h>
#include <Game/User.h>
#include <Game/Input.h>

#include <GUI/WindowManager.h>		///< GUI includes
#include <GUI/WindowParser.h>

#include <World/SceneManager.h>		///< World includes
#include <World/Camera.h>
#include <World/Point.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define GAME_RUN_UPDATE_TIME	16.6f
#define GAME_RUN_UPDATE_FACTOR	0.01f

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
uinteger			GameStatus		= GAME_DESTROY;	///< Game status controller
//Mutex				GameStatusMutex;                ///< Game status mutex
//Thread			GameEventsThread;               ///< Game event update thread
struct Timer		GameTimer;						///< Game timer (todo: remove this)

////////////////////////////////////////////////////////////
/// Initialize the application
////////////////////////////////////////////////////////////
bool GameInitialize()
{
	struct VideoMode		DefaultMode;
	struct WindowSettings	Settings;

	// Initialize game timer
	TimerClear(&GameTimer);

	// Initialize math library
	MathInitialize();

	// Initialize the game status mutex
	//MutexInitialize(&GameStatusMutex);

	// Set status of the game
	GameSetStatus(GAME_INITIALIZE);

	// Initialize the configuration
	ConfigInitialize();

	// Platform Initialize
	#if PLATFORM_TYPE == PLATFORM_MACOS
		InitializeWorkingDirectory(); // Specific in MacOS
	#endif

	// Get the CPU features
	CPUInitialize();

	// Create the window handle
	DefaultMode.Width = 1024;
	DefaultMode.Height = 768;
	//DefaultMode.Width = 1152;
	//DefaultMode.Height = 864;
	//DefaultMode.Width = 1920;
	//DefaultMode.Height = 1080;
	DefaultMode.BitsPerPixel = ConfigGet()->BitsPerPixel;

	Settings.DepthBits = 24;
	Settings.StencilBits = 8;
	Settings.AntialiasingLevel = 0;

	// Create window handle
	if (!WindowCreate(&DefaultMode,
		APPLICATION_NAME,
		//#if COMPILE_TYPE == DEBUG_MODE
			NonStyle,
		//#else
		//	ConfigGet()->WindowStyle,
		//#endif
		&Settings))
	{
		return false;
	}

	// Set the processor affinity
	#if COMPILE_TYPE == DEBUG_MODE
		// SystemSetProcessorAffinity();
    #endif

	#if COMPILE_TYPE == DEBUG_MODE
        WindowShowMouseCursor(true);
    #else
        WindowShowMouseCursor(false);
    #endif

	DeviceUseVerticalSync(DeviceGet(), ConfigGet()->Vsync);

	// Initialize fonts
	FontInitialize();

	// Initialize audio device
	AudioInitializeDevice();

	if (!IndexInitialize())
		return false;

	// Initialize language
	if (!LanguageInitialize())
		return false;

	// Initialize the display
	DisplayInitialize();

	// Initialize the protocol
	ProtocolInitialize();

	// Gui initialization block
	{
		// Register maps to templating the script
		GuiParserRegisterMap(GUI_PARSER_MAP_LANG_ID, (WindowParserMapFunc)&LanguageMessageGet);
		GuiParserRegisterMap(GUI_PARSER_MAP_CONFIG_ID, (WindowParserMapFunc)&ConfigToString);

		// Register the login
		LoginRegister();

		// Register the main menu
		MainMenuRegister();

		// Initialize the gui
		GuiInitialize(ConfigGet()->GuiTemplate, GUI_PARSER_WINDOWS_CLIENT_PATH);

		// Initialize the login
		LoginInitialize();

		// Initialize the main menu
		MainMenuInitialize();
	}

	// World initialization block
	{
		EntitySystemInitialize();
	}

	// Set status to intro
	//GameSetStatus(GAME_INTRO);

	//GameSetStatus(GAME_LOGIN);

	GameSetStatus(GAME_RUN);
	{
		// Initialize scene
		if (!SceneManagerCreate(0))
			return false;

		// Initialize user entity (todo: temporary, must go on packet entity creation)
		UserGetData()->Entity = 0; // use 0 as hardcoded value
	}

    // Launch the event thread
    //ThreadLaunch(&GameEventsThread, (FuncType)&GameEvents, NULL);

	return true;
}

////////////////////////////////////////////////////////////
/// Update the events of the application
////////////////////////////////////////////////////////////
void GameEvents()
{
    uint32 StatusController = GAME_DESTROY;

	do
    {
		struct Event Evt = {{0}};

        // Get the event
        WindowGetEvent(&Evt);

        // Get the game status
        GameGetStatus(&StatusController);

        // Update game status events
		switch (StatusController)
		{
			case GAME_INTRO :
			{
				if (Evt.Key.Code == Return)
					GameSetStatus(GAME_LOGIN);

				if (Evt.Key.Code == F12)
					GameSetStatus(GAME_DESTROY);

				break;
			}

			case GAME_LOGIN :
			{
				if (Evt.Key.Code == F12)
					GameSetStatus(GAME_DESTROY);

				break;
			}

			case GAME_ACCOUNT :
			{
				if (Evt.Key.Code == F12)
					GameSetStatus(GAME_LOGIN);

				break;
			}

			case GAME_RUN :
			{
				float ElapsedTime = TimerGetTime(&GameTimer);

				if (Evt.Key.Code == F12)
				{
					GameSetStatus(GAME_ACCOUNT);
				}

				if (ElapsedTime > GAME_RUN_UPDATE_TIME)
				{
					if (GuiKeyboardAvailable() && InputUpdateMovement())
					{
						// ...
					}

					{
						struct SceneType * Scene = SceneManagerGet(0);

						struct Vector3f Velocity;
						struct Vector3f Orientation;
						struct Vector3f Rotation;

						// Speedup elapsed time
						ElapsedTime *= GAME_RUN_UPDATE_FACTOR;

						// Apply movement
						CharacterMoveApply(UserGetData()->Character.Movement, &Velocity, &Orientation, &Rotation);

						// Update scene player entity
						SceneModelUpdate(Scene, UserGetData()->Entity, &Velocity, &Orientation, &Rotation, ElapsedTime);

						// Update scene point entities
						ScenePointsUpdate(Scene, &Velocity, &Orientation, &Rotation, ElapsedTime);

						// Update scene campera entity
						SceneCameraUpdate(Scene, UserGetData()->Entity, ElapsedTime);

						// Update scene
						SceneUpdate(Scene, ElapsedTime);
					}

					TimerReset(&GameTimer);
				}

				if (Evt.Key.Code == F1)
					TextureScreenShot(WindowWidth, WindowHeight);

				break;
			}
		}

		// Abort key in debug mode
		#if COMPILE_TYPE == DEBUG_MODE
			if (Evt.Key.Code == Esc)
			{
				GameSetStatus(GAME_DESTROY);
			}
		#endif

        // Compute the user interface
        GuiUpdate(&Evt);

		// Clear events
		WindowClearEvent();

		#if COMPILE_TYPE == DEBUG_MODE
		//	SystemSleep(5);
		#endif
	//} while (StatusController != GAME_DESTROY);
	} while (0);
}

////////////////////////////////////////////////////////////
/// Run the application
////////////////////////////////////////////////////////////
void GameRun()
{
    uint32 StatusController = GAME_DESTROY;

	do
	{
		// Clear device scene
		DeviceClear(DeviceGet(), true, false);

		// Get the game status
		GameGetStatus(&StatusController);

		// Check keys
		switch (StatusController)
		{
			case GAME_INTRO :
			{
				// Show intro
				IntroShow();

				// Set game status
				GameSetStatus(GAME_LOGIN);

				break;
			}

			case GAME_LOGIN :
			{
				// Set orthogonal projection
				DeviceSetMatrixOrthogonal(DeviceGet());

				DeviceBegin(DeviceGet());
					DeviceSetState(DeviceGet(), DEVICE_RENDERING_2D | DEVICE_RENDERING_ALPHA);

					// Render game login
					LoginRender();
				DeviceEnd(DeviceGet());

				break;
			}

			case GAME_ACCOUNT :
			{
				// Set orthogonal projection
				DeviceSetMatrixOrthogonal(DeviceGet());

				DeviceBegin(DeviceGet());
					DeviceSetState(DeviceGet(), DEVICE_RENDERING_2D | DEVICE_RENDERING_ALPHA);

					// Render game account
					AccountRender();
				DeviceEnd(DeviceGet());

				break;
			}

			case GAME_RUN :
			{
				// Get the camera
				struct EntityType * Camera = SceneCameraGet(SceneManagerGet(0));

				struct ComponentType * CameraTransform = EntityComponentTransform(Camera);
				struct CameraComponentTransformData * CameraTransformData = ComponentDataDerived(CameraTransform, struct CameraComponentTransformData);

				struct ComponentType * CameraCollision = EntityComponentCollision(Camera);
				struct ComponentCollisionFrustumData * CameraCollisionData = ComponentDataCollisionFrustum(CameraTransform);

				// Apply matrix
				DeviceSetMatrix(DeviceGet(), &CameraTransformData->ViewProjectionMatrix);

				DeviceBegin(DeviceGet());
					DeviceSetState(DeviceGet(), DEVICE_RENDERING_3D);

					// Render scene
					SceneRender(SceneManagerGet(0), &CameraCollisionData->Frustum);

				DeviceEnd(DeviceGet());

				break;
			}
		}

		// Set orthogonal projection
		DeviceSetMatrixOrthogonal(DeviceGet());

		DeviceBegin(DeviceGet());
			DeviceSetState(DeviceGet(), DEVICE_RENDERING_2D | DEVICE_RENDERING_ALPHA);

			// Render GUI
			GuiRender();

			// Update the display
			DisplayUpdate();

			// Render camera info
			//#if COMPILE_TYPE == DEBUG_MODE
			//	if (StatusController == GAME_RUN)
			//		SceneManagerRenderDebug();
			//#endif

		DeviceEnd(DeviceGet());

		// Display on window handle
		WindowDisplay();

		// Process input events
		WindowProcessEvents();

		GameEvents();

		#if COMPILE_TYPE == DEBUG_MODE
		//	SystemSleep(5);
		#endif
	} while (StatusController != GAME_DESTROY);
}

////////////////////////////////////////////////////////////
/// Destroy and cleanup the application
////////////////////////////////////////////////////////////
bool GameDestroy()
{
	// Destroy the login
	LoginClose();

	// Delete the user
	UserDestroy();

	// Save config
	ConfigSave();

	// Destroy video device
	VideoDestroy();

	// Destroy GUI
	GuiDestroy();

	// World destruction block
	{
		// Destroy scene
		SceneManagerDestroy(0);

		// ...

		// Destroy entity system
		EntitySystemDestroy();
	}

	// Destroy Fonts
	FontDestroy();

	// Destroy textures
	TextureManagerDestroy();

	// Destroy graphical device
	DeviceDestroy(DeviceGet());

	// Destroy Sound Buffer
	SoundBufferDestroy();

	// Destroy Audio Device
	AudioDestroyDevice();

	// Destroy the window handle
	WindowClose();

	// Destroy the protocol
	ProtocolDestroy();

	// Destroy cpu queries
	CPUDestroy();

	// Destroy game mutex
	//MutexDestroy(&GameStatusMutex);

	return true;
}

////////////////////////////////////////////////////////////
/// Set up the status of the game
////////////////////////////////////////////////////////////
void GameSetStatus(uint32 StatusController)
{
	switch (StatusController)
	{
		case GAME_INITIALIZE :

			break;

		case GAME_LOGIN :
			LoginOpen();

			break;

		case GAME_ACCOUNT :
			// Close the login
			LoginClose();

			//UserGetData()->Logged = true;

			break;

		case GAME_RUN :
			// Open main menu
			MainMenuOpen();

			// Reset game timer
			TimerReset(&GameTimer);

			break;

		case GAME_DESTROY :
			LoginClose();
			MainMenuClose();

			break;
	}

	// Set the status
    //MutexLock(&GameStatusMutex);

	GameStatus = StatusController;

	//MutexUnlock(&GameStatusMutex);
}

////////////////////////////////////////////////////////////
/// Get the status of the game
////////////////////////////////////////////////////////////
void GameGetStatus(uint32 * StatusController)
{
    //MutexLock(&GameStatusMutex);

    *StatusController = GameStatus;

    //MutexUnlock(&GameStatusMutex);
}
