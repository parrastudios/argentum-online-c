/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATABASE_ACCOUNT_H
#define DATABASE_ACCOUNT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/Vector.h>
#include <DataBase/Character.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define	DB_ACCOUNT_TABLE_DEFAULT         "Account"

#define DB_ACCOUNT_FIELD_ID				0x00
#define DB_ACCOUNT_FIELD_NAME			0x01
#define DB_ACCOUNT_FIELD_PASSWORD		0x02
#define DB_ACCOUNT_FIELD_EMAIL			0x03
#define DB_ACCOUNT_FIELD_ONLINE			0x04

#define DB_ACCOUNT_NAME_MAX_SIZE		UINTEGER_SUFFIX(0xFF)
#define DB_ACCOUNT_PASS_MAX_SIZE		UINTEGER_SUFFIX(0xFF)
#define DB_ACCOUNT_MAIL_MAX_SIZE		UINTEGER_SUFFIX(0xFF)

////////////////////////////////////////////////////////////
// Enumerations
////////////////////////////////////////////////////////////

enum AccountStoredProcedureId
{
	ACCOUNT_STORED_PROCEDURE_EXISTS	                = 0x00,
	ACCOUNT_STORED_PROCEDURE_GETBYEMAIL	            = 0x01,
	ACCOUNT_STORED_PROCEDURE_GETCLIENTONLINE	    = 0x02,
	ACCOUNT_STORED_PROCEDURE_SETCLIENTONLINE    	= 0x03,
	ACCOUNT_STORED_PROCEDURE_GETCHARACTERCOUNT	    = 0x04,

	ACCOUNT_STORED_PROCEDURE_SIZE
};


////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct AccountData;

////////////////////////////////////////////////////////////
// Member Data
////////////////////////////////////////////////////////////
struct DBAccountData
{
	uint64	Id;											///< Account id
	char	Name[DB_ACCOUNT_NAME_MAX_SIZE];				///< Account name
	char	Password[DB_ACCOUNT_PASS_MAX_SIZE];			///< Account password
	char	Email[DB_ACCOUNT_MAIL_MAX_SIZE];			///< Account email
	bool	Banned;										///< Account ban status
	bool	Online;										///< Check if account is logged in
	bool	Active;										///< Check if account is activated
};


////////////////////////////////////////////////////////////
///	Initialize all account stored procedures
////////////////////////////////////////////////////////////
bool AccountStoredProcedureInitialize();

////////////////////////////////////////////////////////////
///	Release all account stored procedures
////////////////////////////////////////////////////////////
void AccountStoredProcedureRelease();

////////////////////////////////////////////////////////////
///	Check if the account exist in the database
////////////////////////////////////////////////////////////
bool DBAccountExist(struct DBAccountData * Account);

////////////////////////////////////////////////////////////
///	Get data from account
////////////////////////////////////////////////////////////
bool DBAccountGetDataByEmail(struct DBAccountData * Account, char * Email);

////////////////////////////////////////////////////////////
///	Set logged field from account
////////////////////////////////////////////////////////////
bool DBAccountSetLogged(struct DBAccountData * Account, bool Logged);

////////////////////////////////////////////////////////////
///	Get logged field from account
////////////////////////////////////////////////////////////
bool DBAccountCheckLogged(struct DBAccountData * Account, bool * Logged);

////////////////////////////////////////////////////////////
///	Count the number of players associated to account
////////////////////////////////////////////////////////////
uinteger DBAccountGetPlayersSize(struct DBAccountData * Account);

////////////////////////////////////////////////////////////
///	Query players basic info of an account
////////////////////////////////////////////////////////////
bool DBAccountGetPlayers(struct DBAccountData * Account, Vector DBPlayerList);

////////////////////////////////////////////////////////////
///	Convert database account to gamelogic account
////////////////////////////////////////////////////////////
void DBAccountToAccount(struct DBAccountData * DBAccount, struct AccountData * Account);

#endif // DATABASE_ACCOUNT_H
