/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/Mesh.h>
#include <Graphics/Texture.h>
#include <Graphics/TextureLoader.h>
#include <Graphics/Device.h>
#include <System/IOHelper.h> // todo: remove

////////////////////////////////////////////////////////////
// Enumerations
////////////////////////////////////////////////////////////
enum MeshLocationId
{
    MESH_POSITION_LOCATION_ID   = 0x00,
    MESH_TEX_COORD_LOCATION_ID  = 0x01,
    MESH_NORMAL_LOCATION_ID     = 0x02,

    MESH_LOCATION_ID_SIZE
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a basic static mesh
////////////////////////////////////////////////////////////
struct MeshType * MeshCreate(struct ModelResourceType * Resource)
{
	// todo: do a proper allocation (factory)
	struct MeshType * Mesh = (struct MeshType *)MemoryAllocate(sizeof(struct MeshType));

	if (Mesh)
	{
		uinteger i;

		// Initialize mesh
		MeshInitialize(Mesh);

		#if (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL2)

		// Generate element buffer object
		if (!Mesh->EBO)
		{
			DeviceGetExt()->glGenBuffersARB(1, &Mesh->EBO);
		}

		// Bind triangles
		DeviceGetExt()->glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, Mesh->EBO);
		DeviceGetExt()->glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER,
			VectorSize(Resource->Triangles) * VectorTypeSize(Resource->Triangles), VectorFront(Resource->Triangles), GL_STATIC_DRAW);
		DeviceGetExt()->glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);

		#endif

		// Calculate vertex list size
		Mesh->VertexListSize = VectorSize(Resource->Positions);

		// Allocate vertex list
		Mesh->VertexList = (struct MeshVertexType *)MemoryAllocate(sizeof(struct MeshVertexType) * Mesh->VertexListSize);

		// Reset memory block
		MemorySet(Mesh->VertexList, 0, sizeof(struct MeshVertexType) * Mesh->VertexListSize);

		// Set up positions
		for (i = 0; i < VectorSize(Resource->Positions); ++i)
		{
			struct Vector3f * Iterator = (struct Vector3f *)VectorAt(Resource->Positions, i);
			struct MeshVertexType * Vertex = &Mesh->VertexList[i];

			Vertex->Position[0] = Iterator->x;
			Vertex->Position[1] = Iterator->y;
			Vertex->Position[2] = Iterator->z;
		}

		// Set up normals
		for (i = 0; i < VectorSize(Resource->Normals); ++i)
		{
			struct Vector3f * Iterator = (struct Vector3f *)VectorAt(Resource->Normals, i);
			struct MeshVertexType * Vertex = &Mesh->VertexList[i];

			Vertex->Normal[0] = Iterator->x;
			Vertex->Normal[1] = Iterator->y;
			Vertex->Normal[2] = Iterator->z;
		}

		// Set up tangents
		for (i = 0; i < VectorSize(Resource->Tangents); ++i)
		{
			struct Vector4f * Iterator = (struct Vector4f *)VectorAt(Resource->Tangents, i);
			struct MeshVertexType * Vertex = &Mesh->VertexList[i];

			Vertex->Tangent[0] = Iterator->x;
			Vertex->Tangent[1] = Iterator->y;
			Vertex->Tangent[2] = Iterator->z;
			Vertex->Tangent[3] = Iterator->w;
		}

		// Set up texture coords
		for (i = 0; i < VectorSize(Resource->TextureCoords); ++i)
		{
			struct Vector2f * Iterator = (struct Vector2f *)VectorAt(Resource->TextureCoords, i);
			struct MeshVertexType * Vertex = &Mesh->VertexList[i];

			Vertex->TexCoord[0] = Iterator->x;
			Vertex->TexCoord[1] = Iterator->y;
		}

		// Set up blend indexes
		for (i = 0; i < VectorSize(Resource->BlendIndexes); ++i)
		{
			struct Vector4ub * Iterator = (struct Vector4ub *)VectorAt(Resource->BlendIndexes, i);
			struct MeshVertexType * Vertex = &Mesh->VertexList[i];

			Vertex->BlendIndex[0] = Iterator->x;
			Vertex->BlendIndex[1] = Iterator->y;
			Vertex->BlendIndex[2] = Iterator->z;
			Vertex->BlendIndex[3] = Iterator->w;
		}

		// Set up blend weights
		for (i = 0; i < VectorSize(Resource->BlendWeights); ++i)
		{
			struct Vector4ub * Iterator = (struct Vector4ub *)VectorAt(Resource->BlendWeights, i);
			struct MeshVertexType * Vertex = &Mesh->VertexList[i];

			Vertex->BlendWeight[0] = Iterator->x;
			Vertex->BlendWeight[1] = Iterator->y;
			Vertex->BlendWeight[2] = Iterator->z;
			Vertex->BlendWeight[3] = Iterator->w;
		}

		// Set up colors
		for (i = 0; i < VectorSize(Resource->Colors); ++i)
		{
			struct Color4ub * Iterator = (struct Color4ub *)VectorAt(Resource->Colors, i);
			struct MeshVertexType * Vertex = &Mesh->VertexList[i];

			Vertex->Color[0] = Iterator->r;
			Vertex->Color[1] = Iterator->g;
			Vertex->Color[2] = Iterator->b;
			Vertex->Color[3] = Iterator->a;
		}

		#if (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL2)

		// Generate vertex buffer object
		if (!Mesh->VBO)
		{
			DeviceGetExt()->glGenBuffersARB(1, &Mesh->VBO);
		}

		DeviceGetExt()->glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, Mesh->VBO);
		DeviceGetExt()->glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER, Mesh->VertexListSize, Mesh->VertexList, GL_STATIC_DRAW);
		DeviceGetExt()->glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);

		#elif (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL3)

		#endif

		// Load textures
		for (i = 0; i < VectorSize(Resource->Textures); ++i)
		{
			char Path[0xFF];

			struct Texture TextureData;

			// Retreive texture name
			uint8 * TextureName = VectorAtT(Resource->Textures, i, uint8 *);

			// Get the proper path
			sprintf(Path, "data/models/test/%s", TextureName);

			// Load texture
			if (TextureLoadFromFile(Path, &TextureData))
			{
				if (TextureCreate(&TextureData, true, false))
				{
					// Insert into texture list
					VectorPushBack(Mesh->TextureList, &TextureData);
				}
				else
				{
					// Clear texture
					MemoryDeallocate(TextureData.Pixels);
				}
			}
		}

		// Set up mesh entries
		for (i = 0; i < VectorSize(Resource->Meshes); ++i)
		{
			struct ModelMeshType * Iterator = (struct ModelMeshType *)VectorAt(Resource->Meshes, i);

			// Insert model mesh into entry list
			VectorPushBack(Mesh->EntryList, Iterator);
		}

		// Set up triangles
		Mesh->IndexListSize = VectorSize(Resource->Triangles) * 3;

		Mesh->IndexList = (uint32 *)MemoryAllocate(sizeof(uint32) * Mesh->IndexListSize);

		if (Mesh->IndexList)
		{
			for (i = 0; i < VectorSize(Resource->Triangles); ++i)
			{
				struct Vector3ui * Iterator = (struct Vector3ui *)VectorAt(Resource->Triangles, i);

				uinteger Index = i * 3;

				Mesh->IndexList[Index] = Iterator->x;
				Mesh->IndexList[Index + 1] = Iterator->y;
				Mesh->IndexList[Index + 2] = Iterator->z;
			}
		}

		return Mesh;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Initialize a basic static mesh
////////////////////////////////////////////////////////////
void MeshInitialize(struct MeshType * Mesh)
{
    if (Mesh)
    {
        #if (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL2)

            Mesh->EBO = 0;
            Mesh->VBO = 0;
            Mesh->UBO = 0;
            Mesh->UBOSize = 0;
            Mesh->BoneMatrixOffset = 0;

        #elif (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL3)

            uinteger i;

            // Initialize buffers
            for (i = 0; i < MESH_VERTEX_BUFFER_ID_SIZE; ++i)
            {
                Mesh->Buffers[i] = 0;
            }

            // Initialize vertex array object
            Mesh->VAO = 0;

        #endif

        // Create texture list
        Mesh->TextureList = VectorNew(sizeof(struct Texture));

        // Create entry list
        Mesh->EntryList = VectorNew(sizeof(struct ModelMeshType));
    }
}

////////////////////////////////////////////////////////////
/// Clear a basic static mesh
////////////////////////////////////////////////////////////
void MeshClear(struct MeshType * Mesh)
{

}

////////////////////////////////////////////////////////////
/// Copy a instance of basic static mesh vertex list
////////////////////////////////////////////////////////////
struct MeshVertexType * MeshCopyVertexList(struct MeshType * Mesh)
{
	if (Mesh && Mesh->VertexList)
	{
		// Allocate vertex list
		struct MeshVertexType * VertexList = (struct MeshVertexType *)MemoryAllocate(sizeof(struct MeshVertexType) * Mesh->VertexListSize);

		if (VertexList)
		{
			// Copy vertex list data
			MemoryCopy(VertexList, Mesh->VertexList, sizeof(struct MeshVertexType) * Mesh->VertexListSize);

			return VertexList;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a basic static mesh
////////////////////////////////////////////////////////////
void MeshDestroy(struct MeshType * Mesh)
{
    if (Mesh)
    {
		uinteger i;

		// Destroy textures
		for (i = 0; i < VectorSize(Mesh->TextureList); ++i)
		{
			// Retreive texture data
			struct Texture * Iterator = (struct Texture *)VectorAt(Mesh->TextureList, i);

			// Destroy texture
			TextureDelete(Iterator);

			// Clear pixels
			MemoryDeallocate(Iterator->Pixels);
		}

        #if (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL2)

            DeviceGetExt()->glDeleteBuffersARB(1, &Mesh->EBO);

        #elif (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL3)
            // Clear index buffer
            if (Mesh->Buffers[MESH_INDEX_BUFFER_ID] != 0)
            {
				DeviceGetExt()->glDeleteBuffersARB(MESH_VERTEX_BUFFER_ID_SIZE, Mesh->Buffers);
            }

            // Clear vertex array object
            if (Mesh->VAO != 0)
            {
                glDeleteVertexArrays(1, &Mesh->VAO);
                Mesh->VAO = 0;
            }
        #endif

        // Destroy texture list
        VectorDestroy(Mesh->TextureList);

        // Destroy entry list
        VectorDestroy(Mesh->EntryList);

		// Deallocate vertex list
		MemoryDeallocate(Mesh->VertexList);

		Mesh->VertexListSize = 0;

		// Deallocate index list
		MemoryDeallocate(Mesh->IndexList);

		Mesh->IndexListSize = 0;

		// Destroy mesh (todo: destroy it in the factory)
		MemoryDeallocate(Mesh);
    }
}

////////////////////////////////////////////////////////////
/// Render a basic static mesh
////////////////////////////////////////////////////////////
void MeshRender(struct MeshType * Mesh)
{
    if (Mesh)
    {
		uinteger i;

       #if (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL1)

		GLfloat Zero[4] = { 0.0f, 0.0f, 0.0f, 0.0f },
			One[4] = { 1.0f, 1.0f, 1.0f, 1.0f },
			Ambient[4] = { 0.5f, 0.5f, 0.5f, 1.0f },
			Diffuse[4] = { 0.5f, 0.5f, 0.5f, 1.0f },
			LightDir[4] = { cosf(degtorad(-60.0f)), 0.0f, sinf(degtorad(-60.0f)), 0.0f };

		glClearDepth(1.0f);
		glDisable(GL_FOG);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
		//glDisable(GL_CULL_FACE);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, Zero);
		glMaterialfv(GL_FRONT, GL_SPECULAR, Zero);
		glMaterialfv(GL_FRONT, GL_EMISSION, Zero);
		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, One);
		glLightfv(GL_LIGHT0, GL_SPECULAR, Zero);
		glLightfv(GL_LIGHT0, GL_AMBIENT, Ambient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, Diffuse);
		glLightfv(GL_LIGHT0, GL_POSITION, LightDir);

		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		glEnable(GL_NORMALIZE);

		glColor3f(1.0f, 1.0f, 1.0f);

		glVertexPointer(3, GL_FLOAT, sizeof(struct MeshVertexType), &Mesh->VertexList[0].Position[0]);
		glNormalPointer(GL_FLOAT, sizeof(struct MeshVertexType), &Mesh->VertexList[0].Normal[0]);
		glTexCoordPointer(2, GL_FLOAT, sizeof(struct MeshVertexType), &Mesh->VertexList[0].TexCoord[0]);
		//glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(struct MeshVertexType), &Mesh->VertexList[0].Color[0]);

        #elif (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL2)

		DeviceGetExt()->glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, Mesh->EBO);
		DeviceGetExt()->glBindBufferARB(GL_ARRAY_BUFFER, Mesh->VBO);

		glVertexPointer(3, GL_FLOAT, sizeof(struct MeshVertexType), (void *)OffsetOf(struct MeshVertexType, Position));
		glNormalPointer(GL_FLOAT, sizeof(struct MeshVertexType), (void *)OffsetOf(struct MeshVertexType, Normal));
		glTexCoordPointer(2, GL_FLOAT, sizeof(struct MeshVertexType), (void *)OffsetOf(struct MeshVertexType, TexCoord));
		DeviceGetExt()->glVertexAttribPointerARB(1, 4, GL_FLOAT, GL_FALSE, sizeof(struct MeshVertexType), (void *)OffsetOf(struct MeshVertexType, Tangent));

		DeviceGetExt()->glEnableVertexAttribArray(1);

		#endif

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		//glEnableClientState(GL_COLOR_ARRAY);

		glEnable(GL_TEXTURE_2D);

		// Iterate through each mesh entry
		for (i = 0; i < VectorSize(Mesh->EntryList); ++i)
		{
			struct ModelMeshType * Entry = (struct ModelMeshType *)VectorAt(Mesh->EntryList, i);

			if (Entry->Material < VectorSize(Mesh->TextureList))
			{
				// Retrieve entry material index
				struct Texture * MaterialTexture = (struct Texture *)VectorAt(Mesh->TextureList, Entry->Material);

				// Bind material texture
				if (MaterialTexture)
				{
			        #if (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL2)
						DeviceGetExt()->glActiveTextureARB(GL_TEXTURE0);
					#endif

					TextureBind(MaterialTexture);
				}
			}
			else
			{
				// Bind invalid texture
				// ...
			}

			// Draw elements
			//glDrawElements(GL_TRIANGLES, 3 * Entry->IndexSize, GL_UNSIGNED_INT, (void *)Entry->IndexOffset);
			
	        #if (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL1)

			glDrawElements(GL_TRIANGLES, Entry->IndexSize * 3, GL_UNSIGNED_INT, &Mesh->IndexList[Entry->IndexOffset * 3]);

	        #elif (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL2)

			glDrawElements(GL_TRIANGLES, 3 * Entry->IndexSize, GL_UNSIGNED_INT, 0);

			#endif
			//glDrawElements(GL_TRIANGLES, 3 * Entry->IndexSize, GL_UNSIGNED_INT, &TriangleList[Entry->IndexOffset]);
			//glDrawElements(GL_TRIANGLES, 3 * Entry->IndexSize, GL_UNSIGNED_INT, (void *)(3 * sizeof(float) * Entry->IndexOffset));
		}

		glDisable(GL_TEXTURE_2D);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		//glEnableClientState(GL_COLOR_ARRAY);

		glDisable(GL_NORMALIZE);    
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHTING);
		
        #if (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL2)

		DeviceGetExt()->glDisableVertexAttribArray(1);

		DeviceGetExt()->glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);
		DeviceGetExt()->glBindBufferARB(GL_ARRAY_BUFFER, 0);

        #elif (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL3)

            // Bind vertex array object
            glBindVertexArray(Mesh->VAO);

            // Iterate through each mesh entry
            for (i = 0; i < VectorSize(Mesh->EntryList); ++i)
            {
				struct ModelMeshType * Entry = (struct ModelMeshType *)VectorAt(Mesh->EntryList, i);

                if (Entry->Material < VectorSize(Mesh->TextureList))
                {
                    // Retrieve entry material index
                    struct Texture * MaterialTexture = (struct Texture *)VectorAt(Mesh->TextureList, Entry->Material);

                    // Bind material texture
                    if (MaterialTexture)
                    {
                        glActiveTexture(GL_TEXTURE0);

                        TextureBind(MaterialTexture);
                    }

                    // Draw elements
                    glDrawElementsBaseVertex(GL_TRIANGLES,
                                             Entry->IndexSize,
                                             GL_UNSIGNED_INT,
                                             (void *)(sizeof(uinteger) * Entry->IndexOffset), // Not sure if this is legal
                                             Entry->VertexOffset);
                }
            }

            // Disable vertex array object
            glBindVertexArray(0);
        #endif
    }
}

////////////////////////////////////////////////////////////
/// Render a basic static mesh instanced
////////////////////////////////////////////////////////////
void MeshRenderInstanced(struct MeshType * Mesh,
    uinteger Instances, struct Matrix4f * MVPMatrixList, struct Matrix4f * WorldMatrixList)
{
    if (Mesh)
    {
        // TODO: ALL

        #if (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL2)

            // ...

        #elif (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL3)
            uinteger i;

            // Bind world view projection matrix list
            glBindBuffer(GL_ARRAY_BUFFER, Mesh->Buffers[MESH_WVP_MAT_BUFFER_ID]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(struct Matrix4f) * Instances, MVPMatrixList, GL_DYNAMIC_DRAW);

            // Bind world matrix list
            glBindBuffer(GL_ARRAY_BUFFER, Mesh->Buffers[MESH_WORLD_MAT_BUFFER_ID]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(struct Matrix4f) * Instances, WorldMatrixList, GL_DYNAMIC_DRAW);

            // Bind vertex array object
            glBindVertexArray(Mesh->VAO);

            // Iterate through each mesh entry
            for (i = 0; i < VectorSize(Mesh->EntryList); ++i)
            {
				struct ModelMeshType * Entry = (struct ModelMeshType *)VectorAt(Mesh->EntryList, i);

                if (Entry->Material < VectorSize(Mesh->TextureList))
                {
                    // Retrieve entry material index
                    struct Texture * MaterialTexture = (struct Texture *)VectorAt(Mesh->TextureList, Entry->Material);

                    // Bind material texture
                    if (MaterialTexture)
                    {
                        glActiveTexture(GL_TEXTURE0);

                        TextureBind(MaterialTexture);
                    }

                    // Draw elements instanced
                    glDrawElementsInstancedBaseVertex(GL_TRIANGLES,
                                                      Entry->IndexSize,
                                                      GL_UNSIGNED_INT,
                                                      (void *)(sizeof(uinteger) * Entry->IndexOffset), // Not sure if this is legal
                                                      Instances,
                                                      Entry->VertexOffset);
                }
            }

            // Disable vertex array object
            glBindVertexArray(0);
        #endif
    }
}
