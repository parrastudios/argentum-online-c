/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Casting.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Reinterpret floating point 32-bit precision into
///	32-bit integer
////////////////////////////////////////////////////////////
int32 CastReinterpret32FloatInt(float32 Value)
{
	int32 IntValue;
	uint8 * IntPointer = (uint8 *)&IntValue;
	uint8 * FloatPointer = (uint8 *)&Value;

	// Reinterpret safely the float into integer without
	// violating strict aliasing and type-pruned rules
	MemoryCopy(IntPointer, FloatPointer, sizeof(int32));

	// Return float value correclty reinterpreted to integer
	return IntValue;
}

////////////////////////////////////////////////////////////
/// Reinterpret floating point 32-bit precision into
///	32-bit unsigned integer
////////////////////////////////////////////////////////////
uint32 CastReinterpret32FloatUInt(float32 Value)
{
	uint32 UIntValue;
	uint8 * UIntPointer = (uint8 *)&UIntValue;
	uint8 * FloatPointer = (uint8 *)&Value;

	// Reinterpret safely the float into integer without
	// violating strict aliasing and type-pruned rules
	MemoryCopy(UIntPointer, FloatPointer, sizeof(uint32));

	// Return float value correclty reinterpreted to integer
	return UIntValue;
}

////////////////////////////////////////////////////////////
/// Reinterpret floating point 64-bit precision into
///	64-bit integer
////////////////////////////////////////////////////////////
int64 CastReinterpret64FloatInt(float64 Value)
{
	int64 IntValue;
	uint8 * IntPointer = (uint8 *)&IntValue;
	uint8 * FloatPointer = (uint8 *)&Value;

	// Reinterpret safely the float into integer without
	// violating strict aliasing and type-pruned rules
	MemoryCopy(IntPointer, FloatPointer, sizeof(int64));

	// Return float value correclty reinterpreted to integer
	return IntValue;
}

////////////////////////////////////////////////////////////
/// Reinterpret floating point 64-bit precision into
///	64-bit unsigned integer
////////////////////////////////////////////////////////////
uint64 CastReinterpret64FloatUInt(float64 Value)
{
	uint64 UIntValue;
	uint8 * UIntPointer = (uint8 *)&UIntValue;
	uint8 * FloatPointer = (uint8 *)&Value;

	// Reinterpret safely the float into integer without
	// violating strict aliasing and type-pruned rules
	MemoryCopy(UIntPointer, FloatPointer, sizeof(uint64));

	// Return float value correclty reinterpreted to integer
	return UIntValue;
}
