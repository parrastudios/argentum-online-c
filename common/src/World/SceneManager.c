/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/List.h>
#include <DataType/Set.h>

#include <Game/Tick.h>

#include <World/SceneManager.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define SCENE_ACTION_QUERY			0x00
#define SCENE_ACTION_ACK			0x01

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct SceneActionQuery
{
	uint8							Movement;	//< Movement user want to do
	bool							Pressed;	//< If it is a pressed or released event
};

struct SceneActionAck
{
	uint8							Movement;	//< Movement user want to do
	struct Vector3f					Position;	//< Final entity position
	struct Vector3f					Rotation;	//< Final entity rotation
};

struct SceneActionEventType
{
	uint32							Index;		//< Index of the entity

	union
	{
		struct SceneActionQuery		Query;		//< Query to apply in the world
		struct SceneActionAck		Ack;		//< Result of query in the world
	} Entry;
};

struct SceneActionQueueType
{
	struct EntityManagerType *		EntityManager;	//< Entity manager
	List							Events;			//< List of scene action events
	uint32							Type;			//< Type of action queue
	uint32							Tick;			//< Current tick in which events are performed
	uint32							Size;			//< Size of the action
};

////////////////////////////////////////////////////////////
// Internal data
////////////////////////////////////////////////////////////
struct SceneType Scene; // I will improve it, I promise

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a scene in the manager from given index
////////////////////////////////////////////////////////////
bool SceneManagerCreate(uinteger Index)
{
	return SceneLoad(&Scene, Index);
}

////////////////////////////////////////////////////////////
/// Get a scene from the manager by given index
////////////////////////////////////////////////////////////
struct SceneType * SceneManagerGet(uinteger Index)
{
	return &Scene;
}

////////////////////////////////////////////////////////////
/// Destroy a scene from the manager
////////////////////////////////////////////////////////////
void SceneManagerDestroy(uinteger Index)
{
	SceneDestroy(&Scene);
}

struct SceneActionQueueType * SceneManagerActionQueueCreate(uint32 Type)
{
	struct SceneActionQueueType * ActionQueue = NULL;
	uint32 ActionSize = 0;
	uint32 QueueSize;

	if (Type == SCENE_ACTION_QUERY)
	{
		ActionSize = sizeof(struct SceneActionQuery);
	}
	else if (ActionQueue->Type == SCENE_ACTION_ACK)
	{
		ActionSize = sizeof(struct SceneActionAck);
	}

	QueueSize = OffsetOf(struct SceneActionEventType, Entry) + ActionSize;

	ActionQueue = (struct SceneActionQueueType *)MemoryAllocate(QueueSize);

	if (ActionQueue)
	{
		ActionQueue->Tick = 0;
		ActionQueue->Size = ActionSize;
		ActionQueue->Type = Type;
		ActionQueue->Events = ListNew(QueueSize);

		return ActionQueue;
	}

	return NULL;
}

void SceneManagerEventAttach(struct SceneActionQueueType * ActionQueue, uint32 Index, void * Action)
{
	if (ActionQueue && Action)
	{
		struct SceneActionEventType Event = { Index, { 0 } };

		// Copy action
		MemoryCopy(&Event.Entry, Action, ActionQueue->Size);

		// Push into list
		ListPushBack(ActionQueue->Events, (void*)&Event);
	}
}

void SceneManagerEventQueryAttach(struct SceneActionQueueType * ActionQueue,
								  uint32 Index, uint8 Movement, bool Pressed)
{
	struct SceneActionQuery Query = { Movement, Pressed };

	SceneManagerEventAttach(ActionQueue, Index, (void *)&Query);
}

void SceneManagerEventAckAttach(struct SceneActionQueueType * ActionQueue,
								uint32 Index, uint8 Movement,
								struct Vector3f * Position, struct Vector3f * Rotation)
{
	struct SceneActionAck Ack = { Movement, { 0 }, { 0 } };

	Vector3fCopy(&Ack.Position, Position);

	Vector3fCopy(&Ack.Rotation, Rotation);

	SceneManagerEventAttach(ActionQueue, Index, (void *)&Ack);
}

TickFuncRet SceneActionEventHandler(TickData Data)
{
	// TODO
	/*
	struct SceneActionQueueType * ActionQueue = (struct SceneActionQueueType *)Data;

	ListIterator ActionIterator;

	ListIterator EntityIterator;

	List UpdateEntityList = ListNew(sizeof(struct EntityType *));

	List WitnessList = ListNew(sizeof(struct EntityType *));

	// For each queued action
	ListForEach(ActionQueue->Events, ActionIterator)
	{
		ListIterator WitnessIterator;

		// Find entity in the octree
		struct EntityType * Entity = EntityManagerGet(	ActionQueue->EntityManager,
														ListItData(ActionIterator, struct SceneActionEventType).Index);

		// Update entity in the scene with specified action
		SceneManagerEntityMove(	ActionQueue->EntityManager,
								Entity,
								ListItData(ActionIterator, struct SceneActionEventType).Entry.Query.Movement,
								ListItData(ActionIterator, struct SceneActionEventType).Entry.Query.Pressed,
								WitnessList);

		// Append entity to each witness
		ListForEach(WitnessList, WitnessIterator)
		{
			// Append to witness the updated entity
			EntityManagerAppendUpdate(ActionQueue->EntityManager, WitnessIterator, Entity);
		}

		// Clear list
		ListClear(WitnessList);
	}

	// Destroy witness list
	ListDestroy(WitnessList);

	// Get entities need to be updated
	EntityManagerGetUpdateList(ActionQueue->EntityManager, UpdateEntityList);

	// For each entity needs update
	ListForEach(UpdateEntityList, EntityIterator)
	{
		// Get user attached to entity (player)
		// EntityIt->Attr...

		// Send world update packet
		// PacketSendWorldUpdate(User);
	}

	// Destroy lists
	ListDestroy(UpdateEntityList);

	*/

	return NULL;
}
