/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include <Graphics/Color.h>
#include <Math/Geometry/Vector.h>

#define BATCH_RENDER_DEFAULT_VERTICES	1024



/*

struct VertexCache * BatchRenderCreate(uint32 Size)
{
	struct VertexCache * Cache = (struct VertexCache*)(Allocate(sizeof(struct VertexCache)));

	if (!Cache)
		return NULL;

	Cache->List = (struct VertexInfo*)(Allocate(sizeof(struct VertexInfo) * Size));

	if (!Cache->List)
	{
		Delete(Cache);
		return NULL;
	}

	Cache->Size = Size;
	Cache->Atlas = NULL;

	BatchRenderReset(Cache);

	return Cache;
}

void BatchRenderReset(struct VertexCache * Cache, bool ResetMemory)
{
	if (Cache)
	{
		// Clear the list
		if (Cache->List && ResetMemory)
		{
			memset(Cache->List, 0, sizeof(struct VertexInfo) * Cache->Size);
		}

		// Reset the size
		Cache->Count = 0;
	}
}

struct BatchRenderDestroy(struct VertexCache * Cache)
{
	if (Cache)
	{
		BatchRenderReset(Cache);
		Delete(Cache);
	}
}

void BatchRenderSetTexture(struct VertexCache * Cache, struct Texture * TexData)
{
	if (Cache && TexData)
	{
		if (Cache->Atlas && TexData->Id != Cache->Atlas->Id)
		{
			BatchRenderFlush(Cache);
		}

		Cache->Atlas = TexData;
	}
}

void BatchRenderAddVertex(struct VertexCache * Cache, struct Vector2f * Vertor, union CoordType * Coord, struct Color4ub * Color)
{
	if (Cache->Count < Cache->Size)
	{
		Vector2fCopy(&Cache->List[Cache->Count].Vector, Vector);
		CoordCopy(&Cache->List[Cache->Count].Coord, Coord);
		Color4ubCopy(&Cache->List[Cache->Count].Color, Color);

		Cache->Count++;
	}
}

void BatchRenderFlush(struct VertexCache * Cache)
{
	if (!Cache || Cache->Count == 0)
		return;





	// Bind texture atlas

	// Render vertices

	BatchRenderReset(Cache, false);
}

*/