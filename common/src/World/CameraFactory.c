/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Camera.h>
#include <World/CameraFactory.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define CAMERA_FACTORY_COMPONENTS \
		(WORLD_BUILD_TYPE_BASE, COMPONENT_DATA_BASE, CameraComponentBaseSize), \
		(WORLD_BUILD_TYPE_TRANSFORM, COMPONENT_DATA_TRANSFORM, CameraComponentTransformSize), \
		(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION_FRUSTUM, CameraComponentCollisionSize), \
		(WORLD_BUILD_TYPE_PHYSICS, COMPONENT_DATA_PHYSICS, CameraComponentPhysicsSize), \
		(WORLD_BUILD_TYPE_RENDER, COMPONENT_DATA_RENDER, CameraComponentRenderSize)

#define CAMERA_COMPONENT_MASK \
		EntityComponentMaskDefine( \
			(WORLD_BUILD_TYPE_BASE, COMPONENT_DATA_BASE), \
			(WORLD_BUILD_TYPE_TRANSFORM, COMPONENT_DATA_TRANSFORM), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION_FRUSTUM), \
			(WORLD_BUILD_TYPE_PHYSICS, COMPONENT_DATA_PHYSICS), \
			(WORLD_BUILD_TYPE_RENDER, COMPONENT_DATA_RENDER) \
		)

#define CAMERA_COMPONENT_TYPES \
		EntityComponentTypesDefine( \
			(WORLD_BUILD_TYPE_BASE, COMPONENT_DATA_BASE, &CameraComponentBaseImpl), \
			(WORLD_BUILD_TYPE_TRANSFORM, COMPONENT_DATA_TRANSFORM, &CameraComponentTransformImpl), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION, NULL), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION_BOX, NULL), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION_SPHERE, NULL), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION_FRUSTUM, &CameraComponentCollisionImpl), \
			(WORLD_BUILD_TYPE_ANIMATION, COMPONENT_DATA_ANIMATION, NULL), \
			(WORLD_BUILD_TYPE_PHYSICS, COMPONENT_DATA_PHYSICS, &CameraComponentPhysicsImpl), \
			(WORLD_BUILD_TYPE_RENDER, COMPONENT_DATA_RENDER, &CameraComponentRenderImpl), \
			(WORLD_BUILD_TYPE_OCCLUSION, COMPONENT_DATA_OCCLUSION, NULL), \
			(WORLD_BUILD_TYPE_SOUND, COMPONENT_DATA_SOUND, NULL) \
		)

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize component pool of camera factory
////////////////////////////////////////////////////////////
bool CameraFactoryInitialize()
{
	// Create all component pools depending on build compilation
	EntityFactoryComponentCreateDefine(CameraFactoryGet(), CAMERA_FACTORY_COMPONENTS);

	return true;
}

////////////////////////////////////////////////////////////
/// Get camera factory instance
////////////////////////////////////////////////////////////
struct EntityFactoryType * CameraFactoryGet()
{
    static struct EntityFactoryType EntityCameraFactory = {
        ENTITY_TYPE_CAMERA,
        TypeInit(Create, &CameraCreate),
        TypeInit(Destroy, &CameraDestroy),
        TypeInit(InitializeImpl, &CameraFactoryInitialize),
        TypeInit(DestroyImpl, &CameraFactoryDestroy),
        TypeInit(EntityPool, NULL),
        TypeInit(EntityFreeIds, NULL),
        TypeInit(ComponentMap, NULL),
        TypeInit(ComponentDataMask, CAMERA_COMPONENT_MASK),
        ExprInit(ComponentTypes, CAMERA_COMPONENT_TYPES)
    };

	return &EntityCameraFactory;
}

////////////////////////////////////////////////////////////
/// Destroy component pool of camera factory
////////////////////////////////////////////////////////////
void CameraFactoryDestroy()
{
	// Destroy all component pools depending on build compilation
	EntityFactoryComponentDestroyDefine(CameraFactoryGet(), CAMERA_FACTORY_COMPONENTS);
}
