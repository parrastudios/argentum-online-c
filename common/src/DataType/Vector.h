/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATATYPE_VECTOR_H
#define DATATYPE_VECTOR_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define VECTOR_CAPACITY_INCREMENT	2	//< Capacity increment
#define VECTOR_CAPACITY_MIN			16	//< Minimum capacity
#define VECTOR_CAPACITY_MIN_USED	8	//< Max unused capacity since free memory

////////////////////////////////////////////////////////////
// Data structure
////////////////////////////////////////////////////////////

// The variables of the structure can not be accessed directly,
// you may use the functions over the Vector type to access the data.
struct VectorType
{
	uinteger	TypeSize;
	uinteger	Capacity;
    uinteger	Size;
    void *		Data;
};

typedef struct VectorType * Vector;

////////////////////////////////////////////////////////////
// Function prototypes
////////////////////////////////////////////////////////////

Vector VectorNew(uinteger TypeSize);
Vector VectorNewReserve(uinteger Size, uinteger TypeSize);
void VectorDestroy(Vector v);
void VectorReserve(Vector v, uinteger Capacity);
void VectorResize(Vector v, uinteger Size);
void VectorClear(Vector v);
//macro: Vector VectorNewT(typename Type);
//macro: Vector VectorNewReserveT(Size, typename Type);

uinteger VectorCapacity(Vector v);
uinteger VectorSize(Vector v);
uinteger VectorTypeSize(Vector v);
bool VectorEmpty(Vector v);

void * VectorFront(Vector v);
void * VectorBack(Vector v);
void * VectorAt(Vector v, uinteger Position);
void VectorSet(Vector v, uinteger Position, void * Element);
//macro: Type& VectorFrontT(Vector v, typename Type);
//macro: Type& VectorBackT(Vector v, typename Type);
//macro: Type& VectorAtT(Vector v, uinteger Position, typename Type);
//macro: void VectorSetV(Vector v, uinteger Position, Type Variable);
//macro: void VectorSetC(Vector v, uinteger Position, const Type Constant, typename Type);

void VectorPushBackEmpty(Vector v);
void VectorPushBack(Vector v, void * Element);
void VectorPopBack(Vector v);
//macro: void VectorPushBackV(Vector v, Type Variable);
//macro: void VectorPushBackC(Vector v, const Type Constant, typename Type);

void VectorPushFrontEmpty(Vector v);
void VectorPushFront(Vector v, void * Element);
void VectorPopFront(Vector v);
//macro: void VectorPushFrontV(Vector v, Type Variable);
//macro: void VectorPushFrontC(Vector v, const Type Constant, typename Type);

void VectorInsertEmpty(Vector v, uinteger Position);
void VectorInsert(Vector v, uinteger Position, void * Element);
void VectorErase(Vector v, uinteger Position);
//macro: void VectorInsertV(Vector v, uinteger Position, Type Variable);
//macro: void VectorInsertC(Vector v, uinteger Position, const Type Constant, typename Type);

////////////////////////////////////////////////////////////
// Helper macros
////////////////////////////////////////////////////////////
#define VectorTypeFatalError(Error) do { /* ... */ } while (0)
#define VectorTypeDataPosbytes(v, Bytes) ((void*) (((char*)v->Data) + (Bytes)))

////////////////////////////////////////////////////////////
// Public macros
////////////////////////////////////////////////////////////

// Creates a new vector to content elements of type 'Type'
#define VectorNewT(Type) \
    VectorNew(sizeof(Type))
// Creates a new vector of size 'Size' to content elemenents of type 'Type'
#define VectorNewReserveT(Size, Type) \
    VectorNewReserve(Size, sizeof(Type))

// Returns a reference to a object of type 'Type'
//   (Change this objects will change the objects in the vector)
#define VectorFrontT(v, Type) \
    (*((Type*) VectorFront(v)))
#define VectorBackT(v, Type) \
    (*((Type*) VectorBack(v)))
#define VectorAtT(v, Position, Type) \
    (*((Type*) VectorAt(v, Position)))

// Adds objects to a vector
//   ('Variable' have to be a variable name, not a constant)
#define VectorSetV(v, Position, Variable) \
    VectorSet(v, Position, &Variable);
#define VectorPushBackV(v, Variable) \
    VectorPushBack(v, &Variable);
#define VectorPushFrontV(v, Variable) \
    VectorPushFront(v, &Variable);
#define VectorInsertV(v, Position, Variable) \
    VectorInsert(v, Position, &Variable);

#define VectorSetC(v, Position, Constant, Type) \
    do { Type cnst = Constant; \
        VectorSet(v, Position, &cnst); } while (0)
#define VectorPushBackC(v, Constant, Type) \
    do { Type cnst = Constant; \
        VectorPushBack(v, &cnst); } while (0)
#define VectorPushFrontC(v, Constant, Type) \
    do { Type cnst = Constant; \
        VectorPushFront(v, &cnst); } while (0)
#define VectorInsertC(v, Position, Constant, Type) \
    do { Type cnst = Constant; \
        VectorInsert(v, Position, &cnst); } while (0)

#endif // DATATYPE_VECTOR_H
