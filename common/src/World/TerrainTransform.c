/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/TerrainTransform.h>

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a transform component of a terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainTransformCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a terrain transform component
////////////////////////////////////////////////////////////
void TerrainTransformDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the terrain transform implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * TerrainComponentTransformImpl()
{
	static const struct ComponentImplType TerrainComponentTransformTypeImpl = {
		ExprInit(Base, TypeInit(Create, &TerrainTransformCreate), TypeInit(Destroy, &TerrainTransformDestroy)),
		TypeInit(Type, COMPONENT_TYPE_TRANSFORM),
		UnionInit(Attributes, Transform, &EntityComponentTransformAttr)
	};

	return &TerrainComponentTransformTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a transform component of a terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainTransformCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

		// ...

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a terrain transform component
////////////////////////////////////////////////////////////
void TerrainTransformDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get transform component
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the terrain transform data size
////////////////////////////////////////////////////////////
uinteger TerrainComponentTransformSize()
{
	return COMPONENT_DATA_LENGTH_DEFAULT;
}
