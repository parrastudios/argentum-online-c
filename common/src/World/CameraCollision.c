/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/CameraCollision.h>

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a collision shapere component of a camera
////////////////////////////////////////////////////////////
struct EntityType * CameraCollisionCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a camera collision sphere component
////////////////////////////////////////////////////////////
void CameraCollisionDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the camera collision implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * CameraComponentCollisionImpl()
{
	static const struct ComponentImplType CameraComponentCollisionFrustumTypeImpl = {
		ExprInit(Base, TypeInit(Create, &CameraCollisionCreate), TypeInit(Destroy, &CameraCollisionDestroy)),
		TypeInit(Type, COMPONENT_TYPE_COLLISION),
		UnionInit(Attributes, Collision, &EntityComponentCollisionFrustumAttr)
	};

	return &CameraComponentCollisionFrustumTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a collision shapere component of a camera
////////////////////////////////////////////////////////////
struct EntityType * CameraCollisionCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Create transform component data reference
        struct ComponentType * Transform = EntityComponentTransform(Entity);
        struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

		// Create collision component data reference
        struct ComponentType * Collision = EntityComponentCollisionFrustum(Entity);
        struct ComponentCollisionFrustumData * CollisionData = ComponentDataCollisionFrustum(Collision);

		// ..

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a camera collision sphere component
////////////////////////////////////////////////////////////
void CameraCollisionDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Create collision component data reference
		struct ComponentType * Collision = EntityComponentCollisionFrustum(Entity);
		struct ComponentCollisionFrustumData * CollisionData = ComponentDataCollisionFrustum(Collision);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the camera collision data size
////////////////////////////////////////////////////////////
uinteger CameraComponentCollisionSize()
{
    return COMPONENT_DATA_LENGTH_DEFAULT;
}
