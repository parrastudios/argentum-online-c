/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_LIGHT_RENDER_H
#define WORLD_LIGHT_RENDER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
static struct ComponentRenderType	ComponentLightRenderAttr;

static struct ComponentImplType		ComponentLightRenderImpl;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct LightRenderComponentData
{
	uinteger			DisplayList;
	uinteger			FrontShadowFBOId;
	uinteger			FrontShadowTextureId;
	uinteger			BackShadowFBOId;
	uinteger			BackShadowTextureId;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a render light entity
////////////////////////////////////////////////////////////
struct EntityType * LightRenderCreate(struct EntityType * Entity, void * Data);

////////////////////////////////////////////////////////////
/// Destroy a render light entity
////////////////////////////////////////////////////////////
void LightRenderDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Render a light entity
////////////////////////////////////////////////////////////
void LightRender(struct EntityType * Entity);

#endif // WORLD_LIGHT_RENDER_H
