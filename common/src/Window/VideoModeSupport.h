/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WINDOW_VIDEOMODE_SUPPORT_H
#define WINDOW_VIDEOMODE_SUPPORT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

struct VideoMode
{
    uint32 Width;        ///< Video mode width, in pixels
    uint32 Height;       ///< Video mode height, in pixels
    uint32 BitsPerPixel; ///< Video mode pixel depth, in bits per pixels (32 by default)
};

struct VideoModeStruct
{
	uint32 Size;
	struct VideoMode * Modes;
};

struct VideoModeStruct VideoModeListDefault;

////////////////////////////////////////////////////////////
/// Get supported video modes
///
/// \param Modes : Array to fill with available video modes
///
////////////////////////////////////////////////////////////
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT

	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>

    void VideoModeGetSupportedModes(struct VideoModeStruct * ModeList);

#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD

    #include <X11/Xlib.h>
    #include <X11/extensions/Xrandr.h>

    void VideoModeGetSupportedModes(struct VideoModeStruct * ModeList, Display* Disp, int32 Screen, XIM * InputMethod);

#endif

////////////////////////////////////////////////////////////
/// Get current desktop video mode
///
/// \return Current desktop video mode
///
////////////////////////////////////////////////////////////
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT

    void VideoModeGetDesktopMode(struct VideoMode * Mode);

#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD

    void VideoModeGetDesktopMode(struct VideoMode * Mode, Display* Disp, int32 * Screen, XIM * InputMethod);

#endif

////////////////////////////////////////////////////////////
/// Open the display (if not already done) (X11 Impl)
///
/// \return True if the display is properly opened
///
////////////////////////////////////////////////////////////
#if PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD

	bool OpenDisplay(Display* Disp, int32 * Screen, XIM * InputMethod);

#endif

#endif // WINDOW_VIDEOMODE_SUPPORT_H
