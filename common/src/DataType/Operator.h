/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATATYPE_OPERATOR_H
#define DATATYPE_OPERATOR_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define OPERATOR_EQUAL			0x01 << 0x00
#define OPERATOR_LESS			0x01 << 0x01
#define OPERATOR_GREATER		0x01 << 0x02
#define OPERATOR_NOT_EQUAL		OPERATOR_LESS | OPERATOR_GREATER
#define OPERATOR_LESS_EQUAL		OPERATOR_LESS | OPERATOR_EQUAL
#define OPERATOR_GREATER_EQUAL	OPERATOR_GREATER | OPERATOR_EQUAL

////////////////////////////////////////////////////////////
// Enumerations
////////////////////////////////////////////////////////////
enum CompareFunctionIdType
{
	COMPARE_FUNC_CSTRING = 0x00,
	COMPARE_FUNC_UINTEGER = 0x01,

	COMPARE_FUNC_SIZE
};

////////////////////////////////////////////////////////////
/// Generic operator type macro
////////////////////////////////////////////////////////////
typedef uinteger OperatorType;

////////////////////////////////////////////////////////////
/// A comparison callback must return any type defined before
/// If we have two complex structs nammed ComplexStructA
/// and ComplexStructB, and we want to check if A is greater
/// than B we can check returning value by this way:
///
/// if (MyCompareCallback(ComplexStructA, ComplexStructB) & 
///	   OPERATOR_GREATER)
///	{
///		// ComplexStructA > ComplexStructB
///	}
////////////////////////////////////////////////////////////
typedef OperatorType (*CompareCallback)(void *, void *);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get default comparison functions
////////////////////////////////////////////////////////////
CompareCallback CompareGetCallback(uinteger CompareFuncId);

#endif // DATATYPE_OPERATOR_H
