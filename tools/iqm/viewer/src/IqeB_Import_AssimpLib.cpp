/*
***********************************************************************************
 IqeB_Import_AssimpLib.cpp

 Import a file via the assimb library (Open Asset Import Library).

20.12.2014 RR: * First editon of this file.
                 Base was the SimpleOpenGL file of the assimb download.
23.12.2014 RR: * From the asstools on gituhb, worked in the file
                 assiqe.c (an Open Asset Import Library based IQE exporter).

05.04.2015 RR: * Bugfix after a crash with many bones.
                 The model got abaout 1200 bones but the bonelist[] was
                 coded for 200 entries.
                 * define 'ASSIMP_IMPORT_MAX_BONES' with 16 K as
                   number of max bones.
                 * build_bone_list_from_nodes()
                   return if bonde list is full.

*/

#include "IqeBrowser.h"

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

//to map image filenames to textureIds
#include <string.h>
#include <map>

// assimp include files. These three are usually needed.
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

/************************************************************************************
 * Settings
 */

static int verbose = 0;           // print
static int need_to_bake_skin = 0; // bake mesh to bind pose / initial frame
static int save_all_bones = 0;    // 1: export all child bones, 2: export all bones (including unused ones)

static int dostatic = 0;          // static mesh only (no skeleton)
static int dorigid = 0;           // export rigid (non-deformed) nodes as bones too (experimental)
static int domesh = 1;            // export mesh
static int doanim = 0;            // export animations
static int dobone = 0;            // export skeleton
static int doflip = 0;            // export flipped (quake-style clockwise winding) triangles

static int doaxis = 0;            // flip bone axis from X to Y to match blender
static int dounscale = 0;         // remove scaling from bind pose
static int dohips = 0;            // reparent thighs to pelvis (see zo_hom_marche)

/************************************************************************************
 * defines and static data
 */

#define MAX_UVMAP 4
#define FIRST_UVMAP 0
#define MAX_COL 4
#define FIRST_COL 4

// We use %.9g to print floats with 9 digits of precision which
// is enough to represent a 32-bit float accurately, while still
// shortening if possible to save space for all those 0s and 1s.

#define EPSILON 0.00001
#define NEAR_0(x) (fabs((x)) < EPSILON)
#define NEAR_1(x) (NEAR_0((x)-1))
#define KILL_0(x) (NEAR_0((x)) ? 0 : (x))
#define KILL_N(x,n) (NEAR_0((x)-(n)) ? (n) : (x))
#define KILL(x) KILL_0(KILL_N(KILL_N(x, 1), -1))

#define LOWP(x) (roundf(x*32768)/32768)

static int fix_hips(int verbose);
static void unfix_hips(void);

static aiMatrix4x4 yup_to_zup(
	1, 0, 0, 0,
	0, 0, -1, 0,
	0, 1, 0, 0,
	0, 0, 0, 1
);

static aiMatrix4x4 axis_x_to_y(
	0, 1, 0, 0,
	-1, 0, 0, 0,
	0, 0, 1, 0,
	0, 0, 0, 1
);

/************************************************************************************
 * Helper
 */

static double aiDeterminant( aiMatrix4x4 *m)
{
	return (double)
		m->a1*m->b2*m->c3*m->d4 - m->a1*m->b2*m->c4*m->d3 +
		m->a1*m->b3*m->c4*m->d2 - m->a1*m->b3*m->c2*m->d4 +
		m->a1*m->b4*m->c2*m->d3 - m->a1*m->b4*m->c3*m->d2 -
		m->a2*m->b3*m->c4*m->d1 + m->a2*m->b3*m->c1*m->d4 -
		m->a2*m->b4*m->c1*m->d3 + m->a2*m->b4*m->c3*m->d1 -
		m->a2*m->b1*m->c3*m->d4 + m->a2*m->b1*m->c4*m->d3 +
		m->a3*m->b4*m->c1*m->d2 - m->a3*m->b4*m->c2*m->d1 +
		m->a3*m->b1*m->c2*m->d4 - m->a3*m->b1*m->c4*m->d2 +
		m->a3*m->b2*m->c4*m->d1 - m->a3*m->b2*m->c1*m->d4 -
		m->a4*m->b1*m->c2*m->d3 + m->a4*m->b1*m->c3*m->d2 -
		m->a4*m->b2*m->c3*m->d1 + m->a4*m->b2*m->c1*m->d3 -
		m->a4*m->b3*m->c1*m->d2 + m->a4*m->b3*m->c2*m->d1;
}

static void aiInverseMatrix( aiMatrix4x4 *p, aiMatrix4x4 *m)
{
	double det = aiDeterminant(m);
	assert(det != 0.0);
	double invdet = 1.0 / det;
	p->a1= invdet * (m->b2*(m->c3*m->d4-m->c4*m->d3) + m->b3*(m->c4*m->d2-m->c2*m->d4) + m->b4*(m->c2*m->d3-m->c3*m->d2));
	p->a2=-invdet * (m->a2*(m->c3*m->d4-m->c4*m->d3) + m->a3*(m->c4*m->d2-m->c2*m->d4) + m->a4*(m->c2*m->d3-m->c3*m->d2));
	p->a3= invdet * (m->a2*(m->b3*m->d4-m->b4*m->d3) + m->a3*(m->b4*m->d2-m->b2*m->d4) + m->a4*(m->b2*m->d3-m->b3*m->d2));
	p->a4=-invdet * (m->a2*(m->b3*m->c4-m->b4*m->c3) + m->a3*(m->b4*m->c2-m->b2*m->c4) + m->a4*(m->b2*m->c3-m->b3*m->c2));
	p->b1=-invdet * (m->b1*(m->c3*m->d4-m->c4*m->d3) + m->b3*(m->c4*m->d1-m->c1*m->d4) + m->b4*(m->c1*m->d3-m->c3*m->d1));
	p->b2= invdet * (m->a1*(m->c3*m->d4-m->c4*m->d3) + m->a3*(m->c4*m->d1-m->c1*m->d4) + m->a4*(m->c1*m->d3-m->c3*m->d1));
	p->b3=-invdet * (m->a1*(m->b3*m->d4-m->b4*m->d3) + m->a3*(m->b4*m->d1-m->b1*m->d4) + m->a4*(m->b1*m->d3-m->b3*m->d1));
	p->b4= invdet * (m->a1*(m->b3*m->c4-m->b4*m->c3) + m->a3*(m->b4*m->c1-m->b1*m->c4) + m->a4*(m->b1*m->c3-m->b3*m->c1));
	p->c1= invdet * (m->b1*(m->c2*m->d4-m->c4*m->d2) + m->b2*(m->c4*m->d1-m->c1*m->d4) + m->b4*(m->c1*m->d2-m->c2*m->d1));
	p->c2=-invdet * (m->a1*(m->c2*m->d4-m->c4*m->d2) + m->a2*(m->c4*m->d1-m->c1*m->d4) + m->a4*(m->c1*m->d2-m->c2*m->d1));
	p->c3= invdet * (m->a1*(m->b2*m->d4-m->b4*m->d2) + m->a2*(m->b4*m->d1-m->b1*m->d4) + m->a4*(m->b1*m->d2-m->b2*m->d1));
	p->c4=-invdet * (m->a1*(m->b2*m->c4-m->b4*m->c2) + m->a2*(m->b4*m->c1-m->b1*m->c4) + m->a4*(m->b1*m->c2-m->b2*m->c1));
	p->d1=-invdet * (m->b1*(m->c2*m->d3-m->c3*m->d2) + m->b2*(m->c3*m->d1-m->c1*m->d3) + m->b3*(m->c1*m->d2-m->c2*m->d1));
	p->d2= invdet * (m->a1*(m->c2*m->d3-m->c3*m->d2) + m->a2*(m->c3*m->d1-m->c1*m->d3) + m->a3*(m->c1*m->d2-m->c2*m->d1));
	p->d3=-invdet * (m->a1*(m->b2*m->d3-m->b3*m->d2) + m->a2*(m->b3*m->d1-m->b1*m->d3) + m->a3*(m->b1*m->d2-m->b2*m->d1));
	p->d4= invdet * (m->a1*(m->b2*m->c3-m->b3*m->c2) + m->a2*(m->b3*m->c1-m->b1*m->c3) + m->a3*(m->b1*m->c2-m->b2*m->c1));
}

static void aiIdentityMatrix4( aiMatrix4x4 *m)
{

  // set to identity
	m->a1 = 1.0;
	m->a2 = 0.0;
	m->a3 = 0.0;
	m->a4 = 0.0;
	m->b1 = 0.0;
	m->b2 = 1.0;
	m->b3 = 0.0;
	m->b4 = 0.0;
	m->c1 = 0.0;
	m->c2 = 0.0;
	m->c3 = 1.0;
	m->c4 = 0.0;
	m->d1 = 0.0;
	m->d2 = 0.0;
	m->d3 = 0.0;
	m->d4 = 1.0;
}

// Matrix multiplication
static void aiMultiplyMatrix4( aiMatrix4x4 *dst, aiMatrix4x4 *src)
{

	*dst = (*dst) * (*src);
}

static void aiComposeMatrix( aiMatrix4x4 *m, aiVector3D *scale, aiQuaternion *q, aiVector3D *pos)
{
  aiMatrix4x4 smat;

	aiIdentityMatrix4(m);

	m->a1 = 1.0f - 2.0f * (q->y * q->y + q->z * q->z);
	m->a2 = 2.0f * (q->x * q->y - q->z * q->w);
	m->a3 = 2.0f * (q->x * q->z + q->y * q->w);
	m->b1 = 2.0f * (q->x * q->y + q->z * q->w);
	m->b2 = 1.0f - 2.0f * (q->x * q->x + q->z * q->z);
	m->b3 = 2.0f * (q->y * q->z - q->x * q->w);
	m->c1 = 2.0f * (q->x * q->z - q->y * q->w);
	m->c2 = 2.0f * (q->y * q->z + q->x * q->w);
	m->c3 = 1.0f - 2.0f * (q->x * q->x + q->y * q->y);

	aiIdentityMatrix4(&smat);
	smat.a1 = scale->x;
	smat.b2 = scale->y;
	smat.c3 = scale->z;

	aiMultiplyMatrix4(m, &smat);

	m->a4 = pos->x; m->b4 = pos->y; m->c4 = pos->z;
}

static void aiNormalizeQuaternion( aiQuaternion *q)
{
	const float mag = sqrt(q->x*q->x + q->y*q->y + q->z*q->z + q->w*q->w);
	if (mag)
	{
		const float invMag = 1.0f/mag;
		q->x *= invMag;
		q->y *= invMag;
		q->z *= invMag;
		q->w *= invMag;
	}
}

// Matrix decomposition
static void aiDecomposeMatrix(const aiMatrix4x4* mat,aiVector3D* scaling,
	aiQuaternion* rotation,
	aiVector3D* position)
{
	mat->Decompose(*scaling,*rotation,*position);
}

// ------------------------------------------------------------------------------------------------
// Vector transformation
static void aiTransformVecByMatrix3(aiVector3D* vec,
	const aiMatrix3x3* mat)
{

	*vec *= (*mat);
}

// ------------------------------------------------------------------------------------------------
static void aiTransformVecByMatrix4(aiVector3D* vec,
	const aiMatrix4x4* mat)
{

	*vec *= (*mat);
}

#ifdef use_again // 23.12.2014 RR: not used for IqeBrowser
static void print_matrix( aiMatrix4x4 *m)
{
#ifdef use_again
	printf("matrix %g %g %g %g %g %g %g %g %g (det=%g)\n",
			m->a1, m->a2, m->a3,
			m->b1, m->b2, m->b3,
			m->c1, m->c2, m->c3,
			aiDeterminant(m));
#endif
}
#endif

static int is_identity_matrix( aiMatrix4x4 *m)
{
	return
		NEAR_1(m->a1) && NEAR_0(m->a2) && NEAR_0(m->a3) &&
		NEAR_0(m->b1) && NEAR_1(m->b2) && NEAR_0(m->b3) &&
		NEAR_0(m->c1) && NEAR_0(m->c2) && NEAR_1(m->c3) &&
		NEAR_0(m->a4) && NEAR_0(m->b4) && NEAR_0(m->c4);
}

static char basename[1024];

static int numtags = 0;
static char **taglist = NULL;

static int numuntags = 0;
static char *untaglist[100];

#define MAXBLEND 12
#ifndef MIN
#define MIN(a,b) ((a)<(b)?(a):(b))
#endif

struct vb {
	int b[MAXBLEND];
	float w[MAXBLEND];
	int n;
};

#define MAX_LEN_SHADER 256    // max length of schader string
struct material {
	struct aiMaterial *material;
	char *name;
	char shader[ MAX_LEN_SHADER];
};

static struct material matlist[200];
static int nummats = 0;

struct bone {
	struct aiNode *node;
	char *name;
	char *clean_name;
	int parent;
	int number; // for iqe export
	int isbone;
	int isskin;
	int isrigid;
	char *reason; // reason for selecting
	float unscale[3]; // inverse of scaling factor in bind pose

	// scratch matrices for inverse bind pose and absolute bind pose
  aiMatrix4x4 invpose; // inv(parent * pose)
  aiMatrix4x4 abspose; // (parent * pose)

	// current pose in matrix and decomposed form
  aiMatrix4x4 pose;
  aiVector3D translate;
  aiQuaternion rotate;
  aiVector3D scale;
};

#define ASSIMP_IMPORT_MAX_BONES (1024 * 16)  // Max number of bones to import

static bone bonelist[ ASSIMP_IMPORT_MAX_BONES];
static int numbones = 0;

static int find_bone(char *name)
{
	int i;
	for (i = 0; i < numbones; i++)
		if (!strcmp(name, bonelist[i].name))
			return i;
	return -1;
}

static char *get_base_name(char *s)
{
	char *p = strrchr(s, '/');
	if (!p) p = strrchr(s, '\\');
	if (!p) return s;
	return p + 1;
}

static char *clean_node_name(char *p)
{
	static char buf[200];
	if (strstr(p, "node-") == p)
		p += 5;
	strcpy(buf, p);
	for (p = buf; *p; p++) {
		*p = tolower(*p);
		if (*p == ' ') *p = '_';
	}
	return strdup(buf); // leak like a sieve
}

#ifdef use_again  // may be used later
static char *clean_material_name(char *p)
{
	static char buf[ 512];    // buffer must be static
	strcpy(buf, p);
	p = strstr(buf, "-material");
	if (p) *p = 0;
	for (p = buf; *p; p++) {
		*p = tolower(*p);
		if (*p == ' ') *p = '_';
		if (*p == '#') *p = '_';
	}
	return buf;   // return pointer to static buffer
}
#endif

static char *find_material(struct aiMaterial *material)
{
	static aiString str;
	char shader[ MAX_LEN_SHADER], *p;
	char *name;
	int i;

	for (i = 0; i < nummats; i++)
		if (matlist[i].material == material)
			return matlist[i].shader;

	aiGetMaterialString(material, AI_MATKEY_NAME, &str);
	name = str.data;

#ifdef use_gain
	strcpy(shader, clean_material_name(name));
	strcat(shader, "+");
	if (!aiGetMaterialString(material, AI_MATKEY_TEXTURE_DIFFUSE(0), &str)) {
		strcat(shader, get_base_name(str.data));
	} else {
		strcat(shader, "unknown");
	}

	p = strrchr(shader, '.');
	if (p) *p = 0;
#else

	if( AI_SUCCESS == material->GetTexture(aiTextureType_DIFFUSE, 0, &str)) {

    strncpy( shader, get_base_name( (char *)str.C_Str()), sizeof( shader));
    shader[ sizeof( shader) - 1] = '\0';

    if( !strcasecmp( shader, "$texture_dummy.bmp")) {  // Catch texture dummy

      // Get base filename of model

      strcpy( shader, basename);

      // add .tga as file extension

      strcat( shader, ".tga");
    }
  } else {

    strcpy( shader, "skin.tga");   // no diffuse material, add default
  }
#endif

  // shader string to lowercase
	p = shader;
  while( *p) {
    *p = tolower(*p);
    p++;
  }

	matlist[nummats].name = name;
	matlist[nummats].material = material;
	strcpy( matlist[nummats].shader, shader);
	return matlist[nummats++].shader;
}

// --- figure out which bones are part of armature ---

static void build_bone_list_from_nodes(struct aiNode *node, int parent, char *clean_name)
{
	int i;

	if( numbones >= ASSIMP_IMPORT_MAX_BONES) {    // Security test

    return;
	}

	// inherit clean names for auto-inserted nodes
	if (!strstr(node->mName.data, "$ColladaAutoName$"))
		clean_name = clean_node_name((char*)node->mName.data);

  bonelist[numbones].name = node->mName.data;
  bonelist[numbones].clean_name = clean_name;
  bonelist[numbones].parent = parent;
  bonelist[numbones].reason = (char *)"<none>";
  bonelist[numbones].isbone = 0;
  bonelist[numbones].isskin = 0;
  bonelist[numbones].isrigid = 0;
  bonelist[numbones].node = node;

  // these are set in calc_bind_pose and/or apply_initial_frame
  aiIdentityMatrix4(&bonelist[numbones].pose);
  aiIdentityMatrix4(&bonelist[numbones].abspose);
  aiIdentityMatrix4(&bonelist[numbones].invpose);

  parent = numbones++;

	for (i = 0; i < (int)node->mNumChildren; i++) {

		build_bone_list_from_nodes(node->mChildren[i], parent, clean_name);
	}
}

static void apply_initial_frame(void)
{
	int i;
	for (i = 0; i < numbones; i++) {
		// restore original transformation
		bonelist[i].pose = bonelist[i].node->mTransformation;
		// ... and update rotate/translate/scale
		aiDecomposeMatrix(&bonelist[i].pose, &bonelist[i].scale, &bonelist[i].rotate, &bonelist[i].translate);
	}
}

// recalculate abspose from local pose matrix
static void calc_abs_pose(void)
{
	int i;
	for (i = 0; i < numbones; i++) {
		bonelist[i].abspose = bonelist[i].pose;
		if (bonelist[i].parent >= 0) {
			bonelist[i].abspose = bonelist[bonelist[i].parent].abspose;
			aiMultiplyMatrix4(&bonelist[i].abspose, &bonelist[i].pose);
		}
	}
}

static void calc_bind_pose(void)
{
	// we now (in the single mesh / non-baking case) have our bind pose
	// our invpose is set to the inv_bind_pose matrix
	// compute forward abspose and pose matrices here
	int i;
	for (i = 0; i < numbones; i++) {
		if (bonelist[i].isskin) {
			// skinned and boned, invpose is our reference
			aiInverseMatrix(&bonelist[i].abspose, &bonelist[i].invpose);
			bonelist[i].pose = bonelist[i].abspose;
			if (bonelist[i].parent >= 0) {
        aiMatrix4x4 m = bonelist[bonelist[i].parent].invpose;
				aiMultiplyMatrix4(&m, &bonelist[i].pose);
				bonelist[i].pose = m;
			}
		} else {
			// not skinned, so no invpose. pose is our reference
			bonelist[i].pose = bonelist[i].node->mTransformation;
			bonelist[i].abspose = bonelist[i].pose;
			if (bonelist[i].parent >= 0) {
				bonelist[i].abspose = bonelist[bonelist[i].parent].abspose;
				aiMultiplyMatrix4(&bonelist[i].abspose, &bonelist[i].pose);
			}
			aiInverseMatrix(&bonelist[i].invpose, &bonelist[i].abspose);
		}
	}

	// compute translate/rotate/scale
	for (i = 0; i < numbones; i++)
		if (bonelist[i].isbone)
			aiDecomposeMatrix(&bonelist[i].pose, &bonelist[i].scale, &bonelist[i].rotate, &bonelist[i].translate);
}

static void mark_bone_parents(int i)
{
	while (i >= 0) {
		if (!bonelist[i].isbone) {
			bonelist[i].reason = (char *)"parent";
			bonelist[i].isbone = 1;
		}
		i = bonelist[i].parent;
	}
}

static void mark_tags(void)
{
	int i, k;
	for (k = 0; k < numtags; k++) {
		for (i = 0; i < numbones; i++) {
			if (!strcmp(taglist[k], bonelist[i].clean_name)) {
				//x/fprintf(stderr, "marking tag %s\n", taglist[k]);
				bonelist[i].reason = (char *)"tagged";
				bonelist[i].isbone = 1;
				break;
			}
		}
	}
}

static void unmark_tags(void)
{
	int i, k;
	for (k = 0; k < numuntags; k++) {
		for (i = 0; i < numbones; i++) {
			if (!strcmp(untaglist[k], bonelist[i].clean_name)) {
				//x/fprintf(stderr, "unmarking tag %s\n", untaglist[k]);
				bonelist[i].reason = (char *)"untagged";
				bonelist[i].isbone = 0;
				break;
			}
		}
	}
}

static void mark_skinned_bones(const struct aiScene *scene)
{
	int i, k, a, b;

	for (i = 0; i < numbones; i++) {
		struct aiNode *node = bonelist[i].node;

#ifdef use_again
		if (only_one_node && strcmp(bonelist[i].clean_name, only_one_node))
			continue;
#endif

		for (k = 0; k < (int)node->mNumMeshes; k++) {
			struct aiMesh *mesh = scene->mMeshes[node->mMeshes[k]];
			for (a = 0; a < (int)mesh->mNumBones; a++) {

				b = find_bone(mesh->mBones[a]->mName.data);

				if( b >= 0) {  // found bone

		  		if (!bonelist[b].isbone) {
			  		bonelist[b].reason = (char *)"skinned";
				  	bonelist[b].invpose = mesh->mBones[a]->mOffsetMatrix;
					  bonelist[b].isbone = 1;
					  bonelist[b].isskin = 1;
				  } else if (!need_to_bake_skin) {
					  if (memcmp(&bonelist[b].invpose, &mesh->mBones[a]->mOffsetMatrix, sizeof bonelist[b].invpose))
						  need_to_bake_skin = 1;
				  }
				}
			}
		}
	}
}

static void mark_animated_bones(const struct aiScene *scene)
{
	int i, k, b;
	for (i = 0; i < (int)scene->mNumAnimations; i++) {
		const struct aiAnimation *anim = scene->mAnimations[i];
		for (k = 0; k < (int)anim->mNumChannels; k++) {

			b = find_bone(anim->mChannels[k]->mNodeName.data);

      if( b >= 0) {  // found bone

			  bonelist[b].reason = (char *)"animated";
			  bonelist[b].isbone = 1;
      }
		}
	}
}

static void mark_rigid_bones(const struct aiScene *scene)
{
	int i, k;
	for (i = 0; i < numbones; i++) {
		struct aiNode *node = bonelist[i].node;
		for (k = 0; k < (int)node->mNumMeshes; k++) {
			static aiMesh *mesh = scene->mMeshes[node->mMeshes[k]];
			if (mesh->mNumBones == 0 && !is_identity_matrix(&node->mTransformation)) {
				bonelist[i].isrigid = 1;
			}
		}
		if (bonelist[i].isrigid) {
			bonelist[i].reason = (char *)"rigid";
			bonelist[i].isbone = 1;
		}
	}
}

static int build_bone_list(const struct aiScene *scene)
{
	int number;
	int i;

	build_bone_list_from_nodes(scene->mRootNode, -1, (char *)"SCENE");

	if (dohips) fix_hips(0);

	// we always need the bind pose
	if (doanim || domesh || dorigid)
		mark_skinned_bones(scene);

	if (doanim || save_all_bones)
		mark_animated_bones(scene);

	if (dorigid)
		mark_rigid_bones(scene);

	mark_tags(); // mark special bones named on command line as "tags" to attach stuff
	unmark_tags(); // remove named bones from list

	// select all parents of selected bones
	for (i = 0; i < numbones; i++) {
		if (bonelist[i].isbone)
			mark_bone_parents(i);
	}

	// select all otherwise 'dead' children of selected bones
	if (save_all_bones) {
		for (i = 0; i < numbones; i++) {
			if (!bonelist[i].isbone)
				if (bonelist[i].parent >= 0 && bonelist[bonelist[i].parent].isbone)
					bonelist[i].isbone = 1;
		}
	}

	if (save_all_bones > 1) {
		for (i = 0; i < numbones; i++) {
			bonelist[i].reason = (char *)"useless";
			bonelist[i].isbone = 1;
		}
	}

	// Skip root nodes if it has 1 child and identity transform.
  // Don't add meshes with names "<MD5_Root>", "<MD5_Mesh>" or "<MD5_Hierarchy>"
  // This is added from assimplib MD5Loader.cpp file.

	int TestParentSkip, count;

	for( TestParentSkip = 0; TestParentSkip < 3; TestParentSkip++) {

	  count = 0;
	  for (i = 0; i < numbones; i++)
		  if (bonelist[i].isbone && bonelist[i].parent == TestParentSkip)
		  	count++;

	  if (count == 0) {

	  	bonelist[ TestParentSkip].reason = (char *)"useless root node";
		  bonelist[ TestParentSkip].isbone = 0;
		  bonelist[ TestParentSkip].number = -1;
	  } else if (count == 1 && is_identity_matrix(&bonelist[TestParentSkip].node->mTransformation)) {

	  	bonelist[ TestParentSkip].reason = (char *)"useless root node";
		  bonelist[ TestParentSkip].isbone = 0;
		  bonelist[ TestParentSkip].number = -1;

	  } else if( is_identity_matrix(&bonelist[TestParentSkip].node->mTransformation) &&
               ( strcmp( bonelist[ TestParentSkip].name, "<MD5_Root>") == 0 ||
                 strcmp( bonelist[ TestParentSkip].name, "<MD5_Mesh>") == 0 ||
                 strcmp( bonelist[ TestParentSkip].name, "<MD5_Hierarchy>") == 0) ) {

      // 02.01.2015 RR: Hack for MD5 import
	  	bonelist[ TestParentSkip].reason = (char *)"useless root node";
		  bonelist[ TestParentSkip].isbone = 0;
		  bonelist[ TestParentSkip].number = -1;

	  } else {

	    break;
	  }
	}

	// ...

	if (verbose)
		for (i = 0; i < numbones; i++)
			if (bonelist[i].isbone) {
				//x/fprintf(stderr, "selecting %s bone %s\n", bonelist[i].reason, bonelist[i].clean_name);
			}
	// assign IQE numbers to bones
	number = 0;
	for (i = 0; i < numbones; i++)
		if (bonelist[i].isbone)
			bonelist[i].number = number++;

	if (dohips) unfix_hips();

	calc_bind_pose();

	return number;
}

// --- export poses and animation frames ---

#ifdef use_again
static void export_pm(FILE *out, int i)
{
  aiMatrix4x4 m = bonelist[i].pose;
	fprintf(out, "pm %.9g %.9g %.9g %.9g %.9g %.9g %.9g %.9g %.9g %.9g %.9g %.9g\n",
		KILL(m.a4), KILL(m.b4), KILL(m.c4),
		(m.a1), (m.a2), (m.a3),
		(m.b1), (m.b2), (m.b3),
		(m.c1), (m.c2), (m.c3));
}
#endif

static void export_pq( IQE_model *pIQEmodel, int i, int *pPose_count, struct pose *pPose, int MaxBones)
{
  aiQuaternion rotate = bonelist[i].rotate;
  aiVector3D scale = bonelist[i].scale;
  aiVector3D translate = bonelist[i].translate;

	if( *pPose_count < MaxBones && *pPose_count < MAXBONE ) {
    pPose[ *pPose_count].location[0] = KILL(translate.x);
		pPose[ *pPose_count].location[1] = KILL(translate.y);
		pPose[ *pPose_count].location[2] = KILL(translate.z);
		pPose[ *pPose_count].rotation[0] = rotate.x;
		pPose[ *pPose_count].rotation[1] = rotate.y;
		pPose[ *pPose_count].rotation[2] = rotate.z;
		pPose[ *pPose_count].rotation[3] = rotate.w;
		pPose[ *pPose_count].scale[0] = KILL(scale.x);
		pPose[ *pPose_count].scale[1] = KILL(scale.y);
		pPose[ *pPose_count].scale[2] = KILL(scale.y);
    (*pPose_count)++;
  }

}

static int saved_parents[1000];

struct hiplist_s {
	char *name; char *parent; int parent_id;
} hiplist[] = {
	{ (char *)"bip01_l_thigh", (char *)"bip01_pelvis", -1 },
	{ (char *)"bip01_r_thigh", (char *)"bip01_pelvis", -1 },
//	{ "bip01_l_foot", "bip01_l_calf", -1 },
//	{ "bip01_r_foot", "bip01_r_calf", -1 },
	{ NULL, NULL, 0 }
};

static int fix_hips(int verbose)
{
	int i, k, p, fixed = 0;

	for (k = 0; hiplist[k].parent; k++)
		hiplist[k].parent_id = -1;

	for (i = 0; i < numbones; i++) {
		saved_parents[i] = bonelist[i].parent;

		for (k = 0; hiplist[k].parent; k++)
			if (!strcmp(bonelist[i].clean_name, hiplist[k].parent))
				hiplist[k].parent_id = i;

		p = bonelist[i].parent;
		for (k = 0; hiplist[k].parent; k++) {
			if (!strcmp(bonelist[i].clean_name, hiplist[k].name)) {
				if (p >= 0 && strcmp(bonelist[p].clean_name, hiplist[k].parent)) {
					if (verbose) {
#ifdef use_again
						fprintf(stderr, "fixing %s -> %s (was connected to %s)\n",
							bonelist[i].clean_name,
							bonelist[hiplist[k].parent_id].clean_name,
							bonelist[p].clean_name);
#endif
					}
					fixed = 1;
					bonelist[i].parent = hiplist[k].parent_id;
				}
			}
		}
	}

	return fixed;
}

static void unfix_hips(void)
{
	int i;
	for (i = 0; i < numbones; i++)
		bonelist[i].parent = saved_parents[i];
}

static void fix_pose(void)
{
	int i;

	calc_abs_pose();

	if (dohips) fix_hips(0);

	for (i = 0; i < numbones; i++) {
		if (bonelist[i].isbone) {
			// remove scaling factor in absolute pose
			if (dounscale < 0) {
        aiVector3D apos, ascale;
        aiQuaternion arot;

				aiDecomposeMatrix(&bonelist[i].abspose, &ascale, &arot, &apos);
				bonelist[i].unscale[0] = ascale.x;
				bonelist[i].unscale[1] = ascale.y;
				bonelist[i].unscale[2] = ascale.z;
				if (KILL(ascale.x) != 1 || KILL(ascale.y) != 1 || KILL(ascale.z) != 1) {
					//x/fprintf(stderr, "unscaling %s: %g %g %g\n", bonelist[i].name, ascale.x, ascale.y, ascale.z);
				}
			}
			if (dounscale) {
				float x = bonelist[i].unscale[0];
				float y = bonelist[i].unscale[1];
				float z = bonelist[i].unscale[2];
				if (KILL(x) != 1 || KILL(y) != 1 || KILL(z) != 1) {
					bonelist[i].abspose.a1 /= x; bonelist[i].abspose.b1 /= x; bonelist[i].abspose.c1 /= x;
					bonelist[i].abspose.a2 /= y; bonelist[i].abspose.b2 /= y; bonelist[i].abspose.c2 /= y;
					bonelist[i].abspose.a3 /= z; bonelist[i].abspose.b3 /= z; bonelist[i].abspose.c3 /= z;
				}
			}

			// flip axis in absolute pose
			if (doaxis)
				aiMultiplyMatrix4(&bonelist[i].abspose, &axis_x_to_y);

			// ...and invert so we can recalculate the local poses
			aiInverseMatrix(&bonelist[i].invpose, &bonelist[i].abspose);

			// ...and recalculate the local pose
			bonelist[i].pose = bonelist[i].abspose;
			if (bonelist[i].parent >= 0) {

        aiMatrix4x4 m = bonelist[bonelist[i].parent].invpose;

				aiMultiplyMatrix4(&m, &bonelist[i].pose);
				bonelist[i].pose = m;
			}

			// ...and make sure we have it in decomposed form
			aiDecomposeMatrix(&bonelist[i].pose, &bonelist[i].scale, &bonelist[i].rotate, &bonelist[i].translate);
		}
	}

	if (dohips) unfix_hips();
}

static void export_pose( IQE_model *pIQEmodel, int *pPose_count, struct pose *pPose, int MaxBones)
{
	int i;

	if (doaxis || dounscale || dohips)
		fix_pose();

	for (i = 0; i < numbones; i++) {

		if (bonelist[i].isbone) {
			export_pq( pIQEmodel, i, pPose_count, pPose, MaxBones);
		}
	}
}

static void export_bone_list( IQE_model *pIQEmodel, int *pPose_count, struct pose *pPose)
{
	int i, n;

	for (n = i = 0; i < numbones; i++) if (bonelist[i].isbone) n++;

#ifdef use_again // 23.12.2014 RR: reworked for IqeBrowers
	if (dounscale) fprintf(stderr, "removing scaling factors from bind pose\n");
	if (doaxis) fprintf(stderr, "flipping bone axis from x to y\n");

	if (dohips) {
		fprintf(stderr, "patching skeleton hierarchy\n");
		dohips = fix_hips(1);
	}

	fprintf(stderr, "exporting skeleton: %d bones\n", n);

	fprintf(out, "\n");
	for (i = 0; i < numbones; i++) {
		if (bonelist[i].isbone) {
			if (bonelist[i].parent >= 0)
				fprintf(out, "joint \"%s\" %d\n",
					bonelist[i].clean_name,
					bonelist[bonelist[i].parent].number);
			else
				fprintf(out, "joint \"%s\" -1\n", bonelist[i].clean_name);
		}
	}

	if (dohips)
		unfix_hips();

	fprintf(out, "\n");

	if (dounscale) dounscale = -1;
	export_pose(out);
	if (dounscale) dounscale = 1;
#else
	//x/if (dounscale) fprintf(stderr, "removing scaling factors from bind pose\n");
	//x/if (doaxis) fprintf(stderr, "flipping bone axis from x to y\n");

	if (dohips) {
		//x/fprintf(stderr, "patching skeleton hierarchy\n");
		dohips = fix_hips(1);
	}

	//x/fprintf(stderr, "exporting skeleton: %d bones\n", n);

	for (i = 0; i < numbones; i++) {
		if (bonelist[i].isbone) {

      int joint_count = pIQEmodel->skel->joint_count;

			if( joint_count < MAXBONE) {
				pIQEmodel->skel->j[ joint_count].name = strdup( bonelist[i].clean_name);
        if (bonelist[i].parent >= 0) {
				  pIQEmodel->skel->j[ joint_count].parent = bonelist[bonelist[i].parent].number;
        } else {
				  pIQEmodel->skel->j[ joint_count].parent = -1;
        }
				pIQEmodel->skel->joint_count++;
			}
		}
	}

	if (dohips)
		unfix_hips();

	if (dounscale) dounscale = -1;
	export_pose( pIQEmodel, pPose_count, pPose, pIQEmodel->skel->joint_count);
	if (dounscale) dounscale = 1;
#endif
}

static void export_static_animation( const struct aiScene *scene, IQE_model *pIQEmodel,
                                     int *pPose_count, struct pose **ppPose)
{
  struct anim *pAnim;

	//fprintf(stderr, "exporting animation: static rest pose\n");

  // one more animation

  pAnim = pushanim( pIQEmodel, basename);

  pAnim->framerate = 30.0;       // fallback, 30 fps

  // create new frame

  *ppPose = pushframe( pAnim, pIQEmodel->skel->joint_count);
  *pPose_count = 0;

	apply_initial_frame();
	export_pose( pIQEmodel, pPose_count, *ppPose, pIQEmodel->skel->joint_count);
}

static int animation_length(const struct aiAnimation *anim)
{
	int i, len = 0;
	for (i = 0; i < (int)anim->mNumChannels; i++) {
		struct aiNodeAnim *chan = anim->mChannels[i];
		if ((int)chan->mNumPositionKeys > len) len = chan->mNumPositionKeys;
		if ((int)chan->mNumRotationKeys > len) len = chan->mNumRotationKeys;
		if ((int)chan->mNumScalingKeys > len) len = chan->mNumScalingKeys;
	}
	return len;
}

static void export_frame( const struct aiAnimation *anim, int frame, IQE_model *pIQEmodel,
                          int *pPose_count, struct pose **ppPose,
                          struct anim *pAnim)
{
	int i;

	// start with fresh matrices
	apply_initial_frame();

	for (i = 0; i < (int)anim->mNumChannels; i++) {
		struct aiNodeAnim *chan = anim->mChannels[i];
		int a = find_bone(chan->mNodeName.data);

    if( a >= 0) {  // found bone

	  	int tframe = MIN(frame, (int)chan->mNumPositionKeys - 1);
		  int rframe = MIN(frame, (int)chan->mNumRotationKeys - 1);
		  int sframe = MIN(frame, (int)chan->mNumScalingKeys - 1);
		  bonelist[a].translate = chan->mPositionKeys[tframe].mValue;
		  bonelist[a].rotate = chan->mRotationKeys[rframe].mValue;
		  bonelist[a].scale = chan->mScalingKeys[sframe].mValue;
#ifdef HACK_MATRIX_KEY
		  bonelist[a].pose = chan->mRotationKeys[rframe].mMatrixValue;
#endif
    }
	}

#ifndef HACK_MATRIX_KEY
	// translate/rotate/scale have changed: recompute pose
	for (i = 0; i < numbones; i++) {
		if (bonelist[i].isbone) {
			// make sure we're not hit by precision issues in decomposematrix
			aiNormalizeQuaternion(&bonelist[i].rotate);
			aiComposeMatrix(&bonelist[i].pose, &bonelist[i].scale, &bonelist[i].rotate, &bonelist[i].translate);
		}
	}
#endif

  // create new frame

  *ppPose = pushframe( pAnim, pIQEmodel->skel->joint_count);
  *pPose_count = 0;

	export_pose( pIQEmodel, pPose_count, *ppPose, pIQEmodel->skel->joint_count);
}

static void export_animations( const struct aiScene *scene, IQE_model *pIQEmodel,
                               int *pPose_count, struct pose **ppPose)
{
	int i, k, len;
	struct anim *pAnim;
	char TempName[ 1024];

	for (i = 0; i < (int)scene->mNumAnimations; i++) {

		const struct aiAnimation *anim = scene->mAnimations[i];

		// construct name for animation

		strcpy( TempName, anim->mName.C_Str());   // get name of animation

		if( strlen( TempName) == 0) {   // name is empty

		  if (scene->mNumAnimations > 1) {
			  sprintf( TempName, "%s,%02d", basename, i);
		  } else {
			  sprintf( TempName, "%s", basename);
		  }
		}

		// one more animation

		pAnim = pushanim( pIQEmodel, TempName);

    // handle framerate

    if( anim->mTicksPerSecond > 0) {   // framerate is defined

      pAnim->framerate = anim->mTicksPerSecond; // copy as fps

    } else {                           // no framerate defined

      pAnim->framerate = 30.0;       // fallback, 30 fps
    }

    //

		len = animation_length(anim);

		//x/ fprintf(stderr, "exporting animation %d: %d frames\n", i+1, len);

		for (k = 0; k < len; k++) {
			export_frame( anim, k, pIQEmodel, pPose_count, ppPose, pAnim);
		}
	}

	if (scene->mNumAnimations == 0) {
		export_static_animation( scene, pIQEmodel, pPose_count, ppPose);
	}
}

/*
 * For multi-mesh models, sometimes each mesh has its own inv_bind_matrix set
 * for each bone. To export to IQE we must have only one inv_bind_matrix per
 * bone. We can bake the mesh by animating it to the initial frame.
 * Once this is done, set the inv_bind_matrix to be the inverse of the forward
 * bind_matrix of this pose.
 */

static void bake_mesh_skin(const struct aiMesh *mesh)
{
	int i, k, b;
  aiMatrix3x3 mat3;
  aiMatrix4x4 bonemat[1000], mat;
  aiVector3D *outpos, *outnorm;

	if (mesh->mNumBones == 0)
		return;

	outpos = (aiVector3D *)malloc(mesh->mNumVertices * sizeof *outpos);
	outnorm = (aiVector3D *)malloc(mesh->mNumVertices * sizeof *outnorm);
	memset(outpos, 0, mesh->mNumVertices * sizeof *outpos);
	memset(outnorm, 0, mesh->mNumVertices * sizeof *outpos);

	calc_abs_pose();

	for (i = 0; i < (int)mesh->mNumBones; i++) {
		b = find_bone(mesh->mBones[i]->mName.data);

		if( b >= 0) {  // found bone

  		bonemat[i] = bonelist[b].abspose;
	  	aiMultiplyMatrix4(&bonemat[i], &mesh->mBones[i]->mOffsetMatrix);
		}
	}

	for (k = 0; k < (int)mesh->mNumBones; k++) {
		struct aiBone *bone = mesh->mBones[k];
		b = find_bone(mesh->mBones[k]->mName.data);
		mat = bonemat[k];
		mat3.a1 = mat.a1; mat3.a2 = mat.a2; mat3.a3 = mat.a3;
		mat3.b1 = mat.b1; mat3.b2 = mat.b2; mat3.b3 = mat.b3;
		mat3.c1 = mat.c1; mat3.c2 = mat.c2; mat3.c3 = mat.c3;
		for (i = 0; i < (int)bone->mNumWeights; i++) {
			struct aiVertexWeight vw = bone->mWeights[i];
			int v = vw.mVertexId;
			float w = vw.mWeight;
      aiVector3D srcpos = mesh->mVertices[v];
      aiVector3D srcnorm = mesh->mNormals[v];
			aiTransformVecByMatrix4(&srcpos, &mat);
			aiTransformVecByMatrix3(&srcnorm, &mat3);
			outpos[v].x += srcpos.x * w;
			outpos[v].y += srcpos.y * w;
			outpos[v].z += srcpos.z * w;
			outnorm[v].x += srcnorm.x * w;
			outnorm[v].y += srcnorm.y * w;
			outnorm[v].z += srcnorm.z * w;
		}
	}

	memcpy(mesh->mVertices, outpos, mesh->mNumVertices * sizeof *outpos);
	memcpy(mesh->mNormals, outnorm, mesh->mNumVertices * sizeof *outnorm);

	free(outpos);
	free(outnorm);
}

static void bake_scene_skin( const struct aiScene *scene)
{
	int i;

	//x/fprintf(stderr, "baking skin to recreate base pose in multi-mesh model\n");
	for (i = 0; i < (int)scene->mNumMeshes; i++)
		bake_mesh_skin(scene->mMeshes[i]);
}

static void export_custom_vertexarrays( IQE_model *pIQEmodel, const struct aiScene *scene)
{
#ifdef use_again  // 23.12.2014 RR: don't support custom vertexarray
	int i, t, first = 1;
	int seen[10] = {0};

	for (i = 0; i < (int)scene->mNumMeshes; i++) {
		struct aiMesh *mesh = scene->mMeshes[i];
		for (t = 1; t < MAX_UVMAP; t++) {
			int custom = FIRST_UVMAP + t - 1;
			if (mesh->mTextureCoords[t]) {
				if (!seen[custom]) {
					if (first) {
            //x/fprintf(out, "\n");
            first = 0;
          }
					fprintf(out, "vertexarray custom%d float 2 \"uvmap.%d\"\n", custom, t);
					seen[custom] = 1;
				}
			}
		}
		for (t = 1; t < MAX_COL; t++) {
			int custom = FIRST_COL + t - 1;
			if (mesh->mColors[t]) {
				if (!seen[custom]) {
					if (first) { fprintf(out, "\n"); first = 0; }
					fprintf(out, "vertexarray custom%d ubyte 4 \"color.%d\"\n", custom, t);
					seen[custom] = 1;
				}
			}
		}
	}
#endif
}

/*
 * Export meshes. Group them by materials. Also apply the node transform
 * to the vertices. IQE does not have a concept of per-group transforms.
 *
 * If we are exporting a rigged model, we have to skip any meshes which
 * are not deformed by the armature. If we are exporting a non-rigged model,
 * we have to pre-transform all meshes.
 *
 * TODO: turn non-rigged meshes into rigged meshes by hooking them up to
 * a synthesized bone for its node.
 */

static void export_node( IQE_model *pIQEmodel, const struct aiScene *scene, const struct aiNode *node,
	                       aiMatrix4x4 mat, char *clean_name, char *basedir)
{
  aiMatrix3x3 mat3;
	int i, a, k, t;

	aiMultiplyMatrix4(&mat, (aiMatrix4x4 *)&node->mTransformation);
	mat3.a1 = mat.a1; mat3.a2 = mat.a2; mat3.a3 = mat.a3;
	mat3.b1 = mat.b1; mat3.b2 = mat.b2; mat3.b3 = mat.b3;
	mat3.c1 = mat.c1; mat3.c2 = mat.c2; mat3.c3 = mat.c3;

	if (!strstr(node->mName.data, "$ColladaAutoName$")) {
		clean_name = clean_node_name((char*)node->mName.data);
	}

#ifdef use_again
	if (only_one_node && strcmp(clean_name, only_one_node))
		goto skip_mesh;
#endif

	for (i = 0; i < (int)node->mNumMeshes; i++) {
		struct aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
		struct aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
    aiString str;

		if (mesh->mNumBones == 0 && dobone && !dorigid) {
			if (verbose) {
				//x/fprintf(stderr, "skipping rigid mesh %d in node %s (no bones)\n", i, clean_name);
			}
			continue;
		}

		// Skip skeleton meshes. Inserted by assimlib for a 'animation only' file.

#ifndef use_again
    if( aiGetMaterialString(material, AI_MATKEY_NAME, &str) == AI_SUCCESS &&
        strcmp( str.data, "SkeletonMaterial") == 0) {     // skeleton build from assimlib.

			continue;
    }
#endif

		//x/fprintf(stderr, "exporting mesh %s[%d]: %d vertices, %d faces\n",
		//x/		clean_name, i, mesh->mNumVertices, mesh->mNumFaces);


		//x/fprintf(out, "mesh \"%s\"\n", clean_name);
		//x/fprintf(out, "material \"%s\"\n", find_material(material));

		struct vb *vb = (struct vb*) malloc(mesh->mNumVertices * sizeof(*vb));
		memset(vb, 0, mesh->mNumVertices * sizeof(*vb));

		// A rigidly animated node -- insert fake blend index/weights
		if (mesh->mNumBones == 0 && dobone) {
			a = find_bone((char*)node->mName.data);

  		if( a >= 0) {  // found bone

			  if (verbose) {
				  //x/fprintf(stderr, "\trigid bone %d for mesh in node %s (no bones)\n", bonelist[a].number, node->mName.data);
			  }
			  for (k = 0; k < (int)mesh->mNumVertices; k++) {
				  vb[k].b[0] = bonelist[a].number;
				  vb[k].w[0] = 1;
				  vb[k].n = 1;
			  }
			}
		}

		// Assemble blend index/weight array
		for (k = 0; k < (int)mesh->mNumBones; k++) {
			struct aiBone *bone = mesh->mBones[k];
			a = find_bone(bone->mName.data);

  		if( a >= 0) {  // found bone

			  for (t = 0; t < (int)bone->mNumWeights; t++) {
				  struct aiVertexWeight *w = mesh->mBones[k]->mWeights + t;
				  int idx = w->mVertexId;
				  if (vb[idx].n < MAXBLEND) {
					  vb[idx].b[vb[idx].n] = bonelist[a].number;
					  vb[idx].w[vb[idx].n] = w->mWeight;
					  vb[idx].n++;
				  }
				}
			}
		}

		// add vertices

    int firstVertex = IqeB_ImportArray_position.len / 3;

		for (k = 0; k < (int)mesh->mNumVertices; k++) {

      aiVector3D vp = mesh->mVertices[k];
			if (!dobone) {
				aiTransformVecByMatrix4(&vp, &mat);
			}

			addposition( vp.x, vp.y, vp.z);

			if (mesh->mNormals) {
        aiVector3D vn = mesh->mNormals[k];
				if (!dobone) {
					aiTransformVecByMatrix3(&vn, &mat3);
				}

				addnormal( vn.x, vn.y, vn.z);
			}

			if (mesh->mTextureCoords[0]) {
				float u = mesh->mTextureCoords[0][k].x;
				float v = 1 - mesh->mTextureCoords[0][k].y;
				addtexcoord( u, v);
			}

#ifdef use_again // 23.12.2014 RR: not supported by IeqBrowser
			for (t = 1; t <= MAX_UVMAP; t++) {
				if (mesh->mTextureCoords[t]) {
					float u = mesh->mTextureCoords[t][k].x;
					float v = 1 - mesh->mTextureCoords[t][k].y;
					fprintf(out, "v%d %.9g %.9g\n", FIRST_UVMAP+t-1, u, v);
				}
			}
#endif

			if (mesh->mColors[0]) {
				float r = mesh->mColors[0][k].r; r = floorf(r * 255) / 255;
				float g = mesh->mColors[0][k].g; g = floorf(g * 255) / 255;
				float b = mesh->mColors[0][k].b; b = floorf(b * 255) / 255;
				float a = mesh->mColors[0][k].a; a = floorf(a * 255) / 255;

				addcolor( r, g, b, a);
			}

#ifdef use_again // 23.12.2014 RR: not supported by IeqBrowser
			for (t = 1; t <= MAX_COL; t++) {
				if (mesh->mColors[t]) {
					float r = mesh->mColors[t][k].r; r = floorf(r * 255) / 255;
					float g = mesh->mColors[t][k].g; g = floorf(g * 255) / 255;
					float b = mesh->mColors[t][k].b; b = floorf(b * 255) / 255;
					float a = mesh->mColors[t][k].a; a = floorf(a * 255) / 255;
					fprintf(out, "v%d %.9g %.9g %.9g %.9g\n", FIRST_COL+t-1, r, g, b, a);
				}
			}
#endif

			if (dobone) {
				int bi[ 4];
				float bw[ 4];

				// preset default
				for( t = 0; t < 4; t++) {
          bi[ t] = 0;
          bw[ t] = 0.0;
				}
        bw[ 0] = 1.0;

        // sort weights (bubblesort)
        for( ; ; ) {

          int exchange, tempi;
          float tempw;

          exchange = false;

				  for( t = 0; t < vb[k].n - 1; t++) {

            if( vb[k].w[t] < vb[k].w[t + 1]) {  // have to exchange

              exchange = true;

              tempi = vb[k].b[t];
              tempw = vb[k].w[t];
              vb[k].b[t] = vb[k].b[t + 1];
              vb[k].w[t] = vb[k].w[t + 1];
              vb[k].b[t + 1] = tempi;
              vb[k].w[t + 1] = tempw;
            }
				  }

				  if( ! exchange) {
            break;
				  }
        }

        // get the 4 biggest weights
				for( t = 0; t < vb[k].n && t < 4; t++) {
          bi[ t] = vb[k].b[t];
          bw[ t] = vb[k].w[t];
				}

				addblend( bi[ 0], bi[ 1], bi[ 2], bi[ 3], bw[ 0], bw[ 1], bw[ 2], bw[ 3]);
			}
		}

		// add faces

		int firstTriangle = IqeB_ImportArray_element.len;

		for (k = 0; k < (int)mesh->mNumFaces; k++) {
			struct aiFace *face = mesh->mFaces + k;
			if (face->mNumIndices == 3) {
				if (doflip)
					addtriangle( firstVertex + face->mIndices[2], firstVertex + face->mIndices[1], firstVertex + face->mIndices[0]);
				else
					addtriangle( firstVertex + face->mIndices[0], firstVertex + face->mIndices[1], firstVertex + face->mIndices[2]);

#ifdef use_again // 23.12.2014 RR: have aiProcess_Triangulate set, so expect only triangles
			} else if (face->mNumIndices == 4) {
				if (doflip)
					fprintf(out, "fm %d %d %d %d\n", face->mIndices[3], face->mIndices[2], face->mIndices[1], face->mIndices[0]);
				else
					fprintf(out, "fm %d %d %d %d\n", face->mIndices[0], face->mIndices[1], face->mIndices[2], face->mIndices[3]);
			} else if (face->mNumIndices > 4) {
				fprintf(stderr, "n-gon (%d) in mesh!\n", face->mNumIndices);
				int i1 = face->mIndices[0];
				int i2 = face->mIndices[1];
				for (a = 2; a < face->mNumIndices; a++) {
					int i3 = face->mIndices[a];
					if (doflip)
						fprintf(out, "fm %d %d %d\n", i3, i2, i1);
					else
						fprintf(out, "fm %d %d %d\n", i1, i2, i3);
					i2 = i3;
				}
#endif
			} else {
				//x/fprintf(stderr, "skipping point/line primitive\n");
			}
		}

    pushpart(&IqeB_ImportArray_partbuf, firstTriangle, IqeB_ImportArray_element.len,
              clean_name, find_material(material), basedir);

		free(vb);
	}

#ifdef use_again
skip_mesh:
#endif

	for (i = 0; i < (int)node->mNumChildren; i++) {
		export_node( pIQEmodel, scene, node->mChildren[i], mat, clean_name, basedir);
	}
}

#ifdef use_again // 23.12.2014 RR: not used for IqeBrowser
static void export_mesh_list(const struct aiScene *scene)
{
	int i, k;

	for (i = 0; i < numbones; i++) {
		struct aiNode *node = bonelist[i].node;
		for (k = 0; k < node->mNumMeshes; k++) {
			struct aiMesh *mesh = scene->mMeshes[node->mMeshes[k]];
			if (mesh->mNumBones > 0) {
				printf("%s\n", bonelist[i].clean_name);
				break;
			}
		}
	}
}

static void export_position_list(const struct aiScene *scene)
{
	int i;

	calc_abs_pose();

	for (i = 0; i < numbones; i++) {
		printf("%s %g %g %g\n", bonelist[i].clean_name,
			bonelist[i].abspose.a4,
			bonelist[i].abspose.b4,
			bonelist[i].abspose.c4);
	}
}
#endif

#ifdef use_again
void usage()
{
	fprintf(stderr, "usage: assiqe [options] [-o out.iqe] input.dae [tags ...]\n");
	fprintf(stderr, "\t-AA -- export all bones (including unused ones)\n");
	fprintf(stderr, "\t-A -- export all child bones\n");
	fprintf(stderr, "\t-H -- fix hierarchy (thighs <- pelvis)\n");
	fprintf(stderr, "\t-M -- print a list of meshes in scene then quit\n");
	fprintf(stderr, "\t-P -- print the positions of all bose in scene then quit\n");
	fprintf(stderr, "\t-S -- static mesh only (no skeleton)\n");
	fprintf(stderr, "\t-n mesh -- export only the named mesh\n");
	fprintf(stderr, "\t-a -- only export animations\n");
	fprintf(stderr, "\t-m -- only export mesh\n");
	fprintf(stderr, "\t-b -- bake mesh to bind pose / initial frame\n");
	fprintf(stderr, "\t-f -- export counter-clockwise winding triangles\n");
	fprintf(stderr, "\t-r -- export rigid nodes too (experimental)\n");
	fprintf(stderr, "\t-l -- low precision mode (for smaller animation files)\n");
	fprintf(stderr, "\t-x -- flip bone orientation from x to y\n");
	fprintf(stderr, "\t-s -- remove scaling from bind pose\n");
	fprintf(stderr, "\t-u -- unmark bone (force it to be excluded)\n");
	fprintf(stderr, "\t-o filename -- save output to file\n");
	exit(1);
}
#endif

/************************************************************************************
 * IqeB_ImportAssimbLib()
 *
 * return:   NULL  error on loading
 *           else  pointer to loaded model data
 */

IQE_model *IqeB_ImportAssimbLib( char *filename)
{
  char basedir[ 1024];
	char *p;

  // Create an instance of the Importer class
  Assimp::Importer importer;
  const struct aiScene* scene = NULL;
  struct IQE_model *pIQEmodel = NULL;
	struct skel *pIQEskel = NULL;
	struct mesh *pIQEmesh = NULL;
	struct pose *pIQEpose = NULL;
	int pose_count = 0;

	//x/fprintf( stderr, "loading obj model '%s' ...\n", filename);

  IqeB_ImportArraysReset();  // reset temporary arrays used on loading files

  // get base directory (path to filename)

	strcpy( basedir, filename);
	p = strrchr(basedir, '/');
	if (!p) p = strrchr(basedir, '\\');
	if (!p) {
    strcpy(basedir, "");    // empty directory
    p = filename;           // point to begin of file name
  } else {
    *p = 0;          // overwrite slash
    p += 1;          // point to begin of file name
  }

  //
  // Prepate the 'asstools' things
  //

  // ...

	int onlyanim = 0;
	int onlymesh = 0;

  // settings to default

  verbose = 0;           // print
  need_to_bake_skin = 0; // bake mesh to bind pose / initial frame
  save_all_bones = 1;    // 1: export all child bones, 2: export all bones (including unused ones)

  dostatic = 0;          // static mesh only (no skeleton)
  dorigid = 0;           // export rigid (non-deformed) nodes as bones too (experimental)
  domesh = 1;            // export mesh
  doanim = 0;            // export animations
  dobone = 0;            // export skeleton
  doflip = 0;            // export flipped (quake-style clockwise winding) triangles

  doaxis = 0;            // flip bone axis from X to Y to match blender
  dounscale = 0;         // remove scaling from bind pose
  dohips = 0;            // reparent thighs to pelvis (see zo_hom_marche), fix hierarchy (thighs <- pelvis)

	// reset some other variables on each call

	numtags = 0;
	taglist = NULL;
	numuntags = 0;
	nummats = 0;
	numbones = 0;

	// get basename from filename

	p = strrchr( filename, '/');
	if (!p) p = strrchr( filename, '\\');
	if (!p) p = filename; else p++;
	strcpy( basename, p);
	p = strrchr( basename, '.');   // remove file extension
	if (p) *p = 0;

  //
  // Read the file with the assimb library.
	// we are taking one of the postprocessing presets to avoid
	// spelling out 20+ single postprocessing flags here.
	// 20.12.2014 RR: NOTE: aiProcess_Triangulate converts polygons to triangels
  //

	int ProcessingFlags;

#ifdef use_again
	ProcessingFlags = aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_Triangulate;

	if( (ProcessingFlags & ( aiProcess_GenNormals | aiProcess_GenSmoothNormals)) == 0) { // no normal processing

    ProcessingFlags |= aiProcess_GenNormals;
	}

  ProcessingFlags |= aiProcess_GenUVCoords;       // convert spherical, cylindrical, box and planar mapping to proper UVs
	ProcessingFlags |= aiProcess_TransformUVCoords; // preprocess UV transformations (scaling, translation ...)
#else

  // 23.12.2014 RR: processingflags as used by assiqe.c

	ProcessingFlags = 0;
	ProcessingFlags |= aiProcess_Triangulate;        // Always like triangles
	ProcessingFlags |= aiProcess_JoinIdenticalVertices;
	ProcessingFlags |= aiProcess_GenSmoothNormals;
	ProcessingFlags |= aiProcess_GenUVCoords;
	ProcessingFlags |= aiProcess_TransformUVCoords;
	ProcessingFlags |= aiProcess_LimitBoneWeights;
	//ProcessingFlags |= aiProcess_FindInvalidData;
	ProcessingFlags |= aiProcess_ImproveCacheLocality;
	//ProcessingFlags |= aiProcess_RemoveRedundantMaterials;
	//ProcessingFlags |= aiProcess_OptimizeMeshes;
#endif

	scene = importer.ReadFile( filename, ProcessingFlags);
	if( scene == NULL) {                                  // error on loading file
    goto ExitPoint;
	}

  // 23.12.2014 RR: more assiqe.c settings

	if (scene->mNumAnimations > 0) doanim = 1;
	if (onlymesh) { domesh = 1; doanim = 0; }
	if (onlyanim) { domesh = 0; doanim = 1; }

	// Convert to Z-UP coordinate system
	aiMultiplyMatrix4(&scene->mRootNode->mTransformation, &yup_to_zup);

	// Build a list of bones and compute the bind pose matrices.
	if (build_bone_list(scene) > 0) {
		dobone = 1;
	}

	if (dostatic) {
		dobone = 0;
		need_to_bake_skin = 0;
	}

#ifdef use_again // 23.12.2014 RR: not used for IqeBrowser
	if (list_all_meshes) {
		export_mesh_list(scene);
		return 0;
	}

	if (list_all_positions) {
		export_position_list(scene);
		return 0;
	}
#endif

	// Mesh is split with incompatible bind matrices, so pick a new
	// bind pose and deform the mesh to fit.
	if (need_to_bake_skin && !onlyanim) {
		apply_initial_frame(); // ditch original bind pose
		bake_scene_skin(scene);
	}

  //
	// allocated an emptyIqeB model
  //

	pIQEmodel = IqeB_ModelCreateEmpty();    // get empty model
	if( pIQEmodel == NULL) {                                  // memory allocation failed

    goto ExitPoint;
	}

  // for less writing, some short cuts

	pIQEskel = pIQEmodel->skel;
	pIQEmesh = pIQEmodel->mesh;

  // ...

	pIQEskel->joint_count = 0;
	pose_count = 0;
	pIQEpose = pIQEskel->pose;

  //
	// convert the data
  //

  // Sekeleton

	if (dobone) {
		export_bone_list( pIQEmodel, &pose_count, pIQEpose);
	}

	// Mesh

	if (domesh) {
    aiMatrix4x4 identity;
		aiIdentityMatrix4(&identity);
		export_custom_vertexarrays( pIQEmodel, scene);
		export_node( pIQEmodel, scene, scene->mRootNode, identity, (char *)"SCENE", basedir);
	}

	// animations

	if (dobone) {
		if (doanim) {
			export_animations( scene, pIQEmodel, &pose_count, &pIQEpose);
		}
	}

  // finish

  IqeB_ModelSkeletonMatrixCalc( pIQEmodel);      // update skeleton matrix
  IqeB_AnimPrepareAfterLoad( pIQEmodel);         // Prepare animations

	pIQEmesh->vertex_count = IqeB_ImportArray_position.len / 3;
	pIQEmesh->position = (float *)dupArray(IqeB_ImportArray_position.data, IqeB_ImportArray_position.len, sizeof(float));
	pIQEmesh->normal = (float *)dupArray(IqeB_ImportArray_normal.data, IqeB_ImportArray_normal.len, sizeof(float));
	pIQEmesh->texcoord = (float *)dupArray(IqeB_ImportArray_texcoord.data, IqeB_ImportArray_texcoord.len, sizeof(float));
	pIQEmesh->color = (float *)dupArray(IqeB_ImportArray_color.data, IqeB_ImportArray_color.len, sizeof(float));
	pIQEmesh->blendindex = (int *)dupArray(IqeB_ImportArray_blendindex.data, IqeB_ImportArray_blendindex.len, sizeof(int));
	pIQEmesh->blendweight = (float *)dupArray(IqeB_ImportArray_blendweight.data, IqeB_ImportArray_blendweight.len, sizeof(float));
	pIQEmesh->aposition = NULL;
	pIQEmesh->anormal = NULL;

	pIQEmesh->element_count = IqeB_ImportArray_element.len;
	pIQEmesh->element = (int *)dupArray(IqeB_ImportArray_element.data, IqeB_ImportArray_element.len, sizeof(int));

	pIQEmesh->part_count = IqeB_ImportArray_partbuf.len;
	pIQEmesh->part = (part *)dupArray(IqeB_ImportArray_partbuf.data, IqeB_ImportArray_partbuf.len, sizeof(struct part));

	//x/fprintf(stderr, "\t%d batches; %d vertices; %d triangles; %d joints\n",
	//x/		mesh->part_count, mesh->vertex_count, mesh->element_count / 3, skel->joint_count);

	// IN every case, reconstruct normals.
	// The assimb laoder returns unusable normals!

  IqeB_MeshComputeVertexNormals( pIQEmesh);

ExitPoint:

	return pIQEmodel;
}

/************************************************************************************
 * IqeB_ImportAssimbLibExtensions()
 *
 * Fills a string with the file extensions of any known image format.
 *
 */

void IqeB_ImportAssimbLibExtensions( char *pExtensionList, int SizeString)
{
  Assimp::Importer importer;  // Create an instance of the Importer class
  aiString TmpExtensions;
  char *pSrc, *pDst;
  int Filled;

  pExtensionList[ 0] = '\0'; // set string to empty

  // let assimb fill the list

  importer.GetExtensionList( TmpExtensions);

  // convert to what we want

  pSrc = (char *)TmpExtensions.C_Str();
  pDst = pExtensionList;
  Filled = 0;

  for ( ; ; ) {

    if( *pSrc == '*') {  // skip '*'

      pSrc++;
    }

    if( *pSrc == '.') {  // skip '.'

      pSrc++;
    }

    if( *pSrc == '\0') {  // end of string

      break;
    }

    if( Filled + 1 >= SizeString) {  // overflow

      // back to last comma

      while( Filled > 0 && pDst[ -1] != ',') {

        pDst--;
        Filled += 1;
      }

      break;
    }

    if( *pSrc == ';') {  // is semicolon
      *pDst++ = ',';     // convert to comma
    } else {
      *pDst++ = *pSrc;
    }
    pSrc++;

    Filled += 1;
  }

  *pDst++ = '\0';  // set end of string

}

/****************************** End Of File ******************************/
