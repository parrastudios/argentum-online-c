/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GUI_CONSOLE_H
#define GUI_CONSOLE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/QueueFixed.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define GUI_CONSOLE_BACKGROUND			0x5C83
#define GUI_CONSOLE_LINE_SIZE			0x20
#define GUI_CONSOLE_LINE_LENGTH			0xFF

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef char ConsoleLineType[GUI_CONSOLE_LINE_LENGTH];

struct GuiConsole
{
	QueueFixedDefine(ConsoleLineType, GUI_CONSOLE_LINE_SIZE)	Queue;		//< Array of lines (strings)
	uint32														LineSize;	//< Max size of visible lines
	uint32														LineCount;	//< Number of visible lines
	uint32														LineStart;	//< Initial visible line
};

////////////////////////////////////////////////////////////
// Widget forward declaration
////////////////////////////////////////////////////////////
struct GuiWidget;

////////////////////////////////////////////////////////////
/// Create a console
////////////////////////////////////////////////////////////
struct GuiWidget * GuiConsoleCreate(int32 X, int32 Y, uint32 Width, uint32 Height, uint32 FontSize);

////////////////////////////////////////////////////////////
// Create a console by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiConsoleCreateDefault();

////////////////////////////////////////////////////////////
/// Add a line of text
////////////////////////////////////////////////////////////
void GuiConsoleAddLine(struct GuiWidget * Widget, char * Line);

////////////////////////////////////////////////////////////
/// Scroll text of the console
////////////////////////////////////////////////////////////
void GuiConsoleScroll(struct GuiWidget * Widget, int32 Position);

////////////////////////////////////////////////////////////
/// Render a console widget
////////////////////////////////////////////////////////////
void GuiConsoleRender(struct GuiWidget * Form, struct GuiWidget * Widget, struct Color4ub * BackgroundColor, struct Color4ub * FontColor);

#endif // GUI_CONSOLE_H
