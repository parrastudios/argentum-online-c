#!/usr/bin/env bash

#############################################################################
## AOC Server development environment installation by Parra Studios
##
## Copyright (C) 2009-2016 Vicente Eduardo Ferrer García <vic798@gmail.com>
##
##      A cross-platform mmorpg which keeps the original essence of
##      Argentum Online as combats, magics, guilds, although it has new
##      implementations as 3D graphics engine, new gameplay,
##      and a better performance, among others.
##
#############################################################################

#Default variables
choice=1


function askforoption {

tput clear
tput cup 3 10

tput setaf 3
echo "AOC Server development environment Parra Studios"
tput sgr0
 
tput cup 5 12

tput rev
echo "Copyright (C) 2009-2016 Vicente Eduardo Ferrer García"
tput sgr0
 
tput cup 7 10
echo "1. Run Vagrant"

tput cup 8 10
echo "2. Install development environment on Debian based OS"
 
tput cup 9 10
echo "3. Install development environment on Arch linux"

tput cup 10 10
echo "4. Destroy Vagrant VM (removes all its traces)"

tput cup 11 10
echo "5. Exit"

tput bold
tput cup 13 10
read -p "Enter your choice [1-5] " choice
 
tput clear
tput sgr0
tput rc

case $choice in
[1]*)
  runvagrant
  askforoption
  ;;
[2]*)
  echo "Installing development environment on Debian based OS"
  installdebian
  askforoption
  ;;
[3]*)
  echo "Installing development environment on Arch Linux"
  installarch
  askforoption
  ;;
[4]*)
  echo "Cleaning up and destroying Vagrant VM"
  vagrant destroy
  ;;
[5]*)
  echo "See you soon!"
  exit
  ;;
*)
  echo "Please, select an avaible option!"
  askforoption
  ;;
esac

}

function installarch {
  yaourt -Syu
  yaourt -S --noconfirm --needed virtualbox
  yaourt -S --noconfirm --needed vagrant
  
  # Fix vb kernel
  sudo /sbin/rcvboxdrv setup

  runvagrant
}

function installdebian {
  sudo apt-get install --yes update
  sudo apt-get install --yes upgrade
  sudo apt-get install --yes virtualbox
  sudo apt-get install --yes vagrant
}

function runvagrant {
  echo "Running Vagrant... please wait."
  vagrant up
  vagrant ssh
}

askforoption




