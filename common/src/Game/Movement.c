/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Movement.h>
#include <Game/Character.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define MOVEMENT_FORWARD_SPEED	120.0f
#define MOVEMENT_HEADING_SPEED	120.0f
#define MOVEMENT_HEIGHT_SPEED	12.0f
#define MOVEMENT_ROLLING_SPEED	45.0f

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get next movement from input and previous movement
////////////////////////////////////////////////////////////
uint32 CharacterMove(uint32 Previous, uint8 Key, bool Pressed)
{
	// Status Not
	if (CharacterMovementIs(Previous, MoveNot))
	{
		if (Pressed)
		{
			if (Key == MOVEMENT_KEY_LEFT)
			{
				return MoveLeft;
			}
			else if (Key == MOVEMENT_KEY_DOWN)
			{
				return MoveDown;
			}
			else if (Key == MOVEMENT_KEY_RIGHT)
			{
				return MoveRight;
			}
			else if (Key == MOVEMENT_KEY_UP)
			{
				return MoveUp;
			}
			else if (Key == MOVEMENT_KEY_NEAR)
			{
				return MoveNear;
			}
			else if (Key == MOVEMENT_KEY_FAR)
			{
				return MoveFar;
			}
		}

		return MoveInvalid;
	}

	// Status Up
	if (CharacterMovementIs(Previous, MoveUp))
	{
		if (Pressed)
		{
			if (Key == MOVEMENT_KEY_LEFT)
			{
				return MoveUpLeft;
			}
			else if (Key == MOVEMENT_KEY_RIGHT)
			{
				return MoveUpRight;
			}
		}
		else
		{
			if (Key == MOVEMENT_KEY_UP)
			{
				return MoveNot;
			}
		}

		return MoveInvalid;
	}

	// Status Right
	if (CharacterMovementIs(Previous, MoveRight))
	{
		if (Pressed)
		{
			if (Key == MOVEMENT_KEY_UP)
			{
				return MoveUpRight;
			}
			else if (Key == MOVEMENT_KEY_DOWN)
			{
				return MoveDownRight;
			}
		}
		else
		{
			if (Key == MOVEMENT_KEY_RIGHT)
			{
				return MoveNot;
			}
		}

		return MoveInvalid;
	}

	// Status Down
	if (CharacterMovementIs(Previous, MoveDown))
	{
		if (Pressed)
		{
			if (Key == MOVEMENT_KEY_RIGHT)
			{
				return MoveDownRight;
			}
			else if (Key == MOVEMENT_KEY_LEFT)
			{
				return MoveDownLeft;
			}
		}
		else
		{
			if (Key == MOVEMENT_KEY_DOWN)
			{
				return MoveNot;
			}
		}

		return MoveInvalid;
	}

	// Status Left
	if (CharacterMovementIs(Previous, MoveLeft))
	{
		if (Pressed)
		{
			if (Key == MOVEMENT_KEY_UP)
			{
				return MoveUpLeft;
			}
			else if (Key == MOVEMENT_KEY_DOWN)
			{
				return MoveDownLeft;
			}
		}
		else
		{
			if (Key == MOVEMENT_KEY_LEFT)
			{
				return MoveNot;
			}
		}

		return MoveInvalid;
	}

	// Status Up Left
	if (CharacterMovementIs(Previous, MoveUpLeft))
	{
		if (!Pressed)
		{
			if (Key == MOVEMENT_KEY_LEFT)
			{
				return MoveUp;
			}
			else if (Key == MOVEMENT_KEY_UP)
			{
				return MoveLeft;
			}
		}

		return MoveInvalid;
	}

	// Status Up Right
	if (CharacterMovementIs(Previous, MoveUpRight))
	{
		if (!Pressed)
		{
			if (Key == MOVEMENT_KEY_RIGHT)
			{
				return MoveUp;
			}
			else if (Key == MOVEMENT_KEY_UP)
			{
				return MoveRight;
			}
		}

		return MoveInvalid;
	}

	// Status Down Right
	if (CharacterMovementIs(Previous, MoveDownRight))
	{
		if (!Pressed)
		{
			if (Key == MOVEMENT_KEY_RIGHT)
			{
				return MoveDown;
			}
			else if (Key == MOVEMENT_KEY_DOWN)
			{
				return MoveRight;
			}
		}

		return MoveInvalid;
	}

	// Status Down Left
	if (CharacterMovementIs(Previous, MoveDownLeft))
	{
		if (!Pressed)
		{
			if (Key == MOVEMENT_KEY_LEFT)
			{
				return MoveDown;
			}
			else if (Key == MOVEMENT_KEY_DOWN)
			{
				return MoveLeft;
			}
		}

		return MoveInvalid;
	}

    // Status Near
	if (CharacterMovementIs(Previous, MoveNear))
	{
	    if (Key == MOVEMENT_KEY_NEAR)
        {
            if (Pressed)
            {
                return MoveNear;
            }
            else
            {
                return MoveNot;
            }
        }

		return MoveInvalid;
	}

    // Status Far
	if (CharacterMovementIs(Previous, MoveFar))
	{
	    if (Key == MOVEMENT_KEY_FAR)
        {
            if (Pressed)
            {
                return MoveFar;
            }
            else
            {
                return MoveNot;
            }
        }

		return MoveInvalid;
	}

	// Reset automata
	if (CharacterMovementIs(Previous, MoveInvalid))
	{
		if (Key == MOVEMENT_KEY_NONE)
		{
			return MoveNot;
		}
	}

	return MoveInvalid;
}

////////////////////////////////////////////////////////////
/// Calculate character speed from movement type
////////////////////////////////////////////////////////////
void CharacterCalculateSpeed(uint32 Status, struct Vector3f * Velocity)
{
	float ForwardSpeed, HeightSpeed;
	
	ForwardSpeed = HeightSpeed = 0.0f;

	if (!CharacterMovementIs(Status, MoveNot))
	{
		if (CharacterMovementCheck(Status, MoveUp))
		{
			ForwardSpeed = -MOVEMENT_FORWARD_SPEED;
		}

		if (CharacterMovementCheck(Status, MoveRight))
		{
			ForwardSpeed = -MOVEMENT_FORWARD_SPEED;
		}

		if (CharacterMovementCheck(Status, MoveDown))
		{
			ForwardSpeed = -MOVEMENT_FORWARD_SPEED;
		}

		if (CharacterMovementCheck(Status, MoveLeft))
		{
			ForwardSpeed = -MOVEMENT_FORWARD_SPEED;
		}

		if (CharacterMovementCheck(Status, MoveNear))
		{
			HeightSpeed = MOVEMENT_HEIGHT_SPEED;
		}

		if (CharacterMovementCheck(Status, MoveFar))
		{
			HeightSpeed = -MOVEMENT_HEIGHT_SPEED;
		}
	}

	Vector3fSet(Velocity, 0.0f, ForwardSpeed, HeightSpeed);
}

////////////////////////////////////////////////////////////
/// Calculate character orientation from movement type
////////////////////////////////////////////////////////////
void CharacterCalculateOrientation(uint32 Status, struct Vector3f * Orientation)
{
	if (!CharacterMovementIs(Status, MoveNot))
	{
		// Check against diagonals
		if (CharacterMovementCheck(Status, MoveUpLeft))
		{
			Vector3fSet(Orientation, 0.0f, 0.0f, 45.0f);

			return;
		}

		if (CharacterMovementCheck(Status, MoveDownLeft))
		{
			Vector3fSet(Orientation, 0.0f, 0.0f, 135.0f);

			return;
		}

		if (CharacterMovementCheck(Status, MoveDownRight))
		{
			Vector3fSet(Orientation, 0.0f, 0.0f, 225.0f);

			return;
		}

		if (CharacterMovementCheck(Status, MoveUpRight))
		{
			Vector3fSet(Orientation, 0.0f, 0.0f, 315.0f);

			return;
		}

		// Check simple directions
		if (CharacterMovementCheck(Status, MoveLeft))
		{
			Vector3fSet(Orientation, 0.0f, 0.0f, 90.0f);

			return;
		}

		if (CharacterMovementCheck(Status, MoveDown))
		{
			Vector3fSet(Orientation, 0.0f, 0.0f, 180.0f);

			return;
		}

		if (CharacterMovementCheck(Status, MoveRight))
		{
			Vector3fSet(Orientation, 0.0f, 0.0f, 270.0f);

			return;
		}

		if (CharacterMovementCheck(Status, MoveUp))
		{
			Vector3fSet(Orientation, 0.0f, 0.0f, 360.0f);

			return;
		}
	}

	Vector3fSet(Orientation, 0.0f, 0.0f, 0.0f);
}

////////////////////////////////////////////////////////////
/// Calculate character rotation from movement type
////////////////////////////////////////////////////////////
void CharacterCalculateRotation(uint32 Status, struct Vector3f * Rotation)
{
	Vector3fSet(Rotation, 0.0f, 0.0f, 0.0f);
}

////////////////////////////////////////////////////////////
/// Obtain entity spatial properties from movement type
////////////////////////////////////////////////////////////
void CharacterMoveApply(uint32 Status, struct Vector3f * Velocity,
						struct Vector3f * Orientation, struct Vector3f * Rotation)
{
	// Calculate speed
	CharacterCalculateSpeed(Status, Velocity);
	
	// Calculate orientation
	CharacterCalculateOrientation(Status, Orientation);

	// Calculate rotation
	CharacterCalculateRotation(Status, Rotation);
}
