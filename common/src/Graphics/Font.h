/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_FONT_H
#define GRAPHICS_FONT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

#include <ft2build.h>			///< FreeType library includes
#include <freetype/freetype.h>
#include <freetype/ftglyph.h>

#include <Graphics/Device.h>
#include <Graphics/Color.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define FONT_LIST_SIZE	16
#define FONT_ERROR		-1

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef struct FontTexture		///< Type definition of Font texture
{
	float pt, * wids, * hoss;
	int32 *		qvws, * qvhs;
	float *		qtws, * qths;
	uint32 *	textures;
	float		ascend;
} FontTexture;

typedef struct FontGlyph		///< Type definition of Font glyph
{
	float	pt;
	FT_Face face;
} FontGlyph;

typedef struct Font				///< Type definition of Font
{
	FontTexture * Texture;
	FontGlyph	* Glyph;
	bool		  Loaded;
} FontData;

FontData FontList[FONT_LIST_SIZE];	///< Font database

////////////////////////////////////////////////////////////
/// Initialize freetype library definition
///
////////////////////////////////////////////////////////////
bool FontInitialize();

////////////////////////////////////////////////////////////
/// Destroy freetype library definition
///
////////////////////////////////////////////////////////////
void FontDestroy();

////////////////////////////////////////////////////////////
/// Load a font from path
///
////////////////////////////////////////////////////////////
FontGlyph * FontGlyphLoad(char * Filename, int Size);

////////////////////////////////////////////////////////////
/// Load a font from glyph
///
////////////////////////////////////////////////////////////
FontTexture * FontTextureFromGlyph(FontGlyph * Font);

////////////////////////////////////////////////////////////
/// Destroy a font glyph
///
////////////////////////////////////////////////////////////
void FontGlyphDestroy(FontGlyph * Font);

////////////////////////////////////////////////////////////
/// Destroy a font texture
///
////////////////////////////////////////////////////////////
void FontTextureDestroy(FontTexture * Font);

////////////////////////////////////////////////////////////
/// Load font
///
////////////////////////////////////////////////////////////
int32 FontLoad(char * Path, uint32 Size);

////////////////////////////////////////////////////////////
/// Render the text from the font
///
////////////////////////////////////////////////////////////
void FontRender(FontTexture * Font, float x, float y, char * Text, GLubyte * Color);

////////////////////////////////////////////////////////////
/// Render the text
///
////////////////////////////////////////////////////////////
void FontRenderTextEx(int32 FontIndex, float X, float Y, char * Text, struct Color4ub * C);

////////////////////////////////////////////////////////////
/// Render the text (without color and font specification)
///
////////////////////////////////////////////////////////////
void FontRenderText(float X, float Y, char * Text);

////////////////////////////////////////////////////////////
/// Render the text (without index specification)
////////////////////////////////////////////////////////////
void FontRenderTextColor(float X, float Y, char * Text, struct Color4ub * Color);

#endif // GRAPHICS_FONT_H
