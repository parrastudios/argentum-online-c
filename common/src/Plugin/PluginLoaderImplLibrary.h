/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	PLUGIN_LOADER_IMPL_LIBRARY_H
#define PLUGIN_LOADER_IMPL_LIBRARY_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Plugin/PluginLoaderImpl.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get default plugin loader library implementation
////////////////////////////////////////////////////////////
struct PluginLoaderImplType * PluginLoaderImplLibraryGet();

////////////////////////////////////////////////////////////
/// Initialize library plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderImplLibraryInitialize(struct PluginLoaderImplType * PluginLoaderImpl);

////////////////////////////////////////////////////////////
/// Destroy library plugin loader
////////////////////////////////////////////////////////////
void PluginLoaderImplLibraryDestroy(struct PluginLoaderImplType * PluginLoaderImpl);

////////////////////////////////////////////////////////////
/// Load a plugin from library plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderImplLibraryLoad(struct PluginLoaderImplType * PluginLoaderImpl, PluginId Id, PluginLoaderImplResourceType * Impl);

////////////////////////////////////////////////////////////
/// Load a whole path of plugins from library plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderImplLibraryLoadPath(struct PluginLoaderImplType * PluginLoaderImpl, const char * Path, List LoadedIdList);

////////////////////////////////////////////////////////////
/// Register library from plugin loader to plugin handle
////////////////////////////////////////////////////////////
bool PluginLoaderImplLibraryRegister(struct PluginLoaderImplType * PluginLoaderImpl, PluginLoaderImplResourceType Impl, struct PluginType * Plugin);

////////////////////////////////////////////////////////////
/// Unload a plugin from library plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderImplLibraryUnload(struct PluginLoaderImplType * PluginLoaderImpl, PluginLoaderImplResourceType Impl);

////////////////////////////////////////////////////////////
/// Clear all plugins from library plugin loader
////////////////////////////////////////////////////////////
void PluginLoaderImplLibraryClear(struct PluginLoaderImplType * PluginLoaderImpl);

#endif // PLUGIN_LOADER_IMPL_LIBRARY_H
