/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/TextureRender.h>
#include <Graphics/Font.h>
#include <System/IOHelper.h>

#include <GUI/Widget.h>

////////////////////////////////////////////////////////////
// Clean table with default values
////////////////////////////////////////////////////////////
void CleanGrhTable(struct GuiTable * Table)
{
	int32 i,j;

	for (i = 0; i < GUI_TABLE_MAXSLOTS; i++)
	{
		for (j = 0; j < GUI_TABLE_MAXSLOTS; j++)
		{
			Table->GhrIndex[i*Table->Rows +j] = GUI_TABLE_DEFAULT_TILE_INVALID;
		}
	}

}


////////////////////////////////////////////////////////////
// Create a Table
////////////////////////////////////////////////////////////
struct GuiWidget * GuiTableCreate(int32 X, int32 Y, uint32 Rows, uint32 Columns, uint32 Span, uint32 Style)
{
	struct GuiWidget * Widget;


	// Create
	Widget = GuiWidgetCreate(GUI_WIDGET_TABLE, X, Y, 13, NULL);

	if (Widget)
	{
		Widget->Attributes.Table.Columns = Columns;
		Widget->Attributes.Table.Rows = Rows;
		Widget->Attributes.Table.SpanX = Span;
		Widget->Attributes.Table.SpanY = Span;
		Widget->Attributes.Table.Style = Style;
		CleanGrhTable(&Widget->Attributes.Table);
		return Widget;
	}


	return NULL;
}

////////////////////////////////////////////////////////////
// Create a Table by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiTableCreateDefault()
{
	// Create
	struct GuiWidget * Widget = GuiWidgetCreateDefault(GUI_WIDGET_TABLE);

	if (Widget)
	{
		Widget->Attributes.Table.Columns = 5;
		Widget->Attributes.Table.Rows = 5;
		Widget->Attributes.Table.SpanX = 0;
		Widget->Attributes.Table.SpanY = 0;
		Widget->Attributes.Table.Style = GUI_TABLE_STYLE_SQUARE;

		CleanGrhTable(&Widget->Attributes.Table);
		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Set Colums
////////////////////////////////////////////////////////////
void GuiTableSetColumns(struct GuiTable * Table, uint32 Columns)
{
	Table->Columns = Columns;
	CleanGrhTable(Table);
}

////////////////////////////////////////////////////////////
// Set Rows
////////////////////////////////////////////////////////////
void GuiTableSetRows(struct GuiTable * Table, uint32 Rows)
{
	Table->Rows = Rows;
	CleanGrhTable(Table);
}

////////////////////////////////////////////////////////////
// Set Grh of specific point
////////////////////////////////////////////////////////////
void GuiTableSetGrh(struct GuiTable * Table, int32 Grh, uint32 row, uint32 column, bool resetDef)
{
	if (!resetDef)
		Table->GhrIndex[column * Table->Rows + row] = Grh;
	else
		Table->GhrIndex[column * Table->Rows + row] = GUI_TABLE_DEFAULT_TILE_INVALID;

}

////////////////////////////////////////////////////////////
// Render a table
////////////////////////////////////////////////////////////
void GuiTableRender(struct GuiWidget * Form, struct GuiWidget * Widget, struct Color4ub * BackgroundColor, struct Color4ub * FontColor)
{
	struct GuiTable * Table = &Widget->Attributes.Table;
	uinteger i, j;
	// Render depends on style property on script

	for (i = 0; i < Table->Columns; i++)
	{
		for (j = 0; j < Table->Rows; j++)
		{
			if (Table->GhrIndex[i*Table->Rows + j] < 65535) {
				if (Table->Style == GUI_SHAPE_TYPE_CIRCUMFERENCE) {
					TextureRenderGrhRound(Table->GhrIndex[i*Table->Rows + j],
							Form->X + Widget->X + Widget->Width/2 + (Widget->Width + Table->SpanX)*(i),
							Form->Y + Widget->Y + Widget->Height/2 + (Widget->Width + Table->SpanY)*(j),
							Widget->Width/2,
							UINTEGER_SUFFIX(32),
							90.0f,
							360.0f,
							BackgroundColor);
				} else {
					TextureRenderGrh(Table->GhrIndex[i*Table->Rows + j],
							Form->X + Widget->X +  (Widget->Width + Table->SpanX)*i ,
							Form->Y + Widget->Y +  (Widget->Width + Table->SpanY)*j);
				}
			}
		}
	}
}
