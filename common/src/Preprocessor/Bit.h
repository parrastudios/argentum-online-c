/////////////////////////////////////////////////////////////////////////////
//	Preprocessor Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A cross-platform macro based library to give support
//	for preprocessor metaprogramming.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PREPROCESSOR_BIT_H
#define PREPROCESSOR_BIT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Preprocessor/Concatenation.h>
#include <Preprocessor/Complement.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PREPROCESSOR_BIT_AND_IMPL_0(Expr) 0
#define PREPROCESSOR_BIT_AND_IMPL_1(Expr) Expr

#define PREPROCESSOR_BIT_OR_IMPL_0(Expr) Expr
#define PREPROCESSOR_BIT_OR_IMPL_1(Expr) 1

#define PREPROCESSOR_BIT_XOR_IMPL_0(Expr) Expr
#define PREPROCESSOR_BIT_XOR_IMPL_1(Expr) PREPROCESSOR_COMPL(Expr)

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define PREPROCESSOR_BIT_AND(Expr) PREPROCESSOR_CONCAT(PREPROCESSOR_BIT_AND_IMPL_, Expr)

#define PREPROCESSOR_BIT_OR(Expr) PREPROCESSOR_CONCAT(PREPROCESSOR_BIT_OR_IMPL_, Expr)

#define PREPROCESSOR_BIT_XOR(Expr) PREPROCESSOR_CONCAT(PREPROCESSOR_BIT_XOR_IMPL_, Expr)

#endif // PREPROCESSOR_BIT_H
