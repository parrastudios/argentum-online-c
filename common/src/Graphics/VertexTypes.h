/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_VERTEX_TYPES_H
#define GRAPHICS_VERTEX_TYPES_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Vector.h>
#include <Graphics/Coordinates.h>
#include <Graphics/VertexAttribute.h>
#include <Graphics/VertexBuffer.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct VertexType
{
	struct Vector3f Position;
	struct Vector3f Normal;

	union
	{
		struct Coord2f Coord;
		struct Coord3f CoordTA; // Texture Array Component
	};
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////
void VertexSetPosition(struct VertexType * Vertex, float x, float y, float z);

void VertexSetNormal(struct VertexType * Vertex, float x, float y, float z);

void VertexSetTexture(struct VertexType * Vertex, float x, float y);

void VertexSetTextureA(struct VertexType * Vertex, float x, float y, float z);

void VertexAddToBuffer(struct VertexType * Vertex, struct VertexBuffer * Buffer);

struct VertexBuffer * VertexCreateBuffer(uint32 Size, bool Dynamic);

struct VertexBuffer * VertexCreateBufferA(uint32 Size, bool Dynamic);

#endif // GRAPHICS_VERTEX_TYPES_H