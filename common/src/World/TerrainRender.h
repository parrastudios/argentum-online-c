/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_TERRAIN_RENDER_H
#define WORLD_TERRAIN_RENDER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Component.h>
#include <Graphics/Cache.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define TERRAIN_RENDER_PATCH_LEVEL_INVALID		UINTEGER_MAX_RANGE
#define TERRAIN_RENDER_VERTEX_SIZE_INVALID		UINTEGER_MAX_RANGE
#define TERRAIN_RENDER_TRIANGLE_SIZE_INVALID	UINTEGER_MAX_RANGE
// #define TERRAIN_RENDER_USE_NV_VAR_FENCE			1

typedef enum
{
	TERRAIN_RENDER_DRAW_WIREFRAME			= 0,
	TERRAIN_RENDER_DRAW_TEXTURE				= 1,
	TERRAIN_RENDER_DRAW_TEXTURE_SPLAT		= 2,
	TERRAIN_RENDER_DRAW_TEXTURE_SPLAT_CVA	= 3

} TerrainRenderDrawMode;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct TerrainPatchRenderData
{
	uinteger						Level;					//< Patch mip level
	float							ViewPointDistance;		//< Perpendicular distance to viewpoint
	integer							ColourMapTexure;		//< Patch colourmap texture object for single pass multitexturing
	integer							BlendMapTexture[4];		//< Patch blend map texture objects for multi pass texture splatting
};

struct TerrainComponentRenderData
{
	TerrainRenderDrawMode			DrawMode;				//< Mode in which terrain will be rendered
	bool							TextureCombine;			//< Enable texture combine
	EntityIdType					CameraEntityId;			//< Id to camera entity
	uinteger						SourceHeight;			//< Height of the view field

	float							ErrorMetric;			//< Patch screen error
	uinteger *						Quadtree;				//< Implicit quadtree

	integer							BaseTexture;			//< Base texture object
	integer							SplatTexture[3];		//< Splat texture objects
	uinteger						TextureSize;			//< Texture tile size in pixels
	uinteger						TextureTile;			//< Number of texture tiles per height map side
	uinteger						BaseTextureTile;		//< Number of base texture tiles per height map side

	uinteger						PatchLevels;			//< Number of levels in a patch
	uint16 *						PatchIndex;				//< Patch indexes (max PatchSize = 128)
	struct CacheType				PatchVertexCache;		//< Cache for patch vertex arrays
	uinteger						PatchVertexCacheSize;	//< Requested cache size
	struct CacheObjectType **		PatchVertexPtr;			//< Pointers to the patch vertex arrays
	float *							PatchErrorArray;		//< Patch error arrays
	uinteger						PatchErrorArraySize;	//< Size of patch error array
	struct CacheType				PatchIndexCache;		//< Cache for patch index arrays
	uinteger						PatchIndexCacheSize;	//< Requested cache index size
	struct CacheObjectType **		PatchIndexPtr;			//< Pointers to the patch index arrays
	struct TerrainPatchRenderData *	Patches;				//< Render patches of terrain
};

struct TerrainComponentRenderType
{
	//< Set the camera target
	void (*SetCameraTarget)(struct EntityType * Entity, EntityIdType CameraId);

	//< Initialize terrain render component
	struct EntityType * (*Initialize)(struct EntityType * Entity,
		uinteger Height,
		uinteger TextureSize, uinteger TextureTile, uinteger BaseTextureTile,
		uinteger PatchVertexCacheSize, uinteger PatchIndexCacheSize);

	//< Set the error metric for the current field of view and window height
	void (*SetErrorMetric)(struct EntityType * Entity, float FovY, uinteger Height);

	//< Set the geo-mip level for the visible parts of the terrain
	void (*SetMipLevel)(struct EntityType * Entity);

	//< Force all patches to a given geo-mip level
	void (*ForceMipLevel)(struct EntityType * Entity, uinteger Level);

	//< Retreive a patch
	struct TerrainPatchRenderData * (*Patch)(struct EntityType * Entity, uinteger X, uinteger Y);
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the terrain render implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentRenderType * TerrainComponentRenderAttr();

////////////////////////////////////////////////////////////
/// Return the terrain render implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * TerrainComponentRenderImpl();

////////////////////////////////////////////////////////////
/// Return the terrain render derived data size
////////////////////////////////////////////////////////////
uinteger TerrainComponentRenderSize();

#endif // WORLD_TERRAIN_RENDER_H
