/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <HelloWorldPlugin.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PLUGIN_HELLO_WORLD_VERSION 0x00010000UL

////////////////////////////////////////////////////////////
/// Hello world example function
////////////////////////////////////////////////////////////
PLUGIN_API_EXPORT uint32 fnHelloWorld()
{
	return 42;
}

////////////////////////////////////////////////////////////
/// Define plugin interface implementation
////////////////////////////////////////////////////////////
PLUGIN_INTERFACE_DESCRIPTOR_IMPL(HelloWorldDescriptor, PLUGIN_PROGRAMMING_LANGUAGE_C, LANGUAGE_VERSION_TYPE, PLUGIN_HELLO_WORLD_VERSION)

////////////////////////////////////////////////////////////
/// Define plugin connection implementation
////////////////////////////////////////////////////////////
PLUGIN_CONNECTOR_IMPL(HelloWorldConnect)
{
	PLUGIN_REGISTER_CONSTRUCTOR(NULL);
	PLUGIN_REGISTER_DESTRUCTOR(NULL);

	PLUGIN_REGISTER_FUNCTION
	(
		PLUGIN_FUNCTION_ADDRESS(fnHelloWorld),
		PLUGIN_FUNCTION_RETURN(uint32),
		PLUGIN_FUNCTION_ARGS(void)
	);

	PLUGIN_REGISTER_COMMIT();
}

////////////////////////////////////////////////////////////
/// Export plugin functionalities
////////////////////////////////////////////////////////////
PLUGIN_CONNECTION_EXPORT()
