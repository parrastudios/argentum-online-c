/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_TEXTURE_H
#define GRAPHICS_TEXTURE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <Graphics/Color.h>
#include <Math/Geometry/Rect.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define TEXTURE_SCREENSHOT_SIZE     UINTEGER_MAX_RANGE	///< Max size of screenshots

enum
{
	TEXTURE_FORMAT_TGA = 0x00,
	TEXTURE_FORMAT_PNG = 0x01,
	TEXTURE_FORMAT_BMP = 0x02,
	TEXTURE_FORMAT_JPG = 0x03,
	TEXTURE_FORMAT_GIF = 0x04,
	TEXTURE_FORMAT_PSD = 0x05,
	TEXTURE_FORMAT_PIC = 0x06,
	TEXTURE_FORMAT_HDR = 0x07
};

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct Texture
{
	uinteger 			  Target;           ///< Texture target
	integer 			  Id;				///< Internal texture ID
	integer 			  Format;			///< Internal texture format
	uinteger              Channels;         ///< Number of color channels
    uinteger			  Width;			///< Image width
    uinteger			  Height;			///< Image Height
    uinteger			  TextureWidth;		///< Actual texture width (can be greater than image width because of padding)
    uinteger			  TextureHeight;	///< Actual texture height (can be greater than image height because of padding)
    uint8 *               Pixels;			///< Pixels of the image
};

////////////////////////////////////////////////////////////
/// Update correct size from GL_ARB
////////////////////////////////////////////////////////////
uinteger TextureValidSize(uinteger Size);

////////////////////////////////////////////////////////////
/// Create 2D texture with loaded data
////////////////////////////////////////////////////////////
bool TextureCreate(struct Texture * TextureData, bool Mipmap, bool ColorKey);

////////////////////////////////////////////////////////////
/// Bind 2D texture with previous cache
////////////////////////////////////////////////////////////
void TextureBind(struct Texture * TextureData);

////////////////////////////////////////////////////////////
/// Render a static 2D texture with triangle strip
////////////////////////////////////////////////////////////
void TextureRenderEx(struct Texture * TextureData, integer X, integer Y, struct Rect * RectData, struct Color4ub * Color);

////////////////////////////////////////////////////////////
/// Delete the texture from memory and OpenGL
////////////////////////////////////////////////////////////
void TextureDelete(struct Texture * TextureData);

////////////////////////////////////////////////////////////
/// Save texture from pixels as a file
///
/// \return true if saving was successful
///
////////////////////////////////////////////////////////////
bool TextureSave(char * Filename, integer Width, integer Height, integer Channels, uint8 * Data, uinteger Format);

////////////////////////////////////////////////////////////
/// Take a screenshot
////////////////////////////////////////////////////////////
void TextureScreenShot(uinteger Width, uinteger Height);

#endif // GRAPHICS_TEXTURE_H
