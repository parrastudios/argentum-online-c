/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/Win32/DeviceImpl.h>

//PFNWGLSWAPINTERVALEXTPROC				wglSwapIntervalEXT			= NULL;
//PFNWGLCHOOSEPIXELFORMATARBPROC			wglChoosePixelFormatARB		= NULL;

/*
// VBO
PFNGLGENBUFFERSARBPROC					pglGenBuffersARB			= NULL;	// VBO Name Generation Procedure
PFNGLBINDBUFFERARBPROC					pglBindBufferARB			= NULL;	// VBO Bind Procedure
PFNGLBUFFERDATAARBPROC					pglBufferDataARB			= NULL;	// VBO Data Loading Procedure
PFNGLBUFFERSUBDATAARBPROC				pglBufferSubDataARB			= NULL;	// VBO Sub Data Loading Procedure
PFNGLDELETEBUFFERSARBPROC				pglDeleteBuffersARB			= NULL;	// VBO Deletion Procedure
PFNGLGETBUFFERPARAMETERIVARBPROC		pglGetBufferParameterivARB	= NULL;	// return various parameters of VBO
PFNGLMAPBUFFERARBPROC					pglMapBufferARB				= NULL;	// map VBO procedure
PFNGLUNMAPBUFFERARBPROC					pglUnmapBufferARB			= NULL; // unmap VBO procedure

// Shaders
PFNGLCREATEPROGRAMOBJECTARBPROC			glCreateProgramObjectARB;
PFNGLDELETEOBJECTARBPROC				glDeleteObjectARB;
PFNGLUSEPROGRAMOBJECTARBPROC			glUseProgramObjectARB;
PFNGLCREATESHADEROBJECTARBPROC			glCreateShaderObjectARB;
PFNGLSHADERSOURCEARBPROC				glShaderSourceARB;
PFNGLCOMPILESHADERARBPROC				glCompileShaderARB;
PFNGLGETOBJECTPARAMETERIVARBPROC		glGetObjectParameterivARB;
PFNGLATTACHOBJECTARBPROC				glAttachObjectARB;
PFNGLGETINFOLOGARBPROC					glGetInfoLogARB;
PFNGLLINKPROGRAMARBPROC					glLinkProgramARB;
PFNGLGETUNIFORMLOCATIONARBPROC			glGetUniformLocationARB;
PFNGLUNIFORM4FARBPROC					glUniform4fARB;
PFNGLUNIFORM3FARBPROC					glUniform3fARB;
PFNGLUNIFORM2FARBPROC					glUniform2fARB;
PFNGLUNIFORM1FARBPROC					glUniform1fARB;
PFNGLUNIFORM1IARBPROC					glUniform1iARB;
PFNGLGETATTRIBLOCATIONARBPROC			glGetAttribLocationARB;
PFNGLVERTEXATTRIB3FARBPROC				glVertexAttrib3fARB;
PFNGLVERTEXATTRIBPOINTERARBPROC			glVertexAttribPointerARB;
PFNGLENABLEVERTEXATTRIBARRAYARBPROC		glEnableVertexAttribArrayARB;
PFNGLDISABLEVERTEXATTRIBARRAYARBPROC	glDisableVertexAttribArrayARB;
PFNGLACTIVETEXTUREARBPROC				pglActiveTextureARB;

// FBO
PFNGLGENRENDERBUFFERSEXTPROC			pglGenRenderbuffersEXT;
PFNGLBINDRENDERBUFFEREXTPROC			pglBindRenderbufferEXT;
PFNGLRENDERBUFFERSTORAGEEXTPROC			pglRenderbufferStorageEXT;
PFNGLGENFRAMEBUFFERSEXTPROC				pglGenFramebuffersEXT;
PFNGLBINDFRAMEBUFFEREXTPROC				pglBindFramebufferEXT;
PFNGLFRAMEBUFFERTEXTURE2DEXTPROC		pglFramebufferTexture2DEXT;
PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC		pglFramebufferRenderbufferEXT;
PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC		pglCheckFramebufferStatusEXT;
*/