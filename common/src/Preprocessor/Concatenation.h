/////////////////////////////////////////////////////////////////////////////
//	Preprocessor Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A cross-platform macro based library to give support
//	for preprocessor metaprogramming.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PREPROCESSOR_CONCATENATION_H
#define PREPROCESSOR_CONCATENATION_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Compiler.h>
#include <Preprocessor/Recursion.h>

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#if COMPILER_TYPE == COMPILER_MSVC

#	define PREPROCESSOR_CONCAT_IMPL_II(Token, Expr) Expr

#	define PREPROCESSOR_CONCAT_IMPL_I(Left, Right) PREPROCESSOR_CONCAT_IMPL_II(~, Left ## Right)

#	define PREPROCESSOR_CONCAT_IMPL(Left, Right) PREPROCESSOR_CONCAT_IMPL_I(Left, Right)

#else

#	define PREPROCESSOR_CONCAT_IMPL(Left, Right) Left ## Right

#endif

#if COMPILER_TYPE == COMPILER_MWERKS

#	define PREPROCESSOR_CONCAT_IMPL_TOKEN(Token) PREPROCESSOR_CONCAT_IMPL ## Token

#	define PREPROCESSOR_CONCAT(Left, Right) PREPROCESSOR_CONCAT_IMPL_TOKEN((Left, Right))

#else

#	define PREPROCESSOR_CONCAT(Left, Right) PREPROCESSOR_CONCAT_IMPL(Left, Right)

#endif

#if COMPILER_TYPE == COMPILER_MSVC

#	define PREPROCESSOR_CONCAT_VARIDIC_IMPL_II(Token, ...) __VA_ARGS__

#	define PREPROCESSOR_CONCAT_VARIDIC_IMPL_I(Left, ...) PREPROCESSOR_CONCAT_VARIDIC_IMPL_II(~, Left ## __VA_ARGS__)

#else

#	define PREPROCESSOR_CONCAT_VARIDIC_IMPL_I(Left, ...) Left ## __VA_ARGS__

#endif

#if COMPILER_TYPE == COMPILER_MWERKS

#	define PREPROCESSOR_CONCAT_VARIDIC_IMPL_TOKEN(Token) PREPROCESSOR_CONCAT_VARIDIC_IMPL_I ## Token

#	define PREPROCESSOR_CONCAT_VARIDIC_IMPL(Left, ...) PREPROCESSOR_CONCAT_VARIDIC_IMPL_TOKEN((Left, __VA_ARGS__))

#else

#	define PREPROCESSOR_CONCAT_VARIDIC_IMPL(Left, ...) PREPROCESSOR_CONCAT_VARIDIC_IMPL_I(Left, __VA_ARGS__)

#endif

#define PREPROCESSOR_CONCAT_VARIDIC(...) \
	PREPROCESSOR_RECURSION(PREPROCESSOR_CONCAT_VARIDIC_IMPL, __VA_ARGS__)

#endif // PREPROCESSOR_CONCATENATION_H
