/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/Hash.h>

#include <Portability/Architecture.h>
#include <Portability/Endianness.h>

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Default hash function for a uint32 (pointer)
////////////////////////////////////////////////////////////
HashData HashCallbackUInt32(HashKeyData Key);

////////////////////////////////////////////////////////////
/// Default hash function for a uint64 (pointer)
////////////////////////////////////////////////////////////
HashData HashCallbackUInt64(HashKeyData Key);

////////////////////////////////////////////////////////////
/// SDBM hash function algorithm for strings
////////////////////////////////////////////////////////////
HashData HashCallbackStrSDBM(HashKeyData Key);

////////////////////////////////////////////////////////////
/// DJB2 hash function algorithm for strings
////////////////////////////////////////////////////////////
HashData HashCallbackStrDJB2(HashKeyData Key);

////////////////////////////////////////////////////////////
/// Alder32 hash function algorithm for strings
////////////////////////////////////////////////////////////
HashData HashCallbackStrAlder32(HashKeyData Key);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get default hash integer function
////////////////////////////////////////////////////////////
HashCallback HashGetCallbackUnsignedIntegerDefault()
{
	#if ARCH_FAMILY == ARCHITECTURE_32

	return &HashCallbackUInt32;
	
	#elif ARCH_FAMILY == ARCHITECTURE_64

	
	return &HashCallbackUInt64;

	#else
	
	return NULL;

	#endif
}

////////////////////////////////////////////////////////////
/// Get default hash string function
////////////////////////////////////////////////////////////
HashCallback HashGetCallbackStringDefault()
{
	return &HashCallbackStrSDBM;
}

////////////////////////////////////////////////////////////
/// Get desired hash function
////////////////////////////////////////////////////////////
HashCallback HashGetCallback(HashCallbackId Id)
{
	if (Id < HASH_CALLBACK_SIZE)
	{
		// Hash callback table definition
		static const HashCallback HashCallbackTable[HASH_CALLBACK_SIZE] =
		{
			ArrayInit(HASH_CALLBACK_UINT32, &HashCallbackUInt32),
			ArrayInit(HASH_CALLBACK_UINT64, &HashCallbackUInt64),
			ArrayInit(HASH_CALLBACK_STR_SDBM, &HashCallbackStrSDBM),
			ArrayInit(HASH_CALLBACK_STR_DJB2, &HashCallbackStrDJB2),
			ArrayInit(HASH_CALLBACK_STR_ADLER32, &HashCallbackStrAlder32)
		};

		return HashCallbackTable[Id];
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Default hash function for a uint32 (pointer)
////////////////////////////////////////////////////////////
HashData HashCallbackUInt32(HashKeyData Key)
{
	if (Key)
	{
		uint32 UInt32 = *((uint32 *)Key);
		
		// Convert to little endian
		UInt32 = EndiannessFromHostUInt32(UInt32, ENDIAN_LITTLE);

		// Generate hash
		UInt32 = ((UInt32 >> 16) ^ UInt32) * UINT32_SUFFIX(0x045D9F3B);
		UInt32 = ((UInt32 >> 16) ^ UInt32) * UINT32_SUFFIX(0x045D9F3B);
		UInt32 = (UInt32 >> 16) ^ UInt32;

		return (HashData)(UInt32);
	}

	return HASH_INVALID;
}

////////////////////////////////////////////////////////////
/// Default hash function for a uint64 (pointer)
////////////////////////////////////////////////////////////
HashData HashCallbackUInt64(HashKeyData Key)
{
	if (Key)
	{
		uint64 UInt64 = *((uint64 *)Key);

		// Convert to little endian
		UInt64 = EndiannessFromHostUInt64(UInt64, ENDIAN_LITTLE);

		// Generate hash
		UInt64 = UInt64 * UINT64_SUFFIX(0x369DEA0F31A53F85) + UINT64_SUFFIX(0x255992D382208B61);

		UInt64 ^= UInt64 >> 21;
		UInt64 ^= UInt64 << 37;
		UInt64 ^= UInt64 >> 4;

		UInt64 *= UINT64_SUFFIX(0x422E19E1D95D2F0D);

		UInt64 ^= UInt64 << 20;
		UInt64 ^= UInt64 >> 41;
		UInt64 ^= UInt64 << 5;

		return (HashData)(UInt64);
	}

	return HASH_INVALID;
}

////////////////////////////////////////////////////////////
/// SDBM hash function algorithm for strings
////////////////////////////////////////////////////////////
HashData HashCallbackStrSDBM(HashKeyData Key)
{
	uint8 * Word = (uint8 *)(Key);

	if (Word)
	{
		HashData Hash = UINTEGER_SUFFIX(0x00);

		while (*Word != NULLCHAR)
		{
			Hash = (HashData)(*Word + (Hash << 6) + (Hash << 16) - Hash);

			++Word;
		}

		return Hash;
	}

	return HASH_INVALID;
}

////////////////////////////////////////////////////////////
/// DJB2 hash function algorithm for strings
////////////////////////////////////////////////////////////
HashData HashCallbackStrDJB2(HashKeyData Key)
{
	uint8 * Word = (uint8 *)(Key);

	if (Word)
	{
		HashData Hash = UINTEGER_SUFFIX(0x1505);

		while (*Word != NULLCHAR)
		{
			Hash = (HashData)(((Hash << 5) + Hash) + *Word);

			++Word;
		}

		return Hash;
	}

	return HASH_INVALID;
}

////////////////////////////////////////////////////////////
/// Alder32 hash function algorithm for strings
////////////////////////////////////////////////////////////
HashData HashCallbackStrAlder32(HashKeyData Key)
{
	uint8 * Word = (uint8 *)(Key);

	if (Word)
	{
		HashData Hash[2] =
		{
			ArrayInit(0, UINTEGER_SUFFIX(0x01)),
			ArrayInit(1, UINTEGER_SUFFIX(0x00))
		};

		while (*Word != NULLCHAR)
		{
			Hash[0] = (HashData)((Hash[0] + *Word) % UINTEGER_SUFFIX(0xFFF1));
			Hash[1] = (HashData)((Hash[0] + Hash[1]) % UINTEGER_SUFFIX(0xFFF1));

			++Word;
		}

		return (HashData)((Hash[1] << 16) | Hash[0]);
	}

	return HASH_INVALID;
}
