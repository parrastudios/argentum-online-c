/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/Skinning.h>

#include <System/IOHelper.h> // todo: remove this

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

bool SkinningGetLocations(struct SkinningTechniqueType * Skinning)
{
	if (Skinning)
	{
	    uinteger i;

		// Link shader bone uniforms
		for (i = 0; i < SKINNING_BONES_MAX; ++i)
		{
			char UniformName[0x40];

			sprintf(UniformName, "Bones[%d]", i);
			Skinning->BoneLocations[i] = ShaderProgramGetUniformLocation(&Skinning->LightingBaseTechnique.Technique, UniformName);

			if (Skinning->BoneLocations[i] == GL_UNIFORM_LOCATION_INVALID)
			{
				return false;
			}
		}

		return true;
	}

	return false;
}

bool SkinningInitialize(struct SkinningTechniqueType * Skinning)
{
	if (Skinning)
	{
		// Initialize shader program, load shaders and build the program
		if (ShaderProgramInitialize(&Skinning->LightingBaseTechnique.Technique) &&
			ShaderProgramAppend(&Skinning->LightingBaseTechnique.Technique, GL_VERTEX_SHADER, "data/shaders/gl3/Skinning.vs") &&
			ShaderProgramAppend(&Skinning->LightingBaseTechnique.Technique, GL_FRAGMENT_SHADER, "data/shaders/gl3/Skinning.fs") &&
			ShaderProgramLink(&Skinning->LightingBaseTechnique.Technique))
		{
			return LightingBaseGetLocations(&Skinning->LightingBaseTechnique) && SkinningGetLocations(Skinning);
		}
	}

	return false;
}

void SkinningSetWVP(struct SkinningTechniqueType * Skinning, struct Matrix4f * WVP)
{
    if (Skinning)
    {
        LightingBaseSetWVP(&Skinning->LightingBaseTechnique, WVP);
    }
}

void SkinningSetWorldMatrix(struct SkinningTechniqueType * Skinning, struct Matrix4f * World)
{
    if (Skinning)
    {
        LightingBaseSetWorldMatrix(&Skinning->LightingBaseTechnique, World);
    }
}

void SkinningSetColorTextureUnit(struct SkinningTechniqueType * Skinning, uinteger TextureUnit)
{
    if (Skinning)
    {
        LightingBaseSetColorTextureUnit(&Skinning->LightingBaseTechnique, TextureUnit);
    }
}

void SkinningSetDirectionalLight(struct SkinningTechniqueType * Skinning, struct LightDirectional * Light)
{
    if (Skinning)
    {
        LightingBaseSetDirectionalLight(&Skinning->LightingBaseTechnique, Light);
    }
}

void SkinningSetPointLights(struct SkinningTechniqueType * Skinning, struct LightPoint * Lights, uinteger Size)
{
    if (Skinning)
    {
        LightingBaseSetPointLights(&Skinning->LightingBaseTechnique, Lights, Size);
    }
}

void SkinningSetSpotLights(struct SkinningTechniqueType * Skinning, struct LightSpot * Lights, uinteger Size)
{
    if (Skinning)
    {
        LightingBaseSetSpotLights(&Skinning->LightingBaseTechnique, Lights, Size);
    }
}

void SkinningSetEyeWorldPos(struct SkinningTechniqueType * Skinning, struct Vector3f * EyeWorldPos)
{
    if (Skinning)
    {
        LightingBaseSetEyeWorldPos(&Skinning->LightingBaseTechnique, EyeWorldPos);
    }
}

void SkinningSetMatSpecularIntensity(struct SkinningTechniqueType * Skinning, float Intensity)
{
    if (Skinning)
    {
        LightingBaseSetMatSpecularIntensity(&Skinning->LightingBaseTechnique, Intensity);
    }
}

void SkinningSetMatSpecularPower(struct SkinningTechniqueType * Skinning, float Power)
{
    if (Skinning)
    {
        LightingBaseSetMatSpecularPower(&Skinning->LightingBaseTechnique, Power);
    }
}

void SkinningSetBoneTransform(struct SkinningTechniqueType * Skinning, uinteger Index, struct Matrix4f * Transform)
{
	if (Skinning && Transform)
	{
		DeviceGetExt()->glUniformMatrix4fv(Skinning->BoneLocations[Index], 1, GL_TRUE, (const GLfloat *)&Transform->v[0]);
	}
}

void SkinningDestroy(struct SkinningTechniqueType * Skinning)
{
	if (Skinning)
	{
		// Destroy lighting base shader program
		LightingBaseDestroy(&Skinning->LightingBaseTechnique);
	}
}
