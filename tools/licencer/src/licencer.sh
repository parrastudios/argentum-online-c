#!/bin/bash
# nekun: 11-12-2013
# bash script for adding the license text at the begining of the source files

for i in `find -type f`
do #para cada resultado del comando find
   filename=$(basename "$i") #obtiene el nombre del archivo
   extension="${filename##*.}" #obtiene la extension
   if [ $extension = "h" ] || [ $extension = "c" ] || [ $extension = "asm" ];
   then #si la extension es h, c o asm
      mv $i "tmp"; #cambia el nombre del archivo a tmp
      echo "Aplicando licencia a $i"; #solo informa el archivo a editar
      cat "licencia" "tmp" > $i; #concatena la licencia con el archivo tmp y guarda el resultado en el archivo original (lo vuelve a crear)
      rm "tmp"; #elimina el archivo tmp qe ya no es necesario
   fi
done