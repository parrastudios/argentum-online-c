/*
***********************************************************************************
 IqeB_Texture_Utils.cpp

 Texture utilities. Load images.

02.01.2015 RR: * First editon of this file.

*/

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/gl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

// ...

#include "jpeg/jpeglib.h"

#ifndef GL_GENERATE_MIPMAP
#define GL_GENERATE_MIPMAP 0x8191
#endif

/************************************************************************************
 * ....
 */

/*
 * Use Sean Barrett's excellent stb_image to load textures.
 */

#define STBI_NO_HDR
#include ".\stb_image.c"   // Use Sean Barrett's excellent stb_image to load textures.

/************************************************************************************
 * IqeB_TextureInitChecker
 */

static unsigned char checker_data[256*256];
static unsigned int checker_texture = 0;

void IqeB_TextureInitChecker(void)
{
	int x, y, i = 0;
	for (y = 0; y < 256; y++) {
		for (x = 0; x < 256; x++) {
			int k = ((x>>5) & 1) ^ ((y>>5) & 1);
			checker_data[i++] = k ? 255 : 192;
		}
	}
	glGenTextures(1, &checker_texture);
	glBindTexture(GL_TEXTURE_2D, checker_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, 1, 256, 256, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, checker_data);
}

/************************************************************************************
 * lowerstring
 */

static void lowerstring(char *s)
{
	while (*s) { *s = tolower(*s); s++; }
}

/************************************************************************************
 * .jpg file loading
 *
 * Original code by Robert 'Heffo' Heffernan
 */

static void jpg_null(j_decompress_ptr cinfo)
{
}

static unsigned char jpg_fill_input_buffer(j_decompress_ptr cinfo)
{
    //x/ri.Con_Printf(PRINT_ALL, "Premature end of JPEG data\n");
    return 1;
}

static void jpg_skip_input_data(j_decompress_ptr cinfo, long num_bytes)
{

    cinfo->src->next_input_byte += (size_t) num_bytes;
    cinfo->src->bytes_in_buffer -= (size_t) num_bytes;

    if (cinfo->src->bytes_in_buffer < 0) {
		  //x/ri.Con_Printf(PRINT_ALL, "Premature end of JPEG data\n");
    }
}

static void jpeg_mem_src(j_decompress_ptr cinfo, unsigned char *mem, int len)
{
    cinfo->src = (struct jpeg_source_mgr *)(*cinfo->mem->alloc_small)((j_common_ptr) cinfo, JPOOL_PERMANENT, sizeof(struct jpeg_source_mgr));
    cinfo->src->init_source = jpg_null;
    cinfo->src->fill_input_buffer = jpg_fill_input_buffer;
    cinfo->src->skip_input_data = jpg_skip_input_data;
    cinfo->src->resync_to_restart = jpeg_resync_to_restart;
    cinfo->src->term_source = jpg_null;
    cinfo->src->bytes_in_buffer = len;
    cinfo->src->next_input_byte = mem;
}

//

static unsigned char *IqeB_TextureLoad_jpg(char const *filename, int *pX, int *pY, int *comp)
{
	struct jpeg_decompress_struct	cinfo;
	struct jpeg_error_mgr			jerr;
	unsigned char	*result    = NULL;
  unsigned char *pFileData = NULL;
	unsigned char *scanline, *p, *q;
	int		i;

  // open file

  FILE *f = fopen(filename, "rb");
  if (!f) return NULL;          // file not found

  // get size of file

  int		pos;
	int		fileLen;

	pos = ftell (f);
	fseek (f, 0, SEEK_END);
	fileLen = ftell (f);
	fseek (f, pos, SEEK_SET);

  // Allocate a local buffer

	pFileData = (unsigned char *)malloc( fileLen);
	if( pFileData == NULL) {  // malloc failed

    fclose(f);
    return( NULL);
	}

	// read file to the buffer

	fread( pFileData, 1, fileLen, f);
  fclose(f);

  // load the image ...

  // Knightmare- check for bad data
	if (	pFileData[6] != 'J'
		||	pFileData[7] != 'F'
		||	pFileData[8] != 'I'
		||	pFileData[9] != 'F')
	{
		//x/ri.Con_Printf (PRINT_ALL, "Bad jpg file %s\n", filename);
		free( pFileData);
		return( NULL);
	}

	// Initialise libJpeg Object
	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_decompress(&cinfo);

	// Feed JPEG memory into the libJpeg Object
	jpeg_mem_src(&cinfo, pFileData, fileLen);

	// Process JPEG header
	jpeg_read_header(&cinfo, true);

	// Start Decompression
	jpeg_start_decompress(&cinfo);

	// Check Colour Components
	if( cinfo.output_components != 1 && cinfo.output_components != 3 && cinfo.output_components != 4)
	{
		//x/ri.Con_Printf(PRINT_ALL, "Invalid JPEG colour components\n");
		jpeg_destroy_decompress(&cinfo);
		free( pFileData);
		return( NULL);
	}

	// Allocate Memory for decompressed image
	result = (unsigned char *)malloc(cinfo.output_width * cinfo.output_height * 4);
	if( result == NULL) { // secuurity test
		//x/ri.Con_Printf(PRINT_ALL, "Insufficient RAM for JPEG buffer\n");
		jpeg_destroy_decompress(&cinfo);
		free( pFileData);
		return( NULL);
	}

	// Pass sizes to output
	*pX = cinfo.output_width;
	*pY = cinfo.output_height;
	*comp = 4;    // return 4 samples, RGB + alpha. Alpha is always set too none transparent.


	// Allocate Scanline buffer
	scanline = (unsigned char *)malloc(cinfo.output_width * 3);
	if( scanline == NULL)
	{
		//x/ri.Con_Printf(PRINT_ALL, "Insufficient RAM for JPEG scanline buffer\n");
		free( result);
		jpeg_destroy_decompress(&cinfo);
		free( pFileData);
		return( NULL);
	}

	// Read Scanlines, and expand from RGB to RGBA
	q = result;
	while(cinfo.output_scanline < cinfo.output_height)
	{
		p = scanline;
		jpeg_read_scanlines(&cinfo, &scanline, 1);

		if( cinfo.output_components == 1 ) {

      for( i = 0; i < (int)cinfo.output_width; i++)
      {
        q[0] =  q[1] = q[2] = *p++;
        q[3] = 255;

        q+=4;
      }
		}
		else
		{
      for( i = 0; i < (int)cinfo.output_width; i++)
      {
        q[0] = p[0];
        q[1] = p[1];
        q[2] = p[2];
        q[3] = 255;

        p+=3; q+=4;
      }
    }
	}

	// Free the scanline buffer
	free(scanline);

	// Finish Decompression
	jpeg_finish_decompress(&cinfo);

	// Destroy JPEG object
	jpeg_destroy_decompress(&cinfo);

  // free buffers
	free( pFileData);

  return( result);
}

/************************************************************************************
 * .pcx file loading
 */

// header of .pcx file

typedef struct
{
    char	manufacturer;
    char	version;
    char	encoding;
    char	bits_per_pixel;
    unsigned short	xmin,ymin,xmax,ymax;
    unsigned short	hres,vres;
    unsigned char	palette[48];
    char	reserved;
    char	color_planes;
    unsigned short	bytes_per_line;
    unsigned short	palette_type;
    char	filler[58];
    unsigned char	data;			// unbounded
} pcx_t;

//here i convert old 256 color to RGB -- hax0r l337
// 21.12.2014 RR: a Quake2 palette
static unsigned char default_pal[768] =
{
0,0,0,15,15,15,31,31,31,47,47,47,63,63,63,75,75,75,91,91,91,107,107,107,123,123,123,139,139,139,155,155,
155,171,171,171,187,187,187,203,203,203,219,219,219,235,235,235,99,75,35,91,67,31,83,63,31,79,59,27,71,55,
27,63,47,23,59,43,23,51,39,19,47,35,19,43,31,19,39,27,15,35,23,15,27,19,11,23,15,11,19,15,7,15,11,7,95,95,
111,91,91,103,91,83,95,87,79,91,83,75,83,79,71,75,71,63,67,63,59,59,59,55,55,51,47,47,47,43,43,39,39,39,35,
35,35,27,27,27,23,23,23,19,19,19,143,119,83,123,99,67,115,91,59,103,79,47,207,151,75,167,123,59,139,103,47,
111,83,39,235,159,39,203,139,35,175,119,31,147,99,27,119,79,23,91,59,15,63,39,11,35,23,7,167,59,43,159,47,
35,151,43,27,139,39,19,127,31,15,115,23,11,103,23,7,87,19,0,75,15,0,67,15,0,59,15,0,51,11,0,43,11,0,35,11,
0,27,7,0,19,7,0,123,95,75,115,87,67,107,83,63,103,79,59,95,71,55,87,67,51,83,63,47,75,55,43,67,51,39,63,47,
35,55,39,27,47,35,23,39,27,19,31,23,15,23,15,11,15,11,7,111,59,23,95,55,23,83,47,23,67,43,23,55,35,19,39,
27,15,27,19,11,15,11,7,179,91,79,191,123,111,203,155,147,215,187,183,203,215,223,179,199,211,159,183,195,
135,167,183,115,151,167,91,135,155,71,119,139,47,103,127,23,83,111,19,75,103,15,67,91,11,63,83,7,55,75,7,47,
63,7,39,51,0,31,43,0,23,31,0,15,19,0,7,11,0,0,0,139,87,87,131,79,79,123,71,71,115,67,67,107,59,59,99,51,51,
91,47,47,87,43,43,75,35,35,63,31,31,51,27,27,43,19,19,31,15,15,19,11,11,11,7,7,0,0,0,151,159,123,143,151,115,
135,139,107,127,131,99,119,123,95,115,115,87,107,107,79,99,99,71,91,91,67,79,79,59,67,67,51,55,55,43,47,47,
35,35,35,27,23,23,19,15,15,11,159,75,63,147,67,55,139,59,47,127,55,39,119,47,35,107,43,27,99,35,23,87,31,19,
79,27,15,67,23,11,55,19,11,43,15,7,31,11,7,23,7,0,11,0,0,0,0,0,119,123,207,111,115,195,103,107,183,99,99,167,
91,91,155,83,87,143,75,79,127,71,71,115,63,63,103,55,55,87,47,47,75,39,39,63,35,31,47,27,23,35,19,15,23,11,7,
7,155,171,123,143,159,111,135,151,99,123,139,87,115,131,75,103,119,67,95,111,59,87,103,51,75,91,39,63,79,27,
55,67,19,47,59,11,35,47,7,27,35,0,19,23,0,11,15,0,0,255,0,35,231,15,63,211,27,83,187,39,95,167,47,95,143,51,
95,123,51,255,255,255,255,255,211,255,255,167,255,255,127,255,255,83,255,255,39,255,235,31,255,215,23,255,
191,15,255,171,7,255,147,0,239,127,0,227,107,0,211,87,0,199,71,0,183,59,0,171,43,0,155,31,0,143,23,0,127,15,
0,115,7,0,95,0,0,71,0,0,47,0,0,27,0,0,239,0,0,55,55,255,255,0,0,0,0,255,43,43,35,27,27,23,19,19,15,235,151,
127,195,115,83,159,87,51,123,63,27,235,211,199,199,171,155,167,139,119,135,107,87,159,91,83
};

//

static unsigned char *IqeB_TextureLoad_pcx(char const *filename, int *pX, int *pY, int *comp)
{
  unsigned char *result = NULL;
  unsigned char *pFileData = NULL;
  unsigned char *pix;
  unsigned char *pColorTable;
	pcx_t	*pcx;
	int		x, y;
	int		dataByte, runLength;

  // open file

  FILE *f = fopen(filename, "rb");
  if (!f) return NULL;          // file not found

  // get size of file

  int		pos;
	int		fileLen;

	pos = ftell (f);
	fseek (f, 0, SEEK_END);
	fileLen = ftell (f);
	fseek (f, pos, SEEK_SET);

	if( fileLen <= 0) {    // security test file length

    return( NULL);
	}

  // Allocate a local buffer

	pFileData = (unsigned char *)malloc( fileLen);
	if( pFileData == NULL) {  // malloc failed

    fclose(f);
    return( NULL);
	}

	// read file to the buffer

	fread( pFileData, 1, fileLen, f);
  fclose(f);

	//
	// parse the PCX file
	//

	pcx = (pcx_t *)pFileData;

  pcx->xmin = LittleShort(pcx->xmin);
  pcx->ymin = LittleShort(pcx->ymin);
  pcx->xmax = LittleShort(pcx->xmax);
  pcx->ymax = LittleShort(pcx->ymax);
  pcx->hres = LittleShort(pcx->hres);
  pcx->vres = LittleShort(pcx->vres);
  pcx->bytes_per_line = LittleShort(pcx->bytes_per_line);
  pcx->palette_type = LittleShort(pcx->palette_type);

	pFileData = &pcx->data;

	if (pcx->manufacturer != 0x0a
		|| pcx->version != 5
		|| pcx->encoding != 1
		|| pcx->bits_per_pixel != 8
		|| pcx->color_planes != 1
		|| pcx->xmax >= 640
		|| pcx->ymax >= 480)
	{
		//x/ ri.Con_Printf (PRINT_ALL, "Bad pcx file %s\n", filename);

    free( pFileData);
    return( NULL);
	}

  // test for color palette

  pColorTable = default_pal;    // preset default color table

  if( fileLen > 768) {   // have enough data for a palette

    if( pFileData[ fileLen - 768 - 1] == 12) {  // is magic number, palette is present

      pColorTable = pFileData - 768;
    }
  }

  // malloc memory
	result = (unsigned char *)malloc( (pcx->ymax+1) * (pcx->xmax+1) * 4);
	if( result == NULL) {  // malloc failed

    free( pFileData);
    return( NULL);
	}

	pix = result;

  *pX = pcx->xmax+1;
	*pY = pcx->ymax+1;
	*comp = 4;

	for (y=0 ; y<=pcx->ymax ; y++, pix += (pcx->xmax+1) * 4)
	{
		for (x=0 ; x<=pcx->xmax ; )
		{
			dataByte = *pFileData++;

			if((dataByte & 0xC0) == 0xC0)
			{
				runLength = dataByte & 0x3F;
				dataByte = *pFileData++;
			}
			else
				runLength = 1;

			while(runLength-- > 0) {

        if( pColorTable != NULL) {

				  pix[ x * 4 + 0] = pColorTable[ dataByte * 3 + 0];
				  pix[ x * 4 + 1] = pColorTable[ dataByte * 3 + 1];
				  pix[ x * 4 + 2] = pColorTable[ dataByte * 3 + 2];
        } else {

				  pix[ x * 4 + 0] = default_pal[ dataByte * 3 + 0];
				  pix[ x * 4 + 1] = default_pal[ dataByte * 3 + 1];
				  pix[ x * 4 + 2] = default_pal[ dataByte * 3 + 2];
        }
  		  pix[ x * 4 + 3] = 0xff;

				x++;
			}
		}

	}

	if ( pFileData - (unsigned char *)pcx > fileLen)
	{
		//x/ ri.Con_Printf (PRINT_DEVELOPER, "PCX file %s was malformed", filename);

    free( result);
    free( pFileData);
    return( NULL);
	}

  // OK

  free( pFileData);

  return( result);
}

/************************************************************************************
 * IqeB_TextureLoadToOpenGL
 *
 * return:    *   Error loading texture
 *          > 0   an OpenGL texture reference
 */

unsigned int IqeB_TextureLoadToOpenGL( char *filename)
{
	unsigned int texture;
	unsigned char *image;
	int w, h, n, intfmt = 0, fmt = 0;

	// load depending from file suffix

	w = strlen( filename);

	if( w >= 4 && strcasecmp( filename + w - 4, ".pcx") == 0) {   // .pcx file

    // do pcx

  	image = IqeB_TextureLoad_pcx(filename, &w, &h, &n);
	  if (!image) {
		  lowerstring(filename);
		  image = IqeB_TextureLoad_pcx(filename, &w, &h, &n);
		  if (!image) {
			  //x/fprintf(stderr, "cannot load texture '%s'\n", filename);
			  return 0;
		  }
	  }

	} else if( w >= 4 && strcasecmp( filename + w - 4, ".jpg") == 0) {  // .jpg file

    // do pcx

  	image = IqeB_TextureLoad_jpg(filename, &w, &h, &n);
	  if (!image) {
		  lowerstring(filename);
		  image = IqeB_TextureLoad_jpg(filename, &w, &h, &n);
		  if (!image) {
			  //x/fprintf(stderr, "cannot load texture '%s'\n", filename);
			  return 0;
		  }
	  }

	} else {

	  // try other formats

  	image = stbi_load(filename, &w, &h, &n, 0);
	  if (!image) {
		  lowerstring(filename);
		  image = stbi_load(filename, &w, &h, &n, 0);
		  if (!image) {
			  //x/fprintf(stderr, "cannot load texture '%s'\n", filename);
			  return 0;
		  }
	  }
	}

	if (n == 1) { intfmt = fmt = GL_LUMINANCE; }
	if (n == 2) { intfmt = fmt = GL_LUMINANCE_ALPHA; }
	if (n == 3) { intfmt = fmt = GL_RGB; }
	if (n == 4) { intfmt = fmt = GL_RGBA; }

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, intfmt, w, h, 0, fmt, GL_UNSIGNED_BYTE, image);
	//glGenerateMipmap(GL_TEXTURE_2D);

	free(image);

	return texture;
}

/************************************************************************************
 * IqeB_TextureLoadMaterial
 */

static char *FileTextureFileTypesTable[] = {
   (char*)"png", (char*)"tga", (char*)"jpg", (char*)"bmp", (char*)"pcx",
   (char*)"psd", (char*)"gif", (char*)"hdr", (char*)"pic" };
static int   FileTextureFileTypesN = sizeof( FileTextureFileTypesTable) / sizeof( char *);

int IqeB_TextureLoadMaterial( struct part *pMeshPart)
{
	int texture, iFiltType;
	char filename[ MAX_FILENAME_LEN];
	char MaterialNoExtension[ MAX_FILENAME_LEN];
	char *pMaterialName, *p, *basedir;

	// unload material from before

  if( pMeshPart->material > 0) {       // openGL has loaded

    unsigned int TempMaterial;

    TempMaterial = pMeshPart->material;

    glDeleteTextures( 1, &TempMaterial);
  }

	// prepareations

	pMaterialName = pMeshPart->pMaterialStr;
	basedir       = pMeshPart->pMaterialBasedir;

	if( pMeshPart->pMaterialLoadedFile) {     // have this set

    free( pMeshPart->pMaterialLoadedFile);  // free data
    pMeshPart->pMaterialLoadedFile = NULL;  // mark free
	}

	// If we have a drive at begin of the path,
	// remove the complete path

	if( pMaterialName[ 0] != '\0' && pMaterialName[ 1] == ':') { // second char is ':'

    // point after drive

    pMaterialName += 2;  // point after ':'

    // and remove path

	  p = strrchr( pMaterialName, '/');  // find last path character
	  if( p == NULL) p = strrchr( pMaterialName, '\\');  // find last path character
    if( p != NULL) {
      pMaterialName = p;
    }
	}

	// Get the material string with no extension

	strcpy( MaterialNoExtension, pMaterialName);

  p = strrchr( MaterialNoExtension, '/');  // find last path character
  if( p == NULL) p = strrchr( MaterialNoExtension, '\\');  // find last path character
  if( p != NULL) {                         // Got a path delimiter
    p = p + 1;                             // Point after path delimter
  } else {                                 // No path delimiter
    p = MaterialNoExtension;               // .. point to begin of string
  }

	p = strrchr( p, '.');                    // find last point
	if( p != NULL) {                         // got a last point
    *p = '\0';                             // set end of string
	}

	// test this directory
  for( iFiltType = 0; iFiltType < FileTextureFileTypesN; iFiltType++) {

	  sprintf(filename, "%s/%s.%s", basedir, MaterialNoExtension, FileTextureFileTypesTable[ iFiltType]);
	  texture = IqeB_TextureLoadToOpenGL(filename);
	  if( texture) {
      goto ExitPoint;
	  }
  }

	// test textures subdirectory
  for( iFiltType = 0; iFiltType < FileTextureFileTypesN; iFiltType++) {

		sprintf(filename, "%s/textures/%s.%s", basedir, MaterialNoExtension, FileTextureFileTypesTable[ iFiltType]);
	  texture = IqeB_TextureLoadToOpenGL(filename);
	  if( texture) {
      goto ExitPoint;
	  }
  }

  // does we have a file path in the ...

  p = strrchr( MaterialNoExtension, '/');  // find last path character
  if( p == NULL) p = strrchr( MaterialNoExtension, '\\');  // find last path character
	if( p != NULL) {                         // got a last path

    strcpy( MaterialNoExtension, p + 1);   // copy down without file path

  	// test this directory
    for( iFiltType = 0; iFiltType < FileTextureFileTypesN; iFiltType++) {

	    sprintf(filename, "%s/%s.%s", basedir, MaterialNoExtension, FileTextureFileTypesTable[ iFiltType]);
	    texture = IqeB_TextureLoadToOpenGL(filename);
	    if( texture) {
        goto ExitPoint;
	    }
    }

    // test textures subdirectory
    for( iFiltType = 0; iFiltType < FileTextureFileTypesN; iFiltType++) {

      sprintf(filename, "%s/textures/%s.%s", basedir, MaterialNoExtension, FileTextureFileTypesTable[ iFiltType]);
      texture = IqeB_TextureLoadToOpenGL(filename);
      if( texture) {
        goto ExitPoint;
      }
    }
	}

	// test 'skin' texture in this directory
  for( iFiltType = 0; iFiltType < FileTextureFileTypesN; iFiltType++) {

		sprintf(filename, "%s/skin.%s", basedir, FileTextureFileTypesTable[ iFiltType]);
	  texture = IqeB_TextureLoadToOpenGL(filename);
	  if( texture) {
      goto ExitPoint;
	  }
  }

ExitPoint:

	if( texture) {                        // got texture
		if( pMeshPart->pMaterialTags != NULL &&
        strstr( pMeshPart->pMaterialTags, "clamp")) {
			glBindTexture(GL_TEXTURE_2D, texture);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		}

		// save file loaded

    IqeB_FileNormalizePathChars( filename); // Normalize path slasches

		pMeshPart->pMaterialLoadedFile = strdup( filename);

	} else {

		texture = checker_texture;
	}

	pMeshPart->material = texture;    // set ID for loaded texture

	return( texture);
}

/************************************************************************************
 * IqeB_TextureCopyLoadedImageFiles
 *
 * Copies all loaded images (materials) of a model to a destination directory.
 *
 * return:    0  OK
 *          < 0  Error
 */

static int IqeB_TextureCopyFile( char *pSrcFile, char *pDestDir)
{
  char DstFileName[ MAX_FILENAME_LEN];
  char TempBuffer[ 4096];
  char *p1, *p2;
  int slen, nRead;
  FILE *pFileSrc = NULL, *pFileDst = NULL;

  // prepare destination directory
  memset( DstFileName, 0, sizeof( DstFileName));
  strncpy( DstFileName, pDestDir, sizeof( DstFileName) - 256);

  // enshure trailing path character
  slen = strlen( DstFileName);
  if( slen > 0) {
    if( DstFileName[ slen - 1] != '/' && DstFileName[ slen - 1] != '\\') {

      DstFileName[ slen] = '/';
      DstFileName[ slen + 1] = '\0';
    }
  }

  // file basename of src file
  p1 = strrchr( pSrcFile, '/');
  p2 = strrchr( pSrcFile, '\\');
  if( p1 != NULL && p2 != NULL)  {  // have both
    if( p1 > p2) {                  // more at string end
      p1 = p1 + 1;                  // point after path character
    } else {
      p1 = p2 + 1;                  // point after path character
    }
  } else if( p1 != NULL) {
     p1 = p1 + 1;                   // point after path character
  } else if( p2 != NULL) {
    p1 = p2 + 1;                    // point after path character
  } else {                          // none
    p1 = pSrcFile;
  }

  if( strlen( DstFileName) && strlen( p1) >= sizeof( DstFileName)) {  // security test size

    return( -2);
  }

  strcat( DstFileName, p1);                  // build destination file name
  IqeB_FileNormalizePathChars( DstFileName); // Normalize path slasches

  // If we copy the file to itself
  // We have nothing to do.
  // --> Return to caller (we can't copy a file to itself).

  if( strcasecmp( DstFileName, pSrcFile) == 0) {   // Src and destination is the same

    return( 0);  // return 'already done'
  }

  // copy the file

  pFileSrc = fopen( pSrcFile, "rb");
  if( pFileSrc == NULL) {  // Error ?

    return( -3);
  }

  pFileDst = fopen( DstFileName, "wb");
  if( pFileDst == NULL) {  // Error ?

    fclose( pFileSrc);
    return( -3);
  }

  for( ; ; ) {

    nRead = fread( TempBuffer, 1, sizeof( TempBuffer), pFileSrc);

    if( nRead > 0) {                            // read some data

      fwrite( TempBuffer, 1, nRead, pFileDst);   // write do destination

    } else {

      // reached end of file

      break;
    }
  }

  fclose( pFileSrc);
  fclose( pFileDst);

  return( 0);   // return OK
}

int IqeB_TextureCopyLoadedImageFiles( IQE_model *pModel, char *pDestDir)
{
  struct mesh *mesh;
  int i, RetVal, RetVal2;


  if( pModel == NULL || pModel->mesh == NULL) {   // securtiy test's

    return( -1);
  }

  RetVal = 0;     // preset return is OK

	mesh = pModel->mesh;

  for( i = 0; i <  mesh->part_count; i++) {

    if( mesh->part[ i].pMaterialLoadedFile != NULL)  {  // have a file loaded from here

      RetVal2 = IqeB_TextureCopyFile( mesh->part[ i].pMaterialLoadedFile, pDestDir);

      if( RetVal == 0 && RetVal2 != 0) { // catch first error
        RetVal = RetVal2;
      }
    }
  }

  return( RetVal);
}

/****************************** End Of File ******************************/
