![](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2012/Dec/26/argentum-online-c-logo-1554735323-0_avatar.png?raw=true "Argentum Online C")

# __Argentum Online C__
##### Copyright (C) 2009 - 2014
##### Vicente Eduardo Ferrer García - Parra Studios (<vic798@gmail.com>)


- - -


##### BRIEF

A cross-platform mmorpg which keeps the original essence of Argentum Online as combats, magics, guilds, although it has new implementations as 3D graphics engine, new gameplay, and a better performance, among others.


- - -


##### LICENSES


![](http://www.gnu.org/graphics/agplv3-155x51.png "GNU Affero General Public License 3.0")

Copyright (C) 2009 - 2014

Vicente Eduardo Ferrer García - Parra Studios (<vic798@gmail.com>)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>


![](http://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/ASF-logo.svg/160px-ASF-logo.svg.png  "Apache License 2.0")

Copyright (C) 2009 - 2014 Parra Studios

Vicente Eduardo Ferrer García (<vic798@gmail.com>)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


![](http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png "Creative Commons License")

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-nc-sa/3.0/ "License")


- - -



__ACKNOWLEDGMENTS__

Gulfas Morgolok (Pablo Ignacio Marquez <morgolock@speedy.com.ar>) for creating Argentum Online.

- - -

__GIT RULES__

This repository use [GITFLOW](http://danielkummer.github.io/git-flow-cheatsheet/) and
underscore naming convention for branchs

- - -
