/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

#ifndef GAME_USER_H
#define GAME_USER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>				///< Type includes

#include <System/Thread.h>		///< System includes
#include <System/Mutex.h>

#include <Game/Account.h>		///< Game includes

#include <Network/Protocol.h>	///< Network includes

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct UserData
{
	uint32					Id;						///< User id
	Thread					ThreadData;				///< User thread
	Mutex					MutexData;				///< User mutex
	struct ConnectionData	Connection;				///< User connection data
	struct AccountData		Account;				///< Account associated to the user
	struct CharacterData *	Character;				///< Character associated to the user

	struct UserData *		Next;					///< Next user data
	struct UserData *		Previous;				///< Previous user data
};

struct UserListData
{
	struct UserData *		First;					///< Fisrt user on the list
	struct UserData *		Last;					///< Last user on the list
	uint32					Count;					///< Number of users
	Mutex					MutexData;				///< Users mutex
};

////////////////////////////////////////////////////////////
/// Create a user
////////////////////////////////////////////////////////////
struct UserData * UserCreate();

////////////////////////////////////////////////////////////
/// Get character from user
////////////////////////////////////////////////////////////
void UserParseData(void * User);

////////////////////////////////////////////////////////////
/// User protocol parser
////////////////////////////////////////////////////////////
struct CharacterData * UserGetCharacter(struct UserData * User);

////////////////////////////////////////////////////////////
/// Initialize users data
////////////////////////////////////////////////////////////
bool UsersInitialize();

////////////////////////////////////////////////////////////
/// Destroy all users
////////////////////////////////////////////////////////////
bool UsersDestroy();

////////////////////////////////////////////////////////////
/// Clear all users
////////////////////////////////////////////////////////////
void UsersClear();

////////////////////////////////////////////////////////////
/// Add user in the list
////////////////////////////////////////////////////////////
void UsersAdd(struct UserData * User);

////////////////////////////////////////////////////////////
/// Remove user from the list
////////////////////////////////////////////////////////////
void UsersRemove(struct UserData * User);

////////////////////////////////////////////////////////////
/// Check if the user is on the list
////////////////////////////////////////////////////////////
bool UsersContain(struct UserData * User);

////////////////////////////////////////////////////////////
/// Wait the thread until the user isn't removed
////////////////////////////////////////////////////////////
void UsersWaitUntilRemoved(struct UserData * User);

////////////////////////////////////////////////////////////
/// Broadcast a message to all users
///		@Packet: First byte to packet structure reinterpreted
///	
///	todo: this has to be implemented in protocol
////////////////////////////////////////////////////////////
void UsersBroadcastMessage(struct PacketHeader * Packet);

////////////////////////////////////////////////////////////
/// Move the character of user
////////////////////////////////////////////////////////////
bool UserCharacterMove(struct UserData * User, uint8 Key, bool Pressed);

#endif // GAME_USER_H
