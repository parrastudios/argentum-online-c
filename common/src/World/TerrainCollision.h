/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_TERRAIN_COLLISION_H
#define WORLD_TERRAIN_COLLISION_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Component.h>

#include <Math/Geometry/Vector.h>
#include <Math/Geometry/Bounding/Box.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define TERRAIN_COLLISION_EPSILON		0.03125f	//< Epsilon for terrain collision detection
#define TERRAIN_COLLISION_USE_Z_AXIS	1			//< Use z axis only for closest approach

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct TerrainComponentCollisionData
{
	void * TODO;
};

struct TerrainComponentCollisionType
{
    // ...

	void * TODO;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the terrain collision implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentCollisionType * TerrainComponentCollisionAttr();

////////////////////////////////////////////////////////////
/// Return the terrain collision implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * TerrainComponentCollisionImpl();

////////////////////////////////////////////////////////////
/// Return the terrain collision derived data size
////////////////////////////////////////////////////////////
uinteger TerrainComponentCollisionSize();

#endif // WORLD_TERRAIN_COLLISION_H
