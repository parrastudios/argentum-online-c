/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DEBUG_H
#define DEBUG_H

////////////////////////////////////////////////////////////
/// Define a portable debug macro
////////////////////////////////////////////////////////////
#define DEBUG_MODE		0x00
#define RELEASE_MODE	0x01

#if defined(_DEBUG) || defined(DEBUG) || !defined(NDEBUG) || defined(__DEBUG) || defined(__DEBUG__)
#	define COMPILE_TYPE DEBUG_MODE
#else
#	define COMPILE_TYPE RELEASE_MODE
#endif

////////////////////////////////////////////////////////////
/// Define a custom assert macro
////////////////////////////////////////////////////////////
#if COMPILE_TYPE == DEBUG_MODE

	// Compile time assert
#	ifndef cassert

#		define cassert_paste(a, b) a##b

#		define cassert_line(predicate, line, file) \
			typedef char cassert_paste(assertion_failed_##file##_, line)[2 * !!(predicate) - 1]

#		define cassert_file(predicate, file) assert_line(predicate, __LINE__, file)

#		ifdef __FILE__
#			define cassert(predicate) cassert_line(predicate, __LINE__, __FILE__)
#		else
#			define cassert(predicate) cassert_line(predicate, __LINE__, __func__)
#		endif
#	endif

	// Run time assert
#	ifndef assert
#		define assert(predicate) // todo
#	endif

#else

#	ifndef cassert
#		define cassert(predicate)
#	endif

#	ifndef assert
#		define assert(predicate)
#	endif
#endif

#endif // DEBUG_H
