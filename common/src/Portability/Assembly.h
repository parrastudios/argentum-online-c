/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PORTABILITY_ASSEMBLY_H
#define PORTABILITY_ASSEMBLY_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Architecture.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define ASM_SYNTAX_UNKNOWN			0x00
#define ASM_SYNTAX_INTEL			0x01
#define ASM_SYNTAX_ATANDT			0x02

////////////////////////////////////////////////////////////
// Detection
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Identify supported assembly language
////////////////////////////////////////////////////////////
#if (ARCH_TYPE == ARCH_IA32) || (ARCH_TYPE == ARCH_AMD64)
#	if (COMPILER_TYPE == COMPILER_GNUC)
		// GCC assembler
#	elif (COMPILER_TYPE == COMPILER_SUNPRO)
		// GCC-compatible assembler
#	endif
#elif (COMPILER_TYPE == COMPILER_GNUC) && (COMPILER_VERSION >= CompilerVersionBuild(4, 0, 1))
	// GCC 4.0.1 intrinsic
#else
#	define ASM_SYNTAX_TYPE ASM_SYNTAX_UNKNOWN
#endif

#endif // PORTABILITY_ASSEMBLY_H
