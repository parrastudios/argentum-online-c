/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/IOHelper.h>
#include <System/Win32/vsscanf.win.c>

////////////////////////////////////////////////////////////
// String print helper method
////////////////////////////////////////////////////////////
void IOHelperStrPrint(char * str, const char * format, ...)
{
	// Argument list
	va_list ArgList;

	// Initialize the argument list
	va_start(ArgList, format);

	// sprintf arguments on string
	vsprintf_s(str, _vscprintf( format, ArgList ) + 1, format, ArgList);

	// Destroy argument list
	va_end(ArgList);
}

////////////////////////////////////////////////////////////
// String copy helper method
////////////////////////////////////////////////////////////
void IOHelperStrCpy(char * destination, const char * source)
{
    if (destination)
		strcpy_s(destination, strlen(source) + 1, source);
}

////////////////////////////////////////////////////////////
// Sscan helper method
////////////////////////////////////////////////////////////
Int32 IOHelperSscan(const char * str, const char * format, ...)
{
	// Argument list
	va_list ArgList;
	Int32	Count;

	// Initialize the argument list
	va_start(ArgList, format);

	// Scan
	Count = vsscanf(str, format, ArgList);

	// Destroy argument list
	va_end(ArgList);

	return Count;
}

////////////////////////////////////////////////////////////
// Open helper method
////////////////////////////////////////////////////////////
bool IOHelperOpen(FILE** pFile, const char * filename, const char * mode)
{
	fopen_s(pFile, filename, mode);

	if (!*pFile)
		return false;

	return true;
}

////////////////////////////////////////////////////////////
// Read field helper method
////////////////////////////////////////////////////////////
void IOHelperReadField(char * Destination, char * Source, char Separator, UInt32 Position)
{
	UInt32 Lenght, Current, Last = 0, CurPos = 0;

	Lenght = strlen(Source);

	for (Current = 0; Current < Lenght; Current++)
	{
		if (Source[Current] == Separator)
		{
			CurPos++;

			if (CurPos == Position)
				Last = Current;

			if (CurPos == (Position + 1))
			{
				if (Last)
					Last++;

				memcpy(&Destination[0], &Source[Last], (Current - Last));
				return;
			}

			Current++;
		}
	}
}
