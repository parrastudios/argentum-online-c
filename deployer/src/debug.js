/**
 * Include files
 */
var fs 	= require('fs');			///< file system module

/**
 *	Debug
 *
 * @author Vicente Ferrer
 * @lastmodify 120214 by Vicente
 * @brief A module gives suport for debugging.
 */

/**
 * Public
 */

/**
 * @brief Ennumeration of verbose modes.
 */ 
var verbose_mode = {
	stdout 	: 0,
	file 	: 1,
	remote 	: 2
};

/**
 * Private
 */
var debug_verbose_mode = verbose_mode.stdout;	///< Current verbose mode

/**
 * @brief Set the current verbose mode.
 * @return {void}
 */
function set_verbose_mode(value) {
	debug_verbose_mode = value;
}

/**
 * @brief Log a message into specified output.
 * @return {void}
 */
function log_array_message(args) {
	var result = '';
		
	// format arguments
	for (var i = 0; i < args.length; ++i) {
		result += ((typeof args[i] != 'undefined') ? args[i].toString() : 'undefined') + ' ';
	}

	// check verbose mode
	switch (debug_verbose_mode) {
		case verbose_mode.stdout : {
			// print formatted result to console
			console.log(result);
			break;
		}

		case verbose_mode.file : {

			break;
		}

		case verbose_mode.remote : {

			break;
		}
	}
}

/**
 * @brief Log a message (closure).
 * @return {void}
 */
function log_message() {
	// splice arguments
	var args = Array.apply(null, arguments);

	// log arguments converted to an array		
	log_array_message(args);
}

/**
 * @brief Process uncaught exception handler.
 * @return {void}
 */
process.on('uncaughtException', function(error) {
	log_message('caught exception:', error);
});

/**
 * @brief Process exit handler.
 * @return {void}
 */
process.on('exit', function() {
	log_message('about to exit');
});

/**
 * Public
 */
exports.verbose 		= verbose_mode;
exports.verbose_mode	= set_verbose_mode;
exports.log_array		= log_array_message;
exports.log 			= log_message;
