/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/BitSet.h>
#include <Memory/General.h>

struct BitSetType * BitSetCreate(uint32 BitsCount)
{
	struct BitSetType * BitSet = (struct BitSetType*)MemoryAllocate(sizeof(struct BitSetType));

	if (BitSet)
	{
		BitSet->BitCount = 0;
		BitSet->ByteCount = 0;
		BitSet->Bits = NULL;

		if (BitSetInitialize(BitSet, BitsCount))
		{
			return BitSet;
		}
		else
		{
			MemoryDeallocate(BitSet);
		}
	}

	return NULL;
}

void BitSetDestroy(struct BitSetType * BitSet)
{
	if (BitSet)
	{
		MemoryDeallocate(BitSet->Bits);
		MemoryDeallocate(BitSet);
	}
}

bool BitSetInitialize(struct BitSetType * BitSet, uint32 BitsCount)
{
	if (BitSet)
	{
		if (BitSet->Bits)
		{
			MemoryDeallocate(BitSet->Bits);
		}

		BitSet->BitCount = BitsCount;
		BitSet->ByteCount = (BitsCount >> 3) + 1;
		BitSet->Bits = (uint8*)MemoryAllocate(sizeof(uint8) * BitSet->ByteCount);

		if (BitSet->Bits)
		{
			BitSetReset(BitSet);
			return true;
		}
	}

	return false;
}

void BitSetReset(struct BitSetType * BitSet)
{
	uint32 i;

	for (i = 0; i < BitSet->ByteCount; i++)
	{
		BitSet->Bits[i] = 0x00;
	}
}

bool BitSetIsNull(struct BitSetType * BitSet)
{
	uint32 i;

	for (i = 0; i < BitSet->ByteCount; i++)
	{
		if (BitSet->Bits[i] != 0x00)
		{
			return false;
		}
	}

	return true;
}

void BitSetClear(struct BitSetType * BitSet, uint32 BitIndex)
{
	if (BitIndex < BitSet->BitCount)
		BitSet->Bits[BitIndex >> 3] &= ~(1 << (BitIndex & 7));
}

bool BitSetIsSet(struct BitSetType * BitSet, uint32 BitIndex)
{
	if (BitIndex < BitSet->BitCount && (BitSet->Bits[BitIndex >> 3] & (1 << (BitIndex & 7))) != 0x00)
		return true;

	return false;
}

void BitSetAdd(struct BitSetType * BitSet, uint32 BitIndex)
{
	if (BitIndex < BitSet->BitCount)
		BitSet->Bits[BitIndex >> 3] |= 1 << (BitIndex & 7);
}

void BitSetAnd(struct BitSetType * Result, struct BitSetType * Left, struct BitSetType * Right)
{
	uint32 i;

	if (Right->BitCount != Left->BitCount)
	{
		return;
	}

	if (Result->BitCount != Left->BitCount)
	{
		BitSetInitialize(Result, Left->BitCount);
	}
	else
	{
		BitSetReset(Result);
	}

	for (i = 0; i < Left->ByteCount; i++)
	{
		Result->Bits[i] = Left->Bits[i] & Right->Bits[i];
	}
}

void BitSetOr(struct BitSetType * Result, struct BitSetType * Left, struct BitSetType * Right)
{
	uint32 i;

	if (Right->BitCount != Left->BitCount)
	{
		return;
	}

	if (Result->BitCount != Left->BitCount)
	{
		BitSetInitialize(Result, Left->BitCount);
	}
	else
	{
		BitSetReset(Result);
	}

	for (i = 0; i < Left->ByteCount; i++)
	{
		Result->Bits[i] = Left->Bits[i] | Right->Bits[i];
	}
}

void BitSetXor(struct BitSetType * Result, struct BitSetType * Left, struct BitSetType * Right)
{
	uint32 i;

	if (Right->BitCount != Left->BitCount)
	{
		return;
	}

	if (Result->BitCount != Left->BitCount)
	{
		BitSetInitialize(Result, Left->BitCount);
	}
	else
	{
		BitSetReset(Result);
	}

	for (i = 0; i < Left->ByteCount; i++)
	{
		Result->Bits[i] = Left->Bits[i] ^ Right->Bits[i];
	}
}
