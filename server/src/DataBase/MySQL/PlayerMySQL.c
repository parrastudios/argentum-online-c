/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/Player.h>

////////////////////////////////////////////////////////////
/// Load all players from database
////////////////////////////////////////////////////////////
bool DBPlayerLoadList(Vector DBPlayerList)
{

	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Reload all players from database
////////////////////////////////////////////////////////////
bool DBPlayerReloadList(Vector DBPlayerList)
{

	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Store players to database
////////////////////////////////////////////////////////////
bool DBPlayerStoreList(Vector DBPlayerList)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Load the player from database
////////////////////////////////////////////////////////////
bool DBPlayerLoadById(struct DBCharacterData * Character, uinteger Index)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Load the player from row of database
////////////////////////////////////////////////////////////
bool DBPlayerLoadByRow(QueryRowType Row, struct DBCharacterData * Character)
{
	if (Row)
	{
		QueryGetField(Row, DB_PLAYER_FIELD_ID, QUERY_FIELD_TYPE_UINT32, &Character->Id);
		QueryGetField(Row, DB_PLAYER_FIELD_NAME, QUERY_FIELD_TYPE_STR, Character->Attributes.Player.Name);
		QueryGetField(Row, DB_PLAYER_FIELD_ACCOUNT_ID, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.AccountId);
		QueryGetField(Row, DB_PLAYER_FIELD_ACTIVE, QUERY_FIELD_TYPE_BOOL, &Character->Attributes.Player.Active);
		QueryGetField(Row, DB_PLAYER_FIELD_BAN, QUERY_FIELD_TYPE_BOOL, &Character->Attributes.Player.Ban);

		QueryGetField(Row, DB_PLAYER_FIELD_CLASS, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Class);
		QueryGetField(Row, DB_PLAYER_FIELD_GENDER, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Gender);
		QueryGetField(Row, DB_PLAYER_FIELD_RACE, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Race);

		QueryGetField(Row, DB_PLAYER_FIELD_HOME, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Home);
		QueryGetField(Row, DB_PLAYER_FIELD_ALIGN, QUERY_FIELD_TYPE_UINT32, &Character->Algin);

		QueryGetField(Row, DB_PLAYER_FIELD_HEAD, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Head);
		QueryGetField(Row, DB_PLAYER_FIELD_BODY, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Body);

		QueryGetField(Row, DB_PLAYER_FIELD_STRENGTH, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Strength);
		QueryGetField(Row, DB_PLAYER_FIELD_AGILITY, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Agility);
		QueryGetField(Row, DB_PLAYER_FIELD_INTELIGENCE, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Inteligence);
		QueryGetField(Row, DB_PLAYER_FIELD_CHARISMA, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Charisma);
		QueryGetField(Row, DB_PLAYER_FIELD_CONSTITUTION, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Constitution);

		QueryGetField(Row, DB_PLAYER_FIELD_LEVEL, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Level);
		QueryGetField(Row, DB_PLAYER_FIELD_EXP, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Exp);
		QueryGetField(Row, DB_PLAYER_FIELD_GOLD, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Gold);

		QueryGetField(Row, DB_PLAYER_FIELD_WEAPON, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Weapon);
		QueryGetField(Row, DB_PLAYER_FIELD_SHIELD, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Shield);
		QueryGetField(Row, DB_PLAYER_FIELD_HELMET, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Helmet);
		QueryGetField(Row, DB_PLAYER_FIELD_FOOTWEAR, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.Footwear);

		QueryGetField(Row, DB_PLAYER_FIELD_MAXHP, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MaxHp);
		QueryGetField(Row, DB_PLAYER_FIELD_MINHP, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MinHp);
		QueryGetField(Row, DB_PLAYER_FIELD_MAXSTA, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MaxSta);
		QueryGetField(Row, DB_PLAYER_FIELD_MINSTA, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MinSta);
		QueryGetField(Row, DB_PLAYER_FIELD_MAXMAN, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MaxMan);
		QueryGetField(Row, DB_PLAYER_FIELD_MINMAN, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MinMan);
		QueryGetField(Row, DB_PLAYER_FIELD_MAXHUN, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MaxHun);
		QueryGetField(Row, DB_PLAYER_FIELD_MINHUM, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MinHun);
		QueryGetField(Row, DB_PLAYER_FIELD_MAXTHI, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MaxThi);
		QueryGetField(Row, DB_PLAYER_FIELD_MINTHI, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.MinThi);


		QueryGetField(Row, DB_PLAYER_FIELD_WORLD, QUERY_FIELD_TYPE_UINT32, &Character->Attributes.Player.World);
		QueryGetField(Row, DB_PLAYER_FIELD_POS_X, QUERY_FIELD_TYPE_FLOAT, &Character->Attributes.Player.Position.x);
		QueryGetField(Row, DB_PLAYER_FIELD_POS_Y, QUERY_FIELD_TYPE_FLOAT, &Character->Attributes.Player.Position.y);
		QueryGetField(Row, DB_PLAYER_FIELD_POS_Z, QUERY_FIELD_TYPE_FLOAT, &Character->Attributes.Player.Position.z);

		return true;
	}

	return false;
}

/*
////////////////////////////////////////////////////////////
/// Load the player from database
////////////////////////////////////////////////////////////
bool DBPlayerGetData(struct DBCharacterData * Character, Id)
{
	// todo

	void * Result = Query("SELECT * FROM `%s` WHERE name='%s'", DB_PLAYER_TABLE_DEFAULT, Name);

	if (Result)
	{
		void * Row = QueryGetRow(Result);

		if (Row)
		{
			// Get player data
			QueryGetField(Row, , , &Account->Active);

			// Release query
			QueryRelease(Result);

			return true;
		}
		else
		{
			// Release query
			QueryRelease(Result);

			return false;
		}

	}

	return false;
}
*/

////////////////////////////////////////////////////////////
/// Disconnect the player (store it in database)
////////////////////////////////////////////////////////////
bool DBPlayerStore(struct DBCharacterData * Character)
{
	// todo

	return true;
}

////////////////////////////////////////////////////////////
/// Set activate flag in database
////////////////////////////////////////////////////////////
void DBPlayerActive(struct DBCharacterData * Character)
{
	struct DBPlayerData * DBPlayer = &Character->Attributes.Player;

	QueryResultType Result;

	// Make the query to database
	if (Query(&Result, "UPDATE `%s` SET active='%d' WHERE name='%s'", DB_PLAYER_TABLE_DEFAULT, DBPlayer->Active, DBPlayer->Name))
	{
		// Release query
		QueryRelease(Result);
	}
}

////////////////////////////////////////////////////////////
/// Check if a player exists in the database
////////////////////////////////////////////////////////////
bool DBPlayerExist(struct DBCharacterData * Character)
{
	struct DBPlayerData * DBPlayer = &Character->Attributes.Player;

	QueryResultType Result;

	if (Query(&Result, "SELECT id FROM `%s` WHERE name='%s'", DB_PLAYER_TABLE_DEFAULT, DBPlayer->Name))
	{
		// Release the query
		QueryRelease(Result);

		return true;
	}

	return false;
}

bool DBPlayerCreate(struct DBCharacterData * Character)
{
	// todo: creates a new player and saves de values from input player (User)

	return true;
}

bool DBPlayerDelete(struct DBCharacterData * Character)
{
	// todo: removes a player from database

	return true;
}

