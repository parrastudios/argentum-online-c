/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/StoredProcedure.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
/// Get stored procedure prototype
/////////////////////////////////////////////////////////////
StoredProcedureArgumentList StoredProcedurePrototype(struct StoredProcedureType * StoredProcedure)
{
	return StoredProcedure->Prototype;
}

/////////////////////////////////////////////////////////////
/// Clear stored procedure result set
/////////////////////////////////////////////////////////////
void StoredProcedureClear(struct StoredProcedureType * StoredProcedure)
{
	if (StoredProcedure->Result)
	{
		if (VectorSize(StoredProcedure->Result) > UINTEGER_SUFFIX(0))
		{
			uinteger Result;

			for (Result = 0; Result < VectorSize(StoredProcedure->Result); ++Result)
			{
				StoredProcedureArgumentList Arguments = VectorAtT(StoredProcedure->Result, Result, StoredProcedureArgumentList);

				// Destroy argument list set
				StoredProcedureArgumentDestroy(Arguments);
			}

			// Clear vector
			VectorClear(StoredProcedure->Result);
		}
	}
}

/////////////////////////////////////////////////////////////
/// Execute stored procedure fetch callback
/////////////////////////////////////////////////////////////
void StoredProcedureFetch(struct StoredProcedureType * StoredProcedure)
{
	if (StoredProcedure && StoredProcedure->Fetch)
	{
		uinteger Size = VectorSize(StoredProcedure->Result);

		if (Size > UINTEGER_SUFFIX(0))
		{
			// Create builder list
			struct StoredProcedureArgumentBuilder * BuilderList = (struct StoredProcedureArgumentBuilder *)MemoryAllocate(sizeof(struct StoredProcedureArgumentBuilder) * Size);

			if (BuilderList)
			{
				uinteger Result;

				// Initialize builder list
				for (Result = 0; Result < VectorSize(StoredProcedure->Result); ++Result)
				{
					StoredProcedureArgumentList Arguments = VectorAtT(StoredProcedure->Result, Result, StoredProcedureArgumentList);

					// Create builder instance
					StoredProcedureArgumentBuildCreate(&BuilderList[Result], Arguments);
				}

				// Execute fetch callback
				StoredProcedure->Fetch(BuilderList, Size);

				// Destroy builder list
				MemoryDeallocate(BuilderList);
			}
		}
		else
		{
			// Execute fetch callback
			StoredProcedure->Fetch(NULL, UINTEGER_SUFFIX(0));
		}
	}
}
