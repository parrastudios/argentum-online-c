/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/Function.h>
#include <MetaData/Parameter.h>
#include <MetaData/Reference.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef struct MetadataFunctionType
{
	MetadataStorageIndexType	Index;		///< Reference to itself
	MetadataParameterListType	Signature;	///< Signature of function
	MetadataReferenceCountType	RefCount;	///< References to function
	MetadataFunctionPtr			FuncPtr;	///< Function stub pointer

};


////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a serial function
////////////////////////////////////////////////////////////
MetadataFunctionType MetadataFunctionCreate(MetadataScopeType Scope, MetadataParameterListType Signature)
{
	if (Signature != METADATA_STORAGE_INDEX_INVALID)
	{
		// Allocate memory for function
		MetadataFunctionType Function = MetadataStorageAllocateFunction(MetadataScopeGetStorage(Scope), sizeof(struct MetadataFunctionType));

		if (Function != METADATA_STORAGE_INDEX_INVALID)
		{
			MetadataFunctionTypeImpl FunctionImpl = MetadataStorageGetFunction(MetadataScopeGetStorage(Scope), Function);

			if (FunctionImpl)
			{
				// Set properties
				FunctionImpl->Index = Function;
				FunctionImpl->RefCount = 0;
				FunctionImpl->Signature = Signature;
				FunctionImpl->FuncPtr = NULL;
			}
			else
			{
				MetadataStorageDeallocateFunction(MetadataScopeGetStorage(Scope), Function);
			}
		}

		return Function;
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Bind a function
////////////////////////////////////////////////////////////
MetadataFunctionType MetadataFunctionBind(MetadataScopeType Scope, MetadataParameterListType Signature, MetadataFunctionPtr FunctionPtr)
{
	if (Signature != METADATA_STORAGE_INDEX_INVALID)
	{
		// Allocate memory for function
		MetadataFunctionType Function = MetadataStorageAllocateFunction(MetadataScopeGetStorage(Scope), sizeof(struct MetadataFunctionType));

		if (Function != METADATA_STORAGE_INDEX_INVALID)
		{
			MetadataFunctionTypeImpl FunctionImpl = MetadataStorageGetFunction(MetadataScopeGetStorage(Scope), Function);

			if (FunctionImpl)
			{
				// Set properties
				FunctionImpl->Index = Function;
				FunctionImpl->RefCount = 0;
				FunctionImpl->Signature = Signature;
				FunctionImpl->FuncPtr = FunctionPtr;
			}
			else
			{
				MetadataStorageDeallocateFunction(MetadataScopeGetStorage(Scope), Function);
			}
		}

		return Function;
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Copy a function
////////////////////////////////////////////////////////////
MetadataFunctionType MetadataFunctionCopy(MetadataScopeType Scope, MetadataFunctionType Function)
{
	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Get signature of a function
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataFunctionGetSignature(MetadataScopeType Scope, MetadataFunctionType Function)
{
	MetadataFunctionTypeImpl FunctionImpl = MetadataStorageGetFunction(MetadataScopeGetStorage(Scope), Function);

	if (FunctionImpl)
	{
		return FunctionImpl->Signature;
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Execute a function dynamically
////////////////////////////////////////////////////////////
void MetadataFunctionExecute(MetadataScopeType Scope, MetadataFunctionType Function, MetadataParameterListType ParameterList)
{
	MetadataFunctionTypeImpl FunctionImpl = MetadataStorageGetFunction(MetadataScopeGetStorage(Scope), Function);

	if (FunctionImpl)
	{
		if (FunctionImpl->FuncPtr)
		{
			FunctionImpl->FuncPtr(ParameterList);
		}
	}
}

////////////////////////////////////////////////////////////
/// Reference a function
////////////////////////////////////////////////////////////
void MetadataFunctionReference(MetadataScopeType Scope, MetadataFunctionType Function)
{
	MetadataFunctionTypeImpl FunctionImpl = MetadataStorageGetFunction(MetadataScopeGetStorage(Scope), Function);

	if (FunctionImpl)
	{
		++FunctionImpl->RefCount;
	}
}

////////////////////////////////////////////////////////////
/// Check if function is referenced
////////////////////////////////////////////////////////////
bool MetadataFunctionIsReferenced(MetadataScopeType Scope, MetadataFunctionType Function)
{
	MetadataFunctionTypeImpl FunctionImpl = MetadataStorageGetFunction(MetadataScopeGetStorage(Scope), Function);

	if (FunctionImpl)
	{
		return (FunctionImpl->RefCount > 0);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Dereference a function
////////////////////////////////////////////////////////////
void MetadataFunctionDereference(MetadataScopeType Scope, MetadataFunctionType Function)
{
	MetadataFunctionTypeImpl FunctionImpl = MetadataStorageGetFunction(MetadataScopeGetStorage(Scope), Function);

	if (FunctionImpl)
	{
		--FunctionImpl->RefCount;
	}
}

////////////////////////////////////////////////////////////
/// Destroy a function
////////////////////////////////////////////////////////////
void MetadataFunctionDestroy(MetadataScopeType Scope, MetadataFunctionType Function)
{
	MetadataFunctionTypeImpl FunctionImpl = MetadataStorageGetFunction(MetadataScopeGetStorage(Scope), Function);

	if (FunctionImpl)
	{
		// Set properties
		FunctionImpl->Index = METADATA_STORAGE_INDEX_INVALID;
		FunctionImpl->RefCount = 0;
		FunctionImpl->Signature = METADATA_STORAGE_INDEX_INVALID;
	}

	MetadataStorageDeallocateFunction(MetadataScopeGetStorage(Scope), Function);
}
