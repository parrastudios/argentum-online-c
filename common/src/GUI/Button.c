/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/IOHelper.h>
#include <Graphics/Font.h>
#include <Graphics/TextureRender.h>

#include <GUI/Widget.h>

////////////////////////////////////////////////////////////
// Create a button
////////////////////////////////////////////////////////////
struct GuiWidget * GuiButtonCreate(int32 X, int32 Y, uint32 FontSize, char * Text)
{
	struct GuiWidget * Widget;

	// Create
	Widget = GuiWidgetCreate(GUI_WIDGET_BUTTON, X, Y, FontSize, Text);

	if (Widget)
	{
		// Set the rect
		IndexGetSize(GUI_BUTTON_NORMAL_TEXTURE, &Widget->Width, &Widget->Height);

		// Set the status
		GuiButtonSetState(&Widget->Attributes.Button, GUI_BUTTON_NORMAL);

		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Create a button by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiButtonCreateDefault()
{
	// Create
	struct GuiWidget * Widget = GuiWidgetCreateDefault(GUI_WIDGET_BUTTON);

	if (Widget)
	{
		// Set the rect (get the width and height from the grh)
		IndexGetSize(GUI_BUTTON_NORMAL_TEXTURE, &Widget->Width, &Widget->Height);

		// Set the status
		GuiButtonSetState(&Widget->Attributes.Button, GUI_BUTTON_NORMAL);

		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Set state of the button
////////////////////////////////////////////////////////////
void GuiButtonSetState(struct GuiButton * Button, int32 State)
{
	Button->ActionStatus = State;
}

////////////////////////////////////////////////////////////
// Render a button
////////////////////////////////////////////////////////////
void GuiButtonRender(struct GuiWidget * Form, struct GuiWidget * Widget, struct Color4ub * BackgroundColor, struct Color4ub * FontColor)
{
	struct GuiButton *	Button = &Widget->Attributes.Button;
	int32				Texture;
	int32				TextX, TextY, OffsetY = 0;

	// Render
	if (Button->ActionStatus == GUI_BUTTON_HOVERED)
	{
		Texture = GUI_BUTTON_HOVERED_TEXTURE;
	}
	else if (Button->ActionStatus == GUI_BUTTON_PRESSED)
	{
		OffsetY = 2;
		Texture = GUI_BUTTON_PRESSED_TEXTURE;
	}
	else
	{
		Texture = GUI_BUTTON_NORMAL_TEXTURE;
	}

	TextureRenderGrhColor(Texture,
						  Form->X + Widget->X,
						  Form->Y + Widget->Y,
						  BackgroundColor);

	if (Widget->Text[0] == NULLCHAR)
		return;

	// Calculate center position
	TextX = (Widget->Width / 2) - (Widget->FontSize * (Widget->Length / 2));
	TextY = (Widget->Height / 2) - 4 + OffsetY;

	FontRenderTextColor((float)(TextX + Widget->X + Form->X), (float)(TextY + Widget->Y + Form->Y), Widget->Text, FontColor);
}
