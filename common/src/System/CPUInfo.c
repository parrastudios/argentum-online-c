/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Assembly.h>
#include <Portability/Platform.h>

#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
#   include <pdh.h>
#endif

#include <System/CPUInfo.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
uint32		CPUFeatures		= 0;		///< CPU features list
static bool CPUReadEnabled	= false;

#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
    HQUERY		CPUQueryHandle;
    HCOUNTER	CPUCounterHandle;
#endif

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
///	Obtains a list of features the cpu supports
////////////////////////////////////////////////////////////
uint32 CPUFeaturesInitialize()
{

#   if (ASM_SYNTAX_TYPE == ASM_SYNTAX_INTEL)
	__asm
	{
		mov eax, 1			// Set request type to a feature list from processor
		cpuid				// Send request
		lea ebx, CPUFeatures
		mov [ebx], edx		// Save value
	}

	return CPUFeatures;
#   elif (ASM_SYNTAX_TYPE == ASM_SYNTAX_ATANDT)

#   endif

    return 0;
}

////////////////////////////////////////////////////////////
///	Initialize the features list and the cpu queries
////////////////////////////////////////////////////////////
void CPUInitialize()
{
	//char CounterPath[0xFF] = "\\Processor(0)\\% Processor Time";

	//LPTSTR lpCounterPath = (TCHAR*)MemoryAllocate(sizeof(TCHAR) * strlen(CounterPath));

	//_tcscpy_s(lpCounterPath, strlen(CounterPath), CounterPath);

	CPUFeaturesInitialize();

	// Create a query object to poll cpu usage
	//CPUReadEnabled = (bool)(PdhOpenQuery(NULL, 0, &CPUQueryHandle) == ERROR_SUCCESS);

	// Set query object to poll all cpus in the system
	//CPUReadEnabled = (bool)(PdhAddCounter(CPUQueryHandle, lpCounterPath/*"\\Processor(_Total)\\% processor time"*/, 0, &CPUCounterHandle) == ERROR_SUCCESS);
}

////////////////////////////////////////////////////////////
///	Destroy all structures which we used for get the cpu info
////////////////////////////////////////////////////////////
void CPUDestroy()
{
	if (CPUReadEnabled)
	{
	    #if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
		PdhCloseQuery(CPUQueryHandle);
		#endif
	}
}

////////////////////////////////////////////////////////////
/// Check if the cpu supports a feature
////////////////////////////////////////////////////////////
uint32 CPUFeatureCheck(uint32 Feature)
{
	// Check if it isn't initialized
	if (!CPUFeatures)
		return 0x10000001;

	// Check if the feature is available
	if (CPUFeatures & Feature)
		return Feature;

	return 0;
}

////////////////////////////////////////////////////////////
/// Gets the cpu id
////////////////////////////////////////////////////////////
int8 * CPUGetID()
{
#   if (ASM_SYNTAX_TYPE == ASM_SYNTAX_INTEL)
	static int8 ID[13];

	__asm
	{
		lea ebx, ID
		mov cl, 0

CLEARID:
		inc cl
		mov dword ptr [ebx], 0
		cmp cl, 13
		jb CLEARID
	}

	__asm
	{
		xor eax, eax		// Set request type to the string ID
		cpuid				// Send request

		xchg eax, ebx		// Swap so address register can be used
		lea ebx, ID			// Get destination string offset

		mov [ebx], eax		// Save first four letters of string
		add ebx, 4			// Go up anther four bytes
							// Repeat for next two registers
		mov [ebx], edx
		add ebx, 4

		mov [ebx], ecx
	}

	return ID;
#   elif (ASM_SYNTAX_TYPE == ASM_SYNTAX_ATANDT)

#   endif

    return 0;
}

////////////////////////////////////////////////////////////
///	Return the current usage of the cpu
////////////////////////////////////////////////////////////
uint32 CPUGetUsage()
{
    #if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
	if (CPUReadEnabled)
	{
		PDH_FMT_COUNTERVALUE Value;

		PdhCollectQueryData(CPUQueryHandle);

		PdhGetFormattedCounterValue(CPUCounterHandle, PDH_FMT_LONG, NULL, &Value);

		return (uint32)Value.longValue;
	}
    #endif

	return 0;
}
