/*
***********************************************************************************
 IqeB_Import_FileIQE.cpp

 Import a .iqe file.

14.12.2014 RR: * First editon of this file.

*/

#include "IqeBrowser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

/************************************************************************************
 * IqeB_ImportFileIQE()
 *
 * return:   NULL  error on loading
 *           else  pointer to loaded model data
 */

struct IQE_model *IqeB_ImportFileIQE( char *filename)
{

  char basedir[ 1024];
	FILE *fp;
	char line[ 512];
	char MaterialString[ 1024] = "";
	char MeshName[ 1024] = "";
	int first = 0;
	int fm = 0;
	char *s, *sp, *p;

	//x/fprintf( stderr, "loading iqe model '%s' ...\n", filename);

  IqeB_ImportArraysReset();  // reset temporary arrays used on loading files

  // get base directory (path to filename)

	strcpy(basedir, filename);
	p = strrchr(basedir, '/');
	if (!p) p = strrchr(basedir, '\\');
	if (!p) {
    strcpy(basedir, "");    // empty directory
    p = filename;          // point to begin of file name
  } else {
    *p = 0;          // overwrite slash
    p += 1;          // point to begin of file name
  }

	// try to open file

	fp = fopen( filename, "r");
	if (!fp) {

		//x/fprintf( stderr, "ERROR: cannot open file '%s'\n", filename);

		return( NULL);
	}

	if (!fgets(line, sizeof line, fp)) {

		//x/fprintf(stderr, "cannot load %s: read error\n", filename);

		fclose( fp);
		return( NULL);
	}

	if (memcmp(line, IQE_FILE_MAGIC, strlen( IQE_FILE_MAGIC))) {

		//x/fprintf( stderr, "cannot load %s: bad iqe magic\n", filename);

		fclose( fp);
		return( NULL);
	}

	// allocated an empty model

	struct IQE_model *model = IqeB_ModelCreateEmpty();    // get empty model
	if( model == NULL) {                                   // memory allocation failed

    return( NULL);
	}

  // for less writing, some short cuts

	struct skel *skel = model->skel;
	struct mesh *mesh = model->mesh;
	struct anim *anim = NULL;

	// ...

	int pose_count = 0;
	struct pose *pose;

	skel->joint_count = 0;
	pose = skel->pose;

	// load the data ...

	while (1) {
		float x, y, z, w;
		int a, b, c, d;

		if (!fgets(line, sizeof line, fp))
			break;

		sp = line;

		if( line[ 0] == '#') {   // is comment line
      continue;
		}

		s = parseword(&sp);
		if (s == NULL || *s == '\0' || *s == '#') {  // empty line or comment
			continue;
		}

		// cut off line after first comment character

    p = strchr( sp, '#');     // Begin of comment
    if( p != NULL) {

      *p = '\0';
    }

    // ...

		if (s[0] == 'v' && s[1] != 0 && s[2] == 0) {

			switch (s[1]) {
			case 'p':
				x = parsefloat(&sp, 0);
				y = parsefloat(&sp, 0);
				z = parsefloat(&sp, 0);
				addposition(x, y, z);
				break;

			case 'n':
				x = parsefloat(&sp, 0);
				y = parsefloat(&sp, 0);
				z = parsefloat(&sp, 0);
				addnormal(x, y, z);
				break;

			case 't':
				x = parsefloat(&sp, 0);
				y = parsefloat(&sp, 0);
				addtexcoord(x, y);
				break;

			case 'c':
				x = parsefloat(&sp, 0);
				y = parsefloat(&sp, 0);
				z = parsefloat(&sp, 0);
				w = parsefloat(&sp, 1);
				addcolor(x, y, z, w);
				break;

			case 'b':
				a = parseint(&sp, 0);
				x = parsefloat(&sp, 1);
				b = parseint(&sp, 0);
				y = parsefloat(&sp, 0);
				c = parseint(&sp, 0);
				z = parsefloat(&sp, 0);
				d = parseint(&sp, 0);
				w = parsefloat(&sp, 0);
				addblend(a, b, c, d, x, y, z, w);
				break;
			}
		}

		else if (s[0] == 'f' && s[1] == 'm' && s[2] == 0) {
			a = parseint(&sp, 0);
			b = parseint(&sp, 0);
			c = parseint(&sp, -1);
			while (c > -1) {
				addtriangleFlipWinding(a+fm, b+fm, c+fm);
				b = c;
				c = parseint(&sp, -1);
			}
		}

		else if (s[0] == 'p' && s[1] == 'q' && s[2] == 0) {
			if (pose_count < MAXBONE) {
				pose[pose_count].location[0] = parsefloat(&sp, 0);
				pose[pose_count].location[1] = parsefloat(&sp, 0);
				pose[pose_count].location[2] = parsefloat(&sp, 0);
				pose[pose_count].rotation[0] = parsefloat(&sp, 0);
				pose[pose_count].rotation[1] = parsefloat(&sp, 0);
				pose[pose_count].rotation[2] = parsefloat(&sp, 0);
				pose[pose_count].rotation[3] = parsefloat(&sp, 1);
				pose[pose_count].scale[0] = parsefloat(&sp, 1);
				pose[pose_count].scale[1] = parsefloat(&sp, 1);
				pose[pose_count].scale[2] = parsefloat(&sp, 1);
				pose_count++;
			}
		}

		else if (s[0] == 'p' && s[1] == 'a' && s[2] == 0) {  // translation, rotations in radians, optional scale
			if (pose_count < MAXBONE) {

        vec3 rot;

				pose[pose_count].location[0] = parsefloat(&sp, 0);
				pose[pose_count].location[1] = parsefloat(&sp, 0);
				pose[pose_count].location[2] = parsefloat(&sp, 0);

				rot[0] = parsefloat(&sp, 0);
				rot[1] = parsefloat(&sp, 0);
				rot[2] = parsefloat(&sp, 0);

				pose[pose_count].scale[0] = parsefloat(&sp, 1);
				pose[pose_count].scale[1] = parsefloat(&sp, 1);
				pose[pose_count].scale[2] = parsefloat(&sp, 1);

				// Convert rotation to quaterion

        if( rot[ 0] == 0.0 && rot[ 1] == 0.0 && rot[ 2] == 0.0) {  // No rotation

          quat_set_identity( pose[pose_count].rotation); // set default rotation

        } else {

          double cx = cos(rot[0]/2), sx = sin(rot[0]/2),
                 cy = cos(rot[1]/2), sy = sin(rot[1]/2),
                 cz = cos(rot[2]/2), sz = sin(rot[2]/2);

          pose[pose_count].rotation[0] = sx*cy*cz - cx*sy*sz;
          pose[pose_count].rotation[1] = cx*sy*cz + sx*cy*sz;
          pose[pose_count].rotation[2] = cx*cy*sz - sx*sy*cz;
          pose[pose_count].rotation[3] = cx*cy*cz + sx*sy*sz;

          if( pose[pose_count].rotation[3] > 0) {     // flip all components

    				pose[pose_count].rotation[0] *= -1.0;
				    pose[pose_count].rotation[1] *= -1.0;
				    pose[pose_count].rotation[2] *= -1.0;
				    pose[pose_count].rotation[3] *= -1.0;
          }
        }

				pose_count++;
			}
		}

		else if (s[0] == 'p' && s[1] == 'd' && s[2] == 0) {  // translation, rotations in degrees, optional scale
			if (pose_count < MAXBONE) {

        vec3 rot;

				pose[pose_count].location[0] = parsefloat(&sp, 0);
				pose[pose_count].location[1] = parsefloat(&sp, 0);
				pose[pose_count].location[2] = parsefloat(&sp, 0);

				rot[0] = parsefloat(&sp, 0);
				rot[1] = parsefloat(&sp, 0);
				rot[2] = parsefloat(&sp, 0);

				rot[0] *= M_PI / 180.0;   // degree to radians
				rot[1] *= M_PI / 180.0;
				rot[2] *= M_PI / 180.0;

				pose[pose_count].scale[0] = parsefloat(&sp, 1);
				pose[pose_count].scale[1] = parsefloat(&sp, 1);
				pose[pose_count].scale[2] = parsefloat(&sp, 1);

				// Convert rotation to quaterion

        if( rot[ 0] == 0.0 && rot[ 1] == 0.0 && rot[ 2] == 0.0) {  // No rotation

          quat_set_identity( pose[pose_count].rotation); // set default rotation

        } else {

          double cx = cos(rot[0]/2), sx = sin(rot[0]/2),
                 cy = cos(rot[1]/2), sy = sin(rot[1]/2),
                 cz = cos(rot[2]/2), sz = sin(rot[2]/2);

          pose[pose_count].rotation[0] = sx*cy*cz - cx*sy*sz;
          pose[pose_count].rotation[1] = cx*sy*cz + sx*cy*sz;
          pose[pose_count].rotation[2] = cx*cy*sz - sx*sy*cz;
          pose[pose_count].rotation[3] = cx*cy*cz + sx*sy*sz;

          if( pose[pose_count].rotation[3] > 0) {     // flip all components

    				pose[pose_count].rotation[0] *= -1.0;
				    pose[pose_count].rotation[1] *= -1.0;
				    pose[pose_count].rotation[2] *= -1.0;
				    pose[pose_count].rotation[3] *= -1.0;
          }
        }

				pose_count++;
			}
		}

		else if (!strcmp(s, "joint")) {
			if (skel->joint_count < MAXBONE) {
				skel->j[ skel->joint_count].name   = strdup(parsestring(&sp));
				skel->j[ skel->joint_count].parent = parseint(&sp, -1);
				skel->joint_count++;
			}
		}

		else if (!strcmp(s, "animation")) {
			s = parsestring(&sp);
			anim = pushanim( model, s);
		}

		else if (!strcmp(s, "frame")) {
			pose = pushframe(anim, skel->joint_count);
			pose_count = 0;
		}

		else if (!strcmp(s, "framerate")) {

      if( anim != NULL) {   // security test

        anim->framerate = parsefloat(&sp, 10.0);
        if( anim->framerate <= 0.01) {  // security test
          anim->framerate = 10.0;
        }
      }
		}

		else if (!strcmp(s, "loop")) {

      if( anim != NULL) {   // security test

        anim->looped = true;
      }
		}

		else if (!strcmp(s, "mesh")) {
      // mesh from before
			if (IqeB_ImportArray_element.len > first) {
				pushpart(&IqeB_ImportArray_partbuf, first, IqeB_ImportArray_element.len,
                 MeshName, MaterialString, basedir);
			}
			first = IqeB_ImportArray_element.len;
			fm = IqeB_ImportArray_position.len / 3;

			s = parsestring(&sp);       // get mesh name of next mesh
			strncpy( MeshName, s, sizeof( MeshName) - 1);
		}

		else if (!strcmp(s, "material")) {
			s = parsestring(&sp);
			strncpy( MaterialString, s, sizeof( MaterialString) - 1);
		}
	}

	if (IqeB_ImportArray_element.len > first) {
		pushpart(&IqeB_ImportArray_partbuf, first, IqeB_ImportArray_element.len,
             MeshName, MaterialString, basedir);
	}

  // finish

  fclose( fp);

  IqeB_ModelSkeletonMatrixCalc( model);          // update skeleton matrix
  IqeB_AnimPrepareAfterLoad( model);             // Prepare animations

	mesh->vertex_count = IqeB_ImportArray_position.len / 3;
	mesh->position = (float *)dupArray(IqeB_ImportArray_position.data, IqeB_ImportArray_position.len, sizeof(float));
	mesh->normal = (float *)dupArray(IqeB_ImportArray_normal.data, IqeB_ImportArray_normal.len, sizeof(float));
	mesh->texcoord = (float *)dupArray(IqeB_ImportArray_texcoord.data, IqeB_ImportArray_texcoord.len, sizeof(float));
	mesh->color = (float *)dupArray(IqeB_ImportArray_color.data, IqeB_ImportArray_color.len, sizeof(float));
	mesh->blendindex = (int *)dupArray(IqeB_ImportArray_blendindex.data, IqeB_ImportArray_blendindex.len, sizeof(int));
	mesh->blendweight = (float *)dupArray(IqeB_ImportArray_blendweight.data, IqeB_ImportArray_blendweight.len, sizeof(float));
	mesh->aposition = NULL;
	mesh->anormal = NULL;

	mesh->element_count = IqeB_ImportArray_element.len;
	mesh->element = (int *)dupArray(IqeB_ImportArray_element.data, IqeB_ImportArray_element.len, sizeof(int));

	mesh->part_count = IqeB_ImportArray_partbuf.len;
	mesh->part = (part *)dupArray(IqeB_ImportArray_partbuf.data, IqeB_ImportArray_partbuf.len, sizeof(struct part));

	//x/fprintf(stderr, "\t%d batches; %d vertices; %d triangles; %d joints\n",
	//x/		mesh->part_count, mesh->vertex_count, mesh->element_count / 3, skel->joint_count);

  //x/fprintf(stderr, "%s: Done loading\n", filename);

	return model;
}

/****************************** End Of File ******************************/
