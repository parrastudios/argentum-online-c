/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Projection.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Project a point from 3D space into 2D view port
////////////////////////////////////////////////////////////
bool ProjectionProjectPoint(struct Vector3f * Position, struct Matrix4f * ModelView, struct Matrix4f * Projection, struct Rect * ViewPort, struct Vector3f * Result)
{
	if (Position && ModelView && Projection && ViewPort && Result)
	{
		struct Vector4f Target, Output;

		Vector4fSet(&Target, Position->x, Position->y, Position->z, 1.0f);

		// Multiply by model view matrix
		Matrix4fVertorMultiply(&Output, ModelView, &Target);

		// Multiply by projection matrix
		Matrix4fVertorMultiply(&Target, Projection, &Output);

		if (Target.w != 0.0f)
		{
			// Normalize target
			Target.x /= Target.w;
			Target.y /= Target.w;
			Target.z /= Target.w;

			// Clamp target (x, y, z) to [0, 1] range
			Result->x = (Target.x * 0.5f) + 0.5f;
			Result->y = (Target.y * 0.5f) + 0.5f;
			Result->z = (Target.z * 0.5f) + 0.5f;

			// Map target (x, y) to view port
			Result->x = (Result->x * ViewPort->Right) + ViewPort->Left;
			Result->y = (Result->y * ViewPort->Bottom) + ViewPort->Top;

			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Unproject a point from 2D view port into 3D space
////////////////////////////////////////////////////////////
bool ProjectionUnProjectPoint(struct Vector3f * Position, struct Matrix4f * ModelView, struct Matrix4f * Projection, struct Rect * ViewPort, struct Vector3f * Result)
{
	if (Position && ModelView && Projection && ViewPort && Result)
	{
		// ...
	}

	return false;
}
