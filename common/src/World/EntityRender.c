/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/EntityRender.h>

#include <Graphics/Device.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Default entity render
////////////////////////////////////////////////////////////
void EntityRenderDefault(struct EntityType * Entity)
{
    // Do nothing
}

////////////////////////////////////////////////////////////
/// Return the entity render implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentRenderType * EntityComponentRenderAttr()
{
	static const struct ComponentRenderType ComponentRenderAttr = {
		TypeInit(Draw, &EntityRenderDefault)
	};

	return (const struct ComponentRenderType *)&ComponentRenderAttr;
}

////////////////////////////////////////////////////////////
/// Return the entity render implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentRenderImpl()
{
	static const struct ComponentImplType ComponentRenderTypeImpl = {
		ExprInit(Base, TypeInit(Create, NULL), TypeInit(Destroy, NULL)),
		TypeInit(Type, COMPONENT_TYPE_RENDER),
		UnionInit(Attributes, Render, &EntityComponentRenderAttr)
	};

	return &ComponentRenderTypeImpl;
}

// todo: move entity render (general func) to upper abstraction layer

////////////////////////////////////////////////////////////
/// Render an entity (general function, not the overridden one)
////////////////////////////////////////////////////////////
void EntityRender(struct EntityType * Entity)
{
    if (Entity)
    {
        // Get transform component
        struct ComponentType * Transform = EntityComponentTransform(Entity);
        struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

        // Get render component
        struct ComponentType * Render = EntityComponentRender(Entity);
        struct ComponentRenderType * RenderType = ComponentTypeRender(Render);

        glPushMatrix();

        // Load entity transform world matrix
        glLoadMatrixf(&TransformData->WorldMatrix.m[0][0]);

        // Render entity
        RenderType->Draw(Entity);

        glPopMatrix();
    }
}
