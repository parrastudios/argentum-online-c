/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Audio/OpenALHelper.h>
#include <System/IOHelper.h>
#include <System/Error.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define ALC_ALL_DEVICES_SPECIFIER 0x1013 // Internal use

////////////////////////////////////////////////////////////
// Static member data
////////////////////////////////////////////////////////////
static ALCdevice*  AudioDevice	= NULL;
static ALCcontext* AudioContext = NULL;
static bool AudioInitialized = false;

////////////////////////////////////////////////////////////
/// Check the last OpenAL error
////////////////////////////////////////////////////////////
void ALCheckError(char * File, uint32 Line)
{
    // Get the last error
    ALenum ErrorCode = alGetError();

    if (ErrorCode != AL_NO_ERROR)
    {
        char * Error = NULL;
        char * Description = NULL;

        // Decode the error code
        switch (ErrorCode)
        {
            case AL_INVALID_NAME :
            {
                Error = "AL_INVALID_NAME";
                Description = "an unacceptable name has been specified";
                break;
            }

            case AL_INVALID_ENUM :
            {
                Error = "AL_INVALID_ENUM";
                Description = "an unacceptable value has been specified for an enumerated argument";
                break;
            }

            case AL_INVALID_VALUE :
            {
                Error = "AL_INVALID_VALUE";
                Description = "a numeric argument is out of range";
                break;
            }

            case AL_INVALID_OPERATION :
            {
                Error = "AL_INVALID_OPERATION";
                Description = "the specified operation is not allowed in the current state";
                break;
            }

            case AL_OUT_OF_MEMORY :
            {
                Error = "AL_OUT_OF_MEMORY";
                Description = "there is not enough memory left to execute the command";
                break;
            }
        }

        // Log the error
		MessageError("ALCheckError", "An internal OpenAL call failed in %s ( %d ) : %s, %s.", File, Line, Error, Description);
    }
}

////////////////////////////////////////////////////////////
/// Set up the audio device
////////////////////////////////////////////////////////////
void AudioInitializeDevice()
{
    // Create the device
    AudioDevice = alcOpenDevice(NULL);

    if (AudioDevice)
    {
        // Create the context
        AudioContext = alcCreateContext(AudioDevice, NULL);

        if (AudioContext)
        {
            // Set the context as the current one (we'll only need one)
			alcMakeContextCurrent(AudioContext);

			AudioInitialized = true;
        }
        else
        {
			MessageError("AudioInitializeDevice", "Failed to create the audio context.");

			AudioInitialized = false;
        }
    }
    else
	{
		MessageError("AudioInitializeDevice", "Failed to open the audio device");

		AudioInitialized = false;
    }

	// Print OpenAL features
	OpenALPrintFeatures();
}


////////////////////////////////////////////////////////////
/// Destroy the audio device
////////////////////////////////////////////////////////////
void AudioDestroyDevice()
{
    // Destroy the context
    alcMakeContextCurrent(NULL);

	if (AudioContext)
	{
		alcDestroyContext(AudioContext);
	}

    // Destroy the device
	if (AudioDevice)
	{
		alcCloseDevice(AudioDevice);
	}

	AudioInitialized = false;
}

////////////////////////////////////////////////////////////
/// Check OpenAL extensions
////////////////////////////////////////////////////////////
bool AudioIsExtensionSupported(char * Extension)
{
	if (AudioInitialized)
	{
		if ((strlen(Extension) > 2) && ((Extension[0] == 'A') && (Extension[1] == 'L') && (Extension[2] == 'C')))
		{
			return alcIsExtensionPresent(AudioDevice, Extension) != AL_FALSE;
		}
		else
		{
			return alIsExtensionPresent(Extension) != AL_FALSE;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Get chanels count format
////////////////////////////////////////////////////////////
int AudioGetFormatFromChannelsCount(uint32 ChannelsCount)
{
	if (AudioInitialized)
	{
		// Find the good format according to the number of channels
		switch (ChannelsCount)
		{
			case 1  : return AL_FORMAT_MONO16;
			case 2  : return AL_FORMAT_STEREO16;
			case 4  : return alGetEnumValue("AL_FORMAT_QUAD16");
			case 6  : return alGetEnumValue("AL_FORMAT_51CHN16");
			case 7  : return alGetEnumValue("AL_FORMAT_61CHN16");
			case 8  : return alGetEnumValue("AL_FORMAT_71CHN16");
			default : return 0;
		}
	}

	return 0;
}

////////////////////////////////////////////////////////////
/// Check if audio device is available
////////////////////////////////////////////////////////////
bool AudioIsAvailable()
{
	return AudioInitialized;
}

////////////////////////////////////////////////////////////
/// Print audio device and OpenAL features
////////////////////////////////////////////////////////////
void OpenALPrintFeatures()
{
	if (AudioInitialized)
	{
		ALCint ALCMajor = 0;
		ALCint ALCMinor = 0;

		// Get version
		alcGetIntegerv(AudioDevice, ALC_MAJOR_VERSION, sizeof(ALCint), &ALCMajor);
		alcGetIntegerv(AudioDevice, ALC_MINOR_VERSION, sizeof(ALCint), &ALCMinor);

		printf("Audio Device:   %s\nOpenAL Version: %d.%d.0\n",
			(char*)alcGetString(AudioDevice, ALC_ALL_DEVICES_SPECIFIER), ALCMajor, ALCMinor);
	}
}
