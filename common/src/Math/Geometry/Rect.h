/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	MATH_GEOMETRY_RECT_H
#define MATH_GEOMETRY_RECT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct Rect
{
	union
	{
		struct
		{
			integer Left;   ///< Left coordinate of the rectangle
			integer Top;    ///< Top coordinate of the rectangle
			integer Right;  ///< Right coordinate of the rectangle
			integer Bottom; ///< Bottom coordinate of the rectangle
		};

		integer Side[4];
	};
};

////////////////////////////////////////////////////////////
/// Initialize the rectangle
///
/// \param Rectangle : Rect to initiaize
/// \param Left : Horizontal offset
/// \param Top : Vertical offset
/// \param Right : Horizontal offset plus width
/// \param Bottom : Vertical offset plus height
///
////////////////////////////////////////////////////////////
void RectInitialize(struct Rect * Rectangle, integer Left, integer Top, integer Right, integer Bottom);

////////////////////////////////////////////////////////////
/// Move the whole rectangle by the given offset
///
/// \param Rectangle : Rect to move
/// \param OffsetX : Horizontal offset
/// \param OffsetY : Vertical offset
///
////////////////////////////////////////////////////////////
void RectMove(struct Rect * Rectangle, integer OffsetX, integer OffsetY);

////////////////////////////////////////////////////////////
/// Check if a point is inside the rectangle's area
///
/// \param Rectangle : Rect to check
/// \param X : X coordinate of the point to test
/// \param Y : Y coordinate of the point to test
///
/// \return true if the point is inside
///
////////////////////////////////////////////////////////////
bool RectContains(struct Rect * Rectangle, integer X, integer Y);

////////////////////////////////////////////////////////////
/// Check if a point is inside the rectangle's area
////////////////////////////////////////////////////////////
bool RectContainsEx(integer Left, integer Top, integer Right, integer Bottom, integer X, integer Y);

////////////////////////////////////////////////////////////
/// Check intersection between two rectangles
///
/// \param Rectangle :       Rectangle to test
/// \param OverlappingRect : Rectangle to be filled with overlapping rect (NULL by default)
///
/// \return True if rectangles overlap
///
////////////////////////////////////////////////////////////
bool RectIntersects(struct Rect * RectangleA, struct Rect * RectangleB, struct Rect * OverlappingRect);

#endif // MATH_GEOMETRY_RECT_H
