/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_DEVICE_H
#define GRAPHICS_DEVICE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Platform.h>
#include <Portability/Type.h>
#include <Window/VideoModeSupport.h>
#include <Window/Settings.h>
#include <Graphics/StatesManager.h>
#include <Graphics/MatrixStack.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define DEVICE_SYSTEM_UNKNOWN			0x00
#define DEVICE_SYSTEM_OPENGL			0x01 << 0x00
#define DEVICE_SYSTEM_DIRECTX			0x01 << 0x01
#define DEVICE_SYSTEM_SOFTWARE			0x01 << 0x02

#define DEVICE_STATE_READY				0x00
#define DEVICE_STATE_UNINIT				0x01
#define DEVICE_STATE_LOST				0x02
#define DEVICE_STATE_UNKNOWN			0x03

#define	DEVICE_RENDERING_2D				0x01 << 0x00
#define	DEVICE_RENDERING_3D				0x01 << 0x01
#define DEVICE_RENDERING_ALPHA			0x01 << 0x02

#define PIPELINE_SYSTEM_FIXED			0x00
#define PIPELINE_SYSTEM_PROGRAMMABLE	0x01

#define RENDER_SYSTEM_GL1				0x01 << 0x00
#define RENDER_SYSTEM_GL2				0x01 << 0x01
#define RENDER_SYSTEM_GL3				0x01 << 0x02
#define RENDER_SYSTEM_GL4				0x01 << 0x03
#define RENDER_SYSTEM_GLES1				0x01 << 0x04
#define RENDER_SYSTEM_GLES2				0x01 << 0x05
#define RENDER_SYSTEM_DX9				0x01 << 0x06
#define RENDER_SYSTEM_DX10				0x01 << 0x07
#define RENDER_SYSTEM_DX11				0x01 << 0x08

#define SHADER_SYSTEM_GLSL				0x01 << 0x00
#define SHADER_SYSTEM_CG				0x01 << 0x01
#define SHADER_SYSTEM_CUDA				0x01 << 0x02
#define SHADER_SYSTEM_HLSL				0x01 << 0x03

#define DEVICE_EXTENSION_STRING_SIZE	0xFFFF

////////////////////////////////////////////////////////////
// Define a default device system
////////////////////////////////////////////////////////////
#define DEVICE_SYSTEM_TYPE		DEVICE_SYSTEM_OPENGL

#if PLATFORM_TYPE == PLATFORM_ANDROID	|| \
	PLATFORM_TYPE == PLATFORM_IOS		|| \
	PLATFORM_TYPE == PLATFORM_NACL
#	define RENDER_SYSTEM_TYPE	RENDER_SYSTEM_GLES2
#else
#	define RENDER_SYSTEM_TYPE	RENDER_SYSTEM_GL2
#endif

#if RENDER_SYSTEM_TYPE == RENDER_SYSTEM_GLES2 || \
	(PLATFORM_TYPE != PLATFORM_IOS && PLATFORM_TYPE != PLATFORM_ANDROID)
#	define PIPELINE_SYSTEM_TYPE	RENDER_PIPELINE_PROGRAMMABLE
#else
#	define PIPELINE_SYSTEM_TYPE	RENDER_PIPELINE_FIXED
#endif

#define SHADER_SYSTEM_TYPE		SHADER_SYSTEM_GLSL

////////////////////////////////////////////////////////////
// Include device system dependencies
////////////////////////////////////////////////////////////
#if DEVICE_SYSTEM_TYPE == DEVICE_SYSTEM_OPENGL		//< OpenGL Includes
#	if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
#		define WIN32_LEAN_AND_MEAN
#		include <windows.h>
#		include <GL/gl.h>
#       if COMPILER_TYPE != COMPILER_MINGW
#		    ifndef WGL_WGLEXT_PROTOTYPES
#		    	define WGL_WGLEXT_PROTOTYPES
#		    endif
#	    	include <GL/wglext.h>
#	    	ifndef GL_GLEXT_PROTOTYPES
#	    		define GL_GLEXT_PROTOTYPES
#	    	endif
#		    include <GL/glext.h>
#       endif
#	elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD
#		include <GL/gl.h>
#       include <GL/glx.h>
#		ifndef GLX_GLXEXT_PROTOTYPES
#			define GLX_GLXEXT_PROTOTYPES
#		endif
#		include <GL/glxext.h>
#		ifndef GL_GLEXT_PROTOTYPES
#			define GL_GLEXT_PROTOTYPES
#		endif
#		include <GL/glext.h>
#	elif PLATFORM_TYPE == PLATFORM_MACOS
#		include <OpenGL/gl.h>
#		ifndef GL_GLEXT_PROTOTYPES
#			define GL_GLEXT_PROTOTYPES
#		endif
#		include <OpenGL/glext.h>
#	elif PLATFORM_TYPE == PLATFORM_ANDROID
#		include <GLES2/gl2.h>
#		include <GLES2/gl2ext.h>
#	elif PLATFORM_TYPE == PLATFORM_IOS
#		include <OpenGLES/ES2/gl.h>
#		include <OpenGLES/ES2/glext.h>
#	elif PLATFORM_TYPE == PLATFORM_NACL
#		include <ppapi/gles2/gl2ext_ppapi.h>
#		include <ppapi/c/ppb_opengles.h>
#		include <GLES2/gl2.h>
#		include <GLES2/gl2ext.h>
//		Not implemented:
//		https://github.com/rlane/nacl-gl-sample/blob/master/nacl-gl-sample.c
#	endif
#elif DEVICE_SYSTEM_TYPE == DEVICE_SYSTEM_DIRECTX	//< DirectX Includes
#	if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
#		if RENDER_SYSTEM_TYPE == RENDER_SYSTEM_DX9
#			include <d3d9.h>
#			include <d3dx9.h>
#		elif RENDER_SYSTEM_TYPE == RENDER_SYSTEM_DX10
#			include <d3d10.h>
#			include <d3dx10.h>
#		elif RENDER_SYSTEM_TYPE == RENDER_SYSTEM_DX10
#			include <d3d11.h>
#			include <d3dx11.h>
#		endif
#	endif
#elif DEVICE_SYSTEM_TYPE == DEVICE_SYSTEM_SOFTWARE	//< Software Includes
//	Nothing yet
#elif DEVICE_SYSTEM_TYPE == DEVICE_SYSTEM_UNKNOWN	//< Unknown Device
#	error Invalid device system
#endif

////////////////////////////////////////////////////////////
// Define device system constants
////////////////////////////////////////////////////////////
#if DEVICE_SYSTEM_TYPE == DEVICE_SYSTEM_OPENGL		//< OpenGL Definitions

#define GL_VERTEX_ATTRIB_ARRAY_ENABLED 0x8622
#define GL_VERTEX_ATTRIB_ARRAY_SIZE 0x8623
#define GL_VERTEX_ATTRIB_ARRAY_STRIDE 0x8624
#define GL_VERTEX_ATTRIB_ARRAY_TYPE 0x8625
#define GL_CURRENT_VERTEX_ATTRIB 0x8626
#define GL_VERTEX_PROGRAM_POINT_SIZE 0x8642
#define GL_VERTEX_PROGRAM_TWO_SIDE 0x8643
#define GL_VERTEX_ATTRIB_ARRAY_POINTER 0x8645
#define GL_STENCIL_BACK_FUNC 0x8800
#define GL_STENCIL_BACK_FAIL 0x8801
#define GL_STENCIL_BACK_PASS_DEPTH_FAIL 0x8802
#define GL_STENCIL_BACK_PASS_DEPTH_PASS 0x8803
#define GL_MAX_DRAW_BUFFERS 0x8824
#define GL_DRAW_BUFFER0 0x8825
#define GL_DRAW_BUFFER1 0x8826
#define GL_DRAW_BUFFER2 0x8827
#define GL_DRAW_BUFFER3 0x8828
#define GL_DRAW_BUFFER4 0x8829
#define GL_DRAW_BUFFER5 0x882A
#define GL_DRAW_BUFFER6 0x882B
#define GL_DRAW_BUFFER7 0x882C
#define GL_DRAW_BUFFER8 0x882D
#define GL_DRAW_BUFFER9 0x882E
#define GL_DRAW_BUFFER10 0x882F
#define GL_DRAW_BUFFER11 0x8830
#define GL_DRAW_BUFFER12 0x8831
#define GL_DRAW_BUFFER13 0x8832
#define GL_DRAW_BUFFER14 0x8833
#define GL_DRAW_BUFFER15 0x8834
#define GL_BLEND_EQUATION_ALPHA 0x883D
#define GL_POINT_SPRITE 0x8861
#define GL_COORD_REPLACE 0x8862
#define GL_MAX_VERTEX_ATTRIBS 0x8869
#define GL_VERTEX_ATTRIB_ARRAY_NORMALIZED 0x886A
#define GL_MAX_TEXTURE_COORDS 0x8871
#define GL_MAX_TEXTURE_IMAGE_UNITS 0x8872
#define GL_FRAGMENT_SHADER 0x8B30
#define GL_VERTEX_SHADER 0x8B31
#define GL_MAX_FRAGMENT_UNIFORM_COMPONENTS 0x8B49
#define GL_MAX_VERTEX_UNIFORM_COMPONENTS 0x8B4A
#define GL_MAX_VARYING_FLOATS 0x8B4B
#define GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS 0x8B4C
#define GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS 0x8B4D
#define GL_SHADER_TYPE 0x8B4F
#define GL_FLOAT_VEC2 0x8B50
#define GL_FLOAT_VEC3 0x8B51
#define GL_FLOAT_VEC4 0x8B52
#define GL_INT_VEC2 0x8B53
#define GL_INT_VEC3 0x8B54
#define GL_INT_VEC4 0x8B55
#define GL_BOOL 0x8B56
#define GL_BOOL_VEC2 0x8B57
#define GL_BOOL_VEC3 0x8B58
#define GL_BOOL_VEC4 0x8B59
#define GL_FLOAT_MAT2 0x8B5A
#define GL_FLOAT_MAT3 0x8B5B
#define GL_FLOAT_MAT4 0x8B5C
#define GL_SAMPLER_1D 0x8B5D
#define GL_SAMPLER_2D 0x8B5E
#define GL_SAMPLER_3D 0x8B5F
#define GL_SAMPLER_CUBE 0x8B60
#define GL_SAMPLER_1D_SHADOW 0x8B61
#define GL_SAMPLER_2D_SHADOW 0x8B62
#define GL_DELETE_STATUS 0x8B80
#define GL_COMPILE_STATUS 0x8B81
#define GL_LINK_STATUS 0x8B82
#define GL_VALIDATE_STATUS 0x8B83
#define GL_INFO_LOG_LENGTH 0x8B84
#define GL_ATTACHED_SHADERS 0x8B85
#define GL_ACTIVE_UNIFORMS 0x8B86
#define GL_ACTIVE_UNIFORM_MAX_LENGTH 0x8B87
#define GL_SHADER_SOURCE_LENGTH 0x8B88
#define GL_ACTIVE_ATTRIBUTES 0x8B89
#define GL_ACTIVE_ATTRIBUTE_MAX_LENGTH 0x8B8A
#define GL_FRAGMENT_SHADER_DERIVATIVE_HINT 0x8B8B
#define GL_SHADING_LANGUAGE_VERSION 0x8B8C
#define GL_CURRENT_PROGRAM 0x8B8D
#define GL_POINT_SPRITE_COORD_ORIGIN 0x8CA0
#define GL_LOWER_LEFT 0x8CA1
#define GL_UPPER_LEFT 0x8CA2
#define GL_STENCIL_BACK_REF 0x8CA3
#define GL_STENCIL_BACK_VALUE_MASK 0x8CA4
#define GL_STENCIL_BACK_WRITEMASK 0x8CA5

// todo: http://www.opensg.org/htdocs/doc-1.8/OSGGLEXT_8h_source.html

#   define GL_TEXTURE_3D                            0x806F
#   define GL_TEXTURE_BINDING_3D                    0x806A
#   define GL_SHADING_LANGUAGE_VERSION              0x8B8C
#	define GL_INVALID_FRAMEBUFFER_OPERATION_EXT		0x0506
#	define GL_BGR									0x80E0
#	define GL_CLAMP_TO_EDGE							0x812F	//< EXT_texture_edge_clamp
#	define GL_MAX_TEXTURES_UNITS_ARB				0x84E2	//< ARB_multitexture
#	define GL_TEXTURE0_ARB							0x84C0
#	define GL_TEXTURE1_ARB							0x84C1
#	define GL_TEXTURE2_ARB							0x84C2
#	define GL_TEXTURE3_ARB							0x84C3
#	define GL_MULTISAMPLE_ARB						0x809D
#	define GL_MAX_ELEMENTS_VERTICES					0x80E8
#	define GL_MAX_ELEMENTS_INDICES					0x80E9
#	define GL_COMBINE_ARB							0x8570	//< ARB_texture_env_combine
#	define GL_COMBINE_RGB_ARB						0x8571
#	define GL_COMBINE_ALPHA_ARB						0x8572
#	define GL_SOURCE0_RGB_ARB						0x8580
#	define GL_SOURCE1_RGB_ARB						0x8581
#	define GL_SOURCE2_RGB_ARB						0x8582
#	define GL_SOURCE0_ALPHA_ARB						0x8588
#	define GL_SOURCE1_ALPHA_ARB						0x8589
#	define GL_SOURCE2_ALPHA_ARB						0x858A
#	define GL_OPERAND0_RGB_ARB						0x8590
#	define GL_OPERAND1_RGB_ARB						0x8591
#	define GL_OPERAND2_RGB_ARB						0x8592
#	define GL_OPERAND0_ALPHA_ARB					0x8598
#	define GL_OPERAND1_ALPHA_ARB					0x8599
#	define GL_OPERAND2_ALPHA_ARB					0x859A
#	define GL_RGB_SCALE_ARB							0x8573
#	define GL_ADD_SIGNED_ARB						0x8574
#	define GL_INTERPOLATE_ARB						0x8575
#	define GL_SUBTRACT_ARB							0x84E7
#	define GL_CONSTANT_ARB							0x8576
#	define GL_PRIMARY_COLOR_ARB						0x8577
#	define GL_PREVIOUS_ARB							0x8578
#   define GL_ARRAY_BUFFER_ARB                      0x8892
#   define GL_STATIC_DRAW_ARB                       0x88E4
#   define GL_DYNAMIC_DRAW_ARB                      0x88E8
#   define GL_MAX_TESS_GEN_LEVEL                    0x8E7E
#   define GL_MAX_PATCH_VERTICES                    0x8E7D

#	define GL_UNIFORM_LOCATION_INVALID				0xFFFFFFFF
#	define GL_PARAMETER_INVALID						0xFFFFFFFF

#ifndef GL_ARRAY_BUFFER
#   define GL_ARRAY_BUFFER 0x8892
#endif // GL_ARRAY_BUFFER

#ifndef GL_ELEMENT_ARRAY_BUFFER
#   define GL_ELEMENT_ARRAY_BUFFER 0x8893
#endif // GL_ELEMENT_ARRAY_BUFFER

#ifndef GL_STREAM_DRAW
#   define GL_STREAM_DRAW 0x88E0
#endif // GL_STREAM_DRAW

#ifndef GL_STATIC_DRAW
#   define GL_STATIC_DRAW 0x88E4
#endif // GL_STATIC_DRAW

#ifndef GL_DYNAMIC_DRAW
#   define GL_DYNAMIC_DRAW 0x88E8
#endif // GL_DYNAMIC_DRAW

#ifndef GLAPIENTRY
#	if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
#	define GLAPIENTRY APIENTRY
#	else
#	define GLAPIENTRY
#	endif
#endif

typedef char GLchar;

// Shaders
typedef void (GLAPIENTRY * PFNGLATTACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (GLAPIENTRY * PFNGLBINDATTRIBLOCATIONPROC) (GLuint program, GLuint index, const GLchar* name);
typedef void (GLAPIENTRY * PFNGLBLENDEQUATIONSEPARATEPROC) (GLenum, GLenum);
typedef void (GLAPIENTRY * PFNGLCOMPILESHADERPROC) (GLuint shader);
typedef GLuint(GLAPIENTRY * PFNGLCREATEPROGRAMPROC) (void);
typedef GLuint(GLAPIENTRY * PFNGLCREATESHADERPROC) (GLenum type);
typedef void (GLAPIENTRY * PFNGLDELETEPROGRAMPROC) (GLuint program);
typedef void (GLAPIENTRY * PFNGLDELETESHADERPROC) (GLuint shader);
typedef void (GLAPIENTRY * PFNGLDETACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (GLAPIENTRY * PFNGLDISABLEVERTEXATTRIBARRAYPROC) (GLuint);
typedef void (GLAPIENTRY * PFNGLDRAWBUFFERSPROC) (GLsizei n, const GLenum* bufs);
typedef void (GLAPIENTRY * PFNGLENABLEVERTEXATTRIBARRAYPROC) (GLuint);
typedef void (GLAPIENTRY * PFNGLGETACTIVEATTRIBPROC) (GLuint program, GLuint index, GLsizei maxLength, GLsizei* length, GLint* size, GLenum* type, GLchar* name);
typedef void (GLAPIENTRY * PFNGLGETACTIVEUNIFORMPROC) (GLuint program, GLuint index, GLsizei maxLength, GLsizei* length, GLint* size, GLenum* type, GLchar* name);
typedef void (GLAPIENTRY * PFNGLGETATTACHEDSHADERSPROC) (GLuint program, GLsizei maxCount, GLsizei* count, GLuint* shaders);
typedef GLint(GLAPIENTRY * PFNGLGETATTRIBLOCATIONPROC) (GLuint program, const GLchar* name);
typedef void (GLAPIENTRY * PFNGLGETPROGRAMINFOLOGPROC) (GLuint program, GLsizei bufSize, GLsizei* length, GLchar* infoLog);
typedef void (GLAPIENTRY * PFNGLGETPROGRAMIVPROC) (GLuint program, GLenum pname, GLint* param);
typedef void (GLAPIENTRY * PFNGLGETSHADERINFOLOGPROC) (GLuint shader, GLsizei bufSize, GLsizei* length, GLchar* infoLog);
typedef void (GLAPIENTRY * PFNGLGETSHADERSOURCEPROC) (GLuint obj, GLsizei maxLength, GLsizei* length, GLchar* source);
typedef void (GLAPIENTRY * PFNGLGETSHADERIVPROC) (GLuint shader, GLenum pname, GLint* param);
typedef GLint(GLAPIENTRY * PFNGLGETUNIFORMLOCATIONPROC) (GLuint program, const GLchar* name);
typedef void (GLAPIENTRY * PFNGLGETUNIFORMFVPROC) (GLuint program, GLint location, GLfloat* params);
typedef void (GLAPIENTRY * PFNGLGETUNIFORMIVPROC) (GLuint program, GLint location, GLint* params);
typedef void (GLAPIENTRY * PFNGLGETVERTEXATTRIBPOINTERVPROC) (GLuint, GLenum, GLvoid**);
typedef void (GLAPIENTRY * PFNGLGETVERTEXATTRIBDVPROC) (GLuint, GLenum, GLdouble*);
typedef void (GLAPIENTRY * PFNGLGETVERTEXATTRIBFVPROC) (GLuint, GLenum, GLfloat*);
typedef void (GLAPIENTRY * PFNGLGETVERTEXATTRIBIVPROC) (GLuint, GLenum, GLint*);
typedef GLboolean(GLAPIENTRY * PFNGLISPROGRAMPROC) (GLuint program);
typedef GLboolean(GLAPIENTRY * PFNGLISSHADERPROC) (GLuint shader);
typedef void (GLAPIENTRY * PFNGLLINKPROGRAMPROC) (GLuint program);
typedef void (GLAPIENTRY * PFNGLSHADERSOURCEPROC) (GLuint shader, GLsizei count, const GLchar **string, const GLint *length);
typedef void (GLAPIENTRY * PFNGLSTENCILFUNCSEPARATEPROC) (GLenum frontfunc, GLenum backfunc, GLint ref, GLuint mask);
typedef void (GLAPIENTRY * PFNGLSTENCILMASKSEPARATEPROC) (GLenum, GLuint);
typedef void (GLAPIENTRY * PFNGLSTENCILOPSEPARATEPROC) (GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass);
typedef void (GLAPIENTRY * PFNGLUNIFORM1FPROC) (GLint location, GLfloat v0);
typedef void (GLAPIENTRY * PFNGLUNIFORM1FVPROC) (GLint location, GLsizei count, const GLfloat* value);
typedef void (GLAPIENTRY * PFNGLUNIFORM1IPROC) (GLint location, GLint v0);
typedef void (GLAPIENTRY * PFNGLUNIFORM1IVPROC) (GLint location, GLsizei count, const GLint* value);
typedef void (GLAPIENTRY * PFNGLUNIFORM2FPROC) (GLint location, GLfloat v0, GLfloat v1);
typedef void (GLAPIENTRY * PFNGLUNIFORM2FVPROC) (GLint location, GLsizei count, const GLfloat* value);
typedef void (GLAPIENTRY * PFNGLUNIFORM2IPROC) (GLint location, GLint v0, GLint v1);
typedef void (GLAPIENTRY * PFNGLUNIFORM2IVPROC) (GLint location, GLsizei count, const GLint* value);
typedef void (GLAPIENTRY * PFNGLUNIFORM3FPROC) (GLint location, GLfloat v0, GLfloat v1, GLfloat v2);
typedef void (GLAPIENTRY * PFNGLUNIFORM3FVPROC) (GLint location, GLsizei count, const GLfloat* value);
typedef void (GLAPIENTRY * PFNGLUNIFORM3IPROC) (GLint location, GLint v0, GLint v1, GLint v2);
typedef void (GLAPIENTRY * PFNGLUNIFORM3IVPROC) (GLint location, GLsizei count, const GLint* value);
typedef void (GLAPIENTRY * PFNGLUNIFORM4FPROC) (GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
typedef void (GLAPIENTRY * PFNGLUNIFORM4FVPROC) (GLint location, GLsizei count, const GLfloat* value);
typedef void (GLAPIENTRY * PFNGLUNIFORM4IPROC) (GLint location, GLint v0, GLint v1, GLint v2, GLint v3);
typedef void (GLAPIENTRY * PFNGLUNIFORM4IVPROC) (GLint location, GLsizei count, const GLint* value);
typedef void (GLAPIENTRY * PFNGLUNIFORMMATRIX2FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);
typedef void (GLAPIENTRY * PFNGLUNIFORMMATRIX3FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);
typedef void (GLAPIENTRY * PFNGLUNIFORMMATRIX4FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);
typedef void (GLAPIENTRY * PFNGLUSEPROGRAMPROC) (GLuint program);
typedef void (GLAPIENTRY * PFNGLVALIDATEPROGRAMPROC) (GLuint program);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB1DPROC) (GLuint index, GLdouble x);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB1DVPROC) (GLuint index, const GLdouble* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB1FPROC) (GLuint index, GLfloat x);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB1FVPROC) (GLuint index, const GLfloat* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB1SPROC) (GLuint index, GLshort x);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB1SVPROC) (GLuint index, const GLshort* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB2DPROC) (GLuint index, GLdouble x, GLdouble y);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB2DVPROC) (GLuint index, const GLdouble* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB2FPROC) (GLuint index, GLfloat x, GLfloat y);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB2FVPROC) (GLuint index, const GLfloat* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB2SPROC) (GLuint index, GLshort x, GLshort y);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB2SVPROC) (GLuint index, const GLshort* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB3DPROC) (GLuint index, GLdouble x, GLdouble y, GLdouble z);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB3DVPROC) (GLuint index, const GLdouble* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB3FPROC) (GLuint index, GLfloat x, GLfloat y, GLfloat z);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB3FVPROC) (GLuint index, const GLfloat* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB3SPROC) (GLuint index, GLshort x, GLshort y, GLshort z);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB3SVPROC) (GLuint index, const GLshort* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4NBVPROC) (GLuint index, const GLbyte* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4NIVPROC) (GLuint index, const GLint* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4NSVPROC) (GLuint index, const GLshort* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4NUBPROC) (GLuint index, GLubyte x, GLubyte y, GLubyte z, GLubyte w);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4NUBVPROC) (GLuint index, const GLubyte* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4NUIVPROC) (GLuint index, const GLuint* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4NUSVPROC) (GLuint index, const GLushort* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4BVPROC) (GLuint index, const GLbyte* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4DPROC) (GLuint index, GLdouble x, GLdouble y, GLdouble z, GLdouble w);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4DVPROC) (GLuint index, const GLdouble* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4FPROC) (GLuint index, GLfloat x, GLfloat y, GLfloat z, GLfloat w);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4FVPROC) (GLuint index, const GLfloat* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4IVPROC) (GLuint index, const GLint* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4SPROC) (GLuint index, GLshort x, GLshort y, GLshort z, GLshort w);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4SVPROC) (GLuint index, const GLshort* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4UBVPROC) (GLuint index, const GLubyte* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4UIVPROC) (GLuint index, const GLuint* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIB4USVPROC) (GLuint index, const GLushort* v);
typedef void (GLAPIENTRY * PFNGLVERTEXATTRIBPOINTERPROC) (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer);

#	if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
		// EXT_compiled_vertex_array
		typedef void (APIENTRY * GLLOCKARRAYSEXTPROC) (GLint first, GLsizei count);
		typedef void (APIENTRY * GLUNLOCKARRAYSEXTPROC) ();

		// ARB_multitexture
		typedef void (APIENTRY * GLMULTITEXCOORD2FARBPROC) (GLenum target, GLfloat s, GLfloat t);
		typedef void (APIENTRY * GLACTIVETEXTUREARBPROC) (GLenum target);
		typedef void (APIENTRY * GLCLIENTACTIVETEXTUREARBPROC) (GLenum target);

		// VBOs
		typedef void (APIENTRY * GLGENBUFFERSARBPROC) (GLsizei n, GLuint* ids);
		typedef void (APIENTRY * GLBINDBUFFERARBPROC) (GLenum target, GLuint id);
		typedef void (APIENTRY * GLBUFFERDATAARBPROC) (GLenum target, GLsizei size, const void* data, GLenum usage);
		typedef void (APIENTRY * GLBUFFERSUBDATAARBPROC) (GLenum target, GLint offset, GLsizei size, void * data);
		typedef void (APIENTRY * GLDELETEBUFFERSARBPROC) (GLsizei n, const GLuint* ids);
		typedef void* (APIENTRY * GLMAPBUFFERARBPROC) (GLenum target, GLenum access);
		typedef GLboolean (APIENTRY * GLUNMAPBUFFERARBPROC) (GLenum target);
		typedef void (APIENTRY * GLVERTEXATTRIBPOINTERPROC) (GLuint index, GLint size, GLenum type, GLboolean normalized,
															 GLsizei stride, const GLvoid * pointer);

		// VAOs
		typedef void (APIENTRY * GLGENVERTEXARRAYPROC) (GLsizei n, GLuint * arrays);
		typedef void (APIENTRY * GLBINDVERTEXARRAYPROC) (GLuint array);

        #if COMPILER_TYPE == COMPILER_MINGW
            typedef bool (APIENTRY * PFNWGLSWAPINTERVALEXTPROC) (int interval);
        #endif

        typedef int (APIENTRY * PFNWGLGETSWAPINTERVALEXTPROC) (void);


		// WGL_ARB_extension_string
		typedef const char * (WINAPI * PFNWGLGETEXTENSIONSSTRINGARBPROC) (HDC hDC);
        // WGL_EXT_extension_string
        typedef const char* (WINAPI * PFNWGLGETEXTENSIONSSTRINGEXTPROC) (void);

		// WGL_ARB_pixel_format
		#define WGL_NUMBER_PIXEL_FORMATS_ARB         0x2000
		#define WGL_DRAW_TO_WINDOW_ARB               0x2001
		#define WGL_DRAW_TO_BITMAP_ARB               0x2002
		#define WGL_ACCELERATION_ARB                 0x2003
		#define WGL_NEED_PALETTE_ARB                 0x2004
		#define WGL_NEED_SYSTEM_PALETTE_ARB          0x2005
		#define WGL_SWAP_LAYER_BUFFERS_ARB           0x2006
		#define WGL_SWAP_METHOD_ARB                  0x2007
		#define WGL_NUMBER_OVERLAYS_ARB              0x2008
		#define WGL_NUMBER_UNDERLAYS_ARB             0x2009
		#define WGL_TRANSPARENT_ARB                  0x200A
		#define WGL_TRANSPARENT_RED_VALUE_ARB        0x2037
		#define WGL_TRANSPARENT_GREEN_VALUE_ARB      0x2038
		#define WGL_TRANSPARENT_BLUE_VALUE_ARB       0x2039
		#define WGL_TRANSPARENT_ALPHA_VALUE_ARB      0x203A
		#define WGL_TRANSPARENT_INDEX_VALUE_ARB      0x203B
		#define WGL_SHARE_DEPTH_ARB                  0x200C
		#define WGL_SHARE_STENCIL_ARB                0x200D
		#define WGL_SHARE_ACCUM_ARB                  0x200E
		#define WGL_SUPPORT_GDI_ARB                  0x200F
		#define WGL_SUPPORT_OPENGL_ARB               0x2010
		#define WGL_DOUBLE_BUFFER_ARB                0x2011
		#define WGL_SAMPLE_BUFFERS_ARB               0x2041
		#define WGL_SAMPLES_ARB                      0x2042
		#define WGL_STEREO_ARB                       0x2012
		#define WGL_PIXEL_TYPE_ARB                   0x2013
		#define WGL_COLOR_BITS_ARB                   0x2014
		#define WGL_RED_BITS_ARB                     0x2015
		#define WGL_RED_SHIFT_ARB                    0x2016
		#define WGL_GREEN_BITS_ARB                   0x2017
		#define WGL_GREEN_SHIFT_ARB                  0x2018
		#define WGL_BLUE_BITS_ARB                    0x2019
		#define WGL_BLUE_SHIFT_ARB                   0x201A
		#define WGL_ALPHA_BITS_ARB                   0x201B
		#define WGL_ALPHA_SHIFT_ARB                  0x201C
		#define WGL_ACCUM_BITS_ARB                   0x201D
		#define WGL_ACCUM_RED_BITS_ARB               0x201E
		#define WGL_ACCUM_GREEN_BITS_ARB             0x201F
		#define WGL_ACCUM_BLUE_BITS_ARB              0x2020
		#define WGL_ACCUM_ALPHA_BITS_ARB             0x2021
		#define WGL_DEPTH_BITS_ARB                   0x2022
		#define WGL_STENCIL_BITS_ARB                 0x2023
		#define WGL_AUX_BUFFERS_ARB                  0x2024
		#define WGL_NO_ACCELERATION_ARB              0x2025
		#define WGL_GENERIC_ACCELERATION_ARB         0x2026
		#define WGL_FULL_ACCELERATION_ARB            0x2027
		#define WGL_SWAP_EXCHANGE_ARB                0x2028
		#define WGL_SWAP_COPY_ARB                    0x2029
		#define WGL_SWAP_UNDEFINED_ARB               0x202A
		#define WGL_TYPE_RGBA_ARB                    0x202B
		#define WGL_TYPE_COLORINDEX_ARB              0x202C

		typedef BOOL (WINAPI * PFNWGLGETPIXELFORMATATTRIBIVARBPROC) (
												HDC hDC,
												int iPixelFormat,
												int iLayerPlane,
												UINT nAttributes,
												const int *piAttributes,
												int *piValues);
		typedef BOOL (WINAPI * PFNWGLGETPIXELFORMATATTRIBFVARBPROC) (
												HDC hDC,
												int iPixelFormat,
												int iLayerPlane,
												UINT nAttributes,
												const int *piAttributes,
												FLOAT *pfValues);
		typedef BOOL (WINAPI * PFNWGLCHOOSEPIXELFORMATARBPROC) (
												HDC hDC,
												const int *piAttribIList,
												const FLOAT *pfAttribFList,
												UINT nMaxFormats,
												int *piFormats,
												UINT *nNumFormats);

		// WGL_ARB_make_current_read
		typedef BOOL (WINAPI * PFNWGLMAKECONTEXTCURRENTARBPROC) (
												HDC hDrawDC,
												HDC hReadDC,
												HGLRC hGLRC);
		typedef HDC (WINAPI * PFNWGLGETCURRENTREADDCARBPROC) (VOID);

		// WGL_ARB_pbuffer
		#define WGL_DRAW_TO_PBUFFER_ARB              0x202D
		#define WGL_MAX_PBUFFER_PIXELS_ARB           0x202E
		#define WGL_MAX_PBUFFER_WIDTH_ARB            0x202F
		#define WGL_MAX_PBUFFER_HEIGHT_ARB           0x2030
		#define WGL_PBUFFER_LARGEST_ARB              0x2033
		#define WGL_PBUFFER_WIDTH_ARB                0x2034
		#define WGL_PBUFFER_HEIGHT_ARB               0x2035
		#define WGL_PBUFFER_LOST_ARB				 0x2036

		#ifndef HPBUFFERARB
		#   ifndef DECLARE_HANDLE
            struct { int32 unused; } HPBUFFERARB;
		#   else
		#       if COMPILER_TYPE != COMPILER_MSVC
                    DECLARE_HANDLE(HPBUFFERARB);
        #       endif
        #   endif
        #endif

		typedef HPBUFFERARB (WINAPI * PFNWGLCREATEPBUFFERARBPROC) (
												HDC hDC,
												int iPixelFormat,
												int iWidth,
												int iHeight,
												const int *piAttribList);
		typedef HDC (WINAPI * PFNWGLGETPBUFFERDCARBPROC) (HPBUFFERARB hPbuffer);
		typedef int (WINAPI * PFNWGLRELEASEPBUFFERDCARBPROC) (
												HPBUFFERARB hPbuffer,
												HDC hDC);
		typedef BOOL (WINAPI * PFNWGLDESTROYPBUFFERARBPROC) (HPBUFFERARB hPbuffer);
		typedef BOOL (WINAPI * PFNWGLQUERYPBUFFERARBPROC) (
												HPBUFFERARB hPbuffer,
												int iAttribute,
												int *piValue);

		// GL_NV_VERTEX_ARRAY_RANGE
		#define GL_VERTEX_ARRAY_RANGE_NV             0x851D
		#define GL_VERTEX_ARRAY_RANGE_LENGTH_NV      0x851E
		#define GL_VERTEX_ARRAY_RANGE_VALID_NV       0x851F
		#define GL_MAX_VERTEX_ARRAY_RANGE_ELEMENT_NV 0x8520
		#define GL_VERTEX_ARRAY_RANGE_POINTER_NV     0x8521
		typedef void (WINAPI * PFNGLFLUSHVERTEXARRAYRANGENVPROC) (void);
		typedef void (WINAPI * PFNGLVERTEXARRAYRANGENVPROC) (
												GLsizei size,
												const GLvoid *pointer);
		typedef void* (WINAPI * PFNWGLALLOCATEMEMORYNVPROC) (
												int size,
												float readfreq,
												float writefreq,
												float priority);
		typedef void  (WINAPI * PFNWGLFREEMEMORYNVPROC) (void *pointer);

		// GL_NV_FENCE
		#define GL_ALL_COMPLETED_NV                  0x84F2
		#define GL_FENCE_STATUS_NV                   0x84F3
		#define GL_FENCE_CONDITION_NV                0x84F4
		typedef void (WINAPI * PFNGLDELETEFENCESNVPROC) (GLsizei n, const GLuint *fences);
		typedef void (WINAPI * PFNGLGENFENCESNVPROC) (GLsizei n, GLuint *fences);
		typedef GLboolean (WINAPI * PFNGLISFENCENVPROC) (GLuint fence);
		typedef GLboolean (WINAPI * PFNGLTESTFENCENVPROC) (GLuint fence);
		typedef void (WINAPI * PFNGLGETFENCEIVNVPROC) (GLuint fence, GLenum pname, GLint *params);
		typedef void (WINAPI * PFNGLFINISHFENCENVPROC) (GLuint fence);
		typedef void (WINAPI * PFNGLSETFENCENVPROC) (GLuint fence, GLenum condition);

#	elif PLATFORM_TYPE == PLATFORM_LINUX

		// EXT_compiled_vertex_array
		typedef void (* GLLOCKARRAYSEXTPROC) (GLint first, GLsizei count);
		typedef void (* GLUNLOCKARRAYSEXTPROC) ();

		// ARB_multitexture
		typedef void (* GLMULTITEXCOORD2FARBPROC) (GLenum target, GLfloat s, GLfloat t);
		typedef void (* GLACTIVETEXTUREARBPROC) (GLenum target);
		typedef void (* GLCLIENTACTIVETEXTUREARBPROC) (GLenum target);

		// VBOs
		typedef void (* GLGENBUFFERSARBPROC) (GLsizei n, GLuint* ids);
		typedef void (* GLBINDBUFFERARBPROC) (GLenum target, GLuint id);
		typedef void (* GLBUFFERDATAARBPROC) (GLenum target, GLsizei size, const void* data, GLenum usage);
		typedef void (* GLBUFFERSUBDATAARBPROC) (GLenum target, GLint offset, GLsizei size, void * data);
		typedef void (* GLDELETEBUFFERSARBPROC) (GLsizei n, const GLuint* ids);
		typedef void* (* GLMAPBUFFERARBPROC) (GLenum target, GLenum access);
		typedef GLboolean (* GLUNMAPBUFFERARBPROC) (GLenum target);
		typedef void (* GLVERTEXATTRIBPOINTERPROC) (GLuint index, GLint size, GLenum type, GLboolean normalized,
															 GLsizei stride, const GLvoid * pointer);
		// VAOs
		typedef void (* GLGENVERTEXARRAYPROC) (GLsizei n, GLuint * arrays);
		typedef void (* GLBINDVERTEXARRAYPROC) (GLuint array);

		// TODO

#	else
#		error TODO OpenGL SO Implementations
#	endif
#elif DEVICE_SYSTEM_TYPE == DEVICE_SYSTEM_DIRECTX
//	Nothing here?
#endif

////////////////////////////////////////////////////////////
// Define context type
////////////////////////////////////////////////////////////
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
	typedef HDC ContextType;

#	if DEVICE_SYSTEM_TYPE == DEVICE_SYSTEM_OPENGL
		typedef HGLRC GLContextType;
#	endif

	typedef void * DeviceTypeImpl;

#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD
	typedef Display * ContextType;

#	if DEVICE_SYSTEM_TYPE == DEVICE_SYSTEM_OPENGL
		typedef GLXContext GLContextType;
#	endif

	typedef struct
	{
		int32	Screen;
		XIC     InputContext;
		XIM 	InputMethod;
	} DeviceTypeImpl;

#elif PLATFORM_TYPE == PLATFORM_MACOS
	// TODO
#else
	typedef void * ContextType;

#	if DEVICE_SYSTEM_TYPE == DEVICE_SYSTEM_OPENGL
		typedef void * GLContextType;
#	endif

	typedef void * DeviceTypeImpl;
#endif

////////////////////////////////////////////////////////////
// Define library handle type
////////////////////////////////////////////////////////////
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
#	define WIN32_LEAN_AND_MEAN
#	include <windows.h>
	typedef HMODULE DeviceLibraryHandle;
#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD
#	include <dlfcn.h>
	typedef void * DeviceLibraryHandle;
#elif PLATFORM_TYPE == PLATFORM_MACOS
#	include <Carbon/Carbon.h>
	typedef struct
	{
		CFBundleRef Bundle;
		CFURLRef	BundleURL;
	} DeviceLibraryHandle;
#else
	typedef void * DeviceLibraryHandle;
#endif

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct DeviceInfoType
{
	struct FeaturesInfo				///< API Features
	{
		char * Vendor;
		char * Renderer;
		char * Version;
		char * ShaderVersion;
	} Features;

	struct BitsInfo					///< Bits info
	{
		int32 Red;
		int32 Green;
		int32 Blue;
		int32 Alpha;
		int32 Depth;
		int32 Stencil;
	} Bits;

	struct StackInfo				///< Sizes of memory stack
	{
		int32 Attrib;
		int32 ModelView;
		int32 Projection;
		int32 ClipPlanes;
		int32 Texture;
	} Stack;

	struct LimitsInfo
	{
		int32 Lights;				///< Lights max size
		int32 Units;				///< Multitexture units
		int32 Texture;				///< Texture max size
		int32 Tessellation;			///< Tessellation - max level size
		int32 PatchVertices;		///< Tessellation - max vertices per patch
	} Limits;

	struct ExtensionSupport
	{
		bool TextureNPOT;		    ///< Texture not power of two
		bool VBO;					///< Vertex buffer objects
		bool CVA;				    ///< Compiled vertex arrays
		bool EdgeClamp;			    ///< Edge clapmp
		bool PBuffer;			    ///< Pixel buffer
		bool MakeCurrentRead;	    ///< ARB Make current read
		bool VARFence;			    ///< Vertex array range
		bool TextureCombine;	    ///< Texture NV Combine
		bool MultiSample;
		bool Shaders;
	} Support;

	char	Extensions[DEVICE_EXTENSION_STRING_SIZE];		///< Extension string
};

struct DeviceExtensionType
{
#	if DEVICE_SYSTEM_TYPE == DEVICE_SYSTEM_OPENGL

			// Shaders
			PFNGLATTACHSHADERPROC glAttachShader;
			PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation;
			PFNGLBLENDEQUATIONSEPARATEPROC glBlendEquationSeparate;
			PFNGLCOMPILESHADERPROC glCompileShader;
			PFNGLCREATEPROGRAMPROC glCreateProgram;
			PFNGLCREATESHADERPROC glCreateShader;
			PFNGLDELETEPROGRAMPROC glDeleteProgram;
			PFNGLDELETESHADERPROC glDeleteShader;
			PFNGLDETACHSHADERPROC glDetachShader;
			PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray;
			PFNGLDRAWBUFFERSPROC glDrawBuffers;
			PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
			PFNGLGETACTIVEATTRIBPROC glGetActiveAttrib;
			PFNGLGETACTIVEUNIFORMPROC glGetActiveUniform;
			PFNGLGETATTACHEDSHADERSPROC glGetAttachedShaders;
			PFNGLGETATTRIBLOCATIONPROC glGetAttribLocation;
			PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;
			PFNGLGETPROGRAMIVPROC glGetProgramiv;
			PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog;
			PFNGLGETSHADERSOURCEPROC glGetShaderSource;
			PFNGLGETSHADERIVPROC glGetShaderiv;
			PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;
			PFNGLGETUNIFORMFVPROC glGetUniformfv;
			PFNGLGETUNIFORMIVPROC glGetUniformiv;
			PFNGLGETVERTEXATTRIBPOINTERVPROC glGetVertexAttribPointerv;
			PFNGLGETVERTEXATTRIBDVPROC glGetVertexAttribdv;
			PFNGLGETVERTEXATTRIBFVPROC glGetVertexAttribfv;
			PFNGLGETVERTEXATTRIBIVPROC glGetVertexAttribiv;
			PFNGLISPROGRAMPROC glIsProgram;
			PFNGLISSHADERPROC glIsShader;
			PFNGLLINKPROGRAMPROC glLinkProgram;
			PFNGLSHADERSOURCEPROC glShaderSource;
			PFNGLSTENCILFUNCSEPARATEPROC glStencilFuncSeparate;
			PFNGLSTENCILMASKSEPARATEPROC glStencilMaskSeparate;
			PFNGLSTENCILOPSEPARATEPROC glStencilOpSeparate;
			PFNGLUNIFORM1FPROC glUniform1f;
			PFNGLUNIFORM1FVPROC glUniform1fv;
			PFNGLUNIFORM1IPROC glUniform1i;
			PFNGLUNIFORM1IVPROC glUniform1iv;
			PFNGLUNIFORM2FPROC glUniform2f;
			PFNGLUNIFORM2FVPROC glUniform2fv;
			PFNGLUNIFORM2IPROC glUniform2i;
			PFNGLUNIFORM2IVPROC glUniform2iv;
			PFNGLUNIFORM3FPROC glUniform3f;
			PFNGLUNIFORM3FVPROC glUniform3fv;
			PFNGLUNIFORM3IPROC glUniform3i;
			PFNGLUNIFORM3IVPROC glUniform3iv;
			PFNGLUNIFORM4FPROC glUniform4f;
			PFNGLUNIFORM4FVPROC glUniform4fv;
			PFNGLUNIFORM4IPROC glUniform4i;
			PFNGLUNIFORM4IVPROC glUniform4iv;
			PFNGLUNIFORMMATRIX2FVPROC glUniformMatrix2fv;
			PFNGLUNIFORMMATRIX3FVPROC glUniformMatrix3fv;
			PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv;
			PFNGLUSEPROGRAMPROC glUseProgram;
			PFNGLVALIDATEPROGRAMPROC glValidateProgram;
			PFNGLVERTEXATTRIB1DPROC glVertexAttrib1d;
			PFNGLVERTEXATTRIB1DVPROC glVertexAttrib1dv;
			PFNGLVERTEXATTRIB1FPROC glVertexAttrib1f;
			PFNGLVERTEXATTRIB1FVPROC glVertexAttrib1fv;
			PFNGLVERTEXATTRIB1SPROC glVertexAttrib1s;
			PFNGLVERTEXATTRIB1SVPROC glVertexAttrib1sv;
			PFNGLVERTEXATTRIB2DPROC glVertexAttrib2d;
			PFNGLVERTEXATTRIB2DVPROC glVertexAttrib2dv;
			PFNGLVERTEXATTRIB2FPROC glVertexAttrib2f;
			PFNGLVERTEXATTRIB2FVPROC glVertexAttrib2fv;
			PFNGLVERTEXATTRIB2SPROC glVertexAttrib2s;
			PFNGLVERTEXATTRIB2SVPROC glVertexAttrib2sv;
			PFNGLVERTEXATTRIB3DPROC glVertexAttrib3d;
			PFNGLVERTEXATTRIB3DVPROC glVertexAttrib3dv;
			PFNGLVERTEXATTRIB3FPROC glVertexAttrib3f;
			PFNGLVERTEXATTRIB3FVPROC glVertexAttrib3fv;
			PFNGLVERTEXATTRIB3SPROC glVertexAttrib3s;
			PFNGLVERTEXATTRIB3SVPROC glVertexAttrib3sv;
			PFNGLVERTEXATTRIB4NBVPROC glVertexAttrib4Nbv;
			PFNGLVERTEXATTRIB4NIVPROC glVertexAttrib4Niv;
			PFNGLVERTEXATTRIB4NSVPROC glVertexAttrib4Nsv;
			PFNGLVERTEXATTRIB4NUBPROC glVertexAttrib4Nub;
			PFNGLVERTEXATTRIB4NUBVPROC glVertexAttrib4Nubv;
			PFNGLVERTEXATTRIB4NUIVPROC glVertexAttrib4Nuiv;
			PFNGLVERTEXATTRIB4NUSVPROC glVertexAttrib4Nusv;
			PFNGLVERTEXATTRIB4BVPROC glVertexAttrib4bv;
			PFNGLVERTEXATTRIB4DPROC glVertexAttrib4d;
			PFNGLVERTEXATTRIB4DVPROC glVertexAttrib4dv;
			PFNGLVERTEXATTRIB4FPROC glVertexAttrib4f;
			PFNGLVERTEXATTRIB4FVPROC glVertexAttrib4fv;
			PFNGLVERTEXATTRIB4IVPROC glVertexAttrib4iv;
			PFNGLVERTEXATTRIB4SPROC glVertexAttrib4s;
			PFNGLVERTEXATTRIB4SVPROC glVertexAttrib4sv;
			PFNGLVERTEXATTRIB4UBVPROC glVertexAttrib4ubv;
			PFNGLVERTEXATTRIB4UIVPROC glVertexAttrib4uiv;
			PFNGLVERTEXATTRIB4USVPROC glVertexAttrib4usv;
			PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer;

			// Compiled vertex array function pointers
			GLLOCKARRAYSEXTPROC					glLockArraysEXT;
			GLUNLOCKARRAYSEXTPROC				glUnlockArraysEXT;

			// Multitexture function pointers
			GLMULTITEXCOORD2FARBPROC			glMultiTexCoord2fARB;
			GLACTIVETEXTUREARBPROC				glActiveTextureARB;
			GLCLIENTACTIVETEXTUREARBPROC		glClientActiveTextureARB;

			// VBOs
			GLGENBUFFERSARBPROC					glGenBuffersARB;
			GLBINDBUFFERARBPROC					glBindBufferARB;
			GLBUFFERDATAARBPROC					glBufferDataARB;
			GLBUFFERSUBDATAARBPROC				glBufferSubDataARB;
			GLDELETEBUFFERSARBPROC				glDeleteBuffersARB;
			GLMAPBUFFERARBPROC					glMapBufferARB;
			GLUNMAPBUFFERARBPROC				glUnmapBufferARB;
			GLVERTEXATTRIBPOINTERPROC			glVertexAttribPointerARB;

			// VAOs
			GLGENVERTEXARRAYPROC				glGenVertexArray;
			GLBINDVERTEXARRAYPROC				glBindVertexArray;

#		if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
			// Swap interval
			PFNWGLSWAPINTERVALEXTPROC			wglSwapIntervalEXT;
			PFNWGLGETSWAPINTERVALEXTPROC		wglGetSwapIntervalEXT;

			// WGL_ARB_pixel_format
			PFNWGLGETPIXELFORMATATTRIBIVARBPROC wglGetPixelFormatAttribivARB;
			PFNWGLGETPIXELFORMATATTRIBFVARBPROC wglGetPixelFormatAttribfvARB;
			PFNWGLCHOOSEPIXELFORMATARBPROC		wglChoosePixelFormatARB;

			// WGL_ARB_make_current_read
			PFNWGLMAKECONTEXTCURRENTARBPROC		wglMakeContextCurrentARB;
			PFNWGLGETCURRENTREADDCARBPROC		wglGetCurrentReadDCARB;

			// WGL_ARB_pbuffer
			PFNWGLCREATEPBUFFERARBPROC			wglCreatePbufferARB;
			PFNWGLGETPBUFFERDCARBPROC			wglGetPbufferDCARB;
			PFNWGLRELEASEPBUFFERDCARBPROC		wglReleasePbufferDCARB;
			PFNWGLDESTROYPBUFFERARBPROC			wglDestroyPbufferARB;
			PFNWGLQUERYPBUFFERARBPROC			wglQueryPbufferARB;

			// GL_NV_VERTEX_ARRAY_RANGE
			PFNWGLALLOCATEMEMORYNVPROC			wglAllocateMemoryNV;
			PFNWGLFREEMEMORYNVPROC				wglFreeMemoryNV;

#		elif PLATFORM_TYPE == PLATFORM_LINUX

			// GL_NV_VERTEX_ARRAY_RANGE
			PFNGLXALLOCATEMEMORYNVPROC			glXAllocateMemoryNV;
			PFNGLXFREEMEMORYNVPROC				glXFreeMemoryNV;

#		else
#			error TODO OpenGL SO Implementations
#		endif
#	elif DEVICE_SYSTEM_TYPE == DEVICE_SYSTEM_DIRECTX
//		Nothing here?
#	endif

        // GL_NV_FENCE
        PFNGLDELETEFENCESNVPROC				glDeleteFencesNV;
        PFNGLGENFENCESNVPROC				glGenFencesNV;
        PFNGLISFENCENVPROC					glIsFenceNV;
        PFNGLTESTFENCENVPROC				glTestFenceNV;
        PFNGLGETFENCEIVNVPROC				glGetFenceivNV;
        PFNGLFINISHFENCENVPROC				glFinishFenceNV;
        PFNGLSETFENCENVPROC					glSetFenceNV;
        PFNGLFLUSHVERTEXARRAYRANGENVPROC	glFlushVertexArrayRangeNV;
        PFNGLVERTEXARRAYRANGENVPROC			glVertexArrayRangeNV;
};

struct DeviceType
{
	ContextType					Context;
	DeviceTypeImpl				Impl;
	struct DeviceInfoType		Info;

#	if DEVICE_SYSTEM_TYPE == DEVICE_SYSTEM_OPENGL
	GLContextType				GLContext;
	struct DeviceExtensionType	Extensions;
	DeviceLibraryHandle			LibraryHandle;
#	endif

	struct VideoMode			CurrentVideoMode;

	struct StatesStack			StatesManager;
	struct MatrixStack *		MatrixRenderStack;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

struct DeviceType * DeviceGet();

struct DeviceInfoType * DeviceGetInfo();

struct DeviceExtensionType * DeviceGetExt();

DeviceTypeImpl * DeviceGetImpl();

////////////////////////////////////////////////////////////
/// Initialize current device (loads extensions and context)
///
////////////////////////////////////////////////////////////
void DeviceCreate(struct DeviceType * Device, struct VideoMode * Mode, struct WindowSettings * Params);

////////////////////////////////////////////////////////////
/// Destroy current device (delete extensions and context)
///
////////////////////////////////////////////////////////////
void DeviceDestroy(struct DeviceType * Device);

////////////////////////////////////////////////////////////
/// Reset current device
///
////////////////////////////////////////////////////////////
void DeviceReset(struct DeviceType * Device);

////////////////////////////////////////////////////////////
/// Configure device states for rendering
///
////////////////////////////////////////////////////////////
bool DeviceInitialize(struct DeviceType * Device);

////////////////////////////////////////////////////////////
/// Clear current device
///
////////////////////////////////////////////////////////////
void DeviceClear(struct DeviceType * Device, bool Depth, bool Stencil);

////////////////////////////////////////////////////////////
/// Check if the current context is created
///
////////////////////////////////////////////////////////////
bool DeviceContextValid(struct DeviceType * Device);

////////////////////////////////////////////////////////////
/// Check if there's an active context on the current thread
///
////////////////////////////////////////////////////////////
bool DeviceContextActive(struct DeviceType * Device);

////////////////////////////////////////////////////////////
/// Enable / disable vertical synchronization
///
/// \param Enabled : True to enable v-sync, false to deactivate
///
////////////////////////////////////////////////////////////
void DeviceUseVerticalSync(struct DeviceType * Device, bool Enabled);

////////////////////////////////////////////////////////////
/// Set the render context associated to window context
///
/// \param Target : Pointer to the render target
///
////////////////////////////////////////////////////////////
void DeviceSetRenderTarget(struct DeviceType * Device);

////////////////////////////////////////////////////////////
/// Return binding constant associated to the texture target
///
////////////////////////////////////////////////////////////
uint32 DeviceBindingConst(uint32 Target);

////////////////////////////////////////////////////////////
/// Check the last device error
////////////////////////////////////////////////////////////
static void DeviceCheckError(char * File, uint32 Line);

////////////////////////////////////////////////////////////
/// Macro to check every Device API call
////////////////////////////////////////////////////////////
#if COMPILE_TYPE == DEBUG_MODE
//	In debug mode, perform a test on every call
#	define DeviceCheck(Func) ((Func), DeviceCheckError(__FILE__, __LINE__))
#else
//	Else, we don't add any overhead
#	define DeviceCheck(Func) (Func)
#endif

////////////////////////////////////////////////////////////
/// Prints graphic device and API features
///
////////////////////////////////////////////////////////////
void DevicePrintFeatures(struct DeviceType * Device);

////////////////////////////////////////////////////////////
/// Get string containing all API extensions
///
////////////////////////////////////////////////////////////
void DeviceGetExtensionString(struct DeviceType * Device);

////////////////////////////////////////////////////////////
/// Load the API extension pointers
///
////////////////////////////////////////////////////////////
void DeviceLoadExtensions(struct DeviceType * Device);

////////////////////////////////////////////////////////////
/// Check the API extensions
///
////////////////////////////////////////////////////////////
bool DeviceCheckExtension(struct DeviceType * Device, char * Extension);

////////////////////////////////////////////////////////////
/// Get the process address from an extension string
///
////////////////////////////////////////////////////////////
void * DeviceGetExtension(struct DeviceType * Device, char * Extension);

////////////////////////////////////////////////////////////
/// Set current state of device
///
////////////////////////////////////////////////////////////
void DeviceSetState(struct DeviceType * Device, uint32 Mode);

////////////////////////////////////////////////////////////
/// Set current matrix
///
////////////////////////////////////////////////////////////
void DeviceSetMatrix(struct DeviceType * Device, struct Matrix4f * Matrix);

////////////////////////////////////////////////////////////
/// Set current matrix to orthogonal ones
///
////////////////////////////////////////////////////////////
void DeviceSetMatrixOrthogonal(struct DeviceType * Device);

////////////////////////////////////////////////////////////
/// Begin render context
///
////////////////////////////////////////////////////////////
void DeviceBegin(struct DeviceType * Device);

////////////////////////////////////////////////////////////
/// End render context
///
////////////////////////////////////////////////////////////
void DeviceEnd(struct DeviceType * Device);

#endif // GRAPHICS_DEVICE_H
