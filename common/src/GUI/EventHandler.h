/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GUI_EVENT_HANDLER_H
#define GUI_EVENT_HANDLER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <GUI/Widget.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define GUI_EVENT_ERROR	0xFF
#define GUI_EVENT_KEY_PRESSED           0x00
#define GUI_EVENT_KEY_RELEASED          0x01
#define GUI_EVENT_TEXT_ENTERED          0x02
#define GUI_EVENT_BUTTON_PRESSED        0x03
#define GUI_EVENT_BUTTON_RELEASED       0x04
#define GUI_EVENT_MOUSE_MOVE            0x05
#define GUI_EVENT_OPEN                  0x06
#define GUI_EVENT_CLOSE                 0x07

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct EventPtr
{
	union
	{
		void (*KeyPressed)(struct KeyEvent*);
		void (*KeyReleased)(struct KeyEvent*);
		void (*TextEntered)(struct TextEvent*);
		void (*ButtonPressed)(struct MouseButtonEvent*);
		void (*ButtonReleased)(struct MouseButtonEvent*);
		void (*MouseMove)(struct MouseMoveEvent*);
		void (*Open)();
		void (*Close)();
	}		Ptr;			///< Pointer to the function
	uint32	Type;			///< Function type
	char	ID[0xFF];		///< ID of the function
};

////////////////////////////////////////////////////////////
// Common Methods
////////////////////////////////////////////////////////////

void VoidHandler_ButtonReleased(struct MouseButtonEvent * ButtonEvent);

////////////////////////////////////////////////////////////
/// Initialize all events
////////////////////////////////////////////////////////////
void GuiEventHandlerInitialize();

////////////////////////////////////////////////////////////
/// Return the event type
////////////////////////////////////////////////////////////
uint32 GuiEventHandlerType(char * EventType);

////////////////////////////////////////////////////////////
/// Get the event type of the function id
////////////////////////////////////////////////////////////
uint32 GuiEventHandlerGetType(char * FunctionID);

////////////////////////////////////////////////////////////
// Add an event to the list
////////////////////////////////////////////////////////////
struct EventNode * GuiEventHandlerAdd(char * ID, void * Func);

////////////////////////////////////////////////////////////
// Get an event by id
////////////////////////////////////////////////////////////
struct EventPtr * GuiEventHandlerGet(char * ID);

////////////////////////////////////////////////////////////
// Destroy the list
////////////////////////////////////////////////////////////
void GuiEventHandlerDestroy();

#endif // GUI_EVENT_HANDLER_H
