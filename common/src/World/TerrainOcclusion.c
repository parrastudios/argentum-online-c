/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/TerrainOcclusion.h>

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

// TODO: lets see what functions are private and what not, in order to make
//          them opaque and also to speedup the calls avoiding to pass the entity
//          by arguments, and passing directly the pointer to the component if need

////////////////////////////////////////////////////////////
/// Create a occlusion component of a terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainOcclusionCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Initialize terrain occlusion properties
////////////////////////////////////////////////////////////
struct EntityType * TerrainOcclusionInitialize(struct EntityType * Entity /*, ... */);

////////////////////////////////////////////////////////////
/// Destroy a terrain occlusion component
////////////////////////////////////////////////////////////
void TerrainOcclusionDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
///	Build sector and sub-sector sine & cosine tables
///
///	Sine and cosine are the sector plane coefficients
///	defined by:
///
///		Ax + By + Cz + D = 0
///
///	where C = D = 0; A = sin(Theta); B = -cos(Theta) and
///	Theta is the angle between the plane and the X axis
////////////////////////////////////////////////////////////
void TerrainOcclusionBuildTrigTables(struct TerrainComponentOcclusionData * Occlusion);

////////////////////////////////////////////////////////////
///	Perpendicular project a 3D point into a 2D point
///	in the plane of a specified sector
////////////////////////////////////////////////////////////
void TerrainOcclusionPerpProject(struct TerrainComponentOcclusionData * Occlusion, struct Vector3i * Point,
	uinteger Sector, struct Vector2i * Result);

////////////////////////////////////////////////////////////
///	Find the horizon point for a point in specified direction
///	using a Bresenham type DDA walk down the center of the
///	sub-sector, so there may be undersampling artifacts
///	if the sub - sectors are too wide
///
///	Returns the distance, elevation and coordinates of the
///	horizon
///
///	Assuming that heightmap is repeated in x-y so that the
///	view at the right edge of the heightmap wraps back to
///	the left edge, etc. and the heightmap size is power of two
////////////////////////////////////////////////////////////
void TerrainOcclusionFindHorizon(struct EntityType * Entity, struct Vector3i * Point, struct Vector2i * Direction,
	float * Distance, float * Elevation, struct Vector3i * Coordinates);

////////////////////////////////////////////////////////////
///	Find the minimum horizon point for a point in a sector
///
/// Returns horizon distance, elevation and coordinates
////////////////////////////////////////////////////////////
void TerrainOcclusionFindSectorHorizon(struct EntityType * Entity, struct Vector3i * Point,
	uinteger Sector, float * Distance, float * Elevation,
struct Vector3i * MinHorizonPoint);

////////////////////////////////////////////////////////////
///	For each point in a patch for sector calculate and store
///	the perpendicular projection of the point and minimum
///	horizon point onto the sectors plane
///
///	The patch is defined by the 2D point and the patch size
///	The result is stored into TerrainOcclusion HorizonPoints
////////////////////////////////////////////////////////////
void TerrainOcclusionCalcHorizonPoints(struct EntityType * Entity, struct Vector2i * Point, uinteger Sector);

void TerrainOcclusionBuildList(struct EntityType * Entity, struct Vector2i * Point, integer * OcclusionPoints);

uinteger TerrainOcclusionMergeRegions(struct EntityType * Entity, struct Vector2i * Point,
	integer * OcclusionPoints);

void TerrainOcclusionMergePatchRegions(struct EntityType * Entity, integer ** OcclusionPatchPoints,
	integer * OcclusionPoints);

void TerrainOcclusionGenerateQuadtree(struct EntityType * Entity, uinteger Level,
struct Vector2f);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the terrain occlusion implementation attributes
////////////////////////////////////////////////////////////
const void * TerrainComponentOcclusionAttr()
{
	static const struct TerrainComponentOcclusionType TerrainComponentOcclusionAttr = {

		// ...

		{ NULL }

	};

	return &TerrainComponentOcclusionAttr;
}

////////////////////////////////////////////////////////////
/// Return the terrain occlusion implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * TerrainComponentOcclusionImpl()
{
	static const struct ComponentImplType TerrainComponentOcclusionTypeImpl = {
		ExprInit(Base, TypeInit(Create, &TerrainOcclusionCreate), TypeInit(Destroy, &TerrainOcclusionDestroy)),
		TypeInit(Type, COMPONENT_TYPE_OCCLUSION),
		UnionInit(Attributes, Derived, &TerrainComponentOcclusionAttr)
	};

	return &TerrainComponentOcclusionTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a occlusion component of a terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainOcclusionCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Create occlusion component data reference
        struct ComponentType * Occlusion = EntityComponentOcclusion(Entity);
		struct TerrainComponentOcclusionData * OcclusionData = ComponentDataDerived(Occlusion, struct TerrainComponentOcclusionData);


        // ...


		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Initialize terrain occlusion properties
////////////////////////////////////////////////////////////
struct EntityType * TerrainOcclusionInitialize(struct EntityType * Entity /*, ... */)
{
	if (Entity)
	{
		// Create occlusion component data reference
        struct ComponentType * Occlusion = EntityComponentOcclusion(Entity);
		struct TerrainComponentOcclusionData * OcclusionData = ComponentDataDerived(Occlusion, struct TerrainComponentOcclusionData);


		// ...


		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a terrain occlusion component
////////////////////////////////////////////////////////////
void TerrainOcclusionDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Create occlusion component data reference
        struct ComponentType * Occlusion = EntityComponentOcclusion(Entity);
		struct TerrainComponentOcclusionData * OcclusionData = ComponentDataDerived(Occlusion, struct TerrainComponentOcclusionData);


		// ...


	}
}










/*
////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Memory/General.h>
#include <World/TerrainOcclusion.h>
#include <World/TerrainLoader.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define OcclusionSectorTrigTableSin(Table, Sector) \
	Table[(Sector) * 2]

#define OcclusionSectorTrigTableCos(Table, Sector) \
	Table[((Sector) * 2) + 1]

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
///	Build sector and sub-sector sine & cosine tables
////////////////////////////////////////////////////////////
void TerrainOcclusionBuildTrigTables(struct TerrainOcclusionType * Occlusion)
{
	// Sector table
	uinteger Count = Occlusion->Sectors;

	if (Occlusion->SectorTrigTable)
	{
		uinteger Sector;

		for (Sector = 0; Sector < Count; ++Sector)
		{
			float Azimuth = 2 * MATH_PI * ((float)Sector + 0.5f) / (float)Count;

			OcclusionSectorTrigTableSin(Occlusion->SectorTrigTable, Sector) = (float)sin(Azimuth);
			OcclusionSectorTrigTableCos(Occlusion->SectorTrigTable, Sector) = (float)cos(Azimuth);
		}
	}

	// Subsector table
	Count = Occlusion->Sectors * Occlusion->SubSectors;

	Occlusion->SubSectorTrigTable = (float*)MemoryAllocate(2 * Count * sizeof(float));

	if (Occlusion->SubSectorTrigTable)
	{
		uinteger Sector;

		for (Sector = 0; Sector < Count; ++Sector)
		{
			float Azimuth = 2 * MATH_PI * ((float)Sector + 0.5f) / (float)Count;

			OcclusionSectorTrigTableSin(Occlusion->SubSectorTrigTable, Sector) = (float)sin(Azimuth);
			OcclusionSectorTrigTableCos(Occlusion->SubSectorTrigTable, Sector) = (float)cos(Azimuth);
		}
	}
}

////////////////////////////////////////////////////////////
///	Perpendicular project a 3D point into a 2D point
///	in the plane of a specified sector
////////////////////////////////////////////////////////////
void TerrainOcclusionPerpProject(struct TerrainOcclusionType * Occlusion, struct Vector3i * Point, uinteger Sector, struct Vector2i * Result)
{
	float Sin = OcclusionSectorTrigTableSin(Occlusion->SectorTrigTable, Sector);
	float Cos = OcclusionSectorTrigTableCos(Occlusion->SectorTrigTable, Sector);

	Result->x = (integer)floor(Point->x * Cos + Point->y * Sin + 0.5f);
	Result->y = Point->z;
}

////////////////////////////////////////////////////////////
///	Find the horizon point for a point in specified direction
////////////////////////////////////////////////////////////
void TerrainOcclusionFindHorizon(struct TerrainType * Terrain, struct Vector3i * Point, struct Vector2i * Direction,
								 float * Distance, float * Elevation, struct Vector3i * Coordinates)
{
	struct Vector2i Increment, Max, Min;
	struct Vector3i Vector, Vector0;
	integer C, M, D;
	integer X, Y;
	float Dist, Elev;

	Vector2iSet(&Increment, 1, 1);

	Vector.x = Vector0.x = Point->x;
	Vector.y = Vector0.y = Point->y;
	Vector.z = Vector0.z = Point->z;

	Max.x = Vector0.x + Terrain->HeightMapTile * Terrain->HeightMapSize;
	Max.y = Vector0.y + Terrain->HeightMapTile * Terrain->HeightMapSize;
	Min.x = Vector0.x - Terrain->HeightMapTile * Terrain->HeightMapSize;
	Min.y = Vector0.y - Terrain->HeightMapTile * Terrain->HeightMapSize;

	*Elevation = INT_MIN;

	if (Direction->x < 0)
	{
		Increment.x = -1;
		Direction->x = -Direction->x;
	}

	if (Direction->y < 0)
	{
		Increment.y = -1;
		Direction->y = -Direction->y;
	}

	if (Direction->y < Direction->x)
	{
		C = 2 * Direction->x;
		D = M = 2 * Direction->y;
		Vector.x += Increment.x;

		if (D > Direction->x)
		{
			Vector.y += Increment.y;
			D -= C;
		}

		while (Vector.x <= Max.x && Vector.x >= Min.x)
		{
			X = Vector.x & (Terrain->HeightMapSize - 1);
			Y = Vector.y & (Terrain->HeightMapSize - 1);
			Vector.z = Terrain->HeightMap[X + Y*(Terrain->HeightMapSize + 1)];
			Dist = (float)sqrt(sqr(Vector.x - Vector0.x) + sqr(Vector.y - Vector0.y));
			Elev = (Vector.z - Vector0.z) / Dist;

			if (Elev > *Elevation)
			{
				*Distance = Dist;
				*Elevation = Elev;
				Coordinates->x = Vector.x;
				Coordinates->y = Vector.y;
				Coordinates->z = Vector.z;
			}

			Vector.x += Increment.x;
			D += M;

			if (D > Direction->x)
			{
				Vector.y += Increment.y;
				D -= C;
			}
		}
	}
	else
	{
		C = 2 * Direction->y;
		D = M = 2 * Direction->x;
		Vector.y += Increment.y;

		if (D > Direction->y)
		{
			Vector.x += Increment.x;
			D -= C;
		}

		while (Vector.y <= Max.y && Vector.y >= Min.y)
		{
			X = Vector.x & (Terrain->HeightMapSize - 1);
			Y = Vector.y & (Terrain->HeightMapSize - 1);
			Vector.z = Terrain->HeightMap[X + Y * (Terrain->HeightMapSize + 1)];
			Dist = (float)sqrt(sqr(Vector.x - Vector0.x) + sqr(Vector.y - Vector0.y));
			Elev = (Vector.z - Vector0.z) / Dist;

			if (Elev > *Elevation)
			{
				*Distance = Dist;
				*Elevation = Elev;
				Coordinates->x = Vector.x;
				Coordinates->y = Vector.y;
				Coordinates->z = Vector.z;
			}

			Vector.y += Increment.y;
			D += M;

			if (D > Direction->y)
			{
				Vector.x += Increment.x;
				D -= C;
			}
		}
	}
}

////////////////////////////////////////////////////////////
///	Find the minimum horizon point for a point in a sector
////////////////////////////////////////////////////////////
void TerrainOcclusionFindSectorHorizon(struct TerrainType * Terrain, struct Vector3i * Point,
									   uinteger Sector, float * Distance, float * Elevation,
									   struct Vector3i * MinHorizonPoint)
{
	struct TerrainOcclusionType * Occlusion = TerrainOcclusion(Terrain);
	uinteger SubSector;

	// For each sub-sector
	for (SubSector = 0; SubSector < Occlusion->SubSectors; ++SubSector)
	{
		struct Vector2i Direction;
		struct Vector3i Coordinates;
		float Dist, Elev;

		// Find the horizon point
		float Sin = OcclusionSectorTrigTableSin(Occlusion->SubSectorTrigTable, Sector * Occlusion->SubSectors + SubSector);
		float Cos = OcclusionSectorTrigTableCos(Occlusion->SubSectorTrigTable, Sector * Occlusion->SubSectors + SubSector);

		Vector2iSet(&Direction, (int32)floor(Cos * Terrain->HeightMapSize + 0.5f),
								(int32)floor(Sin * Terrain->HeightMapSize + 0.5f));

		TerrainOcclusionFindHorizon(Terrain, Point, &Direction, &Dist, &Elev, &Coordinates);

		// Save the minimum elevation horizon point at the greatest distance in the return variables
		if (!SubSector)
		{
			*Elevation = Elev;
			*Distance = Dist;
			MinHorizonPoint->x = Coordinates.x;
			MinHorizonPoint->y = Coordinates.y;
		}
		else
		{
			if (Elev < *Elevation)
			{
				*Elevation = Elev;
			}

			if (Dist > *Distance)
			{
				*Distance = Dist;
				MinHorizonPoint->x = Coordinates.x;
				MinHorizonPoint->y = Coordinates.y;
			}
		}
	}

	// Calculate z for the returned distance and elevation
	MinHorizonPoint->z = (int32)floor((*Distance) * (*Elevation) + Point->z);
}

////////////////////////////////////////////////////////////
///	For each point in a patch for sector calculate and store
///	the perpendicular projection of the point and minimum
///	horizon point onto the sectors plane
////////////////////////////////////////////////////////////
void TerrainOcclusionCalcHorizonPoints(struct TerrainType * Terrain, struct Vector2i * Point, uinteger Sector)
{
	struct TerrainOcclusionType * Occlusion = TerrainOcclusion(Terrain);
	struct Vector3i PatchPoint;
	integer * HorizonPoints = Occlusion->HorizonPoints;

	// For each point in the patch
	for (PatchPoint.y = Point->y; PatchPoint.y <= (int32)(Point->y + Terrain->PatchSize); ++PatchPoint.y)
	{
		integer Y = PatchPoint.y & (Terrain->HeightMapSize - 1);

		for (PatchPoint.x = Point->x; PatchPoint.x <= (int32)(Point->x + Terrain->PatchSize); ++PatchPoint.x)
		{
			integer X = PatchPoint.x & (Terrain->HeightMapSize - 1);

			struct Vector3i HeightMin;
			float Distance, Elevation;

			PatchPoint.z = Terrain->HeightMap[X + Y * (Terrain->HeightMapSize + 1)];

			// Find the minimum elevation horizon point for the sector
			TerrainOcclusionFindSectorHorizon(Terrain, &PatchPoint, Sector, &Distance, &Elevation, &HeightMin);

			// Save the projected heightmap point
			TerrainOcclusionPerpProject(Occlusion, &PatchPoint, Sector, HorizonPoints);
			HorizonPoints += 2;

			// Save the projected minimum horizon point
			TerrainOcclusionPerpProject(Occlusion, &HeightMin, Sector, HorizonPoints);
			HorizonPoints += 2;
		}
	}
}
*/

////////////////////////////////////////////////////////////
/// Return the terrain occlusion derived data size
////////////////////////////////////////////////////////////
uinteger TerrainComponentOcclusionSize()
{
	return sizeof(struct TerrainComponentOcclusionData);
}
