/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/Timer.h>
#include <System/Platform.h>

////////////////////////////////////////////////////////////
// Clears the timer
////////////////////////////////////////////////////////////
void TimerClear(struct Timer * TimerData)
{
	TimerData->ElapsedTime = TimerData->LastCounter = 0.0f;
}

////////////////////////////////////////////////////////////
// Reset the timer to 0
////////////////////////////////////////////////////////////
void TimerReset(struct Timer * TimerData)
{
	// Update elapsed time
	TimerData->ElapsedTime = SystemGetTime();

	// Reset
	TimerData->LastCounter = TimerData->ElapsedTime;
}

////////////////////////////////////////////////////////////
// Check time
////////////////////////////////////////////////////////////
bool TimerCheck(struct Timer * TimerData, float Interval)
{
	// Update elapsed time
	TimerData->ElapsedTime = SystemGetTime();

	if (TimerData->ElapsedTime - TimerData->LastCounter >= Interval)
	{
		// Reset
		TimerData->LastCounter = TimerData->ElapsedTime;
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
// Check time by specified elapsed time
////////////////////////////////////////////////////////////
bool TimerCheckEx(struct Timer * TimerData, float ElapsedTime, float Interval)
{
	// Update elapsed time
	TimerData->ElapsedTime = ElapsedTime;

	if (TimerData->ElapsedTime - TimerData->LastCounter >= Interval)
	{
		// Reset
		TimerData->LastCounter = TimerData->ElapsedTime;
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
// Return time of timer
////////////////////////////////////////////////////////////
float TimerGetTime(struct Timer * TimerData)
{
	// Update elapsed time
	TimerData->ElapsedTime = SystemGetTime();

	return (TimerData->ElapsedTime - TimerData->LastCounter);
}
