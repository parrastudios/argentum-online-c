/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Plugin/Plugin.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a plugin and initialize it
////////////////////////////////////////////////////////////
struct PluginType * PluginCreate()
{
	struct PluginType * Plugin = MemoryAllocate(sizeof(struct PluginType));

	if (Plugin)
	{
		// Initialize plugin connector
		ConnectorInitialize(&Plugin->Connector);

		return Plugin;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a plugin and release its resources
////////////////////////////////////////////////////////////
void PluginDestroy(struct PluginType * Plugin)
{
	if (Plugin)
	{
		// Destroy connector
		ConnectorDestroy(&Plugin->Connector);

		// Clear plugin memory
		MemoryDeallocate(Plugin);
	}
}
