/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	METADATA_TYPE_H
#define METADATA_TYPE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Types.h>

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define MetadataTypeName(TypeName) \
	METADATA_TYPE_DATA_##TypeName

#define MetadataTypeCast(TypeName) \
	METADATA_TYPE_CAST_##TypeName

#define MetadataTypeSize(TypeName) \
	METADATA_TYPE_SIZE_##TypeName

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define METADATA_TYPE_DATA_STRING_LENGTH	0xFFUL

#define METADATA_TYPE_CAST_VOID				(void)
#define METADATA_TYPE_CAST_BOOLEAN			(bool)
#define METADATA_TYPE_CAST_UINT8			(uint8)
#define METADATA_TYPE_CAST_INT8				(int8)
#define METADATA_TYPE_CAST_UINT16			(uint16)
#define METADATA_TYPE_CAST_INT16			(int16)
#define METADATA_TYPE_CAST_UINT32			(uint32)
#define METADATA_TYPE_CAST_INT32			(int32)
#define METADATA_TYPE_CAST_UINT64			(uint64)
#define METADATA_TYPE_CAST_INT64			(int64)
#define METADATA_TYPE_CAST_FLOAT			(float32)
#define METADATA_TYPE_CAST_DOUBLE			(float64)
#define METADATA_TYPE_CAST_STRING			(uint8 *)

#define METADATA_TYPE_SIZE_VOID				0
#define METADATA_TYPE_SIZE_BOOLEAN			sizeof(bool)
#define METADATA_TYPE_SIZE_UINT8			sizeof(uint8)
#define METADATA_TYPE_SIZE_INT8				sizeof(int8)
#define METADATA_TYPE_SIZE_UINT16			sizeof(uint16)
#define METADATA_TYPE_SIZE_INT16			sizeof(int16)
#define METADATA_TYPE_SIZE_UINT32			sizeof(uint32)
#define METADATA_TYPE_SIZE_INT32			sizeof(int32)
#define METADATA_TYPE_SIZE_UINT64			sizeof(uint64)
#define METADATA_TYPE_SIZE_INT64			sizeof(int64)
#define METADATA_TYPE_SIZE_FLOAT			sizeof(float32)
#define METADATA_TYPE_SIZE_DOUBLE			sizeof(float64)
#define METADATA_TYPE_SIZE_STRING			METADATA_TYPE_DATA_STRING_LENGTH

////////////////////////////////////////////////////////////
/// Type name definition
////////////////////////////////////////////////////////////
#define METADATA_TYPE_NAME_INFO_DECL(TypeName, Id) \
	MetadataTypeName(TypeName) = Id

typedef uint32 MetadataTypeName;

enum MetadataTypeNameInfo
{
	METADATA_TYPE_NAME_INFO_DECL(VOID,		0x00),
	METADATA_TYPE_NAME_INFO_DECL(BOOLEAN,	0x01),
	METADATA_TYPE_NAME_INFO_DECL(UINT8,		0x02),
	METADATA_TYPE_NAME_INFO_DECL(INT8,		0x03),
	METADATA_TYPE_NAME_INFO_DECL(UINT16,	0x04),
	METADATA_TYPE_NAME_INFO_DECL(INT16,		0x05),
	METADATA_TYPE_NAME_INFO_DECL(UINT32,	0x06),
	METADATA_TYPE_NAME_INFO_DECL(INT32,		0x07),
	METADATA_TYPE_NAME_INFO_DECL(UINT64,	0x08),
	METADATA_TYPE_NAME_INFO_DECL(INT64,		0x09),
	METADATA_TYPE_NAME_INFO_DECL(FLOAT,		0x0A),
	METADATA_TYPE_NAME_INFO_DECL(DOUBLE,	0x0B),
	METADATA_TYPE_NAME_INFO_DECL(STRING,	0x0C),

	METADATA_TYPE_DATA_SIZE,

	METADATA_TYPE_DATA_INVALID = 0xFFFFFFFF
};

#undef METADATA_TYPE_NAME_INFO_DECL

////////////////////////////////////////////////////////////
/// Type size definition
////////////////////////////////////////////////////////////
#define METADATA_TYPE_SIZE_INVALID	0xFFFFFFFF

typedef uint32 MetadataTypeSize;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get size of type name
////////////////////////////////////////////////////////////
MetadataTypeSize MetadataTypeGetSize(MetadataTypeName TypeName);

#endif // METADATA_TYPE_H
