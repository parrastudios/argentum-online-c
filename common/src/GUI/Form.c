/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Window/Window.h>
#include <Graphics/TextureRender.h>

#include <GUI/Widget.h>

////////////////////////////////////////////////////////////
// Create a form
////////////////////////////////////////////////////////////
struct GuiWidget * GuiFormCreate(uint32 Style, int32 X, int32 Y, int32 Width, int32 Height, uint32 FontSize, char * Title)
{
	struct GuiWidget * Widget;

	// Create
	Widget = GuiWidgetCreate(GUI_WIDGET_FORM, X, Y, FontSize, Title);

	if (Widget)
	{
		// Set the size
		Widget->Width	= Width;
		Widget->Height	= Height;

		if (X == GUI_FORM_CENTERED)
			Widget->X = (WindowWidth / 2) - ((Width + 16) / 2);

		if (Y == GUI_FORM_CENTERED)
			Widget->Y = (WindowHeight / 2) - ((Height + 16) / 2);

		// Set the style
		Widget->Attributes.Form.Background = Style;

		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Create a form by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiFormCreateDefault()
{
	// Create
	struct GuiWidget * Widget = GuiWidgetCreateDefault(GUI_WIDGET_FORM);

	if (Widget)
	{
		// Set size and position
		Widget->Width	= 400;
		Widget->Height	= 400;
		Widget->X		= 16;
		Widget->Y		= 16;

		// Set the style
		Widget->Attributes.Form.Background = GUI_FORM_STYLE_1;

		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Render a form
////////////////////////////////////////////////////////////
void GuiFormRender(struct GuiWidget * Widget, struct Color4ub * BackgroundColor, struct Color4ub * FontColor)
{
    if (Widget->Attributes.Form.Background != 0)
    {
        // Render the background repeating it
        TextureRenderGrhColorRepeat(GUI_FORM_BACKGROUND + Widget->Attributes.Form.Background,
                                    Widget->X, Widget->Y,
                                    Widget->X + Widget->Width, Widget->Y + Widget->Height,
                                    BackgroundColor);
    }

    if (Widget->Border)
    {
        // Render the borders repeating it
        TextureRenderGrhColorRepeat(GUI_FORM_VBORDER,
                                    Widget->X - IndexGetGrh(GUI_FORM_VBORDER)->PixelWidth, Widget->Y,
                                    Widget->X, Widget->Y + Widget->Height,
                                    BackgroundColor);
        TextureRenderGrhColorRepeat(GUI_FORM_VBORDER + 0x01,
                                    Widget->X + Widget->Width, Widget->Y,
                                    Widget->X + Widget->Width + IndexGetGrh(GUI_FORM_VBORDER + 0x01)->PixelWidth, Widget->Y + Widget->Height,
                                    BackgroundColor);
        TextureRenderGrhColorRepeat(GUI_FORM_HBORDER,
                                    Widget->X, Widget->Y - IndexGetGrh(GUI_FORM_HBORDER)->PixelHeight,
                                    Widget->X + Widget->Width, Widget->Y,
                                    BackgroundColor);
        TextureRenderGrhColorRepeat(GUI_FORM_HBORDER + 0x01,
                                    Widget->X, Widget->Y + Widget->Height,
                                    Widget->X + Widget->Width, Widget->Y + Widget->Height + IndexGetGrh(GUI_FORM_HBORDER + 0x01)->PixelHeight,
                                    BackgroundColor);

        // Render the edges
        TextureRenderGrhColor(GUI_FORM_EDGE, Widget->X - IndexGetGrh(GUI_FORM_EDGE)->PixelWidth, Widget->Y - IndexGetGrh(GUI_FORM_EDGE)->PixelHeight, BackgroundColor);
        TextureRenderGrhColor(GUI_FORM_EDGE + 0x01, Widget->X + Widget->Width, Widget->Y - IndexGetGrh(GUI_FORM_EDGE + 0x01)->PixelHeight, BackgroundColor);
        TextureRenderGrhColor(GUI_FORM_EDGE + 0x02, Widget->X - IndexGetGrh(GUI_FORM_EDGE + 0x02)->PixelWidth, Widget->Y + Widget->Height, BackgroundColor);
        TextureRenderGrhColor(GUI_FORM_EDGE + 0x03, Widget->X + Widget->Width, Widget->Y + Widget->Height, BackgroundColor);
    }
}
