/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/Device.h>
#include <System/Error.h>

////////////////////////////////////////////////////////////
// Internal Data
////////////////////////////////////////////////////////////
HGLRC DeviceGLContext = NULL;	///< OpenGL rendering context associated to the HDC

////////////////////////////////////////////////////////////
/// Initialize current device (loads extensions and context)
////////////////////////////////////////////////////////////
void DeviceCreate(ContextType Target, struct VideoMode * Mode, struct WindowSettings * Params)
{
	int BestFormat, Color, Score;
	PIXELFORMATDESCRIPTOR ActualFormat;
	HGLRC CurrentContext;

    // Let's find a suitable pixel format -- first try with antialiasing
    BestFormat = 0;
    if (Params->AntialiasingLevel > 0)
    {
        // Define the basic attributes we want for our window
        int IntAttributes[] =
        {
            WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
		    WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		    WGL_ACCELERATION_ARB,   WGL_FULL_ACCELERATION_ARB,
		    WGL_DOUBLE_BUFFER_ARB,  GL_TRUE,
            WGL_SAMPLE_BUFFERS_ARB, (Params->AntialiasingLevel ? GL_TRUE : GL_FALSE),
		    WGL_SAMPLES_ARB,        Params->AntialiasingLevel,
		    0,                      0
        };

        // Let's check how many formats are supporting our requirements
        int   Formats[128];
	    UINT  NbFormats;
	    float FloatAttributes[] = {0, 0};
	    bool  IsValid = wglChoosePixelFormatARB(Target, IntAttributes, FloatAttributes, sizeof(Formats) / sizeof(*Formats), Formats, &NbFormats) != 0;
        if (!IsValid || (NbFormats == 0))
        {
            if (Params->AntialiasingLevel > 2)
            {
                // No format matching our needs : reduce the multisampling level
				MessageError("CreateContext", "Failed to find a pixel format supporting %u antialiasing levels ; trying with 2 levels.", Params->AntialiasingLevel);

                Params->AntialiasingLevel = IntAttributes[1] = 2;
	            IsValid = wglChoosePixelFormatARB(Target, IntAttributes, FloatAttributes, sizeof(Formats) / sizeof(*Formats), Formats, &NbFormats) != 0;
            }

            if (!IsValid || (NbFormats == 0))
            {
                // Cannot find any pixel format supporting multisampling ; disabling antialiasing
                MessageError("CreateContext", "Failed to find a pixel format supporting antialiasing ; antialiasing will be disabled.");
                Params->AntialiasingLevel = 0;
            }
        }

        // Get the best format among the returned ones
        if (IsValid && (NbFormats > 0))
        {
            int  BestScore = 0xFFFF;
            UINT i		   = 0;
            for (i = 0; i < NbFormats; ++i)
            {
                // Get the current format's attributes
                PIXELFORMATDESCRIPTOR Attribs;
                Attribs.nSize    = sizeof(PIXELFORMATDESCRIPTOR);
                Attribs.nVersion = 1;
                DescribePixelFormat(Target, Formats[i], sizeof(PIXELFORMATDESCRIPTOR), &Attribs);

                // Evaluate the current configuration
                Color = Attribs.cRedBits + Attribs.cGreenBits + Attribs.cBlueBits + Attribs.cAlphaBits;
                Score = SettingsEvaluateConfig(Mode, Params, Color,
											   Attribs.cDepthBits, Attribs.cStencilBits, Params->AntialiasingLevel);

                // Keep it if it's better than the current best
                if (Score < BestScore)
                {
                    BestScore  = Score;
                    BestFormat = Formats[i];
                }
            }
        }
    }

    // Find a pixel format with no antialiasing, if not needed or not supported
    if (BestFormat == 0)
    {
        // Setup a pixel format descriptor from the rendering settings
        PIXELFORMATDESCRIPTOR PixelDescriptor;
        ZeroMemory(&PixelDescriptor, sizeof(PIXELFORMATDESCRIPTOR));
        PixelDescriptor.nSize        = sizeof(PIXELFORMATDESCRIPTOR);
        PixelDescriptor.nVersion     = 1;
        PixelDescriptor.iLayerType   = PFD_MAIN_PLANE;
        PixelDescriptor.dwFlags      = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
        PixelDescriptor.iPixelType   = PFD_TYPE_RGBA;
        PixelDescriptor.cColorBits   = (BYTE)(Mode->BitsPerPixel);
        PixelDescriptor.cDepthBits   = (BYTE)(Params->DepthBits);
        PixelDescriptor.cStencilBits = (BYTE)(Params->StencilBits);

        // Get the pixel format that best matches our requirements
        BestFormat = ChoosePixelFormat(Target, &PixelDescriptor);
        if (BestFormat == 0)
        {
            MessageError("CreateContext", "Failed to find a suitable pixel format for device context -- cannot create OpenGL context.");
            return;
        }
    }

    // Extract the depth and stencil bits from the chosen format
    ActualFormat.nSize    = sizeof(PIXELFORMATDESCRIPTOR);
    ActualFormat.nVersion = 1;
    DescribePixelFormat(Target, BestFormat, sizeof(PIXELFORMATDESCRIPTOR), &ActualFormat);
    Params->DepthBits   = ActualFormat.cDepthBits;
    Params->StencilBits = ActualFormat.cStencilBits;

    // Set the chosen pixel format
    if (!SetPixelFormat(Target, BestFormat, &ActualFormat))
    {
        MessageError("CreateContext", "Failed to set pixel format for device context -- cannot create OpenGL context.");
        return;
    }

    // Create the OpenGL context from the device context
    DeviceGLContext = wglCreateContext(Target);
    if (DeviceGLContext == NULL)
    {
        MessageError("CreateContext", "Failed to create an OpenGL context for this window.");
        return;
    }

    // Share display lists with other contexts
    CurrentContext = wglGetCurrentContext();
    if (CurrentContext)
        wglShareLists(CurrentContext, DeviceGLContext);

    // Activate the context
    DeviceSetRenderTarget(Target);

    // Enable multisampling
    if (Params->AntialiasingLevel > 0)
        glEnable(GL_MULTISAMPLE_ARB);
}

////////////////////////////////////////////////////////////
/// Destroy current device (delete extensions and context)
////////////////////////////////////////////////////////////
void DeviceDestroy()
{
	wglDeleteContext(DeviceGLContext);
	DeviceGLContext = NULL;
}

////////////////////////////////////////////////////////////
/// Check if the current context is created
////////////////////////////////////////////////////////////
bool DeviceContextValid()
{
	return (DeviceGLContext != NULL);
}

////////////////////////////////////////////////////////////
/// Check if there's an active context on the current thread
////////////////////////////////////////////////////////////
bool DeviceContextActive()
{
    return (wglGetCurrentContext() != NULL);
}

////////////////////////////////////////////////////////////
/// Enable / disable vertical synchronization
////////////////////////////////////////////////////////////
void DeviceUseVerticalSync(bool Enabled)
{
    wglSwapIntervalEXT(Enabled ? 1 : 0);
}

////////////////////////////////////////////////////////////
/// Set the render context associated to window context
////////////////////////////////////////////////////////////
void DeviceSetRenderTarget(ContextType Target)
{
	if (Target != NULL)
	{
		if (wglGetCurrentContext() != DeviceGLContext)
		{
			wglMakeCurrent(Target, DeviceGLContext);
		}
	}
	else
	{
        if (wglGetCurrentContext() == DeviceGLContext)
            wglMakeCurrent(NULL, NULL);
	}
}
