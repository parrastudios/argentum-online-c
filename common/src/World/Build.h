/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_BUILD_H
#define WORLD_BUILD_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Config.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define WORLD_BUILD_ARCH_NONE			0x00
#define WORLD_BUILD_ARCH_INTERFACE		0x01
#define WORLD_BUILD_ARCH_CONTROL		0x02
#define WORLD_BUILD_ARCH_REPRESENTATION	0x03
#define WORLD_BUILD_ARCH_ALL			0x04

#ifdef APPLICATION_CLIENT
#	define WORLD_BUILD_ARCH				WORLD_BUILD_ARCH_REPRESENTATION
#elif defined(APPLICATION_SERVER)
#	define WORLD_BUILD_ARCH				WORLD_BUILD_ARCH_CONTROL
#elif defined(APPLICATION_SCENE_EDITOR)
#	define WORLD_BUILD_ARCH				WORLD_BUILD_ARCH_REPRESENTATION
#else
#	define WORLD_BUILD_ARCH				WORLD_BUILD_ARCH_NONE
#endif

#define WORLD_BUILD_ARCH_CHECK(Arch)	( Arch == WORLD_BUILD_ARCH )

#if WORLD_BUILD_ARCH_CHECK(WORLD_BUILD_ARCH_NONE)
#	define WORLD_BUILD_TYPE_NONE		1
#	define WORLD_BUILD_TYPE_BASE		0
#	define WORLD_BUILD_TYPE_TRANSFORM	0
#	define WORLD_BUILD_TYPE_COLLISION	0
#	define WORLD_BUILD_TYPE_PHYSICS		0
#	define WORLD_BUILD_TYPE_ANIMATION	0
#	define WORLD_BUILD_TYPE_RENDER		0
#	define WORLD_BUILD_TYPE_OCCLUSION	0
#	define WORLD_BUILD_TYPE_SOUND		0
#elif WORLD_BUILD_ARCH_CHECK(WORLD_BUILD_ARCH_INTERFACE)
#	define WORLD_BUILD_TYPE_NONE		0
#	define WORLD_BUILD_TYPE_BASE		1
#	define WORLD_BUILD_TYPE_TRANSFORM	0
#	define WORLD_BUILD_TYPE_COLLISION	0
#	define WORLD_BUILD_TYPE_PHYSICS		0
#	define WORLD_BUILD_TYPE_ANIMATION	0
#	define WORLD_BUILD_TYPE_RENDER		0
#	define WORLD_BUILD_TYPE_OCCLUSION	0
#	define WORLD_BUILD_TYPE_SOUND		0
#elif WORLD_BUILD_ARCH_CHECK(WORLD_BUILD_ARCH_CONTROL)
#	define WORLD_BUILD_TYPE_NONE		0
#	define WORLD_BUILD_TYPE_BASE		1
#	define WORLD_BUILD_TYPE_TRANSFORM	1
#	define WORLD_BUILD_TYPE_COLLISION	1
#	define WORLD_BUILD_TYPE_PHYSICS		1
#	define WORLD_BUILD_TYPE_ANIMATION	0
#	define WORLD_BUILD_TYPE_RENDER		0
#	define WORLD_BUILD_TYPE_OCCLUSION	0
#	define WORLD_BUILD_TYPE_SOUND		0
#elif WORLD_BUILD_ARCH_CHECK(WORLD_BUILD_ARCH_REPRESENTATION)
#	define WORLD_BUILD_TYPE_NONE		0
#	define WORLD_BUILD_TYPE_BASE		1
#	define WORLD_BUILD_TYPE_TRANSFORM	1
#	define WORLD_BUILD_TYPE_COLLISION	1
#	define WORLD_BUILD_TYPE_PHYSICS		1
#	define WORLD_BUILD_TYPE_ANIMATION	1
#	define WORLD_BUILD_TYPE_RENDER		1
#	define WORLD_BUILD_TYPE_OCCLUSION	1
#	define WORLD_BUILD_TYPE_SOUND		1
#elif WORLD_BUILD_ARCH_CHECK(WORLD_BUILD_ARCH_ALL)
#	define WORLD_BUILD_TYPE_NONE		0
#	define WORLD_BUILD_TYPE_BASE		1
#	define WORLD_BUILD_TYPE_TRANSFORM	1
#	define WORLD_BUILD_TYPE_COLLISION	1
#	define WORLD_BUILD_TYPE_PHYSICS		1
#	define WORLD_BUILD_TYPE_ANIMATION	1
#	define WORLD_BUILD_TYPE_RENDER		1
#	define WORLD_BUILD_TYPE_OCCLUSION	1
#	define WORLD_BUILD_TYPE_SOUND		1
#else
#	define WORLD_BUILD_TYPE_NONE		1
#	define WORLD_BUILD_TYPE_BASE		0
#	define WORLD_BUILD_TYPE_TRANSFORM	0
#	define WORLD_BUILD_TYPE_COLLISION	0
#	define WORLD_BUILD_TYPE_PHYSICS		0
#	define WORLD_BUILD_TYPE_ANIMATION	0
#	define WORLD_BUILD_TYPE_RENDER		0
#	define WORLD_BUILD_TYPE_OCCLUSION	0
#	define WORLD_BUILD_TYPE_SOUND		0
#endif

#define WORLD_BUILD_TYPE_CHECK(Type)	Type

#define WORLD_BUILD_TYPE_IS(Type) WORLD_BUILD_TYPE_CHECK(Type) // todo: remove

#endif // WORLD_BUILD_H
