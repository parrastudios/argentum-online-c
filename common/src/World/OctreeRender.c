/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/OctreeRender.h>
#include <World/EntityRender.h>
#include <Graphics/Device.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Render bounding box of a octree
////////////////////////////////////////////////////////////
void OctreeRenderBoxDebug(struct OctreeType * Octree)
{
	if (Octree)
	{
		struct Vector3f * Min = &Octree->Box->Bounds[BOUNDING_BOX_MIN];
		struct Vector3f * Max = &Octree->Box->Bounds[BOUNDING_BOX_MAX];

		glColor3f(1.0f, 0.0f, 0.0f);

		glLineWidth(2.0f);

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		// Z
		glBegin(GL_POLYGON);
			glNormal3f(0.0f, 0.0f, 1.0f);
			glVertex3f(Min->x, Min->y, Max->z);
			glNormal3f(0.0f, 0.0f, 1.0f);
			glVertex3f(Max->x, Min->y, Max->z);
			glNormal3f(0.0f, 0.0f, 1.0f);
			glVertex3f(Max->x, Max->y, Max->z);
			glNormal3f(0.0f, 0.0f, 1.0f);
			glVertex3f(Min->x, Max->y, Max->z);
		glEnd();

		// X
		glBegin(GL_POLYGON);
			glNormal3f(1.0f, 0.0f, 0.0f);
			glVertex3f(Max->x, Min->y, Max->z);
			glNormal3f(1.0f, 0.0f, 0.0f);
			glVertex3f(Max->x, Min->y, Min->z);
			glNormal3f(1.0f, 0.0f, 0.0f);
			glVertex3f(Max->x, Max->y, Min->z);
			glNormal3f(1.0f, 0.0f, 0.0f);
			glVertex3f(Max->x, Max->y, Max->z);
		glEnd();

		// Y
		glBegin(GL_POLYGON);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(Min->x, Max->y, Max->z);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(Max->x, Max->y, Max->z);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(Max->x, Max->y, Min->z);
			glNormal3f(0.0f, 1.0f, 0.0f);
			glVertex3f(Min->x, Max->y, Min->z);
		glEnd();

		// Negative Z
		glBegin(GL_POLYGON);
			glNormal3f(0.0f, 0.0f, -1.0f);
			glVertex3f(Min->x, Min->y, Min->z);
			glNormal3f(0.0f, 0.0f, -1.0f);
			glVertex3f(Min->x, Max->y, Min->z);
			glNormal3f(0.0f, 0.0f, -1.0f);
			glVertex3f(Max->x, Max->y, Min->z);
			glNormal3f(0.0f, 0.0f, -1.0f);
			glVertex3f(Max->x, Min->y, Min->z);
		glEnd();

		// Negative Y
		glBegin(GL_POLYGON);
			glNormal3f(0.0f, -1.0f, 0.0f);
			glVertex3f(Min->x, Min->y, Min->z);
			glNormal3f(0.0f, -1.0f, 0.0f);
			glVertex3f(Max->x, Min->y, Min->z);
			glNormal3f(0.0f, -1.0f, 0.0f);
			glVertex3f(Max->x, Min->y, Max->z);
			glNormal3f(0.0f, -1.0f, 0.0f);
			glVertex3f(Min->x, Min->y, Max->z);
		glEnd();

		// Negative X
		glBegin(GL_POLYGON);
			glNormal3f(-1.0f, 0.0f, 0.0f);
			glVertex3f(Min->x, Min->y, Min->z);
			glNormal3f(-1.0f, 0.0f, 0.0f);
			glVertex3f(Min->x, Min->y, Max->z);
			glNormal3f(-1.0f, 0.0f, 0.0f);
			glVertex3f(Min->x, Max->y, Max->z);
			glNormal3f(-1.0f, 0.0f, 0.0f);
			glVertex3f(Min->x, Max->y, Min->z);
		glEnd();
	}
}

////////////////////////////////////////////////////////////
/// Render octree from a specified camera point of view
////////////////////////////////////////////////////////////
void OctreeRender(struct OctreeType * Octree, struct FrustumType * Frustum)
{
	if (Octree && Frustum)
	{
		if (!OctreeHasChildren(Octree))
		{
			ListIterator Iterator;

			ListForEach(Octree->EntityIdRefList, Iterator)
			{
				struct EntityIdPairType * EntityPairRef = ListItDataPtr(Iterator, struct EntityIdPairType);

				struct EntityType * EntityRef = EntityGet(EntityPairRef->Type, EntityPairRef->Id);

				// TODO: Solve this
                //if (EntityCollisionComponentCollide(EntityRef, (struct ComponentCollisionData *)Frustum, COMPONENT_DATA_COLLISION_FRUSTUM))
				{
					EntityRender(EntityRef);
				}
			}

			//OctreeRenderBoxDebug(Octree);
		}
		else
		{
			uinteger Result = FrustumCheckBoundingBox(Frustum, Octree->Box, FRUSTUM_OVERLAP);

			//if (FrustumInside(Result) || FrustumOverlap(Result))
			{
				uinteger i;

				for (i = 0; i < OCTREE_CHILD_COUNT; ++i)
				{
					if (Octree->Child[i])
					{
						OctreeRender(Octree->Child[i], Frustum);
					}
				}
			}
		}
	}
}

