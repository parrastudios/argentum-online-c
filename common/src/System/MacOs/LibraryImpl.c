/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/MacOs/LibraryImpl.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
const char LibraryExtensionImplMacOs[0x08] = ".bundle";

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get the library extension for MacOs
////////////////////////////////////////////////////////////
const char * LibraryExtensionImpl()
{
	return LibraryExtensionImplMacOs;
}

////////////////////////////////////////////////////////////
/// Loads a library returning the handle to it in MacOs platform
////////////////////////////////////////////////////////////
bool LibraryLoadImpl(LibraryImplType * Impl, LibraryNameType Name, LibraryFlagsType Flags)
{
	CFStringRef LibraryNameString = CFStringCreateWithCString(kCFAllocatorDefault, Name, kCFStringEncodingASCII);

	Impl->BundleURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, LibraryNameString, kCFURLPOSIXPathStyle, true);

	Impl->Bundle = CFBundleCreate(kCFAllocatorDefault, Impl->BundleURL);

	CFRelease(LibraryNameString);

	return (bool)(Impl->Bundle != NULL);
}

////////////////////////////////////////////////////////////
/// Unloads a library by its handle in MacOs platform
////////////////////////////////////////////////////////////
bool LibraryUnloadImpl(LibraryImplType * Impl)
{
	CFRelease(Impl->Bundle);
	CFRelease(Impl->BundleURL);

	return true;
}

////////////////////////////////////////////////////////////
/// Get a symbol from library in MacOs platform
////////////////////////////////////////////////////////////
bool LibrarySymbolImpl(LibraryImplType * Impl, LibrarySymbolNameType Name, LibrarySymbolType * Address)
{
	CFStringRef SymbolString = CFStringCreateWithCString(kCFAllocatorDefault, Name, kCFStringEncodingASCII);

	*Address = CFBundleGetFunctionPointerForName(Impl->Bundle, SymbolString);
	
	CFRelease(SymbolString);

	return (*Address != NULL);
}
