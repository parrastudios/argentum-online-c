/*
***********************************************************************************
 IqeB_Tools_SkinBind.cpp

 Tools for skin binding

 06.04.2015 RR: First editon of this file.

*/

#include <math.h>

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/gl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

/************************************************************************************
 * IqeB_SkinBindReset()
 *
 * Reset all skin bindings.
 * This function also enshures that the data for skin binding
 * is allocated.
 */

void IqeB_SkinBindReset( IQE_model *pModel)
{
  // security tests
  if( pModel == NULL || pModel->mesh == NULL) {  // security test, need this pointers

    return;   // exit to caller
  }

  if( pModel->mesh->vertex_count <= 0) {   // minimum need one vertex

    goto ErrorExit;
  }

  allocArray( (void **)&pModel->mesh->blendindex,  pModel->mesh->vertex_count * 4, sizeof( int));
  allocArray( (void **)&pModel->mesh->blendweight, pModel->mesh->vertex_count * 4, sizeof( float));

  if( pModel->mesh->blendindex == NULL ||
      pModel->mesh->blendweight == NULL) {   // security test

    // not enough data, something not OK
    // free already allocated memory

    allocArray( (void **)&pModel->mesh->blendindex,  0, 0);
    allocArray( (void **)&pModel->mesh->blendweight, 0, 0);

    goto ErrorExit;
  }

  // clear blending data

  memset( pModel->mesh->blendindex,  0, pModel->mesh->vertex_count * 4 * sizeof( int));
  memset( pModel->mesh->blendweight, 0, pModel->mesh->vertex_count * 4 * sizeof( float));

	// done

	return;  // return to caller

//
// Error exit
//

ErrorExit:

  // Got error

  return;
}

/************************************************************************************
 * IqeB_SkinBindGetVertexDoBind()
 *
 * Flag vertex which should be skin binded.
 */

static void IqeB_SkinBindGetVertexDoBind( IQE_model *pModel, int AssignType, int **ppVertexBind)
{
  int i;

  *ppVertexBind = (int *)malloc( pModel->mesh->vertex_count * sizeof( int));  // allocated new memory

  if( *ppVertexBind == NULL) {   // security test

    return;
  }

  switch( AssignType) {
  default:
  case IQE_SKINBIND_ASSIGN_ALL_VERT:  // assign to all vertices

    // set all
    for( i = 0; i < pModel->mesh->vertex_count; i++) {
      (*ppVertexBind)[ i] = 1;
    }
    break;

  case IQE_SKINBIND_ASSIGN_SELECTED_MESH:   // assign to vertices of selected mesh

    if( IqeB_DispStyle_MeshPartSelected < 0 ||    // no mesh selected
        IqeB_DispStyle_MeshPartSelected >= pModel->mesh->part_count) {  // To big

      // set all
      for( i = 0; i < pModel->mesh->vertex_count; i++) {
        (*ppVertexBind)[ i] = 1;
      }

    } else {

      // reset all
      for( i = 0; i < pModel->mesh->vertex_count; i++) {
        (*ppVertexBind)[ i] = 0;
      }

      // walk all vertices for selectd mesh

      for( i = 0; i < pModel->mesh->part[ IqeB_DispStyle_MeshPartSelected].count; i += 3) {

        int i1, i2, i3;

        // indices of triangles

        i1 = pModel->mesh->element[ pModel->mesh->part[ IqeB_DispStyle_MeshPartSelected].first + i + 0];
        i2 = pModel->mesh->element[ pModel->mesh->part[ IqeB_DispStyle_MeshPartSelected].first + i + 1];
        i3 = pModel->mesh->element[ pModel->mesh->part[ IqeB_DispStyle_MeshPartSelected].first + i + 2];

        if( i1 >= 0 && i1 < pModel->mesh->vertex_count) {
          (*ppVertexBind)[ i1] = 1;
        }
        if( i2 >= 0 && i2 < pModel->mesh->vertex_count) {
          (*ppVertexBind)[ i2] = 1;
        }
        if( i3 >= 0 && i1 < pModel->mesh->vertex_count) {
          (*ppVertexBind)[ i3] = 1;
        }
      }
    }

    break;

  case IQE_SKINBIND_ASSIGN_UNASSIGNED_VERT: // assign to all unassigned verrtices

    {
      float *bw;

      // walk all vertices
  	  bw = pModel->mesh->blendweight;
      for( i = 0; i < pModel->mesh->vertex_count; i++, bw += 4) {
        (*ppVertexBind)[ i] = bw[ 0] == 0.0;     // First blend weight says 'no binding'
      }
    }
    break;

  } // end switch

}

/************************************************************************************
 * IqeB_SkinBindClear()
 *
 * Clear skin bindings.
 *
 * AssignTypeMeshSelect: can be:
 *                         IQE_SKINBIND_ASSIGN_ALL_VERT       // assign to all vertices
 *                         IQE_SKINBIND_ASSIGN_SELECTED_MESH  // assign to vertices of selected mesh
*
 */

void IqeB_SkinBindClear( IQE_model *pModel, int AssignTypeMeshSelect)
{
	struct mesh *mesh = pModel->mesh;
  int *pVertexBind = NULL;
  int iWeight, iVertex;

  // security tests
  if( pModel == NULL || pModel->mesh == NULL) {  // security test, need this pointers

    return;   // exit to caller
  }

  if( pModel->skel == NULL ||              // security test, need a skeleton
      pModel->skel->joint_count <= 0 ||    // minimum need one joint
      pModel->mesh->vertex_count <= 0) {   // minimum need one vertex

    goto ErrorExit;
  }

  // Enshure to have the blend data

  if( pModel->mesh->blendindex == NULL ||
      pModel->mesh->blendweight == NULL) {   // missing blend data

    IqeB_SkinBindReset( pModel);             // allocate and reset the blend data
  }

  if( pModel->mesh->blendindex == NULL ||
      pModel->mesh->blendweight == NULL) {   // security test, still missing blend data

    goto ErrorExit;
  }

  // get vertics to process

  if( AssignTypeMeshSelect != IQE_SKINBIND_ASSIGN_ALL_VERT &&       // security test, one of the
      AssignTypeMeshSelect != IQE_SKINBIND_ASSIGN_SELECTED_MESH) {  // supported modes

    AssignTypeMeshSelect = IQE_SKINBIND_ASSIGN_ALL_VERT;  // fall back to default
  }

  IqeB_SkinBindGetVertexDoBind( pModel, AssignTypeMeshSelect, &pVertexBind);
  if( pVertexBind == NULL) {  // security test

    goto ErrorExit;
  }

  // Clear the skin binding

	int *bi;
  float *pV, *bw;

  pV = mesh->position;
	bi = mesh->blendindex;
  bw = mesh->blendweight;

  for( iVertex = 0; iVertex < mesh->vertex_count; iVertex++, pV += 3, bi += 4, bw += 4) {

    if( pVertexBind[ iVertex] == 0) {   // Don't bind this vertex

      continue;
    }

    // reset all weights first

		for( iWeight = 0; iWeight < 4; iWeight++) {     // up to 4 blends per vertex

			bi[ iWeight] = 0;
			bw[ iWeight] = 0.0;   // end of list
		}
  }

	// done

  if( pVertexBind != NULL) {

    free( pVertexBind);
  }

	return;  // return to caller

//
// Error exit
//

ErrorExit:

  // Got error

  if( pVertexBind != NULL) {

    free( pVertexBind);
  }

  return;
}

/************************************************************************************
 * IqeB_SkinBindNearestJoint()
 *
 * Bind skin to nearest joint.
 */

void IqeB_SkinBindNearestJoint( IQE_model *pModel, int AssignType, int JointSel)
{
	struct skel *skel = pModel->skel;
	struct mesh *mesh = pModel->mesh;
  int *pVertexBind = NULL;
  int iWeight, iVertex, iJoint;

  // security tests
  if( pModel == NULL || pModel->mesh == NULL) {  // security test, need this pointers

    return;   // exit to caller
  }

  if( pModel->skel == NULL ||              // security test, need a skeleton
      pModel->skel->joint_count <= 0 ||    // minimum need one joint
      pModel->mesh->vertex_count <= 0) {   // minimum need one vertex

    goto ErrorExit;
  }

  // Enshure to have the blend data

  if( pModel->mesh->blendindex == NULL ||
      pModel->mesh->blendweight == NULL) {   // missing blend data

    IqeB_SkinBindReset( pModel);             // allocate and reset the blend data
  }

  if( pModel->mesh->blendindex == NULL ||
      pModel->mesh->blendweight == NULL) {   // security test, still missing blend data

    goto ErrorExit;
  }

  // get vertics to process

  IqeB_SkinBindGetVertexDoBind( pModel, AssignType, &pVertexBind);
  if( pVertexBind == NULL) {  // security test

    goto ErrorExit;
  }

  // Do the skin binding

	int *bi, iBest;
  float *pV, *bw, DistThis, DistBest;

  pV = mesh->position;
	bi = mesh->blendindex;
  bw = mesh->blendweight;

  for( iVertex = 0; iVertex < mesh->vertex_count; iVertex++, pV += 3, bi += 4, bw += 4) {

    if( pVertexBind[ iVertex] == 0) {   // Don't bind this vertex

      continue;
    }

    // reset all weights first

		for( iWeight = 0; iWeight < 4; iWeight++) {     // up to 4 blends per vertex

			bi[ iWeight] = 0;
			bw[ iWeight] = 0.0;   // end of list
		}

		// find nearest joint

		iBest = -1;       // None selected
		DistBest = 0.0;

  	for( iJoint = 0; iJoint < skel->joint_count; iJoint++) {

      if( JointSel == IQE_SKINBIND_JOINT_SELECTED && ! skel->j[ iJoint].isSelected) {  // Like selected joints only

        continue;
      }

      if( JointSel == IQE_SKINBIND_JOINT_NOT_SEL && skel->j[ iJoint].isSelected) {  // Like NOT selected joints only

        continue;
      }

      DistThis = vec_dist2( pV, mesh->abs_bind_matrix[ iJoint] + 12);  // Squareed distance of points

      if( iBest < 0 || DistThis < DistBest) {  // First one or neared

    		iBest = iJoint;
		    DistBest = DistThis;
      }
  	}

    if( iBest >= 0) {

  	  bi[ 0] = iBest;  // Nearest
		  bw[ 0] = 1.0;    // Have only one, so full weight
    }
  }

	// done

  if( pVertexBind != NULL) {

    free( pVertexBind);
  }

	return;  // return to caller

//
// Error exit
//

ErrorExit:

  // Got error

  if( pVertexBind != NULL) {

    free( pVertexBind);
  }

  return;
}

/************************************************************************************
 * IqeB_SkinBindBuildBoneInfo()
 *
 * Build bone info from skeleton joints.
 * A bone is the connection from a joint to it's parent.
 */

typedef struct {
  // Bone data
  vec3 BasePoint;     // Base point of a straight line (a joint position)
  vec3 Normal;        // Normal from joint to parent
  vec3 J2Point;       // Location of 2. point
  float J2Dist;       // Distance from 2. Joint to BasePoint. < 0: Invalid, == 0: is Point, > 0: is bone
  float BoneLength;   // Lenght of bone

  // Some data used in a second pass
  float AvgDist;      // Average vertex distance of joints to this bone
  int nDistances;     // Count for average distances
} TBoneInfo;

typedef struct {

  int nBones;                      // Number of bones Connected joints
  float BoneLengthMax;             // Max bone length
  float BoneLengthAvg;             // Average bone length
  float BoneLengthThres;           // A Threshold do skip vertices depenting from bonelength statistics

  TBoneInfo BoneInfo[ MAXBONE];    // Bone Info

} TBoneInfoData;

// Build bone info from a skeleton

static void IqeB_SkinBindBuildBoneInfo( struct mesh *mesh, struct skel *skel, TBoneInfoData *pBoneInfoData)
{
  int iJoint, iParent;
  TBoneInfo *pBoneInfo;

  memset( pBoneInfoData, 0, sizeof( TBoneInfoData));  // Zero the BoneInfoData table

  pBoneInfo = pBoneInfoData->BoneInfo;                // pointer to bone table

  // ...

  pBoneInfoData->nBones = 0;

 	for( iJoint = 0; iJoint < skel->joint_count; iJoint++, pBoneInfo++) {

    vec_copy( pBoneInfo->BasePoint, mesh->abs_bind_matrix[ iJoint] + 12);  // 1. Joint position

    iParent = skel->j[ iJoint].parent;

    if( iParent < 0) {      // is a root joint

      vec_copy( pBoneInfo->J2Point, mesh->abs_bind_matrix[ iJoint] + 12);    // 2. Joint position
      vec_set( pBoneInfo->Normal, 0.0, 0.0, 1.0);  // a dummy normal

      if( skel->j[ iJoint].nChilds > 0) {     // have any childs

        // Hase childs, do nothing
        pBoneInfo->J2Dist = -1.0;                     // Don't test this, mark with distance of -1.0
        pBoneInfo->BoneLength = 0.0;

      } else {

        // This joint has no childs and is alone in the world, --> test as point

        pBoneInfo->J2Dist = 0.0;                     // No 2. joint, set distance of 0
        pBoneInfo->BoneLength = 0.0;
      }

    } else {

      // A Bone between this point and the parent point

      vec_copy( pBoneInfo->J2Point, mesh->abs_bind_matrix[ iParent] + 12);   // 2. Joint position
      vec_sub( pBoneInfo->Normal, pBoneInfo->J2Point, pBoneInfo->BasePoint);
      pBoneInfo->BoneLength = vec_normalize( pBoneInfo->Normal);

      // Distance 2. joint point from first
      PointDistNearestToLineBasePoint( pBoneInfo->BasePoint, pBoneInfo->Normal,
                                       pBoneInfo->J2Point, &pBoneInfo->J2Dist);

      if( pBoneInfo->J2Dist < 0.0) {   // is negative

        // Want a postive 'J2Dist', so simply negate the vector.
        vec_negate( pBoneInfo->Normal);

        // and negate the distance
        pBoneInfo->J2Dist = - pBoneInfo->J2Dist;
      }

      // statistics about bone length

      if( pBoneInfoData->nBones == 0) {

        pBoneInfoData->BoneLengthMax = pBoneInfo->BoneLength;
        pBoneInfoData->BoneLengthAvg = pBoneInfo->BoneLength;
      } else {

        if( pBoneInfo->BoneLength > pBoneInfoData->BoneLengthMax) {

          pBoneInfoData->BoneLengthMax = pBoneInfo->BoneLength;
        }

        pBoneInfoData->BoneLengthAvg += pBoneInfo->BoneLength;
      }

      pBoneInfoData->nBones += 1;      // one more
    }
 	}

 	if( pBoneInfoData->nBones > 1) {     // make sense to divice

    pBoneInfoData->BoneLengthAvg = pBoneInfoData->BoneLengthAvg / pBoneInfoData->nBones;
 	}

 	// A Threshold do skip vertices depenting from bonelength statistics
#ifdef use_again
 	pBoneInfoData->BoneLengthThres = pBoneInfoData->BoneLengthAvg * 0.5 + pBoneInfoData->BoneLengthMax * 0.5;
#else
 	pBoneInfoData->BoneLengthThres = pBoneInfoData->BoneLengthAvg * 3.0;
#endif
}

/************************************************************************************
 * IqeB_SkinBindNearestBone()
 *
 * Bind skin to nearest bone.
 * A bone is the line between a joint and it's parent.
 * A root joint has a zero length.
 */

void IqeB_SkinBindNearestBone( IQE_model *pModel, int AssignType, int JointSel)
{
	struct skel *skel = pModel->skel;
	struct mesh *mesh = pModel->mesh;
  int *pVertexBind = NULL;
  int iWeight, iVertex, iJoint, iParent, iThis;
  TBoneInfoData BoneInfoData;    // Bone Info data
  TBoneInfo *pBoneInfo;

  // security tests
  if( pModel == NULL || pModel->mesh == NULL) {  // security test, need this pointers

    return;   // exit to caller
  }

  if( pModel->skel == NULL ||              // security test, need a skeleton
      pModel->skel->joint_count <= 0 ||    // minimum need one joint
      pModel->mesh->vertex_count <= 0) {   // minimum need one vertex

    goto ErrorExit;
  }

  // Enshure to have the blend data

  if( pModel->mesh->blendindex == NULL ||
      pModel->mesh->blendweight == NULL) {   // missing blend data

    IqeB_SkinBindReset( pModel);             // allocate and reset the blend data
  }

  if( pModel->mesh->blendindex == NULL ||
      pModel->mesh->blendweight == NULL) {   // security test, still missing blend data

    goto ErrorExit;
  }

  // get vertics to process

  IqeB_SkinBindGetVertexDoBind( pModel, AssignType, &pVertexBind);
  if( pVertexBind == NULL) {  // security test

    goto ErrorExit;
  }

  // Build the bone info

  IqeB_SkinBindBuildBoneInfo( mesh, skel, &BoneInfoData);

  // Do the skin binding

	int *bi, iBest;
  float *pV, *bw, DistThis, DistBest, BasePointOnLine;

  pV = mesh->position;
	bi = mesh->blendindex;
  bw = mesh->blendweight;

  for( iVertex = 0; iVertex < mesh->vertex_count; iVertex++, pV += 3, bi += 4, bw += 4) {

    if( pVertexBind[ iVertex] == 0) {   // Don't bind this vertex

      continue;
    }

    // reset all weights first

		for( iWeight = 0; iWeight < 4; iWeight++) {     // up to 4 blends per vertex

			bi[ iWeight] = 0;
			bw[ iWeight] = 0.0;   // end of list
		}

		// find nearest bone

		iBest = -1;       // None selected
		DistBest = 0.0;

    pBoneInfo = BoneInfoData.BoneInfo;

  	for( iJoint = 0; iJoint < skel->joint_count; iJoint++, pBoneInfo++) {

      if( JointSel == IQE_SKINBIND_JOINT_SELECTED && ! skel->j[ iJoint].isSelected) {  // Like selected joints only

        continue;
      }

      if( JointSel == IQE_SKINBIND_JOINT_NOT_SEL && skel->j[ iJoint].isSelected) {  // Like NOT selected joints only

        continue;
      }

      if( pBoneInfo->J2Dist < 0.0) {          // Is a root joint with childs

        // No need to test this
        continue;

      } else if( pBoneInfo->J2Dist == 0.0) {  // Is a root joint without childs

        DistThis = vec_dist( pBoneInfo->BasePoint, pV);
        iThis = iJoint;

      } else {  // Is a bone

        // Test distance vertex point from bone straight line

        DistThis = PointDistNearestToLineBasePoint( pBoneInfo->BasePoint, pBoneInfo->Normal,
                                                    pV, &BasePointOnLine);

        iParent = skel->j[ iJoint].parent;
        if( iParent < 0) {      // is a root joint

    		  iThis = iJoint;       // Use this joint
        } else {

    		  iThis = iParent;      // Use parent
        }

        if( BasePointOnLine < 0.0) {

          DistThis = vec_dist( pBoneInfo->BasePoint, pV);

    		  iThis = iJoint;       // Use this joint
        } else if( BasePointOnLine > pBoneInfo->J2Dist) {

          DistThis = vec_dist( pBoneInfo->J2Point, pV);
        }
      }

      if( iBest < 0 || DistThis < DistBest) {  // First one or neared

        iBest = iThis;        // Our joint
		    DistBest = DistThis;
      }
  	}

    if( iBest >= 0) {

  	  bi[ 0] = iBest;  // Nearest
		  bw[ 0] = 1.0;    // Have only one, so full weight
    }
  }

	// done

  if( pVertexBind != NULL) {

    free( pVertexBind);
  }

	return;  // return to caller

//
// Error exit
//

ErrorExit:

  // Got error

  if( pVertexBind != NULL) {

    free( pVertexBind);
  }

  return;
}

/************************************************************************************
 * IqeB_SkinBindToBones()
 *
 * Bind skin to bones.
 * Bind skin to one or more bones.
 */

typedef struct {
  float Distance;     // Distance from Vertex
  int   iJoint;       // to this joint (bone)
} TVertexToBoneWeight;

// help to sort TVertexToBoneWeight by joint number

static int IqeB_VertexToBoneWeight_compare_iJoint( const void *arg1, const void *arg2 )
{
  TVertexToBoneWeight *p1, *p2;

  // sort accorting pointedness
  // highest values first

  p1 = (TVertexToBoneWeight *)arg1;
  p2 = (TVertexToBoneWeight *)arg2;

  if( p1->iJoint == p2->iJoint) {  // is equal

    return( 0);
  }

  if( p1->iJoint < p2->iJoint) {   // is less than

    return( -1);
  }

  return( 1);   // must be greater than
}

static int IqeB_VertexToBoneWeight_compare_Distance( const void *arg1, const void *arg2 )
{
  TVertexToBoneWeight *p1, *p2;

  // sort accorting pointedness
  // highest values first

  p1 = (TVertexToBoneWeight *)arg1;
  p2 = (TVertexToBoneWeight *)arg2;

  if( p1->Distance == p2->Distance) {  // is equal

    return( 0);
  }

  if( p1->Distance < p2->Distance) {   // is less than

    return( -1);
  }

  return( 1);   // must be greater than
}

void IqeB_SkinBindToBones( IQE_model *pModel, int AssignType, int JointSel)
{
	struct skel *skel = pModel->skel;
	struct mesh *mesh = pModel->mesh;
  int *pVertexBind = NULL;
  int iWeight, iVertex, iJoint, iParent, iThis, nVertToBoneWeights;
  TBoneInfoData BoneInfoData;    // Bone Info data
  TBoneInfo *pBoneInfo;
  TVertexToBoneWeight VertToBoneWeights[ MAXBONE];
	int *bi, iBest;
  float *pV, *bw, DistThis, DistBest, BasePointOnLine;


  // security tests
  if( pModel == NULL || pModel->mesh == NULL) {  // security test, need this pointers

    return;   // exit to caller
  }

  if( pModel->skel == NULL ||              // security test, need a skeleton
      pModel->skel->joint_count <= 0 ||    // minimum need one joint
      pModel->mesh->vertex_count <= 0) {   // minimum need one vertex

    goto ErrorExit;
  }

  // Enshure to have the blend data

  if( pModel->mesh->blendindex == NULL ||
      pModel->mesh->blendweight == NULL) {   // missing blend data

    IqeB_SkinBindReset( pModel);             // allocate and reset the blend data
  }

  if( pModel->mesh->blendindex == NULL ||
      pModel->mesh->blendweight == NULL) {   // security test, still missing blend data

    goto ErrorExit;
  }

  // get vertics to process

  IqeB_SkinBindGetVertexDoBind( pModel, AssignType, &pVertexBind);
  if( pVertexBind == NULL) {  // security test

    goto ErrorExit;
  }

  // Build the bone info

  IqeB_SkinBindBuildBoneInfo( mesh, skel, &BoneInfoData);

  // Get average vertex distances to joints
  // Only the distances of a vertex nearest to this joint are averaged.
  // NOTE: IqeB_SkinBindBuildBoneInfo() alreada have zeroed all average data.

  pV = mesh->position;

  for( iVertex = 0; iVertex < mesh->vertex_count; iVertex++, pV += 3) {

    if( pVertexBind[ iVertex] == 0) {   // Don't bind this vertex

      continue;
    }

		// find nearest bone

		iBest = -1;       // None selected
		DistBest = 0.0;

    pBoneInfo = BoneInfoData.BoneInfo;

  	for( iJoint = 0; iJoint < skel->joint_count; iJoint++, pBoneInfo++) {

      if( JointSel == IQE_SKINBIND_JOINT_SELECTED && ! skel->j[ iJoint].isSelected) {  // Like selected joints only

        continue;
      }

      if( JointSel == IQE_SKINBIND_JOINT_NOT_SEL && skel->j[ iJoint].isSelected) {  // Like NOT selected joints only

        continue;
      }

      if( pBoneInfo->J2Dist < 0.0) {          // Is a root joint with childs

        // No need to test this
        continue;

      } else if( pBoneInfo->J2Dist == 0.0) {  // Is a root joint without childs

        DistThis = vec_dist( pBoneInfo->BasePoint, pV);
        iThis = iJoint;

      } else {  // Is a bone

        // Test distance vertex point from bone straight line

        DistThis = PointDistNearestToLineBasePoint( pBoneInfo->BasePoint, pBoneInfo->Normal,
                                                    pV, &BasePointOnLine);

        iParent = skel->j[ iJoint].parent;
        if( iParent < 0) {      // is a root joint

    		  iThis = iJoint;       // Use this joint
        } else {

    		  iThis = iParent;      // Use parent
        }

        if( BasePointOnLine < 0.0) {


          DistThis = vec_dist( pBoneInfo->BasePoint, pV);

    		  iThis = iJoint;       // Use this joint
        } else if( BasePointOnLine > pBoneInfo->J2Dist) {

          DistThis = vec_dist( pBoneInfo->J2Point, pV);
        }
      }

      if( iBest < 0 || DistThis < DistBest) {  // First one or neared

        iBest = iThis;        // Our joint
		    DistBest = DistThis;
      }
  	}

    if( iBest >= 0) {

      BoneInfoData.BoneInfo[ iBest].AvgDist    += DistBest;   // Sum up best
      BoneInfoData.BoneInfo[ iBest].nDistances += 1;          // One more
    }
  }

  // Average the bone distances

  pBoneInfo = BoneInfoData.BoneInfo;

  for( iJoint = 0; iJoint < skel->joint_count; iJoint++, pBoneInfo++) {

    if( pBoneInfo->nDistances > 1) {  // more than one

      pBoneInfo->AvgDist = pBoneInfo->AvgDist / pBoneInfo->nDistances;
    }
  }

  // Do the skin binding

  pV = mesh->position;
	bi = mesh->blendindex;
  bw = mesh->blendweight;

  for( iVertex = 0; iVertex < mesh->vertex_count; iVertex++, pV += 3, bi += 4, bw += 4) {

    if( pVertexBind[ iVertex] == 0) {   // Don't bind this vertex

      continue;
    }

    // reset all weights first

		for( iWeight = 0; iWeight < 4; iWeight++) {     // up to 4 blends per vertex

			bi[ iWeight] = 0;
			bw[ iWeight] = 0.0;   // end of list
		}

		// Get weights for all bones, also idendify the neares bone

		nVertToBoneWeights = 0;

    pBoneInfo = BoneInfoData.BoneInfo;

  	for( iJoint = 0; iJoint < skel->joint_count; iJoint++, pBoneInfo++) {

      if( JointSel == IQE_SKINBIND_JOINT_SELECTED && ! skel->j[ iJoint].isSelected) {  // Like selected joints only

        continue;
      }

      if( JointSel == IQE_SKINBIND_JOINT_NOT_SEL && skel->j[ iJoint].isSelected) {  // Like NOT selected joints only

        continue;
      }

      if( pBoneInfo->J2Dist < 0.0) {          // Is a root joint with childs

        // No need to test this
        continue;

      } else if( pBoneInfo->J2Dist == 0.0) {  // Is a root joint without childs

        DistThis = vec_dist( pBoneInfo->BasePoint, pV);
        iThis = iJoint;

      } else {  // Is a bone

        // Test distance vertex point from bone straight line

        DistThis = PointDistNearestToLineBasePoint( pBoneInfo->BasePoint, pBoneInfo->Normal,
                                                    pV, &BasePointOnLine);

        iParent = skel->j[ iJoint].parent;
        if( iParent < 0) {      // is a root joint

    		  iThis = iJoint;       // Use this joint
        } else {

    		  iThis = iParent;      // Use parent
        }

        if( BasePointOnLine < 0.0) {

          DistThis = vec_dist( pBoneInfo->BasePoint, pV);

    		  iThis = iJoint;       // Use this joint
        } else if( BasePointOnLine > pBoneInfo->J2Dist) {

          DistThis = vec_dist( pBoneInfo->J2Point, pV);
        }
      }

      VertToBoneWeights[ nVertToBoneWeights].iJoint   = iThis;
      VertToBoneWeights[ nVertToBoneWeights].Distance = DistThis;
      nVertToBoneWeights += 1;  // One more
  	}

  	// Eliminate multiple joint entires

  	if( nVertToBoneWeights > 1) {    // Two or more

      qsort( VertToBoneWeights, nVertToBoneWeights, sizeof( TVertexToBoneWeight),
             IqeB_VertexToBoneWeight_compare_iJoint);

      int iSrc, iDst;

      // We have minimum one entry, so can start with second

      iDst = 1;

      for( iSrc = 1; iSrc < nVertToBoneWeights; iSrc++) {

        if( VertToBoneWeights[ iSrc].iJoint == VertToBoneWeights[ iDst - 1].iJoint) {  // New one is same as old one

          // Keep the shortest distance

          if( VertToBoneWeights[ iDst - 1].Distance > VertToBoneWeights[ iSrc].Distance) {

            VertToBoneWeights[ iDst - 1].Distance = VertToBoneWeights[ iSrc].Distance;
          }
        } else {   // New one and old one are different

          if( iSrc != iDst) {  // Must copy down

            VertToBoneWeights[ iDst].iJoint   = VertToBoneWeights[ iSrc].iJoint;
            VertToBoneWeights[ iDst].Distance = VertToBoneWeights[ iSrc].Distance;
          }

          iDst += 1;   // One more
        }
      }

    	nVertToBoneWeights = iDst; // Updated bone count
  	}

  	// Sort entries by distance

  	if( nVertToBoneWeights > 1) {    // Two or more

      qsort( VertToBoneWeights, nVertToBoneWeights, sizeof( TVertexToBoneWeight),
             IqeB_VertexToBoneWeight_compare_Distance);
  	}

		// Set weights

  	if( nVertToBoneWeights > 0) {    // One or more

      iBest = VertToBoneWeights[ 0].iJoint;    // This is the nearest joint/bone

      // Get distance threshold

      DistThis = BoneInfoData.BoneInfo[ 0].AvgDist;         // Average dist of nearest joint/bone

      DistThis *= 2.0;                         // Our Magic tune factor
      if( DistThis <= 0.0) {                   // Security test, don't like division by zero
        DistThis = 1.0;
      }

      // To distance thresholds

      for( iWeight = 1; iWeight < nVertToBoneWeights; iWeight++) {

        if( VertToBoneWeights[ iWeight].Distance > DistThis) {  // Over threshold

          nVertToBoneWeights = iWeight;                         // Set this as end of list
          break;
        }
      }

      // Don't need more than 4 weights

      if( nVertToBoneWeights > 4) {   // More than 4

        nVertToBoneWeights = 4;       // Simply clip it
      }

      // Fit weights

      float WeightsSum;

      WeightsSum = 0.0;

      for( iWeight = 0; iWeight < nVertToBoneWeights; iWeight++) {

        VertToBoneWeights[ iWeight].Distance *= VertToBoneWeights[ iWeight].Distance;  // Square it

        WeightsSum += VertToBoneWeights[ iWeight].Distance;
      }

      // And set the weights

      for( iWeight = 0; iWeight < nVertToBoneWeights; iWeight++) {

  	    bi[ iWeight] = VertToBoneWeights[ iWeight].iJoint;                 // Joint
		    bw[ iWeight] = VertToBoneWeights[ iWeight].Distance / WeightsSum;  // Weight
  	  }
    }
  }

	// done

  if( pVertexBind != NULL) {

    free( pVertexBind);
  }

	return;  // return to caller

//
// Error exit
//

ErrorExit:

  // Got error

  if( pVertexBind != NULL) {

    free( pVertexBind);
  }

  return;
}

/************************************************************************************
 * Tools for head diffusion algorithms
 *
 */

typedef struct {       // Weight data used for voxel space
  short iJoint[ 4];    // Associated joint index, first is -1 if outside model
  float Weight[ 4];    // Weights per joint
  int   NeighbourBits; // A bit set for each neighbour, first bit is for center cell itself
} TVoxelWeight;

typedef struct {       // Voxel offset to direct neighbours
  int   Ox, Oy, Oz;    // Offsets to the neighbour
  int   Offset;        // Relative offset in voxel coordinate space
  int   NeighbourBit;  // Bit for this neighbour
} TVoxelOffset;

typedef struct {       // Weight data used for weight accumulation
  int   iJoint;        // Associated joint index
  float Weight;        // Weight sum for this joint.
} TJointWeight;

// ----------------------------------------------------------------------------
// Sort TJointWeight data by weight
static int IqeB_JointWeight_compare_Weight( const void *arg1, const void *arg2 )
{
  TJointWeight *p1, *p2;

  // sort accorting pointedness
  // highest values first

  p1 = (TJointWeight *)arg1;
  p2 = (TJointWeight *)arg2;

  if( p1->Weight == p2->Weight) {  // is equal

    return( 0);
  }

  if( p1->Weight > p2->Weight) {   // is bogger than

    return( -1);
  }

  return( 1);   // must be less than
}

// ----------------------------------------------------------------------------
// Setup neighbour data of voxel cell and voxel weights
static void IqeB_VoxWeightSetupNeighbours( T_VoxelModelData *pVoxelData, TVoxelOffset *pVoxOffCube, int *pnVoxOfCube,
                                           TVoxelWeight *pWDst)
{
  int VoxelAmount, iVertex, iVoxOfCube, ix, iy, iz;
  int  VoxOffset, VoxOff2;
  VOX_CEL_TYPE *pVoxels;
  TVoxelWeight *pVW1;

  VoxelAmount = pVoxelData->Size[ 0] * pVoxelData->Size[ 1] * pVoxelData->Size[ 2];  // multiply it

  //
  // Transfer the inside, outside to voxel weight data
  //

  memset( pWDst, 0, VoxelAmount * sizeof( TVoxelWeight));  // Zero data first

  pVoxels = pVoxelData->pVoxels;  // point to voxel data
  pVW1    = pWDst;                // point to voxel weight data

  for( iVertex = 0; iVertex < VoxelAmount; iVertex++, pVoxels++, pVW1++) {

    if( *pVoxels != 0) {       // Inside model

      pVW1->NeighbourBits = 1; // This voxel cell is inside the volume
    }
  }

  //
  // Prebare offset table of all voxel neighbours
  //

  *pnVoxOfCube = 0;

  // center
  pVoxOffCube[ *pnVoxOfCube].Ox =  0; pVoxOffCube[ *pnVoxOfCube].Oy =  0; pVoxOffCube[ *pnVoxOfCube].Oz =  0;
  *pnVoxOfCube += 1;

  // direct neighbours
  pVoxOffCube[ *pnVoxOfCube].Ox =  1; pVoxOffCube[ *pnVoxOfCube].Oy =  0; pVoxOffCube[ *pnVoxOfCube].Oz =  0;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox = -1; pVoxOffCube[ *pnVoxOfCube].Oy =  0; pVoxOffCube[ *pnVoxOfCube].Oz =  0;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  0; pVoxOffCube[ *pnVoxOfCube].Oy =  1; pVoxOffCube[ *pnVoxOfCube].Oz =  0;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  0; pVoxOffCube[ *pnVoxOfCube].Oy = -1; pVoxOffCube[ *pnVoxOfCube].Oz =  0;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  0; pVoxOffCube[ *pnVoxOfCube].Oy =  0; pVoxOffCube[ *pnVoxOfCube].Oz =  1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  0; pVoxOffCube[ *pnVoxOfCube].Oy =  0; pVoxOffCube[ *pnVoxOfCube].Oz = -1;
  *pnVoxOfCube += 1;

  // other neighbours, edges
#ifdef use_again
  pVoxOffCube[ *pnVoxOfCube].Ox =  0; pVoxOffCube[ *pnVoxOfCube].Oy =  1; pVoxOffCube[ *pnVoxOfCube].Oz =  1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  0; pVoxOffCube[ *pnVoxOfCube].Oy =  1; pVoxOffCube[ *pnVoxOfCube].Oz = -1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  0; pVoxOffCube[ *pnVoxOfCube].Oy = -1; pVoxOffCube[ *pnVoxOfCube].Oz =  1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  0; pVoxOffCube[ *pnVoxOfCube].Oy = -1; pVoxOffCube[ *pnVoxOfCube].Oz = -1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  1; pVoxOffCube[ *pnVoxOfCube].Oy =  0; pVoxOffCube[ *pnVoxOfCube].Oz =  1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  1; pVoxOffCube[ *pnVoxOfCube].Oy =  0; pVoxOffCube[ *pnVoxOfCube].Oz = -1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox = -1; pVoxOffCube[ *pnVoxOfCube].Oy =  0; pVoxOffCube[ *pnVoxOfCube].Oz =  1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox = -1; pVoxOffCube[ *pnVoxOfCube].Oy =  0; pVoxOffCube[ *pnVoxOfCube].Oz = -1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  1; pVoxOffCube[ *pnVoxOfCube].Oy =  1; pVoxOffCube[ *pnVoxOfCube].Oz =  0;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  1; pVoxOffCube[ *pnVoxOfCube].Oy = -1; pVoxOffCube[ *pnVoxOfCube].Oz =  0;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox = -1; pVoxOffCube[ *pnVoxOfCube].Oy =  1; pVoxOffCube[ *pnVoxOfCube].Oz =  0;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox = -1; pVoxOffCube[ *pnVoxOfCube].Oy = -1; pVoxOffCube[ *pnVoxOfCube].Oz =  0;
  *pnVoxOfCube += 1;

  // other neighbours, corners
  pVoxOffCube[ *pnVoxOfCube].Ox =  1; pVoxOffCube[ *pnVoxOfCube].Oy =  1; pVoxOffCube[ *pnVoxOfCube].Oz =  1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  1; pVoxOffCube[ *pnVoxOfCube].Oy = -1; pVoxOffCube[ *pnVoxOfCube].Oz =  1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox = -1; pVoxOffCube[ *pnVoxOfCube].Oy =  1; pVoxOffCube[ *pnVoxOfCube].Oz =  1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox = -1; pVoxOffCube[ *pnVoxOfCube].Oy = -1; pVoxOffCube[ *pnVoxOfCube].Oz =  1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  1; pVoxOffCube[ *pnVoxOfCube].Oy =  1; pVoxOffCube[ *pnVoxOfCube].Oz = -1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox =  1; pVoxOffCube[ *pnVoxOfCube].Oy = -1; pVoxOffCube[ *pnVoxOfCube].Oz = -1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox = -1; pVoxOffCube[ *pnVoxOfCube].Oy =  1; pVoxOffCube[ *pnVoxOfCube].Oz = -1;
  *pnVoxOfCube += 1;

  pVoxOffCube[ *pnVoxOfCube].Ox = -1; pVoxOffCube[ *pnVoxOfCube].Oy = -1; pVoxOffCube[ *pnVoxOfCube].Oz = -1;
  *pnVoxOfCube += 1;
#endif

  for( iVoxOfCube = 0; iVoxOfCube < *pnVoxOfCube; iVoxOfCube++) {

    // Calculate relative offset, see also IqeB_VoxelCoordVoxelInt2Offset()

    pVoxOffCube[ iVoxOfCube].Offset =  pVoxOffCube[ iVoxOfCube].Ox * pVoxelData->Size[ 1] * pVoxelData->Size[ 2];
    pVoxOffCube[ iVoxOfCube].Offset += pVoxOffCube[ iVoxOfCube].Oy * pVoxelData->Size[ 2];
    pVoxOffCube[ iVoxOfCube].Offset += pVoxOffCube[ iVoxOfCube].Oz;

    pVoxOffCube[ iVoxOfCube].NeighbourBit = 1 << iVoxOfCube;    // Bit for this entry
  }

  // Which voxel cell has a neighbour on the inside of the volume, test neighbours

  pVW1 = pWDst;      // point to voxel weight data

  for( ix = 0; ix < pVoxelData->Size[ 0]; ix++) {
    for( iy = 0; iy < pVoxelData->Size[ 1]; iy++) {
      for( iz = 0; iz < pVoxelData->Size[ 2]; iz++) {

        VoxOffset = IqeB_VoxelCoordVoxelInt2Offset( pVoxelData, ix, iy, iz);

        if( pVW1[ VoxOffset].NeighbourBits == 0) {     // This cell is outside, no need to test for neigbours

          continue;
        }

        for( iVoxOfCube = 0; iVoxOfCube < *pnVoxOfCube; iVoxOfCube++) {

          int ix2, iy2, iz2;

          ix2 = ix + pVoxOffCube[ iVoxOfCube].Ox;
          iy2 = iy + pVoxOffCube[ iVoxOfCube].Oy;
          iz2 = iz + pVoxOffCube[ iVoxOfCube].Oz;

          if( ix2 < 0 || ix2 >= pVoxelData->Size[ 0] ||
              iy2 < 0 || iy2 >= pVoxelData->Size[ 1] ||
              iz2 < 0 || iz2 >= pVoxelData->Size[ 2]) {   // compined coordinate is outside volume

            continue;
          }

          VoxOff2 = VoxOffset + pVoxOffCube[ iVoxOfCube].Offset;

          if( pVW1[ VoxOff2].NeighbourBits == 0) {     // This is outside

            continue;
          }

          pVW1[ VoxOffset].NeighbourBits |= pVoxOffCube[ iVoxOfCube].NeighbourBit; // have this as neighbour
        }
      }
    }
  }
}

// ----------------------------------------------------------------------------
// Fill an unfilled voxel cell
static int IqeB_VoxWeightCopy2Neighbours( T_VoxelModelData *pVoxelData, TVoxelOffset *pVoxOffCube, int nVoxOfCube,
                                           TVoxelWeight *pWDst, TVoxelWeight *pWSrc)
{
  int ix, iy, iz;
  int nJLinks, iJLink, VoxOffset, VoxOff2, iVoxOfCube, iWeight, AnySet;
  TVoxelWeight *pVW1;
  TJointWeight JointWeights[ MAXBONE];

  AnySet = false;

  for( ix = 0; ix < pVoxelData->Size[ 0]; ix++) {
    for( iy = 0; iy < pVoxelData->Size[ 1]; iy++) {
      for( iz = 0; iz < pVoxelData->Size[ 2]; iz++) {

        VoxOffset = IqeB_VoxelCoordVoxelInt2Offset( pVoxelData, ix, iy, iz);

        if( pWDst[ VoxOffset].NeighbourBits == 0) {  // Destination is outside

          continue;
        }

        if( pWDst[ VoxOffset].Weight[ 0] != 0.0) {  // Already set a weight for this interion

          continue;
        }

        nJLinks = 0;

        for( iVoxOfCube = 0; iVoxOfCube < nVoxOfCube; iVoxOfCube++) {

          if( (pWSrc[ VoxOffset].NeighbourBits & pVoxOffCube[ iVoxOfCube].NeighbourBit) == 0) { // This is no neighbour

            continue;
          }

          VoxOff2 = VoxOffset + pVoxOffCube[ iVoxOfCube].Offset;

          pVW1 = pWSrc + VoxOff2;   // Point to his voxel cell

#ifdef use_again
          if( pVW1->Weight[ 0] == 0.0) {                  // Has no weight until now

            continue;
          }
#endif

          for( iWeight = 0; iWeight < 4; iWeight++) {     // up to 4 blends per vertex

            if( pVW1->Weight[ iWeight] == 0.0) {          // Unassigned weight

              break;
            }

            // Enter in Weight list

            for( iJLink = 0; iJLink < nJLinks; iJLink++) {

              if( pVW1->iJoint[ iWeight] == JointWeights[ iJLink].iJoint) {  // Already have in list

                JointWeights[ iJLink].Weight += pVW1->Weight[ iWeight];      // Summ up
                break;
              }
            }

            if( iJLink >= nJLinks) {   // not in list until now

              // enter in weight list

              JointWeights[ nJLinks].iJoint = pVW1->iJoint[ iWeight];
              JointWeights[ nJLinks].Weight = pVW1->Weight[ iWeight];
              nJLinks += 1;  // have one more
            }
          }
        }

        // sort weight list by weight

        if( nJLinks <= 0) {          // no weight

          // Reset all weights
          pWDst[ VoxOffset].iJoint[ 0] = 0;
          pWDst[ VoxOffset].Weight[ 0] = 0.0;

          pWDst[ VoxOffset].iJoint[ 1] = 0;
          pWDst[ VoxOffset].Weight[ 1] = 0.0;

          pWDst[ VoxOffset].iJoint[ 2] = 0;
          pWDst[ VoxOffset].Weight[ 2] = 0.0;

          pWDst[ VoxOffset].iJoint[ 3] = 0;
          pWDst[ VoxOffset].Weight[ 3] = 0.0;

        } else if( nJLinks == 1) {   // 1 weight

          AnySet = true;

          // Set first weight, reset others
          pWDst[ VoxOffset].iJoint[ 0] = JointWeights[ 0].iJoint;
          pWDst[ VoxOffset].Weight[ 0] = 1.0;

          pWDst[ VoxOffset].iJoint[ 1] = 0;
          pWDst[ VoxOffset].Weight[ 1] = 0.0;

          pWDst[ VoxOffset].iJoint[ 2] = 0;
          pWDst[ VoxOffset].Weight[ 2] = 0.0;

          pWDst[ VoxOffset].iJoint[ 3] = 0;
          pWDst[ VoxOffset].Weight[ 3] = 0.0;

        } else  {                    // 2 or more weights

          AnySet = true;

          // sort by weight

          qsort( JointWeights, nJLinks, sizeof( TJointWeight), IqeB_JointWeight_compare_Weight);

          // Set first weight, reset others
          pWDst[ VoxOffset].iJoint[ 0] = JointWeights[ 0].iJoint;
          pWDst[ VoxOffset].Weight[ 0] = 1.0;

          pWDst[ VoxOffset].iJoint[ 1] = 0;
          pWDst[ VoxOffset].Weight[ 1] = 0.0;

          pWDst[ VoxOffset].iJoint[ 2] = 0;
          pWDst[ VoxOffset].Weight[ 2] = 0.0;

          pWDst[ VoxOffset].iJoint[ 3] = 0;
          pWDst[ VoxOffset].Weight[ 3] = 0.0;
        }
      }
    }
  }

  return( AnySet);
}

// ----------------------------------------------------------------------------
// Average voxel weight data
static void IqeB_VoxWeightAverage( T_VoxelModelData *pVoxelData, TVoxelOffset *pVoxOffCube, int nVoxOfCube,
                                   TVoxelWeight *pWDst, TVoxelWeight *pWSrc)
{
  int ix, iy, iz;
  int nJLinks, iJLink, nNeighbours, VoxOffset, VoxOff2, iVoxOfCube, iWeight;
  float NeighbourFac;
  TVoxelWeight *pVW1;
  TJointWeight JointWeights[ MAXBONE];

  for( ix = 0; ix < pVoxelData->Size[ 0]; ix++) {
    for( iy = 0; iy < pVoxelData->Size[ 1]; iy++) {
      for( iz = 0; iz < pVoxelData->Size[ 2]; iz++) {

        VoxOffset = IqeB_VoxelCoordVoxelInt2Offset( pVoxelData, ix, iy, iz);

        if( pWDst[ VoxOffset].NeighbourBits == 0) {     // Destination is outside

          continue;
        }

        nJLinks = 0;
        nNeighbours = 0;

        for( iVoxOfCube = 0; iVoxOfCube < nVoxOfCube; iVoxOfCube++) {

          if( (pWSrc[ VoxOffset].NeighbourBits & pVoxOffCube[ iVoxOfCube].NeighbourBit) == 0) { // This is no neighbour

            continue;
          }

          nNeighbours += 1;      // Have one more neighbour

          VoxOff2 = VoxOffset + pVoxOffCube[ iVoxOfCube].Offset;

          pVW1 = pWSrc + VoxOff2;   // Point to his voxel cell

          for( iWeight = 0; iWeight < 4; iWeight++) {     // up to 4 blends per vertex

            if( pVW1->Weight[ iWeight] == 0.0) {          // Unassigned weight

              break;
            }

            // Enter in Weight list

            for( iJLink = 0; iJLink < nJLinks; iJLink++) {

              if( pVW1->iJoint[ iWeight] == JointWeights[ iJLink].iJoint) {  // Already have in list

                JointWeights[ iJLink].Weight += pVW1->Weight[ iWeight];      // Summ up
                break;
              }
            }

            if( iJLink >= nJLinks) {   // not in list until now

              // enter in weight list

              JointWeights[ nJLinks].iJoint = pVW1->iJoint[ iWeight];
              JointWeights[ nJLinks].Weight = pVW1->Weight[ iWeight];
              nJLinks += 1;  // have one more
            }
          }
        }

        if( nNeighbours > 1) {               // more than one neighbour

          NeighbourFac = 1.0 / nNeighbours;  // Factor to average summed weights
        } else {

          NeighbourFac = 1.0;                // Factor to average summed weights
        }

        // sort weight list by weight

        if( nJLinks <= 0) {          // no weight

          // Reset all weights
          pWDst[ VoxOffset].iJoint[ 0] = 0;
          pWDst[ VoxOffset].Weight[ 0] = 0.0;

          pWDst[ VoxOffset].iJoint[ 1] = 0;
          pWDst[ VoxOffset].Weight[ 1] = 0.0;

          pWDst[ VoxOffset].iJoint[ 2] = 0;
          pWDst[ VoxOffset].Weight[ 2] = 0.0;

          pWDst[ VoxOffset].iJoint[ 3] = 0;
          pWDst[ VoxOffset].Weight[ 3] = 0.0;

        } else if( nJLinks == 1) {   // 1 weight

          // Set first weight, reset others
          pWDst[ VoxOffset].iJoint[ 0] = JointWeights[ 0].iJoint;
          pWDst[ VoxOffset].Weight[ 0] = JointWeights[ 0].Weight * NeighbourFac;

          pWDst[ VoxOffset].iJoint[ 1] = 0;
          pWDst[ VoxOffset].Weight[ 1] = 0.0;

          pWDst[ VoxOffset].iJoint[ 2] = 0;
          pWDst[ VoxOffset].Weight[ 2] = 0.0;

          pWDst[ VoxOffset].iJoint[ 3] = 0;
          pWDst[ VoxOffset].Weight[ 3] = 0.0;

        } else  {                    // 2 or more weights

          // Normalize weights to # visited neighbours
          for( iJLink = 0; iJLink < nJLinks; iJLink++) {

            JointWeights[ iJLink].Weight *= NeighbourFac;
          }

          // sort by weight

          qsort( JointWeights, nJLinks, sizeof( TJointWeight), IqeB_JointWeight_compare_Weight);

          // Copy over

          if( nJLinks > 4) {    // get the 4 biggest

            nJLinks = 4;
          }

          for( iWeight = 0; iWeight < nJLinks; iWeight++) {     // up to 4 blends per vertex
            pWDst[ VoxOffset].iJoint[ iWeight] = JointWeights[ iWeight].iJoint;
            pWDst[ VoxOffset].Weight[ iWeight] = JointWeights[ iWeight].Weight;
          }
          for( ; iWeight < 4; iWeight++) {     // zero remainders
            pWDst[ VoxOffset].iJoint[ iWeight] = 0;
            pWDst[ VoxOffset].Weight[ iWeight] = 0.0;
          }
        }
      }
    }
  }
}

// ----------------------------------------------------------------------------
// Normalize the weight values
static void IqeB_VoxWeightNormalizeWeights( T_VoxelModelData *pVoxelData, TVoxelWeight *pWDst)
{
  int VoxelAmount, iVoxel, iWeight, nWithWeight;
  TVoxelWeight *pVW1;
  float WeightSum;
  float WeightClip = 1.0f / (256.0f);

  VoxelAmount = pVoxelData->Size[ 0] * pVoxelData->Size[ 1] * pVoxelData->Size[ 2];  // multiply it

  // For all voxel cells

  pVW1    = pWDst;                // point to voxel weight data

  for( iVoxel = 0; iVoxel < VoxelAmount; iVoxel++, pVW1++) {

    if( pVW1->NeighbourBits == 0) {  // is outside of model

      continue;
    }

    // is inside of model

    WeightSum = 0;
		for( iWeight = 0; iWeight < 4; iWeight++) {     // up to 4 blends per vertex

      if( pVW1->Weight[ iWeight] <= WeightClip) {   // end of list, need some minimum value

        break;
      }

			WeightSum += pVW1->Weight[ iWeight];
		}

		nWithWeight = iWeight;               // have this many with a weight

    for( ; iWeight < 4; iWeight++) {     // zero remainders

      pVW1->iJoint[ iWeight] = 0;
      pVW1->Weight[ iWeight] = 0.0;
    }

    if( WeightSum > 0.0) {  // Make sene to normalize weights

      WeightSum = 1.0 / WeightSum;     // reziproc

  		for( iWeight = 0; iWeight < nWithWeight; iWeight++) { // Normalize the weights

        pVW1->Weight[ iWeight] *= WeightSum;
  		}
    }
  }
}

// ----------------------------------------------------------------------------
// Transfer voxel weight data to skin
static void IqeB_VoxWeightToSkin( struct mesh *mesh,
                                  T_VoxelModelData *pVoxelData,
                                  int *pVertexBind,
                                  TVoxelWeight *pWSrc)
{
  int iVertex, iWeight, VoxOffset;
  float WeightSum;
  int *bi;
  float *pV, *bw;
#ifdef use_again
  float WeightClip = 1.0f / 256.0f;
#else
  float WeightClip = 1.0f / (1024.0f * 1024.0 * 1024.0 * 1024.0);
#endif
  vec3 VPointTemp;
  TVoxelWeight *pVW1;
#ifdef _DEBUG

  // Count number of weights per vertex
  int nVertex_0Weight, nVertex_1Weight, nVertex_MoreWeight;

  nVertex_0Weight = 0;
  nVertex_1Weight = 0;
  nVertex_MoreWeight = 0;
#endif

  pV = mesh->position;
	bi = mesh->blendindex;
  bw = mesh->blendweight;

  for( iVertex = 0; iVertex < mesh->vertex_count; iVertex++, pV += 3, bi += 4, bw += 4) {

    if( pVertexBind[ iVertex] == 0) {   // Don't bind this vertex

      continue;
    }

    IqeB_VoxelCoordWorld2Voxel( pVoxelData, VPointTemp, pV);           // Transform from world to voxel space

    VoxOffset = IqeB_VoxelCoordVoxelFloat2Offset( pVoxelData, VPointTemp); // Get Offset to voxel

    pVW1 = pWSrc + VoxOffset;                       // Pointer to voxel weight element

    WeightSum = 0;
		for( iWeight = 0; iWeight < 4; iWeight++) {     // up to 4 blends per vertex

      if( pVW1->Weight[ iWeight] <= WeightClip) {   // end of list, need some minimum value

        break;
      }
			bi[ iWeight] = pVW1->iJoint[ iWeight];
			bw[ iWeight] = pVW1->Weight[ iWeight];

			WeightSum += bw[ iWeight];
		}
    for( ; iWeight < 4; iWeight++) {     // zero remainders
      bi[ iWeight] = 0;
      bw[ iWeight] = 0.0;
    }

    if( WeightSum > 0.0) {  // Make sene to normalize weights

      WeightSum = 1.0 / WeightSum;     // reziproc

      bw[ 0] *= WeightSum;
      bw[ 1] *= WeightSum;
      bw[ 2] *= WeightSum;
      bw[ 3] *= WeightSum;
    }
#ifdef _DEBUG

    // Count number of weights per vertex

    if( bw[ 0] == 0.0) {

      nVertex_0Weight += 1;
    } else if( bw[ 0] == 1.0) {

      nVertex_1Weight += 1;
    } else {

      nVertex_MoreWeight += 1;
    }
#endif
  }

  return;    // Just do use it as breakpoint
}

// ----------------------------------------------------------------------------
// Setup neighbour data of voxel cell and voxel weights

#define HEAD_LINE_USE_WEIGHT_0_ONLY  1  // define this to set only the wight 0 entry

static void IqeB_VoxWeightHeadLine( T_VoxelModelData *pVoxelData,  // IN: Voxel mesh
                                    TVoxelWeight *pWDst,           // IN: Voxel weight data
                                    int iJoint,                    // IN: Head with this joint number
                                    vec3 p1, vec3 p2,              // IN: Points of line
                                    float HeadValue,               // IN: Head value to set
                                    int PointFillMask)             // IN: 0x01, 0x02, 0x03: bitmask witch point to write, p1 or p2
{
  float d;
  vec3 m;
  int VoxOffset;
  TVoxelWeight *pVW1;
  vec3 VTemp;
#ifdef HEAD_LINE_USE_WEIGHT_0_ONLY
#else
  float WeightLowestVal;
  int iWeight, iWeightLowestVal;
#endif

  if( PointFillMask & 0x01) {  // Fill for first point

    IqeB_VoxelCoordWorld2Voxel( pVoxelData, VTemp, p1);           // Transform from world to voxel space
    VoxOffset = IqeB_VoxelCoordVoxelInt2Offset( pVoxelData, (int)(VTemp[ 0] + 0.5), (int)(VTemp[ 1] + 0.5), (int)(VTemp[ 2] + 0.5));
    pVW1 = pWDst + VoxOffset;   // Point to his voxel cell

    if( pVW1->NeighbourBits != 0) {     // Destination is inside

#ifdef HEAD_LINE_USE_WEIGHT_0_ONLY
      pVW1->iJoint[ 0] = iJoint;
      pVW1->Weight[ 0] = HeadValue;        // Set head value
#else
      // Try to find weight with same joint number

      iWeightLowestVal = 0;
      WeightLowestVal  = pVW1->Weight[ 0];

		  for( iWeight = 0; iWeight < 4; iWeight++) {     // up to 4 blends per vertex

        if( pVW1->Weight[ iWeight] == 0.0) {          // Unassigned weight

          break;
        }

        if( pVW1->iJoint[ iWeight] == iJoint) {       // Assign for this joint

          if( pVW1->Weight[ iWeight] < HeadValue) {   // Below our head value

            pVW1->Weight[ iWeight] = HeadValue;       // Set head value
          }
          break;
        }

        // Catch lowest value (used for full list)

        if( pVW1->Weight[ iWeight] < WeightLowestVal) {

          iWeightLowestVal = iWeight;
          WeightLowestVal  = WeightLowestVal;
        }
		  }

		  if( iWeight >= 4) {                          // Don't found a weight

        // Overwerite lowest value
		    pVW1->iJoint[ iWeightLowestVal] = iJoint;
        pVW1->Weight[ iWeightLowestVal] = HeadValue;  // Set head value

		  } else if( pVW1->Weight[ iWeight] == 0.0) {  // End of list, no weight found

		    // Append the value
		    pVW1->iJoint[ iWeight] = iJoint;
        pVW1->Weight[ iWeight] = HeadValue;        // Set head value
		  }
#endif
		}
  }

  if( PointFillMask & 0x02) {  // Fill for second point

    IqeB_VoxelCoordWorld2Voxel( pVoxelData, VTemp, p2);           // Transform from world to voxel space
    VoxOffset = IqeB_VoxelCoordVoxelInt2Offset( pVoxelData, (int)(VTemp[ 0] + 0.5), (int)(VTemp[ 1] + 0.5), (int)(VTemp[ 2] + 0.5));
    pVW1 = pWDst + VoxOffset;   // Point to his voxel cell

    if( pVW1->NeighbourBits != 0) {     // Destination is inside

#ifdef HEAD_LINE_USE_WEIGHT_0_ONLY
      pVW1->iJoint[ 0] = iJoint;
      pVW1->Weight[ 0] = HeadValue;        // Set head value
#else
      // Try to find weight with same joint number

      iWeightLowestVal = 0;
      WeightLowestVal  = pVW1->Weight[ 0];

		  for( iWeight = 0; iWeight < 4; iWeight++) {     // up to 4 blends per vertex

        if( pVW1->Weight[ iWeight] == 0.0) {          // Unassigned weight

          break;
        }

        if( pVW1->iJoint[ iWeight] == iJoint) {       // Assign for this joint

          if( pVW1->Weight[ iWeight] < HeadValue) {   // Below our head value

            pVW1->Weight[ iWeight] = HeadValue;       // Set head value
          }
          break;
        }

        // Catch lowest value (used for full list)

        if( pVW1->Weight[ iWeight] < WeightLowestVal) {

          iWeightLowestVal = iWeight;
          WeightLowestVal  = WeightLowestVal;
        }
		  }

		  if( iWeight >= 4) {                          // Don't found a weight

        // Overwerite lowest value
		    pVW1->iJoint[ iWeightLowestVal] = iJoint;
        pVW1->Weight[ iWeightLowestVal] = HeadValue;  // Set head value

		  } else if( pVW1->Weight[ iWeight] == 0.0) {  // End of list, no weight found

		    // Append the value
		    pVW1->iJoint[ iWeight] = iJoint;
        pVW1->Weight[ iWeight] = HeadValue;        // Set head value
		  }
#endif
		}
  }

  // subdivide line

  d = vec_dist2( p1, p2);                 // squared distance

  if( d < VOX_CUPE_SET_LEN_THRESHOLD) {   // below length threshold
    return;
  }

  // split line

  vec_add( m, p1, p2);
  vec_scale_to( m, 0.5);

  // recursive fill

  IqeB_VoxWeightHeadLine( pVoxelData, pWDst, iJoint, m, p1, HeadValue, 0x01);
  IqeB_VoxWeightHeadLine( pVoxelData, pWDst, iJoint, m, p2, HeadValue, 0x00);
}

/************************************************************************************
 * IqeB_SkinBindHeadDiffusionSkin()
 *
 * Bind skin by heat diffusion of the skin.
 *
 */

void IqeB_SkinBindHeadDiffusionSkin( IQE_model *pModel, int AssignType, int JointSel,
                                     int BindSmooth, char *pErrorString, float *pFillFactor)
{
	struct skel *skel = pModel->skel;
	struct mesh *mesh = pModel->mesh;
  int *pVertexBind = NULL;
  int iWeight, iVertex, iJoint, iParent, iThis, VoxelAmount;
  vec3 VPointTemp;
  TBoneInfoData BoneInfoData;    // Bone Info
  TBoneInfo *pBoneInfo;
  TVoxelWeight *pVoxelWeight1 = NULL;
  TVoxelWeight *pVoxelWeight2 = NULL;
  T_VoxelModelData VoxelData = { { 0, 0, 0} };
#ifdef _DEBUG
  //int VoxelNormSize =  64;   // for debug, less processing time
  int VoxelNormSize =  VOX_MAX_NORM_SIZE;   // for debug, less processing time
#else
  int VoxelNormSize = VOX_MAX_NORM_SIZE;
#endif

  *pFillFactor = 0.0;    // Reset

  // security tests
  if( pModel == NULL || pModel->mesh == NULL) {  // security test, need this pointers

    return;   // exit to caller
  }

  if( pModel->skel == NULL ||              // security test, need a skeleton
      pModel->skel->joint_count <= 0 ||    // minimum need one joint
      pModel->mesh->vertex_count <= 0) {   // minimum need one vertex

    goto ErrorExit;
  }

  // Enshure to have the blend data

  if( pModel->mesh->blendindex == NULL ||
      pModel->mesh->blendweight == NULL) {   // missing blend data

    IqeB_SkinBindReset( pModel);             // allocate and reset the blend data
  }

  if( pModel->mesh->blendindex == NULL ||
      pModel->mesh->blendweight == NULL) {   // security test, still missing blend data

    goto ErrorExit;
  }

  // get vertics to process

  IqeB_SkinBindGetVertexDoBind( pModel, AssignType, &pVertexBind);
  if( pVertexBind == NULL) {  // security test

    goto ErrorExit;
  }

  // Build the bone info

  IqeB_SkinBindBuildBoneInfo( mesh, skel, &BoneInfoData);

  // Convert model to voxels

  IqeB_VoxelFromIqeModel( pModel, &VoxelData, VoxelNormSize, pErrorString);

  if( VoxelData.pVoxels == NULL) {    // Have no voxel data

    // NOTE: pErrorString is set in case of error

    goto ErrorExit;
  }

  *pFillFactor = VoxelData.FillFactor;    // Copy over

#ifdef _DEBUG
#ifdef use_again

  // Convert voxels to IQE model

  IQE_model *pModelIqeTemp;

#ifndef use_again
  pModelIqeTemp = IqeB_VoxelToIqeCubes( &VoxelData, pErrorString);
#else
  pModelIqeTemp = IqeB_VoxelToIqeMarchingCube( &VoxelData, pErrorString);
#endif

  // free the data of the old model
  IqeB_ModelFreeData( &pModel, IQE_MODEL_FREE_SKELETON | IQE_MODEL_FREE_ANIMATION | IQE_MODEL_FREE_MESH);

  // copy over the new model
  memcpy( pModel, pModelIqeTemp, sizeof( struct IQE_model));

  free( pModelIqeTemp);                                  // free the data of pModelIqeTemp

  // Show the model
  goto ErrorExit;

#endif
#endif

  //
  // Do the skin binding, using the nearest bone alogirthm
  //

	int *bi, iBest, TestVal, AverageN;
  float *pV, *bw, DistThis, DistBest, BasePointOnLine, AverageBest;

  pV = mesh->position;
	bi = mesh->blendindex;
  bw = mesh->blendweight;

  AverageN = 0;
  AverageBest = 0.0;   // Sum up

  for( iVertex = 0; iVertex < mesh->vertex_count; iVertex++, pV += 3, bi += 4, bw += 4) {

    if( pVertexBind[ iVertex] == 0) {   // Don't bind this vertex

      continue;
    }

    // reset all weights first

		for( iWeight = 0; iWeight < 4; iWeight++) {     // up to 4 blends per vertex

			bi[ iWeight] = 0;
			bw[ iWeight] = 0.0;   // end of list
		}

		// Find nearest bone
		// NOTE: The bone must be visible from the vertex point to be OK

		iBest = -1;       // None selected
		DistBest = 0.0;

    pBoneInfo = BoneInfoData.BoneInfo;

  	for( iJoint = 0; iJoint < skel->joint_count; iJoint++, pBoneInfo++) {

      if( JointSel == IQE_SKINBIND_JOINT_SELECTED && ! skel->j[ iJoint].isSelected) {  // Like selected joints only

        continue;
      }

      if( JointSel == IQE_SKINBIND_JOINT_NOT_SEL && skel->j[ iJoint].isSelected) {  // Like NOT selected joints only

        continue;
      }

      if( pBoneInfo->J2Dist < 0.0) {          // Is a root joint with childs

        // No need to test this
        continue;

      } else if( pBoneInfo->J2Dist == 0.0) {  // Is a root joint without childs

        TestVal = IqeB_VoxelLineTest( &VoxelData,              // IN: Voxel mesh
                        pBoneInfo->BasePoint, pV,              // IN: Points of line in worldspace
                        0x01,  // Don't test the vertex        // IN: 0x01 set: Test first point, 0x02 set: test second point
                        1,     // Test for being inside        // IN: 0/1 : Test for any cell equal/unequal to 'TestValue' to be OK
                        0);    // = is the outside value       // IN: Value to test

        if( TestVal != 0) {   // NOT all of the line is inside the volume

          // No need to test this
          continue;
        }

        DistThis = vec_dist( pBoneInfo->BasePoint, pV);
        iThis = iJoint;

      } else {  // Is a bone

        // Test distance vertex point from bone straight line

        DistThis = PointDistNearestToLineBasePoint( pBoneInfo->BasePoint, pBoneInfo->Normal,
                                                    pV, &BasePointOnLine);

        iParent = skel->j[ iJoint].parent;
        if( iParent < 0) {      // is a root joint

    		  iThis = iJoint;       // Use this joint
        } else {

    		  iThis = iParent;      // Use parent
        }

        if( BasePointOnLine < 0.0) {

          TestVal = IqeB_VoxelLineTest( &VoxelData,              // IN: Voxel mesh
                          pBoneInfo->BasePoint, pV,              // IN: Points of line in worldspace
                          0x03,  // Don't test the vertex        // IN: 0x01 set: Test first point, 0x02 set: test second point
                          1,     // Test for being inside        // IN: 0/1 : Test for any cell equal/unequal to 'TestValue' to be OK
                          0);    // = is the outside value       // IN: Value to test

          if( TestVal != 0) {   // NOT all of the line is inside the volume

            // No need to test this
            continue;
          }

          DistThis = vec_dist( pBoneInfo->BasePoint, pV);

    		  iThis = iJoint;       // Use this joint

        } else if( BasePointOnLine > pBoneInfo->J2Dist) {

          TestVal = IqeB_VoxelLineTest( &VoxelData,              // IN: Voxel mesh
                          pBoneInfo->J2Point, pV,                // IN: Points of line in worldspace
                          0x03,  // Don't test the vertex        // IN: 0x01 set: Test first point, 0x02 set: test second point
                          1,     // Test for being inside        // IN: 0/1 : Test for any cell equal/unequal to 'TestValue' to be OK
                          0);    // = is the outside value       // IN: Value to test

          if( TestVal != 0) {   // NOT all of the line is inside the volume

            // No need to test this
            continue;
          }

          DistThis = vec_dist( pBoneInfo->J2Point, pV);

        } else {

          // 'Recompute' the nearest point to the vertex point to the bone straight line

          vec3 PointOnLine;

          vec_copy( PointOnLine, pBoneInfo->BasePoint);
          vec_scale_add_to( PointOnLine, pBoneInfo->Normal, BasePointOnLine);

          TestVal = IqeB_VoxelLineTest( &VoxelData,              // IN: Voxel mesh
                          PointOnLine, pV,                       // IN: Points of line in worldspace
                          0x03,  // Don't test the vertex        // IN: 0x01 set: Test first point, 0x02 set: test second point
                          1,     // Test for being inside        // IN: 0/1 : Test for any cell equal/unequal to 'TestValue' to be OK
                          0);    // = is the outside value       // IN: Value to test

          if( TestVal != 0) {   // NOT all of the line is inside the volume

            // No need to test this
            continue;
          }
        }
      }

      if( DistThis > BoneInfoData.BoneLengthThres) {    // More away than bonelength thresnold

        continue;
      }

      if( iBest < 0 || DistThis < DistBest) {  // First one or neared

        iBest = iThis;        // Our joint
		    DistBest = DistThis;
      }
  	}

    if( iBest >= 0) {

  	  bi[ 0] = iBest;  // Nearest
		  bw[ 0] = 1.0;    // Have only one, so full weight

      // Sum up best distances
      AverageN    += 1;
      AverageBest += DistBest;
    }
  }

  if( AverageN > 1) {                      // More than one sample

    AverageBest = AverageBest / AverageN;  // Average distance mesh points frome bones/joints
  }

#ifdef _DEBUG
#ifdef use_again


  // Show the model
  goto ErrorExit;

#endif
#endif

  //
  // Transfer the Mesh joint data to the voxel space
  //

  // allocate voxel weight data

  VoxelAmount = VoxelData.Size[ 0] * VoxelData.Size[ 1] * VoxelData.Size[ 2];  // multiply it

  pVoxelWeight1 = (TVoxelWeight *)malloc( VoxelAmount * sizeof( TVoxelWeight));
  pVoxelWeight2 = (TVoxelWeight *)malloc( VoxelAmount * sizeof( TVoxelWeight));

  if( pVoxelWeight1 == NULL) {   // Security test

    if( pErrorString != NULL && pErrorString[ 0] == '\0') {  // No error until now

      strcpy( pErrorString, "IqeB_SkinBindHeadDiffusionSkin: Memory allocation failed");
    }
  }

  if( pVoxelWeight2 == NULL) {   // Security test

    if( pErrorString != NULL && pErrorString[ 0] == '\0') {  // No error until now

      strcpy( pErrorString, "IqeB_SkinBindHeadDiffusionSkin: Memory allocation failed");
    }
  }

  //
  // Transfer the inside, outside to voxel weight data
  //

  int iIteration, nIterations, AnySet;
  TVoxelOffset VoxOffCube[ 27];
  int nVoxOfCube;
  int  VoxOffset;
  TVoxelWeight *pVW1, *pWSrc, *pWDst;

  IqeB_VoxWeightSetupNeighbours( &VoxelData, VoxOffCube, &nVoxOfCube, pVoxelWeight1);

  // duplicate inside/outside and neighbour settings

  memcpy( pVoxelWeight2, pVoxelWeight1, VoxelAmount * sizeof( TVoxelWeight));  // Copy over for the outside setting

  //
  // Copy the weights on the skin (we got until now) to the voxel weight cells
  //

  pV = mesh->position;
	bi = mesh->blendindex;
  bw = mesh->blendweight;

  for( iVertex = 0; iVertex < mesh->vertex_count; iVertex++, pV += 3, bi += 4, bw += 4) {

    if( pVertexBind[ iVertex] == 0) {   // Don't bind this vertex

      continue;
    }

    IqeB_VoxelCoordWorld2Voxel( &VoxelData, VPointTemp, pV);           // Transform from world to voxel space

    VoxOffset = IqeB_VoxelCoordVoxelFloat2Offset( &VoxelData, VPointTemp); // Get Offset to voxel

    pVW1 = pVoxelWeight1 + VoxOffset;  // Pointer to voxel weight element

    if( bw[ 0] != 0.0) {    // A weight is set for the first entry

      pVW1->iJoint[ 0] = bi[ 0]; // Copy over
      pVW1->Weight[ 0] = bw[ 0]; // Copy over
    }
  }

  //
  // Copy filled voxel cells to unfilled neighbours
  //

  pWSrc = pVoxelWeight1;      // point to voxel weight data
  pWDst = pVoxelWeight2;

#ifndef use_again
  // Max number of iterations is the diagonal of our voxel volume
  nIterations = (int)sqrt( VoxelData.Size[ 0] * VoxelData.Size[ 0] +
                           VoxelData.Size[ 1] * VoxelData.Size[ 1] +
                           VoxelData.Size[ 2] * VoxelData.Size[ 2]);

  for( iIteration = 0; iIteration < nIterations; iIteration++) {

    AnySet = IqeB_VoxWeightCopy2Neighbours( &VoxelData, VoxOffCube, nVoxOfCube, pWDst, pWSrc);   // Make an average pass

    // Exchange pointers

    pVW1  = pWSrc;
    pWSrc = pWDst;
    pWDst = pVW1;

    if( ! AnySet) {    // All filled

      break;
    }
  }
#endif

  //
  // Average voxel neighbour cells
  //

#ifndef use_again

  // Max number of iterations is the diagonal of our voxel volume
  nIterations = (int)sqrt( VoxelData.Size[ 0] * VoxelData.Size[ 0] +
                           VoxelData.Size[ 1] * VoxelData.Size[ 1] +
                           VoxelData.Size[ 2] * VoxelData.Size[ 2]);

  // Use only percentage of this
  if( BindSmooth < 0) {  // security test range
    BindSmooth = 0;
  }
  if( BindSmooth > 100) {
    BindSmooth = 100;
  }

  nIterations = (nIterations * BindSmooth + 50) / 100;

  for( iIteration = 0; iIteration < nIterations; iIteration++) {

    IqeB_VoxWeightAverage( &VoxelData, VoxOffCube, nVoxOfCube, pWDst, pWSrc);   // Make an average pass

    // Exchange pointers

    pVW1  = pWSrc;
    pWSrc = pWDst;
    pWDst = pVW1;
  }
#endif

  // Copy back weights from the voxel weigts to the skin

  IqeB_VoxWeightToSkin( mesh, &VoxelData, pVertexBind, pWSrc);

	// done, fall into clean up

//
// Error exit
//

ErrorExit:

  // clean up

  if( pVertexBind != NULL) {

    free( pVertexBind);
  }

  if( VoxelData.pVoxels != NULL) {    // Have voxel data

    free( VoxelData.pVoxels);
  }

  if( pVoxelWeight2 != NULL) {

    free( pVoxelWeight1);
  }

  if( pVoxelWeight1 != NULL) {

    free( pVoxelWeight2);
  }

	return;  // return to caller
}

/************************************************************************************
 * IqeB_SkinBindHeadDiffusionBones()
 *
 * Bind skin by heat diffusion from the bones.
 *
 */

void IqeB_SkinBindHeadDiffusionBones( IQE_model *pModel, int AssignType, int JointSel,
                                      int BindSmooth, char *pErrorString, float *pFillFactor)
{
	struct skel *skel = pModel->skel;
	struct mesh *mesh = pModel->mesh;
  int *pVertexBind = NULL;
  int iJoint, iParent, iThis, VoxelAmount;
  TBoneInfoData BoneInfoData;    // Bone Info
  TBoneInfo *pBoneInfo;
  TVoxelWeight *pVoxelWeight1 = NULL;
  TVoxelWeight *pVoxelWeight2 = NULL;
  T_VoxelModelData VoxelData = { { 0, 0, 0} };
#ifdef _DEBUG
  //int VoxelNormSize =  64;   // for debug, less processing time
  int VoxelNormSize =  VOX_MAX_NORM_SIZE;   // for debug, less processing time
#else
  int VoxelNormSize = VOX_MAX_NORM_SIZE;
#endif

  *pFillFactor = 0.0;    // Reset

  // security tests
  if( pModel == NULL || pModel->mesh == NULL) {  // security test, need this pointers

    return;   // exit to caller
  }

  if( pModel->skel == NULL ||              // security test, need a skeleton
      pModel->skel->joint_count <= 0 ||    // minimum need one joint
      pModel->mesh->vertex_count <= 0) {   // minimum need one vertex

    goto ErrorExit;
  }

  // Enshure to have the blend data

  if( pModel->mesh->blendindex == NULL ||
      pModel->mesh->blendweight == NULL) {   // missing blend data

    IqeB_SkinBindReset( pModel);             // allocate and reset the blend data
  }

  if( pModel->mesh->blendindex == NULL ||
      pModel->mesh->blendweight == NULL) {   // security test, still missing blend data

    goto ErrorExit;
  }

  // get vertics to process

  IqeB_SkinBindGetVertexDoBind( pModel, AssignType, &pVertexBind);
  if( pVertexBind == NULL) {  // security test

    goto ErrorExit;
  }

  // Build the bone info

  IqeB_SkinBindBuildBoneInfo( mesh, skel, &BoneInfoData);

  // Convert model to voxels

  IqeB_VoxelFromIqeModel( pModel, &VoxelData, VoxelNormSize, pErrorString);

  if( VoxelData.pVoxels == NULL) {    // Have no voxel data

    // NOTE: pErrorString is set in case of error

    goto ErrorExit;
  }

  *pFillFactor = VoxelData.FillFactor;    // Copy over

#ifdef _DEBUG
#ifdef use_again

  // Convert voxels to IQE model

  IQE_model *pModelIqeTemp;

#ifndef use_again
  pModelIqeTemp = IqeB_VoxelToIqeCubes( &VoxelData, pErrorString);
#else
  pModelIqeTemp = IqeB_VoxelToIqeMarchingCube( &VoxelData, pErrorString);
#endif

  // free the data of the old model
  IqeB_ModelFreeData( &pModel, IQE_MODEL_FREE_SKELETON | IQE_MODEL_FREE_ANIMATION | IQE_MODEL_FREE_MESH);

  // copy over the new model
  memcpy( pModel, pModelIqeTemp, sizeof( struct IQE_model));

  free( pModelIqeTemp);                                  // free the data of pModelIqeTemp

  // Show the model
  goto ErrorExit;

#endif
#endif

  //
  // Prepare voxel weight data
  //

  // allocate voxel weight data

  VoxelAmount = VoxelData.Size[ 0] * VoxelData.Size[ 1] * VoxelData.Size[ 2];  // multiply it

  pVoxelWeight1 = (TVoxelWeight *)malloc( VoxelAmount * sizeof( TVoxelWeight));
  pVoxelWeight2 = (TVoxelWeight *)malloc( VoxelAmount * sizeof( TVoxelWeight));

  if( pVoxelWeight1 == NULL) {   // Security test

    if( pErrorString != NULL && pErrorString[ 0] == '\0') {  // No error until now

      strcpy( pErrorString, "IqeB_SkinBindHeadDiffusionBones: Memory allocation failed");
    }
  }

  if( pVoxelWeight2 == NULL) {   // Security test

    if( pErrorString != NULL && pErrorString[ 0] == '\0') {  // No error until now

      strcpy( pErrorString, "IqeB_SkinBindHeadDiffusionBones: Memory allocation failed");
    }
  }

  //
  // Transfer the inside, outside to voxel weight data
  //

  int iIteration, nIterations, AnySet;
  TVoxelOffset VoxOffCube[ 27];
  int nVoxOfCube;
  TVoxelWeight *pVW1, *pWSrc, *pWDst;

  IqeB_VoxWeightSetupNeighbours( &VoxelData, VoxOffCube, &nVoxOfCube, pVoxelWeight1);

  // duplicate inside/outside and neighbour settings

  memcpy( pVoxelWeight2, pVoxelWeight1, VoxelAmount * sizeof( TVoxelWeight));  // Copy over for the outside setting

  //
  // Average voxel neighbour cells
  //

  pWSrc = pVoxelWeight1;      // point to voxel weight data
  pWDst = pVoxelWeight2;

  // Draw the bones into the voxel volume

  pBoneInfo = BoneInfoData.BoneInfo;

 	for( iJoint = 0; iJoint < skel->joint_count; iJoint++, pBoneInfo++) {

    if( JointSel == IQE_SKINBIND_JOINT_SELECTED && ! skel->j[ iJoint].isSelected) {  // Like selected joints only

      continue;
    }

    if( JointSel == IQE_SKINBIND_JOINT_NOT_SEL && skel->j[ iJoint].isSelected) {  // Like NOT selected joints only

      continue;
    }

    iParent = skel->j[ iJoint].parent;
    if( iParent < 0) {      // is a root joint

  	  iThis = iJoint;       // Use this joint
    } else {

  	  iThis = iParent;      // Use parent
    }

    if( pBoneInfo->J2Dist < 0.0) {          // Is a root joint with childs

      // No need to test this
      continue;

    } else if( pBoneInfo->J2Dist == 0.0) {  // Is a root joint without childs

      // Head this joint
      IqeB_VoxWeightHeadLine( &VoxelData, pWSrc, iThis, pBoneInfo->BasePoint, pBoneInfo->BasePoint, VOX_MAX_NORM_SIZE, 0x01);

    } else {  // Is a bone

      // Head the bone
      IqeB_VoxWeightHeadLine( &VoxelData, pWSrc, iThis, pBoneInfo->BasePoint, pBoneInfo->J2Point, VOX_MAX_NORM_SIZE, 0x03);

      if( skel->j[ iJoint].nChilds == 0) {   // This one has no childs

        IqeB_VoxWeightHeadLine( &VoxelData, pWSrc, iThis, pBoneInfo->BasePoint, pBoneInfo->J2Point, VOX_MAX_NORM_SIZE, 0x02);

        IqeB_VoxWeightHeadLine( &VoxelData, pWSrc, iJoint, pBoneInfo->BasePoint, pBoneInfo->BasePoint, VOX_MAX_NORM_SIZE /* * pBoneInfo->BoneLength * 0.3 * VoxelData.ScaleMeshToVoxel */, 0x01);
      } else {

        IqeB_VoxWeightHeadLine( &VoxelData, pWSrc, iThis, pBoneInfo->BasePoint, pBoneInfo->J2Point, VOX_MAX_NORM_SIZE, 0x03);
      }
    }
 	}

#ifndef use_again

  // caculate number of iterations, use 1/2 average of voxel dimension
  nIterations = (VoxelData.Size[ 0] + VoxelData.Size[ 1] + VoxelData.Size[ 2] + 5) / 6;

  for( iIteration = 0; iIteration < nIterations; iIteration++) {


    // To the average

    IqeB_VoxWeightAverage( &VoxelData, VoxOffCube, nVoxOfCube, pWDst, pWSrc);   // Make an average pass

    // Normalize the weights

    IqeB_VoxWeightNormalizeWeights( &VoxelData, pWDst);   // Normalize the weight values

    // Exchange pointers

    pVW1  = pWSrc;
    pWSrc = pWDst;
    pWDst = pVW1;
  }
#endif

  //
  // Copy filled voxel cells to unfilled neighbours
  //

#ifndef use_again
  // Max number of iterations is the diagonal of our voxel volume
  nIterations = (int)sqrt( VoxelData.Size[ 0] * VoxelData.Size[ 0] +
                           VoxelData.Size[ 1] * VoxelData.Size[ 1] +
                           VoxelData.Size[ 2] * VoxelData.Size[ 2]);

  for( iIteration = 0; iIteration < nIterations; iIteration++) {

    AnySet = IqeB_VoxWeightCopy2Neighbours( &VoxelData, VoxOffCube, nVoxOfCube, pWDst, pWSrc);   // Make an average pass

    // Exchange pointers

    pVW1  = pWSrc;
    pWSrc = pWDst;
    pWDst = pVW1;

    if( ! AnySet) {    // All filled

      break;
    }
  }
#endif

  //
  // Average voxel neighbour cells
  //

#ifndef use_again

  // Max number of iterations is the diagonal of our voxel volume
  nIterations = (int)sqrt( VoxelData.Size[ 0] * VoxelData.Size[ 0] +
                           VoxelData.Size[ 1] * VoxelData.Size[ 1] +
                           VoxelData.Size[ 2] * VoxelData.Size[ 2]);

  // Use only percentage of this
  if( BindSmooth < 0) {  // security test range
    BindSmooth = 0;
  }
  if( BindSmooth > 100) {
    BindSmooth = 100;
  }

  nIterations = (nIterations * BindSmooth + 50) / 100;

  for( iIteration = 0; iIteration < nIterations; iIteration++) {

    IqeB_VoxWeightAverage( &VoxelData, VoxOffCube, nVoxOfCube, pWDst, pWSrc);   // Make an average pass

    // Exchange pointers

    pVW1  = pWSrc;
    pWSrc = pWDst;
    pWDst = pVW1;
  }
#endif

  // Copy back weights from the voxel weigts to the skin

  IqeB_VoxWeightToSkin( mesh, &VoxelData, pVertexBind, pWSrc);

	// done, fall into clean up

//
// Error exit
//

ErrorExit:

  // clean up

  if( pVertexBind != NULL) {

    free( pVertexBind);
  }

  if( VoxelData.pVoxels != NULL) {    // Have voxel data

    free( VoxelData.pVoxels);
  }

  if( pVoxelWeight2 != NULL) {

    free( pVoxelWeight1);
  }

  if( pVoxelWeight1 != NULL) {

    free( pVoxelWeight2);
  }

	return;  // return to caller
}

/****************************** End Of File ******************************/
