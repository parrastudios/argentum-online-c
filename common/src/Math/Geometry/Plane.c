/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Plane.h>
#include <Math/Geometry/Matrix.h>

void PlaneCopy(struct PlaneType * Dest, struct PlaneType * Src)
{
	if (Dest && Src)
	{
		Vector3fCopy(&Dest->v, &Src->v);

		Dest->d = Src->d;

		Dest->SignBits = Src->SignBits;
	}
}

void PlaneSetSignBits(struct PlaneType * Plane)
{
	float InvLen = 1.0f / Vector3fLength(&Plane->v);

	if (InvLen != 1.0f)
	{
		Vector3fMultiply(&Plane->v, InvLen);
		Plane->d *= InvLen;
	}

	Plane->SignBits = 0;

	if		(fabs(Plane->a) == 1.0f)	Plane->SignBits  = 1 << 0;
	else if (fabs(Plane->b) == 1.0f)	Plane->SignBits  = 1 << 1;
	else if (fabs(Plane->c) == 1.0f)	Plane->SignBits  = 1 << 2;
	if		(Plane->a < 0.0f)			Plane->SignBits |= 1 << 3;
	if		(Plane->b < 0.0f)			Plane->SignBits |= 1 << 4;
	if		(Plane->c < 0.0f)			Plane->SignBits |= 1 << 5;
}

bool PlaneCheckPoint(struct PlaneType * Plane, struct Vector3f * Point)
{
	float PlaneEqVal = Plane->d;

	if (Plane->SignBits & 7)
	{
		uinteger i;

		for (i = 0; i < 3; ++i)
		{
			if (Plane->SignBits & (1 << i))
			{
				PlaneEqVal += (Plane->SignBits & (8 << i)) ? -Point->v[i] : Point->v[i];
				break;
			}
		}
	}
	else
	{
		PlaneEqVal += Vector3fDot(&Plane->v, Point);
	}

	return (PlaneEqVal < 0.0f);
}

void PlaneTranslate(struct PlaneType * Plane, struct Vector3f * Vector)
{
	Plane->d -= Vector3fDot(&Plane->v, Vector);
}
float PlaneNormalizeEx(struct PlaneType * Dest, struct PlaneType * Src)
{
	float Normal = PlaneGetNormal(Src);

	if (Normal)
	{
		Dest->a = Src->a / Normal;
		Dest->b = Src->b / Normal;
		Dest->c = Src->c / Normal;
		Dest->d = Src->d / Normal;
	}
	else
	{
		Dest->a = Dest->b = Dest->c = Dest->d = 0.0f;
	}

	return Normal;
}

float PlaneNormalize(struct PlaneType * Plane)
{
	float Normal = PlaneGetNormal(Plane);

	if (Normal)
	{
		Plane->a /= Normal;
		Plane->b /= Normal;
		Plane->c /= Normal;
		Plane->d /= Normal;
	}
	else
	{
		Plane->a = Plane->b = Plane->c = Plane->d = 0.0f;
	}

	return Normal;
}

float PlaneGetNormal(struct PlaneType * Plane)
{
	return (float)sqrt(Plane->a * Plane->a + Plane->b * Plane->b + Plane->c * Plane->c);
}

float PlaneDotCoord(struct PlaneType * Plane, struct Vector3f * Vector)
{
	if (!Plane || !Vector)
		return 0.0f;

	return Plane->a * Vector->x + Plane->b * Vector->y + Plane->c * Vector->z + Plane->d;
}
