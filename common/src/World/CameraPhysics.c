/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/CameraPhysics.h>

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a physics component of a camera
////////////////////////////////////////////////////////////
struct EntityType * CameraPhysicsCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a camera physics component
////////////////////////////////////////////////////////////
void CameraPhysicsDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the camera physics implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * CameraComponentPhysicsImpl()
{
	static const struct ComponentImplType CameraComponentPhysicsTypeImpl = {
		ExprInit(Base, TypeInit(Create, &CameraPhysicsCreate), TypeInit(Destroy, &CameraPhysicsDestroy)),
		TypeInit(Type, COMPONENT_TYPE_PHYSICS),
		UnionInit(Attributes, Physics, &EntityComponentPhysicsAttr)
	};

	return &CameraComponentPhysicsTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a physics component of a camera
////////////////////////////////////////////////////////////
struct EntityType * CameraPhysicsCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Create physics component data reference
		struct ComponentType * Physics = EntityComponentPhysics(Entity);
		struct ComponentPhysicsType * PhysicsType = ComponentTypePhysics(Physics);

		// Set default physics values
		PhysicsType->Mass(Entity, CAMERA_PHYSICS_MASS_DEFAULT);

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a camera physics component
////////////////////////////////////////////////////////////
void CameraPhysicsDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get physics component
		struct ComponentType * Physics = EntityComponentPhysics(Entity);
		struct ComponentPhysicsData * PhysicsData = ComponentDataPhysics(Physics);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the camera physics data size
////////////////////////////////////////////////////////////
uinteger CameraComponentPhysicsSize()
{
    return COMPONENT_DATA_LENGTH_DEFAULT;
}
