/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_TEXTURE_RENDER_H
#define GRAPHICS_TEXTURE_RENDER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/Color.h>
#include <Graphics/Index.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
//#define RENDER_SIZE_WIDTH		704		// 22 * 32
//#define RENDER_SIZE_HEIGHT	544		// 17 * 32

////////////////////////////////////////////////////////////
/// Render a grh from index
// (without color and rect specification)
////////////////////////////////////////////////////////////
void TextureRenderGrh(int32 GrhID, int32 X, int32 Y);

////////////////////////////////////////////////////////////
/// Render a grh from index with alpha specification
// (without rect specification)
////////////////////////////////////////////////////////////
void TextureRenderGrhA(int32 GrhID, int32 X, int32 Y, uint8 Alpha);

////////////////////////////////////////////////////////////
/// Render a grh from index with color specification
// (without rect specification)
////////////////////////////////////////////////////////////
void TextureRenderGrhColor(int32 GrhID, int32 X, int32 Y, struct Color4ub * Color);

////////////////////////////////////////////////////////////
/// Render a grh from index with color specification
// (repeating it in all rect)
////////////////////////////////////////////////////////////
void TextureRenderGrhColorRepeat(int32 GrhID, int32 Left, int32 Top, int32 Right, int32 Bottom, struct Color4ub * Color);

////////////////////////////////////////////////////////////
/// Render a grh from pointer
// (without color and rect specification)
////////////////////////////////////////////////////////////
void TextureRenderGrhPtr(struct GrhData * Grh, int32 X, int32 Y);

////////////////////////////////////////////////////////////
/// Render a circle
/// it starts filling the circle from StartTheta degrees to Alpha (max 360)
/// @param Smothness : Number of segments
////////////////////////////////////////////////////////////
void TextureRenderGrhRound(int32 GrhID, int32 CX, int32 CY, int32 Radio, uinteger Smoothness, float StartTheta, float Alpha, struct Color4ub * Color);

#endif // GRAPHICS_TEXTURE_RENDER_H