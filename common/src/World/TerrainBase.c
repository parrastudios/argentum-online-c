/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
///////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/TerrainBase.h>

#include <System/IOHelper.h> // todo: refactor this

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a base component of a terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainBaseCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Initialize base terrain properties
////////////////////////////////////////////////////////////
struct EntityType * TerrainBaseInitialize(struct EntityType * Entity, uinteger Index, uinteger HeightMapSize, uinteger HeightMapTile, uinteger PatchSize, struct Vector3f * Scale);

////////////////////////////////////////////////////////////
/// Destroy a terrain base component
////////////////////////////////////////////////////////////
void TerrainBaseDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Calculate quadtree nodes for a given patch size
////////////////////////////////////////////////////////////
uinteger TerrainBasePatchNodes(struct EntityType * Entity, uinteger Size);

////////////////////////////////////////////////////////////
/// Return number of quadtree nodes for the heightmap
////////////////////////////////////////////////////////////
uinteger TerrainBaseHeightMapNodesSize(struct EntityType * Entity);

////////////////////////////////////////////////////////////
//< Get the height at desired position
////////////////////////////////////////////////////////////
float TerrainGetHeightAt(struct EntityType * Entity, float X, float Y);

////////////////////////////////////////////////////////////
/// Return number of quadtree nodes for the terrain
////////////////////////////////////////////////////////////
uinteger TerrainBaseNodesSize(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the terrain base implementation attributes
////////////////////////////////////////////////////////////
const void * TerrainComponentBaseAttr()
{
	static const struct TerrainComponentBaseType TerrainComponentBaseAttr = {
		TypeInit(Initialize, &TerrainBaseInitialize),
		TypeInit(PatchNodes, &TerrainBasePatchNodes),
		TypeInit(HeightMapNodesSize, &TerrainBaseHeightMapNodesSize),
		TypeInit(NodesSize, &TerrainBaseNodesSize),
		TypeInit(GetHeightAt, &TerrainGetHeightAt)
	};

	return (const void *)&TerrainComponentBaseAttr;
}

////////////////////////////////////////////////////////////
/// Return the terrain base implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * TerrainComponentBaseImpl()
{
	static const struct ComponentImplType TerrainComponentBaseTypeImpl = {
		ExprInit(Base, TypeInit(Create, &TerrainBaseCreate), TypeInit(Destroy, &TerrainBaseDestroy)),
		TypeInit(Type, COMPONENT_TYPE_BASE),
		UnionInit(Attributes, Derived, &TerrainComponentBaseAttr)
	};

	return &TerrainComponentBaseTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a base component of a terrain
////////////////////////////////////////////////////////////
struct EntityType * TerrainBaseCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		// Set default properties of the terrain
		BaseData->Index = 0;
		BaseData->HeightMap = NULL;
		BaseData->HeightMapSize = 0;
		BaseData->HeightMapTile = 0;
		BaseData->PatchSize = 0;
		BaseData->PatchTile = 0;

		Vector3fIndentity(&BaseData->Scale);

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy base terrain heightmap
////////////////////////////////////////////////////////////
void TerrainBaseHeightMapClear(struct TerrainComponentBaseData * BaseData)
{
	if (BaseData && BaseData->HeightMap)
	{
		MemoryDeallocate(BaseData->HeightMap);
	}
}

////////////////////////////////////////////////////////////
/// Load base terrain heightmap
////////////////////////////////////////////////////////////
bool TerrainBaseHeightMapLoad(struct TerrainComponentBaseData * BaseData, uinteger Index, uinteger Size)
{
	if (BaseData)
	{
		FILE * File = NULL;

		// Clear old heightmap
		TerrainBaseHeightMapClear(BaseData);

		// Allocate heightmap block
		BaseData->HeightMap = MemoryAllocate(sizeof(uint8) * sqr(Size + 1));

		if (BaseData->HeightMap)
		{
			char Path[0xFF];

			// Generate the path of the heightmap (index, size)
			sprintf(Path, "data/geomipmaps/heightmap/heightmap%d_%d.raw", Index, Size);

			if (fopen(&File, Path, "rb"))
			{
				bool Result = false;

				uinteger BufferSize = sqr(Size);

				// Allocate intermediate buffer for loading the raw heightmap
				uint8 * HeightMapBuffer = MemoryAllocate(sizeof(uint8) * BufferSize);

				if (HeightMapBuffer)
				{
					uinteger ReadBytes = fread(HeightMapBuffer, sizeof(uint8), BufferSize, File);

					if (ReadBytes == BufferSize)
					{
						uinteger i;

						// Overlap the heightmap borders
						for (i = 0; i <= Size; ++i)
						{
							MemoryCopy(&BaseData->HeightMap[i * (Size + 1)], &HeightMapBuffer[i * Size], Size);

							// The first shall be last
							BaseData->HeightMap[(i + 1) * (Size + 1) - 1] = BaseData->HeightMap[i * (Size + 1)];
						}

						// The first shall be last
						MemoryCopy(&BaseData->HeightMap[Size * (Size + 1)], BaseData->HeightMap, Size + 1);

						// Correctly loaded
						Result = true;
					}

					// Clear heightmap buffer
					MemoryDeallocate(HeightMapBuffer);
				}

				// Close file
				fclose(File);

				// Return result of the loading operation
				return Result;
			}

			MemoryDeallocate(BaseData->HeightMap);
		}
	}

	return false;
}


////////////////////////////////////////////////////////////
/// Initialize base terrain properties
////////////////////////////////////////////////////////////
struct EntityType * TerrainBaseInitialize(struct EntityType * Entity, uinteger Index, uinteger HeightMapSize, uinteger HeightMapTile, uinteger PatchSize, struct Vector3f * Scale)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		// Load the heightmap
		if (TerrainBaseHeightMapLoad(BaseData, Index, HeightMapSize))
		{
			// Set terrain index
			BaseData->Index = Index;

			// Set heightmap size
			BaseData->HeightMapSize = HeightMapSize;

			// Set height map tile size
			if (HeightMapTile > 0)
			{
				BaseData->HeightMapTile = HeightMapTile;
			}
			else
			{
				BaseData->HeightMapTile = 1;
			}

			// Set patch size
			BaseData->PatchSize = PatchSize;

			// Set patch tile size
			BaseData->PatchTile = HeightMapSize / PatchSize;

			// Set scale of the terrain
			if (Scale)
			{
				Vector3fCopy(&BaseData->Scale, Scale);
			}
			else
			{
				Vector3fIndentity(&BaseData->Scale);
			}
		}

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a terrain base component
////////////////////////////////////////////////////////////
void TerrainBaseDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		// Clear heightmap
		TerrainBaseHeightMapClear(BaseData);
	}
}

////////////////////////////////////////////////////////////
/// Calculate quadtree nodes for a given patch size
////////////////////////////////////////////////////////////
uinteger TerrainBasePatchNodes(struct EntityType * Entity, uinteger Size)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		// At least one node in a quadtree
		uinteger Nodes = 1;
		uinteger NodesAtLevel = 1;
		uinteger PatchSizeIt = BaseData->PatchSize;

		while (Size > PatchSizeIt)
		{
			// Calculate nodes at current level
			NodesAtLevel <<= 2;

			// Increment them to the current counter
			Nodes += NodesAtLevel;

			// Iterate to the next patch level
			Size >>= 1;
		}

		return Nodes;
	}

	return TERRAIN_BASE_NODES_SIZE_INVALID;
}

////////////////////////////////////////////////////////////
/// Return number of quadtree nodes for the heightmap
////////////////////////////////////////////////////////////
uinteger TerrainBaseHeightMapNodesSize(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		return TerrainBasePatchNodes(Entity, BaseData->HeightMapSize);
	}

	return TERRAIN_BASE_NODES_SIZE_INVALID;
}

////////////////////////////////////////////////////////////
//< Get the height at desired position
////////////////////////////////////////////////////////////
float TerrainGetHeightAt(struct EntityType * Entity, float X, float Y)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		integer IntX, IntY;
		float RatioX, RatioY;
		float OppositeX, OppositeY;

		integer IndexList[4];
		float HeightMap[4];
		uinteger i;

		// Get height map size
		float FloatHeightMapSize = (float)(BaseData->HeightMapSize + 1);
		integer IntHeightMapSize = (BaseData->HeightMapSize + 1);

		// Calculate correct coordinates
		X = (X / BaseData->Scale.x) - 0.5f;
		Y = (Y / BaseData->Scale.y) - 0.5f;

		// Get integer coordinates
		IntX = (integer)floorf(X);
		IntY = (integer)floorf(Y);

		// Calculate ratio
		RatioX = X - (float)(IntX);
		RatioY = Y - (float)(IntY);

		// Calculate opposite
		OppositeX = 1.0f - RatioX;
		OppositeY = 1.0f - RatioY;

		// Calculate height map indexes
		IndexList[0] = IntX + IntHeightMapSize * IntY;
		IndexList[1] = (IntX + 1) + IntHeightMapSize * IntY;
		IndexList[2] = IntX + IntHeightMapSize * (IntY + 1);
		IndexList[3] = (IntX + 1) + IntHeightMapSize * (IntY + 1);

		// Assert if index list is valid
		for (i = 0; i < 4; ++i)
		{
			const integer IndexSize = sqr(IntHeightMapSize);

			if (IndexList[i] > 0 && IndexList[i] < IndexSize)
			{
				HeightMap[i] = (float)(BaseData->HeightMap[IndexList[i]]) * BaseData->Scale.z;
			}
			else
			{
				return 0.0f;
			}
		}

		// Return interpolated result
		return (((HeightMap[0] * OppositeX) + (HeightMap[1] * RatioX)) * OppositeY) +
			(((HeightMap[2] * OppositeX) + (HeightMap[3] * RatioX)) * RatioY);
	}

	return 0.0f;
}

////////////////////////////////////////////////////////////
/// Return number of quadtree nodes for the terrain
////////////////////////////////////////////////////////////
uinteger TerrainBaseNodesSize(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct TerrainComponentBaseData * BaseData = ComponentDataDerived(Base, struct TerrainComponentBaseData);

		return TerrainBasePatchNodes(Entity, BaseData->HeightMapSize * BaseData->HeightMapTile);
	}

	return TERRAIN_BASE_NODES_SIZE_INVALID;
}

////////////////////////////////////////////////////////////
/// Return the terrain base derived data size
////////////////////////////////////////////////////////////
uinteger TerrainComponentBaseSize()
{
	return sizeof(struct TerrainComponentBaseData);
}
