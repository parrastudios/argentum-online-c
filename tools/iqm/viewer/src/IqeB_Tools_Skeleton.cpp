/*
***********************************************************************************
 IqeB_Tools_Skeleton.cpp

 Skeleton utilities.

10.03.201r RR: * First editon of this file.

*/

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/gl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

/************************************************************************************
 * IqeB_SkelUpdateHierarchy()
 *
 * Update joint linking, hierarchy info and other things.
 */

void IqeB_SkelUpdateHierarchy( struct skel *pSkel)
{
  int i, j, k, l1, l2, iParent, BreakLoop;
  struct S_skel_joint *pParent, *pLast;

  // Reset hierarchy data

  for( i = 0; i < pSkel->joint_count; i++) {

    pSkel->j[ i].nChilds       = 0;
    pSkel->j[ i].iChildFirst   = -1;
    pSkel->j[ i].iChildLast    = -1;
    pSkel->j[ i].iChildLink    = -1;
    pSkel->j[ i].nParentsChain = 0;
    pSkel->j[ i].nIndent       = 0;
    pSkel->j[ i].iSymmetric    = -1;
  }

  // Gather hierarchy info

  for( i = 0; i < pSkel->joint_count; i++) {

    iParent = pSkel->j[ i].parent;

    if( iParent < 0) {    // is root joint
      continue;
    }

    if( iParent >= pSkel->joint_count || iParent >= MAXBONE) {  // security test range

      pSkel->j[ i].parent = -1; // PANIC action, flag as root joint
      continue;
    }

    pParent = pSkel->j + iParent;   // pointer to parent

    if( pParent->nChilds == 0) {    // first child of this parent

      pParent->iChildFirst = i;

    } else {                        // alreay have childs

      pLast = pSkel->j + pParent->iChildLast;   // pointer to last child of this parent

      pLast->iChildLink = i;        // update link of childs with same parent
    }

    pParent->iChildLast = i;

    pParent->nChilds += 1;          // have one more child
  }

  // Nesting level of childs

  for( ; ; ) {

    BreakLoop = true;               // set flag, can break main loop

    for( i = 0; i < pSkel->joint_count; i++) {

      iParent = pSkel->j[ i].parent;

      if( iParent < 0) {    // is root joint

        // keep nesting level of 0
        continue;
      }

      if( pSkel->j[ i].nParentsChain > 0) { // already have a nesting level

        continue;
      }

      pParent = pSkel->j + iParent;         // pointer to parent

      if( pParent->parent < 0) {            // the parent of the parent is a root bone

        // We have a nesting level of 1
        pSkel->j[ i].nParentsChain = 1;
        continue;
      }

      if( pParent->nParentsChain > 0) {     // the parent already has a nesting level

        // Our nesting level is one higher
        pSkel->j[ i].nParentsChain = pParent->nParentsChain + 1;
        continue;
      }

      // Have more to do in the next round

      BreakLoop = false;  // set flag, don't break main loop
    }

    if( BreakLoop) {      // break the main loop

      break;
    }
  }

  // Indent for diagrams

  for( ; ; ) {

    BreakLoop = true;               // set flag, can break main loop

    for( i = 0; i < pSkel->joint_count; i++) {

      iParent = pSkel->j[ i].parent;

      if( iParent < 0) {    // is root joint

        // keep indent 0
        continue;
      }

      if( pSkel->j[ i].nIndent > 0) {       // already have a nIndent

        continue;
      }

      pParent = pSkel->j + iParent;         // pointer to parent

      if( pParent->parent < 0) {            // the parent of the parent is a root bone

        // We have an nIndent of 1
        pSkel->j[ i].nIndent = 1;
        continue;
      }

      if( pParent->nIndent > 0) {          // the parent already has a nIndent

        pSkel->j[ i].nIndent = pParent->nIndent; // Starts with parent value

        if( pParent->nChilds > 1 ||       // parent has more than one child
            pParent->parent < 0 ||        // or parent parents is a root bone
            pSkel->j[ pParent->parent].nChilds > 1) { // or parents parent has more than 1

          pSkel->j[ i].nIndent += 1;      // Indent one more
        }
        continue;
      }

      // Have more to do in the next round

      BreakLoop = false;  // set flag, don't break main loop
    }

    if( BreakLoop) {      // break the main loop

      break;
    }
  }

  // Gather symmetric info

  for( i = 0; i < pSkel->joint_count; i++) {

    char *pName1, *pName2;

    pName1 = pSkel->j[ i].name;
    l1 = strlen( pName1);                 // Compare this

    for( j = i + 1; j < pSkel->joint_count; j++) {   // with all successors

      if( pSkel->j[ j].iSymmetric >= 0) {  // Already have a symetric

        continue;
      }

      pName2 = pSkel->j[ j].name;
      l2 = strlen( pName2);

      // Test difference of name in one L/R (for left/right) character only

      if( l1 == l2) {                           // have the same length

        int nSame, HaveLR;

        nSame = 0;
        HaveLR = false;

        for( k = 0; k < l1; k++) {

          if( pName1[ k] == pName2[ k]) {  // Same char

            nSame += 1;
          } else {

            if( (toupper( pName1[ k]) == 'L' && toupper( pName2[ k]) == 'R') ||
                (toupper( pName1[ k]) == 'R' && toupper( pName2[ k]) == 'L')) {

              HaveLR = true;   // Got a L/R pair
            }
          }
        }

        if( nSame == l1 - 1 && HaveLR) {   // Have a symmetric name

          pSkel->j[ i].iSymmetric = j;    // set reference
          pSkel->j[ j].iSymmetric = i;

          break;
        }
      }

      // Test difference of name in Left/Right string only

      if( l1 >= 4 && l1 + 1 == l2) {     // maybe 1. has LEFT, 2. has RIGHT

        int nSame, HaveLR, kBegin;

        // Test 1. for left
        HaveLR = false;
        for( k = 0; k <= l1 - 4; k++) {

          if( toupper( pName1[ k + 0]) == 'L' &&
              toupper( pName1[ k + 1]) == 'E' &&
              toupper( pName1[ k + 2]) == 'F' &&
              toupper( pName1[ k + 3]) == 'T') {

            HaveLR = true;
            kBegin = k;            // where LEFT begin
            break;
          }
        }

        if( HaveLR) {              // 1. has left

          nSame = 0;

          // test begin is the same
          for( k = 0; k < kBegin; k++) {

            if( pName1[ k] == pName2[ k]) {  // Same char

              nSame += 1;
            }
          }

          // Test 2. for right
          HaveLR = false;
          for( k = 0; k <= l1 - 4; k++) {

            if( toupper( pName2[ k + 0]) == 'R' &&
                toupper( pName2[ k + 1]) == 'I' &&
                toupper( pName2[ k + 2]) == 'G' &&
                toupper( pName2[ k + 3]) == 'H' &&
                toupper( pName2[ k + 4]) == 'T') {

              HaveLR = true;
              break;
            }
          }

          k += 4;

          // test end is the same

          for( ; k < l1; k++) {

            if( pName1[ k] == pName2[ k + 1]) {  // Same char

              nSame += 1;
            }
          }

          if( nSame == l1 - 4 && HaveLR) {   // Have a symmetric name

            pSkel->j[ i].iSymmetric = j;    // set reference
            pSkel->j[ j].iSymmetric = i;

            break;
          }
        }
      }

      if( l1 >= 5 && l1 - 1 == l2) {   // maybe 1. has RIGHT, 2. has LEFT

        int nSame, HaveLR, kBegin;

        // Test 1. for right
        HaveLR = false;
        for( k = 0; k <= l1 - 5; k++) {

          if( toupper( pName1[ k + 0]) == 'R' &&
              toupper( pName1[ k + 1]) == 'I' &&
              toupper( pName1[ k + 2]) == 'G' &&
              toupper( pName1[ k + 3]) == 'H' &&
              toupper( pName1[ k + 4]) == 'T') {

            HaveLR = true;
            kBegin = k;            // where LEFT begin
            break;
          }
        }

        if( HaveLR) {              // 1. has right

          nSame = 0;

          // test begin is the same
          for( k = 0; k < kBegin; k++) {

            if( pName1[ k] == pName2[ k]) {  // Same char

              nSame += 1;
            }
          }

          // Test 2. for left
          HaveLR = false;
          for( k = 0; k <= l1 - 5; k++) {

            if( toupper( pName2[ k + 0]) == 'L' &&
                toupper( pName2[ k + 1]) == 'E' &&
                toupper( pName2[ k + 2]) == 'F' &&
                toupper( pName2[ k + 3]) == 'T') {

              HaveLR = true;
              break;
            }
          }

          k += 4;

          // test end is the same

          for( ;k < l2; k++) {

            if( pName1[ k + 1] == pName2[ k]) {  // Same char

              nSame += 1;
            }
          }

          if( nSame == l1 - 5 && HaveLR) {   // Have a symmetric name

            pSkel->j[ i].iSymmetric = j;    // set reference
            pSkel->j[ j].iSymmetric = i;

            break;
          }
        }
      }
    }

    if( pSkel->j[ i].iSymmetric >= 0) {  // Have a symetric now

      continue;
    }
  }

  // Test for normalized skeleton (no rotaion and no zesize)

  pSkel->IsNormalized = pSkel->joint_count > 0;   // preset normalized if more than one joint

  for( i = 0; i < pSkel->joint_count; i++) {

    if( pSkel->pose[ i].scale[ 0] != 1.0 ||
        pSkel->pose[ i].scale[ 1] != 1.0 ||
        pSkel->pose[ i].scale[ 2] != 1.0 ||
        pSkel->pose[ i].rotation[ 0] != 0.0 ||
        pSkel->pose[ i].rotation[ 1] != 0.0 ||
        pSkel->pose[ i].rotation[ 2] != 0.0 ||
        (pSkel->pose[ i].rotation[ 3] != 1.0 && pSkel->pose[ i].rotation[ 3] != -1.0)) {

      pSkel->IsNormalized = false;           // is not normalized
      break;
    }
  }

  // Convert rotation quaterion to angles in degree

  for( i = 0; i < pSkel->joint_count; i++) {

    vec3 Angles;

    quat_to_rotation( pSkel->pose[ i].rotation, Angles);

    pSkel->pose[ i].angles[ 0] = RAD2DEG( Angles[ 0]);
    pSkel->pose[ i].angles[ 1] = RAD2DEG( Angles[ 1]);
    pSkel->pose[ i].angles[ 2] = RAD2DEG( Angles[ 2]);

    // Enshure angles in range -180.0  .. 180.0

    while( pSkel->pose[ i].angles[ 0] < -180.0) pSkel->pose[ i].angles[ 0] += 360.0;
    while( pSkel->pose[ i].angles[ 0] >  180.0) pSkel->pose[ i].angles[ 0] -= 360.0;
    while( pSkel->pose[ i].angles[ 1] < -180.0) pSkel->pose[ i].angles[ 1] += 360.0;
    while( pSkel->pose[ i].angles[ 1] >  180.0) pSkel->pose[ i].angles[ 1] -= 360.0;
    while( pSkel->pose[ i].angles[ 2] < -180.0) pSkel->pose[ i].angles[ 2] += 360.0;
    while( pSkel->pose[ i].angles[ 2] >  180.0) pSkel->pose[ i].angles[ 2] -= 360.0;

    // catch tiny values, clip them to 0
    if( pSkel->pose[ i].angles[ 0] > -0.0001 && pSkel->pose[ i].angles[ 0] < 0.0001) pSkel->pose[ i].angles[ 0] = 0.0;
    if( pSkel->pose[ i].angles[ 1] > -0.0001 && pSkel->pose[ i].angles[ 1] < 0.0001) pSkel->pose[ i].angles[ 1] = 0.0;
    if( pSkel->pose[ i].angles[ 2] > -0.0001 && pSkel->pose[ i].angles[ 2] < 0.0001) pSkel->pose[ i].angles[ 2] = 0.0;

    pSkel->pose[ i].KeyFlags = 0;   // All keyframe flags are off now
  }

  // Set color of bones without color

  unsigned int ColorFLTK;
  unsigned Col;
  float r, g, b;

  ColorFLTK = IqeB_DispCol_JOINTS;   // Get the joint color

  if( ColorFLTK & 0xffffff00) {      // Is RGB color
    Col = ColorFLTK;
  } else {
    Col = Fl::get_color( (Fl_Color)ColorFLTK);
  }

  r = ((Col >> 24) & 255) / 255.0;
  g = ((Col >> 16) & 255) / 255.0;
  b = ((Col >>  8) & 255) / 255.0;

  // walk the joints

  for( i = 0; i < pSkel->joint_count; i++) {

    if( pSkel->j[ i].TempBoneColor[ 0] == 0.0 &&
        pSkel->j[ i].TempBoneColor[ 1] == 0.0 &&
        pSkel->j[ i].TempBoneColor[ 2] == 0.0) {    // is black

      pSkel->j[ i].TempBoneColor[ 0] = r;           // set color
      pSkel->j[ i].TempBoneColor[ 1] = g;
      pSkel->j[ i].TempBoneColor[ 2] = b;
    }
  }
}

/************************************************************************************
 * IqeB_SkelMeasureSize()
 *
 * Calculate min/max box of skeleton and returns
 * size and center of this box.
 * NOTE: Need no precalucations. Onyl the skeleton data is used.
 *
 * return:   0: Done OK
 *         < 0: error
 *          -1: No skeleton data
 */

int IqeB_SkelMeasureSize( struct skel *pSkel, float center[3], float size[3])
{
	float minbox[3], maxbox[3];
	int iJoint, nMinMax;
  mat4 loc_bind_matrix[MAXBONE];
	mat4 abs_bind_matrix[MAXBONE];

  // get min max box

  minbox[0] = minbox[1] = minbox[2] = 0;
  maxbox[0] = maxbox[1] = maxbox[2] = 0;
  nMinMax = 0;

  // skeleton

	if( pSkel->joint_count > 0) {

    // do calculations ...

		calc_matrix_from_pose( loc_bind_matrix, pSkel->pose, pSkel->joint_count);
		calc_abs_matrix( abs_bind_matrix, loc_bind_matrix, pSkel);

		// ...

  	for( iJoint = 0; iJoint < pSkel->joint_count; iJoint++) {

      if( nMinMax == 0) {

    		vec_copy( minbox, abs_bind_matrix[ iJoint] + 12);
  	  	vec_copy( maxbox, abs_bind_matrix[ iJoint] + 12);
      } else {

        vec_minmax( minbox, maxbox, abs_bind_matrix[ iJoint] + 12);
      }

      nMinMax += 1;
  	}
	}

	center[0] = (minbox[0] + maxbox[0]) * 0.5;
	center[1] = (minbox[1] + maxbox[1]) * 0.5;
	center[2] = (minbox[2] + maxbox[2]) * 0.5;

	size[0] = maxbox[0] - minbox[0];
	size[1] = maxbox[1] - minbox[1];
	size[2] = maxbox[2] - minbox[2];

  return( nMinMax > 0 ? 0 : -1);  // Got some joints ?
}

/************************************************************************************
 * IqeB_SkelReset
 *
 * Resets the skeletons joint data
 *
 */

void IqeB_SkelReset( struct skel *pSkel)
{
  int iJoint;

  // Free joint names
  for( iJoint = 0; iJoint < pSkel->joint_count; iJoint++) {

    free( pSkel->j[ iJoint].name);
  }

  // Zero all data

  memset( pSkel, 0, sizeof(struct skel));
}

/************************************************************************************
 * IqeB_SkelJointFindByName
 *
 * Find joint by name
 *
 * return: >= 0: index of joint
 *          < 0: joint not found
 */

int IqeB_SkelJointFindByName( struct skel *pSkel, const char *pName)
{
  int iJoint;

  if( pName == NULL || pName[ 0] == '\0') {   // NO name

    return( -1);     // return not found
  }

  for( iJoint = 0; iJoint < pSkel->joint_count; iJoint++) {

    if( strcasecmp( pName, pSkel->j[ iJoint].name) == 0) {  // got it

      return( iJoint);
    }
  }

  return( -1);     // return not found
}

/************************************************************************************
 * IqeB_SkelJointAdd
 *
 * Add a joint, give is:
 *    pNameJoint    Name of the joint
 *    pNameParent   Name of the joints parent
 *                  If name is empty, this joint has a root joint.
 *                  If name is '-successor-', the parent is the last joint
 *                  in the joint table.
 *    location      Location offset to parent
 *
 * NOTE: Rotation information is set to no rotation an d
 *       scale is set to 1.
 *
 * return: >= 0: added joint, returned number is index of added joint
 *          < 0: joint table overflow
 */

int IqeB_SkelJointAdd( struct skel *pSkel, const char *pNameJoint,
                       const char *pNameParent, vec3 location)
{
  int iJoint;

  iJoint = pSkel->joint_count;

  if( iJoint >= MAXBONE) {   // Joint table is already full

    return( -1);             // return joint table overflow
  }

  pSkel->j[ iJoint].name = strdup( pNameJoint);           // Store name of joint

  if( pNameParent == NULL || pNameParent[ 0] == '\0') {   // NO parent

    pSkel->j[ iJoint].parent = -1;            // this is a root joint

  } else if( strcasecmp( pNameParent, "-successor-") == 0) {  // parent is last joint

    pSkel->j[ iJoint].parent = iJoint - 1;    // index of last joint

  } else {

    pSkel->j[ iJoint].parent = IqeB_SkelJointFindByName( pSkel, pNameParent); // look up parent
  }

  vec_copy( pSkel->pose[ iJoint].location, location);           // Copy location info
  vec_set( pSkel->pose[ iJoint].scale, 1.0, 1.0, 1.0);          // default scale
  quat_set_identity( pSkel->pose[ iJoint].rotation);            // default rotation

  pSkel->joint_count += 1;     // Have one more

  return( iJoint);             // return index of joint added
}

/************************************************************************************
 * IqeB_SkelLoadFromFile
 *
 * Load a skelton from the given file.
 *    pSkel          The loaded skelton data is placed here.
 *                   NOTE: If there is already a skeleton loaded here, reset
 *                         the data with IqeB_SkelReset().
 *                   NOTE: As first action done here, this data is zeroe.
 *    pFileName      Name of skelton file
 *    pErrorString   If != NULL, on error an error text is placed here.
 *
 * NOTE: Rotation information is set to no rotation and
 *       scale is set to 1.
 *
 * return:    0: Loading the skeleton is done without error
 *          < 0: Error loading the file
 */

int IqeB_SkelLoadFromFile( struct skel *pSkel, const char *pFileName, char *pErrorString)
{
  FILE *pFile;
	char line[ 512];
	char *s, *sp, *p, *pName, *pParent;
  int LineNr, ierr;
  vec3 location;

  // Zero all data

  memset( pSkel, 0, sizeof(struct skel));

  // open the skeleton file

  pFile = fopen( pFileName, "rt");

  if( pFile == NULL) {  //  can't open file

    if( pErrorString != NULL) {  // have pointer
      strcpy( pErrorString, "Can't open file.");
    }

    return( -1);
  }

  // Test for file Magic

	if (!fgets( line, sizeof line, pFile)) {

    if( pErrorString != NULL) {  // have pointer
      strcpy( pErrorString, "Can't load: read error.");
    }

		fclose( pFile);
    return( -1);
	}

	if (memcmp(line, SKELTON_FILE_MAGIC, strlen( SKELTON_FILE_MAGIC))) {

    if( pErrorString != NULL) {  // have pointer
      strcpy( pErrorString, "Can't load: bad skeleton magic.");
    }

		fclose( pFile);
    return( -1);
	}

  // Parse the skeleton file

  LineNr = 1;

  for( ; ; ) {

    // Try to read next line

		if (!fgets(line, sizeof line, pFile)) {
			break;
		}

    // Got the next line

    LineNr += 1;

		sp = line;

		if( line[ 0] == '#') {   // is comment line
      continue;
		}

		// Go for name

		s = parseword(&sp);
		if (s == NULL || *s == '\0' || *s == '#') {  // empty line or comment
			continue;
		}

		pName = s;   // Name of joint

		// cut off line after first comment character

    p = strchr( sp, '#');     // Begin of comment
    if( p != NULL) {

      *p = '\0';
    }

    // get position

    location[ 0] = parsefloat( &sp, 0.0);
		location[ 1] = parsefloat( &sp, 0.0);
		location[ 2] = parsefloat( &sp, 0.0);

    // Get optional parent

		pParent = parseword(&sp);

		// Add this joint

		ierr = IqeB_SkelJointAdd( pSkel, pName, pParent, location);

		if( ierr < 0) {  // got error

      if( pErrorString != NULL) {  // have pointer
        sprintf( pErrorString, "Can't load (line %d): bad data or joint table overflow", LineNr);
      }

      IqeB_SkelReset( pSkel);   // Reset what we have until now

		  fclose( pFile);
      return( -1);
		}
  }

  // finish up

  fclose( pFile);

  return( 0);   // return OK
}

/************************************************************************************
 * IqeB_SkelWriteToFile
 *
 * Write a skelton to file.
 *    pSkel          The loaded skelton data is placed here.
 *                   NOTE: If there is already a skeleton loaded here, reset
 *                         the data with IqeB_SkelReset().
 *                   NOTE: As first action done here, this data is zeroe.
 *    pFileName      Name of skelton file
 *    pSkelName      Name of skeleton (is basname of the file)
 *    pErrorString   If != NULL, on error an error text is placed here.
 *
 * return:    0: No errors
 *          < 0: Error write to file
 */

int IqeB_SkelWriteToFile( struct skel *pSkel, const char *pFileName, const char *pSkelName, char *pErrorString)
{
  FILE *pFile;
  int iJoint, iParent;
  char *pName, *pParent;
  char TempName[ 256];
  char TempParent[ 256];

  // open the skeleton file

  pFile = fopen( pFileName, "wt");

  if( pFile == NULL) {  //  can't open file

    if( pErrorString != NULL) {  // have pointer
      strcpy( pErrorString, "Can't create file.");
    }

    return( -1);
  }

  // write to file

  fprintf( pFile, "%s\n", SKELTON_FILE_MAGIC);   // Our magic header
  fprintf( pFile, "#\n");
  fprintf( pFile, "# File: %s.txt\n", pSkelName);
  fprintf( pFile, "#\n");
  fprintf( pFile, "\n");
  fprintf( pFile, "# name of joint                   X              Y              Z  parent name\n");
  fprintf( pFile, "\n");

  for( iJoint = 0; iJoint < pSkel->joint_count; iJoint++) {

    // name

    pName = pSkel->j[ iJoint].name;
    if( pName == NULL || pName[0] == '\0') {

      sprintf( TempName, "Joint-%03d", iJoint + 1);
      pName = TempName;
    }

    // parent

    iParent = pSkel->j[ iJoint].parent;

    if( iParent < 0 || iParent >= pSkel->joint_count) {     // parent index has a problem

      pParent = (char *)"";

    } else if( iParent == iJoint - 1 && pSkel->j[ iParent].nChilds == 1) { // An single successor

      pParent = (char *)"-successor-";

    } else {

      pParent = pSkel->j[ iParent].name;
      if( pParent == NULL || pParent[0] == '\0') {

        sprintf( TempParent, "Joint-%03d", iParent + 1);
        pParent = TempParent;
      }
    }

    fprintf( pFile, "%-20s  %13g  %13g  %13g  %-20s  # %3d\n",
             pName,
             pSkel->pose[ iJoint].location[ 0],
             pSkel->pose[ iJoint].location[ 1],
             pSkel->pose[ iJoint].location[ 2],
             pParent, iJoint);
  }

  fprintf( pFile, "\n");
  fprintf( pFile, "# End Of File\n");

  // finish up

  fclose( pFile);

  return( 0);   // return OK
}

/************************************************************************************
 * IqeB_SkelAdjustSizeToMesh
 *
 * Adjust the skeleton to the size of the mesh.
 *
 */
void IqeB_SkelAdjustSizeToMesh( struct skel *pSkel, struct mesh *pMesh)
{
  float center_skel[3], size_skel[3];
  float center_mesh[3], size_mesh[3];
  float ScaleBy, TranslateBy;
  int i;

  if( pSkel->joint_count <= 0) {   // have no skeleton

    return;
  }

  if( pMesh->vertex_count <= 0) {  // have no mesh

    return;
  }

  if( IqeB_SkelMeasureSize( pSkel, center_skel, size_skel) < 0) {  // have error

    return;
  }

  if( IqeB_MeshMeasureSize( pMesh, center_mesh, size_mesh) < 0) {  // have error

    return;
  }

  // we Adjust according to z size.

  if( size_skel[2] <= 0.0001 || size_mesh[2] <= 0.0001) {   // Too small

    return;
  }

  // Scale skeleton size to 90% of the mesh size

  ScaleBy = (size_mesh[2] * 0.9) / size_skel[2] ;

  //
  // scale the skeleton
  //

  for( i = 0; i < pSkel->joint_count; i++) {

    pSkel->pose[ i].location[ 0] *= ScaleBy;
    pSkel->pose[ i].location[ 1] *= ScaleBy;
    pSkel->pose[ i].location[ 2] *= ScaleBy;
  }

  // remeasure the skeleton

  if( IqeB_SkelMeasureSize( pSkel, center_skel, size_skel) < 0) {  // have error

    return;
  }

  //
  // translate the skeleton center to the mesh center
  //

  TranslateBy = center_mesh[ 2] - center_skel[ 2];

  for( i = 0; i < pSkel->joint_count; i++) {

    if( pSkel->j[ i].parent < 0) {   // have NO parent

      pSkel->pose[ i].location[ 2] += TranslateBy;
    }
  }
}

/************************************************************************************
 * IqeB_SkelJointManipulate
 *
 * Manipulation of joints, see IQE_SKEL_JOINT_xxx defines.
 *
 * return:  < 0:  error, something not OK with arguments
 *         >= 0:  OK, joint list has changed.
 *
 */

int IqeB_SkelJointManipulate( struct skel *pSkel, int Action, int iJointArg)
{
  int iJoint;
  char TempString[ 256];

  if( pSkel == NULL) {   // security test

    return( -1);         // no skeleton data
  }

  // dispatcher

  switch( Action) {
  case IQE_SKEL_JOINT_ADD_CHILD:   // Add child to name joint
    if( iJointArg < 0 || iJointArg >= pSkel->joint_count ||  // out of range
        pSkel->joint_count >= MAXBONE) {                     // joint list is full

      return( -2);    // Joint index out of range or joint list is full
    }

    iJoint = pSkel->joint_count;   // New joint

    sprintf( TempString, "Joint-%03d", iJoint + 1);  // Name for this joint
    pSkel->j[ iJoint].name = strdup( TempString);    // Store name of joint
    pSkel->j[ iJoint].parent = iJointArg;            // Joint argument is parent

    vec_set( pSkel->pose[ iJoint].location, 0.0, 0.0, 0.0);       // Set location info
    vec_set( pSkel->pose[ iJoint].scale, 1.0, 1.0, 1.0);          // default scale
    quat_set_identity( pSkel->pose[ iJoint].rotation);            // default rotation

    pSkel->joint_count += 1;     // Have one more

    return( iJoint);             // return index of joint added
    break;

  case IQE_SKEL_JOINT_ADD_ROOT:    // Add new root joint
    if( pSkel->joint_count >= MAXBONE) {                     // joint list is full

      return( -2);    // Joint index out of range or joint list is full
    }

    iJoint = pSkel->joint_count;   // New joint

    sprintf( TempString, "Joint-%03d", iJoint + 1);  // Name for this joint
    pSkel->j[ iJoint].name = strdup( TempString);    // Store name of joint
    pSkel->j[ iJoint].parent = -1;                   // this is a root joint

    vec_set( pSkel->pose[ iJoint].location, 0.0, 0.0, 0.0);       // Set location info
    vec_set( pSkel->pose[ iJoint].scale, 1.0, 1.0, 1.0);          // default scale
    quat_set_identity( pSkel->pose[ iJoint].rotation);            // default rotation

    pSkel->joint_count += 1;     // Have one more

    return( iJoint);             // return index of joint added
    break;

  case IQE_SKEL_JOINT_DEL_THIS:    // Delete named joint

    if( iJointArg < 0 || iJointArg >= pSkel->joint_count) {  // out of range

      return( -2);    // Joint index out of range or joint list is full
    }

    // Look for childs of this joint

    for( iJoint = 0; iJoint < pSkel->joint_count; iJoint++) {

      if( pSkel->j[ iJoint].parent == iJointArg) {  // This is a child of this joint

        pSkel->j[ iJoint].parent = pSkel->j[ iJointArg].parent;  // Update parent
        vec_add_to( pSkel->pose[ iJoint].location, pSkel->pose[ iJointArg].location);  // Add translation of parent
      }
    }

    // free joint name
    free( pSkel->j[ iJointArg].name);

    // copy down data

    iJoint = pSkel->joint_count - iJointArg - 1;  // Amount of joints to copy

    if( iJoint > 0) {   // have to copy data

      memcpy( pSkel->j + iJointArg, pSkel->j + iJointArg + 1, iJoint * sizeof( struct S_skel_joint));
      memcpy( pSkel->pose + iJointArg, pSkel->pose + iJointArg + 1, iJoint * sizeof( struct pose));
    }

    // have one joint less

    pSkel->joint_count -= 1;

    // Must update parents after copy down

    for( iJoint = 0; iJoint < pSkel->joint_count; iJoint++) {

      if( pSkel->j[ iJoint].parent > iJointArg) {   // Was copied down

        pSkel->j[ iJoint].parent -= 1;              // One less
      }
    }

    // return next joint number (if possible)

    iJoint = iJointArg;   // This is the next joint (copied down)

    if( iJoint >= pSkel->joint_count) {   // after last

      iJoint = pSkel->joint_count - 1;    // return last joint (or -1 if empty)
    }

    return( iJoint);             // return index of next joint
    break;

  case IQE_SKEL_JOINT_DEL_CHILDS:  // Delete named joint and all it's childs


    if( iJointArg < 0 || iJointArg >= pSkel->joint_count) {  // out of range

      return( -2);    // Joint index out of range or joint list is full
    }

    // Mark this joint for deletion

    pSkel->j[ iJointArg].parent = -4711;    // Use parent as flag for deletion

    // Look for childs of deleted childs, iterative delete

    int BreakLoop, iParent;

    for( ; ; ) {

      BreakLoop = true;

      for( iJoint = 0; iJoint < pSkel->joint_count; iJoint++) {

        iParent = pSkel->j[ iJoint].parent;
        if( iParent >= 0 && iParent < pSkel->joint_count &&  // Still valid parent
            pSkel->j[ iParent].parent == -4711) {            // is flagged for deleation

          pSkel->j[ iJoint].parent = -4711;    // Use parent as flag for deletion
          BreakLoop = false;                   // Don't break loop
        }
      }

      if( BreakLoop) {   // Break iteration loop
        break;
      }
    }

    // Copy down not delete entries

	  int JointNewNumber[ MAXBONE]; // renumber of joints
    int iDst;

    iDst = 0;

    for( iJoint = 0; iJoint < pSkel->joint_count; iJoint++) {

      if( pSkel->j[ iJoint].parent == -4711) { // is flagged for deleation

        // free joint name
        free( pSkel->j[ iJoint].name);

        JointNewNumber[ iJoint] = -1;          // Set deleted (or root joint)

      } else {

        JointNewNumber[ iJoint] = iDst;        // Orignal number is changed to this

        if( iDst != iJoint) {      // have to copy

          memcpy( pSkel->j + iDst, pSkel->j + iJoint, sizeof( struct S_skel_joint));
          memcpy( pSkel->pose + iDst, pSkel->pose + iJoint, sizeof( struct pose));
        }

        iDst += 1;      // Have one copied down
      }
    }

    // Must update parents after copy down

    for( iJoint = 0; iJoint < iDst; iJoint++) {  // loop over the new joint count

      iParent = pSkel->j[ iJoint].parent;

      if( iParent >= 0) {                   // has parent

        if( iParent < pSkel->joint_count) { // security test of range

          pSkel->j[ iJoint].parent = JointNewNumber[ iParent]; // remap changed parent
        } else {

          pSkel->j[ iJoint].parent = -1; // emergency action, set root joint
        }
      }
    }

    // Set new joint count

    pSkel->joint_count = iDst;

    // return next joint number (if possible)

    iJoint = iJointArg;   // This is the next joint (copied down)

    if( iJoint >= pSkel->joint_count) {   // after last

      iJoint = pSkel->joint_count - 1;    // return last joint (or -1 if empty)
    }

    return( iJoint);             // return index of next joint

    break;
  }

  return( -9);   // Action code not known
}

/****************************** End Of File ******************************/
