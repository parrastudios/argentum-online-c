/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_IOHELPER_H
#define SYSTEM_IOHELPER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Config.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <malloc.h>

#if defined(SYSTEM_LINUX) || defined(SYSTEM_FREEBSD)

	#include <unistd.h>

#endif

////////////////////////////////////////////////////////////
// Defines
////////////////////////////////////////////////////////////
#ifdef SYSTEM_WINDOWS
	#define printf	printf_s
	#define sprintf	IOHelperStrPrint
	#define strcpy	IOHelperStrCpy
	#define sscanf	IOHelperSscan
#endif

#define sreadfield	IOHelperReadField

////////////////////////////////////////////////////////////
// String print helper method
////////////////////////////////////////////////////////////
void IOHelperStrPrint(char * str, const char * format, ...);

////////////////////////////////////////////////////////////
// String copy helper method
////////////////////////////////////////////////////////////
void IOHelperStrCpy(char * destination, const char * source);

////////////////////////////////////////////////////////////
// Sscan helper method
////////////////////////////////////////////////////////////
Int32 IOHelperSscan(const char * str, const char * format, ...);

////////////////////////////////////////////////////////////
// Open helper method
////////////////////////////////////////////////////////////
bool IOHelperOpen(FILE** pFile, const char * filename, const char * mode);

////////////////////////////////////////////////////////////
// Read field helper method
////////////////////////////////////////////////////////////
void IOHelperReadField(char * Destination, char * Source, char Separator, UInt32 Position);

#endif // SYSTEM_IOHELPER_H