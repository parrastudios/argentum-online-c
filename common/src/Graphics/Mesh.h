/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_MESH_H
#define GRAPHICS_MESH_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/Vector.h>
#include <Math/Geometry/Matrix.h>
#include <Graphics/Device.h>
#include <Graphics/MeshManager.h>
#include <World/ModelLoader.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define MODEL_RENDER_GL1        0x00
#define MODEL_RENDER_GL2        0x01
#define MODEL_RENDER_GL3        0x02

#define MODEL_RENDER_DEFAULT    MODEL_RENDER_GL1

#if (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL2)

        // ...

#elif (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL3)

    enum MeshVertexBufferId
    {
        MESH_INDEX_BUFFER_ID        = 0x00,
        MESH_POSITION_BUFFER_ID     = 0x01,
        MESH_NORMAL_BUFFER_ID       = 0x02,
        MESH_TEX_COORD_BUFFER_ID    = 0x03,
        MESH_WVP_MAT_BUFFER_ID      = 0x04,
        MESH_WORLD_MAT_BUFFER_ID    = 0x05,

        MESH_VERTEX_BUFFER_ID_SIZE
    };

#endif

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct MeshVertexType
{
	GLfloat Position[3];
	GLfloat Normal[3];
	GLfloat TexCoord[2];
	GLubyte Color[4];
	GLfloat Tangent[4];
	GLubyte BlendIndex[4];
	GLubyte BlendWeight[4];
};

struct MeshType
{
	MeshIdType Index;

    #if (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL2)

        GLuint EBO;
        GLuint VBO;
        GLuint UBO;
        GLint UBOSize;
        GLint BoneMatrixOffset;

    #elif (MODEL_RENDER_DEFAULT == MODEL_RENDER_GL3)

        GLuint VAO;
        GLuint Buffers[MESH_VERTEX_BUFFER_ID_SIZE];

    #endif

    GLuint TextureInvalid;
    Vector TextureList;
    Vector EntryList;
	struct MeshVertexType * VertexList;
	uinteger VertexListSize;
	uint32 * IndexList;
	uinteger IndexListSize;
};

////////////////////////////////////////////////////////////
// Methods
///////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a basic static mesh
////////////////////////////////////////////////////////////
struct MeshType * MeshCreate(struct ModelResourceType * Resource);

////////////////////////////////////////////////////////////
/// Initialize a basic static mesh
////////////////////////////////////////////////////////////
void MeshInitialize(struct MeshType * Mesh);

////////////////////////////////////////////////////////////
/// Clear a basic static mesh
////////////////////////////////////////////////////////////
void MeshClear(struct MeshType * Mesh);

////////////////////////////////////////////////////////////
/// Copy a instance of basic static mesh vertex list
////////////////////////////////////////////////////////////
struct MeshVertexType * MeshCopyVertexList(struct MeshType * Mesh);

////////////////////////////////////////////////////////////
/// Destroy a basic static mesh
////////////////////////////////////////////////////////////
void MeshDestroy(struct MeshType * Mesh);

////////////////////////////////////////////////////////////
/// Render a basic static mesh
////////////////////////////////////////////////////////////
void MeshRender(struct MeshType * Mesh);

////////////////////////////////////////////////////////////
/// Render a basic static mesh instanced
////////////////////////////////////////////////////////////
void MeshRenderInstanced(struct MeshType * Mesh,
    uinteger Instances, struct Matrix4f * MVPMatrixList, struct Matrix4f * WorldMatrixList);

#endif // GRAPHICS_MESH_H
