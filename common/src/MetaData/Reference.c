/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/Reference.h>
#include <MetaData/Object.h>
#include <MetaData/Function.h>

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef void (*ReferenceCountIncrementFunc)(MetadataScopeType, MetadataStorageIndexType);
typedef bool (*ReferenceCountConsultorFunc)(MetadataScopeType, MetadataStorageIndexType);
typedef void (*ReferenceCountDecrementFunc)(MetadataScopeType, MetadataStorageIndexType);

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
static const ReferenceCountIncrementFunc ReferenceCountIncrement[METADATA_REFERENCE_TYPE_SIZE] =
{
	ArrayInit(METADATA_REFERENCE_TYPE_VALUE, &MetadataValueReference),
	ArrayInit(METADATA_REFERENCE_TYPE_REFERENCE, &MetadataReferenceReference),
	ArrayInit(METADATA_REFERENCE_TYPE_OBJECT, &MetadataFunctionReference),
	ArrayInit(METADATA_REFERENCE_TYPE_FUNCTION, &MetadataObjectReference)
};

static const ReferenceCountConsultorFunc ReferenceCountConsultor[METADATA_REFERENCE_TYPE_SIZE] =
{
	ArrayInit(METADATA_REFERENCE_TYPE_VALUE, &MetadataValueIsReferenced),
	ArrayInit(METADATA_REFERENCE_TYPE_REFERENCE, &MetadataReferenceIsReferenced),
	ArrayInit(METADATA_REFERENCE_TYPE_OBJECT, &MetadataFunctionIsReferenced),
	ArrayInit(METADATA_REFERENCE_TYPE_FUNCTION, &MetadataObjectIsReferenced)
};

static const ReferenceCountDecrementFunc ReferenceCountDecrement[METADATA_REFERENCE_TYPE_SIZE] =
{
	ArrayInit(METADATA_REFERENCE_TYPE_VALUE, &MetadataValueDereference),
	ArrayInit(METADATA_REFERENCE_TYPE_REFERENCE, &MetadataReferenceDereference),
	ArrayInit(METADATA_REFERENCE_TYPE_OBJECT, &MetadataFunctionDereference),
	ArrayInit(METADATA_REFERENCE_TYPE_FUNCTION, &MetadataObjectDereference)
};

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct MetadataReferenceType
{
	MetadataStorageIndexType		Index;		///< Index referencing to itself
	MetadataReferenceCountType		RefCount;	///< References to this reference
	MetadataReferenceTypeInfo		Type;		///< Internal type of reference

	union
	{
		MetadataStorageIndexType	Index;
		MetadataValueType			Value;
		MetadataReferenceType		Reference;
		MetadataObjectType			Object;
		MetadataFunctionType		Function;
	} Pointer;									///< Pointer to data implementation

};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a serial reference
////////////////////////////////////////////////////////////
MetadataReferenceType MetadataReferenceCreate(MetadataScopeType Scope, MetadataReferenceTypeInfo Type, MetadataStorageIndexType Index)
{
	// Allocate memory for reference
	MetadataReferenceType Reference = MetadataStorageAllocateReference(MetadataScopeGetStorage(Scope), sizeof(struct MetadataReferenceType));

	if (Reference != METADATA_STORAGE_INDEX_INVALID)
	{
		MetadataReferenceTypeImpl ReferenceImpl = MetadataStorageGetReference(MetadataScopeGetStorage(Scope), Reference);

		if (ReferenceImpl)
		{
			// Set properties
			ReferenceImpl->Index = Reference;
			ReferenceImpl->RefCount = 0;
			ReferenceImpl->Type = Type;
			ReferenceImpl->Pointer.Index = Index;

			// Increment reference
			ReferenceCountIncrement[ReferenceImpl->Type](Scope, ReferenceImpl->Pointer.Index);
		}
		else
		{
			MetadataStorageDeallocateReference(MetadataScopeGetStorage(Scope), Reference);
		}
	}

	return Reference;
}

////////////////////////////////////////////////////////////
/// Create a serial reference signature
////////////////////////////////////////////////////////////
MetadataReferenceType MetadataReferenceSignature(MetadataScopeType Scope, MetadataReferenceTypeInfo Type)
{
	// Allocate memory for reference
	MetadataReferenceType Reference = MetadataStorageAllocateReference(MetadataScopeGetStorage(Scope), sizeof(struct MetadataReferenceType));

	if (Reference != METADATA_STORAGE_INDEX_INVALID)
	{
		MetadataReferenceTypeImpl ReferenceImpl = MetadataStorageGetReference(MetadataScopeGetStorage(Scope), Reference);

		if (ReferenceImpl)
		{
			// Set properties
			ReferenceImpl->Index = Reference;
			ReferenceImpl->RefCount = 0;
			ReferenceImpl->Type = Type;
			ReferenceImpl->Pointer.Index = METADATA_STORAGE_INDEX_INVALID;
		}
		else
		{
			MetadataStorageDeallocateReference(MetadataScopeGetStorage(Scope), Reference);
		}
	}

	return Reference;
}

////////////////////////////////////////////////////////////
/// Copy a reference
////////////////////////////////////////////////////////////
MetadataReferenceType MetadataReferenceCopy(MetadataScopeType Scope, MetadataReferenceType Reference)
{
	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Set a reference to a resource
////////////////////////////////////////////////////////////
void MetadataReferenceSetPointer(MetadataScopeType Scope, MetadataReferenceType Reference, MetadataStorageIndexType Index)
{
	if (Index != METADATA_STORAGE_INDEX_INVALID)
	{
		MetadataReferenceTypeImpl ReferenceImpl = MetadataStorageGetReference(MetadataScopeGetStorage(Scope), Reference);

		if (ReferenceImpl)
		{
			// Dereference the old data if any
			ReferenceCountDecrement[ReferenceImpl->Type](Scope, ReferenceImpl->Pointer.Index);

			// Change pointer to new index
			ReferenceImpl->Pointer.Index = Index;

			// Reference new data if available
			ReferenceCountIncrement[ReferenceImpl->Type](Scope, ReferenceImpl->Pointer.Index);
		}
	}
}

////////////////////////////////////////////////////////////
/// Get the data that reference is pointing to
////////////////////////////////////////////////////////////
MetadataStorageIndexType MetadataReferenceGetPointer(MetadataScopeType Scope, MetadataReferenceType Reference)
{
	MetadataReferenceTypeImpl ReferenceImpl = MetadataStorageGetReference(MetadataScopeGetStorage(Scope), Reference);

	if (ReferenceImpl)
	{
		return ReferenceImpl->Pointer.Index;
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Get the type of pointer (type-safety)
////////////////////////////////////////////////////////////
MetadataReferenceTypeInfo MetadataReferenceTypeOf(MetadataScopeType Scope, MetadataReferenceType Reference)
{
	MetadataReferenceTypeImpl ReferenceImpl = MetadataStorageGetReference(MetadataScopeGetStorage(Scope), Reference);

	if (ReferenceImpl)
	{
		return ReferenceImpl->Type;
	}

	return METADATA_REFERENCE_TYPE_INVALID;
}

////////////////////////////////////////////////////////////
/// Reference a reference
////////////////////////////////////////////////////////////
void MetadataReferenceReference(MetadataScopeType Scope, MetadataReferenceType Reference)
{
	MetadataReferenceTypeImpl ReferenceImpl = MetadataStorageGetReference(MetadataScopeGetStorage(Scope), Reference);

	if (ReferenceImpl)
	{
		++ReferenceImpl->RefCount;
	}
}

////////////////////////////////////////////////////////////
/// Check if reference is referenced
////////////////////////////////////////////////////////////
bool MetadataReferenceIsReferenced(MetadataScopeType Scope, MetadataReferenceType Reference)
{
	MetadataReferenceTypeImpl ReferenceImpl = MetadataStorageGetReference(MetadataScopeGetStorage(Scope), Reference);

	if (ReferenceImpl)
	{
		return (ReferenceImpl->RefCount > 0);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Dereference a reference
////////////////////////////////////////////////////////////
void MetadataReferenceDereference(MetadataScopeType Scope, MetadataReferenceType Reference)
{
	MetadataReferenceTypeImpl ReferenceImpl = MetadataStorageGetReference(MetadataScopeGetStorage(Scope), Reference);

	if (ReferenceImpl)
	{
		--ReferenceImpl->RefCount;
	}
}

////////////////////////////////////////////////////////////
/// Destroy a reference
////////////////////////////////////////////////////////////
void MetadataReferenceDestroy(MetadataScopeType Scope, MetadataReferenceType Reference)
{
	MetadataReferenceTypeImpl ReferenceImpl = MetadataStorageGetReference(MetadataScopeGetStorage(Scope), Reference);

	if (ReferenceImpl)
	{
		// Increment reference
		ReferenceCountDecrement[ReferenceImpl->Type](Scope, ReferenceImpl->Pointer.Index);

		// Set properties
		ReferenceImpl->Index = METADATA_STORAGE_INDEX_INVALID;
		ReferenceImpl->RefCount = 0;
		ReferenceImpl->Type = METADATA_REFERENCE_TYPE_INVALID;
		ReferenceImpl->Pointer.Index = METADATA_STORAGE_INDEX_INVALID;
	}

	MetadataStorageDeallocateReference(MetadataScopeGetStorage(Scope), Reference);
}
