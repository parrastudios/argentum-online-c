-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 13, 2016 at 11:37 AM
-- Server version: 5.6.28-log
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aocdb`
--
CREATE DATABASE IF NOT EXISTS `aocdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `aocdb`;

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `AccountExists`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `AccountExists` (IN `p_AccountId` INT UNSIGNED)  NO SQL
BEGIN

DECLARE p_Return INT UNSIGNED;

SELECT 1 INTO p_Return
FROM Account
WHERE Account.AccountId = p_AccountId;

SELECT p_Return;

END$$

DROP PROCEDURE IF EXISTS `AccountLogin`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `AccountLogin` (IN `p_email` VARCHAR(255), IN `p_password` CHAR(10))  READS SQL DATA
    SQL SECURITY INVOKER
BEGIN

SELECT
  Account.AccountId, Account.Email
FROM
  Account
WHERE
  Account.Password = p_password AND Account.Email = p_email;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `Account`
--

DROP TABLE IF EXISTS `Account`;
CREATE TABLE `Account` (
  `AccountId` int(10) UNSIGNED NOT NULL,
  `Password` char(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Ip` varbinary(16) NOT NULL,
  `CreationTime` int(10) UNSIGNED NOT NULL,
  `ClientOnline` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Account`
--

INSERT INTO `Account` (`AccountId`, `Password`, `Email`, `Ip`, `CreationTime`, `ClientOnline`) VALUES
(1, 'asdasd', 'asd@asd.asd', '', 1457042654, 0),
(2, 'Nothing', 'Pepito', 0x01, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `BuffPrototype`
--

DROP TABLE IF EXISTS `BuffPrototype`;
CREATE TABLE `BuffPrototype` (
  `BuffPrototypeId` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Character`
--

DROP TABLE IF EXISTS `Character`;
CREATE TABLE `Character` (
  `CharacterId` int(10) UNSIGNED NOT NULL,
  `AlginId` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `NewsBrief`
--

DROP TABLE IF EXISTS `NewsBrief`;
CREATE TABLE `NewsBrief` (
  `NewsId` smallint(5) UNSIGNED NOT NULL,
  `Title` varchar(125) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `CreationTime` int(10) UNSIGNED NOT NULL,
  `Language` char(2) NOT NULL,
  `Active` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `NewsFull`
--

DROP TABLE IF EXISTS `NewsFull`;
CREATE TABLE `NewsFull` (
  `NewsId` smallint(5) UNSIGNED NOT NULL,
  `Body` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Npc`
--

DROP TABLE IF EXISTS `Npc`;
CREATE TABLE `Npc` (
  `NpcId` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ObjectInstance`
--

DROP TABLE IF EXISTS `ObjectInstance`;
CREATE TABLE `ObjectInstance` (
  `ObjectInstanceId` int(10) UNSIGNED NOT NULL,
  `ObjectPrototypeId` int(10) UNSIGNED NOT NULL,
  `Amount` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ObjectPrototype`
--

DROP TABLE IF EXISTS `ObjectPrototype`;
CREATE TABLE `ObjectPrototype` (
  `ObjectPrototypeId` int(10) UNSIGNED NOT NULL,
  `Type` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Player`
--

DROP TABLE IF EXISTS `Player`;
CREATE TABLE `Player` (
  `PlayerId` int(10) UNSIGNED NOT NULL,
  `AccountId` int(10) UNSIGNED NOT NULL,
  `Name` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Spell`
--

DROP TABLE IF EXISTS `Spell`;
CREATE TABLE `Spell` (
  `SpellId` int(10) UNSIGNED NOT NULL,
  `Name` varchar(20) NOT NULL,
  `SpellWord` varchar(128) NOT NULL,
  `Description` varchar(128) NOT NULL,
  `Category` int(10) UNSIGNED NOT NULL,
  `Type` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `SpellBuff`
--

DROP TABLE IF EXISTS `SpellBuff`;
CREATE TABLE `SpellBuff` (
  `BuffId` int(10) UNSIGNED NOT NULL,
  `SpellId` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `SpellType`
--

DROP TABLE IF EXISTS `SpellType`;
CREATE TABLE `SpellType` (
  `SpellId` int(10) UNSIGNED NOT NULL,
  `Type` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `SpellTypePrototype`
--

DROP TABLE IF EXISTS `SpellTypePrototype`;
CREATE TABLE `SpellTypePrototype` (
  `SpellTypeId` int(10) UNSIGNED NOT NULL,
  `Name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Account`
--
ALTER TABLE `Account`
  ADD PRIMARY KEY (`AccountId`);

--
-- Indexes for table `BuffPrototype`
--
ALTER TABLE `BuffPrototype`
  ADD PRIMARY KEY (`BuffPrototypeId`);

--
-- Indexes for table `Character`
--
ALTER TABLE `Character`
  ADD PRIMARY KEY (`CharacterId`);

--
-- Indexes for table `NewsBrief`
--
ALTER TABLE `NewsBrief`
  ADD PRIMARY KEY (`NewsId`);

--
-- Indexes for table `NewsFull`
--
ALTER TABLE `NewsFull`
  ADD PRIMARY KEY (`NewsId`);

--
-- Indexes for table `Npc`
--
ALTER TABLE `Npc`
  ADD PRIMARY KEY (`NpcId`);

--
-- Indexes for table `ObjectInstance`
--
ALTER TABLE `ObjectInstance`
  ADD PRIMARY KEY (`ObjectInstanceId`),
  ADD KEY `ObjectPrototypeId` (`ObjectPrototypeId`);

--
-- Indexes for table `ObjectPrototype`
--
ALTER TABLE `ObjectPrototype`
  ADD PRIMARY KEY (`ObjectPrototypeId`);

--
-- Indexes for table `Player`
--
ALTER TABLE `Player`
  ADD PRIMARY KEY (`PlayerId`),
  ADD KEY `AccountId` (`AccountId`);

--
-- Indexes for table `Spell`
--
ALTER TABLE `Spell`
  ADD PRIMARY KEY (`SpellId`),
  ADD KEY `Type` (`Type`);

--
-- Indexes for table `SpellBuff`
--
ALTER TABLE `SpellBuff`
  ADD PRIMARY KEY (`BuffId`),
  ADD KEY `SpellId` (`SpellId`);

--
-- Indexes for table `SpellType`
--
ALTER TABLE `SpellType`
  ADD PRIMARY KEY (`SpellId`),
  ADD KEY `Type` (`Type`);

--
-- Indexes for table `SpellTypePrototype`
--
ALTER TABLE `SpellTypePrototype`
  ADD PRIMARY KEY (`SpellTypeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Account`
--
ALTER TABLE `Account`
  MODIFY `AccountId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `BuffPrototype`
--
ALTER TABLE `BuffPrototype`
  MODIFY `BuffPrototypeId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Character`
--
ALTER TABLE `Character`
  MODIFY `CharacterId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `NewsBrief`
--
ALTER TABLE `NewsBrief`
  MODIFY `NewsId` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `NewsFull`
--
ALTER TABLE `NewsFull`
  MODIFY `NewsId` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Npc`
--
ALTER TABLE `Npc`
  MODIFY `NpcId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ObjectInstance`
--
ALTER TABLE `ObjectInstance`
  MODIFY `ObjectInstanceId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Player`
--
ALTER TABLE `Player`
  MODIFY `PlayerId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Spell`
--
ALTER TABLE `Spell`
  MODIFY `SpellId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `SpellTypePrototype`
--
ALTER TABLE `SpellTypePrototype`
  MODIFY `SpellTypeId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `BuffPrototype`
--
ALTER TABLE `BuffPrototype`
  ADD CONSTRAINT `BuffPrototype_ibfk_1` FOREIGN KEY (`BuffPrototypeId`) REFERENCES `SpellBuff` (`BuffId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `NewsFull`
--
ALTER TABLE `NewsFull`
  ADD CONSTRAINT `NewsFull_ibfk_1` FOREIGN KEY (`NewsId`) REFERENCES `NewsBrief` (`NewsId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Npc`
--
ALTER TABLE `Npc`
  ADD CONSTRAINT `Npc_ibfk_1` FOREIGN KEY (`NpcId`) REFERENCES `Character` (`CharacterId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ObjectInstance`
--
ALTER TABLE `ObjectInstance`
  ADD CONSTRAINT `ObjectInstance_ibfk_1` FOREIGN KEY (`ObjectPrototypeId`) REFERENCES `ObjectPrototype` (`ObjectPrototypeId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Player`
--
ALTER TABLE `Player`
  ADD CONSTRAINT `Player_ibfk_1` FOREIGN KEY (`AccountId`) REFERENCES `Account` (`AccountId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Player_ibfk_2` FOREIGN KEY (`PlayerId`) REFERENCES `Character` (`CharacterId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `SpellBuff`
--
ALTER TABLE `SpellBuff`
  ADD CONSTRAINT `SpellBuff_ibfk_1` FOREIGN KEY (`SpellId`) REFERENCES `Spell` (`SpellId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `SpellType`
--
ALTER TABLE `SpellType`
  ADD CONSTRAINT `SpellType_ibfk_1` FOREIGN KEY (`SpellId`) REFERENCES `Spell` (`SpellId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `SpellTypePrototype`
--
ALTER TABLE `SpellTypePrototype`
  ADD CONSTRAINT `SpellTypePrototype_ibfk_1` FOREIGN KEY (`SpellTypeId`) REFERENCES `SpellType` (`Type`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
