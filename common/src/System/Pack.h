/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_PACK_H
#define SYSTEM_PACK_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Data structures
////////////////////////////////////////////////////////////
struct PackHeader
{
	char UniqueID[7];	///< Unique ID to know if this file is from this class
	char Version[6];	///< File version to check integrity
	uint32 FileCount;	///< Number of files that there are in the pack
};

struct PackFile
{
	int32 Index;		///< ID of file data
	int32 Size;			///< Size of file data
	int32 Offset;		///< Where the file is
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get the file path depending on the file type with index
////////////////////////////////////////////////////////////
char * PackGetFilePath(char * FilesPath, int32 FileIndex);

////////////////////////////////////////////////////////////
/// Create a new pack
////////////////////////////////////////////////////////////
bool PackSaveData(char * PackPath, char * FilesPath, int32 FilesSize);

////////////////////////////////////////////////////////////
/// Extract all data from pack
////////////////////////////////////////////////////////////
bool PackExtractData(char * PackPath, char * ExtractPath);

////////////////////////////////////////////////////////////
/// Load file data with index to array
////////////////////////////////////////////////////////////
bool PackLoadData(char * PackPath, uint32 FileIndex, uint8 **Data, int32 * Size);

#endif // SYSTEM_PACK_H
