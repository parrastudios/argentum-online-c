/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/TimerManager.h>
#include <System/Platform.h>
#include <System/Thread.h>
#include <System/Condition.h>
#include <System/Mutex.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define TIMERS_MAX_SIZE		0x20
#define TIMERS_INVALID_ID	0xFFFFFFFF

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct TimerNode
{
	float			Tick;			///< Time of the next tick
	float			Interval;		///< Timer interval

	Thread			ThreadData;		///< Thread callback
	Mutex			MutexData;		///< Mutex callback
	Condition		ConditionData;	///< Condition callback
	
	CallbackType	Callback;		///< Callback function

	bool			Period;			///< If true, timer restarts when period ends
	bool			Remove;			///< If true, timer will be auto-removed
};

// todo:
//	sort in descendend
//		when the task is launch, save offset
//		interval + (initial_time % interval)
//		sort the Id array

//	in the coordinator (when sorted)
//		if (elapsed_time < timer[i].interval)
//			exit for

// need to sort always that a Tick value is updated
//		timer_add, timer_set, timer_update

// note:
//	could all problems be solved with condition timed wait?

struct TimerManagerData
{
	uint32				Count;						///< Current timers working
	struct TimerNode	Timers[TIMERS_MAX_SIZE];	///< Vector of timers
	uint32				Id[TIMERS_MAX_SIZE];		///< Map ids to timers vector (todo: sort it in descendent interval order)
	Mutex				MutexData;					///< Mutex for timers
	Thread				ThreadData;					///< Thread for updating timers
	float				Delay;						///< Delay the updating
	bool				Run;						///< Run thread worker	
} TimerManager;

////////////////////////////////////////////////////////////
/// Initialize a timer
////////////////////////////////////////////////////////////
void TimerNodeInitialize(struct TimerNode * Timer)
{
	// Iitialize all attributes
	Timer->Tick = 0.0f;
	Timer->Interval = 0.0f;
	Timer->Callback = NULL;
	Timer->Period = false;
	Timer->Remove = false;

	// Initialize mutex
	MutexInitialize(&Timer->MutexData);

	// Initialize condition
	ConditionInitialize(&Timer->ConditionData);
}

////////////////////////////////////////////////////////////
/// Set the timer interval
////////////////////////////////////////////////////////////
float TimerNodeSetInterval(struct TimerNode * Timer, float Interval)
{
	float Tick = 0.0f;

	// Signal all threads
	ConditionBroadcast(&Timer->ConditionData);

	// Lock mutex
	MutexLock(&Timer->MutexData);

	// If timer is active
	if (Timer->Interval != 0.0f)
	{
		// Set the interval
		Timer->Interval = Interval;

		// Set the next tick
		Tick = Timer->Tick = SystemGetTime() + Timer->Interval;
	}

	// Unlock mutex
	MutexUnlock(&Timer->MutexData);

	// Return time of the new tick
	return Tick;
}

////////////////////////////////////////////////////////////
/// Shutdown a timer (wait until thread finishes)
////////////////////////////////////////////////////////////
void TimerNodeShutdown(struct TimerNode * Timer)
{
	// Signal all threads
	ConditionBroadcast(&Timer->ConditionData);

	// Lock mutex
	MutexLock(&Timer->MutexData);

	// Reset all attributes
	Timer->Tick = 0.0f;
	Timer->Interval = 0.0f;
	Timer->Period = false;
	
	Timer->Callback = NULL;
	Timer->Remove = false;

	// Unlock mutex
	MutexUnlock(&Timer->MutexData);

	// Wait for thread execusion
	ThreadWait(&Timer->ThreadData);

	// Terminate thread
	ThreadTerminate(&Timer->ThreadData);
}

////////////////////////////////////////////////////////////
/// Destroy a timer (wait until thread finishes)
////////////////////////////////////////////////////////////
void TimerNodeDestroy(struct TimerNode * Timer)
{
	// Ensure each timer thread is finished
	ThreadTerminate(&Timer->ThreadData);

	// Destroy condition
	ConditionDestroy(&Timer->ConditionData);

	// Destroy mutex
	MutexDestroy(&Timer->MutexData);
}

////////////////////////////////////////////////////////////
/// Generic routine of a timer
////////////////////////////////////////////////////////////
void TimerNodeRoutine(void * UserData)
{
	struct TimerNode *	Timer			= (struct TimerNode*)UserData;
	bool				RunTimerRoutine	= true;

	while (RunTimerRoutine)
	{
		// Lock mutex
		MutexLock(&Timer->MutexData);

		// If timer is active
		if (Timer->Interval > 0.0f)
		{
			// Wait until the next tick
			ConditionWait(&Timer->ConditionData, &Timer->MutexData);

			// Call to the function
			Timer->Callback();

			// If not period, exit
			RunTimerRoutine = Timer->Period;
		}
		else
		{
			// If not active, exit
			RunTimerRoutine = false;
		}
		
		// Set remove attribute
		Timer->Remove = !RunTimerRoutine;

		// Unlock mutex
		MutexUnlock(&Timer->MutexData);
	}
}

////////////////////////////////////////////////////////////
/// Launches the thread of timer
////////////////////////////////////////////////////////////
void TimerNodeLaunch(struct TimerNode * Timer, float Interval, CallbackType Callback, bool Period)
{
	// Initialize attributes
	Timer->Interval	= Interval;
	Timer->Callback	= Callback;
	Timer->Period	= Period;
	Timer->Remove	= false;

	// Set the next tick
	Timer->Tick = SystemGetTime() + Timer->Interval;

	// Launch timer routine
	ThreadLaunch(&Timer->ThreadData, (FuncType)&TimerNodeRoutine, (void*)Timer);
}

////////////////////////////////////////////////////////////
/// Coordinator thread of all timers
////////////////////////////////////////////////////////////
void TimerManagerRoutine(void * UserData)
{
	struct TimerManagerData *	Manager				= (struct TimerManagerData*)UserData;
	bool						RunManagerRoutine	= false;
	uint32	i;

	do
	{
		float ElapsedTime = SystemGetTime();

		// Lock manager mutex
		MutexLock(&Manager->MutexData);

		// Update timers
		for (i = 0; i < Manager->Count; i++)
		{
			if (Manager->Timers[Manager->Id[i]].Remove == false)
			{
				if (ElapsedTime > Manager->Timers[Manager->Id[i]].Tick)
				{
					// Signal tick to the timer  routine
					ConditionSignal(&Manager->Timers[Manager->Id[i]].ConditionData);

					// Update next tick
					Manager->Timers[Manager->Id[i]].Tick = ElapsedTime + Manager->Timers[Manager->Id[i]].Interval;

					// todo: sort properly Id array by tick value
				}
			}
			else
			{
				Manager->Timers[Manager->Id[i]].Remove = false;

				// Remove
				if (Manager->Timers[Manager->Id[i]].Interval != 0.0f)
				{
					// Shutdown timer routine
					TimerNodeShutdown(&Manager->Timers[Manager->Id[i]]);
				}

				if (i != (--Manager->Count))
				{
					Manager->Id[i] = Manager->Id[Manager->Count];
				}

				Manager->Id[Manager->Count] = TIMERS_INVALID_ID;
			}
		}

		RunManagerRoutine = Manager->Run;

		if (Manager->Delay)
		{
			float Delay = Manager->Delay;

			// Unlock manager mutex
			MutexUnlock(&Manager->MutexData);

			// Sleep delay
			SystemSleep(Delay);
		}
		else
		{
			// Unlock manager mutex
			MutexUnlock(&Manager->MutexData);
		}

	} while (RunManagerRoutine);

}

////////////////////////////////////////////////////////////
/// Initialize all timers
////////////////////////////////////////////////////////////
bool TimerManagerInitialize(float Delay)
{
	uint32 i;

	// Initialize all timers
	for (i = 0; i < TIMERS_MAX_SIZE; i++)
	{
		TimerNodeInitialize(&TimerManager.Timers[i]);

		TimerManager.Id[i] = TIMERS_INVALID_ID;
	}

	// Reset attributes
	TimerManager.Count = 0;
	TimerManager.Run = true;
	TimerManager.Delay = Delay;

	// Initialize mutex
	MutexInitialize(&TimerManager.MutexData);

	// Launch routine
	ThreadLaunch(&TimerManager.ThreadData, (FuncType)&TimerManagerRoutine, (void*)&TimerManager);

	return true;
}

////////////////////////////////////////////////////////////
/// Destroy all timers
////////////////////////////////////////////////////////////
void TimerManagerDestroy()
{
	uint32 i;

	// Mutex lock
	MutexLock(&TimerManager.MutexData);

	// Shutdown manager routine
	TimerManager.Run = false;

	// Unlock mutex
	MutexUnlock(&TimerManager.MutexData);

	// Wait until thread is finished
	ThreadWait(&TimerManager.ThreadData);

	// Destroy mutex
	MutexDestroy(&TimerManager.MutexData);

	// Clear timer array
	for (i = 0; i < TIMERS_MAX_SIZE; i++)
	{
		// Destroy timer
		TimerNodeDestroy(&TimerManager.Timers[i]);

		// Clear timer id
		TimerManager.Id[i] = TIMERS_INVALID_ID;
	}

	// Reset manager attributes
	TimerManager.Delay = 0.0f;
	TimerManager.Count = 0;
}

////////////////////////////////////////////////////////////
// Gets a free bucket in the array
////////////////////////////////////////////////////////////
uint32 TimerManagerGetShutdownId()
{
	uint32 i;

	for (i = 0; i < TIMERS_MAX_SIZE; i++)
	{
		if (TimerManager.Timers[i].Interval == 0.0f)
		{
			return i;
		}
	}

	return TIMERS_INVALID_ID;
}

////////////////////////////////////////////////////////////
// Add a timer to the manager
////////////////////////////////////////////////////////////
uint32 TimerManagerAdd(float Interval, CallbackType Callback, bool Period)
{
	MutexLock(&TimerManager.MutexData);

	if (TimerManager.Count < TIMERS_MAX_SIZE)
	{
		uint32 ShutdownId = TimerManagerGetShutdownId();

		if (ShutdownId != TIMERS_INVALID_ID)
		{
			// todo: sort properly Id array by tick value (pass tick value by args on Launch?)

			TimerManager.Id[TimerManager.Count++] = ShutdownId;

			MutexUnlock(&TimerManager.MutexData);

			TimerNodeLaunch(&TimerManager.Timers[ShutdownId], Interval, Callback, Period);

			return ShutdownId;
		}
	}

	MutexUnlock(&TimerManager.MutexData);
	
	return TIMERS_INVALID_ID;
}

////////////////////////////////////////////////////////////
// Modify the interval of a timer in the manager
////////////////////////////////////////////////////////////
void TimerManagerSet(uint32 Id, float Interval)
{
	if (Id != TIMERS_INVALID_ID)
	{
		// Set the interval
		float Tick = TimerNodeSetInterval(&TimerManager.Timers[Id], Interval);

		// Lock manager mutex
		MutexLock(&TimerManager.MutexData);

		// Update id array by next tick value
		// (todo)

		// Unlock manager mutex
		MutexUnlock(&TimerManager.MutexData);
	}
}

////////////////////////////////////////////////////////////
// Remove a timer to the manager
////////////////////////////////////////////////////////////
void TimerManagerRemove(uint32 Id)
{
	if (Id != TIMERS_INVALID_ID)
	{
		uint32 i;

		MutexLock(&TimerManager.MutexData);

		for (i = 0; i < TimerManager.Count; i++)
		{
			if (TimerManager.Id[i] == Id)
			{
				if (i != (--TimerManager.Count))
				{
					// todo: sort properly Id array by tick value

					TimerManager.Id[i] = TimerManager.Id[TimerManager.Count];
				}

				TimerManager.Id[TimerManager.Count] = TIMERS_INVALID_ID;

				MutexUnlock(&TimerManager.MutexData);

				if (TimerManager.Timers[Id].Interval != 0.0f)
				{
					TimerNodeShutdown(&TimerManager.Timers[Id]);
				}

				return;
			}
		}

		MutexUnlock(&TimerManager.MutexData);
	}
}
