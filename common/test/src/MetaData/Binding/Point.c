/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Point.h>

#include <System/IOHelper.h> // todo: refactor

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
PointType PointObject;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Print point information
////////////////////////////////////////////////////////////
void PointPrint(PointType * Point)
{
	printf("Point instance <%p>: X (%f), Y (%f)\n", (void *)Point, Point->X, Point->Y);
}

////////////////////////////////////////////////////////////
/// Move a point
////////////////////////////////////////////////////////////
void PointMove(PointType * Point, float X, float Y)
{
	Point->X = X;
	Point->Y = Y;
}

////////////////////////////////////////////////////////////
/// Destroy a point instance
////////////////////////////////////////////////////////////
void PointDestroy(PointType * Point)
{
	if (Point)
	{
		// Destroy point attributes if neccessary
	}
}

////////////////////////////////////////////////////////////
/// Create a point instance
////////////////////////////////////////////////////////////
PointType * PointCreate(PointType * Point, float X, float Y)
{
	// Construct the point, note that metadata system is not visible at this level
	if (Point)
	{
		// Set initial values
		Point->X = X;
		Point->Y = Y;

		// Note that point is given by arguments, so or it's binded or
		// it's allocated by metadata system and we receive a pointer to region where it belongs to.
		// Memory is automatically managed and for the point, metadata system doesn't exists.
		Point->Print = &PointPrint;
		Point->Move = &PointMove;
		Point->Destroy = &PointDestroy;
	}

	return Point;
}
