/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_MODEL_RENDER_H
#define WORLD_MODEL_RENDER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Component.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define MODEL_MATERIAL_TEXTURE		0x00
#define MODEL_MATERIAL_DIFFUSE		0x01
#define MODEL_MATERIAL_SPECULAR		0x02
#define MODEL_MATERIAL_NORMAL		0x03
#define MODEL_MATERIAL_HEIGHT		0x04
#define MODEL_MATERIAL_BUMP			0x05

#define MODEL_MATERIAL_SIZE			0x06

#define MODEL_RENDER_STATIC_MESH	0x00
#define MODEL_RENDER_SKINNED_MESH	0x01

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef void * ModelRenderHandleType;
typedef uinteger ModelRenderHandleId;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct ModelComponentRenderData
{
	ModelRenderHandleId Type;
	ModelRenderHandleType Handle;
};

struct ModelComponentRenderType
{
	void * TODO;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize model render components
////////////////////////////////////////////////////////////
void ModelFactoryInitializeRenderCallback();

////////////////////////////////////////////////////////////
/// Destroy render component pool of model factory
////////////////////////////////////////////////////////////
void ModelFactoryDestroyRenderCallback();

////////////////////////////////////////////////////////////
/// Return the model render implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentRenderType * ModelComponentRenderAttr();

////////////////////////////////////////////////////////////
/// Return the model render implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * ModelComponentRenderImpl();

////////////////////////////////////////////////////////////
/// Return the model render derived data size
////////////////////////////////////////////////////////////
uinteger ModelComponentRenderSize();

#endif // WORLD_MODEL_RENDER_H
