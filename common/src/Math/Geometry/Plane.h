/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	MATH_GEOMETRY_PLANE_H
#define MATH_GEOMETRY_PLANE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Vector.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct PlaneType
{
	union
	{
		struct
		{
			float a, b, c;
		};
		struct Vector3f v;
	};

	float d;

	uint8 SignBits;
};

////////////////////////////////////////////////////////////////////////////////
// @doc SignBits :
//
//  The first three signbits indicate whether the plane is axially aligned and
//  are used to speed up the dot product calculation used to test if a point is
//  inside the plane.
//
//  We use Hearn & Baker's definition of inside and outside.
//  In a right handed system, the normal abc is directed from inside (behind)
//  the plane to outside (in front of) the plane. The outside of the plane
//  is the visible side.
//  Polygons in the plane with counter-clockwise winding when viewed from
//  outside the plane are only visible from outside the plane (front facing).
//  Polygons in the plane with counter-clockwise winding when viewed from
//  inside the plane are only visible from inside the plane (back facing).
//                                 The plane equation is  ax + by + cz + d = 0.
//       Points inside (behind) the plane are defined by  ax + by + cz + d < 0
//  Points ouside (in front of) the plane are defined by  ax + by + cz + d > 0
//
//  The next three signbits encode the signs of the abc components of the plane
//  normal, and are used to build accept/reject points to test if a bounding box
//  is inside the plane, e.g.:-
//
//       outside           inside
//                   |
//    Normal   <-----|
//                   |
//
//               |       |
//               |       |
//            min x     max x
//
//  If       max x is outside the plane, the bounding box is rejected
//  Else If  min x is inside  the plane, the bounding box is accepted
//  Else     the bounding box is clipped by the plane
//
//  i.e. If N.x < 0, max x is the reject point, min x is the accept point.
//
//        inside           outside
//                   |
//                   |----->   Normal
//                   |
//
//               |       |
//               |       |
//            min x     max x
//
//  If       min x is outside the plane, the bounding box is rejected
//  Else If  max x is inside  the plane, the bounding box is accepted
//  Else     the bounding box is clipped by the plane
//
//  i.e. If N.x > 0, min x is the reject point, max x is the accept point.
//
//  The same argument applies to the y and z components.
//
////////////////////////////////////////////////////////////////////////////////

void PlaneCopy(struct PlaneType * Dest, struct PlaneType * Src);

void PlaneSetSignBits(struct PlaneType * Plane);

bool PlaneCheckPoint(struct PlaneType * Plane, struct Vector3f * Point);

void PlaneTranslate(struct PlaneType * Plane, struct Vector3f * Vector);

float PlaneNormalizeEx(struct PlaneType * Dest, struct PlaneType * Src);

float PlaneNormalize(struct PlaneType * Plane);

float PlaneGetNormal(struct PlaneType * Plane);

float PlaneDotCoord(struct PlaneType * Plane, struct Vector3f * Vector);

float PlaneDistance(struct PlaneType * Plane, struct Vector3f * Position);

#endif // MATH_GEOMETRY_PLANE_H
