var connection = require('./Connection');
var options = require('../Options');
var CodeBuilder = require('./CodeBuilderMySQL');

module.exports = {

  getTables: function (callback) {

    connection.query('select cols.TABLE_NAME, cols.COLUMN_NAME, cols.COLUMN_TYPE FROM information_schema.columns AS cols where table_schema = "' + options.database + '" order by table_name,ordinal_position', function (err, rows, fields) {
      if (err) throw err;

  var columnGroups = [];

  for (var index = 0; index < rows.length; index++) {
    var element = rows[index];

    var tableName = element.TABLE_NAME;
    var columnName = element.COLUMN_NAME;

    var columnUnsigned = false;

    var columnType = element.COLUMN_TYPE.split("(");

    if (typeof columnType[1] == 'string') {
      // Get the type number
      var columnTypeNumber = columnType[1].split(")");
        
      // Get column's unsigned value
      if (columnTypeNumber[1] === ' unsigned') {
        columnUnsigned = true;
      }
    } else {
      // If there is no type value set it to 0
      columnTypeNumber[0] = 0;
    }
          columnGroups.push({tableName: tableName, columnAttributes: {
          columnName: columnName, 
          columnType: columnType[0], 
          columnTypeNumber: columnTypeNumber[0], 
          columnUnsigned: columnUnsigned }} );
       }
       
    var columns = [];
  // Put the array in order
  columnGroups.forEach(function (o) {
    var column = o.tableName;
    columns[column] = columns[column] || [];
    columns[column].push(o.columnAttributes);
  });    
      callback(columns);

  });
}
}
