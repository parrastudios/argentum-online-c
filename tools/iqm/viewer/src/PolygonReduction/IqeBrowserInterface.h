/* IqeBrowserInterface.h

01.10.2014 RR: * First edition of this file
                 Base was the file xxx\DemoUI\processor.h of
                 the Pinocchio download from GitHup.
                 Reworked file so Pinocchio is usable from IqeBrowser.


*/


#ifndef _IEQ_BROWSER_POLY_REDUCTION_H
#define _IEQ_BROWSER_POLY_REDUCTION_H

int IqeB_ModelPolygonReduction( IQE_model *pModel,
                                int ReduceVertexPercentage,
                                int ReduceMaxVertexAmount,
                                int ReduceMinVertexAmount
                              );

#endif //_IEQ_BROWSER_POLY_REDUCTION_H
