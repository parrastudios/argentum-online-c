/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/Library.h>
#include <Plugin/PluginLoaderImplLibrary.h>

#include <System/IOHelper.h> // todo: refactor into IO module

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
struct PluginLoaderImplType PluginLoaderImplLibrary = {
	TypeInit(Initialize, &PluginLoaderImplLibraryInitialize),
	TypeInit(Destroy, &PluginLoaderImplLibraryDestroy),
	TypeInit(Load, &PluginLoaderImplLibraryLoad),
	TypeInit(LoadPath, &PluginLoaderImplLibraryLoadPath),
	TypeInit(Register, &PluginLoaderImplLibraryRegister),
	TypeInit(Unload, &PluginLoaderImplLibraryUnload),
	TypeInit(Clear, &PluginLoaderImplLibraryClear),
	TypeInit(Initialized, false),
	TypeInit(ResourceList, NULL)
};

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////

// ...

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get default plugin loader library implementation
////////////////////////////////////////////////////////////
struct PluginLoaderImplType * PluginLoaderImplLibraryGet()
{
	return &PluginLoaderImplLibrary;
}

////////////////////////////////////////////////////////////
/// Initialize library plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderImplLibraryInitialize(struct PluginLoaderImplType * PluginLoaderImpl)
{

	// ..

	return true;
}

////////////////////////////////////////////////////////////
/// Destroy library plugin loader
////////////////////////////////////////////////////////////
void PluginLoaderImplLibraryDestroy(struct PluginLoaderImplType * PluginLoaderImpl)
{
	// ..

}

////////////////////////////////////////////////////////////
/// Load a plugin from library plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderImplLibraryLoad(struct PluginLoaderImplType * PluginLoaderImpl, PluginId Id, PluginLoaderImplResourceType * Impl)
{
	PluginId LibraryId;

	// Generate library with extension name
	sprintf(LibraryId, "%s%s", Id, LibraryExtension());

	// Load the library
	*Impl = (PluginLoaderImplResourceType)LibraryLoad(LibraryId, LIBRARY_FLAGS_BIND_LAZY | LIBRARY_FLAGS_BIND_GLOBAL);

	return (*Impl != NULL);
}

////////////////////////////////////////////////////////////
/// Load a whole path of plugins from library plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderImplLibraryLoadPath(struct PluginLoaderImplType * PluginLoaderImpl, const char * Path, List LoadedIdList)
{

	return false;
}

////////////////////////////////////////////////////////////
/// Register library from plugin loader to plugin handle
////////////////////////////////////////////////////////////
bool PluginLoaderImplLibraryRegister(struct PluginLoaderImplType * PluginLoaderImpl, PluginLoaderImplResourceType Impl, struct PluginType * Plugin)
{
	LibrarySymbolType Address;

	if (LibrarySymbol((struct LibraryType *)Impl, (LibrarySymbolNameType)Plugin->Connector.Name(), &Address))
	{
		// Bind connector
		if (Plugin->Connector.Bind(Plugin, (ConnectorAddress)Address))
		{
			return Plugin->Connector.Invoke(Plugin);
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Unload a plugin from library plugin loader
////////////////////////////////////////////////////////////
bool PluginLoaderImplLibraryUnload(struct PluginLoaderImplType * PluginLoaderImpl, PluginLoaderImplResourceType Impl)
{
	// Unload the library
	LibraryUnload((struct LibraryType *)Impl);

	return true;
}

////////////////////////////////////////////////////////////
/// Clear all plugins from library plugin loader
////////////////////////////////////////////////////////////
void PluginLoaderImplLibraryClear(struct PluginLoaderImplType * PluginLoaderImpl)
{

}
