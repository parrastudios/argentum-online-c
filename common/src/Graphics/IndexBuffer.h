/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_INDEX_BUFFER_H
#define GRAPHICS_INDEX_BUFFER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct IndexBuffer
{
	void *		Data;
	union
	{
		uint16 * Data16;
		uint32 * Data32;
	};
	uint32		Size;
	uint32		Count;
	bool		Dynamic;
	uint32		IndexSize;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////
struct IndexBuffer * IndexBufferCreate(uint32 Size, bool Dynamic, uint32 IndexSize);

void IndexBufferDestroy(struct IndexBuffer * Buffer);

void IndexBufferReset(struct IndexBuffer * Buffer);

void IndexBufferAdd(struct IndexBuffer * Buffer, uint32 Index);

void IndexBufferAddTriangle(struct IndexBuffer * Buffer, uint32 VertexBase);

void IndexBufferAddQuad(struct IndexBuffer * Buffer, uint32 VertexBase);

void IndexBufferAddGrid(struct IndexBuffer * Buffer, uint32 VertexBase, uint32 Length, uint32 Rows, uint32 Cols, bool Outward);

uint32 IndexBufferLength(struct IndexBuffer * Buffer);

uint32 IndexBufferSizeI(struct IndexBuffer * Buffer);

bool IndexBufferIsAvailable(struct IndexBuffer * Buffer, uint32 Count);

#endif // GRAPHICS_INDEX_BUFFER_H
