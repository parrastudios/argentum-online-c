/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATATYPE_HASH_H
#define DATATYPE_HASH_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define HASH_INVALID				UINTEGER_MAX_RANGE

////////////////////////////////////////////////////////////
// Enumerations
////////////////////////////////////////////////////////////
enum HashCallbackIdType
{
	HASH_CALLBACK_UINT32 = 0x00,
	HASH_CALLBACK_UINT64 = 0x01,
	HASH_CALLBACK_STR_SDBM = 0x02,
	HASH_CALLBACK_STR_DJB2 = 0x03,
	HASH_CALLBACK_STR_ADLER32 = 0x04,

	HASH_CALLBACK_SIZE
};

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef enum HashCallbackIdType HashCallbackId;
typedef uinteger HashData;
typedef void * HashKeyData;
typedef HashData (*HashCallback)(HashKeyData);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get default hash integer function
////////////////////////////////////////////////////////////
HashCallback HashGetCallbackUnsignedIntegerDefault();

////////////////////////////////////////////////////////////
/// Get default hash string function
////////////////////////////////////////////////////////////
HashCallback HashGetCallbackStringDefault();

////////////////////////////////////////////////////////////
/// Get desired hash function
////////////////////////////////////////////////////////////
HashCallback HashGetCallback(HashCallbackId Id);

#endif // DATATYPE_HASH_H
