/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/TextureManager.h>
#include <Graphics/Texture.h>
#include <Graphics/TextureLoader.h>
#include <Graphics/Index.h>
#include <System/Pack.h>
#include <System/Platform.h>
#include <System/IOHelper.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define TEXTURE_ERROR	-1	///< Texture error

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct TextureCache TextureControl[TEXTURE_CACHE_SIZE] = {{0}};		///< Texture cache array
struct Texture 		TextureList[TEXTURE_LOAD_SIZE] = {{0}};			///< Texture data array

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////
int32 TextureSearchFree();	///< Search the deallocate slot
int32 TextureLastUsed();	///< Search the last used texture

////////////////////////////////////////////////////////////
/// Returns the texture path
////////////////////////////////////////////////////////////
char * TexturePath(int32 Index)
{
	char * Path = (char*)(MemoryAllocate(sizeof(char) * 255));

	sprintf(Path, "data/graphics/%d.png", Index);

	return Path;
}

////////////////////////////////////////////////////////////
/// Returns the texture loaded into memory
////////////////////////////////////////////////////////////
bool TextureLoad(int32 Index, uint8 * Data, bool StaticTexture, struct Rect * SubTexture)
{
	// Index in the array
	int32 TextureIndex;

	// If it is loaded
	if (TextureControl[Index].ID > 0)
	{
		TextureControl[Index].LastUsed = SystemGetTime();
		return true;
	}

	// Search deallocate slot
	TextureIndex = TextureSearchFree();

	if (TextureIndex == TEXTURE_ERROR)
	{
		TextureIndex = TextureLastUsed();

		if (TextureIndex == TEXTURE_ERROR)
			return false;

		// Delete texture
		TextureDelete(&TextureList[TextureIndex]);
	}

	// So.. load it
	if (Data == NULL)
	{
	    // Obtain the path
	    char * Path = TexturePath(IndexGetGrh(Index)->FileNumber);

		if (SubTexture)
		{
			// Load from file by sub rect
			TextureLoadFromFileEx(Path, &TextureList[TextureIndex], SubTexture);
		}
		else
		{
			// Load from file
			TextureLoadFromFile(Path, &TextureList[TextureIndex]);
		}

		// Delete it
		MemoryDeallocate(Path);
	}
	else
	{
		char * FilePack = "data/pack/graphics.pak";
		int32 BufferSize;

		// Load buffer from pack
		if (!PackLoadData(FilePack, IndexGetGrh(Index)->FileNumber, &Data, &BufferSize))
			return false;

		// Load from memory
		TextureLoadFromMemory(Data, BufferSize, &TextureList[TextureIndex]);
	}

	if (!TextureList[TextureIndex].Pixels)
		return false;

	// Load texture into OGL memory
	if (TextureCreate(&TextureList[TextureIndex], false, false))
	{
		// Set data
		TextureControl[Index].ID = TextureIndex;
		TextureControl[Index].LastUsed = SystemGetTime();
		TextureControl[Index].Remove = StaticTexture;
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Returns the texture data
////////////////////////////////////////////////////////////
struct Texture * TextureGetData(int32 Index)
{
	return &TextureList[TextureControl[Index].ID];
}

////////////////////////////////////////////////////////////
/// Render a texture
////////////////////////////////////////////////////////////
void TextureRender(int32 Id, int32 X, int32 Y, struct Rect * SubRect, struct Color4ub * Color)
{
	// Load it, if it isn't loaded
	if (TextureLoad(Id, NULL, false, SubRect))
	{
		// Render it
		TextureRenderEx(TextureGetData(Id), X, Y, SubRect, Color);
	}
}

////////////////////////////////////////////////////////////
/// Delete all textures
////////////////////////////////////////////////////////////
void TextureManagerDestroy()
{
	uint32 i;

	for (i = 1; i < TEXTURE_LOAD_SIZE; i++)
	{
		// If the texture is loaded
		if (TextureList[i].Id != 0)
		{
			TextureDelete(&TextureList[i]);
		}
	}
}

////////////////////////////////////////////////////////////
/// Search deallocate slot
////////////////////////////////////////////////////////////
int32 TextureSearchFree()
{
	uint32 i;

	for (i = 1; i < TEXTURE_LOAD_SIZE; i++)
	{
		if (TextureList[i].Id == 0)
			return i;
	}

	return TEXTURE_ERROR;
}

////////////////////////////////////////////////////////////
/// Search the last used texture
////////////////////////////////////////////////////////////
int32 TextureLastUsed()
{
	float  MinorTime = 1e+37f;
	uint32 Dimension = 0;
	uint32 i;

	for (i = 1; i < TEXTURE_CACHE_SIZE; i++)
	{
		if (TextureControl[i].Remove == false)
		{
			if (TextureControl[i].LastUsed < MinorTime)
			{
				MinorTime = TextureControl[i].LastUsed;
				Dimension = i;
			}
		}
	}

	if (MinorTime != 0)
		return TextureControl[Dimension].ID;
	else
		return TEXTURE_ERROR;
}
