/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

#ifndef GAME_WEATHER_H
#define GAME_WEATHER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define WEATHER_TYPE_INDOORS	0x01 << 0x00 ///< Disables weather
#define WEATHER_TYPE_SUNNY		0x01 << 0x01 ///< Sunny
#define WEATHER_TYPE_CLOUDY		0x01 << 0x02 ///< Cloudy
#define WEATHER_TYPE_RAINY		0x01 << 0x03 ///< Rainy
#define WEATHER_TYPE_STORMY		0x01 << 0x04 ///< Stormy
#define WEATHER_TYPE_WINDY		0x01 << 0x05 ///< Windy
#define WEATHER_TYPE_SNOWY		0x01 << 0x06 ///< Snowy
#define WEATHER_TYPE_FOGGY		0x01 << 0x07 ///< Foggy
#define WEATHER_TYPE_CUSTOM		0x01 << 0x08 ///< Custom weather

// todo

#endif // GAME_WEATHER_H