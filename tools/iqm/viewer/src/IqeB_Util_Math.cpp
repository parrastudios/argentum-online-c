/*
***********************************************************************************
 IqeB_Util_Math.cpp

 Utilities mathematics

14.12.2014 RR: * First editon of this file.

*/

#include "IqeBrowser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

/*
============================================================================

					BYTE ORDER FUNCTIONS

============================================================================
*/

static int	bigendien;

// can't just use function pointers, or dll linkage can
// mess up when qcommon is included in multiple places
static short	(*_BigShort) (short l);
static short	(*_LittleShort) (short l);
static int		(*_BigLong) (int l);
static int		(*_LittleLong) (int l);
static float	(*_BigFloat) (float l);
static float	(*_LittleFloat) (float l);

//x/short	BigShort(short l){return _BigShort(l);}
short	LittleShort(short l) {return _LittleShort(l);}
//x/int		BigLong (int l) {return _BigLong(l);}
int		LittleLong (int l) {return _LittleLong(l);}
//x/float	BigFloat (float l) {return _BigFloat(l);}
float	LittleFloat (float l) {return _LittleFloat(l);}

static short   ShortSwap (short l)
{
	unsigned char    b1,b2;

	b1 = l&255;
	b2 = (l>>8)&255;

	return (b1<<8) + b2;
}

static short	ShortNoSwap (short l)
{
	return l;
}

static int    LongSwap (int l)
{
	unsigned char    b1,b2,b3,b4;

	b1 = l&255;
	b2 = (l>>8)&255;
	b3 = (l>>16)&255;
	b4 = (l>>24)&255;

	return ((int)b1<<24) + ((int)b2<<16) + ((int)b3<<8) + b4;
}

static int	LongNoSwap (int l)
{
	return l;
}

static float FloatSwap (float f)
{
	union
	{
		float	f;
		unsigned char	b[4];
	} dat1, dat2;


	dat1.f = f;
	dat2.b[0] = dat1.b[3];
	dat2.b[1] = dat1.b[2];
	dat2.b[2] = dat1.b[1];
	dat2.b[3] = dat1.b[0];
	return dat2.f;
}

static float FloatNoSwap (float f)
{
	return f;
}

/*
================
Swap_Init
================
*/
void Swap_Init( void)
{
	unsigned char	swaptest[2] = {1,0};
	void *pSwapTest = &swaptest;

// set the byte swapping variables in a portable manner
	if ( *(short *)pSwapTest == 1)
	{
		bigendien = false;
		_BigShort = ShortSwap;
		_LittleShort = ShortNoSwap;
		_BigLong = LongSwap;
		_LittleLong = LongNoSwap;
		_BigFloat = FloatSwap;
		_LittleFloat = FloatNoSwap;
	}
	else
	{
		bigendien = true;
		_BigShort = ShortNoSwap;
		_LittleShort = ShortSwap;
		_BigLong = LongNoSwap;
		_LittleLong = LongSwap;
		_BigFloat = FloatNoSwap;
		_LittleFloat = FloatSwap;
	}
}

/************************************************************************************
 * Some vector and matrix math.
 */

#define A(row,col) a[(col<<2)+row]
#define B(row,col) b[(col<<2)+row]
#define M(row,col) m[(col<<2)+row]

void mat_identity( mat4 m)
{
	m[1] = m[2] = m[3] = m[4] = m[6] = m[7] = m[8] = m[9] =	m[11] = m[12] = m[13] = m[14] = 0;
	m[0] = m[5] = m[10] = m[15] = 1.0f;
}

void mat_copy(mat4 p, const mat4 m)
{
	memcpy(p, m, sizeof(mat4));
}

void mat_mul(mat4 m, const mat4 a, const mat4 b)
{
	int i;
	for (i = 0; i < 4; i++) {
		const float ai0=A(i,0), ai1=A(i,1), ai2=A(i,2), ai3=A(i,3);
		M(i,0) = ai0 * B(0,0) + ai1 * B(1,0) + ai2 * B(2,0) + ai3 * B(3,0);
		M(i,1) = ai0 * B(0,1) + ai1 * B(1,1) + ai2 * B(2,1) + ai3 * B(3,1);
		M(i,2) = ai0 * B(0,2) + ai1 * B(1,2) + ai2 * B(2,2) + ai3 * B(3,2);
		M(i,3) = ai0 * B(0,3) + ai1 * B(1,3) + ai2 * B(2,3) + ai3 * B(3,3);
	}
}

void mat_invert(mat4 out, const mat4 m)
{
	mat4 inv;
	float det;
	int i;

	inv[0] = m[5]*m[10]*m[15] - m[5]*m[11]*m[14] - m[9]*m[6]*m[15] +
		m[9]*m[7]*m[14] + m[13]*m[6]*m[11] - m[13]*m[7]*m[10];
	inv[4] = -m[4]*m[10]*m[15] + m[4]*m[11]*m[14] + m[8]*m[6]*m[15] -
		m[8]*m[7]*m[14] - m[12]*m[6]*m[11] + m[12]*m[7]*m[10];
	inv[8] = m[4]*m[9]*m[15] - m[4]*m[11]*m[13] - m[8]*m[5]*m[15] +
		m[8]*m[7]*m[13] + m[12]*m[5]*m[11] - m[12]*m[7]*m[9];
	inv[12] = -m[4]*m[9]*m[14] + m[4]*m[10]*m[13] + m[8]*m[5]*m[14] -
		m[8]*m[6]*m[13] - m[12]*m[5]*m[10] + m[12]*m[6]*m[9];
	inv[1] = -m[1]*m[10]*m[15] + m[1]*m[11]*m[14] + m[9]*m[2]*m[15] -
		m[9]*m[3]*m[14] - m[13]*m[2]*m[11] + m[13]*m[3]*m[10];
	inv[5] = m[0]*m[10]*m[15] - m[0]*m[11]*m[14] - m[8]*m[2]*m[15] +
		m[8]*m[3]*m[14] + m[12]*m[2]*m[11] - m[12]*m[3]*m[10];
	inv[9] = -m[0]*m[9]*m[15] + m[0]*m[11]*m[13] + m[8]*m[1]*m[15] -
		m[8]*m[3]*m[13] - m[12]*m[1]*m[11] + m[12]*m[3]*m[9];
	inv[13] = m[0]*m[9]*m[14] - m[0]*m[10]*m[13] - m[8]*m[1]*m[14] +
		m[8]*m[2]*m[13] + m[12]*m[1]*m[10] - m[12]*m[2]*m[9];
	inv[2] = m[1]*m[6]*m[15] - m[1]*m[7]*m[14] - m[5]*m[2]*m[15] +
		m[5]*m[3]*m[14] + m[13]*m[2]*m[7] - m[13]*m[3]*m[6];
	inv[6] = -m[0]*m[6]*m[15] + m[0]*m[7]*m[14] + m[4]*m[2]*m[15] -
		m[4]*m[3]*m[14] - m[12]*m[2]*m[7] + m[12]*m[3]*m[6];
	inv[10] = m[0]*m[5]*m[15] - m[0]*m[7]*m[13] - m[4]*m[1]*m[15] +
		m[4]*m[3]*m[13] + m[12]*m[1]*m[7] - m[12]*m[3]*m[5];
	inv[14] = -m[0]*m[5]*m[14] + m[0]*m[6]*m[13] + m[4]*m[1]*m[14] -
		m[4]*m[2]*m[13] - m[12]*m[1]*m[6] + m[12]*m[2]*m[5];
	inv[3] = -m[1]*m[6]*m[11] + m[1]*m[7]*m[10] + m[5]*m[2]*m[11] -
		m[5]*m[3]*m[10] - m[9]*m[2]*m[7] + m[9]*m[3]*m[6];
	inv[7] = m[0]*m[6]*m[11] - m[0]*m[7]*m[10] - m[4]*m[2]*m[11] +
		m[4]*m[3]*m[10] + m[8]*m[2]*m[7] - m[8]*m[3]*m[6];
	inv[11] = -m[0]*m[5]*m[11] + m[0]*m[7]*m[9] + m[4]*m[1]*m[11] -
		m[4]*m[3]*m[9] - m[8]*m[1]*m[7] + m[8]*m[3]*m[5];
	inv[15] = m[0]*m[5]*m[10] - m[0]*m[6]*m[9] - m[4]*m[1]*m[10] +
		m[4]*m[2]*m[9] + m[8]*m[1]*m[6] - m[8]*m[2]*m[5];

	det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];
	assert (det != 0);
	det = 1.0 / det;
	for (i = 0; i < 16; i++)
		out[i] = inv[i] * det;
}

// NOTE: x, y, z must be normalized
void mat_rotate( mat4 a, float angle, float x, float y, float z)
{
	mat4 m, b;
	float c = cos( angle);
	float s = sin( angle);
	float mc = 1 - c, t1, t2;

	mat_identity( m);

	M(0,0) = (x * x * mc) + c;
	M(1,1) = (y * y * mc) + c;
	M(2,2) = (z * z * mc) + c;

	t1 = y * x * mc;
	t2 = z * s;
	M(0,1) = t1 + t2;
	M(1,0) = t1 - t2;

	t1 = x * z * mc;
	t2 = y * s;
	M(0,2) = t1 - t2;
	M(2,0) = t1 + t2;

	t1 = y * z * mc;
	t2 = x * s;
	M(1,2) = t1 + t2;
	M(2,1) = t1 - t2;

	mat_copy( b, a);
	mat_mul( a, b, m);
}

void mat_from_pose(mat4 m, const vec3 t, const vec4 q, const vec3 s)
{
	float x2 = q[0] + q[0];
	float y2 = q[1] + q[1];
	float z2 = q[2] + q[2];
	{
		float xx2 = q[0] * x2;
		float yy2 = q[1] * y2;
		float zz2 = q[2] * z2;
		M(0,0) = 1 - yy2 - zz2;
		M(1,1) = 1 - xx2 - zz2;
		M(2,2) = 1 - xx2 - yy2;
	}
	{
		float yz2 = q[1] * z2;
		float wx2 = q[3] * x2;
		M(2,1) = yz2 + wx2;
		M(1,2) = yz2 - wx2;
	}
	{
		float xy2 = q[0] * y2;
		float wz2 = q[3] * z2;
		M(1,0) = xy2 + wz2;
		M(0,1) = xy2 - wz2;
	}
	{
		float xz2 = q[0] * z2;
		float wy2 = q[3] * y2;
		M(0,2) = xz2 + wy2;
		M(2,0) = xz2 - wy2;
	}

	m[0] *= s[0]; m[4] *= s[1]; m[8] *= s[2];
	m[1] *= s[0]; m[5] *= s[1]; m[9] *= s[2];
	m[2] *= s[0]; m[6] *= s[1]; m[10] *= s[2];

	M(0,3) = t[0];
	M(1,3) = t[1];
	M(2,3) = t[2];

	M(3,0) = 0;
	M(3,1) = 0;
	M(3,2) = 0;
	M(3,3) = 1;
}

#undef A
#undef B
#undef M

void vec_set( vec3 p, float a0, float a1, float a2)
{

	p[0] = a0;
	p[1] = a1;
	p[2] = a2;
}

// vector distance squaret
float vec_dist2(const vec3 a, const vec3 b)
{
	float d0, d1, d2;

	d0 = a[0] - b[0];
	d1 = a[1] - b[1];
	d2 = a[2] - b[2];

	return( d0 * d0 + d1 * d1 + d2 * d2);
}

// vector distance
float vec_dist( const vec3 a, const vec3 b)
{
	float d0, d1, d2;

	d0 = a[0] - b[0];
	d1 = a[1] - b[1];
	d2 = a[2] - b[2];

  return( sqrt( d0 * d0 + d1 * d1 + d2 * d2));
}

float vec_DotProduct(const vec3 a, const vec3 b)
{
	return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

void vec_scale(vec3 p, const vec3 v, float s)
{
	p[0] = v[0] * s;
	p[1] = v[1] * s;
	p[2] = v[2] * s;
}

void vec_scale_add_to(vec3 p, const vec3 v, float s)
{
	p[0] += v[0] * s;
	p[1] += v[1] * s;
	p[2] += v[2] * s;
}

void vec_scale_to(vec3 p, float s)
{
	p[0] *= s;
	p[1] *= s;
	p[2] *= s;
}

void vec_add(vec3 p, const vec3 a, const vec3 b)
{
	p[0] = a[0] + b[0];
	p[1] = a[1] + b[1];
	p[2] = a[2] + b[2];
}

void vec_add_to(vec3 p, const vec3 a)
{
	p[0] += a[0];
	p[1] += a[1];
	p[2] += a[2];
}

void vec_mul(vec3 p, const vec3 a, const vec3 b)
{
	p[0] = a[0] * b[0];
	p[1] = a[1] * b[1];
	p[2] = a[2] * b[2];
}

void vec_mul_to(vec3 p, const vec3 a)
{
	p[0] *= a[0];
	p[1] *= a[1];
	p[2] *= a[2];
}

void vec_sub(vec3 p, const vec3 a, const vec3 b)
{
	p[0] = a[0] - b[0];
	p[1] = a[1] - b[1];
	p[2] = a[2] - b[2];
}

void vec_sub_from(vec3 p, const vec3 a)
{
	p[0] -= a[0];
	p[1] -= a[1];
	p[2] -= a[2];
}

void vec_cross(vec3 p, const vec3 a, const vec3 b)
{
	p[0] = a[1] * b[2] - a[2] * b[1];
	p[1] = a[2] * b[0] - a[0] * b[2];
	p[2] = a[0] * b[1] - a[1] * b[0];
}

void vec_negate(vec3 p)
{
	p[0] = p[1];
	p[1] = p[2];
	p[2] = p[0];
}

float vec_normalize( vec3 a)
{
  float length, iLenght;

  length = sqrt( a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);

  if( length < 0.000001) {   // have some reasonable length
    return( 0.0);            // return length of zero
  }

  iLenght = 1.0 / length;     // Reziprok

	a[0] = a[0] * iLenght;
	a[1] = a[1] * iLenght;
	a[2] = a[2] * iLenght;

  return( length);
}

void vec_copy(vec3 p, const vec3 a)
{
	p[0] = a[0];
	p[1] = a[1];
	p[2] = a[2];
}

void vec_minmax( vec3 min, vec3 max, const vec3 a)
{
	if( min[0] > a[0]) min[0] = a[0];
	if( min[1] > a[1]) min[1] = a[1];
	if( min[2] > a[2]) min[2] = a[2];

	if( max[0] < a[0]) max[0] = a[0];
	if( max[1] < a[1]) max[1] = a[1];
	if( max[2] < a[2]) max[2] = a[2];

}

/** Returns true if this vector is zero length. */
int vec_isZeroLength( const vec3 a)
{
  float  sqlen = (a[0] * a[0]) + (a[1] * a[1]) + (a[2] * a[2]);

  return (sqlen < (1e-06 * 1e-06));
}

// Copy quat
void quat_copy( vec4 q1, const vec4 q2)
{

	q1[0] = q2[0];
	q1[1] = q2[1];
	q1[2] = q2[2];
	q1[3] = q2[3];
}

// set quad from given arguments
void quat_set( vec4 q, float x, float y, float z, float w)
{
  q[0] = x;
  q[1] = y;
  q[2] = z;
  q[3] = w;
}

void quat_from_axis_angle( vec4 q, const vec3 axis, const float angle)
{
  double s = sin( 0.5 * angle);

  q[0] = s * axis[0];
  q[1] = s * axis[1];
  q[2] = s * axis[2];
  q[3] = cos( 0.5 * angle);

  if( q[3] > 0) {      // flip all components

	  q[0] = -q[0];
	  q[1] = -q[1];
	  q[2] = -q[2];
	  q[3] = -q[3];
  }
}

void quat_from_two_normalized_vectors( vec4 q, const vec3 u, const vec3 v)
{
#ifndef use_again
  float real_part = 1.0f + vec_DotProduct( u, v);
  vec3 w;

  if (real_part < 1.e-6f)
  {

    /* If u and v are exactly opposite, rotate 180 degrees
     * around an arbitrary orthogonal axis. Axis normalisation
     * can happen later, when we normalise the quaternion. */
    real_part = 0.0f;
    if( abs(u[ 0]) > abs(u[ 2])) {
      vec_set( w, -u[ 1], u[ 0], 0.f);
    } else {
      vec_set( w, 0.f, -u[ 2], u[ 1]);
    }
  } else {

    /* Otherwise, build quaternion the standard way. */
    vec_cross(w, u, v);
  }

  quat_set( q, w[ 0], w[ 1], w[ 2], real_part);

  quat_normalize( q);
#else

  // v0: u
  // V1: v

  float d = vec_DotProduct( u, v);

  // If dot == 1, vectors are the same
  if (d >= 1.0f) {

    quat_set_identity( q);
    return;
  }

  if (d < (1e-6f - 1.0f)) {

    // Generate an axis
		vec3 axis;

    if( abs(u[ 0]) > abs(u[ 2])) {
      vec_set( axis, -u[ 1], u[ 0], 0.f);
    } else {
      vec_set( axis, 0.f, -u[ 2], u[ 1]);
    }

    vec_normalize( axis);

		quat_from_axis_angle( q, axis, M_PI);

  } else {

    float s = sqrt( (1+d)*2 );
	  float invs = 1 / s;
	  vec3 c;

		vec_cross( c, u, v);

    q[0] = c[0] * invs;
    q[1] = c[1] * invs;
    q[2] = c[2] * invs;
    q[3] = s * 0.5f;

    quat_normalize( q);
  }
#endif
}

// set quad to iendtity
void quat_set_identity( vec4 q)
{

	q[0] = 0.0;
	q[1] = 0.0;
	q[2] = 0.0;
	q[3] = 1.0;
}

// Quat conjugate
void quat_conjugate( vec4 q)
{
	q[0] = -q[0];
	q[1] = -q[1];
	q[2] = -q[2];
	//x/q[3] =  q[3];
}

// Normalize the quat
float quat_normalize( vec4 q)
{
	float d = sqrtf(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);

	if (d >= 0.00001) {
		d = 1 / d;
		q[0] *= d;
		q[1] *= d;
		q[2] *= d;
		q[3] *= d;
	} else {
		q[0] = q[1] = q[2] = 0;
		q[3] = 1;
	}

	return( d);
}

// Quat inverse
void quat_inverse( vec4 q)
{

	quat_conjugate( q);

	//quat_normalize( q);

  if( q[3] > 0) {      // flip all components

	  q[0] = -q[0];
	  q[1] = -q[1];
	  q[2] = -q[2];
	  q[3] = -q[3];
  }
}

// Quat flip
void quat_flip( vec4 q)
{
	q[0] = -q[0];
	q[1] = -q[1];
	q[2] = -q[2];
	q[3] = -q[3];
}

// set quad from rotations in radians
void quat_from_rotation( vec4 q, const vec3 r)
{
#ifdef use_again
  double cx, cy, cz, sx, sy, sz;

  cx = cos( r[0]/2), sx = sin( r[0]/2),
  cy = cos( r[1]/2), sy = sin( r[1]/2),
  cz = cos( r[2]/2), sz = sin( r[2]/2);

  q[0] = sx*cy*cz - cx*sy*sz;
  q[1] = cx*sy*cz + sx*cy*sz;
  q[2] = cx*cy*sz - sx*sy*cz;
  q[3] = cx*cy*cz + sx*sy*sz;

  if( q[3] > 0) {      // flip all components

	  q[0] = -q[0];
	  q[1] = -q[1];
	  q[2] = -q[2];
	  q[3] = -q[3];
  }
#else

  double heading, attitude, bank;
  double c1, c2, c3, s1, s2, s3;

  bank = r[0];
  heading = r[1];
  attitude = r[2];

  c1 = cos( heading / 2);
  c2 = cos( attitude / 2);
  c3 = cos( bank / 2);
  s1 = sin( heading / 2);
  s2 = sin( attitude / 2);
  s3 = sin( bank / 2);

  q[3] = c1 * c2 * c3 - s1 * s2 * s3;
  q[0] = s1 * s2 * c3 + c1 * c2 * s3;
  q[1] = s1 * c2 * c3 + c1 * s2 * s3;
  q[2] = c1 * s2 * c3 - s1 * c2 * s3;

  if( q[3] > 0) {      // flip all components

	  q[0] = -q[0];
	  q[1] = -q[1];
	  q[2] = -q[2];
	  q[3] = -q[3];
  }
#endif

}

// set rotations in radians from quat
void quat_to_rotation( const vec4 q, vec3 r)
{
  float heading, attitude, bank;

	double test = q[0]*q[1] + q[2]*q[3];

	if (test > 0.499) { // singularity at north pole

		heading = 2 * atan2(q[0],q[3]);
		attitude = M_PI/2;
		bank = 0;

	} else if (test < -0.499) { // singularity at south pole

		heading = -2 * atan2(q[0],q[3]);
		attitude = - M_PI/2;
		bank = 0;

	} else {

    double sqx = q[0]*q[0];
    double sqy = q[1]*q[1];
    double sqz = q[2]*q[2];

    heading = atan2(2*q[1]*q[3]-2*q[0]*q[2] , 1 - 2*sqy - 2*sqz);
	  attitude = asin(2*test);
	  bank = atan2(2*q[0]*q[3]-2*q[1]*q[2] , 1 - 2*sqx - 2*sqz);
	}

#ifdef use_again
  r[0] = heading;
  r[1] = attitude;
  r[2] = bank;
#else

  r[0] = bank;
  r[1] = heading;
  r[2] = attitude;
#endif

  return;
}

void quat_transfrom_vec3( const vec4 q, vec3 p, const vec3 a)
{
  vec3 t1, t2;

  vec_cross( t1, q, a);
  vec_scale_add_to( t1, a, q[3]);
  vec_cross( t2, q, t1);
  vec_scale_to( t2, 2.0);
  vec_add( p, a, t2);
}

void quat_mul( vec4 q, const vec4 aArg, const vec4 bArg)
{
  vec4 a, b;

  // get copy of arguments, so output 'q' can be one of the other arguments

  a[ 0] = aArg[ 0];
  a[ 1] = aArg[ 1];
  a[ 2] = aArg[ 2];
  a[ 3] = aArg[ 3];

  b[ 0] = bArg[ 0];
  b[ 1] = bArg[ 1];
  b[ 2] = bArg[ 2];
  b[ 3] = bArg[ 3];

  // multiply ...

  q[ 0] = a[3]*b[0] + a[0]*b[3] + a[1]*b[2] - a[2]*b[1];
  q[ 1] = a[3]*b[1] - a[0]*b[2] + a[1]*b[3] + a[2]*b[0];
  q[ 2] = a[3]*b[2] + a[0]*b[1] - a[1]*b[0] + a[2]*b[3];
  q[ 3] = a[3]*b[3] - a[0]*b[0] - a[1]*b[1] - a[2]*b[2];
}

void mat_vec_mul(vec3 p, const mat4 m, const vec3 v)
{
	assert(p != v);
	p[0] = m[0] * v[0] + m[4] * v[1] + m[8] * v[2] + m[12];
	p[1] = m[1] * v[0] + m[5] * v[1] + m[9] * v[2] + m[13];
	p[2] = m[2] * v[0] + m[6] * v[1] + m[10] * v[2] + m[14];
}

void mat_vec_mul_n(vec3 p, const mat4 m, const vec3 v)
{
	assert(p != v);
	p[0] = m[0] * v[0] + m[4] * v[1] + m[8] * v[2];
	p[1] = m[1] * v[0] + m[5] * v[1] + m[9] * v[2];
	p[2] = m[2] * v[0] + m[6] * v[1] + m[10] * v[2];
}

void calc_mul_matrix(mat4 *skin_matrix, mat4 *abs_pose_matrix, mat4 *inv_bind_matrix, int count)
{
	int i;
	for (i = 0; i < count; i++)
		mat_mul(skin_matrix[i], abs_pose_matrix[i], inv_bind_matrix[i]);
}

void calc_inv_matrix(mat4 *inv_bind_matrix, mat4 *abs_bind_matrix, int count)
{
	int i;
	for (i = 0; i < count; i++)
		mat_invert(inv_bind_matrix[i], abs_bind_matrix[i]);
}

void calc_abs_matrix(mat4 *abs_pose_matrix, mat4 *pose_matrix, struct skel *pSkel)
{
	int i;
	for (i = 0; i < pSkel->joint_count; i++)
		if ( pSkel->j[ i].parent >= 0)
			mat_mul(abs_pose_matrix[i], abs_pose_matrix[ pSkel->j[ i].parent], pose_matrix[i]);
		else
			mat_copy(abs_pose_matrix[i], pose_matrix[i]);
}

void calc_matrix_from_pose(mat4 *pose_matrix, struct pose *pose, int count)
{
	int i;
	for (i = 0; i < count; i++)
		mat_from_pose(pose_matrix[i], pose[i].location, pose[i].rotation, pose[i].scale);
}


float Vector4Normalize( float v[] )
{
	float length, ilength;

	length = v[0]*v[0] + v[1]*v[1] + v[2]*v[2] + v[3]*v[3];

	if( length )
	{
		length = sqrt( length ); // FIXME
		ilength = 1.0/length;
		v[0] *= ilength;
		v[1] *= ilength;
		v[2] *= ilength;
		v[3] *= ilength;
	}

	return length;
}

//
// Given is a line (a point and a vector).
// Get the distance of a point to the line.
// Also return the point on the line to the give point.
//

float PointDistanceFromLine( const vec3 LineP, const vec3 LineV, const vec3 Point, vec3 PointOnLine)
{
  // line equation:
  // x = x0 + ft
  // y = y0 + gt
  // z = z0 + ht

  // LineV must be normalized

  // The value of parameter, t, where the point intersects the line is
  // given by:
  // t = (f*( p(1) - x0) + g*(p(2) - y0) + h*(p(3) - z0)/( f * f + g * g + h*h);

  float x0, y0, z0, f, g, h, dist, t;

  vec3 VecPointToLine;

  x0 = LineP[ 0];
  y0 = LineP[ 1];
  z0 = LineP[ 2];

  f = LineV[ 0];
  g = LineV[ 1];
  h = LineV[ 2];

  t = (f*(Point[ 0] - x0) + g*(Point[ 1] - y0) + h*(Point[ 2] - z0)) / ( f * f + g * g + h*h);

  PointOnLine[ 0] = x0 + f * t;
  PointOnLine[ 1] = y0 + g * t;
  PointOnLine[ 2] = z0 + h * t;

  vec_sub( VecPointToLine, PointOnLine, Point);
  dist = vec_normalize( VecPointToLine);

  return dist;
}

//
// Given is a line (the line base point and a vector).
// Calculate the nearest point on the line to the give point.
// Return the distance between line base point and this neares point.
// If pBasePointOnLine is != NULL, return here the distance of the point
// project to the line to LineP. This distance can be negative.
// Multiply LineV with this and add to LineP to get the coordinate of the
// base point.
//

float PointDistNearestToLineBasePoint( const vec3 LineP, const vec3 LineV, const vec3 Point,
                                       float *pBasePointOnLine)
{
  // line equation:
  // x = x0 + ft
  // y = y0 + gt
  // z = z0 + ht

  // LineV must be normalized

  // The value of parameter, t, where the point intersects the line is
  // given by:
  // t = (f*( p(1) - x0) + g*(p(2) - y0) + h*(p(3) - z0)/( f * f + g * g + h*h);

  float x0, y0, z0, f, g, h, dist, t;

  vec3 PointOnLine, VecPointToLine;

  x0 = LineP[ 0];
  y0 = LineP[ 1];
  z0 = LineP[ 2];

  f = LineV[ 0];
  g = LineV[ 1];
  h = LineV[ 2];

  t = (f*(Point[ 0] - x0) + g*(Point[ 1] - y0) + h*(Point[ 2] - z0)) / ( f * f + g * g + h*h);

  PointOnLine[ 0] = x0 + f * t;
  PointOnLine[ 1] = y0 + g * t;
  PointOnLine[ 2] = z0 + h * t;

  vec_sub( VecPointToLine, PointOnLine, Point);
  dist = vec_normalize( VecPointToLine);

  if( pBasePointOnLine != NULL) {  // Want this result

    *pBasePointOnLine = t;
  }

  return dist;
}

/****************************** End Of File ******************************/
