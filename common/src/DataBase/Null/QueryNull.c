/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2016 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/Query.h>
#include <DataBase/Null/General.h>
#include <DataBase/Default.h>

#include <string.h> // strcpy

/////////////////////////////////////////////////////////////
/// Makes a query to the database
/////////////////////////////////////////////////////////////
bool Query(QueryResultType * Result, char * Body, ...)
{
	struct DataBase * DB = DataBaseGetDefault();

	// Check if the database is avaible
	if (DB && DB->Connection)
	{
		if (Result)
		{
			*Result = NULL;
		}

		return true;
	}

	// Show error
	MessageError("Query", "Error: NULL");

	return false;
}

/////////////////////////////////////////////////////////////
/// Get the next result of a query and releases the previous
///	in case of multiple queries
/////////////////////////////////////////////////////////////
QueryResultType QueryGetNextResult(QueryResultType Result)
{
	struct DataBase * DB = DataBaseGetDefault();

	// Check if the database is avaible
	if (DB && DB->Connection)
	{
		return NULL;
	}

	return NULL;
}

/////////////////////////////////////////////////////////////
/// Get the number of rows of the last query result
/////////////////////////////////////////////////////////////
uinteger QueryRowCount(QueryResultType Result)
{
	struct DataBase * DB = DataBaseGetDefault();

	// Check if the database is avaible and the query is valid
	if (DB && DB->Connection)
	{
		return UINTEGER_SUFFIX(0);
	}

	return UINTEGER_SUFFIX(0);
}

/////////////////////////////////////////////////////////////
/// Get a row from the last query result
/////////////////////////////////////////////////////////////
QueryRowType QueryGetRow(QueryResultType Result)
{
	struct DataBase * DB = DataBaseGetDefault();

	// Check if the database is avaible and the query is valid
	if (DB && DB->Connection)
	{
		return NULL;
	}

	// Show the error
	MessageError("QueryGet", "Error: Invalid Row");

	return NULL;
}

/////////////////////////////////////////////////////////////
/// Get the number of colums of the last query result
/////////////////////////////////////////////////////////////
uinteger QueryFieldCount(QueryResultType Result)
{
	struct DataBase * DB = DataBaseGetDefault();

	// Check if the database is avaible and the query is valid
	if (DB && DB->Connection)
	{
		return UINTEGER_SUFFIX(0);
	}

	return UINTEGER_SUFFIX(0);
}

/////////////////////////////////////////////////////////////
/// Get the number of colums of the last query result
/////////////////////////////////////////////////////////////
void QueryGetField(QueryRowType Row, uinteger Field, uinteger Type, QueryFieldType Dest)
{
	switch (Type)
	{
		case QUERY_FIELD_TYPE_STR :
		{
			strcpy(Dest, "NULL");
			break;
		}

		case QUERY_FIELD_TYPE_INT32 :
		{
			int32 * Value = (int32 *)Dest;

			*Value = 0;
			break;
		}

		case QUERY_FIELD_TYPE_UINT32 :
		{
			uint32 * Value = (uint32 *)Dest;

			*Value = 0;
			break;
		}

		case QUERY_FIELD_TYPE_BOOL :
		{
			bool * Boolean = (bool *)Dest;

			*Boolean = false;
			break;
		}

		case QUERY_FIELD_TYPE_FLOAT :
		{
			float * Float = (float *)Dest;

			*Float = 0.0f;
			break;
		}
	}
}

/////////////////////////////////////////////////////////////
/// Release a query
/////////////////////////////////////////////////////////////
void QueryRelease(QueryResultType Result)
{
	// Nothing
}
