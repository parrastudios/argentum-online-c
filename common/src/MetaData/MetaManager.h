/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	METADATA_MANAGER_H
#define METADATA_MANAGER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/VariableArguments.h>

#include <Preprocessor/Arguments.h>

#include <MetaData/Type.h>
#include <MetaData/Value.h>
#include <MetaData/Reference.h>
#include <MetaData/Parameter.h>
#include <MetaData/Function.h>
#include <MetaData/Object.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define ValueCreate(Name, Type, Count, Data) \
	ValueCreateType(Name, MetadataTypeId(Type), Count, Data)

#define ValueBind(Name, Type, Count, Data) \
	ValueBindType(Name, MetadataTypeId(Type), Count, Data)

#define ValueSignature(Name, Type, Count) \
	ValueSignatureType(Name, MetadataTypeId(Type), Count)

#define ObjectBindAttribute(Object, Type, TypeDecl, Member) \
	ObjectBindAttributeOffset(Object, MetadataTypeId(Type), OffsetOf(TypeDecl, Member), PPREPROCESSOR_STRINGIFY(Member))

#define ObjectBindMethod(Object, Type, Member, Args) \
	ObjectBindMethodOffset(Object, OffsetOf(Type, Member), PPREPROCESSOR_STRINGIFY(Member), MetadataParameterListCreate(PREPROCESSOR_ARGS_COUNT_PREPREND(Args)))

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef struct MetadataManagerType
{
	// Type block
	MetadataTypeSize (*SizeOf)(MetadataType);

	// Value block
	MetadataValueType (*ValueCreateType)(MetadataValueNameType, MetadataType, MetadataValueCountType, void *);

	MetadataValueType (*ValueBindType)(MetadataValueNameType, MetadataType, MetadataValueCountType, void *);

	MetadataValueType (*ValueSignatureType)(MetadataValueNameType, MetadataType, MetadataValueCountType);

	MetadataValueType (*ValueCopy)(MetadataValueNameType);

	MetadataValueType (*ValueGet)(MetadataValueNameType);

	bool (*ValueValid)(MetadataValueType);

	bool (*ValueAbstract)(MetadataValueType);

	bool (*ValueAbstractBuiltin)(MetadataValueType);

	bool (*ValueAbstractArray)(MetadataValueType);

	bool (*ValueInstanced)(MetadataValueType);

	bool (*ValueBuiltin)(MetadataValueType);

	bool (*ValueArray)(MetadataValueType);

	MetadataType (*ValueTypeOf)(MetadataValueType);

	MetadataTypeSize (*ValueTypeSize)(MetadataValueType);

	MetadataValueCountType (*ValueCount)(MetadataValueType);

	#define MetadataDataModifierGetDecl(Type, Name, Id) \
		MetadataTypeDecl(Type) (*PREPROCESSOR_CONCAT(ValueGet, Name))(MetadataValueType);

		// Define metadata type declaration
		METADATA_TYPE_DECL(MetadataDataModifierGetDecl)

	#undef MetadataDataModifierGetDecl

	#define MetadataDataModifierGetAtDecl(Type, Name, Id) \
		MetadataTypeDecl(Type) (*PREPROCESSOR_CONCAT(ValueGetAt, Name))(MetadataValueType, MetadataValueCountType);

		// Define metadata type declaration
		METADATA_TYPE_DECL(MetadataDataModifierGetAtDecl)

	#undef MetadataDataModifierGetAtDecl

	#define MetadataDataModifierSetDecl(Type, Name, Id) \
		void (*PREPROCESSOR_CONCAT(ValueSet, Name))(MetadataValueType, MetadataTypeDecl(Type));

		// Define metadata type declaration
		METADATA_TYPE_DECL(MetadataDataModifierSetDecl)

	#undef MetadataDataModifierSetDecl

	#define MetadataDataModifierSetAtDecl(Type, Name, Id) \
		void (*PREPROCESSOR_CONCAT(ValueSetAt, Name))(MetadataValueType, MetadataValueCountType, MetadataTypeDecl(Type));

		// Define metadata type declaration
		METADATA_TYPE_DECL(MetadataDataModifierSetAtDecl)

	#undef MetadataDataModifierSetAtDecl

	void (*ValueDestroy)(MetadataValueType);

	// Reference block
	MetadataReferenceType (*ReferenceCreate)(MetadataReferenceNameType, MetadataReferenceTypeInfo, MetadataStorageIndexType);

	MetadataReferenceType (*ReferenceSignature)(MetadataReferenceNameType, MetadataReferenceTypeInfo);

	MetadataReferenceType (*ReferenceCopy)(MetadataReferenceNameType);

	MetadataReferenceType (*ReferenceGet)(MetadataReferenceNameType);

	void (*ReferenceSetPointer)(MetadataReferenceType, MetadataStorageIndexType);

	MetadataStorageIndexType (*ReferenceGetPointer)(MetadataReferenceType);

	MetadataReferenceTypeInfo (*ReferenceTypeOf)(MetadataReferenceType);

	void (*ReferenceDestroy)(MetadataReferenceType);

	// Parameter block
	MetadataParameterSignature * (*ParameterSignature)(MetadataParameterSignature *, MetadataParameterTypeInfo, MetadataStorageResourceType, MetadataStorageIndexType);

	MetadataParameterListType (*ParameterListSignature)(MetadataParameterListCountType, MetadataParameterSignature *);

	MetadataStorageIndexType (*ParameterRetrieve)(MetadataParameterListType, MetadataParameterType);

	// Function block
	MetadataFunctionType (*FunctionCreate)(MetadataFunctionNameType, MetadataParameterListType);

	MetadataFunctionType (*FunctionBind)(MetadataFunctionNameType, MetadataParameterListType, void *);

	MetadataFunctionType (*FunctionCopy)(MetadataFunctionNameType);

	MetadataFunctionType (*FunctionGet)(MetadataFunctionNameType);

	MetadataParameterListType (*FunctionGetSignature)(MetadataFunctionType);

	void (*FunctionExecute)(MetadataFunctionNameType, ...);

	void (*FunctionDestroy)(MetadataFunctionType);

	// Object block

	// Annotation block

	// Runtime block

	// Scope block
	bool (*ScopeCreate)(MetadataScopeName, MetadataStorageName);

	bool (*ScopeBegin)(MetadataScopeName);

	bool (*ScopeEnd)(MetadataScopeName);

	bool (*ScopeDestroy)(MetadataScopeName);

	// Storage block
	bool (*StorageCreate)(MetadataStorageName);

	bool (*StorageAttachScope)(MetadataStorageName, MetadataScopeName);

	bool (*StorageDestroy)(MetadataStorageName);

	// General block
	bool (*Initialize)();

	void (*Destroy)();

} * MetadataManagerType;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get global metadata manager instance
////////////////////////////////////////////////////////////
MetadataManagerType MetadataManagerGlobalInstance();

#endif // METADATA_MANAGER_H
