/*
***********************************************************************************
 * IqeB_VCGLib_Tridecimator.cpp

 Triangle reduction.
 Base was the file 'tridecimator.cpp' of the VCGLib examples.
 See http://vcg.isti.cnr.it/~cignoni/newvcglib/html/

13.12.2014 RR: * First editon of this file.
               * Reworked from console application to interface
                 the IqeB meshes.


-------------------------------------------------------------------------------
Usage printout of (not anymore used) console application

           "---------------------------------\n"
           "         TriSimp V.1.0 \n"
           "     http://vcg.isti.cnr.it\n"
           "    http://vcg.sourceforge.net\n"
           "   release date: "__DATE__"\n"
           "---------------------------------\n\n"
          "TriDecimator 1.0 \n"__DATE__"\n"
      "Copyright 2003-2012 Visual Computing Lab I.S.T.I. C.N.R.\n"
      "\nUsage:  "\
      "tridecimator fileIn fileOut face_num [opt]\n"\
      "Where opt can be:\n"\
      "     -e# QuadricError threshold  (range [0,inf) default inf)\n"
      "     -b# Boundary Weight (default .5)\n"
      "     -q# Quality threshold (range [0.0, 0.866],  default .3 )\n"
      "     -n# Normal threshold  (degree range [0,180] default 90)\n"
      "     -E# Minimal admitted quadric value (default 1e-15, must be >0)\n"
      "     -Q[y|n]  Use or not Quality Threshold (default yes)\n"
      "     -N[y|n]  Use or not Normal Threshold (default no)\n"
      "     -A[y|n]  Use or not Area Weighted Quadrics (default yes)\n"
      "     -O[y|n]  Use or not vertex optimal placement (default yes)\n"
      "     -S[y|n]  Use or not Scale Independent quadric measure(default yes) \n"
      "     -B[y|n]  Preserve or not mesh boundary (default no)\n"
      "     -T[y|n]  Preserve or not Topology (default no)\n"
      "     -H[y|n]  Use or not Safe Heap Update (default no)\n"
      "     -P       Before simplification, remove duplicate & unreferenced vertices\n"
                                       );
*/

#include "IqeBrowser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

//---- VCGLib includes -----------------------------

// stuff to define the mesh
#include <vcg/complex/complex.h>

// io
#include <wrap/io_trimesh/import.h>
#include <wrap/io_trimesh/export_ply.h>

// local optimization
#include <vcg/complex/algorithms/local_optimization.h>
#include <vcg/complex/algorithms/local_optimization/tri_edge_collapse_quadric.h>

using namespace vcg;
using namespace tri;

/**********************************************************
Mesh Classes for Quadric Edge collapse based simplification

For edge collpases we need verteses with:
- V->F adjacency
- per vertex incremental mark
- per vertex Normal


Moreover for using a quadric based collapse the vertex class
must have also a Quadric member Q();
Otherwise the user have to provide an helper function object
to recover the quadric.

******************************************************/
// The class prototypes.
class MyVertex;
class MyEdge;
class MyFace;

struct MyUsedTypes: public UsedTypes<Use<MyVertex>::AsVertexType,Use<MyEdge>::AsEdgeType,Use<MyFace>::AsFaceType> {};

class MyVertex  : public Vertex< MyUsedTypes,
  vertex::VFAdj,
  vertex::Coord3f,
  vertex::Normal3f,
  vertex::Mark,
  vertex::BitFlags  >
{
public:
  vcg::math::Quadric<double> &Qd()
  {
    return q;
  }
private:
  math::Quadric<double> q;
};

class MyEdge : public Edge< MyUsedTypes> {};

typedef BasicVertexPair<MyVertex> VertexPair;

class MyFace    : public Face< MyUsedTypes,
  face::VFAdj,
  face::VertexRef,
  face::Normal3f,
  face::BitFlags > {};

// the main mesh class
class MyMesh    : public vcg::tri::TriMesh<std::vector<MyVertex>, std::vector<MyFace> > {};


class MyTriEdgeCollapse: public vcg::tri::TriEdgeCollapseQuadric< MyMesh, VertexPair, MyTriEdgeCollapse, QInfoStandard<MyVertex>  >
{
public:
  typedef  vcg::tri::TriEdgeCollapseQuadric< MyMesh,  VertexPair, MyTriEdgeCollapse, QInfoStandard<MyVertex>  > TECQ;
  typedef  MyMesh::VertexType::EdgeType EdgeType;
  inline MyTriEdgeCollapse(  const VertexPair &p, int i, BaseParameterClass *pp) :TECQ(p,i,pp) {}
};

/************************************************************************************
 * IqeB_Mesh_VCGLib_MeshToVCGLib()
 *
 * Copy mesh_IqeB to mesh_VCBLib
 *
 * return:   0: OK
 *         < 0: error
 */

static int IqeB_Mesh_VCGLib_MeshToVCGLib( struct mesh *mesh_IqeB, MyMesh &m)
{
  int i, nMeshTriangles;
  int *pTri;

  // prepare mesh

  m.Clear();

  // get the number of triangles

  nMeshTriangles = 0;
	for( i = 0; i < mesh_IqeB->part_count; i++) {

    nMeshTriangles += mesh_IqeB->part[i].count / 3;
  }

  // security tests

  if( mesh_IqeB->vertex_count < 3 || nMeshTriangles < 1) {    // need some reasonable minimum

    return( -1);
  }

  // add vertices

  Allocator<MyMesh>::AddVertices(m, mesh_IqeB->vertex_count);
  for( i = 0; i < mesh_IqeB->vertex_count; i++) {

    m.vert[i].P() = Point3f( mesh_IqeB->position[i*3+0], mesh_IqeB->position[i*3+1], mesh_IqeB->position[i*3+2]);
  }

  // add triangles

  Allocator<MyMesh>::AddFaces( m, nMeshTriangles);

  pTri = mesh_IqeB->element;
  for( i = 0; i < nMeshTriangles; i++, pTri += 3) {

    m.face[i].V(0)=&m.vert[pTri[0]];
    m.face[i].V(1)=&m.vert[pTri[1]];
    m.face[i].V(2)=&m.vert[pTri[2]];
  }

  return( 0);    // return OK
}

/************************************************************************************
 * IqeB_Mesh_VCGLib_MeshToIqeB()
 *
 * Copy mesh_IqeB to mesh_VCBLib
 *
 * return:   0: OK
 *         < 0: error
 */

static int IqeB_Mesh_VCGLib_MeshToIqeB( MyMesh &m, struct mesh *mesh_IqeB)
{
  int i, nVertices, nTriangles;

  IqeB_MeshFreeAllData( mesh_IqeB);    // Enshure all data is freed

  // get the number of elements

  nVertices  = m.vn;
  nTriangles = m.fn;

  // allocate memory

  mesh_IqeB->position = (float *)malloc( sizeof( float) * nVertices * 3);

  if( mesh_IqeB->position == NULL) {  // memory allocation failed

    return( -1);
  }

  mesh_IqeB->element  = (int *)malloc( sizeof( int) * nTriangles * 3);

  if( mesh_IqeB->element == NULL) {  // memory allocation failed

    free( mesh_IqeB->position);      // for shanti's shake, have to free this already allocated memory

    return( -2);
  }

  // copy vertices

  for( i = 0; i < nVertices; i++) {

    mesh_IqeB->position[ i*3+0] = m.vert[i].P()[0];
    mesh_IqeB->position[ i*3+1] = m.vert[i].P()[1];
    mesh_IqeB->position[ i*3+2] = m.vert[i].P()[2];
  }

  mesh_IqeB->vertex_count = nVertices;

  // copy triangle

  for( i = 0; i < nTriangles; i++) {

    mesh_IqeB->element[ i*3+0] = m.face[i].V(0) - &*m.vert.begin();
    mesh_IqeB->element[ i*3+1] = m.face[i].V(1) - &*m.vert.begin();
    mesh_IqeB->element[ i*3+2] = m.face[i].V(2) - &*m.vert.begin();
  }

  mesh_IqeB->element_count = nTriangles * 3;

  // prepare rest of model

	mesh_IqeB->normal      = NULL;
	mesh_IqeB->texcoord    = NULL;
	mesh_IqeB->color       = NULL;
	mesh_IqeB->blendindex  = NULL;
	mesh_IqeB->blendweight = NULL;
	mesh_IqeB->aposition   = NULL;
	mesh_IqeB->anormal     = NULL;

	mesh_IqeB->part_count = 1;
	mesh_IqeB->part = (part *)malloc( sizeof( struct part));
	memset( mesh_IqeB->part, 0, sizeof( struct part));

	mesh_IqeB->part->pMaterialStr     = strdup( "");
	mesh_IqeB->part->pMaterialTags    = strdup( "");
	mesh_IqeB->part->pMaterialBasedir = strdup( "");
	mesh_IqeB->part->material = -1;
	mesh_IqeB->part->first = 0;
	mesh_IqeB->part->count = mesh_IqeB->element_count;

	// recontruct normals if we have none

	if( mesh_IqeB->normal == NULL) {

    IqeB_MeshComputeVertexNormals( mesh_IqeB);
	}

  return( 0);    //  OK
}

/************************************************************************************
 * IqeB_VCGLib_Tridecimator()
 *
 * Triangle reduciton
 *
 * return:   0: OK
 *         < 0: error
 */

int IqeB_Mesh_VCGLib_Tridecimator( struct mesh *mesh_IqeB,
                                   int ReduceVertexPercentage,
                                   int ReduceMaxVertexAmount,
                                   int ReduceMinVertexAmount)
{

  MyMesh mesh_VCBLib;                         // mesh to simplify
  int FinalSize;                              // number of vertices after triangle reduciton
  int ierr;

  //
	// How many vertices to reduce in percent
  //

	FinalSize = mesh_IqeB->vertex_count / 2;          // preset 50 %

  if( ReduceVertexPercentage < 100 && ReduceVertexPercentage > 0) {  // is in reasonable range

	  FinalSize = (mesh_IqeB->vertex_count * (100 - ReduceVertexPercentage) + 50) / 100;
  }

	if( ReduceMaxVertexAmount > 0 && FinalSize > ReduceMaxVertexAmount) {  // is valid and reduce by max # vertices

     FinalSize = ReduceMaxVertexAmount;
	}

	if( ReduceMinVertexAmount > 0 && FinalSize < ReduceMinVertexAmount) {  // is valid and keep minimum this # of vertices

     FinalSize = ReduceMinVertexAmount;
	}

	// security tests of # vertices

	if(FinalSize < 5) {  // the absolute minimim we want

    FinalSize = 5;
	}

	if(FinalSize > mesh_IqeB->vertex_count) {

    FinalSize = mesh_IqeB->vertex_count;
	}

	// security test, we don't reduce

	if( FinalSize == mesh_IqeB->vertex_count) {   // This would change nothing

	  return( 0);                             // Simply return OK
	}

  //
  // Copy mesh_IqeB to mesh_VCBLib
  //

  ierr = IqeB_Mesh_VCGLib_MeshToVCGLib( mesh_IqeB, mesh_VCBLib);
  if( ierr < 0) {  // error on conversion

    return( ierr);
  }

  //
  // preset/modify parameters
  //

  TriEdgeCollapseQuadricParameter qparams;
  float TargetError;
  bool CleaningFlag;

  TargetError = std::numeric_limits<float>::max(); // -e# QuadricError threshold  (range [0,inf) default inf)
  qparams.BoundaryWeight   = 0.5;                  // -b# Boundary Weight (default .5)
  qparams.QualityThr       = 0.3;                  // -q# Quality threshold (range [0.0, 0.866],  default .3 )
  qparams.NormalThrRad = math::ToRad( 90.0);       // -n# Normal threshold  (degree range [0,180] default 90)
  qparams.QuadricEpsilon   = 1e-15;                // -E# Minimal admitted quadric value (default 1e-15, must be >0)
  qparams.QualityCheck     = true;                 // -Q[y|n]  Use or not Quality Threshold (default yes)
  qparams.NormalCheck      = false;                // -N[y|n]  Use or not Normal Threshold (default no)
  qparams.UseArea          = true;                 // -A[y|n]  Use or not Area Weighted Quadrics (default yes)
  qparams.OptimalPlacement = true;                 // -O[y|n]  Use or not vertex optimal placement (default yes)
  qparams.ScaleIndependent = true;                 // -S[y|n]  Use or not Scale Independent quadric measure(default yes)
  qparams.PreserveBoundary = false;                // -B[y|n]  Preserve or not mesh boundary (default no)
  qparams.PreserveTopology = false;                // -T[y|n]  Preserve or not Topology (default no)
  qparams.SafeHeapUpdate   = false;                // -H[y|n]  Use or not Safe Heap Update (default no)
  CleaningFlag             = false;                // -P       Before simplification, remove duplicate & unreferenced vertices

  if(CleaningFlag) {

#ifdef use_again
    int dup = tri::Clean<MyMesh>::RemoveDuplicateVertex(mesh_VCBLib);
    int unref =  tri::Clean<MyMesh>::RemoveUnreferencedVertex(mesh_VCBLib);

    //x/printf("Removed %i duplicate and %i unreferenced vertices from mesh \n",dup,unref);

#else // 13.12.2014 RR: don't like warnings about unused variables

    tri::Clean<MyMesh>::RemoveDuplicateVertex(mesh_VCBLib);
    tri::Clean<MyMesh>::RemoveUnreferencedVertex(mesh_VCBLib);
#endif
  }

  // update normals

  tri::UpdateNormal<MyMesh>::NormalizePerFace(mesh_VCBLib);

  //
  // triangle reduciton
  //

  vcg::tri::UpdateBounding<MyMesh>::Box(mesh_VCBLib);

  // decimator initialization
  vcg::LocalOptimization<MyMesh> DeciSession(mesh_VCBLib,&qparams);

  DeciSession.Init<MyTriEdgeCollapse>();
  //x/printf("Initial Heap Size %i\n",int(DeciSession.h.size()));

  DeciSession.SetTargetSimplices(FinalSize);
  DeciSession.SetTimeBudget(0.5f);
  if(TargetError< std::numeric_limits<float>::max() ) DeciSession.SetTargetMetric(TargetError);

  while(DeciSession.DoOptimization() && mesh_VCBLib.fn>FinalSize && DeciSession.currMetric < TargetError) {

    //x/printf("Current Mesh size %7i heap sz %9i err %9g \r",mesh_VCBLib.fn, int(DeciSession.h.size()),DeciSession.currMetric);
  }

  //x/printf("mesh  %d %d Error %g \n",mesh_VCBLib.vn,mesh_VCBLib.fn,DeciSession.currMetric);

  //
  // Garbage collection, remove deleted elements
  //

  // To remove the elements marked as deleted use
  vcg::tri::Allocator<MyMesh>::CompactFaceVector(mesh_VCBLib);
  vcg::tri::Allocator<MyMesh>::CompactVertexVector(mesh_VCBLib);

  // To clean all the containers from deleted elements
  vcg::tri::Allocator<MyMesh>::CompactEveryVector(mesh_VCBLib);

  //
  // Copy mesh_VCBLib back to mesh_IqeB
  //

  ierr = IqeB_Mesh_VCGLib_MeshToIqeB( mesh_VCBLib, mesh_IqeB);
  if( ierr < 0) {  // error on conversion

    return( ierr);
  }

  return 0;
}
