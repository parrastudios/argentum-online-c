/*
***********************************************************************************
 IqeB_Import_Utils.cpp

 Utilities for importing files.

14.12.2014 RR: * First editon of this file.

*/

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/gl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

/************************************************************************************
 * loadmodel() helper functions
 */

// global scratch buffers used on importing files
struct floatarray IqeB_ImportArray_position = { 0, 0, NULL };
struct floatarray IqeB_ImportArray_normal = { 0, 0, NULL };
struct floatarray IqeB_ImportArray_texcoord = { 0, 0, NULL };
struct floatarray IqeB_ImportArray_color = { 0, 0, NULL };
struct intarray   IqeB_ImportArray_blendindex = { 0, 0, NULL };
struct floatarray IqeB_ImportArray_blendweight = { 0, 0, NULL };
struct intarray   IqeB_ImportArray_element = { 0, 0, NULL };
struct partarray  IqeB_ImportArray_partbuf = { 0, 0, NULL };

// reset temporary arrays used on loading files
void IqeB_ImportArraysReset()
{

  IqeB_ImportArray_position.len = 0;
	IqeB_ImportArray_normal.len = 0;
	IqeB_ImportArray_texcoord.len = 0;
	IqeB_ImportArray_color.len = 0;
	IqeB_ImportArray_blendindex.len = 0;
	IqeB_ImportArray_blendweight.len = 0;
	IqeB_ImportArray_element.len = 0;
  IqeB_ImportArray_partbuf.len = 0;
}

// duplicate array to new allocated memory
void *dupArray( void *data, int count, int size)
{
	if (count == 0)
		return NULL;

	if (data == NULL)
		return NULL;

	void *p = malloc(count * size);
	memcpy(p, data, count * size);
	return p;
}

// allocated memmory
// size count or size of 0 releases data.
void allocArray( void **ppMemData, int count, int size)
{
	if (count <= 0 || size <= 0) {   // free data

    if( *ppMemData != NULL) {   // There was someting allocated

      free( *ppMemData);        // free it
      *ppMemData = NULL;        // set pointer to NULL
    }
		return;
	}

  if( *ppMemData == NULL) {   // no memory allocated until now


    *ppMemData = malloc( count * size);  // allocated new memory

  } else {                   // there is already memory allocated

    *ppMemData = realloc( *ppMemData, count * size);  // reallocate memory
  }

  return;
}

// pushfloat
void pushfloat(struct floatarray *a, float v)
{
	if (a->len + 1 >= a->cap) {
		a->cap = 600 + a->cap * 2;
		a->data = (float *)realloc(a->data, a->cap * sizeof(*a->data));
	}
	a->data[a->len++] = v;
}

// pushint
void pushint(struct intarray *a, int v)
{
	if (a->len + 1 >= a->cap) {
		a->cap = 600 + a->cap * 2;
		a->data = (int *)realloc(a->data, a->cap * sizeof(*a->data));
	}
	a->data[a->len++] = v;
}

// pushpart
void pushpart( struct partarray *a, int first, int last, char *pMeshName,
               char *pMaterialString, char *pMaterialBasedir)
{
	/* merge parts if they share materials */
#ifdef use_again // 21.12.2014 RR: don't use the material argument,
                 //                maybe can use the pMaterialString.
	if (a->len > 0 && (int)a->data[a->len-1].material == material) {
		a->data[a->len-1].count += last - first;
		return;
	}
#endif

  // get tags from material string

  char *pMaterialStr;        // material string
  char *pMaterialTags;       // material tags
  char *pDelim;

  if( pMaterialString == NULL) {  // is null pointer

    pMaterialString = (char *)"";
  }

  pDelim = strrchr( pMaterialString, ';'); // have tag delimiter

  if( pDelim == NULL) {  // no delimiter

    pMaterialStr  = pMaterialString;
    pMaterialTags = (char *)"";

  } else {               // have tags

    pMaterialStr  = pDelim + 1;
    pMaterialTags = pMaterialString;

    *pDelim = '\0';   // overwrite semicolon by 0 byte.
  }

  // normalize path slasches

  IqeB_FileNormalizePathChars( pMaterialString);   // Normalize path slasches
  IqeB_FileNormalizePathChars( pMaterialBasedir);  // Normalize path slasches

  // ...

	if (a->len + 1 >= a->cap) {
		a->cap = 10 + a->cap * 2;
		a->data = (part *)realloc(a->data, a->cap * sizeof(*a->data));
	}
	a->data[a->len].first = first;
	a->data[a->len].count = last - first;
	a->data[a->len].material = -1;   // flag, no material loaded
	a->data[a->len].pMeshName        = strdup( pMeshName != NULL ? pMeshName : "");
	a->data[a->len].pMaterialStr     = strdup( pMaterialStr  != NULL ? pMaterialStr  : "");
	a->data[a->len].pMaterialTags    = strdup( pMaterialTags != NULL ? pMaterialTags : "");
	a->data[a->len].pMaterialBasedir = strdup( pMaterialBasedir);
	a->data[a->len].pMaterialLoadedFile = NULL;
	a->len++;

  if( pDelim != NULL) {  // Got delimiter

    *pDelim = ';';       // write back delimiter
  }
}

// pushanim
// NOTE: pModel can have a NULL pointer, is used from animation clipbaord code
anim *pushanim( struct IQE_model *pModel, char *name)
{
	struct anim *anim;

  // allocated and preset animation

	anim = (struct anim *)malloc( sizeof(struct anim)); // allocate
	memset( anim, 0, sizeof(struct anim));              // zero data

	anim->name = strdup(name);
	anim->len = anim->cap = 0;
	anim->data = NULL;
	anim->framerate = 10.0;          // preset 10 fps
	anim->looped = false;            // preset not lopped
  anim->FrameMarked = -1;          // Reset marked frame

  // manage table of animation pointers

  if( pModel != NULL) {    // Have a model housing the animation data

	  if (pModel->anim_count + 1 >= pModel->anim_cap) {
		  pModel->anim_cap = 16 + pModel->anim_cap;
		  pModel->anim_data = (struct anim **)realloc( pModel->anim_data, pModel->anim_cap * sizeof(*pModel->anim_data));
	  }
	  pModel->anim_data[ pModel->anim_count++] = anim;
  }

	return anim;
}

// pushframe
struct pose *pushframe(struct anim *a, int bone_count)
{
	struct pose *pose;

	pose = (struct pose *)malloc( sizeof(struct pose) * bone_count); // allocate
	memset( pose, 0, sizeof(struct pose) * bone_count);              // zero data

	if (a->len + 1 >= a->cap) {
		a->cap = 64 + a->cap * 2;
		a->data = (struct pose **)realloc(a->data, a->cap * sizeof(*a->data));
	}
	a->data[a->len++] = pose;
	return pose;
}

// addposition
void addposition(float x, float y, float z)
{
	pushfloat(&IqeB_ImportArray_position, x);
	pushfloat(&IqeB_ImportArray_position, y);
	pushfloat(&IqeB_ImportArray_position, z);
}

// addnormal
void addnormal(float x, float y, float z)
{
	pushfloat(&IqeB_ImportArray_normal, x);
	pushfloat(&IqeB_ImportArray_normal, y);
	pushfloat(&IqeB_ImportArray_normal, z);
}

// addtexcoord
void addtexcoord(float u, float v)
{
	pushfloat(&IqeB_ImportArray_texcoord, u);
	pushfloat(&IqeB_ImportArray_texcoord, v);
}

// addcolor
void addcolor(float x, float y, float z, float w)
{
	pushfloat(&IqeB_ImportArray_color, x);
	pushfloat(&IqeB_ImportArray_color, y);
	pushfloat(&IqeB_ImportArray_color, z);
	pushfloat(&IqeB_ImportArray_color, w);
}

// addblend
void addblend(int a, int b, int c, int d, float x, float y, float z, float w)
{
	float total = x + y + z + w;
	pushint(&IqeB_ImportArray_blendindex, a);
	pushint(&IqeB_ImportArray_blendindex, b);
	pushint(&IqeB_ImportArray_blendindex, c);
	pushint(&IqeB_ImportArray_blendindex, d);
	pushfloat(&IqeB_ImportArray_blendweight, x / total);
	pushfloat(&IqeB_ImportArray_blendweight, y / total);
	pushfloat(&IqeB_ImportArray_blendweight, z / total);
	pushfloat(&IqeB_ImportArray_blendweight, w / total);
}

// addtriangle
void addtriangle(int a, int b, int c)
{
	pushint(&IqeB_ImportArray_element, a);
	pushint(&IqeB_ImportArray_element, b);
	pushint(&IqeB_ImportArray_element, c);
}

// addtriangleFlipWinding
void addtriangleFlipWinding(int a, int b, int c)
{
	// flip triangle winding
	pushint(&IqeB_ImportArray_element, c);
	pushint(&IqeB_ImportArray_element, b);
	pushint(&IqeB_ImportArray_element, a);
}

// parsestring
char *parsestring(char **stringp)
{
	char *start, *end, *s = *stringp;

	while (isspace(*s)) s++;

	if ( *s == 0) {       // already end of string
  	*stringp = s;
	  return s;
	}

	if (*s == '"') {
		s++;
		start = end = s;
		while (*end && *end != '"') end++;
		if (*end) *end++ = 0;
	} else {
		start = end = s;
		while (*end && !isspace(*end)) end++;
		if (*end) *end++ = 0;
	}
	*stringp = end;
	return start;
}

// parseword
char *parseword(char **stringp)
{
	char *start, *end, *s = *stringp;

	while (*s && isspace(*s)) s++;

	if ( *s == 0) {       // already end of string
  	*stringp = s;
	  return s;
	}

	start = end = s;

	while (*end && !isspace(*end)) end++;
	if (*end) *end++ = 0;
	*stringp = end;
	return start;
}

// parsefloat
float parsefloat(char **stringp, float def)
{
	char *s = parseword(stringp);
	return *s ? atof(s) : def;
}

// parseint
int parseint(char **stringp, int def)
{
	char *s = parseword(stringp);
	return *s ? atoi(s) : def;
}

/************************************************************************************
 * IqeB_ImportFile()
 *
 * Load file of any known format.
 *
 * return:   NULL  error on loading
 *           else  pointer to loaded model data
 */

struct IQE_model *IqeB_ImportFile( char *pFilename)
{
  IQE_model *pModelTemp;
  char *pFileType;

  pModelTemp = NULL;            // preset no model loaded

  if( pFilename != NULL) {

    pFileType = strrchr( pFilename, '.');        // get pointer to file type

    if( !strcasecmp( pFileType, ".iqe")) {       // is .iqe file

  		pModelTemp = IqeB_ImportFileIQE( pFilename);

    } else if( !strcasecmp( pFileType, ".iqm")) { // is .iqm file

  		pModelTemp = IqeB_ImportFileIQM( pFilename);

    } else {  // must be supported by assimb

  		pModelTemp = IqeB_ImportAssimbLib( pFilename);
    }
  }

  return( pModelTemp);
}

/************************************************************************************
 * IqeB_ImportFileExtensions()
 *
 * Fills a string with the file extensions of any known image format.
 *
 */

void IqeB_ImportFileExtensions( char *pExtensionList, int SizeString)
{

  pExtensionList[ 0] = '\0'; // set string to empty

  // let assimb fill the list first

  IqeB_ImportAssimbLibExtensions( pExtensionList, SizeString);

  // append others

  if( pExtensionList[ 0] == '\0') { // still empty

    strcpy( pExtensionList, "iqe,iqm");  // simply copy

  } else {

    if( (int)strlen( pExtensionList) + 6 < SizeString) {

      strcat( pExtensionList, ",iqe");
    }

    if( (int)strlen( pExtensionList) + 6 < SizeString) {

      strcat( pExtensionList, ",iqm");
    }
  }
}

// ---------------------------------------------------------------------------------
// File system utilities
// ---------------------------------------------------------------------------------

/************************************************************************************
 * IqeB_FileNormalizePathChars()
 *
 * Normalize the path delimiter characters to
 *   \  for WIN32
 *   /  for other operting systems.
 */

void IqeB_FileNormalizePathChars( char *pPath)
{
  char *p;

  // Normalize path slasches
  p = pPath;
  while( *p != '\0') {

#ifdef _WIN32
    if( *p == '/') *p = '\\';
#else
    if( *p == '\\') *p = '/';
#endif
    p++;
  }
}

/****************************** End Of File ******************************/
