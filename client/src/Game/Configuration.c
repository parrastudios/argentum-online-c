/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Configuration.h>
#include <Game/Language.h>
#include <System/IOHelper.h>
#include <System/Error.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct ConfigData AppConfig;			///< Application config

////////////////////////////////////////////////////////////
/// Basic error handler
////////////////////////////////////////////////////////////
uint32 ConfigErrorValue(uint32 Index, uint32 Result)
{
	MessageError("Config", "An internal error ocurred : ID(%d) VALUE(%d).", Index, Result);

	return CONFIG_ERROR_INVALID;
}

////////////////////////////////////////////////////////////
/// Configuration initializer
////////////////////////////////////////////////////////////
void ConfigInitialize()
{
	FILE * File;

	if (fopen(&File, CONFIG_DEFAULT_PATH, "r"))
	{
		// Get data
		fread(&AppConfig, sizeof(struct ConfigData), 1, File);

		// Close it
		fclose(File);

		// Check the integrity
		if (ConfigCheckIntegrity(&AppConfig))
			return;
	}

	// Save the default config
	ConfigSave();
}

////////////////////////////////////////////////////////////
/// Configuration default seter
////////////////////////////////////////////////////////////
void ConfigSetDefault(struct ConfigData * Config)
{
	// Set all values as a default
	Config->Language		= LANGUAGE_ENGLISH;
	Config->WindowStyle		= Fullscreen;
	Config->BitsPerPixel	= 32;
	Config->Vsync			= true;
	Config->Name[0]			= NULLCHAR;
	Config->Password[0]		= NULLCHAR;
	Config->GuiTemplate		= 0;
}

////////////////////////////////////////////////////////////
/// Configuration save data
////////////////////////////////////////////////////////////
bool ConfigSave()
{
	FILE * File;

	if (!fopen(&File, CONFIG_DEFAULT_PATH, "w"))
	{
		MessageError("ConfigDestroy", "An internal error ocurred : '%s' not found or corrupted.", CONFIG_DEFAULT_PATH);
		return false;
	}

	// Check if the config is correct
	if (!ConfigCheckIntegrity(&AppConfig))
		ConfigSetDefault(&AppConfig);

	// Put data
	fwrite(&AppConfig, sizeof(struct ConfigData), 1, File);

	// Close it
	fclose(File);

	return true;
}

////////////////////////////////////////////////////////////
/// Check if the values are correct
////////////////////////////////////////////////////////////
uint32 ConfigCheckIntegrity(struct ConfigData * Config)
{
	if (Config->Language > LANGUAGE_SIZE)
	{
		return ConfigErrorValue(CONFIG_FLAG_LANGUAGE, Config->Language);
	}
	else if (Config->WindowStyle > Fullscreen)
	{
		return ConfigErrorValue(CONFIG_FLAG_WINDOW_MODE, Config->WindowStyle);
	}
	else if (Config->BitsPerPixel > 32)
	{
		return ConfigErrorValue(CONFIG_FLAG_WINDOW_BPP, Config->BitsPerPixel);
	}
	else if (Config->Vsync > 1)
	{
		return ConfigErrorValue(CONFIG_FLAG_WINDOW_VSYNC, Config->Vsync);
	}

	return true;
}

////////////////////////////////////////////////////////////
/// Configuration getter
////////////////////////////////////////////////////////////
struct ConfigData * ConfigGet()
{
	return &AppConfig;
}

////////////////////////////////////////////////////////////
/// Get values of config from an id
////////////////////////////////////////////////////////////
char * ConfigToString(char * Id)
{
	if		(strncmp(Id, "USER", 4) == 0)		return ConfigGet()->Name;
	else if (strncmp(Id, "PASSWORD", 8) == 0)	return ConfigGet()->Password;
	// else if (strncmp(ID, "LANG", 4) == 0)		return 
	// else if (strncmp(ID, "WSTYLE", 6) == 0)		return

	return NULL;
}