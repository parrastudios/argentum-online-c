/*
***********************************************************************************
 * IqeBrowser.h

 01.11.2014 RR: * First edtion to tis file
                * Moved structure defines to here so the model can be
                  use for the pinocchio api interface.

*/

/************************************************************************************
 * Some version defines
 */

#define WIN_PROG_NAME      "IqeBrowser"
#define WIN_PROG_VERSION   "V2.10  June 7 2015"
#define WIN_DEFAULT_TITLE  "IQE/IQM model browser and compiler"

#define IQE_CREDITS_TEXT                       \
   "IQM Developer Kit\n"                       \
   "  https://github.com/lsalzman/iqm\n"       \
   "3d Asset Tools\n"                          \
   "  https://github.com/ccxvii/asstools\n"    \
   "Open Asset Import Library\n"               \
   "  //assimp.sourceforge.net/index.html\n"   \
   "3d Asset Tools\n"                          \
   "  https://github.com/ccxvii/asstools\n"    \
   "FLTK, Fast Light Toolkit\n"                \
   "  http://www.fltk.org/index.php\n"         \
   "Pinocchio library\n"                       \
   "  http://www.mit.edu/~ibaran/autorig/\n"   \
   "VCG Library\n"                             \
   "  http://vcg.isti.cnr.it/vcglib/"          \

/************************************************************************************
 * Some global defines
 */

#define MAX_FILENAME_LEN 2048   // length of filenames with path

//x/#define IQE_FEATURE_ANIM_OLY_NORMALIZED 1 // define this --> animation edtior works only if the skeleton is normalized

/************************************************************************************
 * Some vector and matrix math.
 */

typedef float vec3[3];
typedef float vec4[4];
typedef float mat4[16];

#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define CLAMP(x,a,b) MIN(MAX(x,a),b)

/************************************************************************************
 * Model.
 */

#define MAXBONE 256              // Max number of bones supported
#define MAX_ANIM_FRAMES 1024     // Max number of animation frames supported

struct IQE_model {
	struct skel *skel;             // skeleton
	struct mesh *mesh;             // mesh

	// anim data
	int anim_poses;                // number of poses in an animation frame
	int anim_count;                // number of animations used
	int anim_cap;                  // number of animations allocated
	struct anim **anim_data;       // list of animations
};

#define KEYFLAG_LOC_0      0x0001 // KeyFlags bit set for location[ 0]
#define KEYFLAG_LOC_1      0x0002 // KeyFlags bit set for location[ 1]
#define KEYFLAG_LOC_2      0x0004 // KeyFlags bit set for location[ 2]
#define KEYFLAG_LOC_ALL    0x0007 // KeyFlags bit set for all locations

#define KEYFLAG_ANG_0      0x0010 // KeyFlags bit set for angles[ 0]
#define KEYFLAG_ANG_1      0x0020 // KeyFlags bit set for angles[ 1]
#define KEYFLAG_ANG_2      0x0040 // KeyFlags bit set for angles[ 2]
#define KEYFLAG_ANG_ALL    0x0070 // KeyFlags bit set for all angles

#define KEYFLAG_SCALE_0    0x0100 // KeyFlags bit set for scale[ 0]
#define KEYFLAG_SCALE_1    0x0200 // KeyFlags bit set for scale[ 1]
#define KEYFLAG_SCALE_2    0x0400 // KeyFlags bit set for scale[ 2]
#define KEYFLAG_SCALE_ALL  0x0700 // KeyFlags bit set for all scales

#define KEYFLAGS_ALL  (KEYFLAG_LOC_ALL | KEYFLAG_ANG_ALL | KEYFLAG_SCALE_ALL) // All bits set

struct pose {
  // pose information
	vec3 location;                 // location
	vec4 rotation;                 // rotation as quaterion
	vec3 scale;                    // scale
	// Used for skeleteon and/or animation editor
	vec3 angles;                   // rotation as angles [degrees], is converted 'rotation' quaterion
	int  KeyFlags;                 // Flag bit for key pose
};

struct S_skel_joint {
  // data of the joint
	int parent;           // index of parent joint
	char *name;           // name of joint
  // Temp, dolors show influence of blendweights to skin.
	vec3 TempBoneColor;   // temp use for visualize blend weights
	// Temp selection of joints
	int isSelected;       // Used by 'mouse edit mode' IQE_GUI_MOUSE_MODE_JOINT_SEL
	// Linking and hierarchy
	int nChilds;          // number of childs
	int iChildFirst;      // index of first child
	int iChildLast;       // index of last child
	int iChildLink;       // Link childs with same parent
	int nParentsChain;    // Size of parent chain
	int nIndent;          // How much to indent (for tree diagram)
	int iSymmetric;       // If >= 0, index of symmetric joint.
};

struct skel {
	int joint_count;                 // number of joints
  int IsNormalized;                // != 0 if skeleton is normalized (no rotaion and no zesize)
	struct S_skel_joint j[ MAXBONE]; // joint data
	struct pose pose[ MAXBONE];      // transformation of joint
};

struct mesh {

  // Vertex things
	int vertex_count;
	float *position, *normal, *texcoord, *color;
	float *blendweight;
	int *blendindex;

  // Triangles
	int element_count;
	int *element;

  // Model parts
	int part_count;
	struct part *part;

  // inter use only
	float *aposition, *anormal;

	mat4 abs_bind_matrix[MAXBONE];
	mat4 inv_bind_matrix[MAXBONE];

  // Four values per vertex.
  // if != NULL, colors show influence of blendweights to skin.
	float *blendColors;
	int   blendColorsRebuild;  // If != 0, rebuild blendColors
};

struct part {
  char *pMeshName;           // name of mesh
  char *pMaterialStr;        // material string
  char *pMaterialTags;       // material tags
	int first, count;          // triangles for this partial mesh
	// data internal for IQM fiewer
  char *pMaterialBasedir;    // directory to use for material lookup
  char *pMaterialLoadedFile; // If != NULL, loaded this image file for material display
	int material;              // openGL material index, -1 if no index assigned
};

struct anim {
	char *name;                // Name of animation
	int len, cap;              // Number of frames, allocated # for data
	float framerate;           // Framerate
	int looped;                // Looped animation
	struct pose **data;        // Table of pointers to frame poses
	// Used for skeleteon and/or animation editor
	int  FrameMarked;          // If >= 0, This frame is marked
};

//
// IqeB_Display.cpp
//

// model container

#define MODEL_CONTAINER_MAX  128    // max number of models in model container

typedef struct {
  char   DispName[ MAX_FILENAME_LEN];             // Display name
  char   FileName[ MAX_FILENAME_LEN];             // file selected model, file name
  char   ModelFullName[ MAX_FILENAME_LEN];        // file selected model, path and file name
  int    part_count_before_add;                   // number of meshes before any add of mesh or skeleton
  IQE_model *pModel;                              // parsed model data
} T_ModelContainer;

extern T_ModelContainer ModelContainer[ MODEL_CONTAINER_MAX]; // the model container
extern int    ModelContainer_n;                               // number of models in container

// Clipboard
extern struct IQE_model *pClipBoardModel;              // Clipboard model
extern char   ClipBoardModelName[ MAX_FILENAME_LEN];     // drawing model, file name
extern char   ClipBoardModelFullName[ MAX_FILENAME_LEN]; // drawing model, path and file name

// draw this model
extern struct IQE_model *pDrawModel;                   // drawing this model
extern struct anim *pDrawModelCurAnim;                 // current selected animation
extern int    DrawModelCurframe;                       // Draw this frame of current model
extern char   DrawModelSelName[ MAX_FILENAME_LEN];     // drawing model, file name
extern char   DrawModelSelFullName[ MAX_FILENAME_LEN]; // drawing model, path and file name

// OpenGL window view mode

#define IQE_VIEWMODE_FIRST     0  // First view mode value
#define IQE_VIEWMODE_XY_TOP    0  // X/Y plane
#define IQE_VIEWMODE_XY_BOTTOM 1  // X/Y plane
#define IQE_VIEWMODE_YZ_FRONT  2  // Y/Z plane
#define IQE_VIEWMODE_YZ_BACK   3  // Y/Z plane
#define IQE_VIEWMODE_ZX_RIGHT  4  // Z/X plane
#define IQE_VIEWMODE_ZX_LEFT   5  // Z/X plane
#define IQE_VIEWMODE_ORTHO     6  // 3D orthogonal
#define IQE_VIEWMODE_PERSP     7  // 3D perspective
#define IQE_VIEWMODE_LAST      8  // Last view mode value

#define IQE_VIEWMODE_3D   IQE_VIEWMODE_ORTHO  // >= this value, is a 3d view mode

extern int IqeB_DispStyle_ViewMode;             // plane/orthogonal/perspective camera

#define IQE_VIEW_PORT_NUM_1       0  // 1 view port
#define IQE_VIEW_PORT_NUM_4       1  // 4 view ports

extern int IqeB_DispStyle_NumViewPorts;         // Number of view ports

extern int IqeB_DispStyle_MeshPartSelected;     // if >= 0, selected mesh part
extern int IqeB_DispStyle_JointSelected;        // if >= 0, selected joint

// Mouse edit mode

#define IQE_GUI_MOUSE_MODE_NONE           0   // Mouse mode: none
#define IQE_GUI_MOUSE_MODE_SKEL_EDIT      1   // Mouse mode: Skeleton edit
#define IQE_GUI_MOUSE_MODE_JOINT_SEL      2   // Mouse mode: Skeleton joint select
#define IQE_GUI_MOUSE_MODE_ANIM_EDIT      3   // Mouse mode: Animation edit

extern int IqeB_DispStyle_MouseMode;          // Mouse edit mode

// OpenGL window display state variables

extern int IqeB_DispStyle_doplane;              // draw gound plane
extern int IqeB_DispStyle_dowire;               // draw model wireframe
extern int IqeB_DispStyle_donormals;            // draw normals
extern int IqeB_DispStyle_dotexture;            // draw model with textures
extern int IqeB_DispStyle_dobackface;           // draw model with NO backface culling
extern int IqeB_DispStyle_doskeleton;           // draw skeleton
extern int IqeB_DispStyle_doJointNames;         // draw the joint names of a skeleton
extern int IqeB_DispStyle_doBlendWeightColors;  // draw model with blend weight colors
extern int IqeB_DispStyle_doAnimations;         // do animations
extern int IqeB_DispStyle_doplay;               // animation play/pause
extern int IqeB_DispStyle_doPlayOnce;           // animation 'play once' only

// OpenGL window colors, use FLGK colors here

extern unsigned int IqeB_DispCol_BACKGROUND;   // Background
extern unsigned int IqeB_DispCol_VERSION;      // Version text
extern unsigned int IqeB_DispCol_INFOS;        // Infos
extern unsigned int IqeB_DispCol_NORMALS;      // Normals

extern unsigned int IqeB_DispCol_GROUND_PLANE; // Ground plane grid color
extern unsigned int IqeB_DispCol_AXIS;         // Ground plane axis color
extern unsigned int IqeB_DispCol_BOX;          // Reference box
extern unsigned int IqeB_DispCol_BOX_GROUND;   // Reference box ground level

extern unsigned int IqeB_DispCol_SKELETON;     // Skeleton
extern unsigned int IqeB_DispCol_JOINTS;       // Joints
extern unsigned int IqeB_DispCol_JOINTS_SEL;   // Selected joints
extern unsigned int IqeB_DispCol_JOINT_NAMES;  // Joint names

// Messge to be drawn in OpenGL window at bottom left corner

#define DRAW_MSG_COL_ERR     1.0, 0.2, 0.2     // red, error
#define DRAW_MSG_COL_OK      0.1, 0.8, 0.1     // green, OK
#define DRAW_MSG_COL_WARNING 0.9, 0.0, 0.9     // mangenta, warning

void IqeB_DispMessage( float r, float g, float b, char *pText1,  char *pText2, char *pText3);

// more functions

void IqeB_DispAnimateModel( struct IQE_model *model, struct anim *anim, int frame); // animade model for display
unsigned int IqeB_Disp_GetTickCount();  // Returns time im ms since system start
void IqeB_DispSetViewportSize( int w, int h);
void IqeB_DispMouseEventAction( int event);

void IqeB_DispSetOpenGLColor4f( unsigned int ColorFLTK, float alpha);
void IqeB_DispSetOpenGLClearColor( unsigned int ColorFLTK, float alpha);

int  IqeB_DispTestRedraw( void);
void IqeB_DispDisplay( unsigned int BackgroundColor);
void IqeB_DispViewReset( void);
void IqeB_DispViewAdaptFromModel( struct IQE_model *pModel);
void IqeB_DispPrepareNewModel( char *pFilename, IQE_model *pLoadedModel);
void IqeB_DispPrepareContainerModel( T_ModelContainer *pModelContainer);
int  IqeB_DispModelGetInfo( vec3 ObjSize, vec3 ObjCenter, float *pGridStep = 0, int *pCoordAfterpointDigits = 0);

//
// IqeB_Export options
//

#define IQE_FILE_MAGIC "# Inter-Quake Export" // Header of IQE file

#define IQE_EXPORT_OPTION_COPY_TEX      0x0001    // Also copy image files (textures)
#define IQE_EXPORT_OPTION_TANGENT       0x0002    // Generate texture tangent data
#define IQE_EXPORT_OPTION_ADJACENCY     0x0004    // Generate triangle adjacency data
#define IQE_EXPORT_OPTION_MODEL         0x0008    // Export meshes, joints and base frame
#define IQE_EXPORT_OPTION_POSES         0x0010    // Export poses

#define IQE_EXPORT_OPTION_DEFAULT  (IQE_EXPORT_OPTION_COPY_TEX | \
                                    IQE_EXPORT_OPTION_TANGENT | \
                                    IQE_EXPORT_OPTION_MODEL | \
                                    IQE_EXPORT_OPTION_POSES)   // Defautl export settings

//
// IqeB_Export_File_IQM_IQE.cpp
//

#define IQE_EXPORT_FILE_TYPE_IQM  0    // Export file of type IQM
#define IQE_EXPORT_FILE_TYPE_IQE  1    // Export file of type IQE

int IqeB_Export_File_IQM_IQE( IQE_model *pModel, char *filename, char *pDstDir,
                              int FileType, int ExportOptionFlags);

//
// IqeB_GUI_Main.cpp
//

#define IQE_GUI_TOOLS_STD_WITDH 240   // Std with of tools windows

#define IQE_GUI_NO_WINPOS_X   -4711   // if this value, no preference window postion set
#define IQE_GUI_NO_WINPOS_Y   -4711   //

// preferences handling

#define PREF_T_INT       1     // Settings is of type int
#define PREF_T_FLOAT     2     // Settings is of type float
#define PREF_T_STRING    3     // Settings is a string

//x/#define PREF_TO_STR( a)  #a    // enclose arguments in strings

// Define a preference setting

typedef struct {
  int  Type;                  // Type of setting
  const char *pName;          // Point to name of setting
  const char *pDefaultValue;  // String with preset value
  void *pValue;               // Pointer to variable holding value
  int   SizeOfString;         // Length string for PREF_T_STRING

  // intern use
	union {                     // Default value
		int   DVal_Int;
		float DVal_Float;
	};
	union {                     // last value
		int   LVal_Int;
		float LVal_Float;
	};
} T_GUI_PreferenceEntry;

// Helper class IqeB_PreferencesGroup
// Automatic add preference settings at startup of the program.

class IqeB_PreferencesGroup {

public:

  IqeB_PreferencesGroup( const char *pGroupName, T_GUI_PreferenceEntry *pSettings, int nSettings,
                         void **ppMyToolWin, int *pWinPosX, int *pWinPosY);
  ~IqeB_PreferencesGroup();
};

// ...

void IqeB_PreferencesGetFromFile();
void IqeB_PreferencesUpdateChanges();

// ...

void IqeB_GUI_OpenGLUpdateGUISettingVariable( int *pValue, int NewValue); // Update variable and GUI element
void IqeB_GUI_OpenGLIdleAction( void *);       // flag the opengl window to redraw
void IqeB_GUI_SetWindowTitle( char *pTitle);
void IqeB_GUI_WidgetActivate( void *wArg, int ActivateIt); // Set widget activated/inactive
void IqeB_GUI_RegisterCallback( void **ppMyToolWin,       // Pointer to hold pointer to tool win data
                                void (*pUpdateGUI)(int),  // Pointer to call from IqeB_GUI_UpdateWidgets() to update the GUI
                                int UpdateFlags,          // Flags for calling pUpdateGUI
                                int MouseMode = IQE_GUI_MOUSE_MODE_NONE,            // if != 0, needed edit mode for this ...
                                void (*pCloseToolWin)(void *, void *) = 0); // Close this toolwindow
void IqeB_GUI_CloseToolWindow( void **ppMyToolWin);
int  IqeB_GUI_ToolWindowTestFreeEditMode( int MouseMode);  // Test to have a free edit mode

// Update GUI

#define IQE_GUI_UPDATE_DEFAULT             0   // Update GUI things
#define IQE_GUI_UPDATE_MODEL_CHANGE   0x0001   // The model has changed, meshes, animation, ...
#define IQE_GUI_UPDATE_JOINT_SELECTED 0x0002   // A joint selection has changed, see variable 'IqeB_DispStyle_JointSelected'
#define IQE_GUI_UPDATE_JOINT_CHANGED  0x0004   // Joint data of selected joint has changed, see variable 'IqeB_DispStyle_JointSelected'
#define IQE_GUI_UPDATE_ANIMATION      0x0008   // Anything with animation has changed

void IqeB_GUI_UpdateWidgets( int UpdateFlags = IQE_GUI_UPDATE_DEFAULT); // Update GUI things

//
// IqeB_GUI_ToolsAnimEdit.cpp
//

void IqeB_GUI_ToolsAnimEditWin( int xLeft, int xRight, int yTop, int yBotton); // Open dialog

// What pose part to edit

#define IQE_ANIM_EDIT_POSE_PART_LOCATION 0  // Pose part to edit: location
#define IQE_ANIM_EDIT_POSE_PART_ANGLES   1  // Pose part to edit: angles
#define IQE_ANIM_EDIT_POSE_PART_SCALE    2  // Pose part to edit: scale

extern int IqeB_ToolsAnimEdit_PosePart;     // What pose part to edit

extern int IqeB_ToolsAnimEdit_PoseScaleAll; // For pose scale change, change all scales

//
// IqeB_GUI_ToolsAnimKinect.cpp
//

void IqeB_GUI_ToolsAnimKinectWin( int xLeft, int xRight, int yTop, int yBotton); // Open dialog

//
// IqeB_GUI_ToolsAnimManager.cpp
//

void IqeB_GUI_ToolsAnimManagerWin( int xLeft, int xRight, int yTop, int yBotton); // Open dialog

//
// IqeB_GUI_ToolsDispBox.cpp
//

// show box (as reference object with definded height)

extern int   IqeB_ToolsDisp_BoxEnable;   // Shows a box in the display area
extern float IqeB_ToolsDisp_BoxHeight1;  // Height 1 of box
extern float IqeB_ToolsDisp_BoxHeight2;  // Height 2 of box
extern float IqeB_ToolsDisp_BoxWidth;    // Width of box

void IqeB_GUI_ToolsDispBoxWin( int xLeft, int xRight, int yTop, int yBotton); // Open dialog

//
// IqeB_GUI_ToolsDispColors.cpp
//

void IqeB_GUI_ToolsDispColorsWin( int xLeft, int xRight, int yTop, int yBotton); // Open dialog

//
// IqeB_GUI_ToolsMeshPartsWin.cpp
//

void IqeB_GUI_ToolsMeshPartsWin( int xLeft, int xRight, int yTop, int yBotton); // Open dialog

//
// IqeB_GUI_ToolsMeshTransform.cpp
//

void IqeB_GUI_ToolsMeshTransformWin( int xLeft, int xRight, int yTop, int yBotton); // Open dialog

//
// IqeB_GUI_ToolsMeshWorkbench.cpp
//

void IqeB_GUI_ToolsMeshWorkbenchWin( int xLeft, int xRight, int yTop, int yBotton); // Open dialog

//
// IqeB_GUI_ToolsSkelEdit.cpp
//

extern int IqeB_ToolsSkelEdit_MoveSymmetricJoints;  // If enabled, move symetric joints

void IqeB_GUI_ToolsSkelEditWin( int xLeft, int xRight, int yTop, int yBotton); // Open dialog

//
// IqeB_GUI_ToolsSkelNew.cpp
//

void IqeB_GUI_ToolsSkelNewWin( int xLeft, int xRight, int yTop, int yBotton); // Open dialog

//
// IqeB_GUI_ToolsSkinBind.cpp
//

void IqeB_GUI_ToolsSkinBindWin( int xLeft, int xRight, int yTop, int yBotton); // Open dialog

void IqeB_GUI_ToolsSkinBindDo( int UseSmooth);

//
// IqeB_Util_Math.cpp
//

// Byte order functions

void Swap_Init( void);
short	LittleShort(short l);
int		LittleLong (int l);
float	LittleFloat (float l);

// vector and matrix

#define DEG2RAD( a ) (( a ) * (M_PI / 180.0F))
#define RAD2DEG( a ) (( a ) * (180.0f / M_PI))

void mat_identity( mat4 m);
void mat_copy(mat4 p, const mat4 m);
void mat_mul(mat4 m, const mat4 a, const mat4 b);
void mat_invert(mat4 out, const mat4 m);
void mat_rotate( mat4 a, float angle, float x, float y, float z);
void mat_from_pose(mat4 m, const vec3 t, const vec4 q, const vec3 s);

void vec_set( vec3 p, float a0, float a1, float a2);
float vec_dist2(const vec3 a, const vec3 b);
float vec_dist(const vec3 a, const vec3 b);
float vec_DotProduct(const vec3 a, const vec3 b);
void vec_scale(vec3 p, const vec3 v, float s);
void vec_scale_add_to(vec3 p, const vec3 v, float s);
void vec_scale_to(vec3 p, float s);
void vec_add(vec3 p, const vec3 a, const vec3 b);
void vec_add_to(vec3 p, const vec3 a);
void vec_mul(vec3 p, const vec3 a, const vec3 b);
void vec_mul_to(vec3 p, const vec3 a);
void vec_sub(vec3 p, const vec3 a, const vec3 b);
void vec_sub_from(vec3 p, const vec3 a);
void vec_cross(vec3 p, const vec3 a, const vec3 b);
float vec_normalize( vec3 a);
void vec_copy(vec3 p, const vec3 a);
void vec_negate(vec3 p);
void vec_minmax( vec3 min, vec3 max, const vec3 a);
int vec_isZeroLength( const vec3 a);

void quat_copy( vec4 q1, const vec4 q2);
void quat_set( vec4 q, float x, float y, float z, float w);
void quat_from_axis_angle( vec4 q, const vec3 axis, const float angle);
void quat_from_two_normalized_vectors( vec4 q, const vec3 u, const vec3 v);
void quat_set_identity( vec4 q);
void quat_conjugate( vec4 q);
float quat_normalize( vec4 q);
void quat_inverse( vec4 q);
void quat_flip( vec4 q);
void quat_from_rotation( vec4 q, const vec3 r);
void quat_to_rotation( const vec4 q, vec3 r);
void quat_transfrom_vec3( const vec4 q, vec3 p, const vec3 a);
void quat_mul( vec4 q, const vec4 a, const vec4 b);

void mat_vec_mul(vec3 p, const mat4 m, const vec3 v);
void mat_vec_mul_n(vec3 p, const mat4 m, const vec3 v);
void calc_mul_matrix(mat4 *skin_matrix, mat4 *abs_pose_matrix, mat4 *inv_bind_matrix, int count);
void calc_inv_matrix(mat4 *inv_bind_matrix, mat4 *abs_bind_matrix, int count);
void calc_abs_matrix(mat4 *abs_pose_matrix, mat4 *pose_matrix, struct skel *pSkel);
void calc_matrix_from_pose(mat4 *pose_matrix, struct pose *pose, int count);

#define Vector4Inverse( v )			( ( v )[0] = -( v )[0], ( v )[1] = -( v )[1], ( v )[2] = -( v )[2], ( v )[3] = -( v )[3] )
float Vector4Normalize( float v[] );

float PointDistanceFromLine( const vec3 LineP, const vec3 LineV, const vec3 Point, vec3 PointOnLine);
float PointDistNearestToLineBasePoint( const vec3 LineP, const vec3 LineV, const vec3 Point,
                                       float *pBasePointOnLine);

//
// IqeB_Import_Utils.cpp
//

struct floatarray {
	int len, cap;
	float *data;
};

struct intarray {
	int len, cap;
	int *data;
};

struct partarray {
	int len, cap;
	struct part *data;
};

// global scratch buffers used on importing files
extern struct floatarray IqeB_ImportArray_position;
extern struct floatarray IqeB_ImportArray_normal;
extern struct floatarray IqeB_ImportArray_texcoord;
extern struct floatarray IqeB_ImportArray_color;
extern struct intarray   IqeB_ImportArray_blendindex;
extern struct floatarray IqeB_ImportArray_blendweight;
extern struct intarray   IqeB_ImportArray_element;
extern struct partarray  IqeB_ImportArray_partbuf;

void IqeB_ImportArraysReset();

void *dupArray( void *data, int count, int size);
void allocArray( void **ppMemData, int count, int size);

void pushfloat(struct floatarray *a, float v);
void pushint(struct intarray *a, int v);
void pushpart( struct partarray *a, int first, int last, char *pMeshName,
               char *pMaterialString, char *pMaterialBasedir);
anim *pushanim(struct IQE_model *pModel, char *name);
struct pose *pushframe(struct anim *a, int bone_count);
void addposition(float x, float y, float z);
void addnormal(float x, float y, float z);
void addtexcoord(float u, float v);
void addcolor(float x, float y, float z, float w);
void addblend(int a, int b, int c, int d, float x, float y, float z, float w);
void addtriangle(int a, int b, int c);
void addtriangleFlipWinding(int a, int b, int c);

char *parsestring(char **stringp);
char *parseword(char **stringp);
float parsefloat(char **stringp, float def);
int parseint(char **stringp, int def);

// File system utilities

void IqeB_FileNormalizePathChars( char *pPath);

//
// IqeB_Import_FileXXX.cpp  things
//

// load file of any known format
struct IQE_model *IqeB_ImportFile( char *filename);
void IqeB_ImportFileExtensions( char *pExtensionList, int SizeString);

// load file of specific format
struct IQE_model *IqeB_ImportAssimbLib( char *filename);
void IqeB_ImportAssimbLibExtensions( char *pExtensionList, int SizeString);

struct IQE_model *IqeB_ImportFileIQE( char *filename);
struct IQE_model *IqeB_ImportFileIQM( char *filename);

//
// IqeB_Texture_Utils.cpp
//
void IqeB_TextureInitChecker(void);
unsigned int IqeB_TextureLoadToOpenGL( char *filename);
int  IqeB_TextureLoadMaterial( struct part *pMeshPart);
int  IqeB_TextureCopyLoadedImageFiles( IQE_model *pModel, char *pDestDir);

//
// IqeB_Tools_Animation.cpp
//

// Animation action

#define IQE_ANIMATE_ACTION_NONE         0  // No animation action
#define IQE_ANIMATE_ACTION_UPDATE       1  // Update animation calculation after size changes (or such things ...)
// Frame ...
#define IQE_ANIMATE_ACTION_FRAME_PREV   2  // Previous animation frame
#define IQE_ANIMATE_ACTION_FRAME_NEXT   3  // Next animation frame
#define IQE_ANIMATE_ACTION_FRAME_FIRST  4  // First animation frame
#define IQE_ANIMATE_ACTION_FRAME_LAST   5  // Last animation frame
#define IQE_ANIMATE_ACTION_FRAME_SET    6  // Set animation frame (use argument)
// Animations ...
#define IQE_ANIMATE_ACTION_PREV         7  // Previous animation
#define IQE_ANIMATE_ACTION_NEXT         8  // Next animation
#define IQE_ANIMATE_ACTION_FIRST        9  // First animation
#define IQE_ANIMATE_ACTION_LAST        10  // Last animation
#define IQE_ANIMATE_ACTION_SET         11  // Set animation (use argument)
// Management ...
#define IQE_ANIMATE_ACTION_DELETE_ALL  12  // Delete all animations
#define IQE_ANIMATE_ACTION_DELETE_ARG  13  // Delete animation named in argument (use argument)
#define IQE_ANIMATE_ACTION_NEW_ARG     14  // Create new animation (after named argument nr.) (use argument)
#define IQE_ANIMATE_ACTION_SHIFT_UP    15  // Shift named animation up (use argument)
#define IQE_ANIMATE_ACTION_SHIFT_DOWN  16  // Shift named animation down (use argument)
#define IQE_ANIMATE_ACTION_NUM_FRAMES  17  // Change number of frames of current animation (use argument)

void IqeB_AnimAction( int ActionCode, int ActionArgument);

int  IqeB_AnimFindByName( IQE_model *pModel, char *pName);

void IqeB_AnimRotToAnglesFrame( struct pose *pPose, int nPoses);
void IqeB_AnimRotToAnglesAnimation( struct anim *pAnim, int nPoses);
void IqeB_AnimRotToAnglesModel( IQE_model *pModel);

void IqeB_AnimAnglesToRotPose( struct pose *pPose);

void IqeB_AnimKeyFlagsPresetAnimation( struct anim *pAnim, int nPoses);
void IqeB_AnimKeyFlagsPresetModel( IQE_model *pModel);
void IqeB_AnimKeyFlagsRecalcAnimation( struct anim *pAnim, int nPoses);
void IqeB_AnimKeyFlagsChangeAnimation( struct anim *pAnim, int nPoses, int iFrame, int iPose, int KeyFlagMask, int KeyFlagValue);

int  IqeB_AnimNormalizeModel( IQE_model *pModel);

void IqeB_AnimPrepareAfterLoad( IQE_model *pModel);

//
// IqeB_Tools_Mesh.cpp
//

void IqeB_MeshFreeAllData( struct mesh *mesh);
void IqeB_MeshComputeVertexNormals( struct mesh *mesh);
void IqeB_MeshSmooth( struct mesh *mesh, float SmootValue, float KeepPointedness, float PointednessFullSmooth);
int IqeB_MeshMeasureSize( struct mesh *pMesh, float center[3], float size[3]);

//
// IqeB_Tools_Model.cpp
//

struct IQE_model *IqeB_ModelCreateEmpty();

// --- IqeB_ModelFreeData ---

#define IQE_MODEL_FREE_ALL            0    // Free all
#define IQE_MODEL_FREE_SKELETON  0x0001    // Free skeleton (and animation)
#define IQE_MODEL_FREE_ANIMATION 0x0002    // Free animation data
#define IQE_MODEL_FREE_MESH      0x0004    // Free mesh data

void IqeB_ModelFreeData( struct IQE_model **ppModel, int WhatToFree);

// --- IqeB_ModelFreeData ---

#define IQE_MODEL_COPY_NEW_ALL    0   // copy everything, distination model is allocated
#define IQE_MODEL_COPY_SKELETON   1   // copy skeleton, distination must be allocated and have no skeleton
#define IQE_MODEL_COPY_MESH       2   // copy skeleton, distination must be allocated and have no mesh, vertexes, trigangle
#define IQE_MODEL_COPY_ANIMATION  4   // copy animation, distination must be allocated, append animation
#define IQE_MODEL_COPY_ALL_PARTS ( IQE_MODEL_COPY_SKELETON | IQE_MODEL_COPY_MESH | IQE_MODEL_COPY_ANIMATION) // copy all parts, distination must be allocated

void IqeB_ModelCopy( struct IQE_model **ppModelSrc, struct IQE_model **ppModelDst, int CopyFlags);

// ---

void IqeB_ModelMergeTo( struct IQE_model *pModelDst, struct IQE_model *pModelSrc);
void IqeB_ModelSkeletonMatrixCalc( struct IQE_model *pModel);
void IqeB_ModelBuildBlendColors( struct IQE_model *pModel);
int  IqeB_ModelSizeScale( struct IQE_model *pModel, float Scale);
int  IqeB_ModelTranslate( struct IQE_model *pModel, vec3 Translate);
int  IqeB_ModelRotate( struct IQE_model *pModel, vec4 QuatRotate);
int  IqeB_ModelSkelNormalize( struct IQE_model *pModel);

//
// IqeB_Tools_Pinocchio.cpp
//

int IqeB_ProcessForPinocchio( IQE_model *pModel,
                              char *pArg_Skeleton,
                              char *pArg_Rotation,
                              char *pArg_Scale,
                              bool Arg_noFit,
                              char *pErrorString
                            );

//
// IqeB_Tools_Skeleton.cpp
//

#define SKELTON_FILE_MAGIC "# Skeleton file" // Header of skeleton file

void IqeB_SkelUpdateHierarchy( struct skel *pSkel);
int IqeB_SkelMeasureSize( struct skel *pSkel, float center[3], float size[3]);
void IqeB_SkelReset( struct skel *pSkel);
int IqeB_SkelJointFindByName( struct skel *pSkel, const char *pName);
int IqeB_SkelJointAdd( struct skel *pSkel, const char *pNameJoint,
                       const char *pNameParent, vec3 location);
int IqeB_SkelLoadFromFile( struct skel *pSkel, const char *pFileName, char *pErrorString);
int IqeB_SkelWriteToFile( struct skel *pSkel, const char *pFileName, const char *pSkelName, char *pErrorString);
void IqeB_SkelAdjustSizeToMesh( struct skel *pSkel, struct mesh *pMesh);

// Action for IqeB_SkelJointManipulate()
#define IQE_SKEL_JOINT_ADD_CHILD  1  // Add child to name joint
#define IQE_SKEL_JOINT_ADD_ROOT   2  // Add new root joint
#define IQE_SKEL_JOINT_DEL_THIS   3  // Delete named joint
#define IQE_SKEL_JOINT_DEL_CHILDS 4  // Delete named joint and all it's childs

int IqeB_SkelJointManipulate( struct skel *pSkel, int Action, int iJointArg);

//
// IqeB_Tools_SkinBind.cpp
//

#define IQE_SKINBIND_ASSIGN_ALL_VERT        0 // assign to all vertices
#define IQE_SKINBIND_ASSIGN_SELECTED_MESH   1 // assign to vertices of selected mesh
#define IQE_SKINBIND_ASSIGN_UNASSIGNED_VERT 2 // assign to all unassigned verrtices

#define IQE_SKINBIND_JOINT_ALL              0 // assign from all joints
#define IQE_SKINBIND_JOINT_SELECTED         1 // assign from all selected joints
#define IQE_SKINBIND_JOINT_NOT_SEL          2 // assign from all not selected joints

#define IQE_SKINBIND_LOW_FILLFACTOR      0.04 // Voxel fill factor is low, the model may have holes.

void IqeB_SkinBindReset( IQE_model *pModel);
void IqeB_SkinBindClear( IQE_model *pModel, int AssignTypeMeshSelect);
void IqeB_SkinBindNearestJoint( IQE_model *pModel, int AssignType, int JointSel);
void IqeB_SkinBindNearestBone( IQE_model *pModel, int AssignType, int JointSel);
void IqeB_SkinBindToBones( IQE_model *pModel, int AssignType, int JointSel);
void IqeB_SkinBindHeadDiffusionSkin( IQE_model *pModel, int AssignType, int JointSel, int BindSmooth, char *pErrorString, float *pFillFactor);
void IqeB_SkinBindHeadDiffusionBones( IQE_model *pModel, int AssignType, int JointSel, int BindSmooth, char *pErrorString, float *pFillFactor);

//
// IqeB_Tools_Voxel.cpp
//

/************************************************************************************

  defines for voxel processing

*/

#define VOX_CEL_TYPE  unsigned char  // data typ of a voxel cell
#define VOX_CEL_TYPE_IS_INTEGER   1  // define this if the voxel cell tpye is an integer type
#define VOX_MAX_NORM_SIZE       150  // max value for IqeB_VoxelFromIqeModel() argument VoxelNormSize

#define VOX_CUPE_SET_LEN_THRESHOLD  (0.49 * 0.49)   // Cube set/test length threshold. Square it!

typedef struct {          // Voxel data envelope
  int Size[ 3];           // Voxel sizes of the three dimensions
  vec3 minbox, maxbox;    // Volume of input mesh point cloud
  float ScaleMeshToVoxel; // Scale coordinates from mesh to voxel space
  float ScaleVoxelToMesh; // Scale coordinates from voxel to mesh space
  float FillFactor;       // Fill factor = '# all voxels' / '# inside voxels'
  VOX_CEL_TYPE *pVoxels;  // point to voxel data
} T_VoxelModelData;

int IqeB_VoxelFromIqeModel( IQE_model *pModel,             // IN: IQE model
                            T_VoxelModelData *pVoxelData,  // OUT: Voxel mesh
                            int VoxelNormSize,             // IN: Normalize voxel data
                            char *pErrorString);

IQE_model *IqeB_VoxelToIqeMarchingCube( T_VoxelModelData *pVoxelData,   // IN: Voxel mesh
                                        char *pErrorString);

IQE_model *IqeB_VoxelToIqeCubes( T_VoxelModelData *pVoxelData,   // IN: Voxel mesh
                                 char *pErrorString);

void IqeB_VoxelCoordWorld2Voxel( T_VoxelModelData *pVoxelData, vec3 pOut, const vec3 pIn);

int IqeB_VoxelCoordVoxelFloat2Offset( T_VoxelModelData *pVoxelData,   // IN: Voxel mesh
                                      const vec3 pIn);                // IN: Coordinates in voxel space
int IqeB_VoxelCoordVoxelInt2Offset( T_VoxelModelData *pVoxelData,     // IN: Voxel mesh
                                    int ix, int iy, int iz);          // IN: Coordinates in voxel space

void IqeB_VoxelLineSet( T_VoxelModelData *pVoxelData,  // IN: Voxel mesh
                        vec3 p1, vec3 p2,              // IN: Points of line
                        int PointFillMask,             // IN: 0x01, 0x02, 0x03: bitmask witch point to write, p1 or p2
                        int FillValue);                // IN: Value to fill

int IqeB_VoxelLineTest( T_VoxelModelData *pVoxelData,  // IN: Voxel mesh
                        vec3 p1, vec3 p2,              // IN: Points of line in worldspace
                        int PointTestMask,             // IN: 0x01 set: Test first point, 0x02 set: test second point
                        int TestType,                  // IN: 0/1 : Test for any cell equal/unequal to 'TestValue' to be OK
                        int TestValue);                // IN: Value to test

//
// IqeB_VCGLib_Tridecimator.cpp
//

int IqeB_Mesh_VCGLib_Tridecimator( struct mesh *mesh,
                                   int ReduceVertexPercentage,
                                   int ReduceMaxVertexAmount,
                                   int ReduceMinVertexAmount);

/****************************** End Of File ******************************/
