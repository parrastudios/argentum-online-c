/****************************************************************************

  IqeB_GUI_ToolsExampleTreeView.cpp
  
  17.02.2015 RR: Save some work with a tree view element.
                 Maybe i can use it later.

*****************************************************************************
*/

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Tree.H>
#include <FL/Fl_Input.H>

/************************************************************************************
 * Globals
 *
 * NOTE: A Quake2 palyer has a height of 56 and a witdh of 32 units.
 */

/************************************************************************************
 * Statics
 */

static  Fl_Window *pMyToolWin;
static  Fl_Tree   *pTreeView;

/************************************************************************************
 * IqeB_GUI_Tree_Callback
 */

#define TREE_ACTION_MAX       8  // Max actions per mesh part
#define TREE_ACTION_DUMMY     0  // Need this for NULL pointer as arguments
#define TREE_ACTION_SELECT    1  // select mesh part
#define TREE_ACTION_NAME      2  // change name of mesh part
#define TREE_ACTION_MATERIAL  3  // change material of mesh part

static void IqeB_GUI_Tree_Callback( Fl_Widget *w, void *pValueArg)
{
  int iPart, iAction;
  Fl_Tree *pThis;
  Fl_Tree_Item *item;

  // ...

  pThis  = (Fl_Tree *)w;

  item = pThis->callback_item();

  if( item == NULL) {   // security test

    return;
  }

  // secuirty test, have model

  if( pDrawModel == NULL ||
      pDrawModel->mesh  == NULL ||
      pDrawModel->mesh->part  == NULL ||
      pDrawModel->mesh->part_count <= 0) {

    return;
  }

  // item must be selected or reslected

  if( pThis->callback_reason() != FL_TREE_REASON_SELECTED &&
      pThis->callback_reason() != FL_TREE_REASON_RESELECTED) {

    return;
  }

  // get part number and action

  iPart   = (int)item->user_data() / TREE_ACTION_MAX;
  iAction = (int)item->user_data() % TREE_ACTION_MAX;

  if( iPart < 0 || iPart >= pDrawModel->mesh->part_count) {   // out of range ?

    return;
  }


}

/************************************************************************************
 * close_cb, close this window
 *
 * pValueArg is a pointer to the widget. Set this pointer to NULL on deletion.
 */

static void close_cb( Fl_Widget *w, void *pValueArg)
{

  // simply hide the window.
  // If we show it gain, it keeps it position on the screen.

  w->hide();
}

/************************************************************************************
 * Presets for this tools window
 *
 */

#ifdef use_again // 16.02.2014 RR: Keep this until we need preferences
 static T_GUI_PreferenceEntry MyPreferences[] =
 {

  // Box display

  { PREF_T_INT,   "BoxEnable",      "0", &IqeB_ToolsDisp_BoxEnable},
  { PREF_T_FLOAT, "BoxHeight1",  "32.0", &IqeB_ToolsDisp_BoxHeight1},
  { PREF_T_FLOAT, "BoxHeight2", "-24.0", &IqeB_ToolsDisp_BoxHeight2},
  { PREF_T_FLOAT, "BoxWidth",    "32.0", &IqeB_ToolsDisp_BoxWidth},
};

// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "ToolsMeshParts", MyPreferences, sizeof( MyPreferences) / sizeof( T_GUI_PreferenceEntry));
#endif

/************************************************************************************
 * IqeB_GUI_ToolsMeshPartsUpdate
 *
 * Updates the GUI
 */

static void IqeB_GUI_ToolsMeshPartsUpdate()
{
  int i;
  char TempString[ MAX_FILENAME_LEN];
  Fl_Tree_Item *pItem, *pItem2;
  Fl_Input *pInput;

  if( pTreeView == NULL) {   // security test, need this pointer

    return;
  }

  // clear tree view

  pTreeView->clear();

  // test for mesh parts to display

  if( pDrawModel == NULL ||
      pDrawModel->mesh  == NULL ||
      pDrawModel->mesh->part  == NULL ||
      pDrawModel->mesh->part_count <= 0) {

    pTreeView->redraw();     // Redraw it

    return;
  }

  // add mesh parts to tree

  for( i = 0; i < pDrawModel->mesh->part_count; i++) {

    sprintf( TempString, "%2d: %s", i + 1, pDrawModel->mesh->part[ i].pMeshName);

    pItem = pTreeView->add( TempString);

    if( pItem != NULL) {   // security test

      pItem->user_data( (void *)( i * TREE_ACTION_MAX + TREE_ACTION_SELECT));
      pItem->close();   // keep this closed

      sprintf( TempString, "Name: %s", pDrawModel->mesh->part[ i].pMeshName);
      pItem2 = pTreeView->add( pItem, TempString);
      if( pItem2 != NULL) pItem2->user_data( (void *)( i * TREE_ACTION_MAX + TREE_ACTION_NAME));

      sprintf( TempString, "Material: %s", pDrawModel->mesh->part[ i].pMaterialString);
      pItem2 = pTreeView->add( pItem, TempString);
      if( pItem2 != NULL) pItem2->user_data( (void *)( i * TREE_ACTION_MAX + TREE_ACTION_MATERIAL));
    }
  }

  pTreeView->redraw();     // Redraw it
}

/************************************************************************************
 * IqeB_GUI_ToolsMeshPartsWin
 *
 * Open dialog
 */

void IqeB_GUI_ToolsMeshPartsWin( int xPos, int yPos)
{

  //
  // winodow already created= --> show it
  //

  if( pMyToolWin != NULL) {         // already have tool window

    if( ! pMyToolWin->visible()) {  // window is not visible

       pMyToolWin->show();          // show it
    }

    return;
  }

  //
  // creation of window on first call
  //

  if( pMyToolWin == NULL) {  // no tool window until now

    pMyToolWin = new Fl_Window( xPos, yPos, IQE_GUI_TOOLS_STD_WITDH, 400, "Tools/Parts");

    if( pMyToolWin == NULL) {  // security test

      return;
    }
  }

  //
  //  GUI things
  //

  int x1, x2, xx, xx2, y1, y, yy, yy2;
  char TempBuffer[ 256];

  Fl_Check_Button *pTemp_Check_Button;
  Fl_Float_Input  *pTemp_Float_Input;
  Fl_Box          *pTemp_Box;

  x1  = 8;
  xx  = pMyToolWin->w() - 16;
  xx2 = xx / 2;
  x2  = x1 + xx2 / 2;
  yy  = 30;

  y1 = 8;
  y = y1;

  // frame it

  pTemp_Box = new Fl_Box( x1 - 4, y1 - 4, pMyToolWin->w() - 8, pMyToolWin->h() - 8, "Box");
  pTemp_Box->align( FL_ALIGN_TOP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
  pTemp_Box->box( FL_DOWN_FRAME);

  // Tree view

  yy2 = pMyToolWin->h() - y1 - 16;

  pTreeView = new Fl_Tree( x1, y1, xx, yy2);

  pTreeView->box(FL_DOWN_BOX);
  pTreeView->tooltip( "Show mesh part names and textures");
  pTreeView->showroot( 0);				// don't show root of tree
  pTreeView->item_reselect_mode( FL_TREE_SELECTABLE_ALWAYS); // can reselect item
  pTreeView->callback( IqeB_GUI_Tree_Callback, NULL);
  pTreeView->end();

  y += yy2;

  // update frame height

  pTemp_Box->size( pTemp_Box->w(), y - y1 + 8);

  // finish up

  pMyToolWin->size( pMyToolWin->w(), pTemp_Box->y() + pTemp_Box->h() + 4);  // final size of window

  pMyToolWin->end();
  pMyToolWin->set_non_modal();
  pMyToolWin->callback( close_cb, &pMyToolWin);

  IqeB_GUI_ToolsMeshPartsUpdate(); // Update the GUI

  pMyToolWin->show();
}

/********************************** End Of File **********************************/
