/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef DATATYPE_BIT_SET_H
#define DATATYPE_BIT_SET_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Data structure
////////////////////////////////////////////////////////////
struct BitSetType
{
	uint32	BitCount;
	uint32	ByteCount;
	uint8 * Bits;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

struct BitSetType * BitSetCreate(uint32 BitsCount);

void BitSetDestroy(struct BitSetType * BitSet);

bool BitSetInitialize(struct BitSetType * BitSet, uint32 BitsCount);

void BitSetReset(struct BitSetType * BitSet);

bool BitSetIsNull(struct BitSetType * BitSet);

void BitSetClear(struct BitSetType * BitSet, uint32 BitIndex);

bool BitSetIsSet(struct BitSetType * BitSet, uint32 BitIndex);

void BitSetAdd(struct BitSetType * BitSet, uint32 BitIndex);

void BitSetAnd(struct BitSetType * Result, struct BitSetType * Left, struct BitSetType * Right);

void BitSetOr(struct BitSetType * Result, struct BitSetType * Left, struct BitSetType * Right);

void BitSetXor(struct BitSetType * Result, struct BitSetType * Left, struct BitSetType * Right);

#endif // DATATYPE_BIT_SET_H
