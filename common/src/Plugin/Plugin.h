/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	PLUGIN_PLUGIN_H
#define PLUGIN_PLUGIN_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Plugin/Connector.h>
#include <Plugin/PluginInterface.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PLUGIN_ID_LENGTH		UINTEGER_SUFFIX(0xFF)
typedef char					PluginId[PLUGIN_ID_LENGTH];

#define PLUGIN_CONNECTOR_DEFINE(ConnectorName) \
	PLUGIN_CONNECTOR_FUNC_DECL; \
	PLUGIN_CONNECTOR_IMPL_DECL(ConnectorName); \
	struct PluginConnectorInfoType PLUGIN_CONNECTOR_INFO_NAME = \
	{ \
		TypeInit(Name, PREPROCESSOR_STRINGIFY(ConnectorName)), \
		TypeInit(Register, (ConnectorRegisterHandler)&ConnectorName) \
	}

#define PLUGIN_CONNECTOR_IMPL(ConnectorName) \
	PLUGIN_CONNECTOR_IMPL_DECL(ConnectorName)

#define PLUGIN_CONNECTION_EXPORT() \
	PLUGIN_CONNECTOR_FUNC_IMPL \
	{ \
		if (PLUGIN_CONNECTOR_INFO_PARAM) \
		{ \
			*PLUGIN_CONNECTOR_INFO_PARAM = &PLUGIN_CONNECTOR_INFO_NAME; \
		} \
	}

#define PLUGIN_DESC(Name, Version, Language, RegisterHandler) \
	struct PluginDescriptorType Descriptor = \
	{ \

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct PluginType
{
	PluginId				Id;
	PluginInterfaceType	*	Interface;
	PluginConnectorType		Connector;

};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a plugin and initialize it
////////////////////////////////////////////////////////////
struct PluginType * PluginCreate();

////////////////////////////////////////////////////////////
/// Destroy a plugin and release its resources
////////////////////////////////////////////////////////////
void PluginDestroy(struct PluginType * Plugin);

#endif // PLUGIN_PLUGIN_H
