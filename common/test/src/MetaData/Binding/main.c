/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/General.h>

#include <MetaFloatArray.h>
#include <MetaFunctor.h>
#include <MetaPoint.h>

#include <System/IOHelper.h> // todo: refactor

////////////////////////////////////////////////////////////
/// Register tests into metadata system
////////////////////////////////////////////////////////////
void MetadataRegisterTests()
{
	// Float array declaration and implementation is opaque, so register it into metadata system
	MetaFloatArrayRegister();

	// Function declaration and implementation is opaque, so register it into metadata system
	MetaFunctorRegister();

	// Point declaration and implementation is opaque, so register it into metadata system
	MetaPointRegister();
}

////////////////////////////////////////////////////////////
/// Modify and print a meta float array
////////////////////////////////////////////////////////////
void MetadataFloatArrayTestRunImpl(MetadataManagerType MetaManager, MetadataValueType MetaFloatValueArray, float32 FloatValue)
{
	MetadataValueCountType Count;

	// Set float array
	for (Count = 0; Count < MetaManager->ValueCount(MetaFloatValueArray); ++Count)
	{
		// FloatArray[Count] = FloatValue;
		MetaManager->ValueSetAtFloat(MetaFloatValueArray, Count, FloatValue);

		FloatValue += 1.0f;
	}

	// Print float array
	for (Count = 0; Count < MetaManager->ValueCount(MetaFloatValueArray); ++Count)
	{
		// FloatValue = FloatArray[Count];
		FloatValue = MetaManager->ValueGetAtFloat(MetaFloatValueArray, Count);

		printf("%f\n", FloatValue);
	}
}

////////////////////////////////////////////////////////////
/// Execute float array test
////////////////////////////////////////////////////////////
void MetadataFloatArrayTestRun(MetadataManagerType MetaManager)
{
	// Obtain binded array and run test
	MetadataFloatArrayTestRunImpl(MetaManager, MetaManager->ValueGet("my_float_array"), 0.0f);

	// Obtain created array and run test
	MetadataFloatArrayTestRunImpl(MetaManager, MetaManager->ValueGet("my_runtime_float_array"), -100.0f);
}

////////////////////////////////////////////////////////////
/// Execute function test
////////////////////////////////////////////////////////////
void MetadataFunctionTestRun(MetadataManagerType MetaManager)
{
	MetaManager->FunctionExecute("float_int_function", 9923.0f, 50011.0f, 34666490);
}

////////////////////////////////////////////////////////////
/// Execute point test
////////////////////////////////////////////////////////////
void MetadataPointTestRun(MetadataManagerType MetaManager)
{
#if 0
	MetadataObjectType MetaPoint;

	// Point now is available through metadata system
	MetaPoint = MetadataManager->Create("Point", 10.0f, 5.0f);

	// Print information about point
	MetadataManager->Invoke(MetaPoint, "print");

	// Update position from point
	MetadataManager->Invoke(MetaPoint, "move", 20.0f, 18.0f);

	// Print information about point
	MetadataManager->Invoke(MetaPoint, "print");

	// Clear MetaPoint
	MetadataManager->Destroy(MetaPoint);
#endif
}

////////////////////////////////////////////////////////////
/// Application entry point
////////////////////////////////////////////////////////////
int main(int argc, char * argv[])
{
	// Get metadata manager instance
	MetadataManagerType MetaManager = MetadataManagerGlobalInstance();

	// Initialize tests
	MetadataRegisterTests();

	// Run float array test
	MetadataFloatArrayTestRun(MetaManager);

	// Run function test
	MetadataFunctionTestRun(MetaManager);

	// Run point test
	MetadataPointTestRun(MetaManager);

	// Destroy metadata manager instance
	MetaManager->Destroy();

	return 0;
}