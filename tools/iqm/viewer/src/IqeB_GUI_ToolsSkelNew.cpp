/****************************************************************************

  IqeB_GUI_ToolsSkelNewWin.cpp

  Create new skeletons or modify complete skeleton.

  27.03.2015 RR: First editon of this file.

*****************************************************************************
*/

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_File_Browser.H>
#include <FL/Fl_Button.H>
#include <FL/fl_ask.H>

/************************************************************************************
 * Globals
 *
 */

/************************************************************************************
 * Statics
 */

static  Fl_Window  *pMyToolWin;
static int MyWinPosX = IQE_GUI_NO_WINPOS_X, MyWinPosY = IQE_GUI_NO_WINPOS_Y; // last window position

static Fl_File_Browser *pGUI_SkeletonBrowser;      // Skeleton file browser
static Fl_Button *pGUI_ButRemove;     // Remove the skeleton
static Fl_Button *pGUI_ButNormalize;  // Normalize the skeleton
static Fl_Button *pGUI_ButLoad;       // Load skeleton from file
static Fl_Button *pGUI_ButSave;       // Save current skelton to file
static Fl_Button *pGUI_ButRefresh;    // Refresh skelton file list

/************************************************************************************
 * update GUI of this tool window
 */

static void MyWinUpdate()
{

  IqeB_GUI_WidgetActivate( pGUI_ButRemove , pDrawModel != NULL &&               // Have a model displayed
                                            pDrawModel->skel != NULL &&
                                            pDrawModel->skel->joint_count > 0); // Have a skeleton

  IqeB_GUI_WidgetActivate( pGUI_ButNormalize , pDrawModel != NULL &&               // Have a model displayed
                                               pDrawModel->skel != NULL &&
                                               pDrawModel->skel->joint_count > 0 && // Have a skeleton
                                               pDrawModel->skel->IsNormalized == false); // and skeleton is not normalized

  IqeB_GUI_WidgetActivate( pGUI_ButLoad , pDrawModel != NULL &&               // Have a model displayed
                                          pDrawModel->skel != NULL &&
                                          pGUI_SkeletonBrowser->size() > 0 && // The browser shows something
                                          pGUI_SkeletonBrowser->value() > 0); // An item is selected

  IqeB_GUI_WidgetActivate( pGUI_ButSave , pDrawModel != NULL &&               // Have a model displayed
                                          pDrawModel->skel != NULL &&
                                          pDrawModel->skel->joint_count > 0); // Have a skeleton
}

/************************************************************************************
 * Refresh the skeleton file list
 */

static void SkeletonFileListRefresh()
{

  if( pGUI_SkeletonBrowser == NULL) {    // secuirty test

    return;
  }

  pGUI_SkeletonBrowser->load( "Skeletons", fl_numericsort);

  // remove directories

  int i, slen;
  const char *pText;

  for( i = pGUI_SkeletonBrowser->size() - 1; i >= 1 ; i--) {

    pText = pGUI_SkeletonBrowser->text( i);

    if( pText != NULL) {

      slen = strlen( pText);

      // test for directory, last char is '/' or '\'
      if( slen > 0 && (pText[ slen - 1] == '/' || pText[ slen - 1] == '\\')) {

        pGUI_SkeletonBrowser->remove( i);  // Remove the directory
      }
    }
  }
}

/************************************************************************************
 * Callback, button 'Remove' pressed
 */

static void ButtonRemoveCallback(Fl_Widget *w, void *data)
{

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  // Test we have a skeleton

  if( pDrawModel == NULL ||
      pDrawModel->skel == NULL ||
      pDrawModel->skel->joint_count <= 0) {  // have no skeleton or no model

    return;
  }

  // Delete an existing skeleton and any animations.

  IqeB_SkelReset( pDrawModel->skel);
  IqeB_ModelFreeData( &pDrawModel, IQE_MODEL_FREE_ANIMATION);  // remove all animations

  // and update ...

  IqeB_ModelSkeletonMatrixCalc( pDrawModel);  // update skeleton matrix

  IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_MODEL_CHANGE); // Also update the GUI
}

/************************************************************************************
 * Callback, button 'Normalize' pressed
 */

static void ButtonNormalizeCallback(Fl_Widget *w, void *data)
{

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  // Test we have a skeleton

  if( pDrawModel == NULL ||
      pDrawModel->skel == NULL ||
      pDrawModel->skel->joint_count <= 0) {  // have no skeleton or no model

    return;
  }

  // No rotations ...

  IqeB_ModelSkelNormalize( pDrawModel);

  // and update ...

  IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_MODEL_CHANGE); // Also update the GUI
}

/************************************************************************************
 * Callback, button 'Load' pressed
 */

static void ButtonLoadCallback(Fl_Widget *w, void *data)
{
  int Selected, ierr;
  const char *pText;
  char FileName[ MAX_FILENAME_LEN];
  char ErrorString[ 1024];
  struct skel TempSkel;

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  // Test we have a model

  if( pDrawModel == NULL ||                 // have no model
      pDrawModel->skel == NULL ||
      pGUI_SkeletonBrowser->size() <= 0 ||  // have no skeleton selected
      pGUI_SkeletonBrowser->value() <= 0) {

    return;
  }

  // Test the skeleton files to be existing

  Selected = pGUI_SkeletonBrowser->value();
  if( Selected <= 0) {    // no line selected
    return;               // panic exit
  }

  pText = pGUI_SkeletonBrowser->text( Selected);

  // Try to load the skeleton

  sprintf( FileName, "Skeletons/%s", pText);

  ErrorString[ 0] = '\0';
  ierr = IqeB_SkelLoadFromFile( &TempSkel, FileName, ErrorString);

  if( ierr < 0) {

    IqeB_DispMessage( DRAW_MSG_COL_ERR,
                      (char *)"Error loading the skeleton file",
                      FileName, ErrorString);

    return;
  }

  // got the file, this is the right moment to delete an existing
  // skeleton and any animations.

  IqeB_SkelReset( pDrawModel->skel);
  IqeB_ModelFreeData( &pDrawModel, IQE_MODEL_FREE_ANIMATION);  // remove all animations

  // Copy over the skeleton data

  memcpy( pDrawModel->skel, &TempSkel, sizeof( TempSkel));

  // Autoadjust the skeleton to the size of the model

  IqeB_SkelAdjustSizeToMesh( pDrawModel->skel, pDrawModel->mesh);

  // and update ...

  IqeB_ModelSkeletonMatrixCalc( pDrawModel);  // update skeleton matrix

  // Enshure animation is off ot show the skeleton
  IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doAnimations, 0); // Update variable and GUI

  // Enshure animation is off to show the skeleton
  IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doAnimations, 0); // Update variable and GUI

  // Enshure 'show skelton' is on to show the skeleton
  IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doskeleton, 1); // Update variable and GUI

  // ...

  IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_MODEL_CHANGE); // Also update the GUI
}

/************************************************************************************
 * Callback, button 'Saved' pressed
 */

static void ButtonSaveCallback(Fl_Widget *w, void *data)
{

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  // Test we have a skeleton

  if( pDrawModel == NULL ||
      pDrawModel->skel == NULL ||
      pDrawModel->skel->joint_count <= 0) {  // have no skeleton or no model

    return;
  }

  // Ask for filename

  const char *input;
  char FileName[ MAX_FILENAME_LEN];
  char ErrorString[ 1024];
  int ierr;

  fl_message_title( "Save skelton file");
  input = fl_input( "Filename:", "MySkeleton");

  if( input == NULL || input[0] == '\0') {   // have NO filename

    return;    // return to caller
  }

  sprintf( FileName, "Skeletons/%s.txt", input);

  // Test file for existing

  FILE *fp = fl_fopen( FileName, "rt");   // Try to open file
  if( fp) {                               // file exists

    fclose(fp);

    if( fl_choice( "The file \"%s.txt\" already exists.\n"
                   "Do you want to replace it?",
                   "Cancel", "Replace", NULL, input) == 0) {
      return;
    }
  }

  // write to file

  ErrorString[ 0] = '\0';
  ierr = IqeB_SkelWriteToFile( pDrawModel->skel, FileName, input, ErrorString);

  if( ierr < 0) {

    IqeB_DispMessage( DRAW_MSG_COL_ERR,
                      (char *)"Error save to skeleton file",
                      FileName, ErrorString);

    return;
  }

  // ...

  SkeletonFileListRefresh();    // Load skeleton files to browser
}

/************************************************************************************
 * Callback, button 'Refresh' pressed
 */

static void ButtonRefreshCallback(Fl_Widget *w, void *data)
{

  IqeB_DispMessage( DRAW_MSG_COL_OK,
                    NULL, NULL, NULL);

  SkeletonFileListRefresh();    // Load skeleton files to browser
}

/************************************************************************************
 * Callback, browser entry is selected
 */

static void BrowserSelectCallback(Fl_Widget *w, void *data)
{
  int MouseButton, event, clicks;

  MyWinUpdate();  // Update

  // test for left mouse double click

	event       = Fl::event();         // Last (this) event processed
	clicks      = Fl::event_clicks();  // Mouse clicks
	MouseButton = Fl::event_button();  // mouse button from last event

  if( event == FL_RELEASE && clicks > 0 &&
      MouseButton == FL_LEFT_MOUSE) {  // Mouse release and double click left mouse button

    // Try the load skelton button

    ButtonLoadCallback( pGUI_ButLoad, NULL);
  }
}

/************************************************************************************
 * IqeB_GUI_UpdateCallback
 *
 * Is called from IqeB_GUI_UpdateWidgets() if something has changed
 * what needs an update (new model displayed, ...).
 */

static void IqeB_GUI_UpdateCallback( int UpdateFlags)
{

  if( pMyToolWin == NULL) {  // security test, has main window

    return;
  }

  MyWinUpdate();               // Update the GUI
}

/************************************************************************************
 * close_cb, close this window
 *
 * pValueArg is a pointer to the widget. Set this pointer to NULL on deletion.
 */

static void close_cb( Fl_Widget *w, void *pValueArg)
{

  IqeB_GUI_CloseToolWindow( (void **)&pMyToolWin);
}

/************************************************************************************
 * Presets for this tools window
 *
 */

#ifdef use_again // 16.02.2014 RR: Keep this until we need preferences
 static T_GUI_PreferenceEntry MyPreferences[] =
 {

  // Box display

  { PREF_T_INT,   "BoxEnable",      "0", &IqeB_ToolsDisp_BoxEnable},
  { PREF_T_FLOAT, "BoxHeight1",  "32.0", &IqeB_ToolsDisp_BoxHeight1},
  { PREF_T_FLOAT, "BoxHeight2", "-24.0", &IqeB_ToolsDisp_BoxHeight2},
  { PREF_T_FLOAT, "BoxWidth",    "32.0", &IqeB_ToolsDisp_BoxWidth},
};
#endif

// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "ToolsSkelFiles", NULL, 0,
                                               (void **)(&pMyToolWin), &MyWinPosX, &MyWinPosY);

/************************************************************************************
 * IqeB_GUI_ToolsSkelNewWin
 *
 * Open dialog
 */

void IqeB_GUI_ToolsSkelNewWin( int xLeft, int xRight, int yTop, int yBotton)
{

  //
  // winodow already created --> show it
  //

  if( pMyToolWin != NULL) {         // already have tool window

    // Show invisble window or bring visible window to foreground

    pMyToolWin->show();          // show it

    return;
  }

  //
  // creation of window on first call
  //

  if( pMyToolWin == NULL) {  // no tool window until now

    int xPos, yPos;

    xPos = xRight;
    yPos = yTop;

    if( MyWinPosX != IQE_GUI_NO_WINPOS_X && MyWinPosY != IQE_GUI_NO_WINPOS_Y) { // have last window position

      xPos = MyWinPosX;
      yPos = MyWinPosY;
    }

    pMyToolWin = new Fl_Window( xPos, yPos, IQE_GUI_TOOLS_STD_WITDH, 400, "Skeleton/Files + utils");

    if( pMyToolWin == NULL) {  // security test

      return;
    }

    IqeB_GUI_RegisterCallback( (void **)&pMyToolWin, IqeB_GUI_UpdateCallback, IQE_GUI_UPDATE_MODEL_CHANGE);
  }

  //
  //  GUI things
  //

  int x, x1, xx, xx2, y1, y, yy, yy2;

  x1  = 4;
  xx  = pMyToolWin->w() - 8;
  yy  = 32;

  y1 = 20;
  y = y1;

  // File Browser

  yy2 = 280;

  pGUI_SkeletonBrowser = new Fl_File_Browser( x1, y1, xx, yy2, "Skeleton files");
  pGUI_SkeletonBrowser->align( FL_ALIGN_TOP_LEFT);     // align for label
  pGUI_SkeletonBrowser->type(FL_HOLD_BROWSER);        // use for single selection
  pGUI_SkeletonBrowser->callback( BrowserSelectCallback, NULL);
  pGUI_SkeletonBrowser->tooltip( "Doubleclick with left mouse button\n"
                                 "to load the skeleton file.");

  pGUI_SkeletonBrowser->filter( "*.txt");
  pGUI_SkeletonBrowser->filetype(Fl_File_Browser::FILES);

  SkeletonFileListRefresh();    // Load skeleton files to browser

  // ...

  y += yy2;

  // Buttons

  y += 4;
  yy = 22;

  x = x1;
  xx2 = (xx - 8) / 3;

  pGUI_ButLoad = new Fl_Button( x, y, xx2, yy, "&Load");
  pGUI_ButLoad->callback( ButtonLoadCallback, NULL);
  pGUI_ButLoad->tooltip( "Load skeleton from selected file");

  x += xx2 + 4;
  pGUI_ButSave = new Fl_Button( x, y, xx2, yy, "&Save");
  pGUI_ButSave->callback( ButtonSaveCallback, NULL);
  pGUI_ButSave->tooltip( "Save current shown skeleton\n"
                         "to a skelton file");

  x += xx2 + 4;
  pGUI_ButRefresh = new Fl_Button( x, y, xx2, yy, "Refresh");
  pGUI_ButRefresh->callback( ButtonRefreshCallback, NULL);
  pGUI_ButRefresh->tooltip( "Refresh the skelton file list.\n"
                            "Use it after copy a file into the skeleton direcory.\n");

  y += yy + 4;
  x = x1;

  pGUI_ButRemove = new Fl_Button( x, y, xx2, yy, "&Remove");
  pGUI_ButRemove->callback( ButtonRemoveCallback, NULL);
  pGUI_ButRemove->tooltip( "Removes the skeleton (and all animations) from the model");

  x += xx2 + 4;

  pGUI_ButNormalize = new Fl_Button( x, y, xx2, yy, "&Normalize");
  pGUI_ButNormalize->callback( ButtonNormalizeCallback, NULL);
  pGUI_ButNormalize->tooltip( "If this button is enabled your skeleton is not normalized!\n"
                              "  A normalized skeleton has zeroed joint rotation angles and\n"
                              "  joint sizes of 1.0.\n"
                              "* Recalculate joint location information and reset joint\n"
                              "  rotation angles to zero and joint sizes to 1.\n"
                              "  IqeBrowser needs 'normalized' skeletons to edit the\n"
                              "  skeletons joint locations with the mouse.\n"
                              "* Also recalculates the animations.");

  y += yy;

  // finish up

  pMyToolWin->size( pMyToolWin->w(), y + 4);  // final size of window

  pMyToolWin->end();
  pMyToolWin->set_non_modal();
  pMyToolWin->callback( close_cb, &pMyToolWin);

  MyWinUpdate(); // Update the GUI

  pMyToolWin->show();
}

/********************************** End Of File **********************************/
