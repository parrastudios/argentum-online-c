/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/LightingBase.h>

#include <System/IOHelper.h> // todo: remove this

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

bool LightingBaseGetLocations(struct LightingBaseTechniqueType * LightingBase)
{
	if (LightingBase)
	{
		uinteger i;

		// Link shader uniforms
		LightingBase->WVPLocation = ShaderProgramGetUniformLocation(&LightingBase->Technique, "WVP");
		LightingBase->WorldMatrixLocation = ShaderProgramGetUniformLocation(&LightingBase->Technique, "World");
		LightingBase->ColorTextureLocation = ShaderProgramGetUniformLocation(&LightingBase->Technique, "ColorMap");
		LightingBase->EyeWorldPosLocation = ShaderProgramGetUniformLocation(&LightingBase->Technique, "EyeWorldPos");
		LightingBase->LightDirectionalLocation.Color = ShaderProgramGetUniformLocation(&LightingBase->Technique, "DirectLight.Base.Color");
		LightingBase->LightDirectionalLocation.AmbientIntensity = ShaderProgramGetUniformLocation(&LightingBase->Technique, "DirectLight.Base.AmbientIntensity");
		LightingBase->LightDirectionalLocation.Direction = ShaderProgramGetUniformLocation(&LightingBase->Technique, "DirectLight.Direction");
		LightingBase->LightDirectionalLocation.DiffuseIntensity = ShaderProgramGetUniformLocation(&LightingBase->Technique, "DirectLight.Base.DiffuseIntensity");
		LightingBase->MatSpecularIntensityLocation = ShaderProgramGetUniformLocation(&LightingBase->Technique, "MatSpecularIntensity");
		LightingBase->MatSpecularPowerLocation = ShaderProgramGetUniformLocation(&LightingBase->Technique, "SpecularPower");
		LightingBase->LightsPointSizeLocation = ShaderProgramGetUniformLocation(&LightingBase->Technique, "PointLightsSize");
		LightingBase->LightsSpotSizeLocation = ShaderProgramGetUniformLocation(&LightingBase->Technique, "SpotLightsSize");

		// Check if uniform locations are valid
		if (LightingBase->WVPLocation == GL_UNIFORM_LOCATION_INVALID ||
			LightingBase->WorldMatrixLocation == GL_UNIFORM_LOCATION_INVALID ||
			LightingBase->ColorTextureLocation == GL_UNIFORM_LOCATION_INVALID ||
			LightingBase->EyeWorldPosLocation == GL_UNIFORM_LOCATION_INVALID ||
			LightingBase->LightDirectionalLocation.Color == GL_UNIFORM_LOCATION_INVALID ||
			LightingBase->LightDirectionalLocation.AmbientIntensity == GL_UNIFORM_LOCATION_INVALID ||
			LightingBase->LightDirectionalLocation.Direction == GL_UNIFORM_LOCATION_INVALID ||
			LightingBase->LightDirectionalLocation.DiffuseIntensity == GL_UNIFORM_LOCATION_INVALID ||
			LightingBase->MatSpecularIntensityLocation == GL_UNIFORM_LOCATION_INVALID ||
			LightingBase->MatSpecularPowerLocation == GL_UNIFORM_LOCATION_INVALID ||
			LightingBase->LightsPointSizeLocation == GL_UNIFORM_LOCATION_INVALID ||
			LightingBase->LightsSpotSizeLocation == GL_UNIFORM_LOCATION_INVALID)
		{
			return false;
		}

		// Link shader point light uniforms
		for (i = 0; i < LIGHTING_BASE_LIGHT_POINT_MAX; ++i)
		{
			char UniformName[0x40];

			sprintf(UniformName, "PointLights[%d].Base.Color", i);
			LightingBase->LightPointLocations[i].Color = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "PointLights[%d].Base.AmbientIntensity", i);
			LightingBase->LightPointLocations[i].AmbientIntensity = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "PointLights[%d].Position", i);
			LightingBase->LightPointLocations[i].Position = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "PointLights[%d].Base.DiffuseIntensity", i);
			LightingBase->LightPointLocations[i].DiffuseIntensity = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "PointLights[%d].Atten.Constant", i);
			LightingBase->LightPointLocations[i].Attenuation.Constant = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "PointLights[%d].Atten.Linear", i);
			LightingBase->LightPointLocations[i].Attenuation.Linear = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "PointLights[%d].Atten.Exp", i);
			LightingBase->LightPointLocations[i].Attenuation.Exp = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			if (LightingBase->LightPointLocations[i].Color == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightPointLocations[i].AmbientIntensity == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightPointLocations[i].Position == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightPointLocations[i].DiffuseIntensity == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightPointLocations[i].Attenuation.Constant == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightPointLocations[i].Attenuation.Linear == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightPointLocations[i].Attenuation.Exp == GL_UNIFORM_LOCATION_INVALID)
			{
				return false;
			}
		}

		// Link shader point light uniforms
		for (i = 0; i < LIGHTING_BASE_LIGHT_SPOT_MAX; ++i)
		{
			char UniformName[0x40];

			sprintf(UniformName, "SpotLights[%d].Base.Base.Color", i);
			LightingBase->LightSpotLocations[i].Color = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "SpotLights[%d].Base.Base.AmbientIntensity", i);
			LightingBase->LightSpotLocations[i].AmbientIntensity = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "SpotLights[%d].Base.Position", i);
			LightingBase->LightSpotLocations[i].Position = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "SpotLights[%d].Direction", i);
			LightingBase->LightSpotLocations[i].Direction = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "SpotLights[%d].Cutoff", i);
			LightingBase->LightSpotLocations[i].Cutoff = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "SpotLights[%d].Base.Base.DiffuseIntensity", i);
			LightingBase->LightSpotLocations[i].DiffuseIntensity = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "SpotLights[%d].Base.Atten.Constant", i);
			LightingBase->LightSpotLocations[i].Atten.Constant = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "SpotLights[%d].Base.Atten.Linear", i);
			LightingBase->LightSpotLocations[i].Atten.Linear = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			sprintf(UniformName, "SpotLights[%d].Base.Atten.Exp", i);
			LightingBase->LightSpotLocations[i].Atten.Exp = ShaderProgramGetUniformLocation(&LightingBase->Technique, UniformName);

			if (LightingBase->LightSpotLocations[i].Color == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightSpotLocations[i].AmbientIntensity == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightSpotLocations[i].Position == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightSpotLocations[i].Direction == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightSpotLocations[i].Cutoff == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightSpotLocations[i].DiffuseIntensity == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightSpotLocations[i].Atten.Constant == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightSpotLocations[i].Atten.Linear == GL_UNIFORM_LOCATION_INVALID ||
				LightingBase->LightSpotLocations[i].Atten.Exp == GL_UNIFORM_LOCATION_INVALID)
			{
				return false;
			}
		}

		return true;
	}

	return false;
}

bool LightingBaseInitialize(struct LightingBaseTechniqueType * LightingBase)
{
	if (LightingBase)
	{
		// Initialize shader program, load shaders and build the program
		if (ShaderProgramInitialize(&LightingBase->Technique) &&
			ShaderProgramAppend(&LightingBase->Technique, GL_VERTEX_SHADER, "data/shaders/gl3/LightingBase.vs") &&
			ShaderProgramAppend(&LightingBase->Technique, GL_FRAGMENT_SHADER, "data/shaders/gl3/LightingBase.fs") &&
			ShaderProgramLink(&LightingBase->Technique))
		{
			return LightingBaseGetLocations(LightingBase);
		}
	}

	return false;
}

void LightingBaseSetWVP(struct LightingBaseTechniqueType * LightingBase, struct Matrix4f * WVP)
{
	if (LightingBase && WVP)
	{
		DeviceGetExt()->glUniformMatrix4fv(LightingBase->WVPLocation, 1, GL_TRUE, (const GLfloat *)&WVP->v[0]);
	}
}

void LightingBaseSetWorldMatrix(struct LightingBaseTechniqueType * LightingBase, struct Matrix4f * World)
{
	if (LightingBase && World)
	{
		DeviceGetExt()->glUniformMatrix4fv(LightingBase->WorldMatrixLocation, 1, GL_TRUE, (const GLfloat *)&World->v[0]);
	}
}

void LightingBaseSetColorTextureUnit(struct LightingBaseTechniqueType * LightingBase, uinteger TextureUnit)
{
	if (LightingBase)
	{
		DeviceGetExt()->glUniform1i(LightingBase->ColorTextureLocation, TextureUnit);
	}
}

void LightingBaseSetDirectionalLight(struct LightingBaseTechniqueType * LightingBase, struct LightDirectional * Light)
{
	if (LightingBase && Light)
	{
		struct Vector3f NormalizedDirection;

		// Copy direction vector
		Vector3fCopy(&NormalizedDirection, &Light->Direction);

		// Normalize light direction
		Vector3fNormalize(&NormalizedDirection);

		// Set color
		DeviceGetExt()->glUniform3f(LightingBase->LightDirectionalLocation.Color, Light->Base.Color.x, Light->Base.Color.y, Light->Base.Color.z);

		// Set ambient intensity
		DeviceGetExt()->glUniform1f(LightingBase->LightDirectionalLocation.AmbientIntensity, Light->Base.AmbientIntensity);

		// Set light direction
		DeviceGetExt()->glUniform3f(LightingBase->LightDirectionalLocation.Direction, NormalizedDirection.x, NormalizedDirection.y, NormalizedDirection.z);

		// Set diffuse intensity
		DeviceGetExt()->glUniform1f(LightingBase->LightDirectionalLocation.DiffuseIntensity, Light->Base.DiffuseIntensity);
	}
}

void LightingBaseSetPointLights(struct LightingBaseTechniqueType * LightingBase, struct LightPoint * Lights, uinteger Size)
{
	if (LightingBase && Lights && Size > 0)
	{
		uinteger i;

		DeviceGetExt()->glUniform1i(LightingBase->LightsPointSizeLocation, Size);

		for (i = 0; i < Size; ++i)
		{
			// Set point light properties
			DeviceGetExt()->glUniform3f(LightingBase->LightPointLocations[i].Color, Lights[i].Base.Color.x, Lights[i].Base.Color.y, Lights[i].Base.Color.z);
			DeviceGetExt()->glUniform1f(LightingBase->LightPointLocations[i].AmbientIntensity, Lights[i].Base.AmbientIntensity);
			DeviceGetExt()->glUniform1f(LightingBase->LightPointLocations[i].DiffuseIntensity, Lights[i].Base.DiffuseIntensity);
			DeviceGetExt()->glUniform3f(LightingBase->LightPointLocations[i].Position, Lights[i].Position.x, Lights[i].Position.y, Lights[i].Position.z);
			DeviceGetExt()->glUniform1f(LightingBase->LightPointLocations[i].Attenuation.Constant, Lights[i].Attenuation.Constant);
			DeviceGetExt()->glUniform1f(LightingBase->LightPointLocations[i].Attenuation.Linear, Lights[i].Attenuation.Linear);
			DeviceGetExt()->glUniform1f(LightingBase->LightPointLocations[i].Attenuation.Exp, Lights[i].Attenuation.Exp);
		}
	}
}

void LightingBaseSetSpotLights(struct LightingBaseTechniqueType * LightingBase, struct LightSpot * Lights, uinteger Size)
{
	if (LightingBase && Lights && Size > 0)
	{
		uinteger i;

		DeviceGetExt()->glUniform1i(LightingBase->LightsSpotSizeLocation, Size);

		for (i = 0; i < Size; ++i)
		{
			struct Vector3f NormalizedDirection;

			// Copy direction vector
			Vector3fCopy(&NormalizedDirection, &Lights[i].Direction);

			// Normalize light direction
			Vector3fNormalize(&NormalizedDirection);

			// Set spot light properties
			DeviceGetExt()->glUniform3f(LightingBase->LightSpotLocations[i].Color, Lights[i].Point.Base.Color.x, Lights[i].Point.Base.Color.y, Lights[i].Point.Base.Color.z);
			DeviceGetExt()->glUniform1f(LightingBase->LightSpotLocations[i].AmbientIntensity, Lights[i].Point.Base.AmbientIntensity);
			DeviceGetExt()->glUniform1f(LightingBase->LightSpotLocations[i].DiffuseIntensity, Lights[i].Point.Base.DiffuseIntensity);
			DeviceGetExt()->glUniform3f(LightingBase->LightSpotLocations[i].Position, Lights[i].Point.Position.x, Lights[i].Point.Position.y, Lights[i].Point.Position.z);
			DeviceGetExt()->glUniform3f(LightingBase->LightSpotLocations[i].Direction, NormalizedDirection.x, NormalizedDirection.y, NormalizedDirection.z);
			DeviceGetExt()->glUniform1f(LightingBase->LightSpotLocations[i].Cutoff, cosf(degtorad(Lights[i].Cutoff)));
			DeviceGetExt()->glUniform1f(LightingBase->LightSpotLocations[i].Atten.Constant, Lights[i].Point.Attenuation.Constant);
			DeviceGetExt()->glUniform1f(LightingBase->LightSpotLocations[i].Atten.Linear, Lights[i].Point.Attenuation.Linear);
			DeviceGetExt()->glUniform1f(LightingBase->LightSpotLocations[i].Atten.Exp, Lights[i].Point.Attenuation.Exp);
		}
	}
}

void LightingBaseSetEyeWorldPos(struct LightingBaseTechniqueType * LightingBase, struct Vector3f * EyeWorldPos)
{
	if (LightingBase && EyeWorldPos)
	{
		DeviceGetExt()->glUniform3f(LightingBase->EyeWorldPosLocation, EyeWorldPos->x, EyeWorldPos->y, EyeWorldPos->z);
	}
}

void LightingBaseSetMatSpecularIntensity(struct LightingBaseTechniqueType * LightingBase, float Intensity)
{
	if (LightingBase)
	{
		DeviceGetExt()->glUniform1f(LightingBase->MatSpecularIntensityLocation, Intensity);
	}
}

void LightingBaseSetMatSpecularPower(struct LightingBaseTechniqueType * LightingBase, float Power)
{
	if (LightingBase)
	{
		DeviceGetExt()->glUniform1f(LightingBase->MatSpecularPowerLocation, Power);
	}
}

void LightingBaseDestroy(struct LightingBaseTechniqueType * LightingBase)
{
	if (LightingBase)
	{
		// Destroy shader program
		ShaderProgramDestroy(&LightingBase->Technique);
	}
}
