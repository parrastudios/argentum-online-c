/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

#ifndef GAME_PLAYER_H
#define GAME_PLAYER_H

////////////////////////////////////////////////////////////
// Header constraints
////////////////////////////////////////////////////////////
#ifndef GAME_CHARACTER_H
#	error Game/Character must be included instead
#endif

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/PlayerDef.h>
#include <Math/Geometry/Vector.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PLAYER_LOG_IN_ERROR_NONE		0x00	///< Player Login Errors
#define PLAYER_LOG_IN_ERROR_INVALID		0x01
#define PLAYER_LOG_IN_ERROR_LOGGED		0x02
#define PLAYER_LOG_IN_ERROR_BAN			0x03

#define PLAYER_LOG_OUT_ERROR_NONE		0x00	///< Player Logout Errors
#define PLAYER_LOG_OUT_ERROR_NOT_EXIT	0x01

#define PlayerGetSize() CharacterGetSize(CHARACTER_TYPE_PLAYER)
#define PlayerToDBCharacter(Character, DBCharacter) CharacterToDBCharacter(Character, DBCharacter, DB_PLAYER_OBJECT_TYPE)

////////////////////////////////////////////////////////////
// Enumerations
////////////////////////////////////////////////////////////
enum
{
	PLAYER_CLASS_MAGE = 0x00,		///< Old classes
	PLAYER_CLASS_CLERIC,
	PLAYER_CLASS_WARRIOR,
	PLAYER_CLASS_ASSASIN,
	PLAYER_CLASS_THIEF,
	PLAYER_CLASS_BARD,
	PLAYER_CLASS_DRUID,
	PLAYER_CLASS_BANDIT,
	PLAYER_CLASS_PALADIN,
	PLAYER_CLASS_HUNTER,
	PLAYER_CLASS_FISHER,
	PLAYER_CLASS_BLACKSMITH,
	PLAYER_CLASS_LUMBERJACK,
	PLAYER_CLASS_MINER,
	PLAYER_CLASS_CARPENTER,
	PLAYER_CLASS_PIRAT,
	PLAYER_CLASS_SHAMAN,			///< New classes
	PLAYER_CLASS_SORCERER,
	PLAYER_CLASS_MINDBENDER,
	PLAYER_CLASS_PYROMANCER,
	PLAYER_CLASS_NECROMANCER,
	PLAYER_CLASS_ARACHNOMANCER,
	PLAYER_CLASS_MEDIUM,
	PLAYER_CLASS_SAGE,
	PLAYER_CLASS_ACOLYTE,
	PLAYER_CLASS_ARCHANGEL,
	PLAYER_CLASS_AVATAR,
	PLAYER_CLASS_ILLUSIONIST,
	PLAYER_CLASS_JOKER,
	PLAYER_CLASS_DERVISH,
	PLAYER_CLASS_MONK,
	PLAYER_CLASS_DAEMON,
	PLAYER_CLASS_MASKED,
	PLAYER_CLASS_MYSTICAL_SAILOR,
	PLAYER_CLASS_GUARDIAN,
	PLAYER_CLASS_RUNEMASTER,
	PLAYER_CLASS_HERO,
	PLAYER_CLASS_ARCANE_ROGUE,
	PLAYER_CLASS_DRAGON_DISCIPLE,
	PLAYER_CLASS_TAOIST,
	PLAYER_CLASS_WATCHMAN,
	PLAYER_CLASS_GLADIATOR,
	PLAYER_CLASS_MASTER_OF_CHAINS,
	PLAYER_CLASS_HITMAN,
	PLAYER_CLASS_FIGHTER,
	PLAYER_CLASS_MUSKETEER,
	PLAYER_CLASS_TAMER,
	PLAYER_CLASS_ARCHER,
	PLAYER_CLASS_TAILOR,
	PLAYER_CLASS_HORSEMAN,
	PLAYER_CLASS_BERSERKER,
	PLAYER_CLASS_FETISHIST,

	PLAYER_CLASS_COUNT
};

enum
{
	PLAYER_CITY_ULLATHORPE = 0x00,	///< Old cities
	PLAYER_CITY_NIX,
	PLAYER_CITY_BANDERBILL,
	PLAYER_CITY_LINDOS,
	PLAYER_CITY_ARGHAL,
	//PLAYER_CITY_TODO,				///< New cities

	PLAYER_CITY_COUNT
};

enum
{
	PLAYER_RACE_HUMAN = 0x00,		///< Old races
	PLAYER_RACE_ELF,
	PLAYER_RACE_DROW,
	PLAYER_RACE_GNOME,
	PLAYER_RACE_DWARF,
	PLAYER_RACE_CTHULHU,			///< New races
	PLAYER_RACE_MOON_ELF,
	PLAYER_RACE_GHOUL,
	PLAYER_RACE_TIEFLING,
	PLAYER_RACE_TROLL,
	PLAYER_RACE_ORC,
	PLAYER_RACE_OGRE,
	PLAYER_RACE_BASTET,
	PLAYER_RACE_KOBOLD,
	PLAYER_RACE_GAROU,
	PLAYER_RACE_CENTAUR,

	PLAYER_RACE_COUNT
};

enum
{
	PLAYER_GENERE_FEMALE = 0x00,	///< Ladies first :P
	PLAYER_GENERE_MALE,

	PLAYER_GENERE_COUNT
};

enum
{
	PLAYER_SKILL_ = 0x00,

	// todo

	PLAYER_SKILL_COUNT
};

enum
{
	PLAYER_ATTR_ = 0x00,

	// todo

	PLAYER_ATTR_COUNT
};

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct CharacterData;

////////////////////////////////////////////////////////////
// Member Data
////////////////////////////////////////////////////////////
struct PlayerBasicInfo
{
	uint16	Class;
	uint16	Race;
	uint8	Genere;
	uint16	City;
};

struct PlayerStats
{
	uint16	Level;
	uint32	Exp;
	uint32	Gold;

	uint16	Hp[PLAYER_STATS_COUNT];
	uint16	Mana[PLAYER_STATS_COUNT];
	uint16	Stamina[PLAYER_STATS_COUNT];
	uint16	Hunger[PLAYER_STATS_COUNT];
	uint16	Thirst[PLAYER_STATS_COUNT];

	uint16	SkillsFree;
	uint16	Skills[PLAYER_SKILL_COUNT];

	uint8	Attributes[PLAYER_ATTR_COUNT];

	uint16	Spells[PLAYER_SPELLS_MAX_SLOTS];
};

struct PlayerFlags
{
	bool	Dead;
	bool	Poisoned;
	bool	Hidden;
	bool	Trading;
	bool	Running;
	bool	Jumping;
	bool Paralized;
};

struct PlayerObject
{
	uint32	Index;
	uint32	Amount;
	bool	Equipped;
};

struct PlayerInventory
{
	uint32	WeaponEqpIndex;
	uint16	WeaponEqpSlot;

	uint32	ArmorEqpIndex;
	uint16	ArmorEqpSlot;

	uint32	ShieldEqpIndex;
	uint16	ShieldEqpSlot;

	uint32	HelmetEqpIndex;
	uint16	HelmetEqpSlot;

	uint32	FootwearEqpIndex;
	uint16	FootwearEqpSlot;

	uint32	ShipEqpIndex;
	uint16	ShipEqpSlot;

	uint16				Count;
	struct PlayerObject	Objects[PLAYER_INV_MAX_SLOTS];
};

struct PlayerData
{
	uint32 AccountId;

	bool Active;						//< Check if player is ingame
	bool Ban;							//< Check if player is banned or not

	char Name[PLAYER_NAME_MAX_SIZE];
	char Desc[PLAYER_DESC_MAX_SIZE];

	struct PlayerBasicInfo		BasicInfo;
	struct PlayerStats			Stats;
	struct PlayerFlags			Flags;
	struct PlayerInventory		Inventory;

	uint32						World;
	struct Vector3f				Position;
};

////////////////////////////////////////////////////////////
/// Initialize the player
////////////////////////////////////////////////////////////
void PlayerInitialize(struct CharacterData * Character);

////////////////////////////////////////////////////////////
/// Destroy the player
////////////////////////////////////////////////////////////
void PlayerDestroy(struct CharacterData * Character);

////////////////////////////////////////////////////////////
/// Connect the player (load it from database)
////////////////////////////////////////////////////////////
uint32 PlayerLogin(struct CharacterData * Character);

////////////////////////////////////////////////////////////
/// Disconnect the player (store it in database)
////////////////////////////////////////////////////////////
bool PlayerDisconnect(struct CharacterData * Character);

////////////////////////////////////////////////////////////
/// Check if a player is banned in the database
////////////////////////////////////////////////////////////
bool PlayerCheckBan(struct CharacterData * Character);

#endif // GAME_PLAYER_H

