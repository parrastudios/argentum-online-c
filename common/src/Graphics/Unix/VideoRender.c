/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/VideoRender.h>

////////////////////////////////////////////////////////////
// Declarations
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Error handler
////////////////////////////////////////////////////////////
void VideoHandleError(uint32 Result, char * FilePath)
{
	char Error[0xFF];

	switch (Result)
	{

		default :
			strcpy(Error, "Unknown");
			break;
	}

	MessageError("VideoLoad", "Failed to open the AVI frame: %s. Reason: %s.", FilePath, Error);
}

////////////////////////////////////////////////////////////
// Load a video
////////////////////////////////////////////////////////////
void VideoLoad(struct Video * VideoData, char * FilePath)
{

}

////////////////////////////////////////////////////////////
// Render a frame of the video
////////////////////////////////////////////////////////////
void VideoRender(struct Video * VideoData, bool Loop, uint32 X, uint32 Y, uint32 Width, uint32 Height)
{

}

////////////////////////////////////////////////////////////
// Destroy a video
////////////////////////////////////////////////////////////
void VideoDelete(struct Video * VideoData)
{

}

////////////////////////////////////////////////////////////
// Destroy the video library
////////////////////////////////////////////////////////////
void VideoDestroy()
{

}
