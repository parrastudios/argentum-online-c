/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/VertexBuffer.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

struct VertexBuffer * VertexBufferCreate(uint32 VertexSize, struct VertexAttrib * Attribs, uint32 Size, bool Dynamic)
{
	struct VertexBuffer * Buffer = (struct VertexBuffer*)MemoryAllocate(sizeof(struct VertexBuffer));

	if (Buffer)
	{
		Buffer->VertexSize	= VertexSize;
		Buffer->Attribs		= Attribs;
		Buffer->Size		= Size;
		Buffer->Count		= 0;
		Buffer->Data		= (uint8*)MemoryAllocate(Buffer->Size * Buffer->VertexSize);
		Buffer->Dynamic		= Dynamic;
	}

	return Buffer;
}

void VertexBufferDestroy(struct VertexBuffer * Buffer)
{
	if (Buffer)
	{
		if (Buffer->Data)
		{
			MemoryDeallocate(Buffer->Data);
		}

		MemoryDeallocate(Buffer);
	}
}

void VertexBufferAdd(struct VertexBuffer * Buffer, void * Vertex)
{
	if (VertexBufferIsAvailable(Buffer, 1))
	{
		MemoryCopy(Buffer->Data + Buffer->VertexSize * Buffer->Count, Vertex, Buffer->VertexSize);
		++Buffer->Count;
	}
}

uint32 VertexBufferLength(struct VertexBuffer * Buffer)
{
	return Buffer->Count;
}

bool VertexBufferIsAvailable(struct VertexBuffer * Buffer, uint32 Count)
{
	return ((Buffer->Count + Count) <= Buffer->Size);
}
