/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef NETWORK_SOCKET_HELPER_H
#define NETWORK_SOCKET_HELPER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
#	ifndef _WINSOCKAPI_
#		include <winsock2.h>
#		define _WINSOCKAPI_
#	endif

	////////////////////////////////////////////////////////////
	// Define some socket types
	////////////////////////////////////////////////////////////
	typedef SOCKET	SocketType;
	typedef int		SocketLengthType;

#elif PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_MACOS || PLATFORM_TYPE == PLATFORM_BSD

#	include <sys/types.h>
#	include <sys/socket.h>
#	include <netinet/in.h>
#	include <netinet/tcp.h>
#	include <arpa/inet.h>
#	include <netdb.h>
#	include <unistd.h>

    ////////////////////////////////////////////////////////////
    // Define some socket types
    ////////////////////////////////////////////////////////////
    typedef int			SocketType;
    typedef socklen_t	SocketLengthType;

#endif


////////////////////////////////////////////////////////////
/// Enumeration of status returned by socket functions
////////////////////////////////////////////////////////////
enum SocketStatus
{
	Done,         ///< The socket has sent / received the data
	NotReady,     ///< The socket is not ready to send / receive data yet
	Disconnected, ///< The TCP socket has been disconnected
	Error         ///< An unexpected error happened
};

////////////////////////////////////////////////////////////
/// This class defines helper functions to do all the
/// non-portable socket stuff. This class is meant for internal
/// use only
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Windows needs some initialization and cleanup to get
/// sockets working properly..
////////////////////////////////////////////////////////////
void SocketHelperInitialize(); void SocketHelperCleanup();

////////////////////////////////////////////////////////////
/// Return the value of the invalid socket
///
/// \return Unique value of the invalid socket
///
////////////////////////////////////////////////////////////
SocketType InvalidSocket();

////////////////////////////////////////////////////////////
/// Close / destroy a socket
///
/// \param Socket : Socket to close
///
/// \return True on success
///
////////////////////////////////////////////////////////////
bool SocketHelperClose(SocketType Socket);

////////////////////////////////////////////////////////////
/// Set a socket as blocking or non-blocking
///
/// \param Socket : Socket to modify
/// \param Block :  New blocking state of the socket
///
////////////////////////////////////////////////////////////
void SocketHelperSetBlocking(SocketType Socket, bool Block);

////////////////////////////////////////////////////////////
/// Get the last socket error status
///
/// \return Status corresponding to the last socket error
///
////////////////////////////////////////////////////////////
enum SocketStatus SocketHelperGetErrorStatus();

#endif // NETWORK_SOCKET_HELPER_H
