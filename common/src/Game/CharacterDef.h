/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GAME_CHARACTER_DEFINITIONS_H
#define GAME_CHARACTER_DEFINITIONS_H

////////////////////////////////////////////////////////////
// Header constraints
////////////////////////////////////////////////////////////
#ifndef GAME_CHARACTER_H
#	error Game/Character must be included instead
#endif

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <Game/Player.h>
#include <Game/Npc.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define CHARACTER_TYPE_INVALID		0xFFFFFFFF
#define CHARACTER_TYPE_PLAYER		0x00
#define CHARACTER_TYPE_NPC			0x01
#define CHARACTER_TYPE_PLAYER_DB	0x02
#define CHARACTER_TYPE_NPC_DB		0x03

#define CHARACTER_ALGIN_NEUTRAL		0x00
#define CHARACTER_ALGIN_CRIMINAL	0x01
#define CHARACTER_ALGIN_CIVIL		0x02

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct CharacterAparence
{
	uint32 Head;
	uint32 Body;

	uint32 Helmet;
	uint32 Weapon;
	uint32 Shield;

	uint32 Footwear;
};

enum CharacterMovement
{
	MoveNot			= 0x00,
	MoveUp			= 0x01 << 0x00,
	MoveDown		= 0x01 << 0x01,
	MoveLeft		= 0x01 << 0x02,
	MoveRight		= 0x01 << 0x03,
    MoveNear        = 0x01 << 0x04,
    MoveFar         = 0x01 << 0x05,

	MoveUpLeft		= MoveUp | MoveLeft,
	MoveUpRight		= MoveUp | MoveRight,
	MoveDownLeft	= MoveDown | MoveLeft,
	MoveDownRight	= MoveDown | MoveRight,

	// Invalid controls
	MoveUpDown		= MoveUp | MoveDown,
	MoveLeftRight	= MoveRight | MoveLeft,
	MoveNearFar     = MoveNear | MoveFar,
	MoveInvalid		= MoveUpDown | MoveLeftRight | MoveNearFar,

	MoveCount
};


#define CharacterMovementValid(Movement)			((bool)( \
													((Movement & MoveUpDown) == MoveUpDown) || \
													((Movement & MoveLeftRight) == MoveLeftRight) || \
													((Movement & MoveNearFar) == MoveNearFar) || \
													((Movement & MoveInvalid) == MoveInvalid) ))

#define CharacterMovementCheck(Status, Movement)	((bool)(((Status & Movement) == Movement) || ((Status | MoveNot) == MoveNot)))

#define CharacterMovementIs(Status, Movement)		((bool)(Status == Movement))

struct CharacterData
{
	uint32						Index;
	uint32						Algin;
	struct CharacterAparence	Aparence;
	uint32						World;
	uint32						Movement;

	union
	{
		struct PlayerData		Player;
		struct NpcData			Npc;
	} Attributes;
};

#endif // GAME_CHARACTER_DEFINITIONS_H
