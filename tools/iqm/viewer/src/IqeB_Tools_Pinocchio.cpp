/* IqeB_Tools_Pinocchio.cpp

01.10.2014 RR: * First edition of this file
                 Base was the file xxx\DemoUI\processor.cpp of
                 the Pinocchio download from GitHup.
                 Reworked file so Pinocchio is usable from IqeBrowser.


*/

/*
Copyright (c) 2007 Ilya Baran

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <math.h>
#include <string.h>
#include <fstream>

#include "IqeBrowser.h"
#include "../Pinocchio/skeleton.h"
#include "../Pinocchio/utils.h"
#include "../Pinocchio/debugging.h"
#include "../Pinocchio/attachment.h"
#include "../Pinocchio/pinocchioApi.h"

/************************************************************************************
  Argument data and preprocessing for rigging a model
*/

struct ArgData
{
    ArgData() :
        skelScale(1.), noFit(false),
        skeleton(HumanSkeleton())
    {
    }

    Quaternion<> meshTransform;
    double skelScale;
    bool noFit;
    Skeleton skeleton;
    string skeletonname;
};

static ArgData processArgs( char *pArg_Skeleton,
                            char *pArg_Rotation,
                            char *pArg_Scale,
                            bool Arg_noFit
                          )
{
    ArgData out;
    char *curStr;

    // Skeleton

    curStr = pArg_Skeleton;
    if( strlen( curStr) > 0) {

      if(curStr == string("human"))
          out.skeleton = HumanSkeleton();
      else if(curStr == string("horse"))
          out.skeleton = HorseSkeleton();
      else if(curStr == string("quad"))
         out.skeleton = QuadSkeleton();
      else if(curStr == string("centaur"))
          out.skeleton = CentaurSkeleton();
      else
          out.skeleton = FileSkeleton(curStr);
      out.skeletonname = curStr;
    }

    // Rotation
    curStr = pArg_Rotation;
    if( strlen( curStr) > 0) {

      double x, y, z, deg;
      sscanf( curStr, "%lf %lf %lf %lf", &x, &y, &z, &deg);

      out.meshTransform = Quaternion<>(Vector3(x, y, z), deg * M_PI / 180.) * out.meshTransform;
    }

    // Scale
    curStr = pArg_Scale;
    if( strlen( curStr) > 0) {

      sscanf( curStr, "%lf", &out.skelScale);
    }

    // other arguments

    out.noFit            = Arg_noFit;

    return out;
}

/************************************************************************************
  Convert IQE Model to mesh usable for pinocchio library
*/

static Mesh *IqeModelToPinocchioMesh( IQE_model *pModel, char *pErrorString)
{
  Mesh *pMesh;
  int i, j, a[ 3], first;

  // create empty meht

  pMesh = new Mesh();

  // convert model to mesh

	for( i = 0; i < pModel->mesh->vertex_count; i++) {

    double x, y, z;

    x = pModel->mesh->position[ i * 3 + 0];
    y = pModel->mesh->position[ i * 3 + 1];
    z = pModel->mesh->position[ i * 3 + 2];

    pMesh->vertices.resize( pMesh->vertices.size() + 1);
    pMesh->vertices.back().pos = Vector3( x, y, z);
  }

	for (i = 0; i < pModel->mesh->part_count; i++) {

    for( j = 0; j <pModel->mesh->part[i].count; j += 3) {

      a[ 0] = pModel->mesh->element[ pModel->mesh->part[ i].first + j + 0];
      a[ 1] = pModel->mesh->element[ pModel->mesh->part[ i].first + j + 1];
      a[ 2] = pModel->mesh->element[ pModel->mesh->part[ i].first + j + 2];

      first = pMesh->edges.size();
      pMesh->edges.resize( pMesh->edges.size() + 3);

      //indices in file are 0-based
      pMesh->edges[first + 0].vertex = a[0];
      pMesh->edges[first + 1].vertex = a[1];
      pMesh->edges[first + 2].vertex = a[2];
    }
	}

  //reconstruct the rest of the information

  int verts = pMesh->vertices.size();

  if( verts == 0) {

    pMesh->vertices.clear();
    pMesh->edges.clear();
    delete pMesh;
    return NULL;
  }

  for(i = 0; i < (int)pMesh->edges.size(); ++i) { //make sure all vertex indices are valid

    if( pMesh->edges[i].vertex < 0 || pMesh->edges[i].vertex >= verts) {

      //Debugging::out() << "Error: invalid vertex index " << edges[i].vertex << endl;

      pMesh->vertices.clear();
      pMesh->edges.clear();
      delete pMesh;
      return NULL;
    }
  }

  pMesh->fixDupFaces();

  pMesh->computeTopology( pErrorString);

#ifdef use_again
  if( pMesh->integrityCheck()) {
        Debugging::out() << "Successfully read " << file << ": " << vertices.size() << " vertices, " << edges.size() << " edges" << endl;
  } else {
        Debugging::out() << "Somehow read " << file << ": " << vertices.size() << " vertices, " << edges.size() << " edges" << endl;
  }
#endif

  pMesh->computeVertexNormals();

  return( pMesh);
}

/************************************************************************************
  Autorigging a IEQ model to the pinocchio autorigging library.
*/

int IqeB_ProcessForPinocchio( IQE_model *pModel,
                              char *pArg_Skeleton,
                              char *pArg_Rotation,
                              char *pArg_Scale,
                              bool Arg_noFit,
                              char *pErrorString
                            )
{
    Mesh *pMesh = NULL;
    int i, j;

    ArgData a = processArgs( pArg_Skeleton, pArg_Rotation, pArg_Scale,
                             Arg_noFit);

    Debugging::setOutStream(cout);

#ifdef use_again

    // Convert model to mesh
    pMesh = IqeModelToPinocchioMesh( pModel, pErrorString);

#else

    // ...

    T_VoxelModelData VoxelData;

#ifdef _DEBUG
    int VoxelNormSize =  64;   // for debug, less processing time
#else
    int VoxelNormSize = 150;
#endif

    // Convert model to voxels
    IqeB_VoxelFromIqeModel( pModel, &VoxelData, VoxelNormSize, pErrorString);

    if( VoxelData.pVoxels == NULL) {    // Have no voxel data

      // NOTE: pErrorString is set in case of error

      return( -1);
    }

    // Convert voxels to IQE model

    IQE_model *pModelIqeTemp;

#ifndef use_again
    pModelIqeTemp = IqeB_VoxelToIqeMarchingCube( &VoxelData, pErrorString);
#else
    pModelIqeTemp = IqeB_VoxelToIqeCubes( &VoxelData, pErrorString);
#endif

    free( VoxelData.pVoxels);       // free the voxel data allocated

    if( pModelIqeTemp == NULL) {    // Have no model converted

      // NOTE: pErrorString is set in case of error

      return( -1);
    }

    // Lower amound of vertices and faces

#ifndef use_again
    if( pModelIqeTemp != NULL) {                 // have a mesh

      // Lower number of vertices, otherwise the autorig() functions will make a stack overflow

      IqeB_MeshSmooth( pModelIqeTemp->mesh, 0.5, 0.0, 0.0);

      //x/IqeB_MeshTriangleReduction( pModelIqeTemp->mesh, 0, 2500, 0);
      IqeB_Mesh_VCGLib_Tridecimator( pModelIqeTemp->mesh, 0, 2500, 0);
    }
#endif

#ifndef use_again
    // free the data of the old model
    IqeB_ModelFreeData( &pModel, IQE_MODEL_FREE_SKELETON | IQE_MODEL_FREE_ANIMATION | IQE_MODEL_FREE_MESH);

    // copy over the new model
    memcpy( pModel, pModelIqeTemp, sizeof( struct IQE_model));

    free( pModelIqeTemp);                                  // free the data of pModelIqeTemp

    pMesh = IqeModelToPinocchioMesh( pModel, pErrorString);

    //x/return( 0);
#else

    pMesh = IqeModelToPinocchioMesh( pModelIqeTemp, pErrorString);

    free( pModelIqeTemp);                                  // free the data of pModelIqeTemp
#endif
#endif

    if( pMesh == NULL) {

      cout << "Error reading file.  Aborting." << endl;

      return( -1);
    }

    if( pMesh->vertices.size() == 0) {

      cout << "Error reading file.  Aborting." << endl;

      delete pMesh;
      return( -1);
    }

    for( i = 0; i < (int)pMesh->vertices.size(); ++i) {
      pMesh->vertices[i].pos = a.meshTransform * pMesh->vertices[i].pos;
    }

    pMesh->normalizeBoundingBox();
    pMesh->computeVertexNormals();

    Skeleton given = a.skeleton;
    given.scale(a.skelScale * 0.7);

    PinocchioOutput o;

#ifndef use_again
    if(!a.noFit) { //do everything
        o = autorig(given, *pMesh, pErrorString);
    }
    else { //skip the fitting step--assume the skeleton is already correct for the mesh
        TreeType *distanceField = constructDistanceField( *pMesh);
        VisTester<TreeType> *tester = new VisTester<TreeType>(distanceField);

        o.embedding = a.skeleton.fGraph().verts;
        for(i = 0; i < (int)o.embedding.size(); ++i) {
            o.embedding[i] = pMesh->toAdd + o.embedding[i] * pMesh->scale;
        }

        o.attachment = new Attachment( *pMesh, a.skeleton, o.embedding, tester);

        delete tester;
        delete distanceField;
    }
#else

    // add skeleton only

    o.embedding = a.skeleton.fGraph().verts;
    for(i = 0; i < (int)o.embedding.size(); ++i) {

      o.embedding[i] = pMesh->toAdd + o.embedding[i] * pMesh->scale;
    }
#endif

    if(o.embedding.size() == 0) {
        cout << "Error embedding" << endl;

      delete pMesh;
      return( -2);
    }

#ifdef use_again
    //output skeleton embedding
    for(i = 0; i < (int)o.embedding.size(); ++i)
        o.embedding[i] = (o.embedding[i] - m.toAdd) / m.scale;
    ofstream os("skeleton.out");
    for(i = 0; i < (int)o.embedding.size(); ++i) {
        os << i << " " << o.embedding[i][0] << " " << o.embedding[i][1] <<
                   " " << o.embedding[i][2] << " " << a.skeleton.fPrev()[i] << endl;
    }

    //output attachment
    std::ofstream astrm("attachment.out");
    for(i = 0; i < (int)m.vertices.size(); ++i) {
        Vector<double, -1> v = o.attachment->getWeights(i);
        for(int j = 0; j < v.size(); ++j) {
            double d = floor(0.5 + v[j] * 10000.) / 10000.;
            astrm << d << " ";
        }
        astrm << endl;
    }

#endif

    //
    // Copy skeleton to model
    //

    // modify skeleton points

    for(i = 0; i < (int)o.embedding.size(); ++i) {

      o.embedding[i] = (o.embedding[i] - pMesh->toAdd) / pMesh->scale;
    }

	  // free animations

    struct anim *anim;

    for( i = 0; i < pModel->anim_count; i++) {

	    anim = pModel->anim_data[ i];

      free( anim->name);

      if( anim->data) {

        for( j = 0; j < anim->len; j++) {

          free( anim->data[ j]);
        }

        free( anim->data);
      }

      free( anim);
	  }

    if( pModel->anim_data != NULL) {

      free( pModel->anim_data);
    }

	  pModel->anim_count = 0;               // reset number of animations
	  pModel->anim_cap   = 0;               // reset number of allocated animation pointers
	  pModel->anim_poses = 0;               // reset number of poses in an animation frame
	  pModel->anim_data  = NULL;            // reset

	  // free skeleton thinks

	  for( i = 0; i < pModel->skel->joint_count; i++) {

      free( pModel->skel->j[ i].name);
	  }

	  // add skeleton

	  memset( pModel->skel, 0, sizeof( struct skel));

	  pModel->skel->joint_count = (int)o.embedding.size();    // Number of joints

	  if( pModel->skel->joint_count > MAXBONE) {  // security test

      pModel->skel->joint_count = MAXBONE;
	  }

    for( i = 0; i < pModel->skel->joint_count; i++) {

      int iParent;

      pModel->skel->j[ i].name = strdup( a.skeleton.JNnameOut()[ i].c_str());

      iParent = a.skeleton.fPrev()[i];         // index of parent
      pModel->skel->j[ i].parent = iParent;

      if( iParent < 0 || iParent >= pModel->skel->joint_count) {  // has no parent or out of range

        pModel->skel->pose[ i].location[ 0] = o.embedding[i][0];
        pModel->skel->pose[ i].location[ 1] = o.embedding[i][1];
        pModel->skel->pose[ i].location[ 2] = o.embedding[i][2];
      } else {

        pModel->skel->pose[ i].location[ 0] = o.embedding[i][0] - o.embedding[iParent][0];
        pModel->skel->pose[ i].location[ 1] = o.embedding[i][1] - o.embedding[iParent][1];
        pModel->skel->pose[ i].location[ 2] = o.embedding[i][2] - o.embedding[iParent][2];
      }

      vec_set( pModel->skel->pose[ i].scale, 1.0, 1.0, 1.0);  // default scale
      quat_set_identity( pModel->skel->pose[ i].rotation);    // default rotation
    }

    IqeB_ModelSkeletonMatrixCalc( pModel);         // update skeleton matrix
    IqeB_AnimPrepareAfterLoad( pModel);            // Prepare animations


    //
    // add vertex blend things
    //

    allocArray( (void **)&pModel->mesh->blendindex,  pModel->mesh->vertex_count * 4, sizeof( int));
    allocArray( (void **)&pModel->mesh->blendweight, pModel->mesh->vertex_count * 4, sizeof( float));

    if( pModel->mesh->blendindex == NULL ||
        pModel->mesh->blendweight == NULL ||
        pModel->skel->joint_count <= 0) {   // security test

      // not enough data, something not OK
      // free already allocated memory

      allocArray( (void **)&pModel->mesh->blendindex,  0, 0);
      allocArray( (void **)&pModel->mesh->blendweight, 0, 0);

    } else {

      int *bi;
      float *bw;
      int    BestIndex0,  BestIndex1,  BestIndex2,  BestIndex3;
      double BestWeight0, BestWeight1, BestWeight2, BestWeight3, SumWeight;

      // inital clear blending data

      memset( pModel->mesh->blendindex,  0, pModel->mesh->vertex_count * 4 * sizeof( int));
      memset( pModel->mesh->blendweight, 0, pModel->mesh->vertex_count * 4 * sizeof( float));

      // work pointer throgh blending data
      bi = pModel->mesh->blendindex;
      bw = pModel->mesh->blendweight;

	    for( i = 0; i < pModel->mesh->vertex_count; i++) {

        Vector<double, -1> v = o.attachment->getWeights(i);

        // get four biggest weights

        BestIndex0  = BestIndex1  = BestIndex2  = BestIndex3  = -1;
        BestWeight0 = BestWeight1 = BestWeight2 = BestWeight3 = 0.0;

        // Weight0
        for( j = 0; j < v.size(); j++) {

          if( BestIndex0 < 0 || v[ j] > BestWeight0) {

            BestIndex0  = j;
            BestWeight0 = v[ j];
          }
        }

        // Weight1
        for( j = 0; j < v.size(); j++) {

          if( j != BestIndex0 &&
              (BestIndex1 < 0 || v[ j] > BestWeight1)) {

            BestIndex1  = j;
            BestWeight1 = v[ j];
          }
        }

        // Weight2
        for( j = 0; j < v.size(); j++) {

          if( j != BestIndex0 && j != BestIndex1 &&
              (BestIndex2 < 0 || v[ j] > BestWeight2)) {

            BestIndex2  = j;
            BestWeight2 = v[ j];
          }
        }

        // Weight3
        for( j = 0; j < v.size(); j++) {

          if( j != BestIndex0 && j != BestIndex1 && j != BestIndex2 &&
              (BestIndex3 < 0 || v[ j] > BestWeight3)) {

            BestIndex3  = j;
            BestWeight3 = v[ j];
          }
        }

        // normalize sum of weights to 1.0

        SumWeight = BestWeight0 + BestWeight1 + BestWeight2 + BestWeight3;
        if( SumWeight < 0.00001) {  // enshure some minimum

          SumWeight = 0.00001;
        }

        BestWeight0 = BestWeight0 / SumWeight;
        BestWeight1 = BestWeight1 / SumWeight;
        BestWeight2 = BestWeight2 / SumWeight;
        BestWeight3 = BestWeight3 / SumWeight;

        // clipping weights against minimum reasonable value for .iqm files

        if( BestWeight1 < 1.0 / 255.0) {

          BestIndex1  = 0;
          BestWeight1 = 0.0;
        }

        if( BestWeight2 < 1.0 / 255.0) {

          BestIndex2  = 0;
          BestWeight2 = 0.0;
        }

        if( BestWeight3 < 1.0 / 255.0) {

          BestIndex3  = 0;
          BestWeight3 = 0.0;
        }

        // again, normalize sum of weights to 1.0, some may have been clipped

        SumWeight = BestWeight0 + BestWeight1 + BestWeight2 + BestWeight3;
        if( SumWeight < 0.00001) {  // enshure some minimum

          SumWeight = 0.00001;
        }

        BestWeight0 = BestWeight0 / SumWeight;
        BestWeight1 = BestWeight1 / SumWeight;
        BestWeight2 = BestWeight2 / SumWeight;
        BestWeight3 = BestWeight3 / SumWeight;

        // store blending data

        bi[ 0] = BestIndex0;
        bi[ 1] = BestIndex1;
        bi[ 2] = BestIndex2;
        bi[ 3] = BestIndex3;

        bw[ 0] = BestWeight0;
        bw[ 1] = BestWeight1;
        bw[ 2] = BestWeight2;
        bw[ 3] = BestWeight3;

        // ...

        bi += 4;
        bw += 4;
      }
    }

    // finish up

    delete o.attachment;

    delete pMesh;

    return( 0);
}

/*********************************** End Of File ***********************************/
