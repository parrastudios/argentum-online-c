/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Audio/Sound.h>
#include <Audio/OpenALHelper.h>

////////////////////////////////////////////////////////////
/// Start or resume playing the sound
////////////////////////////////////////////////////////////
void SoundPlay(struct Sound * SoundData)
{
	if (AudioIsAvailable() && SoundData)
	{
		ALCheck(alSourcePlay(SoundData->Source));
	}
}

////////////////////////////////////////////////////////////
/// Pause the sound
////////////////////////////////////////////////////////////
void SoundPause(struct Sound * SoundData)
{
	if (AudioIsAvailable() && SoundData)
	{
		ALCheck(alSourcePause(SoundData->Source));
	}
}

////////////////////////////////////////////////////////////
/// Stop playing the sound
////////////////////////////////////////////////////////////
void SoundStop(struct Sound * SoundData)
{
	if (AudioIsAvailable() && SoundData)
	{
		ALCheck(alSourceStop(SoundData->Source));
	}
}

////////////////////////////////////////////////////////////
/// Set whether or not the sound should loop after reaching the end
////////////////////////////////////////////////////////////
void SoundLoop(struct Sound * SoundData, bool Loop)
{
	if (AudioIsAvailable() && SoundData)
	{
		// Set loop
		ALCheck(alSourcei(SoundData->Source, AL_LOOPING, Loop));
	}
}

////////////////////////////////////////////////////////////
/// Set the pitch of the sound
////////////////////////////////////////////////////////////
void SoundPitch(struct Sound * SoundData, float Pitch)
{
	if (AudioIsAvailable() && SoundData)
	{
		// Set frequency
		ALCheck(alSourcef(SoundData->Source, AL_PITCH, Pitch));
	}
}

////////////////////////////////////////////////////////////
/// Set the volume of the sound
////////////////////////////////////////////////////////////
void SoundVolume(struct Sound * SoundData, float Volume)
{
	if (AudioIsAvailable() && SoundData)
	{
		// Set volume
		ALCheck(alSourcef(SoundData->Source, AL_GAIN, Volume * 0.01f));
	}
}

////////////////////////////////////////////////////////////
/// Set the 3D position of the sound in the audio scene
////////////////////////////////////////////////////////////
void SoundPosition(struct Sound * SoundData, float x, float y, float z)
{
	if (AudioIsAvailable() && SoundData)
	{
		// Set 3D sound
		ALCheck(alSource3f(SoundData->Source, AL_POSITION, x, y, z));
	}
}

////////////////////////////////////////////////////////////
/// Make the sound's position relative to the listener or absolute
////////////////////////////////////////////////////////////
void SoundRelativeToListener(struct Sound * SoundData, bool Relative)
{
	if (AudioIsAvailable() && SoundData)
	{
		// Relative sound or absolute
		ALCheck(alSourcei(SoundData->Source, AL_SOURCE_RELATIVE, Relative));
	}
}

////////////////////////////////////////////////////////////
/// Set the minimum distance of the sound
////////////////////////////////////////////////////////////
void SoundMinDistance(struct Sound * SoundData, float Distance)
{
	if (AudioIsAvailable() && SoundData)
	{
		ALCheck(alSourcef(SoundData->Source, AL_REFERENCE_DISTANCE, Distance));
	}
}

////////////////////////////////////////////////////////////
/// Set the attenuation factor of the sound
////////////////////////////////////////////////////////////
void SoundAttenuation(struct Sound * SoundData, float Attenuation)
{
	if (AudioIsAvailable() && SoundData)
	{
		ALCheck(alSourcef(SoundData->Source, AL_ROLLOFF_FACTOR, Attenuation));
	}
}

////////////////////////////////////////////////////////////
/// Get the current status of the sound (stopped, paused, playing)
////////////////////////////////////////////////////////////
enum SoundState SoundStatus(struct Sound * SoundData)
{
	if (AudioIsAvailable() && SoundData)
	{
		ALint Status;
		ALCheck(alGetSourcei(SoundData->Source, AL_SOURCE_STATE, &Status));

		switch (Status)
		{
			case AL_INITIAL :
			case AL_STOPPED : return Stopped;
			case AL_PAUSED :  return Paused;
			case AL_PLAYING : return Playing;
		}
	}

    return Stopped;
}
