/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_COMPONENT_H
#define WORLD_COMPONENT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/Vector.h>
#include <DataType/List.h>

#include <World/Build.h>
#include <World/EntityIndex.h>

#if WORLD_BUILD_TYPE_IS(WORLD_BUILD_TYPE_BASE)
#	include <World/EntityBase.h>
#endif

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define COMPONENT_DATA_LENGTH_DEFAULT				0x00
#define COMPONENT_DATA_MASK_EMPTY					UINTEGER_MIN_RANGE

#define COMPONENT_DATA_BASE							0x00
#define COMPONENT_DATA_TRANSFORM					0x01
#define COMPONENT_DATA_COLLISION					0x02
#define COMPONENT_DATA_COLLISION_BOX				0x03
#define COMPONENT_DATA_COLLISION_SPHERE				0x04
#define COMPONENT_DATA_COLLISION_FRUSTUM			0x05
#define COMPONENT_DATA_ANIMATION					0x06
#define COMPONENT_DATA_PHYSICS						0x07
#define COMPONENT_DATA_RENDER						0x08
#define COMPONENT_DATA_OCCLUSION					0x09
#define COMPONENT_DATA_SOUND						0x0A

#define COMPONENT_DATA_SIZE							0x0B

#define COMPONENT_TYPE_BASE							0x00
#define COMPONENT_TYPE_TRANSFORM					0x01
#define COMPONENT_TYPE_COLLISION					0x02
#define COMPONENT_TYPE_ANIMATION					0x03
#define COMPONENT_TYPE_PHYSICS						0x04
#define COMPONENT_TYPE_RENDER						0x05
#define COMPONENT_TYPE_OCCLUSION					0x06
#define COMPONENT_TYPE_SOUND						0x07

#define COMPONENT_TYPE_SIZE							0x08

#define COMPONENT_ID_INVALID	UINT32_MAX_RANGE	//< Component invalid id for holding free ids in the table

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct EntityType;

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef uinteger					ComponentIdType;	//< Component id type for indexing

typedef struct ComponentTypesImplElement
{
	const struct ComponentImplType * (*Impl)();		//< Component type implementation
} ComponentTypesImpl[COMPONENT_DATA_SIZE];

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Component data implementations and interfaces
///
///	The [Impl] attribute is used to make the derivation of
///	components, so by this way a component can be derivded
///	although it is being treated as if was of the same type
///	as parent.
///
///	[Impl] is the first byte of the derived structure (that
///	is allocated contigously in memory if there), so by this
///	way we can define three kinds of components:
///
///	1) Basic component, with no derivation (it doesn't have
///		the attribute [Impl] so it cannot be derived).
///
///	2) Pure interface, with no base implementation (it only
///		has a one attributed that is [Impl], this is just
///		a wrapper to make explicit that the component is of
///		that type).
///
///	3) Derived component, with base implementation (the base
///		component and the derived component have different
///		attributes, so we can access to base attributes o to
///		derived attributes, just casting the structure or
///		doing the OffsetOf trick if we want to access from
///		derived to the base).
////////////////////////////////////////////////////////////
union ComponentAttributesData
{
	struct ComponentBaseData *			Base;		//< Base component data interface
	struct ComponentTransformData *		Transform;	//< Transform component data
	struct ComponentCollisionData *		Collision;	//< Collision component data interface
	struct ComponentAnimationData *		Animation;	//< Animation component data interface
	struct ComponentPhysicsData *		Physics;	//< Physics component data
	struct ComponentRenderData *		Render;		//< Render component data interface
	struct ComponentOcclusionData *		Occlusion;	//< Occlusion component data interface
	struct ComponentSoundData *			Sound;		//< Sound component data
};

////////////////////////////////////////////////////////////
/// Component type interfaces (callbacks or handlers)
///
///	These are the function virtual types. There are some
///	declared as base, for components that are basic, and
///	don't have derivation.
///
///	In case of derived components, these functions can be
///	override just passing a new type implementation.
////////////////////////////////////////////////////////////
union ComponentAttributesType
{
	const void *							(*Derived)();	//< Custom type implementation for extending the component virtual table
	const struct ComponentBaseType *		(*Base)();		//< Pointer to parent component type (in case of base type only)
	const struct ComponentTransformType *	(*Transform)();	//< Transform component type
	const struct ComponentCollisionType *	(*Collision)();	//< Collision component type
	const struct ComponentAnimationType *	(*Animation)();	//< Animation component type
	const struct ComponentPhysicsType *		(*Physics)();	//< Physics component type
	const struct ComponentRenderType *		(*Render)();	//< Render component type
	const struct ComponentOcclusionType *	(*Occlusion)();	//< Occlusion component type
	const struct ComponentSoundType *		(*Sound)();		//< Sound component type
};

////////////////////////////////////////////////////////////
/// Component data implementation structure
////////////////////////////////////////////////////////////
struct ComponentImplData
{
	uinteger							Type;			//< Component type id
	uinteger							Size;			//< Component data size
	union ComponentAttributesData *		Attributes;		//< Implementation components
};

////////////////////////////////////////////////////////////
/// Component type implementation structure
////////////////////////////////////////////////////////////
struct ComponentImplType
{
	struct ComponentBaseType			Base;			//< Base component type
	uinteger							Type;			//< Component type id
	union ComponentAttributesType		Attributes;		//< Implementation components
};

////////////////////////////////////////////////////////////
/// Component implementation structure
////////////////////////////////////////////////////////////
struct ComponentType
{
	ComponentIdType						Index;		    //< Index if need in managed way
	uinteger                            EntityFactory;  //< Entity factory type which belongs to
	EntityIdType					    EntityId;	    //< Entity which component is attached
	struct ComponentImplData			DataImpl;	    //< Component data implementation
	const struct ComponentImplType *	TypeImpl;	    //< Component type implementation
	const char *						Tag;		    //< Component type tag
	bool								Managed;	    //< Check if data is managed externally or not
};

////////////////////////////////////////////////////////////
/// Component pool structure
////////////////////////////////////////////////////////////
struct ComponentPoolType
{
	uinteger			DataType;						//< Current component data type
	Vector				Components;						//< Vector holding contigously all components
	Vector				Table;							//< Table mapping static ids to dynamic ids (elements of component vector)
	List				FreeIds;						//< List holding free static ids of the index table
};

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Size macro definition for static definitions
////////////////////////////////////////////////////////////
#define ComponentSize(DataType, DerivedSize) \
	(sizeof(struct ComponentType) + ComponentDataSize[DataType] + DerivedSize)

////////////////////////////////////////////////////////////
/// Obtains the implementation component
////////////////////////////////////////////////////////////
#define ComponentTypeDecl(Name)				struct Component ## Name ## Type

////////////////////////////////////////////////////////////
/// Obtains the base component data
////////////////////////////////////////////////////////////
#define ComponentDataImplBase(Component, DataType) ((DataType *)(ComponentGetAttributes(Component)))

////////////////////////////////////////////////////////////
/// Obtains the pointer to the data implementation
////////////////////////////////////////////////////////////
#define ComponentGetDataImpl(Component) (union ComponentAttributesData *) (((uint8 *)(Component)) + sizeof(struct ComponentType))

////////////////////////////////////////////////////////////
/// Obtains the derived component data (contiguous allocation)
////////////////////////////////////////////////////////////
#define ComponentDataImplDerived(Component, BaseDataType, DerivedDataType) \
	((DerivedDataType *)(&(ComponentDataImplBase(Component, BaseDataType)->Impl[0])))

////////////////////////////////////////////////////////////
/// Declare easy access for component type implementations
////////////////////////////////////////////////////////////
#define ComponentTypeImplDecl(Component, Name)			((ComponentTypeDecl(Name) *)((Component)->TypeImpl->Attributes.Name()))

#define ComponentTypeBase(Component)					ComponentTypeImplDecl(Component, Base)
#define ComponentTypeTransform(Component)				ComponentTypeImplDecl(Component, Transform)
#define ComponentTypeCollision(Component)				ComponentTypeImplDecl(Component, Collision)
#define ComponentTypeAnimation(Component)				ComponentTypeImplDecl(Component, Animation)
#define ComponentTypePhysics(Component)					ComponentTypeImplDecl(Component, Physics)
#define ComponentTypeRender(Component)					ComponentTypeImplDecl(Component, Render)
#define ComponentTypeOcclusion(Component)				ComponentTypeImplDecl(Component, Occlusion)
#define ComponentTypeSound(Component)					ComponentTypeImplDecl(Component, Sound)

#define ComponentTypeDerived(Component, Type)			((Type *)((Component)->TypeImpl->Attributes.Derived()))

////////////////////////////////////////////////////////////
/// Declare easy access for collision type implementations
////////////////////////////////////////////////////////////
#define ComponentTypeCollisionDecl(Name)                struct ComponentCollision ## Name ## Type

#define ComponentTypeCollisionImplDecl(Component, Name)	((ComponentTypeCollisionDecl(Name) *)((Component)->TypeImpl->Attributes.Collision()->TypeImpl.Name))

#define ComponentTypeCollisionBox(Component)			ComponentTypeCollisionImplDecl(Component, Box)
#define ComponentTypeCollisionSphere(Component)			ComponentTypeCollisionImplDecl(Component, Sphere)
#define ComponentTypeCollisionFrustum(Component)		ComponentTypeCollisionImplDecl(Component, Frustum)

#define ComponentTypeCollisionDerived(Component, Type)	((Type *)((Component)->TypeImpl->Attributes.Collision()->TypeImpl.Derived))

////////////////////////////////////////////////////////////
/// Declare easy access for render type implementations
////////////////////////////////////////////////////////////
#define ComponentTypeRenderDerived(Component, Type)     ((Type *)((Component)->TypeImpl->Attributes.Render()->TypeImpl))

////////////////////////////////////////////////////////////
/// Declare easy access for component data interfaces
///
///	By default, size of interfaces (pure virtual) are 1 byte,
///	the size of inheritable components, if there is no
///	derived data equals to (sizeof(ParentComponentDataType) - 1)
///	if derived data (sizeof(ParentComponentDataType) +
///	sizeof(DerivedComponentDataType) - 1)
///
///	Usage {
///
///	// With generic base component :
///
///	#define ComponentDataBaseTerrain(Component)	\
///		(struct ComponentBaseTerrainData *) \
///			ComponentDataBase(Component)
///
///	struct ComponentBaseTerrainData * TerrainBaseData =
///		ComponentDataBaseTerrain(Component);
///
///	// With a normal component (not inheritable) :
///
///	struct ComponentTransformData * TransformData =
///		ComponentDataTransform(Component);
///
///	// With a pure virtual component (it can be casted,
///	//  accessed directly or by using implementation macros) :
///
///	struct ComponentCollisionBoxData * CollisionBoxData =
///		(struct ComponentCollisionData *)
///		ComponentDataCollision(Component); // cast
///
///	struct ComponentCollisionBoxData * CollisionBoxData =
///		(struct ComponentCollisionData *) // optional cast (derivation)
///		Component->DataImpl.Attributes->Collision; // direct access
///
///	struct ComponentCollisionBoxData * CollisionBoxData =
///		ComponentDataCollisionBox(Component); // macro
///
///	// Mix (component inheritance) :
///	//
///	//	+---------+		 +-------------------+
///	//	| Physics | <--- | PhysicsKinematics |
///	//	+---------+		 +-------------------+
///
///	#define ComponentDataPhysicsKinematics(Component) ComponentDataImplDecl(Component, Physics, Kinematics)
///
///	// Same component (memory is contiguous) but different inherit attributes :
///
///	struct ComponentPhysicsData * KinematicsBaseData = ComponentDataPhysics(Component);
///	struct ComponentPhysicsKinematicsData * KinematicsData = ComponentDataPhysicsKinematics(Component);
///
/// }
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Declare easy access for component data
////////////////////////////////////////////////////////////
#define ComponentDataTypeDecl(Name)					    struct Component ## Name ## Data

#define ComponentDataDecl(Component, Name)			    ComponentDataImplBase(Component, ComponentDataTypeDecl(Name))

#	define ComponentDataBase(Component)					ComponentDataDecl(Component, Base)
#	define ComponentDataTransform(Component)			ComponentDataDecl(Component, Transform)
#	define ComponentDataCollision(Component)			ComponentDataDecl(Component, Collision)
#	define ComponentDataAnimation(Component)			ComponentDataDecl(Component, Animation)
#	define ComponentDataPhysics(Component)				ComponentDataDecl(Component, Physics)
#	define ComponentDataRender(Component)				ComponentDataDecl(Component, Render)
#   define ComponentDataOcclusion(Component)			ComponentDataDecl(Component, Occlusion)
#	define ComponentDataSound(Component)				ComponentDataDecl(Component, Sound)

#	define ComponentDataDerived(Component, Derived)		(Derived *)ComponentGetDataImpl(Component)

////////////////////////////////////////////////////////////
/// Declare easy access for component data implementations
////////////////////////////////////////////////////////////
#define ComponentDerivedDataTypeDecl(Name, Derived)     struct Component ## Name ## Derived ## Data

#define ComponentDataImplDecl(Component, Name, Derived)	\
                            ComponentDataImplDerived(   (Component), \
                                                        ComponentDataTypeDecl(Name), \
                                                        ComponentDerivedDataTypeDecl(Name, Derived) \
                                                    )

#define ComponentDataCollisionBox(Component)		    ComponentDataImplDecl(Component, Collision, Box)
#define ComponentDataCollisionSphere(Component)			ComponentDataImplDecl(Component, Collision, Sphere)
#define ComponentDataCollisionFrustum(Component)	    ComponentDataImplDecl(Component, Collision, Frustum)

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Map between component data type id and its type id
////////////////////////////////////////////////////////////
uinteger ComponentDataInfoType(uinteger DataType);

////////////////////////////////////////////////////////////
///  Map between component type and its tag
////////////////////////////////////////////////////////////
const char * ComponentTag(uinteger TypeId);

////////////////////////////////////////////////////////////
/// Definition of interface base (null type implementation)
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentBaseImpl();

////////////////////////////////////////////////////////////
/// Create a component (optional with a given type impl)
///
///	If no [DataImpl] defined, the component is created
///	with no data attached to it.
///
///	If there is [DataImpl] defined, but not has allocated
///	data for implementation, data is preallocated automatically
///	with no support for derived data (review this, or just
///	adopt this constrait).
///
///	If [DataImpl] defined and allocation done (this means
///	that management will be done externally), then only
///	copy the pointer to allocated data. External allocation
///	and deallocation can also be done dynamically or statically.
///	In case of dynamically, component data must be
///	deallocated explicitly when calling to the component
///	data destructor. Otherwise it can be deallocated after
///	destruction of the component (by this way, deallocation
///	can be performed in block).
///
///
///	If no [TypeImpl] defined, the system will use the
///	predefined callbacks associated to the data.
///	Otherwise it will override the callbacks with the
///	new implementation.
///
///	Overridden functions can call parent functions if
///	needed, by using the same strategy as if [Type Impl]
///	was not defined.
////////////////////////////////////////////////////////////
struct ComponentType * ComponentCreate(	struct EntityType * Entity,
										struct ComponentImplData * DataImpl,
										struct ComponentImplType * TypeImpl);


////////////////////////////////////////////////////////////
/// Initialize a component previously created
////////////////////////////////////////////////////////////
struct ComponentType * ComponentInitialize(	struct ComponentType * Component,
											struct EntityType * Entity,
											struct ComponentImplData * DataImpl,
											struct ComponentImplType * TypeImpl);

////////////////////////////////////////////////////////////
/// Create type of component with data previously initialized
////////////////////////////////////////////////////////////
struct ComponentType * ComponentCreateType(	struct ComponentType * Component,
											struct EntityType * Entity,
											struct ComponentImplType * TypeImpl);

////////////////////////////////////////////////////////////
/// Get the size of a component including its data block
////////////////////////////////////////////////////////////
uinteger ComponentGetSize(uinteger DataType, uinteger DerivedSize);

////////////////////////////////////////////////////////////
/// Get attributes of a component as a generic pointer
////////////////////////////////////////////////////////////
union ComponentAttributesData * ComponentGetAttributes(struct ComponentType * Component);

////////////////////////////////////////////////////////////
/// Destroy a component
///
///	Deallocation of data can be done automatically, by
///	means of the destructor of the component (dynamically)
///	or after destruction of all components (statically).
////////////////////////////////////////////////////////////
void ComponentDestroy(struct ComponentType * Component);

#endif // WORLD_COMPONENT_H
