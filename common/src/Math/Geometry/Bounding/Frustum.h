/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef MATH_GEOMETRY_BOUNDINGVOLUME_FRUSTUM_H
#define MATH_GEOMETRY_BOUNDINGVOLUME_FRUSTUM_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Plane.h>
#include <Math/Geometry/Matrix.h>
#include <Math/Geometry/Bounding/Box.h>
#include <Math/Geometry/Bounding/Sphere.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define FRUSTUM_TOP				0x00
#define FRUSTUM_BOTTOM			0x01
#define FRUSTUM_RIGHT			0x02
#define FRUSTUM_LEFT			0x03
#define FRUSTUM_NEAR			0x04
#define FRUSTUM_FAR				0x05

#define FRUSTUM_PLANES_SIZE		0x06

#define FRUSTUM_INSIDE			0x00
#define FRUSTUM_OUTSIDE			0x01
#define FRUSTUM_OVERLAP_TOP		0x02
#define FRUSTUM_OVERLAP_BOTTOM	0x04
#define FRUSTUM_OVERLAP_RIGHT	0x08
#define FRUSTUM_OVERLAP_LEFT	0x10
#define FRUSTUM_OVERLAP_NEAR	0x20
#define FRUSTUM_OVERLAP_FAR		0x40
#define FRUSTUM_OVERLAP			FRUSTUM_OVERLAP_TOP | FRUSTUM_OVERLAP_BOTTOM | FRUSTUM_OVERLAP_RIGHT | \
								FRUSTUM_OVERLAP_LEFT | FRUSTUM_OVERLAP_NEAR | FRUSTUM_OVERLAP_FAR

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct BoundingBox;
struct BoundingSphere;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct FrustumType
{
	struct PlaneType Planes[FRUSTUM_PLANES_SIZE];
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Check if frustum flags are inside
////////////////////////////////////////////////////////////
bool FrustumInside(uinteger Flags);

////////////////////////////////////////////////////////////
/// Check if frustum flags are outside
////////////////////////////////////////////////////////////
bool FrustumOutside(uinteger Flags);

////////////////////////////////////////////////////////////
/// Check if frustum flags are overlapping
////////////////////////////////////////////////////////////
bool FrustumOverlap(uinteger Flags);

////////////////////////////////////////////////////////////
/// Create a frustum type
////////////////////////////////////////////////////////////
struct FrustumType * FrustumCreate();

////////////////////////////////////////////////////////////
/// Initialize a frustum type
////////////////////////////////////////////////////////////
struct FrustumType * FrustumInitialize(struct FrustumType * Frustum);

////////////////////////////////////////////////////////////
/// Copy a frustum type
////////////////////////////////////////////////////////////
void FrustumCopy(struct FrustumType * Dest, struct FrustumType * Src);

////////////////////////////////////////////////////////////
/// Destroy a frustum type
////////////////////////////////////////////////////////////
void FrustumDestroy(struct FrustumType * Frustum);

////////////////////////////////////////////////////////////
/// Checks if a point is in the viewing volume
////////////////////////////////////////////////////////////
bool FrustumCheckPoint(struct FrustumType * Frustum, struct Vector3f * Point);

////////////////////////////////////////////////////////////
/// Checks if a sphere is in the viewing volume (indicating where is it)
////////////////////////////////////////////////////////////
uinteger FrustumCheckSpherePosition(struct FrustumType * Frustum, struct BoundingSphere * Sphere);

////////////////////////////////////////////////////////////
/// Checks if a sphere is in the viewing volume, return
/// the distance to the frustum on success
////////////////////////////////////////////////////////////
float FrustumCheckSphereDistance(struct FrustumType * Frustum, struct BoundingSphere * Sphere);

////////////////////////////////////////////////////////////
/// Checks if a cube is in the viewing volume
////////////////////////////////////////////////////////////
bool FrustumCheckCube(struct FrustumType * Frustum, struct Vector3f * Position, float Radius);

////////////////////////////////////////////////////////////
/// Checks if a cube is in the viewing volume (indicating where is it)
////////////////////////////////////////////////////////////
uinteger FrustumCheckCubePosition(struct FrustumType * Frustum, struct Vector3f * Position, float Radius);

////////////////////////////////////////////////////////////
/// Checks if a rectangle is in the viewing volume
////////////////////////////////////////////////////////////
bool FrustumCheckRectangle(struct FrustumType * Frustum, struct Vector3f * Position, struct Vector3f * Size);

////////////////////////////////////////////////////////////
/// Checks if a polygon is in the viewing volume
////////////////////////////////////////////////////////////
bool FrustumCheckPolygon(struct FrustumType * Frustum, struct Vector3f * Points, uinteger PointSize);

////////////////////////////////////////////////////////////
/// Check if a bounding box is colliding with a view frustum volume
///
///     plane    index     clip flag
///     -----    -----     ---------
///    (completely inside    0x00)
///    (completely outside   0x01)
///     top        0         0x02
///     bottom     1         0x04
///     right      2         0x08
///     left       3         0x10
///     near       4         0x20
///     far        5         0x40
////////////////////////////////////////////////////////////
uinteger FrustumCheckBoundingBox(struct FrustumType * Frustum, struct BoundingBox * Box, uinteger ClipFlags);

////////////////////////////////////////////////////////////
/// Normalize frustum planes and update sing bits
////////////////////////////////////////////////////////////
void FrustumUpdatePlanes(struct FrustumType * Frustum);

////////////////////////////////////////////////////////////
/// Update frustum using a view projection matrix
////////////////////////////////////////////////////////////
void FrustumUpdate(struct FrustumType * Frustum, struct Matrix4f * ViewProj);

////////////////////////////////////////////////////////////
/// Update frustum using a view projection matrix extended
////////////////////////////////////////////////////////////
void FrustumUpdateEx(struct FrustumType * Frustum, struct Matrix4f * View, struct Matrix4f * Proj, struct Matrix4f * ViewProj, float Depth);

#endif // MATH_GEOMETRY_BOUNDINGVOLUME_FRUSTUM_H
