/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/Condition.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
/// Initialize a condition variable
////////////////////////////////////////////////////////////
void ConditionInitialize(Condition * ConditionData)
{
	pthread_cond_init(ConditionData, NULL);
}

////////////////////////////////////////////////////////////
/// Destroy a condition variable
////////////////////////////////////////////////////////////
void ConditionDestroy(Condition * ConditionData)
{
	pthread_cond_destroy(ConditionData);
}

////////////////////////////////////////////////////////////
/// Unblock a thread
////////////////////////////////////////////////////////////
void ConditionSignal(Condition * ConditionData)
{
	pthread_cond_signal(ConditionData);
}

////////////////////////////////////////////////////////////
/// Unblock all threads
////////////////////////////////////////////////////////////
void ConditionBroadcast(Condition * ConditionData)
{
	pthread_cond_broadcast(ConditionData);
}

////////////////////////////////////////////////////////////
/// Block on a condition variable
////////////////////////////////////////////////////////////
void ConditionWait(Condition * ConditionData, Mutex * Spinlock)
{
	pthread_cond_wait(ConditionData, Spinlock);
}

////////////////////////////////////////////////////////////
/// Block on a condition variable (with timeout : milliseconds)
////////////////////////////////////////////////////////////
void ConditionTimedWait(Condition * ConditionData, Mutex * Spinlock, float TimeOut)
{
// todo
}
