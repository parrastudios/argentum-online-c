/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PORTABILITY_THREAD_H
#define PORTABILITY_THREAD_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Compiler.h>
#include <Portability/Language.h>

////////////////////////////////////////////////////////////
// Detection
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Thread-safety of local static initialization
////////////////////////////////////////////////////////////
#if (LANGUAGE_VERSION_TYPE >= LANGUAGE_VERSION_CPP11) || \
	(COMPILER_TYPE == COMPILER_GNUC && COMPILER_VERSION >= CompilerVersionBuild(4, 0, 0)) || \
	(COMPILER_TYPE == COMPILER_CLANG)
#	ifndef LOCAL_STATIC_THREADSAFE
#		define LOCAL_STATIC_THREADSAFE 1
#	endif
#endif

#endif // PORTABILITY_THREAD_H
