/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Network/Protocol.h>		//< Network includes

#include <Memory/General.h>	//< System includes
#include <System/MD5.h>

#include <Game/Login.h>				//< Game includes
#include <Game/Chat.h>
#include <Game/User.h>

////////////////////////////////////////////////////////////
/// Packets handlers
////////////////////////////////////////////////////////////
bool PacketHandleLoginRequestAck();

bool PacketHandleLoginAccountAck();

bool PacketHandleLoginPlayerAck();

bool PacketHandleLogoutAccountAck();

bool PacketHandleLogoutPlayerAck();

bool PacketHandleTalkPublicAck();

bool PacketHandleWorldLoad();

bool PacketHandleWorldCreate();

bool PacketHandleWorldUpdate();

bool PacketHandleWorldDestroy();

////////////////////////////////////////////////////////////
/// Create connection
////////////////////////////////////////////////////////////
void ProtocolInitialize()
{
	// Initialize socket helper from Win32
	SocketHelperInitialize();

	// Set network status
	NetworkConnection.State = NETWORK_STATUS_INITIALIZED;
}

////////////////////////////////////////////////////////////
/// Destroy connection
////////////////////////////////////////////////////////////
void ProtocolDestroy()
{
	if (NetworkConnection.State != NETWORK_STATUS_DESTROYED)
	{
		// Clean the socket helper from Win32
		SocketHelperCleanup();
	}
}

////////////////////////////////////////////////////////////
/// Get the client connection status
////////////////////////////////////////////////////////////
uinteger ProtocolGetStatus()
{
	return NetworkConnection.State;
}

////////////////////////////////////////////////////////////
/// Connect to the server
////////////////////////////////////////////////////////////
void ProtocolConnect()
{
	if (NetworkConnection.State != NETWORK_STATUS_CONNECTED)
	{
		// Try to connect
		if (SocketConnect(&NetworkConnection.Socket, NETWORK_CONNECTION_PORT, NETWORK_CONNECTION_IP, 5.0f) == Done)
		{
			// Initialize user
			UserReset();

			// Any more thing to reset?
			// ...

			// Connected
			NetworkConnection.State = NETWORK_STATUS_CONNECTED;

			// Launch the thread
			ThreadLaunch(&NetworkConnection.SocketThread, (FuncType)&ProtocolListen, NULL);

			// Send packet request to server
			PacketSendRequest();
		}
		else
		{
			// Disconnect from the server
			SocketClose(&NetworkConnection.Socket);

			// Show the message error
			MessageError("ProtocolConnect", "An error ocurred when the client was trying to connect to the server.");
		}
	}
}

////////////////////////////////////////////////////////////
/// Disconnect from the server
////////////////////////////////////////////////////////////
bool ProtocolDisconnect()
{
	if (NetworkConnection.State == NETWORK_STATUS_CONNECTED)
	{
		if (SocketClose(&NetworkConnection.Socket) == Done)
		{
			 // Wait until the thread finishes
			ThreadWait(&NetworkConnection.SocketThread);

			// Terminate thread
			ThreadTerminate(&NetworkConnection.SocketThread);

			// Update connection status
			NetworkConnection.State = NETWORK_STATUS_DISCONNECTED;

			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Listen procediment
////////////////////////////////////////////////////////////
void ProtocolListen()
{
	while (NetworkConnection.State == NETWORK_STATUS_CONNECTED)
	{
		// Socket incoming buffer
		uint8 Buffer[NETWORK_CONNECTION_SOCKET_MAX_SIZE];

		// Socket incoming data size
		uinteger SizeRecived = 0;

		// Check the result of the socket
		enum SocketStatus Result;

		// Recive data
		Result = SocketReceive(&NetworkConnection.Socket, Buffer, NETWORK_CONNECTION_SOCKET_MAX_SIZE, &SizeRecived);

		// Handle the result
		switch (Result)
		{
			case Done :
			{
				// Parse the data
				if (!ProtocolParser(Buffer, SizeRecived))
				{
					// Handle error

					return;
				}

				break;
			}

			case Disconnected :
			{
				// Disconnect
				ProtocolDisconnect();
				break;
			}

			default :
			{
				// Disconnect
				ProtocolDisconnect();

				// Set error status
				NetworkConnection.State = NETWORK_STATUS_ERROR;
				break;
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Get properly the size of an outgoing packet
////////////////////////////////////////////////////////////
uinteger ProtocolPacketGetSize()
{
	// todo

	switch (NetworkConnection.Outgoing.Header.Type)
	{
		case NETWORK_TYPE_ERROR :
		{
			return NETWORK_PACKET_DATA_EMPTY;
		}

		case NETWORK_TYPE_LOG_IN :
		{
			switch (NetworkConnection.Outgoing.Header.Code)
			{
				case NETWORK_CODE_LOG_IN_REQUEST :
				{
					return sizeof(struct PacketLoginRequest);
				}

				case NETWORK_CODE_LOG_IN_ACCOUNT :
				{
					return sizeof(struct PacketLoginAccount);
				}

				case NETWORK_CODE_LOG_IN_PLAYER :
				{
					return sizeof(struct PacketLoginPlayer);
				}
			}

			break;
		}

		case NETWORK_TYPE_LOG_OUT :
		{
			switch (NetworkConnection.Outgoing.Header.Code)
			{
				case NETWORK_CODE_LOG_OUT_ACCOUNT :
				{
					return NETWORK_PACKET_DATA_EMPTY;
				}

				case NETWORK_CODE_LOG_OUT_PLAYER :
				{
					return NETWORK_PACKET_DATA_EMPTY;
				}
			}

			break;
		}

		case NETWORK_TYPE_MOVEMENT :
		{
			switch (NetworkConnection.Outgoing.Header.Code)
			{
				case NETWORK_CODE_TALK_PUBLIC :
				{
					return sizeof(struct PacketMovementWalk);
				}
			}

			break;
		}

		case NETWORK_TYPE_TALK :
		{
			switch (NetworkConnection.Outgoing.Header.Code)
			{
				case NETWORK_CODE_TALK_PUBLIC :
				{
					// Header data size hack
					return NetworkConnection.Outgoing.Header.DataSize;
				}
			}

			break;
		}

		case NETWORK_TYPE_WORLD :
		{
			switch (NetworkConnection.Outgoing.Header.Code)
			{
				case NETWORK_CODE_WORLD_CHECK :
				{
					return sizeof(struct PacketWorldCheck);
				}
			}

			break;
		}
	}


	// If invalid type or code
	return NETWORK_PACKET_INVALID_SIZE;
}

////////////////////////////////////////////////////////////
/// Check properly the size of an incoming packet
////////////////////////////////////////////////////////////
bool ProtocolPacketCheckSize()
{
	uinteger Size = NetworkConnection.Incoming.Header.DataSize;

	switch (NetworkConnection.Incoming.Header.Type)
	{
		case NETWORK_TYPE_ERROR :
		{
			return (Size == NETWORK_PACKET_DATA_EMPTY);
		}

		case NETWORK_TYPE_LOG_IN :
		{
			switch (NetworkConnection.Incoming.Header.Code)
			{
				case NETWORK_CODE_LOG_IN_REQUEST :
				{
					return (Size == NETWORK_PACKET_DATA_EMPTY);
				}

				case NETWORK_CODE_LOG_IN_ACCOUNT :
				{
					bool ValidSize = (Size % sizeof(struct AccountPlayerInfo) == 0);

					if (ValidSize)
					{
						uint8 PlayerCount = NetworkConnection.Incoming.Header.DataSize / sizeof(struct AccountPlayerInfo);

						NetworkConnection.Incoming.Packet.LoginAccountAck.Count = PlayerCount;
					}

					return ValidSize;
				}

				case NETWORK_CODE_LOG_IN_PLAYER :
				{
					uinteger MotdLength = strlen((char*)&NetworkConnection.Incoming.Packet.LoginPlayerAck.Motd[0]);

					return (Size == (sizeof(struct PacketLoginPlayerAck) - 1 + MotdLength));
				}
			}

			break;
		}

		case NETWORK_TYPE_LOG_OUT :
		{
			switch (NetworkConnection.Incoming.Header.Code)
			{
				case NETWORK_CODE_LOG_OUT_ACCOUNT :
				{
					return (Size == NETWORK_PACKET_DATA_EMPTY);
				}

				case NETWORK_CODE_LOG_OUT_PLAYER :
				{
					return (Size == NETWORK_PACKET_DATA_EMPTY);
				}
			}

			break;
		}

		case NETWORK_TYPE_MOVEMENT :
		{
			switch (NetworkConnection.Incoming.Header.Code)
			{
				case NETWORK_CODE_TALK_PUBLIC :
				{
					return sizeof(struct PacketMovementWalkAck);
				}
			}

			break;
		}

		case NETWORK_TYPE_TALK :
		{
			switch (NetworkConnection.Incoming.Header.Code)
			{
				case NETWORK_CODE_TALK_PUBLIC :
				{
					uinteger Length = strlen((char*)&NetworkConnection.Incoming.Packet.TalkDefault.Text[0]);

					return (Size == (Length + 1));
				}
			}

			break;
		}

		case NETWORK_TYPE_ENVIRONMENT :
		{
			switch (NetworkConnection.Incoming.Header.Code)
			{
				case NETWORK_CODE_ENVIRONMENT_AUDIO :
				{
					return (Size == sizeof(struct PacketEnvironmentAudio));
				}

				case NETWORK_CODE_ENVIRONMENT_SOUND :
				{
					return (Size == sizeof(struct PacketEnvironmentSound));
				}

				case NETWORK_CODE_ENVIRONMENT_FOG :
				{
					return (Size == sizeof(struct PacketEnvironmentFog) ||
							Size == NETWORK_PACKET_DATA_EMPTY);
				}
			}

			break;
		}
	}



	// Invalid size
	return false;
}

////////////////////////////////////////////////////////////
/// Protocol parser
////////////////////////////////////////////////////////////
bool ProtocolParser(uint8 * Buffer, uinteger BufferSize)
{
	uinteger i;

	for (i = 0; i < BufferSize; i++)
	{
		switch (NetworkConnection.ParserStatus)
		{
			case NETWORK_PARSER_WAITING_PREAMBLE :
			{
				if (Buffer[i] == NETWORK_PARSER_PREAMBLE)
				{
					// Get preamble token
					NetworkConnection.Incoming.Header.Preamble = Buffer[i];

					// Start parsing packet
					NetworkConnection.ParserStatus = NETWORK_PARSER_WAITING_TYPE;
				}
				break;
			}

			case NETWORK_PARSER_WAITING_TYPE :
			{
				// Get packet type
				NetworkConnection.Incoming.Header.Type = Buffer[i];

				// Wait for packet code
				NetworkConnection.ParserStatus = NETWORK_PARSER_WAITING_CODE;
				break;
			}

			case NETWORK_PARSER_WAITING_CODE :
			{
				// Get packet code
				NetworkConnection.Incoming.Header.Code = Buffer[i];

				// Wait for packet lenght
				NetworkConnection.ParserStatus = NETWORK_PARSER_WAITING_LENGTH;
				break;
			}

			case NETWORK_PARSER_WAITING_LENGTH :
			{
				// Obtain packet data length
				NetworkConnection.Incoming.Header.DataSize = Buffer[i];

				// Wait for checksum
				NetworkConnection.ParserStatus = NETWORK_PARSER_WAITING_CLENGTH;
				break;
			}

			case NETWORK_PARSER_WAITING_CLENGTH :
			{
				// Check integrity
				if (NetworkConnection.Incoming.Header.DataSize + Buffer[i] != NETWORK_PACKET_LENGTH)
				{
					// Error, wait for next packet
					NetworkConnection.ParserStatus = NETWORK_PARSER_WAITING_PREAMBLE;

	                break;
				}

				// If no data attached, handle user directly
				if (NetworkConnection.Incoming.Header.DataSize == 0)
				{
					// Wait for next packet
					NetworkConnection.ParserStatus = NETWORK_PARSER_WAITING_PREAMBLE;

					// Handle packet without data
					return (ProtocolPacketCheckSize() ? ProtocolHandleData() : false);
				}

				// If attached data, parse it
				if (NetworkConnection.Incoming.Header.DataSize > 0)
				{
					// Set current data counter to first byte of the structure
					NetworkConnection.CurrentLength = NETWORK_PACKET_HEADER_LENGTH;

					// Start parsing data
					NetworkConnection.ParserStatus = NETWORK_PARSER_WAITING_DATA;

					break;
				}

				break;
			}

			case NETWORK_PARSER_WAITING_DATA :
			{
				// Get the data
				NetworkConnection.Incoming.Buffer[NetworkConnection.CurrentLength++] = Buffer[i];

				// If data is read completely
				if ((NetworkConnection.CurrentLength - NETWORK_PACKET_HEADER_LENGTH) ==
					 NetworkConnection.Incoming.Header.DataSize)
				{
					// Wait for the next packet
					NetworkConnection.ParserStatus = NETWORK_PARSER_WAITING_PREAMBLE;

					// Return false if the data is wrong
					return (ProtocolPacketCheckSize() ? ProtocolHandleData() : false);
				}

				break;
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Handles incoming data socket
////////////////////////////////////////////////////////////
bool ProtocolHandleData()
{
	switch (NetworkConnection.Incoming.Header.Type)
	{
		// Error packet

		case NETWORK_TYPE_ERROR :
		{
			switch (NetworkConnection.Incoming.Header.Code)
			{
				case NETWORK_CODE_ERR_REQ_INVALID :
				{
					// Invalid request -> update client
					break;
				}

				case NETWORK_CODE_ERR_ACC_LOGGED :
				{
					// Account already logged
					break;
				}

				case NETWORK_CODE_ERR_ACC_BAN :
				{
					// Account banned
					break;
				}

				case NETWORK_CODE_ERR_ACC_PASSW :
				{
					// Invalid password
					break;
				}

				case NETWORK_CODE_ERR_ACC_INVALID :
				{
					// Invalid account
					break;
				}

				// todo ...

				default :
				{
					// Invalid code
					return false;
				}
			}

			break;
		}

		// Login packet

		case NETWORK_TYPE_LOG_IN :
		{
			switch (NetworkConnection.Incoming.Header.Code)
			{
				case NETWORK_CODE_LOG_IN_REQUEST :
				{
					return PacketHandleLoginRequestAck();
				}

				case NETWORK_CODE_LOG_IN_ACCOUNT :
				{
					return PacketHandleLoginAccountAck();
				}

				case NETWORK_CODE_LOG_IN_PLAYER :
				{
					return PacketHandleLoginPlayerAck();
				}
			}

			break;
		}

		// Logout packet

		case NETWORK_TYPE_LOG_OUT :
		{
			switch (NetworkConnection.Incoming.Header.Code)
			{
				case NETWORK_CODE_LOG_OUT_ACCOUNT :
				{
					return PacketHandleLogoutAccountAck();
				}

				case NETWORK_CODE_LOG_OUT_PLAYER :
				{
					return PacketHandleLogoutPlayerAck();
				}
			}

			break;
		}

		// World packet

		case NETWORK_TYPE_WORLD :
		{
			switch (NetworkConnection.Incoming.Header.Code)
			{
				// todo: improve packet flow

				// setup:
				// client	<--->	server
				//
				//			<---	check
				// check_ack --->
				//			<---	download	: on error
				// down_ack	 --->
				//			<---	preload		: on valid
				// pre_ack	 --->
				//			<---	load
				// load_ack	 --->
				//
				// run:
				//			<---	create		: world (entity) initializer
				//			<---	update		: (entity) update position, rotation
				//			<---	modify		: (entity) update properties
				//			<---	destroy		: (entity) destructor
				//

				case NETWORK_CODE_WORLD_LOAD :
				{
					return PacketHandleWorldLoad();
				}

				case NETWORK_CODE_WORLD_CREATE :
				{
					return PacketHandleWorldCreate();
				}

				case NETWORK_CODE_WORLD_UPDATE :
				{
					return PacketHandleWorldUpdate();
				}

				case NETWORK_CODE_WORLD_DESTROY :
				{
					return PacketHandleWorldDestroy();
				}
			}

			break;
		}

		// Talk packet

		case NETWORK_TYPE_TALK :
		{
			switch (NetworkConnection.Incoming.Header.Code)
			{
				case NETWORK_CODE_TALK_PUBLIC :
				{
					return PacketHandleTalkPublicAck();
				}
			}

			break;
		}

		default :
		{
			// Invalid type
			return false;
		}
	}

    return false;
}

////////////////////////////////////////////////////////////
/// Handle the result packet
////////////////////////////////////////////////////////////
bool PacketHandleLoginRequestAck()
{
	// Send account login
	PacketSendLoginAccount();

	return true;
}

////////////////////////////////////////////////////////////
/// Handle the account packet
////////////////////////////////////////////////////////////
bool PacketHandleLoginAccountAck()
{
	uinteger i;

	// Copy data into user
	UserGetData()->Account.Count = NetworkConnection.Incoming.Packet.LoginAccountAck.Count;

	for (i = 0; i < UserGetData()->Account.Count; i++)
	{
		AccountPlayerInfoCopy(&UserGetData()->Account.Players[i],
							  &NetworkConnection.Incoming.Packet.LoginAccountAck.Players[i]);
	}

	// Open account
	UserAccountOpen();

	return true;
}

bool PacketHandleLoginPlayerAck()
{
	// Unload account
	AccountUnload();

	// NetworkConnection.Incoming.Packet.LoginPlayerAck.Player.Index

	// Write MOTD into console
	ChatAddLine((char*)&NetworkConnection.Incoming.Packet.LoginPlayerAck.Motd[0]);

	return true;
}

bool PacketHandleLogoutAccountAck()
{
	// Close socket connection
	ProtocolDisconnect();

	// Exit from account
	AccountExit();

	return true;
}

bool PacketHandleLogoutPlayerAck()
{

	return true;
}

bool PacketHandleWorldLoad()
{
	// TODO

	// Load the scene
	//if (SceneManagerLoad(NetworkConnection.Incoming.Packet.WorldLoad.Index))
	{
		// Send ack
		ProtocolSendData(NETWORK_TYPE_WORLD, NETWORK_CODE_WORLD_LOAD);

		return true;
	}

	return false;
}

bool PacketHandleWorldCreate()
{

	// foreach entity in list
	//		SceneManagerCreate(Entity);

	return true;
}

bool PacketHandleWorldUpdate()
{
	// foreach entity in list
	//		SeceneManagerUpdate(Entity);
	return true;
}

bool PacketHandleWorldDestroy()
{
	// foreach entity in list
	//		SceneManagerDestroy(Entity);
	return true;
}

bool PacketHandleTalkPublicAck()
{
	// Write message into console
	ChatAddLine((char*)&NetworkConnection.Incoming.Packet.TalkDefault.Text[0]);

	return true;
}

////////////////////////////////////////////////////////////
/// Protocol send data procediment
////////////////////////////////////////////////////////////
void ProtocolSendData(uint8 Type, uint8 Code)
{
	uinteger Size;

	if (!SocketIsValid(&NetworkConnection.Socket)) return;

	// Set buffer initial data
	NetworkConnection.Outgoing.Header.Preamble	= NETWORK_PARSER_PREAMBLE;
	NetworkConnection.Outgoing.Header.Type		= Type;
	NetworkConnection.Outgoing.Header.Code		= Code;

	Size = ProtocolPacketGetSize();

	if (Size != NETWORK_PACKET_INVALID_SIZE && Size <= NETWORK_PACKET_DATA_LENGTH)
	{
		// Update size
		NetworkConnection.Outgoing.Header.DataSize = Size;
		NetworkConnection.Outgoing.Header.Checksum = ~Size;

		// Send data to user
		SocketSend(&NetworkConnection.Socket,
				   &NetworkConnection.Outgoing.Buffer[0],
				   NetworkConnection.Outgoing.Header.DataSize + NETWORK_PACKET_HEADER_LENGTH);
	}
}

////////////////////////////////////////////////////////////
///  Protocol packet request sender
////////////////////////////////////////////////////////////
void PacketSendRequest()
{
	// Get MD5 File
	#if COMPILE_TYPE == RELEASE_MODE
        #if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
            MD5File("Client.exe", NetworkConnection.Outgoing.Packet.Request.MD5);
        #else
            MD5File("Client", NetworkConnection.Outgoing.Packet.Request.MD5);
        #endif
	#else
		strcpy((char*)NetworkConnection.Outgoing.Packet.Request.MD5, "abcdefghijklmnop");
	#endif

	// Update application version
	NetworkConnection.Outgoing.Packet.Request.Version.Major		= APPLICATION_MAJOR_VERSION;
	NetworkConnection.Outgoing.Packet.Request.Version.Minor		= APPLICATION_MINOR_VERSION;
	NetworkConnection.Outgoing.Packet.Request.Version.Revision	= APPLICATION_MICRO_VERSION;

	// Send packet
	ProtocolSendData(NETWORK_TYPE_LOG_IN, NETWORK_CODE_LOG_IN_REQUEST);
}

////////////////////////////////////////////////////////////
///  Protocol packet account login sender
////////////////////////////////////////////////////////////
void PacketSendLoginAccount()
{
	// Copy user data into packet data
	strcpy(NetworkConnection.Outgoing.Packet.LoginAccount.Name, UserGetData()->Account.Name);
	strcpy(NetworkConnection.Outgoing.Packet.LoginAccount.Password, UserGetData()->Account.Password);

	// Send login packet
	ProtocolSendData(NETWORK_TYPE_LOG_IN, NETWORK_CODE_LOG_IN_ACCOUNT);
}
////////////////////////////////////////////////////////////
///  Protocol packet player login sender
////////////////////////////////////////////////////////////
void PacketSendLoginPlayer()
{
	if (UserGetData()->Account.PlayerLogged < ACCOUNT_PLAYER_MAX_SIZE)
	{
		// Set the packet data
		NetworkConnection.Outgoing.Packet.LoginPlayer.Index = UserGetData()->Account.PlayerLogged;

		// Send packet
		ProtocolSendData(NETWORK_TYPE_LOG_IN, NETWORK_CODE_LOG_IN_PLAYER);
	}
}
////////////////////////////////////////////////////////////
///  Protocol packet account logout sender
////////////////////////////////////////////////////////////
void PacketSendLogoutAccount()
{
	// Send packet
	ProtocolSendData(NETWORK_TYPE_LOG_OUT, NETWORK_CODE_LOG_OUT_ACCOUNT);
}

////////////////////////////////////////////////////////////
///  Protocol packet player logout sender
////////////////////////////////////////////////////////////
void PacketSendLogoutPlayer()
{
	// Send packet
	ProtocolSendData(NETWORK_TYPE_LOG_OUT, NETWORK_CODE_LOG_OUT_PLAYER);
}

////////////////////////////////////////////////////////////
///  Protocol packet chat public sender
////////////////////////////////////////////////////////////
void PacketSendChatPublic(char * Text, uinteger Length)
{
	if (Length < NETWORK_PACKET_DATA_LENGTH)
	{
		// Add null char
		Text[Length] = NULLCHAR;

		// Packet header hack
		NetworkConnection.Outgoing.Header.DataSize = Length + 1;

		// Copy data
		MemoryCopy(&NetworkConnection.Outgoing.Packet.TalkDefault.Text[0], Text, NetworkConnection.Outgoing.Header.DataSize);

		// Send packet
		ProtocolSendData(NETWORK_TYPE_TALK, NETWORK_CODE_TALK_PUBLIC);
	}
}

////////////////////////////////////////////////////////////
/// Protocol packet movement sender
////////////////////////////////////////////////////////////
void PacketSendMovementWalk(uint8 Key, bool Pressed)
{
	NetworkConnection.Outgoing.Packet.MovementWalk.Key = Key;
	NetworkConnection.Outgoing.Packet.MovementWalk.Pressed = Pressed;

	// Send packet
	ProtocolSendData(NETWORK_TYPE_MOVEMENT, NETWORK_CODE_MOVEMENT_WALK);
}
