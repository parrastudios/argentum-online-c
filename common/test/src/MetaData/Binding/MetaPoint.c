/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaPoint.h>
#include <Point.h>
#include <MetaData/General.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// MetaPoint create register
////////////////////////////////////////////////////////////
void * MetaPointCreate(MetadataParameterListType * MetaParamList)
{
#if 0
	float ParamX = MetaParamList->GetFloat(MetaParamList, "x");
	// float ParamX = MetaParamList->GetAtFloat(MetaParamList, 0);

	float ParamY = MetaParamList->GetFloat(MetaParamList, "y");
	// float ParamX = MetaParamList->GetAtFloat(MetaParamList, 1);

	return PointCreate(ParamX, ParamY);
#else
	return NULL;
#endif
}

////////////////////////////////////////////////////////////
/// Register point into metadata system
////////////////////////////////////////////////////////////
void MetaPointRegister()
{
	// Get metadata manager instance
	MetadataManagerType MetaManager = MetadataManagerGlobalInstance();

#if 0
	// Register point object
	MetadataObjectType MetaPoint = MetaManager->ObjectBind("Point", &PointObject);

	// Register object attributes
	MetaManager->ObjectBindAttribute(MetaPoint, struct PointType, X, FLOAT);
	MetaManager->ObjectBindAttribute(MetaPoint, struct PointType, Y, FLOAT);

	// Register object methods
	MetaManager->ObjectBindMethod(MetaPoint, struct PointType, Move, (FLOAT, FLOAT));
	MetaManager->ObjectBindMethod(MetaPoint, struct PointType, Print);
	MetaManager->ObjectBindMethod(MetaPoint, struct PointType, Destroy);
#endif
}
