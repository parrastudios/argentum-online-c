/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Plugin/PluginManager.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create plugin manager instance
////////////////////////////////////////////////////////////
bool PluginManagerCreate()
{
	PluginManagerType * PluginManager = PluginManagerGetInstance();

	// Initialize plugin loader
	if (PluginManager->Loader()->Initialize())
	{
		// Create plugin map
		PluginManager->ResourceList = HashMapCreate(HashGetCallbackStringDefault(), CompareGetCallback(COMPARE_FUNC_CSTRING));

		if (PluginManager->ResourceList)
		{
			// ...

			return true;
		}
		else
		{
			// Destroy plugin loader
			PluginManager->Loader()->Destroy();
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Retreive a plugin from the manager
////////////////////////////////////////////////////////////
PluginType * PluginManagerGet(char * Name)
{
	PluginManagerType * PluginManager = PluginManagerGetInstance();

	return HashMapGet(PluginManager->ResourceList, (HashKeyData)Name);
}

////////////////////////////////////////////////////////////
/// Attach a plugin to the manager
////////////////////////////////////////////////////////////
PluginType * PluginManagerAttach(char * Name)
{
	// Get plugin if already exists
	PluginType * Plugin = PluginManagerGet(Name);

	// If plugin is not in the manager
	if (!Plugin)
	{
		PluginManagerType * PluginManager = PluginManagerGetInstance();

		// Create plugin
		Plugin = PluginCreate();

		if (Plugin)
		{
			// Load plugin
			if (PluginManager->Loader()->Load(Name, Plugin))
			{
				// Insert plugin into the map
				if (!HashMapInsert(PluginManager->ResourceList, (HashKeyData)Plugin->Id, (void *)Plugin))
				{
					// On error, unload plugin
					PluginManager->Loader()->Unload(Name);

					// Destroy plugin
					PluginDestroy(Plugin);
				}
			}
			else
			{
				// Destroy plugin
				PluginDestroy(Plugin);
			}
		}
	}

	return Plugin;
}

////////////////////////////////////////////////////////////
/// Detach plugin from the manager
////////////////////////////////////////////////////////////
void PluginManagerDetach(PluginType * Plugin)
{
	PluginManagerType * PluginManager = PluginManagerGetInstance();

	// If plugin is already loaded on the manager
	if (Plugin && PluginManager->Get(Plugin->Id))
	{
		// Remove from loader
		PluginManager->Loader()->Unload(Plugin->Id);

		// Remove from map
		HashMapRemove(PluginManager->ResourceList, Plugin->Id);

		// Destroy plugin
		PluginDestroy(Plugin);
	}
}

////////////////////////////////////////////////////////////
/// Destroy plugin manager instance
////////////////////////////////////////////////////////////
void PluginManagerDestroy()
{
	PluginManagerType * PluginManager = PluginManagerGetInstance();

	// Destroy plugin map
	if (PluginManager->ResourceList)
	{
		HashMapDestroy(PluginManager->ResourceList);
	}

	// Destroy plugin loader
	PluginManager->Loader()->Destroy();
}

////////////////////////////////////////////////////////////
/// Get instance of plugin manager singleton
////////////////////////////////////////////////////////////
PluginManagerType * PluginManagerGetInstance()
{
	static PluginManagerType PluginManagerTypeInstance = {
		TypeInit(Create,		&PluginManagerCreate),
		TypeInit(Get,			&PluginManagerGet),
		TypeInit(Attach,		&PluginManagerAttach),
		TypeInit(Detach,		&PluginManagerDetach),
		TypeInit(Destroy,		&PluginManagerDestroy),
		TypeInit(Loader,		&PluginLoaderGetInstance),
		TypeInit(ResourceList,	NULL)
	};

	return &PluginManagerTypeInstance;
}