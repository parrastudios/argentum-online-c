/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_OPENGLHELPER_H
#define GRAPHICS_OPENGLHELPER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Types.h>

////////////////////////////////////////////////////////////
/// Begin rendering states
////////////////////////////////////////////////////////////
void OpenGLBegin(struct OpenGLInfoType * OpenGLInfo);

////////////////////////////////////////////////////////////
/// End rendering states
////////////////////////////////////////////////////////////
void OpenGLEnd(struct OpenGLInfoType * OpenGLInfo);

#endif // GRAPHICS_OPENGLHELPER_H

/*
////////////////////////////////////////////////////////////
/// Begin rendering states
////////////////////////////////////////////////////////////
void OpenGLBegin(struct OpenGLInfoType * OpenGLInfo)
{
	if (OpenGLInfo->States[GL_STATES_MODE] == GL_STATES_SAVE)
	{
		// Save render states
		glPushAttrib(GL_ALL_ATTRIB_BITS);

		// Save matrices
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
	}

	if (OpenGLInfo->States[GL_RENDERING_MODE] == GL_RENDERING_2D)
	{
		GLint Viewport[4];

		glGetIntegerv(GL_VIEWPORT, Viewport);

        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();

		glOrtho(Viewport[0], Viewport[0] + Viewport[2], Viewport[1] + Viewport[3], Viewport[1], -1.0f, 1.0f);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();

        glPushAttrib(GL_DEPTH_BUFFER_BIT | GL_LIGHTING_BIT);

        glDisable(GL_DEPTH_TEST);
        glDisable(GL_LIGHTING);
		glDisable(GL_DITHER);

		glEnable(GL_TEXTURE_2D);
	}
	else if (OpenGLInfo->States[GL_RENDERING_MODE] == GL_RENDERING_3D)
	{
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		glEnable(GL_LIGHTING);
		glEnable(GL_DITHER);
		glEnable(GL_CULL_FACE);

		glDisable(GL_TEXTURE_2D);
	}

}

////////////////////////////////////////////////////////////
/// End rendering states
////////////////////////////////////////////////////////////
void OpenGLEnd(struct OpenGLInfoType * OpenGLInfo)
{
	if (OpenGLInfo->States[GL_STATES_MODE] == GL_STATES_SAVE)
	{
		// Restore render states
		glPopAttrib();

		// Restore matrices
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
	}

	if (OpenGLInfo->States[GL_RENDERING_MODE] == GL_RENDERING_2D)
	{
        glPopAttrib();

		glMatrixMode(GL_PROJECTION);
		glPopMatrix();

		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();

		glDisable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);

		glEnable(GL_DEPTH_TEST);
        glEnable(GL_LIGHTING);
		glEnable(GL_DITHER);
	}
	else if (OpenGLInfo->States[GL_RENDERING_MODE] == GL_RENDERING_3D)
	{
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		glDisable(GL_DITHER);
		glDisable(GL_CULL_FACE);
		
		glEnable(GL_TEXTURE_2D);
	}
}
*/