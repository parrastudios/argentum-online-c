/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	METADATA_PARAMETER_H
#define METADATA_PARAMETER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/Storage.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define METADATA_PARAMETER_LIST_NULL			0xFFFFFFFFUL

enum MetadataParameterTypeInfo
{
	METADATA_PARAMETER_TYPE_NULL = 0x00,
	METADATA_PARAMETER_TYPE_BY_VALUE = 0x01,
	METADATA_PARAMETER_TYPE_BY_REFERENCE = 0x02,

	METADATA_PARAMETER_TYPE_SIZE,

	METADATA_PARAMETER_TYPE_INVALID = 0xFFFFFFFFUL
};

typedef MetadataStorageIndexType	MetadataParameterType;
typedef uint32						MetadataParameterTypeInfo;
typedef char *						MetadataParameterNameType;

typedef MetadataStorageIndexType	MetadataParameterListType;
typedef uint32						MetadataParameterListCountType;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a parameter
////////////////////////////////////////////////////////////
MetadataParameterType MetadataParameterCreate(MetadataStorageIndexType Index, MetadataParameterTypeInfo Type);

////////////////////////////////////////////////////////////
/// Destroy a parameter
////////////////////////////////////////////////////////////
void MetadataParameterDestroy(MetadataParameterType Parameter);

////////////////////////////////////////////////////////////
/// Create a parameter list
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataParameterListCreate();

////////////////////////////////////////////////////////////
/// Appennd to a parameter list
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataParameterListAppend(MetadataParameterListType ParameterList, MetadataParameterNameType Name, MetadataParameterType Parameter);

////////////////////////////////////////////////////////////
/// Retreive from a parameter list
////////////////////////////////////////////////////////////
MetadataParameterType MetadataParameterListRetreive(MetadataParameterListType ParameterList, MetadataParameterNameType Name);

////////////////////////////////////////////////////////////
/// Destroy a parameter list
////////////////////////////////////////////////////////////
void MetadataParameterListDestroy(MetadataParameterListType ParameterList);

#endif // METADATA_PARAMETER_H
