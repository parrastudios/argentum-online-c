#version 120

// TODO

attribute vec4 vtangent;

void main(void)
{
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

	gl_TexCoord[0] = gl_MultiTexCoord0;

	vec3 vbitangent = cross(gl_Normal, vtangent.xyz) * vtangent.w; // bitangent not used

	gl_FrontColor = gl_Color * (clamp(dot(normalize(gl_NormalMatrix * gl_Normal), gl_LightSource[0].position.xyz), 0.0, 1.0) * gl_LightSource[0].diffuse + gl_LightSource[0].ambient);
}
