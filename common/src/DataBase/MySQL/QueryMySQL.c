/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Platform.h>
#include <DataBase/MySQL/General.h>
#include <DataBase/Query.h>
#include <DataBase/Default.h>

#include <System/IOHelper.h> // todo: remove this

////////////////////////////////////////////////////////////
/// Report an error in query
////////////////////////////////////////////////////////////
void QueryError(struct DataBase * DB, char * Tag, bool Reconnect)
{
	if (DB)
	{
		if (Reconnect)
		{
			// Show error
			MessageError("Query",
				"Error in %s: The program couldn't make a query. %s [ID: %d]. Trying to reconnect",
				Tag,
				mysql_error((MYSQL*)DB->Connection),
				mysql_errno((MYSQL*)DB->Connection));

			// Reconnect
			DataBaseReconnect();
		}
		else
		{
			// Show error
			MessageError("Query",
				"Error in %s: The program couldn't make a query. %s [ID: %d]",
				Tag,
				mysql_error((MYSQL*)DB->Connection),
				mysql_errno((MYSQL*)DB->Connection));
		}
	}
	else
	{
		// Show error
		MessageError("Query", "Invalid DataBase handler");
	}
}

/////////////////////////////////////////////////////////////
/// Makes a query to the database
/////////////////////////////////////////////////////////////
bool Query(QueryResultType * Result, char * Body, ...)
{
	struct DataBase * DB = DataBaseGetDefault();

	// Check if the database is avaible
	if (DB && DB->Connection)
	{
		char QueryFormated[QUERY_STRING_LENGTH];

		// Argument list
		va_list ArgList;

		// Initialize the argument list
		va_start(ArgList, Body);

		// sprintf arguments on string
		#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT && COMPILER_TYPE == COMPILER_MSVC
			vsprintf_s(QueryFormated, QUERY_STRING_LENGTH, Body, ArgList);
		#else
			vsnprintf(QueryFormated, QUERY_STRING_LENGTH, Body, ArgList);
		#endif

		// Destroy argument list
		va_end(ArgList);

		// Check if the connection is active
		if (mysql_ping((MYSQL*)DB->Connection))
		{
			// Show error
			QueryError(DB, "Query", true);

			// On error
			if (Result)
			{
				// Retreive result
				*Result = NULL;
			}

			return false;
		}

		// Make the query
		if (!mysql_query((MYSQL*)DB->Connection, QueryFormated))
		{
			// On success
			if (Result)
			{
				// Retreive result
				*Result = (QueryResultType)mysql_store_result((MYSQL*)DB->Connection);
			}

			return true;
		}
	}

	// On error
	if (Result)
	{
		*Result = NULL;
	}

	// Show error
	QueryError(DB, "Query", false);

	// Close it (todo: generates memory leak on database singleton)
	// DBConnectorDestroy(DB);

	return false;
}

/////////////////////////////////////////////////////////////
/// Get the next result of a query and releases the previous
///	in case of multiple queries
/////////////////////////////////////////////////////////////
QueryResultType QueryGetNextResult(QueryResultType Result)
{
	if (Result)
	{
		struct DataBase * DB = DataBaseGetDefault();

		// Check if the database is avaible
		if (DB && DB->Connection)
		{
			// Release the previous query
			mysql_free_result((MYSQL_RES *)Result);

			// Get next result
			if (!mysql_next_result((MYSQL *)DB->Connection))
			{
				// Store it
				MYSQL_RES * NextResult = mysql_store_result((MYSQL*)DB->Connection);

				return (QueryResultType)NextResult;
			}
		}
	}

	return NULL;
}

/////////////////////////////////////////////////////////////
/// Get the number of rows of the last query result
/////////////////////////////////////////////////////////////
uinteger QueryRowCount(QueryResultType Result)
{
	if (Result)
	{
		struct DataBase * DB = DataBaseGetDefault();

		// Check if the database is avaible and the query is valid
		if (DB && DB->Connection)
		{
			return (uinteger)mysql_num_rows((MYSQL_RES *)Result);
		}
	}

	return UINTEGER_SUFFIX(0);
}

/////////////////////////////////////////////////////////////
/// Get a row from the last query result
/////////////////////////////////////////////////////////////
QueryRowType QueryGetRow(QueryResultType Result)
{
	if (Result)
	{
		struct DataBase * DB = DataBaseGetDefault();

		// Check if the database is avaible and the query is valid
		if (DB && DB->Connection)
		{
			// Get the row
			MYSQL_ROW Row = mysql_fetch_row((MYSQL_RES *)Result);

			return (QueryRowType)Row;
		}

		// Show the error
		QueryError(DB, "QueryGetRow", false);
	}

	return NULL;
}

/////////////////////////////////////////////////////////////
/// Get the number of colums of the last query result
/////////////////////////////////////////////////////////////
uinteger QueryFieldCount(QueryResultType Result)
{
	if (Result)
	{
		struct DataBase * DB = DataBaseGetDefault();

		// Check if the database is avaible and the query is valid
		if (DB && DB->Connection)
		{
			return (uint32)mysql_num_fields(Result);
		}
	}

	return UINTEGER_SUFFIX(0);
}

/////////////////////////////////////////////////////////////
/// Get the number of colums of the last query result
/////////////////////////////////////////////////////////////
void QueryGetField(QueryRowType Row, uinteger Field, uinteger Type, QueryFieldType Dest)
{
	switch (Type)
	{
		case QUERY_FIELD_TYPE_STR :
		{
			strcpy(Dest, ((MYSQL_ROW)Row)[Field]);
			break;
		}

		case QUERY_FIELD_TYPE_INT32 :
		{
			int32 * Value = (int32 *)Dest;

			*Value = (int32)atoi(((MYSQL_ROW)Row)[Field]);
			break;
		}

		case QUERY_FIELD_TYPE_UINT32 :
		{
			uint32 * Value = (uint32 *)Dest;

			*Value = (uint32)strtoul(((MYSQL_ROW)Row)[Field], NULL, 10);
			break;
		}

		case QUERY_FIELD_TYPE_BOOL :
		{
			bool * Boolean = (bool *)Dest;

			*Boolean = (bool)(atoi(((MYSQL_ROW)Row)[Field]));
			break;
		}

		case QUERY_FIELD_TYPE_FLOAT :
		{
			float * Float = (float *)Dest;

			*Float = (float)atof(((MYSQL_ROW)Row)[Field]);
			break;
		}
	}
}

/////////////////////////////////////////////////////////////
/// Release a query
/////////////////////////////////////////////////////////////
void QueryRelease(QueryResultType Result)
{
	if (Result)
	{
		struct DataBase * DB = DataBaseGetDefault();

		// Check if the database is avaible and the query is valid
		if (DB && DB->Connection)
		{
			// Free it
			mysql_free_result((MYSQL_RES *)Result);
		}
	}
}
