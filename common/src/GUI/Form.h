/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GUI_FORM_H
#define GUI_FORM_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define GUI_FORM_SOUND_OPEN				"data/audio/ui/window_open.ogg"
#define GUI_FORM_SOUND_CLOSE			"data/audio/ui/window_close.ogg"
#define GUI_FORM_SOUND_FOCUS			"data/audio/ui/window_focus.ogg"

#define GUI_FORM_STYLE_1		0x01
#define GUI_FORM_STYLE_2		0x02

#define GUI_FORM_BACKGROUND		0x5C62
#define GUI_FORM_HBORDER		0x5C65
#define GUI_FORM_VBORDER		0x5C67
#define GUI_FORM_EDGE			0x5C69
#define GUI_FORM_CENTERED		-1

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct GuiForm
{
	uint32	Background;		///< Background style
};

////////////////////////////////////////////////////////////
// Widget forward declaration
////////////////////////////////////////////////////////////
struct GuiWidget;

////////////////////////////////////////////////////////////
// Create a form
////////////////////////////////////////////////////////////
struct GuiWidget * GuiFormCreate(uint32 Style, int32 X, int32 Y, int32 Width, int32 Height, uint32 FontSize, char * Title);

////////////////////////////////////////////////////////////
// Create a form by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiFormCreateDefault();

////////////////////////////////////////////////////////////
// Render a form
////////////////////////////////////////////////////////////
void GuiFormRender(struct GuiWidget * Widget, struct Color4ub * BackgroundColor, struct Color4ub * FontColor);

#endif // GUI_FORM_H
