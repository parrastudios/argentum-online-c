/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef TYPES_H
#define TYPES_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Platform.h>

////////////////////////////////////////////////////////////
// Define floating point type precision
////////////////////////////////////////////////////////////
#define SINGLE_PRECISION	0x00
#define DOUBLE_PRECISION	0x01

#ifndef PRECISION_TYPE
#	if ARCH_FAMILY == ARCHITECTURE_64
#		define PRECISION_TYPE DOUBLE_PRECISION
#	elif ARCH_FAMILY == ARCHITECTURE_32
#		define PRECISION_TYPE SINGLE_PRECISION
#	endif
#endif

////////////////////////////////////////////////////////////
// Define portable fixed-size types
////////////////////////////////////////////////////////////
#include <limits.h>
#include <float.h>

// 8 bits integer types
#if UCHAR_MAX == 0xFF
    typedef signed   char int8;
    typedef unsigned char uint8;
#else
#	error No 8 bits integer type for this platform
#endif

cassert(sizeof(int8) == 1 && sizeof(uint8) == 1);

// 16 bits integer types
#if USHRT_MAX == 0xFFFF
    typedef signed   short int16;
    typedef unsigned short uint16;
#elif UINT_MAX == 0xFFFF
    typedef signed   int int16;
    typedef unsigned int uint16;
#elif ULONG_MAX == 0xFFFF
    typedef signed   long int16;
    typedef unsigned long uint16;
#else
#	error No 16 bits integer type for this platform
#endif

cassert(sizeof(int16) == 2 && sizeof(uint16) == 2);

// 32 bits integer types
#ifdef __palmos__
  typedef signed   long int32;
  typedef unsigned long uint32;
#else
#	if USHRT_MAX == 0xFFFFFFFF
		typedef signed   short int32;
		typedef unsigned short uint32;
#	elif UINT_MAX == 0xFFFFFFFF
		typedef signed   int int32;
		typedef unsigned int uint32;
#	elif ULONG_MAX == 0xFFFFFFFF
		typedef signed   long int32;
		typedef unsigned long uint32;
#	else
#		error No 32 bits integer type for this platform
#	endif
#endif

cassert(sizeof(int32) == 4 && sizeof(uint32) == 4);

// 64 bits integer types
#if COMPILER_TYPE == COMPILER_MSVC || COMPILER_TYPE == COMPILER_WATCOM || COMPILER_TYPE == COMPILER_BORLAND
	typedef signed   __int64 int64;
	typedef unsigned __int64 uint64;
#elif defined(__LP64__) || defined(__powerpc64__) || defined(__sparc__)
	typedef signed	 long int64;
	typedef unsigned long uint64;
#else
	typedef signed	 long long int64;
	typedef unsigned long long uint64;
#endif

cassert(sizeof(int64) == 8 && sizeof(uint64) == 8);

// Generic integer types
#if ARCH_FAMILY == ARCHITECTURE_32
	typedef int32	integer;
	typedef uint32	uinteger;
#elif ARCH_FAMILY == ARCHITECTURE_64
	typedef int64	integer;
	typedef uint64	uinteger;
#else
#	error No integer type for this platform
#endif

// Floating point types
typedef float	float32;

cassert(sizeof(float32) == 4);

typedef double	float64;

cassert(sizeof(float64) == 8);

// Generic real type
#if PRECISION_TYPE == SINGLE_PRECISION
	typedef float32 real;
#elif PRECISION_TYPE == DOUBLE_PRECISION
	typedef float64 real;
#else
#	error No floating point type for this platform
#endif

////////////////////////////////////////////////////////////
// Generic boolean macros
////////////////////////////////////////////////////////////
#ifndef bool
#	define bool		uint8
#endif
#ifndef false
#	define false	0x00
#endif
#ifndef true
#	define true		0x01
#endif

////////////////////////////////////////////////////////////
// Generic string macros
////////////////////////////////////////////////////////////
#ifndef NULLCHAR
#	define NULLCHAR '\0'
#endif

////////////////////////////////////////////////////////////
// Generic pointer macros
////////////////////////////////////////////////////////////
#ifndef NULL
#	ifndef __cplusplus
#		define NULL	((void *)0)
#	else
#		define NULL (0)
#	endif
#endif

////////////////////////////////////////////////////////////
// Generic offset evaluator macro
////////////////////////////////////////////////////////////
#ifndef OffsetOf
#	if (COMPILER_TYPE == COMPILER_GNUC && COMPILER_VERSION >= 400)
#		define OffsetOf(Type, Member) __builtin_offsetof(Type, Member)
#	else
#		define OffsetOf(Type, Member) ((integer) ((int8 *)&((Type *)0)->Member - (int8 *)((Type *)0)))
#	endif
#endif

#ifndef ContainerOf
#	if (COMPILER_TYPE == COMPILER_GNUC)
#		define ContainerOf(Ptr, Type, Member) ({ \
		const typeof(((Type*)0)->Member) * _Ptr = (Ptr); \
		(Type*)((int8*)_Ptr - OffsetOf(Type, Member)); })
#	else
#		define ContainerOf(Ptr, Type, Member) do { /* ... */ } while (0)
#	endif
#endif

////////////////////////////////////////////////////////////
// Generic bit evaluator macro
////////////////////////////////////////////////////////////
#define BitSetCount32Static(Value)		\
	(((((Value - ((Value >> 1) & 0x55555555)) & 0x33333333) + \
	(((Value - ((Value >> 1) & 0x55555555)) >> 2) & 0x33333333) + \
	(((Value - ((Value >> 1) & 0x55555555)) & 0x33333333) + \
	(((Value - ((Value >> 1) & 0x55555555)) >> 2) & 0x33333333) >> 4)) & \
	0x0F0F0F0F) * 0x01010101) >> 24

#define BitSetCount32(Value, Result)	\
	do { \
		Result = Value - ((Value >> 1) & 0x55555555); \
		Result = (Result & 0x33333333) + ((Result >> 2) & 0x33333333); \
		Result = (((Result + (Result >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24; \
	} while (0)

////////////////////////////////////////////////////////////
// Generic function parameter evaluator macro (avoid warning)
////////////////////////////////////////////////////////////
#ifndef UnusedParam
#	define UnusedParam(arg) do { (void)arg; } while (0)
#endif

////////////////////////////////////////////////////////////
// Generic struct member initializer macro
////////////////////////////////////////////////////////////
#ifndef TypeInit
//	Initialization must be done in same order as
//	in structure definition to allow backward compatibility
#	if (LANGUAGE_VERSION_TYPE == LANGUAGE_VERSION_C99 || LANGUAGE_VERSION_TYPE == LANGUAGE_VERSION_C11)
#		define TypeInit(Member, Value)				.Member = Value
#		define ExprInit(Member, ...)				.Member = { __VA_ARGS__ }
#		define UnionInit(Name, Member, Value)		.Name.Member = Value
#		define ArrayInit(Position, Value)			[Position] = Value
#	else
#		define TypeInit(Member, Value)				Value
#		define ExprInit(Member, ...)				{ __VA_ARGS__ }
#		define UnionInit(Name, Member, Value)		(void *)Value
#		define ArrayInit(Position, Value)			Value
#	endif
#endif

#endif // TYPES_H
