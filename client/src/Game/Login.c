/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Network/Protocol.h>

#include <Game/Language.h>
#include <Game/Login.h>
#include <Game/User.h>
#include <Game/Application.h>

#include <Audio/SoundManager.h>

#include <Graphics/TextureRender.h>

#include <Window/Window.h>

#include <GUI/WindowManager.h>

#include <System/Timer.h>
#include <System/Error.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define LOGIN_SOUND_THEME				"data/audio/1.ogg"

struct GuiWindow * LoginWindow, * AccountWindow;
struct GuiWidget * UserTextBox, * PasswordTextBox, * AccountList;
struct GuiWidget * LevelLabel, * GoldLabel, * DeadLabel, * ModelRenderImage;

////////////////////////////////////////////////////////////
/// Register the login
////////////////////////////////////////////////////////////
void LoginRegister()
{
	// Login window
	GuiEventHandlerAdd("LogConnect_ButtonReleased", (void*)&LoginConnect_ButtonReleased);
	GuiEventHandlerAdd("LogExit_ButtonReleased", (void*)&LoginExit_ButtonReleased);

	// Account window
	GuiEventHandlerAdd("AccConnect_ButtonReleased", (void*)&AccountConnect_ButtonReleased);
	GuiEventHandlerAdd("AccExit_ButtonReleased", (void*)&AccountExit_ButtonReleased);
	GuiEventHandlerAdd("ListPlayers_ButtonReleased", (void*)ListPlayers_ButtonReleased);
}

////////////////////////////////////////////////////////////
/// Initialize the login
////////////////////////////////////////////////////////////
void LoginInitialize()
{
	// Obtain window references
	LoginWindow = GuiGetWindow("frmAccLogin");
	AccountWindow = GuiGetWindow("frmAccMain");

	// Obtain widget references
	UserTextBox = GuiWindowGetWidget(LoginWindow, "txtUser");
	PasswordTextBox = GuiWindowGetWidget(LoginWindow, "txtPassword");
	AccountList = GuiWindowGetWidget(AccountWindow, "lstPlayers");

	LevelLabel = GuiWindowGetWidget(AccountWindow, "lblLevel");
	GoldLabel = GuiWindowGetWidget(AccountWindow, "lblGold");
	DeadLabel = GuiWindowGetWidget(AccountWindow, "lblDead");

	// todo:
	ModelRenderImage = GuiWindowGetWidget(AccountWindow, "lstDemoModelRender");
}

////////////////////////////////////////////////////////////
/// Connect button released event
////////////////////////////////////////////////////////////
void LoginConnect_ButtonReleased(struct MouseButtonEvent * ButtonEvent)
{
	// Copy the data
	strcpy(UserGetData()->Account.Name, UserTextBox->Text);
	strcpy(UserGetData()->Account.Password, PasswordTextBox->Text);

	// Login
	ProtocolConnect(); // Show in render "Connecting..."
}

////////////////////////////////////////////////////////////
/// Exit button released event
////////////////////////////////////////////////////////////
void LoginExit_ButtonReleased(struct MouseButtonEvent * ButtonEvent)
{
	// Exit game
	GameSetStatus(GAME_DESTROY);
}

////////////////////////////////////////////////////////////
/// Open the login
////////////////////////////////////////////////////////////
void LoginOpen()
{
	if (LoginWindow && LoginWindow->hStatus == GUI_WINDOW_CLOSED)
	{
		// Play sound
		SoundBufferPlay(LOGIN_SOUND_THEME, false, 0.0f, 0.0f, 0.0f);

		// Open login
		GuiOpenWindow(LoginWindow);
	}
}

////////////////////////////////////////////////////////////
/// Renders the background of the login
////////////////////////////////////////////////////////////
void LoginRender()
{
	// Render logo
	TextureRenderGrh(LOGIN_GRH_LOGO,
					(WindowWidth / 2) - (IndexGetGrh(LOGIN_GRH_LOGO)->PixelWidth / 2),
					(100 - (IndexGetGrh(LOGIN_GRH_LOGO)->PixelHeight / 2)));
}

////////////////////////////////////////////////////////////
/// Close the login
////////////////////////////////////////////////////////////
void LoginClose()
{
	if (LoginWindow && LoginWindow->hStatus == GUI_WINDOW_OPENED)
	{
		// Stop sound
		SoundBufferStopByFile(LOGIN_SOUND_THEME);

		// Close the window
		GuiCloseWindow(LoginWindow);
	}
}

////////////////////////////////////////////////////////////
/// Connect button released event
////////////////////////////////////////////////////////////
void AccountConnect_ButtonReleased(struct MouseButtonEvent * ButtonEvent)
{
	// Get the selected player
	uint32 ItemSelected = AccountList->Attributes.List.ItemSelected;

	if (ItemSelected != GUI_LIST_INVALID_ITEM)
	{
		// Login the player selected
		AccountLoginPlayer();
	}
}

////////////////////////////////////////////////////////////
/// Exit button released event
////////////////////////////////////////////////////////////
void AccountExit_ButtonReleased(struct MouseButtonEvent * ButtonEvent)
{
	// Close account panel
	AccountClose();
}

////////////////////////////////////////////////////////////
/// Player list on selected item change event
////////////////////////////////////////////////////////////
void ListPlayers_ButtonReleased(struct MouseButtonEvent * ButtonEvent)
{
	char LevelText[0x20], GoldText[0x80], DeadText[0x10];
	uint32 ItemSelected = AccountList->Attributes.List.ItemSelected;

	if (ItemSelected != GUI_LIST_INVALID_ITEM)
	{
		sprintf(LevelText, "%d", UserGetData()->Account.Players[ItemSelected].Level);
		sprintf(GoldText, "%d", UserGetData()->Account.Players[ItemSelected].Gold);
		strcpy(DeadText, UserGetData()->Account.Players[ItemSelected].Dead ? LanguageMessage(MSG_YES) : LanguageMessage(MSG_NO));

		GuiLabelChangeCaption(LevelLabel, LevelText);
		GuiLabelChangeCaption(GoldLabel, GoldText);
		GuiLabelChangeCaption(DeadLabel, DeadText);
	}
}

////////////////////////////////////////////////////////////
/// Initialize the account
////////////////////////////////////////////////////////////
void AccountOpen()
{
	if (AccountWindow && AccountWindow->hStatus == GUI_WINDOW_CLOSED)
	{
		uinteger i;

		// Close login
		GuiCloseWindow(LoginWindow);

		// Open account main
		GuiOpenWindow(AccountWindow);

		// Fill information into account list
		for (i = 0; i < UserGetData()->Account.Count; i++)
		{
			GuiListAddItem(&AccountList->Attributes.List, &UserGetData()->Account.Players[i].Name[0]);
		}

		// Select the first item
		GuiListSetItemSelectedById(AccountList, 0);

		// Change status
		GameSetStatus(GAME_ACCOUNT);
	}
}

////////////////////////////////////////////////////////////
/// Renders the background of the account panel
////////////////////////////////////////////////////////////
void AccountRender()
{
	TextureRenderGrh(LOGIN_GRH_LOGO,
					(WindowWidth / 2) - (IndexGetGrh(LOGIN_GRH_LOGO)->PixelWidth / 2),
					(100 - (IndexGetGrh(LOGIN_GRH_LOGO)->PixelHeight / 2)));
}

////////////////////////////////////////////////////////////
/// Login a player of an account
////////////////////////////////////////////////////////////
void AccountLoginPlayer()
{
	// Get current player logged
	UserGetData()->Account.PlayerLogged = AccountList->Attributes.List.ItemSelected;

	PacketSendLoginPlayer();
}

////////////////////////////////////////////////////////////
/// Close the account
////////////////////////////////////////////////////////////
void AccountClose()
{
	PacketSendLogoutAccount();
}

////////////////////////////////////////////////////////////
/// Hide account panel and run
////////////////////////////////////////////////////////////
void AccountUnload()
{
	if (AccountWindow && AccountWindow->hStatus == GUI_WINDOW_OPENED)
	{
		// Close the window
		GuiCloseWindow(AccountWindow);

		// Go to game
		GameSetStatus(GAME_RUN);
	}
}

////////////////////////////////////////////////////////////
/// Exit from account
////////////////////////////////////////////////////////////
void AccountExit()
{
	if (AccountWindow && AccountWindow->hStatus == GUI_WINDOW_OPENED)
	{
		// Clear account list
		GuiListClear(&AccountList->Attributes.List);

		// Close the window
		GuiCloseWindow(AccountWindow);

		// Go back to login panel
		GameSetStatus(GAME_LOGIN);
	}
}
