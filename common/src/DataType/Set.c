/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/Set.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define SET_MAX_LOAD_FACTOR		0.75f
#define SET_MID_LOAD_FACTOR		0.40f
#define SET_MIN_LOAD_FACTOR		0.0f
#define SET_MIN_BUCKETS_SIZE	128

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define SetGetNodeSize(SetData)	\
	(sizeof(struct SetNodeType) + SetData->TypeSize)

#define SetGetBucketImpl(Buckets, Index, NodeSize) \
	(SetNode)(((uint8 *)(Buckets)) + (NodeSize * Index))

#define SetGetBucket(SetData, Index) \
	SetGetBucketImpl(SetData->Buckets, Index, SetGetNodeSize(SetData))

#define SetGetBucketValuePtr(Node) \
	(void *)((SetNode)(Node + 1))

#define SetIsNodeValid(Node) \
	(Node->Hash != SET_HASH_INVALID)

#define SetIsNodeNotValid(Node) \
	(Node->Hash == SET_HASH_INVALID)

////////////////////////////////////////////////////////////
// Data structure
////////////////////////////////////////////////////////////
struct SetNodeType
{
	SetNode			Next;			///< Next node entry
	SetHashType		Hash;			///< Hash node key
};

struct SetType
{
	uinteger		Size;			///< Current number of buckets used
	uinteger		TypeSize;		///< Size of data type that holds the set
	uinteger		BucketsSize;	///< Number of buckets set can hold
	SetNode			Buckets;		///< Array of buckets
	SetHashCallback	HashFunc;		///< Hash function pointer
	CompareCallback	CompareFunc;	///< Compare function pointer
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Reset a set node
////////////////////////////////////////////////////////////
void SetNodeReset(SetNode Node, uinteger TypeSize)
{
	if (Node)
	{
		Node->Next = NULL;
		Node->Hash = SET_HASH_INVALID;
		MemorySet(SetGetBucketValuePtr(Node), 0, TypeSize);
	}
}

////////////////////////////////////////////////////////////
///  If is sure that the hash function cant't return the same
///  value for two different objects, the CompareFunc
///  argument can be set to NULL. In this case the hash value
///  will be used to compare if two objects are the same.
///  
///  If a CompareFunc is passed it will be used for final
///  compare if two object are equal. Only the operator
///  OPERATOR_EQUAL needs to be implemented by CompareFunc.
////////////////////////////////////////////////////////////
SetNode SetFindNode(Set SetData, void * Value, SetHashType Hash)
{
	if (SetData && Hash != SET_HASH_INVALID)
	{
		uinteger Bucket = Hash % SetData->BucketsSize;

		if (SetData->CompareFunc && Value)
		{
			SetNode Node;

			for (Node = SetGetBucket(SetData, Bucket); Node != NULL; Node = Node->Next)
			{
				if (Node->Hash == Hash && (SetData->CompareFunc(SetGetBucketValuePtr(Node), Value)))
				{
					return Node;
				}
			}
		}
		else
		{
			SetNode Node;

			for (Node = SetGetBucket(SetData, Bucket); Node != NULL; Node = Node->Next)
			{
				if (Node->Hash == Hash)
				{
					return Node;
				}
			}
		}
	}

    return NULL;
}

////////////////////////////////////////////////////////////
/// Create a set
////////////////////////////////////////////////////////////
Set SetCreate(uinteger TypeSize, SetHashCallback HashFunc, CompareCallback CompareFunc)
{
	if (HashFunc)
	{
		Set SetData = (struct SetType *)MemoryAllocate(sizeof(struct SetType));

		if (SetData)
		{
			uinteger BucketsMemSize;

			SetData->TypeSize = TypeSize;

			BucketsMemSize = SetGetNodeSize(SetData) * SET_MIN_BUCKETS_SIZE;

			SetData->BucketsSize = SET_MIN_BUCKETS_SIZE;

			SetData->Buckets = (SetNode)MemoryAllocate(BucketsMemSize);

			if (SetData->Buckets)
			{
				uinteger i;

				for (i = 0; i < SetData->BucketsSize; ++i)
				{
					SetNode Bucket = SetGetBucket(SetData, i);

					// Reset bucket
					SetNodeReset(Bucket, SetData->TypeSize);
				}

				SetData->Size = 0;
				SetData->HashFunc = HashFunc;
				SetData->CompareFunc = CompareFunc;

				return SetData;
			}
			else
			{
				MemoryDeallocate(SetData);
			}
		}
	}

    return NULL;
}

////////////////////////////////////////////////////////////
/// Create a set without comparing function
////////////////////////////////////////////////////////////
Set SetCreateHashOnly(uinteger TypeSize, SetHashCallback HashFunc)
{
    return SetCreate(TypeSize, HashFunc, NULL);
}

////////////////////////////////////////////////////////////
/// Destroy a set
////////////////////////////////////////////////////////////
void SetDestroy(Set SetData)
{
    if (SetData)
    {
        SetClear(SetData);

        MemoryDeallocate(SetData->Buckets);

		MemoryDeallocate(SetData);
    }
}

////////////////////////////////////////////////////////////
/// Get number of elements a set contains
////////////////////////////////////////////////////////////
uinteger SetGetSize(Set SetData)
{
	return SetData->Size;
}

////////////////////////////////////////////////////////////
/// Insert a new element into a set
////////////////////////////////////////////////////////////
bool SetInsert(Set SetData, void * Value)
{
    SetHashType Hash = SetData->HashFunc(Value);
    
	uinteger Bucket = Hash % SetData->BucketsSize;

    SetNode Node = SetFindNode(SetData, Value, Hash);

	if (Node == NULL)
	{
		SetNode NewNode;

		Node = SetGetBucket(SetData, Bucket);

		if (Node->Hash == SET_HASH_INVALID && Node->Next == NULL)
		{
			NewNode = Node;
		}
		else
		{
			NewNode = MemoryAllocate(SetGetNodeSize(SetData));

			if (NewNode)
			{
				while (Node->Next != NULL)
				{
					Node = Node->Next;
				}

				// Set node reference
				Node->Next = NewNode;
			}
			else
			{
				return false;
			}
		}

		// Initialize new node
		NewNode->Next = NULL;

		NewNode->Hash = Hash;

		MemoryCopy(SetGetBucketValuePtr(NewNode), Value, SetData->TypeSize);

		++SetData->Size;

		// Rehash if need
		SetRehash(SetData);

		return true;
    }

	return false;
}

////////////////////////////////////////////////////////////
/// Remove an element by its hash index
////////////////////////////////////////////////////////////
void SetRemoveByHash(Set SetData, SetHashType Hash)
{
	if (SetData && Hash != SET_HASH_INVALID)
	{
		uinteger Bucket = Hash % SetData->BucketsSize;

		SetNode Node, Prev = NULL;

		// Iterate through buckents and find the node
		for (Node = SetGetBucket(SetData, Bucket); Node != NULL; Node = Node->Next)
		{
			if (Node->Hash == Hash)
			{
				break;
			}

			Prev = Node;
		}

		if (Node)
		{
			if (Prev == NULL)
			{
				// Node is the first, it cannot be deallocated
				if (Node->Next)
				{
					// If there is a next, copy next to current
					MemoryCopy(SetGetBucketValuePtr(Node), SetGetBucketValuePtr(Node->Next), SetData->TypeSize);
					Node->Hash = Node->Next->Hash;

					// Remove from list
					Node->Next = Node->Next->Next;
				}
				else
				{
					// Otherwise, reset it
					SetNodeReset(Node, SetData->TypeSize);
				}
			}
			else
			{
				// Otherwise remove from list and deallocate node
				Prev->Next = Node->Next;

				MemoryDeallocate(Node);
			}

			// Decrement set size
			--SetData->Size;

			// Rehash if need
			SetRehash(SetData);
		}
	}
}

////////////////////////////////////////////////////////////
/// Clear all elements of a set
////////////////////////////////////////////////////////////
void SetClear(Set SetData)
{
	if (SetData)
	{
		uinteger i;

		for (i = 0; i < SetData->BucketsSize; ++i)
		{
			SetNode Next, Iterator, Bucket = SetGetBucket(SetData, i);

			for (Next = NULL, Iterator = Bucket->Next; Iterator != NULL; Iterator = Next)
			{
				Next = Iterator->Next;

				// Destroy current node
				MemoryDeallocate(Iterator);
			}

			SetNodeReset(Bucket, SetData->TypeSize);
		}

		SetData->Size = 0;

		SetRehash(SetData);
	}
}

////////////////////////////////////////////////////////////
/// Check if set contains a value
////////////////////////////////////////////////////////////
bool SetHasValue(Set SetData, void * Value)
{
    SetHashType Hash = SetData->HashFunc(Value);
	
	uinteger Bucket = Hash % SetData->BucketsSize;

    SetNode Node = SetFindNode(SetData, Value, Hash);

    return (Node != NULL);
}

////////////////////////////////////////////////////////////
/// Returns the value of set by its hash
////////////////////////////////////////////////////////////
void * SetGetValueByHash(Set SetData, SetHashType Hash)
{
	SetNode Node = SetFindNode(SetData, NULL, Hash);

	if (Node)
	{
		return SetGetBucketValuePtr(Node);
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Reallocate if need and reindex all buckets
////////////////////////////////////////////////////////////
void SetRehash(Set SetData)
{
	float LoadFactor = (float)SetData->Size / (float)SetData->BucketsSize;

    if (LoadFactor > SET_MAX_LOAD_FACTOR || LoadFactor < SET_MIN_LOAD_FACTOR)
    {
        uinteger NewBucketsSize = (uinteger)(SetData->Size / SET_MID_LOAD_FACTOR);

        if (NewBucketsSize < SET_MIN_BUCKETS_SIZE)
        {
            NewBucketsSize = SET_MIN_BUCKETS_SIZE;
        }

        if (NewBucketsSize != SetData->BucketsSize)
        {
			// Calculate new bucket size
			uinteger NewBucketsMemSize = SetGetNodeSize(SetData) * NewBucketsSize;
            
			// Allocate new buckets
			SetNode NewBuckets = (SetNode)MemoryAllocate(NewBucketsMemSize);

			if (NewBuckets)
			{
				uinteger Bucket;

				// Reset new buckets
				for (Bucket = 0; Bucket < NewBucketsSize; ++Bucket)
				{
					SetNode Node = SetGetBucketImpl(NewBuckets, Bucket, SetGetNodeSize(SetData));

					SetNodeReset(Node, SetData->TypeSize);
				}

				// Copy and rehash old buckets into the new ones
				for (Bucket = 0; Bucket < SetData->BucketsSize; ++Bucket)
				{
					SetNode Node, NewNode;

					for (Node = SetGetBucket(SetData, Bucket); Node != NULL; Node = Node->Next)
					{
						if (SetIsNodeValid(Node))
						{
							uinteger NewBucket = Node->Hash % NewBucketsSize;

							NewNode = SetGetBucketImpl(NewBuckets, NewBucket, SetGetNodeSize(SetData));

							NewNode->Hash = Node->Hash;

							MemoryCopy(SetGetBucketValuePtr(NewNode), SetGetBucketValuePtr(Node), SetData->TypeSize);
						}
					}
				}

				// Destroy old buckets
				MemoryDeallocate(SetData->Buckets);

				// Assing new buckets
				SetData->BucketsSize = NewBucketsSize;
				SetData->Buckets = NewBuckets;
			}
        }
    }
}

////////////////////////////////////////////////////////////
/// Set iterator begin
////////////////////////////////////////////////////////////
SetNode SetIteratorBegin(Set SetData, SetIterator Iterator)
{
	if (SetData && Iterator)
	{
		Iterator->SetData = SetData;
		Iterator->Bucket = 0;
		Iterator->Node = SetGetBucket(SetData, Iterator->Bucket);

		return Iterator->Node;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set iterator end
////////////////////////////////////////////////////////////
SetNode SetIteratorEnd(Set SetData, SetIterator Iterator)
{
	if (SetData && Iterator)
	{
		Iterator->SetData = SetData;
		Iterator->Bucket = SetData->BucketsSize - 1;
		Iterator->Node = SetGetBucket(SetData, Iterator->Bucket);

		while (Iterator->Node->Next != NULL)
		{
			Iterator->Node = Iterator->Node->Next;
		}

		return Iterator->Node;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set iterator retreive next
////////////////////////////////////////////////////////////
SetNode SetIteratorNext(SetIterator Iterator)
{
	if (Iterator && Iterator->SetData)
	{
		SetNode Node = Iterator->Node;

		do
		{
			if (Node->Next == NULL)
			{
				uinteger Bucket = (Iterator->Bucket + 1);

				if (Bucket < Iterator->SetData->Size)
				{
					Iterator->Bucket = Bucket;

					Node = SetGetBucket(Iterator->SetData, Iterator->Bucket);
				}
				else
				{
					Node = NULL;
				}
			}
			else
			{
				Node = Node->Next;
			}

		} while (Node && SetIsNodeNotValid(Node));

		Iterator->Node = Node;

		return Iterator->Node;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set iterator retreive previous
////////////////////////////////////////////////////////////
SetNode SetIteratorPrev(SetIterator Iterator)
{
	// TODO

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set iterator retreive value
////////////////////////////////////////////////////////////
void * SetIteratorValue(SetIterator Iterator)
{
	if (Iterator && SetIsNodeValid(Iterator->Node))
	{
		return SetGetBucketValuePtr(Iterator->Node);
	}

	return NULL;
}
