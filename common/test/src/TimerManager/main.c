/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/TimerManager.h>
#include <System/Platform.h>
#include <stdio.h>

void timer0() {
	printf("0\n");
}

void timer1() {
	printf("1\n");
}

void timer2() {
	printf("2\n");
}

void timer3() {
	printf("3\n");
}

void timer4() {
	printf("4\n");
}

void timer5() {
	printf("5\n");
}

////////////////////////////////////////////////////////////
// Program entry point
////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
	uint32 timers_id[5];

	TimerManagerInitialize(1.0f); // 1 ms

	printf("Added timer0----------------\n");

	timers_id[0] = TimerManagerAdd(350.0f, &timer0, false);

	SystemSleep(100.0f);

	printf("Added timer1----------------\n");

	timers_id[1] = TimerManagerAdd(200.0f, &timer1, true);

	SystemSleep(100.0f);

	printf("Added timer2----------------\n");

	timers_id[2] = TimerManagerAdd(100.0f, &timer2, true);

	printf("Set timer1 interval to 10ms-\n");

	TimerManagerSet(timers_id[1], 10.0f);
	
	SystemSleep(100.0f);

	TimerManagerRemove(timers_id[1]);

	printf("Removed timer1----------------\n");

	SystemSleep(100.0f);

	printf("Added timer3----------------\n");

	timers_id[3] = TimerManagerAdd(100.0f, &timer3, true);

	SystemSleep(500.0f);

	TimerManagerRemove(timers_id[3]);

	printf("Removed timer3----------------\n");

	SystemSleep(200.0f);

	TimerManagerRemove(timers_id[0]);

	printf("Added timer4----------------\n");

	timers_id[4] = TimerManagerAdd(50.0f, &timer4, true);

	SystemSleep(400.0f);

	TimerManagerRemove(timers_id[2]);

	printf("Removed timer2----------------\n");

	SystemSleep(100.0f);
	
	TimerManagerDestroy();

	printf("Removed all_timers------------\n");

	return 0;
}
