/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/Shader.h>
#include <Graphics/ShaderLoader.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

bool ShaderCreate(struct ShaderType * Shader, GLenum Type, const char * Path)
{
	if (Shader)
	{
		ShaderInitialize(Shader, Type);

		// Create shader and set the source
		if (ShaderLoadFromFile(Shader, Path))
		{
			// Compile shader
			ShaderCompile(Shader);

			return true;
		}

		// Could not read shader, destroy it
		ShaderDestroy(Shader);
	}

	return false;
}

void ShaderInitialize(struct ShaderType * Shader, GLenum Type)
{
	if (Shader)
	{
		// Set shader type
		Shader->Type = Type;

		// Set compiled flag
		Shader->Compiled = false;

		// Create the shader object
		Shader->Id = DeviceGetExt()->glCreateShader(Shader->Type);
	}
}

void ShaderSetSource(struct ShaderType * Shader, const char * Source, uinteger Length)
{
	if (Shader && !Shader->Compiled)
	{
		const GLchar * Strings[1] = { (const GLchar *)Source };
		GLint Lengths[1] = { (GLint)Length };

		// Check version
		// ..

		// Set shader source
		DeviceGetExt()->glShaderSource(Shader->Id, 1, Strings, Lengths);
	}
}

bool ShaderCheckError(struct ShaderType * Shader, GLenum State)
{
	if (Shader)
	{
		GLint Result = 0;

		// Retrieve the result of the operation
		DeviceGetExt()->glGetShaderiv(Shader->Id, State, &Result);

		// Check for errors
		if (Result == 0)
		{
			GLchar Error[1024] = { 0 };

			DeviceGetExt()->glGetShaderInfoLog(Shader->Id, sizeof(Error), NULL, Error);

			// Handle error

			return false;
		}

		return true;
	}

	return false;
}

bool ShaderCompile(struct ShaderType * Shader)
{
	if (!Shader->Compiled)
	{
		DeviceGetExt()->glCompileShader(Shader->Id);

		if (ShaderCheckError(Shader, GL_COMPILE_STATUS))
		{
			Shader->Compiled = true;

			return true;
		}
	}

	return false;
}

void ShaderDestroy(struct ShaderType * Shader)
{
	if (Shader)
	{
		if (Shader->Id != 0)
		{
			// Delete shader
			DeviceGetExt()->glDeleteShader(Shader->Id);
		}

		Shader->Compiled = false;

		Shader->Type = 0;
	}
}
