/*
***********************************************************************************
 * IqeB_Display.cpp

 Display the model.

 This file is based on the 'iqeview.c' from Tor Andersson.

 2014-03-21 RR: * Started to rework the iqeview.c program from Tor Anderssons
                  '3d Asset Tools' als IqeBrowser.

 .......

 2015-01-07 RR: Reworked for used by FLTK GUI.


*/

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Gl_Window.H>
#include <FL/gl.h>
#include <FL/fl_draw.H>
#  ifdef __APPLE__
#    include <OpenGL/glu.h>
#  else
#    include <GL/glu.h>
#  endif

#include <math.h>

#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE 0x809D
#endif

/************************************************************************************
 * Colors
 */

unsigned int IqeB_DispCol_BACKGROUND   = 97;             // Background
unsigned int IqeB_DispCol_VERSION      = FL_RED;         // Version text
unsigned int IqeB_DispCol_INFOS        = FL_WHITE;       // Infos
unsigned int IqeB_DispCol_NORMALS      = FL_MAGENTA;     // Normals

unsigned int IqeB_DispCol_GROUND_PLANE = 18;             // Ground plane grid color
unsigned int IqeB_DispCol_AXIS         = FL_CYAN;        // Ground plane axis color
unsigned int IqeB_DispCol_BOX          = FL_GREEN;       // Reference box
unsigned int IqeB_DispCol_BOX_GROUND   = 59;             // Reference box ground level

unsigned int IqeB_DispCol_SKELETON     = FL_YELLOW;      // Skeleton
unsigned int IqeB_DispCol_JOINTS       = FL_YELLOW;      // Joints
unsigned int IqeB_DispCol_JOINTS_SEL   = FL_RED;         // Selected joints
unsigned int IqeB_DispCol_JOINT_NAMES  = 91;             // Joint names

/************************************************************************************
 * IqeB_DispDisplayInit
 *
 * Display the model
 */

void IqeB_DispSetOpenGLColor4f( unsigned int ColorFLTK, float alpha)
{
  unsigned Col;
  float r, g, b;

  if( ColorFLTK & 0xffffff00) {  // Is RGB color
    Col = ColorFLTK;
  } else {
    Col = Fl::get_color( (Fl_Color)ColorFLTK);
  }

  r = ((Col >> 24) & 255) / 255.0;
  g = ((Col >> 16) & 255) / 255.0;
  b = ((Col >>  8) & 255) / 255.0;

  glColor4f( r, g, b, alpha);
}

static void IqeB_DispSetOpenGLColor4f2( unsigned int ColorFLTK1, unsigned int ColorFLTK2, float alpha)
{
  unsigned Col1, Col2;
  float r1, g1, b1;
  float r2, g2, b2;

  if( ColorFLTK1 & 0xffffff00) {  // Is RGB color
    Col1 = ColorFLTK1;
  } else {
    Col1 = Fl::get_color( (Fl_Color)ColorFLTK1);
  }

  r1 = ((Col1 >> 24) & 255) / 255.0;
  g1 = ((Col1 >> 16) & 255) / 255.0;
  b1 = ((Col1 >>  8) & 255) / 255.0;

  if( ColorFLTK2 & 0xffffff00) {  // Is RGB color
    Col2 = ColorFLTK2;
  } else {
    Col2 = Fl::get_color( (Fl_Color)ColorFLTK2);
  }

  r2 = ((Col2 >> 24) & 255) / 255.0;
  g2 = ((Col2 >> 16) & 255) / 255.0;
  b2 = ((Col2 >>  8) & 255) / 255.0;

  glColor4f( (r1 + r2) * 0.5, (g1 + g2) * 0.5, (b1 + b2) * 0.5, alpha);
}

void IqeB_DispSetOpenGLClearColor( unsigned int ColorFLTK, float alpha)
{
	float clearcolor[4];
	static unsigned int LastColorFLTK = 0x47110815;  // For shure, this is no FLTK  color
  unsigned Col;

  if( LastColorFLTK == ColorFLTK) {   // Not changed since last call

    return;
  }

  LastColorFLTK = ColorFLTK;

  if( ColorFLTK & 0xffffff00) {  // Is RGB color
    Col = ColorFLTK;
  } else {
    Col = Fl::get_color( (Fl_Color)ColorFLTK);
  }

  clearcolor[ 0] = ((Col >> 24) & 255) / 255.0;
  clearcolor[ 1] = ((Col >> 16) & 255) / 255.0;
  clearcolor[ 2] = ((Col >>  8) & 255) / 255.0;
  clearcolor[ 3] = alpha;

  glClearColor( clearcolor[ 0], clearcolor[ 1], clearcolor[ 2], clearcolor[ 3]);
}

/************************************************************************************
 * Models to draw on GUI
 */

// model container

T_ModelContainer ModelContainer[ MODEL_CONTAINER_MAX]; // the model container
int    ModelContainer_n = 0;                           // number of models in container

// Clipboard
struct IQE_model *pClipBoardModel;              // Clipboard model
char   ClipBoardModelName[ MAX_FILENAME_LEN];     // drawing model, file name
char   ClipBoardModelFullName[ MAX_FILENAME_LEN]; // drawing model, path and file name

// Draw this model
struct IQE_model *pDrawModel = NULL;            // drawing this model
struct anim *pDrawModelCurAnim = NULL;          // current selected animation
int    DrawModelCurframe = 0;                   // Draw this frame of current model
char   DrawModelSelName[ MAX_FILENAME_LEN];     // drawing model, file name
char   DrawModelSelFullName[ MAX_FILENAME_LEN]; // drawing model, path and file name

static float curtime = 0;
static unsigned int lasttime = 0;

// Draw mouse coordinates and things
static char DrawMouseCoordinate[ 512];
static char DrawMouseEditMode[ 512];
static char DrawMouseEditModeErr[ 512];

static int  DrawMouseAnimEnabled = false;        // != 0 to draw mouse animation things
static int  DrawMouseAnimNunmLines = 0;          // number of lines to draw, 0 = off, 1 or 2
static int  DrawMouseAnimFrameNr = 0;            // Frame of pose to change
static int  DrawMouseAnimPoseNr  = 0;            // Pose to change
static struct pose *DrawMouseAnim_pPose;         // Point to pose to change
static struct pose DrawMouseAnim_PoseLatch;      // Latch pose to change
static int  DrawMouseAnim_ViewMode = 0;          // Latch viewmode

static int  DrawMouseAnimJX, DrawMouseAnimJY;    // Ponter joint of line to draw
static int  DrawMouseAnimP1X, DrawMouseAnimP1Y;  // Point 1 of line to draw
static int  DrawMouseAnimP2X, DrawMouseAnimP2Y;  // Point 2 of line to draw
static unsigned int DrawMouseAnimLineCol1;       // Color of line 1 to draw
static unsigned int DrawMouseAnimLineCol2;       // Color of line 2 to draw

// Draw this message

static float DrawMessageColR, DrawMessageColG, DrawMessageColB;
static char DrawMessageText1[ 1024];
static char DrawMessageText2[ 1024];
static char DrawMessageText3[ 1024];

// Background iamge
static unsigned int DrawBackGroundImageRef = 0;  // Reference to background image

// Redraw handling, see IqeB_DispTestRedraw()

static unsigned int IqeB_Disp_LastDrawTime = 0;   // Timestampe, last draw done. Set do 0 to redraw as soon as possible

// display styles

int IqeB_DispStyle_ViewMode = IQE_VIEWMODE_PERSP;      // plane/orthogonal/perspective camera
int IqeB_DispStyle_NumViewPorts = IQE_VIEW_PORT_NUM_1; // Number of view ports

int IqeB_DispStyle_MouseMode = IQE_GUI_MOUSE_MODE_NONE; // if >= 0, selected joint
int IqeB_DispStyle_MeshPartSelected = -1;    // if >= 0, selected mesh part
int IqeB_DispStyle_JointSelected = -1;       // if >= 0, selected joint
int IqeB_DispStyle_JointMouseNearby = -1;    // if >= 0, mouse is near this joint

int IqeB_DispStyle_doplane = 1;              // draw gound plane
int IqeB_DispStyle_dowire = 0;               // draw model wireframe
int IqeB_DispStyle_donormals = 0;            // draw normals
int IqeB_DispStyle_dotexture = 1;            // draw model with textures
int IqeB_DispStyle_dobackface = 1;           // draw model with NO backface culling
int IqeB_DispStyle_doskeleton = 1;           // draw skeleton
int IqeB_DispStyle_doJointNames = 0;         // draw the joint names of a skeleton
int IqeB_DispStyle_doBlendWeightColors = 0;  // draw model with blend weight colors
int IqeB_DispStyle_doAnimations = 1;         // do animations
int IqeB_DispStyle_doplay = 1;               // animation play/pause
int IqeB_DispStyle_doPlayOnce = 0;           // animation 'play once' only

// Viewports drawn

typedef struct {
  int ViewLayout;      // Layout for 1: 1 view or 2: 4 views, one of the 2D windows
  int ViewMode;        // One of the viewmode displays
  int x, y, w, h;      // Position and size of view

	// Temp 2D drawing pos. of joint
	int  SkelDrawPosX[ MAXBONE];  // temp use for draing bone 2D coordinates on 3D screen
	int  SkelDrawPosY[ MAXBONE];  // temp use for draing bone 2D coordinates on 3D screen
} T_ViewPort;

#define IQE_VIEW_PORT_MAX 4             // Max 4 viewports

static T_ViewPort ViewPortLayout[ IQE_VIEW_PORT_MAX];
static int ViewPorts_n = 0;             // Number of active viewports

// Mouse things

static int mousex, mousey;
static int  ViewPort_MouseX, ViewPort_MouseY, ViewPort_MouseNum;

/************************************************************************************
 * IqeB_Disp_GetTickCount
 *
 * Returns time im ms since system start
 *
 * NOTE: Never returns 0 (0 can be used as flag).
 */

unsigned int IqeB_Disp_GetTickCount()
{

#ifdef _WIN32
	unsigned int curtime = GetTickCount();

	if( curtime == 0) {    // is zero
    curtime = 1;         // set to 1, never return zero!
	}

  return curtime;
#else  // UNIX implementation
	struct timeval tp;
	struct timezone tzp;
	static int secbase = 0;

	gettimeofday(&tp, &tzp);

	if (!secbase) {
		secbase = tp.tv_sec;
		curtime = tp.tv_usec / 1000;
	} else {

	  curtime =  (tp.tv_sec - secbase) * 1000 + tp.tv_usec / 1000;
  }

	if( curtime == 0) {    // is zero
    curtime = 1;         // set to 1, never return zero!
	}

  return curtime;
}
#endif
}

/************************************************************************************
 * IqeB_DispAnimateModel()
 *
 * animage model for display
 */

static mat4 loc_pose_matrix[MAXBONE];
static mat4 abs_pose_matrix[MAXBONE];

void IqeB_DispAnimateModel( struct IQE_model *model, struct anim *anim, int frame)
{
  mat4 skin_matrix[MAXBONE];
	struct skel *skel = model->skel;
	struct mesh *mesh = model->mesh;

	if( anim == NULL) {     // No animation

    // Remvove anmiaton position/normals (if any)

	  if( model->mesh->aposition)   free( model->mesh->aposition);
	  if( model->mesh->anormal)     free( model->mesh->anormal);

	  model->mesh->aposition    = NULL;
	  model->mesh->anormal      = NULL;

    // and update for skeleton display
 	  if( model->skel != NULL && model->skel->joint_count > 0) {    // have all we need

      // overwrite last (possible) animation in 'abs_pose_matrix'
      // with skeleton pose.
		  memcpy(abs_pose_matrix, mesh->abs_bind_matrix, sizeof abs_pose_matrix);
	  }

    return;
	}

	frame = CLAMP(frame, 0, anim->len-1);

	calc_matrix_from_pose(loc_pose_matrix, anim->data[frame], skel->joint_count);
	calc_abs_matrix(abs_pose_matrix, loc_pose_matrix, skel);
	calc_mul_matrix(skin_matrix, abs_pose_matrix, mesh->inv_bind_matrix, skel->joint_count);

	if( mesh->vertex_count == 0 || mesh->blendindex == NULL || mesh->blendweight == NULL) { // security test

    return;
	}

	if (!mesh->aposition) mesh->aposition = (float *)malloc(sizeof(float) * mesh->vertex_count * 3);
	if (!mesh->anormal) mesh->anormal = (float *)malloc(sizeof(float) * mesh->vertex_count * 3);

	int *bi = mesh->blendindex;
	float *bw = mesh->blendweight;
	float *sp = mesh->position;
	float *sn = mesh->normal;
	float *dp = mesh->aposition;
	float *dn = mesh->anormal;
	int n = mesh->vertex_count;

	while (n--) {
		int i;
		dp[0] = dp[1] = dp[2] = 0;
		dn[0] = dn[1] = dn[2] = 0;
		for (i = 0; i < 4; i++) {
			vec3 tp, tn;

			if( bw[i] == 0.0) {   // end of list
        break;
			}

			mat_vec_mul(tp, skin_matrix[bi[i]], sp);
			mat_vec_mul_n(tn, skin_matrix[bi[i]], sn);
			vec_scale(tp, tp, bw[i]);
			vec_scale(tn, tn, bw[i]);
			vec_add(dp, dp, tp);
			vec_add(dn, dn, tn);
		}
		bi += 4; bw += 4;
		sp += 3; sn += 3;
		dp += 3; dn += 3;
	}
}

/************************************************************************************
 * drawskeleton()
 */

/*
============
R_SetSky
NOTE: R_DrawSkyBox was called before
============
*/

static void My__gluMultMatrixVecd(const GLdouble matrix[16], const GLdouble in[4],
                                  GLdouble out[4])
{
    int i;

    for (i=0; i<4; i++)
    {
        out[i] =
            in[0] * matrix[0*4+i] +
            in[1] * matrix[1*4+i] +
            in[2] * matrix[2*4+i] +
            in[3] * matrix[3*4+i];
    }
}

static GLint
My_gluProject(GLdouble objx, GLdouble objy, GLdouble objz,
           const GLdouble modelMatrix[16],
           const GLdouble projMatrix[16],
           const GLint viewport[4],
           GLdouble *winx, GLdouble *winy, GLdouble *winz)
{
    double in[4];
    double out[4];

    in[0]=objx;
    in[1]=objy;
    in[2]=objz;
    in[3]=1.0;
    My__gluMultMatrixVecd(modelMatrix, in, out);
    My__gluMultMatrixVecd(projMatrix, out, in);
    if (in[3] == 0.0) return(GL_FALSE);
    in[0] /= in[3];
    in[1] /= in[3];
    in[2] /= in[3];
    /* Map x, y and z to range 0-1 */
    in[0] = in[0] * 0.5 + 0.5;
    in[1] = in[1] * 0.5 + 0.5;
    in[2] = in[2] * 0.5 + 0.5;

    /* Map x,y to viewport */
    in[0] = in[0] * viewport[2] + viewport[0];
    in[1] = in[1] * viewport[3] + viewport[1];

    *winx=in[0];
    *winy=in[1];
    *winz=in[2];
    return(GL_TRUE);
}

static void getPixel3dTo2D( float x, float y, float z, int *pOutX, int *pOutY)
{
  GLdouble modelMatrix[16];
  GLdouble projMatrix[16];
  GLint viewport[4];

  GLdouble objx = x;
  GLdouble objy = y;
  GLdouble objz = z;

  GLdouble pix_x = 0;
  GLdouble pix_y = 0;
  GLdouble pix_z = 0;

  memset( modelMatrix, 0, sizeof( modelMatrix));
  memset( projMatrix, 0, sizeof( projMatrix));
  memset( viewport, 0, sizeof( viewport));

  glGetDoublev(GL_MODELVIEW_MATRIX, modelMatrix);
  glGetDoublev(GL_PROJECTION_MATRIX, projMatrix);
  glGetIntegerv(GL_VIEWPORT, viewport);

  My_gluProject( objx, objy, objz, modelMatrix, projMatrix, viewport,
                &pix_x, &pix_y, &pix_z);

  //need to reverse y since OpenGL has y=0 at bottom, but windows has y=0 at top
  *pOutX = (int)pix_x - viewport[0];
  *pOutY = viewport[1] + viewport[3] - (int)pix_y;
}

static void getPixel2dTo3D( int x, int y, float *pOutX, float *pOutY, float *pOutZ)
{
  GLdouble modelMatrix[16];
  GLdouble projMatrix[16];
  GLint viewport[4];

  GLdouble objx = x;
  GLdouble objy = y;
  GLdouble objz = 0;

  GLdouble pix_x = 0;
  GLdouble pix_y = 0;
  GLdouble pix_z = 0;

  memset( modelMatrix, 0, sizeof( modelMatrix));
  memset( projMatrix, 0, sizeof( projMatrix));
  memset( viewport, 0, sizeof( viewport));

  glGetDoublev(GL_MODELVIEW_MATRIX, modelMatrix);
  glGetDoublev(GL_PROJECTION_MATRIX, projMatrix);
  glGetIntegerv(GL_VIEWPORT, viewport);

  gluUnProject( objx, objy, objz, modelMatrix, projMatrix, viewport,
                &pix_x, &pix_y, &pix_z);

  *pOutX = pix_x;
  *pOutY = pix_y;
  *pOutZ = pix_z;
}

void drawskeleton( struct IQE_model *model, int ThisViewPortNum)
{
	struct skel *skel = model->skel;
  mat4 *pPoseMatrix;
  T_ViewPort *pThisViewPort;
	int i;

  pThisViewPort = ViewPortLayout + ThisViewPortNum;

	if( IqeB_DispStyle_doAnimations) {     // active animation

  	pPoseMatrix = abs_pose_matrix;
	} else {
  	pPoseMatrix = model->mesh->abs_bind_matrix;
	}

	// draw bones

	glBegin(GL_LINES);
	for (i = 0; i < skel->joint_count; i++) {
		float *a = pPoseMatrix[i];


		IqeB_DispSetOpenGLColor4f( IqeB_DispCol_SKELETON, 1);      // bone color

		if (skel->j[i].parent >= 0) {   // have a parent
			float *b = pPoseMatrix[skel->j[i].parent];
			glVertex3f(a[12], a[13], a[14]);
			IqeB_DispSetOpenGLColor4f( IqeB_DispCol_SKELETON, 1);     // bone color
			glVertex3f(b[12], b[13], b[14]);
		} else {
		  // this has no parent
			glVertex3f(a[12], a[13], a[14]);
			glColor4f(0, 0, 0, 1);     // black
			glVertex3f(0, 0, 0);
		}
	}
	glEnd();

  //
	// draw joints
  //

  // Get 2D positions of joints
  // NOTE: Must do this outside glBegin()/glEnd(
	for (i = 0; i < skel->joint_count; i++) {
		float *a = pPoseMatrix[i];

    getPixel3dTo2D( a[12], a[13], a[14], &pThisViewPort->SkelDrawPosX[ i], &pThisViewPort->SkelDrawPosY[ i]);
	}

	// Get mouse near to joint

  if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_SKEL_EDIT ||  // Mouse mode: Skeleton edit
      IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_JOINT_SEL ||  // Mouse mode: Skeleton joint select
      IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_ANIM_EDIT) {  // Mouse mode: Animation edit

    if(  ThisViewPortNum == ViewPort_MouseNum) {  // mouse inside this view

      int dx, dy, dist;
  	  int BestJointDist = 0;

  	  IqeB_DispStyle_JointMouseNearby = -1;    // reset, mouse is not nearby a joint

  	  for (i = 0; i < skel->joint_count; i++) {

        dx = pThisViewPort->SkelDrawPosX[ i] - (mousex - pThisViewPort->x);
        dy = pThisViewPort->SkelDrawPosY[ i] - (pThisViewPort->h - (mousey - pThisViewPort->y));

        dist = dx * dx + dy * dy;  // square dist

        if( dist <= 8 * 8) {     // not more than 8 pixel away from joint

          if( IqeB_DispStyle_JointMouseNearby < 0 || dist < BestJointDist) {

            IqeB_DispStyle_JointMouseNearby = i;
            BestJointDist = dist;
          }
        }
      }
  	}
  } else {

    // NOT mouse edit mode skeleton
	  IqeB_DispStyle_JointMouseNearby = -1;    // reset, mouse is not nearby a joint
  }

  // Draw the joints in the view

	for (i = 0; i < skel->joint_count; i++) {

		float *a = pPoseMatrix[i];
		float PointSize;
		int isSelected;

    if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_SKEL_EDIT ||  // Mouse mode: Skeleton edit
        IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_ANIM_EDIT) {  // Mouse mode: Animation edit

      isSelected = IqeB_DispStyle_JointSelected == i;
    } else if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_JOINT_SEL) {  // Mouse mode: Skeleton joint select

      isSelected = skel->j[i].isSelected;
    } else {

      isSelected = false;
    }

    // is a selected joint
    if( isSelected) {           // selected joints

	    IqeB_DispSetOpenGLColor4f( IqeB_DispCol_JOINTS_SEL, 1); // selected joint color
	    PointSize = 9.0;                        // bigger points

    } else {   // joint is not selected

      // Overwrite joint color if we display blend weights
      if( IqeB_DispStyle_JointMouseNearby == i) {    // Mouse is near this

  	    IqeB_DispSetOpenGLColor4f2( IqeB_DispCol_JOINTS, IqeB_DispCol_JOINTS_SEL, 1);     // joint color
	    } else if( IqeB_DispStyle_doBlendWeightColors && pDrawModel->mesh->blendColors) {  // Display blend weight colors

	      glColor4f( skel->j[i].TempBoneColor[ 0], skel->j[i].TempBoneColor[ 1], skel->j[i].TempBoneColor[ 2], 1.0);

	    } else {

  	    IqeB_DispSetOpenGLColor4f( IqeB_DispCol_JOINTS, 1);     // joint color
	    }

	    PointSize = 5.0;                         // points
    }

    if( IqeB_DispStyle_JointMouseNearby == i) {    // Mouse is near this

	    PointSize = 11.0;                            // Even more bigger points
    }

    // Must glBegin/glEnd for each point. glPointSize is only valid before glBegin.
    glPointSize( PointSize);  // set size of points
  	glBegin( GL_POINTS);
  	glVertex3f( a[12], a[13], a[14]);
	  glEnd();
	}

	glPointSize( 1.0);           // reset point size
}

/************************************************************************************
 * drawmodel()
 */

static void drawmodel( struct IQE_model *model, int DoDrawBlendWeightColors)
{
	struct mesh *mesh = model->mesh;
	int i;

	glEnableClientState(GL_VERTEX_ARRAY);
	if( IqeB_DispStyle_doAnimations) {     // active animation
	  glVertexPointer(3, GL_FLOAT, 0, mesh->aposition ? mesh->aposition : mesh->position);
	} else {
	  glVertexPointer(3, GL_FLOAT, 0, mesh->position);
	}

	if( IqeB_DispStyle_doAnimations) {     // active animation
  	if (mesh->normal || mesh->anormal) {
      glEnableClientState(GL_NORMAL_ARRAY);
      glNormalPointer(GL_FLOAT, 0, mesh->anormal ? mesh->anormal : mesh->normal);
	  }
	} else {
  	if (mesh->normal) {
      glEnableClientState(GL_NORMAL_ARRAY);
      glNormalPointer(GL_FLOAT, 0, mesh->normal);
	  }
	}

	if (mesh->texcoord) {
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(2, GL_FLOAT, 0, mesh->texcoord);
	}

	if( DoDrawBlendWeightColors && mesh->blendColors) {

    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(4, GL_FLOAT, 0, mesh->blendColors);

	} else if (mesh->color) {
    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(4, GL_FLOAT, 0, mesh->color);
	}

	for (i = 0; i < mesh->part_count; i++) {

    if( IqeB_DispStyle_MeshPartSelected < 0 ||    // no mesh selected
        IqeB_DispStyle_MeshPartSelected != i) {   // mesh is not selected

		  glColor4f( 1.0, 1.0, 1.0, 1.0);

    } else {                                      // mesh is selected

      if( IqeB_Disp_GetTickCount() & 0x200) {     // each 1/2 second

#ifdef use_again
		    glColor4f( 0.3, 0.3, 0.3, 1.0);
#else
        continue;
#endif
      } else {

		    glColor4f( 1.0, 1.0, 1.0, 1.0);
      }
    }

		glBindTexture(GL_TEXTURE_2D, mesh->part[i].material);
		glDrawElements(GL_TRIANGLES, mesh->part[i].count, GL_UNSIGNED_INT, mesh->element + mesh->part[i].first);
	}

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
}

/************************************************************************************
 * IqeB_Measuremodel()
 */

static float IqeB_Measuremodel( struct IQE_model *model, float center[3], float size[3])
{
	struct skel *skel = model->skel;
	struct mesh *mesh = model->mesh;
	struct anim *anim;
	float dist, maxdist = 0.25;
	float minbox[3], maxbox[3];
	int i, k, nMinMax;

	center[0] = center[1] = center[2] = 0;

#ifdef use_again
	for (i = 0; i < mesh->vertex_count; i++)
		vec_add(center, center, mesh->position + i * 3);
	if (mesh->vertex_count) {
		center[0] /= mesh->vertex_count;
		center[1] /= mesh->vertex_count;
		center[2] /= mesh->vertex_count;
	}
#else
  // 25.03.2014 RR: use min/max box center, is better than center of gravity

  minbox[0] = minbox[1] = minbox[2] = 0;
  maxbox[0] = maxbox[1] = maxbox[2] = 0;
  nMinMax = 0;

  // vertex

	for (i = 0; i < mesh->vertex_count; i++) {

    if( nMinMax == 0) {

  		vec_copy( minbox, mesh->position + i * 3);
  		vec_copy( maxbox, mesh->position + i * 3);
    } else {

      vec_minmax( minbox, maxbox, mesh->position + i * 3);
    }

    nMinMax += 1;
  }

  // skeleton

	if (skel->joint_count > 0) {

  	for (i = 0; i < skel->joint_count; i++) {

      if( nMinMax == 0) {

    		vec_copy( minbox, mesh->abs_bind_matrix[i] + 12);
  	  	vec_copy( maxbox, mesh->abs_bind_matrix[i] + 12);
      } else {

        vec_minmax( minbox, maxbox, mesh->abs_bind_matrix[i] + 12);
      }

      nMinMax += 1;
  	}

  	for( i = 0; i < model->anim_count; i++) {

		  anim = model->anim_data[ i];

			for (k = 0; anim && k < anim->len; k++) {
				calc_matrix_from_pose(loc_pose_matrix, anim->data[k], skel->joint_count);
				calc_abs_matrix(abs_pose_matrix, loc_pose_matrix, skel);
				for (i = 0; i < skel->joint_count; i++) {

          if( nMinMax == 0) {

    	  	  vec_copy( minbox, abs_pose_matrix[i] + 12);
  	  	    vec_copy( maxbox, abs_pose_matrix[i] + 12);
          } else {

            vec_minmax( minbox, maxbox, abs_pose_matrix[i] + 12);
          }

          nMinMax += 1;
				}
			}
		}
	}

	center[0] = (minbox[0] + maxbox[0]) * 0.5;
	center[1] = (minbox[1] + maxbox[1]) * 0.5;
	center[2] = (minbox[2] + maxbox[2]) * 0.5;

	size[0] = maxbox[0] - minbox[0];
	size[1] = maxbox[1] - minbox[1];
	size[2] = maxbox[2] - minbox[2];

	// Keep some minimum size

	if( size[0] < 0.001) size[0] = 0.001;
	if( size[1] < 0.001) size[1] = 0.001;
	if( size[2] < 0.001) size[2] = 0.001;
#endif

	for (i = 0; i < mesh->vertex_count; i++) {
		dist = vec_dist2(center, mesh->position + i * 3);
		if (dist > maxdist)
			maxdist = dist;
	}

	if (skel->joint_count > 0) {
		for (i = 0; i < skel->joint_count; i++) {
			dist = vec_dist2(center, mesh->abs_bind_matrix[i] + 12);
			if (dist > maxdist)
				maxdist = dist;
		}

  	for( i = 0; i < model->anim_count; i++) {

		  anim = model->anim_data[ i];

			for (k = 0; anim && k < anim->len; k++) {
				calc_matrix_from_pose(loc_pose_matrix, anim->data[k], skel->joint_count);
				calc_abs_matrix(abs_pose_matrix, loc_pose_matrix, skel);
				for (i = 0; i < skel->joint_count; i++) {
					dist = vec_dist2(center, abs_pose_matrix[i] + 12);
					if (dist > maxdist)
						maxdist = dist;
				}
			}
		}

		memcpy(abs_pose_matrix, mesh->abs_bind_matrix, sizeof abs_pose_matrix);
	}

	return sqrt(maxdist);
}

/************************************************************************************
 * Boring UI and GLUT hooks.
 */

#define DIABLO 36.8698976	// 4:3 isometric view
#define ISOMETRIC 35.264	// true isometric view
#define DIMETRIC 30		    // 2:1 'isometric' as seen in pixel art

static float gridsize = 3.0;
static float gridstep = 1.0;
static int   CoordAfterpointDigits = 2;   // is >= 0, afterpoint digits for coordinates

static float light_position[4] = { -1, -2, 2, 0 };

typedef struct {
	float distanceCenter; // Distance center (fixed per model)
  float distanceMin;    // Min view distance (fixed per model)
  float distanceMax;    // Max view distance (fixed per model)
	float distance1V;     // Current view distance for 1 view layout
	float distance2V;     // Current view distance for 4 view layout, a 2D window
	float yaw2D;          // Current 2D yaw
	float pitch2D;        // Current 2D pitch
	float yaw3D;          // Current 3D yaw
	float pitch3D;        // Current 3D pitch
	vec3  ObjCenter;      // Center of object (fixed per model)
	vec3  DispShift1V;    // Current shifted display center for 1 view layout
	vec3  DispShift2V;    // Current shifted display center for 4 view layout, a 2D window
	vec3  ObjSize;        // Size of object (fixed per model)
} T_camera;

static T_camera camera = { 3, 1, 10, 3, 3, 0, 0, 45, -DIMETRIC, { 0, 1, 0 } };

// ...

static void perspective(float fov, float aspect, float znear, float zfar)
{
	fov = fov * 3.14159 / 360.0;
	fov = tan(fov) * znear;
	glFrustum(-fov * aspect, fov * aspect, -fov, fov, znear, zfar);
}

static void orthogonal(float fov, float aspect, float znear, float zfar)
{
	glOrtho( -fov * aspect, fov * aspect, -fov, fov, znear, zfar);
}

static void drawstring(float x, float y, char *s)
{

  gl_font( FL_HELVETICA, 12);
  gl_draw( s, x, y);
}

static void drawstring_right(float x, float y, char *s)
{

  gl_font( FL_HELVETICA, 12);
  gl_draw( s, x, y, 1, 10, FL_ALIGN_RIGHT);
}

/************************************************************************************
 * IqeB_DispSetViewportSize
 */

static int DrawViewport_w, DrawViewport_h;
static int DisplayedAnyModel;

void IqeB_DispSetViewportSize( int w, int h)
{

  DrawViewport_w = w;
  DrawViewport_h = h;

  glViewport( 0, 0, DrawViewport_w, DrawViewport_h);
}

/************************************************************************************
 * IqeB_DispMouseEventAction
 */
void IqeB_DispMouseEventAnimChangeAbort()
{

  if( DrawMouseAnimNunmLines == 2 &&    // Two lines until now
      DrawMouseAnim_pPose != NULL) {    // Security test, have pointer set

    *DrawMouseAnim_pPose = DrawMouseAnim_PoseLatch; // Restore latched pose

    // And update for display
    IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);

    DrawMouseAnim_pPose = NULL;
  }

  DrawMouseAnimNunmLines = 0;        // Mouse animation support, number of lines to draw, 0 = off
}

/************************************************************************************
 * IqeB_DispMouseEventAction
 */

void IqeB_DispMouseEventAction( int event)
{
  int i, x, y, dx, dy, ViewLayout, ViewMode, h;
  int mouseleft, mouseright;
  T_ViewPort *pThisViewPort;

  // reset mouse display things

  DrawMouseAnimEnabled = false;                // != 0 to draw mouse animation things

  // ...

  x = Fl::event_x();
  y = DrawViewport_h - Fl::event_y();

  if( event == FL_LEAVE || event == FL_ENTER) { // Mouse enter or leave the view

    ViewPort_MouseNum = -1;                // Not in any viewport
    IqeB_DispMouseEventAnimChangeAbort();  // Mouse animation support, abort

	  mousex = x;                // catch last mouse position
  	mousey = y;

    return;
  }

  // Mouse is in which viewport

  ViewPort_MouseNum = -1;            // Not in any viewport

  for( i = 0; i < ViewPorts_n; i++) {

    if( x >= ViewPortLayout[ i].x &&
        x <  ViewPortLayout[ i].x + ViewPortLayout[ i].w &&
        y >= ViewPortLayout[ i].y &&
        y <  ViewPortLayout[ i].y + ViewPortLayout[ i].h) {

      ViewLayout = ViewPortLayout[ i].ViewLayout;
      ViewMode   = ViewPortLayout[ i].ViewMode;
      //x/w = ViewPortLayout[ i].w;
      h = ViewPortLayout[ i].h;

      ViewPort_MouseNum = i;            // inside in this viewport
#ifdef use_again
      ViewPort_MouseX = x - ViewPortLayout[ i].x;
      ViewPort_MouseY = y - ViewPortLayout[ i].y;
#else
      ViewPort_MouseX = x;
      ViewPort_MouseY = y;
#endif
      break;
    }
  }

  if( i >= ViewPorts_n) {      // Mouse not in any viewport

    IqeB_DispMouseEventAnimChangeAbort();  // Mouse animation support, abort

	  mousex = x;                // catch last mouse position
  	mousey = y;

    return;                    // return to caller
  }

  pThisViewPort = ViewPortLayout + ViewPort_MouseNum;

  // mouse wheel

  if( event == FL_MOUSEWHEEL) {      // Mouse wheel

    // Mouse wheel processing

    dy = Fl::event_dy();

    if( ViewLayout == 1) {   // 1 view layout

  		camera.distance1V += dy * -0.25 * camera.distance1V;
	  	if (camera.distance1V < camera.distanceMin) camera.distance1V = camera.distanceMin;
		  if (camera.distance1V > camera.distanceMax) camera.distance1V = camera.distanceMax;

    } else {                 // 4 view layout, a 2D view

  		camera.distance2V += dy * -0.25 * camera.distance2V;
	  	if (camera.distance2V < camera.distanceMin) camera.distance2V = camera.distanceMin;
		  if (camera.distance2V > camera.distanceMax) camera.distance2V = camera.distanceMax;
    }

    IqeB_Disp_LastDrawTime = 0;           // flag redraw

	  mousex = x;                // catch last mouse position
  	mousey = y;

    return;
  }

  // mouse buttons

	mouseleft   = Fl::event_state() & FL_BUTTON1;
	mouseright  = Fl::event_state() & FL_BUTTON3;

	// mouse move since last call

	dx = x - mousex;
	dy = y - mousey;

	// Left mouse button is down and mouse edit mode skeleton
  if( mouseleft && (IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_SKEL_EDIT ||   // Mouse mode: Skeleton edit
      (IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_ANIM_EDIT &&               // or mouse mode: Animation edit and not angles
      IqeB_ToolsAnimEdit_PosePart != IQE_ANIM_EDIT_POSE_PART_ANGLES))) {

	  // Left mouse button is down and mouse is near joint and this joint is selected
    if( event == FL_PUSH &&                         // Mouse button is down
        IqeB_DispStyle_JointMouseNearby >= 0 &&     // Is a selected joint in the near
        IqeB_DispStyle_JointMouseNearby != IqeB_DispStyle_JointSelected) {

      IqeB_DispStyle_JointSelected = IqeB_DispStyle_JointMouseNearby;  // Select this joint

      // Selected joint has changed
      IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_JOINT_SELECTED); // Also update the GUI
	  }

    if( ViewMode < IQE_VIEWMODE_3D) {            // 2D view mode

      // IQE_GUI_MOUSE_MODE_SKEL_EDIT: move joint of skeleton

      if( IqeB_DispStyle_JointSelected >= 0 &&               // have a selected joint
          pDrawModel != NULL && pDrawModel->skel != NULL &&  // have a model and a skeleton
#ifdef IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
          pDrawModel->skel->IsNormalized &&                  // and model has a normalized skeleton
#else
          (IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_ANIM_EDIT || pDrawModel->skel->IsNormalized) && // and model has a normalized skeleton
#endif // IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
          (dx != 0 || dy != 0)) {                            // mouse has moved

        vec3 vx, vy;
        float t;

        if( ViewLayout == 1) {   // 1 view layout
          t = camera.distance1V / h;
        } else {                 // 4 view layout, a 2D view
          t = camera.distance2V / h;
        }

        switch( ViewMode) {
        case IQE_VIEWMODE_XY_TOP:
          vec_set( vx, t, 0, 0);
          vec_set( vy, 0, t, 0);
          break;
        case IQE_VIEWMODE_XY_BOTTOM:
          vec_set( vx, t, 0, 0);
          vec_set( vy, 0, -t, 0);
          break;
        case IQE_VIEWMODE_ZX_RIGHT:
          vec_set( vx, t, 0, 0);
          vec_set( vy, 0, 0, t);
          break;
        case IQE_VIEWMODE_ZX_LEFT:
          vec_set( vx, -t, 0, 0);
          vec_set( vy, 0, 0, t);
          break;
        case IQE_VIEWMODE_YZ_FRONT:
          vec_set( vx, 0, t, 0);
          vec_set( vy, 0, 0, t);
          break;
        case IQE_VIEWMODE_YZ_BACK:
          vec_set( vx, 0, -t, 0);
          vec_set( vy, 0, 0, t);
          break;
        default:
          // not decoded, no display shift
          vec_set( vx, 0, 0, 0);
          vec_set( vy, 0, 0, 0);
          break;
        }

        if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_SKEL_EDIT) { // Mouse mode: Skeleton edit

          vec_scale_add_to( pDrawModel->skel->pose[ IqeB_DispStyle_JointSelected].location, vx, dx);
          vec_scale_add_to( pDrawModel->skel->pose[ IqeB_DispStyle_JointSelected].location, vy, dy);

          if( IqeB_ToolsSkelEdit_MoveSymmetricJoints &&    // Also move symmetric joints
              pDrawModel->skel->j[ IqeB_DispStyle_JointSelected].iSymmetric >= 0) {  // This joint is smmetric

            int iOther;

            iOther = pDrawModel->skel->j[ IqeB_DispStyle_JointSelected].iSymmetric;

            // Copy over the positon info
            vec_copy( pDrawModel->skel->pose[ iOther].location, pDrawModel->skel->pose[ IqeB_DispStyle_JointSelected].location);

            // Mirror Y-axis

            pDrawModel->skel->pose[ iOther].location[ 1] = - pDrawModel->skel->pose[ IqeB_DispStyle_JointSelected].location[ 1];
          }

          IqeB_ModelSkeletonMatrixCalc( pDrawModel);  // update skeleton matrix

          // Data of selected joint has changed
          IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_JOINT_CHANGED); // Also update the GUI

        } else if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_ANIM_EDIT &&  // Mouse mode: Animation edit
                   IqeB_ToolsAnimEdit_PosePart == IQE_ANIM_EDIT_POSE_PART_LOCATION) {  // edit location

          if( IqeB_DispStyle_JointSelected >= 0 &&               // have a selected joint
              pDrawModel != NULL && pDrawModel->skel != NULL &&  // have a model and a skeleton
#ifdef IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
              pDrawModel->skel->IsNormalized &&                  // and model has a normalized skeleton
#endif // IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
              ! IqeB_DispStyle_doplay &&                         // Animation not playing
              pDrawModel->anim_count > 0 &&                      // have animations
              pDrawModel->anim_poses > 0 &&                      // have poses
              pDrawModelCurAnim != NULL &&                       // have current animation
              DrawModelCurframe >= 0 && DrawModelCurframe < pDrawModelCurAnim->len) {  // Frame indes in range

            struct pose *pPose;
            vec3 ValuesBefore;

            // Update at joint location

            pPose = pDrawModelCurAnim->data[ DrawModelCurframe] + IqeB_DispStyle_JointSelected;

            vec_copy( ValuesBefore, pPose->location);               // latch value before chagne
            vec_scale_add_to( pPose->location, vx, dx);
            vec_scale_add_to( pPose->location, vy, dy);

            // In any case, we have a keyflag for this, so set it
            if( ValuesBefore[ 0] != pPose->location[ 0]) pPose->KeyFlags |= (KEYFLAG_LOC_0 << 0);
            if( ValuesBefore[ 1] != pPose->location[ 1]) pPose->KeyFlags |= (KEYFLAG_LOC_0 << 1);
            if( ValuesBefore[ 2] != pPose->location[ 2]) pPose->KeyFlags |= (KEYFLAG_LOC_0 << 2);

            // Recalc all poses of this animation
            IqeB_AnimKeyFlagsRecalcAnimation( pDrawModelCurAnim, pDrawModel->skel->joint_count);

            // And update for display
            IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);

            // Data of selected joint has changed
            IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
          }
        } else if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_ANIM_EDIT &&  // Mouse mode: Animation edit
                   IqeB_ToolsAnimEdit_PosePart == IQE_ANIM_EDIT_POSE_PART_SCALE) {  // edit scale

          if( IqeB_DispStyle_JointSelected >= 0 &&               // have a selected joint
              pDrawModel != NULL && pDrawModel->skel != NULL &&  // have a model and a skeleton

#ifdef IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
              pDrawModel->skel->IsNormalized &&                  // and model has a normalized skeleton
#endif // IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
              ! IqeB_DispStyle_doplay &&                         // Animation not playing
              pDrawModel->anim_count > 0 &&                      // have animations
              pDrawModel->anim_poses > 0 &&                      // have poses
              pDrawModelCurAnim != NULL &&                       // have current animation
              DrawModelCurframe >= 0 && DrawModelCurframe < pDrawModelCurAnim->len) {  // Frame indes in range

            struct pose *pPose;
            vec3 ValuesBefore;
            float ValuesBeforeAverage, ScaleMin, ScaleMax;

            // Update at joint scale

            pPose = pDrawModelCurAnim->data[ DrawModelCurframe] + IqeB_DispStyle_JointSelected;

            vec_copy( ValuesBefore, pPose->scale);               // latch value before chagne
            ValuesBeforeAverage = (ValuesBefore[ 0] + ValuesBefore[ 1] + ValuesBefore[ 2]) / 3.0;

            vec_scale_add_to( pPose->scale, vx, dx);
            vec_scale_add_to( pPose->scale, vy, dy);

	          if( pPose->scale[0] < 0.001) pPose->scale[0] = 0.001;
	          if( pPose->scale[1] < 0.001) pPose->scale[1] = 0.001;
	          if( pPose->scale[2] < 0.001) pPose->scale[2] = 0.001;

            if( IqeB_ToolsAnimEdit_PoseScaleAll) { // For pose scale change, change all scales

               ScaleMin = pPose->scale[0];
               if( ScaleMin > pPose->scale[1]) ScaleMin = pPose->scale[1];
               if( ScaleMin > pPose->scale[2]) ScaleMin = pPose->scale[2];

               ScaleMax = pPose->scale[0];
               if( ScaleMax < pPose->scale[1]) ScaleMax = pPose->scale[1];
               if( ScaleMax < pPose->scale[2]) ScaleMax = pPose->scale[2];

               // What is the biggest change in size

               if( ScaleMax - ValuesBeforeAverage >= ValuesBeforeAverage - ScaleMin) {

                 pPose->scale[0] = ScaleMax;
	               pPose->scale[1] = ScaleMax;
	               pPose->scale[2] = ScaleMax;
               } else {

                 pPose->scale[0] = ScaleMin;
	               pPose->scale[1] = ScaleMin;
	               pPose->scale[2] = ScaleMin;
               }
            }


            // In any case, we have a keyflag for this, so set it
            if( ValuesBefore[ 0] != pPose->scale[ 0]) pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 0);
            if( ValuesBefore[ 1] != pPose->scale[ 1]) pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 1);
            if( ValuesBefore[ 2] != pPose->scale[ 2]) pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 2);

            // Recalc all poses of this animation
            IqeB_AnimKeyFlagsRecalcAnimation( pDrawModelCurAnim, pDrawModel->skel->joint_count);

            // And update for display
            IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);

            // Data of selected joint has changed
            IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
          }
        }
      }

  	  mousex = x;                // catch last mouse position
    	mousey = y;

      return;                    // return to caller
    }
	}

	// Mouse mode: Animation edit
  if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_ANIM_EDIT &&  // Mouse mode: Animation edit
      IqeB_ToolsAnimEdit_PosePart == IQE_ANIM_EDIT_POSE_PART_ANGLES) {  // Edit angles

	  // Left mouse button is down and mouse is near joint and this joint is selected
    if( mouseleft && event == FL_PUSH &&            // Left mouse button is down
        IqeB_DispStyle_JointMouseNearby >= 0 &&     // Is a selected joint in the near
        IqeB_DispStyle_JointMouseNearby != IqeB_DispStyle_JointSelected) {

      IqeB_DispStyle_JointSelected = IqeB_DispStyle_JointMouseNearby;  // Select this joint
      IqeB_DispMouseEventAnimChangeAbort();  // Mouse animation support, abort

      // Selected joint has changed
      IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_JOINT_SELECTED); // Also update the GUI
	  }

    if( ViewMode < IQE_VIEWMODE_3D) {            // 2D view mode

      // IQE_GUI_MOUSE_MODE_ANIM_EDIT: change angle for animation pose

      if( DrawMouseAnimNunmLines == 2 &&                     // Two lines
          DrawMouseAnim_ViewMode != ViewMode) {              // Latched view mode is different

        IqeB_DispMouseEventAnimChangeAbort();                // Must abort
        DrawMouseAnim_ViewMode = ViewMode;
      }

      if( IqeB_DispStyle_JointSelected >= 0 &&               // have a selected joint
          pDrawModel != NULL && pDrawModel->skel != NULL &&  // have a model and a skeleton
#ifdef IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
          pDrawModel->skel->IsNormalized &&                  // and model has a normalized skeleton
#endif // IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
          ! IqeB_DispStyle_doplay &&                         // Animation not playing
          pDrawModel->anim_count > 0 &&                      // have animations
          pDrawModel->anim_poses > 0 &&                      // have poses
          pDrawModelCurAnim != NULL &&                       // have current animation
          DrawModelCurframe >= 0 && DrawModelCurframe < pDrawModelCurAnim->len) {  // Frame indes in range

        int dx, dy, dist;
        int MinMouseDist = 24 * 24;  // Mosue must be more than this away to draw

        // Merge two collors

        unsigned Col1, Col2;
        unsigned r1, g1, b1;
        unsigned r2, g2, b2;

        if( IqeB_DispCol_JOINTS_SEL & 0xffffff00) { // Is RGB color
          Col1 = IqeB_DispCol_JOINTS_SEL;
        } else {
          Col1 = Fl::get_color( (Fl_Color)IqeB_DispCol_JOINTS_SEL);
        }

        r1 = (Col1 >> 24) & 255;
        g1 = (Col1 >> 16) & 255;
        b1 = (Col1 >>  8) & 255;

        if( IqeB_DispCol_JOINTS & 0xffffff00) {   // Is RGB color
          Col2 = IqeB_DispCol_JOINTS;
        } else {
          Col2 = Fl::get_color( (Fl_Color)IqeB_DispCol_JOINTS);
        }

        r2 = (Col2 >> 24) & 255;
        g2 = (Col2 >> 16) & 255;
        b2 = (Col2 >>  8) & 255;

        // merge
        r1 = (r1 + r2 + 1) >> 1;
        g1 = (g1 + g2 + 1) >> 1;
        b1 = (b1 + b2 + 1) >> 1;

        DrawMouseAnimLineCol1 = fl_rgb_color( r1, g1, b1);    // Color of line to draw
        DrawMouseAnimLineCol2 = IqeB_DispCol_JOINTS_SEL;      // Color of line to draw

        // Joint coordinate
        DrawMouseAnimJX = pThisViewPort->SkelDrawPosX[ IqeB_DispStyle_JointSelected];
        DrawMouseAnimJY = pThisViewPort->SkelDrawPosY[ IqeB_DispStyle_JointSelected];

        if( mouseleft &&                                       // Mouse button down
            ((event == FL_PUSH && DrawMouseAnimNunmLines == 1) ||  // Left mouse button is down and was one line before
             (DrawMouseAnimNunmLines == 2))) {                 // is two lines now

          // Point 1 of line to draw
          DrawMouseAnimP2X = mousex - pThisViewPort->x;
          DrawMouseAnimP2Y = pThisViewPort->h - (mousey - pThisViewPort->y);

          // TEST: not to near selected joint
          dx = DrawMouseAnimP2X - DrawMouseAnimJX;
          dy = DrawMouseAnimP2Y - DrawMouseAnimJY;

          if( DrawMouseAnimNunmLines != 2) {    // No two lines until now

            DrawMouseAnimNunmLines = 2;         // now two line

            // Catch pose to change

            DrawMouseAnimFrameNr = DrawModelCurframe;            // Frame of pose to change
            DrawMouseAnimPoseNr  = IqeB_DispStyle_JointSelected; // Pose to change
            DrawMouseAnim_pPose  = pDrawModelCurAnim->data[ DrawModelCurframe] + IqeB_DispStyle_JointSelected;
            DrawMouseAnim_PoseLatch = *DrawMouseAnim_pPose;      // Latch pose at entry
            DrawMouseAnim_ViewMode = ViewMode;                   // Latch view mode

          } else {

            // Change the pose

            dist = dx * dx + dy * dy;  // square dist
            if( dist >= MinMouseDist) {        // more than MinMouseDist pixel away from joint

              float AngleDelta1, AngleDelta2, AngleSign;
              int iAxis;

               // What axis to change
               switch( ViewMode) {
              case IQE_VIEWMODE_XY_TOP:
                iAxis = 2;
                AngleSign = 1.0;
                break;
              case IQE_VIEWMODE_XY_BOTTOM:
                iAxis = 2;
                AngleSign = -1.0;
                break;
              case IQE_VIEWMODE_ZX_RIGHT:
                iAxis = 1;
                AngleSign = -1.0;
                break;
              case IQE_VIEWMODE_ZX_LEFT:
                iAxis = 1;
                AngleSign = 1.0;
                break;
              case IQE_VIEWMODE_YZ_FRONT:
                iAxis = 0;
                AngleSign = 1.0;
                break;
              case IQE_VIEWMODE_YZ_BACK:
                iAxis = 0;
                AngleSign = -1.0;
                break;
              default:
                // not decoded, use some default
                iAxis = 0;
                AngleSign = 0.0;
                break;
              }

              // Delta angle
              AngleDelta1 = atan2( DrawMouseAnimP1X - DrawMouseAnimJX, DrawMouseAnimP1Y - DrawMouseAnimJY);
              AngleDelta2 = atan2( dx, dy);

              // Update at joint position
              DrawMouseAnim_pPose->angles[ iAxis] = DrawMouseAnim_PoseLatch.angles[ iAxis] + RAD2DEG( (AngleDelta2 - AngleDelta1) * AngleSign);

              // In any case, we have a keyflag for this, so set it
              DrawMouseAnim_pPose->KeyFlags |= (KEYFLAG_ANG_0 << iAxis);

              // Must converte angle to quaterion
              IqeB_AnimAnglesToRotPose( DrawMouseAnim_pPose);

              // Recalc all poses of this animation
              IqeB_AnimKeyFlagsRecalcAnimation( pDrawModelCurAnim, pDrawModel->skel->joint_count);

              // And update for display
              IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);

              // Data of selected joint has changed
              IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
            }
          }

        } else if( ! mouseleft && event == FL_RELEASE &&         // Mouse button just up
                   DrawMouseAnimNunmLines == 2) {                // and was two lines before

          // Proper end of animation action

          DrawMouseAnimNunmLines = 0;        // Mouse animation support, number of lines to draw, 0 = off
          DrawMouseAnim_pPose = NULL;

          // TEST: not to near selected joint
          dx = 0;
          dy = 0;

        } else {

          // Point 1 of line to draw
          DrawMouseAnimP1X = mousex - pThisViewPort->x;
          DrawMouseAnimP1Y = pThisViewPort->h - (mousey - pThisViewPort->y);

          // TEST: not to near selected joint
          dx = DrawMouseAnimP1X - DrawMouseAnimJX;
          dy = DrawMouseAnimP1Y - DrawMouseAnimJY;

          DrawMouseAnimNunmLines = 1;        // draw one line
        }

        // TEST: not to near selected joint
        dist = dx * dx + dy * dy;  // square dist

        if( dist >= MinMouseDist) {        // more than MinMouseDist pixel away from joint

          DrawMouseAnimEnabled = true;     // To draw mouse animation things
        }
      }

      if( mouseleft) {                     // left mouse button is down
  	    mousex = x;                // catch last mouse position
    	  mousey = y;

        return;                    // return to caller
      }
    } else {

      DrawMouseAnimNunmLines = 0;        // Mouse animation support, number of lines to draw, 0 = off
    }
	} else {

    DrawMouseAnimNunmLines = 0;        // Mouse animation support, number of lines to draw, 0 = off
	}


	// Left mouse button is down and mouse edit mode joint select
  if( mouseleft && IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_JOINT_SEL) {

	  // Left mouse button is down and mouse is near joint and this joint is selected
    if( event == FL_PUSH &&                         // Mouse button is down
        IqeB_DispStyle_JointMouseNearby >= 0 &&     // Is a selected joint in the near
        pDrawModel != NULL && pDrawModel->skel != NULL) {  // have a model and a skeleton

      // Toggle selection for this joint
      pDrawModel->skel->j[ IqeB_DispStyle_JointMouseNearby].isSelected = ! pDrawModel->skel->j[ IqeB_DispStyle_JointMouseNearby].isSelected;

      // Selected joint has changed
      IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_JOINT_SELECTED); // Also update the GUI
	  }
	}

	// Test no button pressed or not moved

	if( ! mouseleft && ! mouseright)  {   // nothing to do

	  mousex = x;                // catch last mouse position
  	mousey = y;

    return;                    // return to caller
	}

	if( dx == 0 && dy == 0) {    // nothing to do

    return;                    // return to caller
	}

	// ...

	if (mouseright) {            // Turn camera view

    if( ViewMode >= IQE_VIEWMODE_3D) {   // only for 3D view modes

		  camera.yaw3D -= dx * 0.3;
		  camera.pitch3D += dy * 0.2;
		  if (camera.pitch3D < -90) camera.pitch3D = -90;
		  if (camera.pitch3D > 90)  camera.pitch3D = 90;
		  if (camera.yaw3D < 0)   camera.yaw3D += 360;
		  if (camera.yaw3D > 360) camera.yaw3D -= 360;

      IqeB_Disp_LastDrawTime = 0;           // flag redraw
    }
	}

  // Test for shift viewport

	if( mouseleft || (mouseright && ViewMode < IQE_VIEWMODE_3D)) {     // Shift view

    int i;
    vec3 vx, vy;
    float t;

    if( ViewLayout == 1) {   // 1 view layout

      t = camera.distance1V / h;

      vec_set( vx, -t, 0, 0);
      vec_set( vy, 0, 0, -t);

      vec_scale_add_to( camera.DispShift1V, vx, dx);
      vec_scale_add_to( camera.DispShift1V, vy, dy);

    } else {                 // 4 view layout, a 2D view

      t = camera.distance2V / h;

      switch( ViewMode) {
      case IQE_VIEWMODE_XY_TOP:
        vec_set( vx, -t, 0, 0);
        vec_set( vy, 0, -t, 0);
        break;
      case IQE_VIEWMODE_ZX_RIGHT:
        vec_set( vx, -t, 0, 0);
        vec_set( vy, 0, 0, -t);
        break;
      case IQE_VIEWMODE_YZ_FRONT:
        vec_set( vx, 0, -t, 0);
        vec_set( vy, 0, 0, -t);
        break;
      default:
        // not decoded, no display shift
        vec_set( vx, 0, 0, 0);
        vec_set( vy, 0, 0, 0);
        break;
      }

      vec_scale_add_to( camera.DispShift2V, vx, dx);
      vec_scale_add_to( camera.DispShift2V, vy, dy);
    }

    for( i = 0; i < 3; i++) {

      // DispShift1V

      if( camera.DispShift1V[ i] > gridsize) { // clip it
        camera.DispShift1V[ i] = gridsize;
      }

      if( camera.DispShift1V[ i] < -gridsize) { // clip it
        camera.DispShift1V[ i] = -gridsize;
      }

      // DispShift2V

      if( camera.DispShift2V[ i] > gridsize) { // clip it
        camera.DispShift2V[ i] = gridsize;
      }

      if( camera.DispShift2V[ i] < -gridsize) { // clip it
        camera.DispShift2V[ i] = -gridsize;
      }
    }

    IqeB_Disp_LastDrawTime = 0;           // flag redraw
	}

	mousex = x;
	mousey = y;
}

/************************************************************************************
 * MySleep
 *
 * Sleep (give up processing time).
 *
 * Duration: sleep time in ms.
 *
 */

static void MySleep( unsigned int Duration)
{

#ifdef _WIN32

	Sleep( Duration);
#else  // UNIX implementation
  usleep( Duration * 1000);
#endif
}

/************************************************************************************
 * AxisCompine
 *
 * Compines axis vectors
 */

static void AxisCompine( vec3 p, const vec3 Axis1Vec, const vec3 Axis2Vec, float f1, float f2)
{
	p[0] = Axis1Vec[0] * f1 + Axis2Vec[0] * f2;
	p[1] = Axis1Vec[1] * f1 + Axis2Vec[1] * f2;
	p[2] = Axis1Vec[2] * f1 + Axis2Vec[2] * f2;
}

/************************************************************************************
 * AxisSymDraw
 *
 * Draws an axis symbol
 */

static void AxisSymDraw( vec3 Center, const vec3 Axis1Vec, const vec3 Axis2Vec, int AxisSymNr, float SymSize)
{
  vec3 v1, v2;
  float t;

  t = SymSize * 0.3;      // helper to draw symbol

  switch( AxisSymNr) {
  default:
  case 0:   // X

    AxisCompine( v1, Axis1Vec, Axis2Vec, -t,  t);
    AxisCompine( v2, Axis1Vec, Axis2Vec,  t, -t);
    vec_add_to( v1, Center);
    vec_add_to( v2, Center);
    glVertex3fv( v1); glVertex3fv( v2);

    AxisCompine( v1, Axis1Vec, Axis2Vec, -t, -t);
    AxisCompine( v2, Axis1Vec, Axis2Vec,  t,  t);
    vec_add_to( v1, Center);
    vec_add_to( v2, Center);
    glVertex3fv( v1); glVertex3fv( v2);
    break;

  case 1:   // Y

    AxisCompine( v1, Axis1Vec, Axis2Vec, -t,  t);
    AxisCompine( v2, Axis1Vec, Axis2Vec,  0.0,  0.0);
    vec_add_to( v1, Center);
    vec_add_to( v2, Center);
    glVertex3fv( v1); glVertex3fv( v2);

    AxisCompine( v1, Axis1Vec, Axis2Vec,  t,  t);
    AxisCompine( v2, Axis1Vec, Axis2Vec,  0.0,  0.0);
    vec_add_to( v1, Center);
    vec_add_to( v2, Center);
    glVertex3fv( v1); glVertex3fv( v2);

    AxisCompine( v1, Axis1Vec, Axis2Vec,  0.0,  0.0);
    AxisCompine( v2, Axis1Vec, Axis2Vec,  0.0, -t);
    vec_add_to( v1, Center);
    vec_add_to( v2, Center);
    glVertex3fv( v1); glVertex3fv( v2);
    break;

  case 2:   // Z

    AxisCompine( v1, Axis1Vec, Axis2Vec, -t,  t);
    AxisCompine( v2, Axis1Vec, Axis2Vec,  t,  t);
    vec_add_to( v1, Center);
    vec_add_to( v2, Center);
    glVertex3fv( v1); glVertex3fv( v2);

    AxisCompine( v1, Axis1Vec, Axis2Vec,  t,  t);
    AxisCompine( v2, Axis1Vec, Axis2Vec, -t, -t);
    vec_add_to( v1, Center);
    vec_add_to( v2, Center);
    glVertex3fv( v1); glVertex3fv( v2);

    AxisCompine( v1, Axis1Vec, Axis2Vec, -t, -t);
    AxisCompine( v2, Axis1Vec, Axis2Vec,  t, -t);
    vec_add_to( v1, Center);
    vec_add_to( v2, Center);
    glVertex3fv( v1); glVertex3fv( v2);
    break;
  }
}

/************************************************************************************
 * IqeB_DispDisplayInit
 *
 * Display the model
 */

static int IqeB_DispDisplayInit_Called = false;

static void IqeB_DispDisplayInit( void)
{
  char TempString[ 256];

	glEnable(GL_MULTISAMPLE);
	glEnable(GL_NORMALIZE);
	glDepthFunc(GL_LEQUAL);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	IqeB_DispSetOpenGLClearColor( IqeB_DispCol_BACKGROUND, 1.0);

  glEnable( GL_POINT_SMOOTH);   // Round points for glBegin(GL_POINTS)

  //x/glShadeModel(GL_SMOOTH);

  // Load background image
  // NOTE: May use no constante string, it my be lowercased.
  //       This results in a memory write fault!

  strcpy( TempString, (char *)"./Resources/BackGroundImage.png");
  DrawBackGroundImageRef = IqeB_TextureLoadToOpenGL( TempString);

  // ...

  IqeB_DispDisplayInit_Called = true;

  glFlush();
}

/************************************************************************************
 * IqeB_DispDisplayView
 *
 * Display the model
 */

static void IqeB_DispDisplayView( int ViewLayout, int ViewMode, int x, int y, int w, int h)
{
	char buf[ 256];
	int i, DoDrawJointNames, CoordWithField, ThisViewPortNum;
	int DoDrawBlendWeightColors, Axis1SymNr, Axis2SymNr;
	vec3 Axis1Vec, Axis2Vec, v1, v2, Axis1SymV, Axis2SymV;
  char CoordFormatString[ 256];
  float distance;
  T_ViewPort *pThisViewPort;

  // Remember viewport info

  ThisViewPortNum = ViewPorts_n;    // Current viewport number
  pThisViewPort = ViewPortLayout + ThisViewPortNum;

  pThisViewPort->ViewLayout = ViewLayout;
  pThisViewPort->ViewMode   = ViewMode;
  pThisViewPort->x = x;
  pThisViewPort->y = y;
  pThisViewPort->w = w;
  pThisViewPort->h = h;

  ViewPorts_n += 1;

  // ...

	DoDrawJointNames = false;                // preset no draw of bone names

  glViewport( x, y, w, h);

	if( pDrawModel == NULL) {           // no model loaded

    IqeB_DispStyle_JointMouseNearby = -1;    // reset, mouse is no nearby a joint
    goto SkipDrawModel;
	}

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	vec_set( Axis1Vec, 1.0, 0.0, 0.0);      // Preset axis vectors
	vec_set( Axis2Vec, 0.0, 1.0, 0.0);

	vec_set( Axis1SymV, 0.0, 1.0, 0.0);     // Preset axis symbol base vector
	vec_set( Axis2SymV,-1.0, 0.0, 0.0);     // Preset axis symbol height vector

	Axis1SymNr = 0;                         // Preset axis symbol nr (X)
	Axis2SymNr = 1;                         // Preset axis symbol nr (Y)

  if( ViewLayout == 1) {   // 1 view layout

  	distance = camera.distance1V;
  } else {                // 4 view layout, a 2D window

  	distance = camera.distance2V;
  }

	switch( ViewMode) {
  case IQE_VIEWMODE_XY_TOP:
    camera.yaw2D   = 0.0;
    camera.pitch2D = -90.0;
 		orthogonal( distance/2, (float)w/h, camera.distanceMin / 5, camera.distanceMax * 5);

	  vec_set( Axis1SymV, 1.0, 0.0, 0.0);     // Preset axis symbol base vector
	  vec_set( Axis2SymV, 0.0, 1.0, 0.0);     // Preset axis symbol height vector

  	Axis1SymNr = 0;                         // Preset axis symbol nr (X)
	  Axis2SymNr = 1;                         // Preset axis symbol nr (Y)

		break;

  case IQE_VIEWMODE_XY_BOTTOM:
    camera.yaw2D   = 0.0;
    camera.pitch2D = 90.0;
 		orthogonal( distance/2, (float)w/h, camera.distanceMin / 5, camera.distanceMax * 5);

	  vec_set( Axis1SymV, 1.0, 0.0, 0.0);     // Preset axis symbol base vector
	  vec_set( Axis2SymV, 0.0, -1.0, 0.0);    // Preset axis symbol height vector

  	Axis1SymNr = 0;                         // Preset axis symbol nr (X)
	  Axis2SymNr = 1;                         // Preset axis symbol nr (Y)

		break;

  case IQE_VIEWMODE_YZ_FRONT:
    camera.yaw2D   = 90.0;
    camera.pitch2D = 0.0;
 		orthogonal( distance/2, (float)w/h, camera.distanceMin / 5, camera.distanceMax * 5);

  	vec_set( Axis1Vec, 0.0, 0.0, 1.0);      // Z axis
	  vec_set( Axis2Vec, 0.0, 1.0, 0.0);      // Y axis

	  vec_set( Axis1SymV, 0.0, 1.0, 0.0);     // Preset axis symbol base vector
	  vec_set( Axis2SymV, 0.0, 0.0, 1.0);     // Preset axis symbol height vector

	  Axis1SymNr = 2;                         // Preset axis symbol nr (X)
	  Axis2SymNr = 1;                         // Preset axis symbol nr (Y)
		break;

  case IQE_VIEWMODE_YZ_BACK:
    camera.yaw2D   = -90.0;
    camera.pitch2D = 0.0;
 		orthogonal( distance/2, (float)w/h, camera.distanceMin / 5, camera.distanceMax * 5);

  	vec_set( Axis1Vec, 0.0, 0.0, 1.0);      // Z axis
	  vec_set( Axis2Vec, 0.0, 1.0, 0.0);      // Y axis

	  vec_set( Axis1SymV, 0.0, -1.0, 0.0);    // Preset axis symbol base vector
	  vec_set( Axis2SymV, 0.0, 0.0, 1.0);     // Preset axis symbol height vector

	  Axis1SymNr = 2;                         // Preset axis symbol nr (X)
	  Axis2SymNr = 1;                         // Preset axis symbol nr (Y)
		break;

  case IQE_VIEWMODE_ZX_RIGHT:
    camera.yaw2D   = 0.0;
    camera.pitch2D = 0.0;
 		orthogonal( distance/2, (float)w/h, camera.distanceMin / 5, camera.distanceMax * 5);

  	vec_set( Axis1Vec, 0.0, 0.0, 1.0);      // Z axis
	  vec_set( Axis2Vec, 1.0, 0.0, 0.0);      // X axis

	  vec_set( Axis1SymV, 1.0, 0.0, 0.0);     // Preset axis symbol base vector
	  vec_set( Axis2SymV, 0.0, 0.0, 1.0);     // Preset axis symbol height vector

	  Axis1SymNr = 2;                         // Preset axis symbol nr (X)
	  Axis2SymNr = 0;                         // Preset axis symbol nr (Y)
		break;

  case IQE_VIEWMODE_ZX_LEFT:
    camera.yaw2D   = 180.0;
    camera.pitch2D = 0.0;
 		orthogonal( distance/2, (float)w/h, camera.distanceMin / 5, camera.distanceMax * 5);

  	vec_set( Axis1Vec, 0.0, 0.0, 1.0);      // Z axis
	  vec_set( Axis2Vec, 1.0, 0.0, 0.0);      // X axis

	  vec_set( Axis1SymV, -1.0, 0.0, 0.0);    // Preset axis symbol base vector
	  vec_set( Axis2SymV, 0.0, 0.0, 1.0);     // Preset axis symbol height vector

	  Axis1SymNr = 2;                         // Preset axis symbol nr (X)
	  Axis2SymNr = 0;                         // Preset axis symbol nr (Y)
		break;

  case IQE_VIEWMODE_ORTHO:
 		orthogonal( distance/2, (float)w/h, camera.distanceMin / 5, camera.distanceMax * 5);

		break;

  default:
  case IQE_VIEWMODE_PERSP:
		perspective(50, (float)w/h, camera.distanceMin / 5, camera.distanceMax * 5);

		break;
	}

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(-90, 1, 0, 0); // Z-up

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);

	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

  if( ViewLayout == 1) {   // 1 view layout

	  glTranslatef( -camera.DispShift1V[ 0],
                  -camera.DispShift1V[ 1],
                  -camera.DispShift1V[ 2]);

  }

  glTranslatef(0, distance, 0);

  if( ViewMode >= IQE_VIEWMODE_3D) {   // 3D view modes

  	glRotatef(-camera.pitch3D, 1, 0, 0);
	  glRotatef(-camera.yaw3D, 0, 0, 1);
  } else {                                            // 2D view mode

  	glRotatef(-camera.pitch2D, 1, 0, 0);
	  glRotatef(-camera.yaw2D, 0, 0, 1);
  }

	glTranslatef( -camera.ObjCenter[0],
                -camera.ObjCenter[1],
                -camera.ObjCenter[2]);

  if( ViewLayout != 1) {   // 4 view layout, a 2D view


	  glTranslatef( -camera.DispShift2V[ 0],
                  -camera.DispShift2V[ 1],
                  -camera.DispShift2V[ 2]);

  }

  // mouse coordindate to 3D coordinates

  if( CoordAfterpointDigits >= 0) {
    CoordWithField = 5;
  } else {
    CoordWithField = CoordAfterpointDigits + 4;
  }

  strcpy( DrawMouseCoordinate, "");
  strcpy( DrawMouseEditMode, "");
  strcpy( DrawMouseEditModeErr, "");

  if( ViewMode < IQE_VIEWMODE_3D &&            // 2D view mode
      ThisViewPortNum == ViewPort_MouseNum) {  // mouse inside this view

    float TempX, TempY, TempZ;

    //

    if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_SKEL_EDIT) {   // Mouse mode: Skeleton edit

      if( pDrawModel->skel && pDrawModel->skel->joint_count > 0 &&  // have skeleton
          pDrawModel->skel->IsNormalized == false) {                // and skeleton is not normalized

        strcpy( DrawMouseEditModeErr, "Skeleton is NOT normalized ");
        strcpy( DrawMouseEditMode,    "NO shift of joints possible ");
      } else {

        strcpy( DrawMouseEditMode, "Shift selected joint:");
      }

    } else if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_ANIM_EDIT &&  // Mouse mode: Animation edit and not angles
               IqeB_ToolsAnimEdit_PosePart != IQE_ANIM_EDIT_POSE_PART_ANGLES) {

#ifdef IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
      if( pDrawModel->skel && pDrawModel->skel->joint_count > 0 &&   // have skeleton
          pDrawModel->skel->IsNormalized == false ) {                // and skeleton is not normalized


        strcpy( DrawMouseEditModeErr, "Skeleton is NOT normalized ");
        strcpy( DrawMouseEditMode,    "NO shift of joints possible ");

      } else
#endif // IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
      if( IqeB_DispStyle_doplay) {

        strcpy( DrawMouseEditModeErr, "Play animation is on ");
        strcpy( DrawMouseEditMode,    "Select joints:");

      } else {

        if( IqeB_ToolsAnimEdit_PosePart == IQE_ANIM_EDIT_POSE_PART_SCALE) {  // Animation edit and scale

          strcpy( DrawMouseEditMode, "Scale selected joint:");
        } else {
          strcpy( DrawMouseEditMode, "Shift selected joint:");
        }
      }

    } else if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_JOINT_SEL) { // Mouse mode: Skeleton joint select

      strcpy( DrawMouseEditMode, "Toggle joint selection:");

    } else if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_ANIM_EDIT &&  // Mouse mode: Animation edit
               IqeB_ToolsAnimEdit_PosePart == IQE_ANIM_EDIT_POSE_PART_ANGLES) { // edit angles

#ifdef IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
      if( pDrawModel->skel && pDrawModel->skel->joint_count > 0   // have skeleton
           && pDrawModel->skel->IsNormalized == false            // and skeleton is not normalized
           ) {

        strcpy( DrawMouseEditModeErr, "Skeleton is NOT normalized ");
        strcpy( DrawMouseEditMode,    "Select joints:");

      } else
#endif // IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
      if( IqeB_DispStyle_doplay) {

        strcpy( DrawMouseEditModeErr, "Play animation is on ");
        strcpy( DrawMouseEditMode,    "Select joints:");

      } else {
        strcpy( DrawMouseEditMode, "Select joints / change angles:");
      }
    }

    //

    getPixel2dTo3D( ViewPort_MouseX, ViewPort_MouseY,
                    &TempX, &TempY, &TempZ);

	  switch( ViewMode) {
    case IQE_VIEWMODE_XY_TOP:
    case IQE_VIEWMODE_XY_BOTTOM:

	    sprintf( CoordFormatString, "X: %%%d.%df  Y: %%%d.%df  Z: %*s",
                                  CoordWithField, CoordAfterpointDigits,
                                  CoordWithField, CoordAfterpointDigits,
                                  CoordWithField, "---");

      sprintf( DrawMouseCoordinate, CoordFormatString,
               TempX, TempY);
      break;

    case IQE_VIEWMODE_YZ_FRONT:
    case IQE_VIEWMODE_YZ_BACK:

	    sprintf( CoordFormatString, "X: %*s  Y: %%%d.%df  Z: %%%d.%df",
                                  CoordWithField, "---",
                                  CoordWithField, CoordAfterpointDigits,
                                  CoordWithField, CoordAfterpointDigits);

      sprintf( DrawMouseCoordinate, CoordFormatString,
               TempY, TempZ);

      break;

    case IQE_VIEWMODE_ZX_RIGHT:
    case IQE_VIEWMODE_ZX_LEFT:

	    sprintf( CoordFormatString, "X: %%%d.%df  Y: %*s  Z: %%%d.%df",
                                  CoordWithField, CoordAfterpointDigits,
                                  CoordWithField, "---",
                                  CoordWithField, CoordAfterpointDigits);

      sprintf( DrawMouseCoordinate, CoordFormatString,
               TempX, TempZ);
      break;

    default:

      strcpy( DrawMouseCoordinate, "");
      break;
	  } // end switch()
  }

	// setup drawing of blend colors
  if( IqeB_DispStyle_doBlendWeightColors &&        // drawing blend colors is on
      ( pDrawModel->mesh->blendColors == NULL ||   // and no blend colors
        pDrawModel->mesh->blendColorsRebuild)) {   //   or rebuild blend colors

    IqeB_ModelBuildBlendColors( pDrawModel);  // build the blend colors
  }

	if( IqeB_DispStyle_doBlendWeightColors && pDrawModel->mesh->blendColors) {  // Display blend weight colors

    DoDrawBlendWeightColors = true;

		glDisable(GL_TEXTURE_2D);                  // textures off

	} else {

    DoDrawBlendWeightColors = false;

  	if (IqeB_DispStyle_dotexture)
	  	glEnable(GL_TEXTURE_2D);
	  else
		  glDisable(GL_TEXTURE_2D);

	}

  if (IqeB_DispStyle_dowire)
  	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  else
	  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	if (IqeB_DispStyle_dobackface)
		glDisable(GL_CULL_FACE);
	else
		glEnable(GL_CULL_FACE);

	glAlphaFunc(GL_GREATER, 0.2);
	glEnable(GL_ALPHA_TEST);

	drawmodel( pDrawModel, DoDrawBlendWeightColors);

	glDisable(GL_ALPHA_TEST);

	glDisable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_TEXTURE_2D);

	glDisable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);

	if( IqeB_DispStyle_donormals && pDrawModel->mesh->normal != NULL) {

    float NormalLength;

    NormalLength = gridsize * 0.025;    // This part of grid size
    if( NormalLength < 0.05) {         // clip
      NormalLength = 0.05;
    }
    if( NormalLength > 1.0) {         // clip
      NormalLength = 1.0;
    }

		glBegin(GL_LINES);
		IqeB_DispSetOpenGLColor4f( IqeB_DispCol_NORMALS, 1);

  	for (i = 0; i < pDrawModel->mesh->vertex_count; i++) {

      vec3 p, n;

      if( IqeB_DispStyle_doAnimations && pDrawModel->mesh->aposition && pDrawModel->mesh->anormal) { // animated postion and normal
        vec_copy( p, pDrawModel->mesh->aposition + i * 3);  // get point
        vec_copy( n, pDrawModel->mesh->anormal + i * 3);    // get normal
      } else {                                              // static mesh
        vec_copy( p, pDrawModel->mesh->position + i * 3);   // get point
        vec_copy( n, pDrawModel->mesh->normal + i * 3);     // get normal
      }
      vec_scale_to( n, NormalLength);                       // scale length for visualisation
      vec_add_to( n, p);                                    // end point of line we draw

      glVertex3f( p[ 0],  p[ 1],  p[ 2]);
      glVertex3f( n[ 0],  n[ 1],  n[ 2]);
  	}

		glEnd();
	}

#ifdef _DEBUG
#ifdef use_again
  // Draw normals of trinangles

  {
    int j;
    struct mesh *mesh;
    float NormalLength;

    NormalLength = gridsize * 0.025;    // This part of grid size
    if( NormalLength < 0.05) {         // clip
      NormalLength = 0.05;
    }
    if( NormalLength > 1.0) {         // clip
      NormalLength = 1.0;
    }

    mesh = pDrawModel->mesh;

		glBegin(GL_LINES);

  	for (i = 0; i < mesh->part_count; i++) {

      for( j = 0; j < mesh->part[i].count; j += 3) {

        int i1, i2, i3;
        vec3 p1, p2, p3, d1, d2, normal, center;

        // get points of triangle
        i1 = mesh->element[ mesh->part[ i].first + j + 0];
        i2 = mesh->element[ mesh->part[ i].first + j + 1];
        i3 = mesh->element[ mesh->part[ i].first + j + 2];

        vec_copy( p1, mesh->position + i1 * 3);  // get point
        vec_copy( p2, mesh->position + i2 * 3);  // get point
        vec_copy( p3, mesh->position + i3 * 3);  // get point

        // normal on triangle
        vec_sub( d1, p2, p1);
        vec_sub( d2, p3, p1);
        vec_cross( normal, d1, d2);
        vec_normalize( normal);
        vec_scale_to( normal, NormalLength);   // scale length for visualisation

        // Center of grafity of points

        vec_set( center, 0.0, 0.0, 0.0);
        vec_add_to( center, p1);
        vec_add_to( center, p2);
        vec_add_to( center, p3);
        vec_scale_to( center, 1.0 / 3.0);

        // draw from edge points to center
		    glColor4f( 0.4, 0.8, 0.4, 1);
        glVertex3f( center[ 0], center[ 1], center[ 2]);
        glVertex3f( p1[ 0], p1[ 1], p1[ 2]);
        glVertex3f( center[ 0], center[ 1], center[ 2]);
        glVertex3f( p2[ 0], p2[ 1], p2[ 2]);
        glVertex3f( center[ 0], center[ 1], center[ 2]);
        glVertex3f( p3[ 0], p3[ 1], p3[ 2]);

        // draw normal from center
        vec_copy( p1, center);
        vec_add_to( p1, normal);

		    glColor4f( 0.2, 1.0, 0.2, 1);
        glVertex3f( center[ 0], center[ 1], center[ 2]);
        glVertex3f( p1[ 0], p1[ 1], p1[ 2]);
      }
	  }

		glEnd();
  }
#endif
#endif

	if( IqeB_DispStyle_doplane) {  // draw groundplane

    float g;

		glBegin(GL_LINES);

		// Draw Grid
		IqeB_DispSetOpenGLColor4f( IqeB_DispCol_GROUND_PLANE, 1);
		for (g = 0; g >= - gridsize; g -= gridstep) {
      if( g == 0.0) {

        AxisCompine( v1, Axis1Vec, Axis2Vec, g, -gridsize);
        AxisCompine( v2, Axis1Vec, Axis2Vec, g, 0);
			  glVertex3fv( v1); glVertex3fv( v2);

        AxisCompine( v1, Axis1Vec, Axis2Vec, -gridsize, g);
        AxisCompine( v2, Axis1Vec, Axis2Vec, 0, g);
			  glVertex3fv( v1); glVertex3fv( v2);
      } else {

        AxisCompine( v1, Axis1Vec, Axis2Vec, g, -gridsize);
        AxisCompine( v2, Axis1Vec, Axis2Vec, g, gridsize);
			  glVertex3fv( v1); glVertex3fv( v2);

        AxisCompine( v1, Axis1Vec, Axis2Vec, -gridsize, g);
        AxisCompine( v2, Axis1Vec, Axis2Vec, gridsize, g);
			  glVertex3fv( v1); glVertex3fv( v2);
      }
		}
		for (g = gridstep; g <= gridsize; g += gridstep) {
      if( g == 0.0) {

        AxisCompine( v1, Axis1Vec, Axis2Vec, g, -gridsize);
        AxisCompine( v2, Axis1Vec, Axis2Vec, g, 0);
			  glVertex3fv( v1); glVertex3fv( v2);

        AxisCompine( v1, Axis1Vec, Axis2Vec, -gridsize, g);
        AxisCompine( v2, Axis1Vec, Axis2Vec, 0, g);
			  glVertex3fv( v1); glVertex3fv( v2);
      } else {

        AxisCompine( v1, Axis1Vec, Axis2Vec, g, -gridsize);
        AxisCompine( v2, Axis1Vec, Axis2Vec, g, gridsize);
			  glVertex3fv( v1); glVertex3fv( v2);

        AxisCompine( v1, Axis1Vec, Axis2Vec, -gridsize, g);
        AxisCompine( v2, Axis1Vec, Axis2Vec, gridsize, g);
			  glVertex3fv( v1); glVertex3fv( v2);
      }
		}

		glEnd();

		// Draw 1. axis

	  glLineWidth( 3.0);           // like bigger lines
		glBegin(GL_LINES);

		IqeB_DispSetOpenGLColor4f( IqeB_DispCol_AXIS, 1);
    AxisCompine( v1, Axis1Vec, Axis2Vec, 0, 0);
    AxisCompine( v2, Axis1Vec, Axis2Vec, gridsize + 1.0 * gridstep, 0);
    glVertex3fv( v1); glVertex3fv( v2);

    AxisCompine( v1, Axis1Vec, Axis2Vec, gridsize + 1.0 * gridstep, 0);
    AxisCompine( v2, Axis1Vec, Axis2Vec, gridsize + 0.5 * gridstep, 0.5 * gridstep);
    glVertex3fv( v1); glVertex3fv( v2);

    AxisCompine( v1, Axis1Vec, Axis2Vec, gridsize + 1.0 * gridstep, 0);
    AxisCompine( v2, Axis1Vec, Axis2Vec, gridsize + 0.5 * gridstep, -0.5 * gridstep);
    glVertex3fv( v1); glVertex3fv( v2);

		glEnd();
	  glLineWidth( 1.0);           // reset line size

		glBegin(GL_LINES);

    AxisCompine( v1, Axis1Vec, Axis2Vec, gridsize + 1.5 * gridstep, 0);
    AxisSymDraw( v1, Axis1SymV, Axis2SymV, Axis1SymNr, gridstep);

		glEnd();

		// Draw 2. axis

	  glLineWidth( 3.0);           // like bigger lines
		glBegin(GL_LINES);

		IqeB_DispSetOpenGLColor4f( IqeB_DispCol_AXIS, 1);
    AxisCompine( v1, Axis1Vec, Axis2Vec, 0, 0);
    AxisCompine( v2, Axis1Vec, Axis2Vec, 0, gridsize + 1.0 * gridstep);
    glVertex3fv( v1); glVertex3fv( v2);

    AxisCompine( v1, Axis1Vec, Axis2Vec, 0, gridsize + 1.0 * gridstep);
    AxisCompine( v2, Axis1Vec, Axis2Vec, 0.5 * gridstep, gridsize + 0.5 * gridstep);
    glVertex3fv( v1); glVertex3fv( v2);

    AxisCompine( v1, Axis1Vec, Axis2Vec, 0, gridsize + 1.0 * gridstep);
    AxisCompine( v2, Axis1Vec, Axis2Vec, -0.5 * gridstep, gridsize + 0.5 * gridstep);
    glVertex3fv( v1); glVertex3fv( v2);

		glEnd();
	  glLineWidth( 1.0);           // reset line size

		glBegin(GL_LINES);

    AxisCompine( v1, Axis1Vec, Axis2Vec, 0, gridsize + 1.5 * gridstep);
    AxisSymDraw( v1, Axis1SymV, Axis2SymV, Axis2SymNr, gridstep);

		glEnd();
	}

	if( IqeB_ToolsDisp_BoxEnable) {  // draw reference box

    float t, b, w2;
		vec3 cg0, cg1, cg2, cg3, ct0, ct1, ct2, ct3, cb0, cb1, cb2, cb3;

    t  = IqeB_ToolsDisp_BoxHeight1;
    b  = IqeB_ToolsDisp_BoxHeight2;
    w2 = IqeB_ToolsDisp_BoxWidth * 0.5;

		// grou d plsnr box points

		vec_set( cg0,   w2,   w2, 0.0);
		vec_set( cg1,   w2, - w2, 0.0);
		vec_set( cg2, - w2, - w2, 0.0);
		vec_set( cg3, - w2,   w2, 0.0);

		// top box points

		vec_set( ct0,   w2,   w2, t);
		vec_set( ct1,   w2, - w2, t);
		vec_set( ct2, - w2, - w2, t);
		vec_set( ct3, - w2,   w2, t);

		// bottom box points

		vec_set( cb0,   w2,   w2, b);
		vec_set( cb1,   w2, - w2, b);
		vec_set( cb2, - w2, - w2, b);
		vec_set( cb3, - w2,   w2, b);

    // draw it

	  glLineWidth( 3.0);           // like bigger lines

		glBegin(GL_LINES);

		if( (t > 0.0 && b < 0.0) || (t < 0.0 && b > 0.0)) {

		  IqeB_DispSetOpenGLColor4f( IqeB_DispCol_BOX_GROUND, 1);

      glVertex3fv( cg0); glVertex3fv( cg1);
      glVertex3fv( cg1); glVertex3fv( cg2);
      glVertex3fv( cg2); glVertex3fv( cg3);
      glVertex3fv( cg3); glVertex3fv( cg0);
		}

		IqeB_DispSetOpenGLColor4f( IqeB_DispCol_BOX, 1);

    glVertex3fv( ct0); glVertex3fv( ct1);
    glVertex3fv( ct1); glVertex3fv( ct2);
    glVertex3fv( ct2); glVertex3fv( ct3);
    glVertex3fv( ct3); glVertex3fv( ct0);

    glVertex3fv( cb0); glVertex3fv( cb1);
    glVertex3fv( cb1); glVertex3fv( cb2);
    glVertex3fv( cb2); glVertex3fv( cb3);
    glVertex3fv( cb3); glVertex3fv( cb0);

    glVertex3fv( ct0); glVertex3fv( cb0);
    glVertex3fv( ct1); glVertex3fv( cb1);
    glVertex3fv( ct2); glVertex3fv( cb2);
    glVertex3fv( ct3); glVertex3fv( cb3);

		glEnd();

	  glLineWidth( 1.0);           // reset line size
	}

	glDisable(GL_DEPTH_TEST);

	if (IqeB_DispStyle_doskeleton) {
		drawskeleton( pDrawModel, ThisViewPortNum);
  	DoDrawJointNames = IqeB_DispStyle_doJointNames;    // set no draw of joint names
	} else {

    IqeB_DispStyle_JointMouseNearby = -1;    // reset, mouse is not nearby a joint
  }

SkipDrawModel:

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho( 0, w, h, 0, -1, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if( pDrawModel && DoDrawJointNames) {

	  struct skel *skel = pDrawModel->skel;

  	IqeB_DispSetOpenGLColor4f( IqeB_DispCol_JOINT_NAMES, 1);

  	for (i = 0; i < skel->joint_count; i++) {

  	  drawstring( pThisViewPort->SkelDrawPosX[ i] + 6, pThisViewPort->SkelDrawPosY[ i] + 4, skel->j[ i].name);
  	}
	}

	IqeB_DispSetOpenGLColor4f( IqeB_DispCol_INFOS, 1);

  if( pDrawModel != NULL) {

    DisplayedAnyModel = true;   // displayed any model

	  if( (IqeB_DispStyle_NumViewPorts == IQE_VIEW_PORT_NUM_1 || ThisViewPortNum == 2)) {

  	  sprintf(buf, "%d meshes; %d vertices; %d faces;",
	  	  pDrawModel->mesh->part_count,
        pDrawModel->mesh->vertex_count,
        pDrawModel->mesh->element_count/3);
	    drawstring(8, 18+0, buf);

  	  sprintf(buf, "%d joints; %d animations; %d poses",
        pDrawModel->skel->joint_count,
        pDrawModel->anim_count,
        pDrawModel->anim_poses);
	    drawstring(8, 18+16, buf);

	    sprintf( buf, "Model sizes: %.2f / %.2f / %.2f",
        camera.ObjSize[ 0], camera.ObjSize[ 1], camera.ObjSize[ 2]);
	    drawstring(8, 18+32, buf);

	    if (pDrawModelCurAnim) {
        int ThisAnimNum;

        for( ThisAnimNum = 0; ThisAnimNum < pDrawModel->anim_count; ThisAnimNum++) {

          if( pDrawModel->anim_data[ ThisAnimNum] == pDrawModelCurAnim) {  // have not reched the active animation

            break;
          }
        }

		    sprintf(buf, "%s %2d / %2d (%03d / %03d), %.1f fps",
          pDrawModelCurAnim->name, ThisAnimNum + 1, pDrawModel->anim_count,
          DrawModelCurframe + 1, pDrawModelCurAnim->len, pDrawModelCurAnim->framerate);
		    drawstring(8, 18+48, buf);
	    }
	  }

	  // viewport type

    if( ViewMode < IQE_VIEWMODE_3D) {            // 2D view mode

	    Fl_Font CurrFont;
	    Fl_Fontsize CurrFontSize;
	    char *pTextBig, *pTextLeft, *pTextBottom;

	    pTextBig = NULL;  // Preset no text set

	    switch( ViewMode) {
      case IQE_VIEWMODE_XY_TOP:    // X/Y plane
      case IQE_VIEWMODE_XY_BOTTOM: // X/Y plane
        pTextBig    = (char *)"Z";
        pTextLeft   = (char *)"Y";
        pTextBottom = (char *)"X";
        break;
      case IQE_VIEWMODE_YZ_FRONT:  // Y/Z plane
      case IQE_VIEWMODE_YZ_BACK:   // Y/Z plane
        pTextBig    = (char *)"X";
        pTextLeft   = (char *)"Z";
        pTextBottom = (char *)"Y";
        break;
      case IQE_VIEWMODE_ZX_RIGHT:  // Z/X plane
      case IQE_VIEWMODE_ZX_LEFT:   // Z/X plane
        pTextBig    = (char *)"Y";
        pTextLeft   = (char *)"Z";
        pTextBottom = (char *)"X";
        break;
	    }

	    if( pTextBig != NULL) {    // Have a pointer

  	    // get info about default fond

  	    CurrFont = fl_font();
	      CurrFontSize = fl_size();

        gl_font( CurrFont, 20);

	      IqeB_DispSetOpenGLColor4f( IqeB_DispCol_INFOS, 1);
        gl_draw( pTextBig, w - 20, 20);

        // Restore default font

        gl_font( CurrFont, CurrFontSize);

        gl_draw( pTextLeft,   w - 30, 17);
        gl_draw( pTextBottom, w - 17, 32);
	    }
    }

	  // mouse coordindate

	  drawstring( w / 2, h - 6, DrawMouseCoordinate);

	  if( DrawMouseEditModeErr[0] != '\0') {

      glColor4f( DRAW_MSG_COL_ERR, 1);

      if( DrawMouseEditMode[0] != '\0') {
        drawstring_right( w / 2 - 4, h - 20, DrawMouseEditModeErr);
    	  drawstring_right( w / 2 - 4, h - 6, DrawMouseEditMode);
      } else {
        drawstring_right( w / 2 - 4, h - 6, DrawMouseEditModeErr);
      }

    	IqeB_DispSetOpenGLColor4f( IqeB_DispCol_INFOS, 1);

	  } else if( DrawMouseEditMode[0] != '\0') {

  	  drawstring_right( w / 2 - 4, h - 6, DrawMouseEditMode);
	  }

	  // Special draw for IQE_GUI_MOUSE_MODE_ANIM_EDIT

    if( IqeB_DispStyle_MouseMode == IQE_GUI_MOUSE_MODE_ANIM_EDIT && // Mouse mode: Animation edit
        pDrawModel->skel && pDrawModel->skel->joint_count > 0      // have skeleton
#ifdef IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
         && pDrawModel->skel->IsNormalized
#endif // IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
        ) {                           // and skeleton is normalized

      if( DrawMouseAnimEnabled &&                  // Enabled line drawing
          ThisViewPortNum == ViewPort_MouseNum) {  // mouse inside this view

        if( DrawMouseAnimNunmLines >= 1) {

          IqeB_DispSetOpenGLColor4f( DrawMouseAnimLineCol1, 1);

          glBegin(GL_LINES);
          glVertex2f( DrawMouseAnimJX,  DrawMouseAnimJY);
          glVertex2f( DrawMouseAnimP1X, DrawMouseAnimP1Y);
		      glEnd();
        }

        if( DrawMouseAnimNunmLines >= 2) {

          IqeB_DispSetOpenGLColor4f( DrawMouseAnimLineCol2, 1);

          glBegin(GL_LINES);
          glVertex2f( DrawMouseAnimJX,  DrawMouseAnimJY);
          glVertex2f( DrawMouseAnimP2X, DrawMouseAnimP2Y);
		      glEnd();
        }
      }
    }

  } else {

    // Display background image (if we have one)

	  if( ! DisplayedAnyModel &&           // no model displayed until now
        DrawBackGroundImageRef > 0) {    // have background image

      int SizeImage = 512;
      int XPos, YPos;

      // Adapt size

      if( SizeImage > w) SizeImage = w;
      if( SizeImage > h) SizeImage = h;

      // Pos

      XPos = x + (w - SizeImage) / 2;
      YPos = y + (h - SizeImage) / 2;

      // draw it ..

      glAlphaFunc(GL_GREATER, 0.2);
      glEnable(GL_ALPHA_TEST);
	  	glEnable(GL_TEXTURE_2D);

      glColor4f( 1.0, 1.0, 1.0, 1.0);
      glBindTexture( GL_TEXTURE_2D, DrawBackGroundImageRef);

      glBegin(GL_QUADS);

      glVertex2f( XPos            , YPos            ); glTexCoord2f( 1.0, 0.0);
      glVertex2f( XPos + SizeImage, YPos            ); glTexCoord2f( 1.0, 1.0);
      glVertex2f( XPos + SizeImage, YPos + SizeImage); glTexCoord2f( 0.0, 1.0);
      glVertex2f( XPos            , YPos + SizeImage); glTexCoord2f( 0.0, 0.0);

      glEnd();

		  glDisable(GL_TEXTURE_2D);
      glDisable(GL_ALPHA_TEST);
    }

    // ...

	  sprintf(buf, "No model loaded");
	  drawstring(8, 18+0, buf);

	  if( ! DisplayedAnyModel) {   // no model displayed until now

      int yPos, BoxSize;

	    // get info about default fond

	    Fl_Font CurrFont;
	    Fl_Fontsize CurrFontSize;

	    CurrFont = fl_font();
	    CurrFontSize = fl_size();

	    // Program Version

      if( DrawBackGroundImageRef > 0) {    // have background image
	      yPos = y + 80;
      } else {
	      yPos = h / 3;
      }

      IqeB_DispSetOpenGLColor4f( IqeB_DispCol_VERSION, 1);
      gl_font( CurrFont, 36);

      gl_draw( WIN_PROG_NAME, 4, yPos, w - 8, 1, FL_ALIGN_TOP);
      yPos += 38;

      gl_draw( WIN_PROG_VERSION, 4, yPos, w - 8, 1, FL_ALIGN_TOP);

      if( DrawBackGroundImageRef > 0) {    // have background image
	      yPos = y + h - 35;
      } else {
        yPos += 38;
        yPos += 38;
      }

	    IqeB_DispSetOpenGLColor4f( IqeB_DispCol_INFOS, 1);
      gl_font( CurrFont, 20);

      gl_draw( "Select a model with 'file browser'", 4, yPos, w - 8, 1, FL_ALIGN_TOP);

      // Restore default font

      gl_font( CurrFont, CurrFontSize);

      // some more hints

      if( DrawBackGroundImageRef > 0) {    // have background image

        BoxSize = 200;
	      yPos = y + h / 2 - 60;
      } else {

        BoxSize = w - 8;
        yPos += 28;
        yPos += 28;
      }

      gl_draw( "2D view", 4, yPos, BoxSize, 1, FL_ALIGN_TOP);
      yPos += CurrFontSize + 2;
      gl_draw( "Left mouse button: shift view or edit", 4, yPos, BoxSize, 1, FL_ALIGN_TOP);
      yPos += CurrFontSize + 2;
      gl_draw( "Right mouse button: shift view", 4, yPos, BoxSize, 1, FL_ALIGN_TOP);
      yPos += CurrFontSize + 2;
      yPos += 8;

      gl_draw( "3D view", 4, yPos, BoxSize, 1, FL_ALIGN_TOP);
      yPos += CurrFontSize + 2;
      gl_draw( "Left mouse button: shift view", 4, yPos, BoxSize, 1, FL_ALIGN_TOP);
      yPos += CurrFontSize + 2;

      gl_draw( "Right mouse button: rotate view", 4, yPos, BoxSize, 1, FL_ALIGN_TOP);
      yPos += CurrFontSize + 2;
      yPos += 8;

      gl_draw( "Mouse wheel: zoom", 4, yPos, BoxSize, 1, FL_ALIGN_TOP);
      yPos += CurrFontSize + 2;
	  }
  }

	// draw message

	if( DrawMessageText1[ 0] != '\0' &&   // have to draw message
      (IqeB_DispStyle_NumViewPorts == IQE_VIEW_PORT_NUM_1 || ThisViewPortNum == 0)) {

    glColor4f( DrawMessageColR, DrawMessageColG, DrawMessageColB, 1);

		drawstring( 8, h - 42, DrawMessageText1);
		drawstring( 8, h - 26, DrawMessageText2);
		drawstring( 8, h - 10, DrawMessageText3);
	}

	i = glGetError();

	if (i) {
    //x/fprintf(stderr, "opengl error: %d\n", i);
	}

  glFinish();
}

/************************************************************************************
 * IqeB_DispDisplay
 *
 * Display the model
 */

static void IqeB_DispDisplayResetTo3D() // Reset to 2D view
{

    glViewport( 0, 0, DrawViewport_w, DrawViewport_h);

	  glMatrixMode(GL_PROJECTION);
	  glLoadIdentity();
	  glOrtho( 0, DrawViewport_w, DrawViewport_h, 0, -1, 1);

	  glMatrixMode(GL_MODELVIEW);
	  glLoadIdentity();
}

void IqeB_DispDisplay( unsigned int BackgroundColor)
{
	unsigned int timediff;

	if( !IqeB_DispDisplayInit_Called) {

    IqeB_DispDisplayInit();
	}

	IqeB_Disp_LastDrawTime = IqeB_Disp_GetTickCount(); // time this function is called
	timediff = IqeB_Disp_LastDrawTime - lasttime;      // time difference to last call
	lasttime = IqeB_Disp_LastDrawTime;                 // rember for last time called

  // View complete OpenGL view
  glViewport( 0, 0, DrawViewport_w, DrawViewport_h);

	IqeB_DispSetOpenGLClearColor( IqeB_DispCol_BACKGROUND, 1.0);  // Maybe IqeB_DispCol_BACKGROUND has changed
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // animate model

	if( pDrawModel == NULL || pDrawModel->anim_data == NULL) {  // No model or no animation data

    pDrawModelCurAnim = NULL;

	} else if( IqeB_DispStyle_doAnimations && IqeB_DispStyle_doplay && !pDrawModelCurAnim && pDrawModel->anim_count > 0) {
    pDrawModelCurAnim = pDrawModel->anim_data[ 0];
		lasttime = IqeB_Disp_GetTickCount();
	}

	if( IqeB_DispStyle_doAnimations && IqeB_DispStyle_doplay && pDrawModelCurAnim) {

    int LastFrame;

    LastFrame = DrawModelCurframe;

		curtime = curtime + (timediff / 1000.0) * pDrawModelCurAnim->framerate;   // animate with given frames per second
		DrawModelCurframe = ((int)curtime) % pDrawModelCurAnim->len;
		IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);

		// play one ?

		if( IqeB_DispStyle_doPlayOnce &&                // 'play once' is set and
        pDrawModelCurAnim->len > 1 &&               // have more than one animation
        DrawModelCurframe == 0 && LastFrame > 0) {  // Changed to first frame

      // Play animations to off
      IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doplay, 0); // Update variable and GUI
    }
	}

	// draw the views

	ViewPorts_n = 0;

  if( IqeB_DispStyle_NumViewPorts == IQE_VIEW_PORT_NUM_1 ||  // one view
      ! DisplayedAnyModel) {                                 // or not displayed any model until now

    IqeB_DispDisplayView( 1, IqeB_DispStyle_ViewMode, 0, 0, DrawViewport_w, DrawViewport_h);

  } else {   // must be 4 views

    int QuadXX, QuadYY, PosX2, PosY2, i, n;

    // calculations for layout

    QuadXX = (DrawViewport_w - 3) / 2;
    QuadYY = (DrawViewport_h - 3) / 2;

    PosX2 = DrawViewport_w - QuadXX;
    PosY2 = DrawViewport_h - QuadYY;

    // NOTE the order of drawing and thre gl_rectf() calls to clear
    // the background. This is because 3D text drawings draw also into
    // other views (a problem with viewport clipping).

	  // 1. view

    IqeB_DispDisplayView( 2, IQE_VIEWMODE_XY_TOP,       0,     0, QuadXX, QuadYY);

	  // 2. view

    IqeB_DispDisplayResetTo3D(); // Reset to 2D view
    IqeB_DispSetOpenGLColor4f( IqeB_DispCol_BACKGROUND, 1);
    gl_rectf( PosX2, PosY2, QuadXX, QuadYY);

    IqeB_DispDisplayView( 1, IqeB_DispStyle_ViewMode, PosX2,     0, QuadXX, QuadYY);

	  // 3. view

    IqeB_DispDisplayResetTo3D(); // Reset to 2D view
    IqeB_DispSetOpenGLColor4f( IqeB_DispCol_BACKGROUND, 1);
    gl_rectf( 0, 0, QuadXX, QuadYY);

    IqeB_DispDisplayView( 2, IQE_VIEWMODE_ZX_RIGHT,         0, PosY2, QuadXX, QuadYY);

	  // 4. view

    IqeB_DispDisplayResetTo3D(); // Reset to 2D view
    IqeB_DispSetOpenGLColor4f( IqeB_DispCol_BACKGROUND, 1);
    gl_rectf( PosX2, 0, QuadXX, QuadYY);

    IqeB_DispDisplayView( 2, IQE_VIEWMODE_YZ_FRONT,   PosX2, PosY2, QuadXX, QuadYY);

	  // Draw the crosses

    IqeB_DispDisplayResetTo3D(); // Reset to 2D view

  	IqeB_DispSetOpenGLColor4f( BackgroundColor, 1);

  	glBegin(GL_LINES);

	  n = DrawViewport_w - QuadXX - QuadXX;  // Number of columns

	  for( i = 0; i < n; i++) {

      glVertex2f( QuadXX + i + 1, 0);
      glVertex2f( QuadXX + i + 1, DrawViewport_h);
	  }

	  n = DrawViewport_h - QuadYY - QuadYY;  // Number of lines

	  for( i = 0; i < n; i++) {

      glVertex2f( 0, QuadYY + i);
      glVertex2f( DrawViewport_w, QuadYY + i);
	  }

  	glEnd();
  }
}

/************************************************************************************
 * IqeB_DispViewReset
 */

void IqeB_DispViewReset( void)
{
  camera.distance1V = camera.distanceCenter;
  camera.distance2V = camera.distanceCenter;
  camera.yaw3D      = 45;
  camera.pitch3D    = -DIMETRIC;

  vec_set( camera.DispShift1V, 0, 0, 0);
  vec_set( camera.DispShift2V, 0, 0, 0);
}

/************************************************************************************
 * IqeB_DispViewAdaptFromModel
 */

void IqeB_DispViewAdaptFromModel( struct IQE_model *pModel)
{
  double radius, radius2;

  if( pModel == NULL) {    // security test

    return;
  }

  radius = IqeB_Measuremodel( pModel, camera.ObjCenter, camera.ObjSize);

  if( radius < 0.01) radius = 0.01; // clip to minimum

  if( IqeB_ToolsDisp_BoxEnable) {  // Also show a box in the reference area

    radius2 = fabs( IqeB_ToolsDisp_BoxHeight1 * 1.1);

    if( radius < radius2) {

      radius = radius2;
    }

    radius2 = fabs( IqeB_ToolsDisp_BoxHeight2 * 1.1);

    if( radius < radius2) {

      radius = radius2;
    }

    radius2 = fabs( IqeB_ToolsDisp_BoxWidth * 0.5 * 1.1);

    if( radius < radius2) {

      radius = radius2;
    }
  }

  camera.distanceCenter = radius * 2.5;
  camera.distanceMin    = radius * 0.1;
  camera.distanceMax    = radius * 10;
  camera.distance1V     = camera.distanceCenter;
  camera.distance2V     = camera.distanceCenter;

  // Try to snap gridstep to the radius

  gridsize = radius * 1.01;

  gridstep = -1.0;   // flag not set

  for( radius2 = 10000.0; radius2 >= 0.01; radius2 *= 0.1) {  // Decrement in power of 2

    if( radius >= radius2) {     // snap to this

       if( radius >= radius2 * 5.0) {

         gridstep = radius2 * 0.5;
       } else if( radius >= radius2 * 2.5) {

         gridstep = radius2 * 0.25;
       } else {

         gridstep = radius2 * 0.1;
       }

       break;
    }
  }

  if( gridstep < 0.0001) {  // not set

    gridstep = radius * 0.1;
  }

  if( gridstep >= 10.0) {

    CoordAfterpointDigits = 0;

  } else if( gridstep >= 1.0) {

    CoordAfterpointDigits = 1;

  } else if( gridstep >= 0.1) {

    CoordAfterpointDigits = 2;

  } else if( gridstep >= 0.01) {

    CoordAfterpointDigits = 3;

  } else {

    CoordAfterpointDigits = 4;
  }

  vec_set( camera.DispShift1V, 0, 0, 0);
  vec_set( camera.DispShift2V, 0, 0, 0);
}

/************************************************************************************
 * IqeB_DispPrepareNewModel
 */

void IqeB_DispPrepareNewModel( char *pFilename, IQE_model *pLoadedModel)
{
  int i;
  char *p;
  IQE_model *pModel;
  char ModelSelName[ MAX_FILENAME_LEN];     // file name
  char ModelSelFullName[ MAX_FILENAME_LEN]; // path and file name

  // clean things

  // unload current draw model and it's textures
  IqeB_ModelFreeData( &pDrawModel, IQE_MODEL_FREE_ALL);

  pModel = NULL;
  ModelSelName[ 0] = '\0';
  ModelSelFullName[ 0] = '\0';

  // try to load file

  if( pFilename != NULL) {

		IqeB_GUI_SetWindowTitle( pFilename);

		if( pLoadedModel != NULL) {   // have preloaded model first

      IqeB_ModelCopy( &pLoadedModel, &pModel, IQE_MODEL_COPY_NEW_ALL);  // Get copy

		} else {                      // NO preloaded model

      pModel = IqeB_ImportFile( pFilename);  // try to load from file
		}

		if( pModel != NULL) {        // load was OK

      IqeB_DispViewAdaptFromModel( pModel);

		  if( pModel->mesh->part_count == 0 && pModel->skel->joint_count > 0) {

        IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doskeleton, 1); // Update variable and GUI
		  }

      // get pointer to file name

	    p = strrchr( pFilename, '/');
	    if (!p) p = strrchr( pFilename, '\\');
	    if (!p) {
        p = pFilename;   // point to begin of file name
      } else {
        p += 1;          // point to begin of file name
      }

      // ...

      strcpy( ModelSelName, p);                // file name
      strcpy( ModelSelFullName, pFilename);    // path + file name
		}
  } else {

		IqeB_GUI_SetWindowTitle( (char *)WIN_DEFAULT_TITLE);
  }

  // prepare draw model data

  DrawModelSelName[ 0] = '\0';
  DrawModelSelFullName[ 0] = '\0';

  // model to draw

  pDrawModel = pModel;

  // Reset some display variables

  DrawModelCurframe = 0;
  pDrawModelCurAnim  = NULL;
  IqeB_DispStyle_MeshPartSelected = -1;
  IqeB_DispStyle_JointSelected = -1;
  IqeB_DispStyle_JointMouseNearby = -1;

  // for draw model in use, load materials

  if( pDrawModel != NULL) {

	  for( i = 0; i < pDrawModel->mesh->part_count; i++) {

      IqeB_TextureLoadMaterial( pDrawModel->mesh->part + i);  // load texture
	  }

    if( pDrawModel != NULL && pDrawModel->anim_count > 0) {

      pDrawModelCurAnim  = pDrawModel->anim_data[ 0];
    }

    strncpy( DrawModelSelName, ModelSelName, sizeof( DrawModelSelName) - 1);
    strncpy( DrawModelSelFullName, ModelSelFullName, sizeof( DrawModelSelFullName) - 1);
  }

  // invalidate any message to draw

  strcpy( DrawMessageText1, "");

  // Update GUI animation things
  IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_MODEL_CHANGE);
}

/************************************************************************************
 * IqeB_DispPrepareContainerModel
 */

void IqeB_DispPrepareContainerModel( T_ModelContainer *pModelContainer)
{

  IqeB_DispPrepareNewModel( pModelContainer->ModelFullName, pModelContainer->pModel);
}

/************************************************************************************
 * IqeB_DispMessage
 *
 * Messge to be drawn in OpenGL window at bottom left corner
 */

void IqeB_DispMessage( float r, float g, float b, char *pText1,  char *pText2, char *pText3)
{

  DrawMessageColR = r;
  DrawMessageColG = g;
  DrawMessageColB = b;

  DrawMessageText1[ 0] = '\0';  // reset to empty
  DrawMessageText2[ 0] = '\0';
  DrawMessageText3[ 0] = '\0';

  if( pText1 != NULL) strcpy( DrawMessageText1, pText1);
  if( pText2 != NULL) strcpy( DrawMessageText2, pText2);
  if( pText3 != NULL) strcpy( DrawMessageText3, pText3);
}

/************************************************************************************
 * IqeB_DispTestRedraw
 *
 */

int IqeB_DispTestRedraw( void)
{
  unsigned int TimeTemp;
  static unsigned int TimeLastCalled = 0;

  TimeTemp = IqeB_Disp_GetTickCount(); // Get current time

  if( IqeB_Disp_LastDrawTime == 0) {   // Wand redraw

    if( TimeLastCalled == TimeTemp) {  // Not many time gone

      MySleep( 2);                     // Sleep a bit
      TimeTemp = IqeB_Disp_GetTickCount();  // Get current time
    }

    TimeLastCalled = TimeTemp;         // Remeber last time called

    return( 1);                        // Want to redraw
  }

  if( TimeTemp - IqeB_Disp_LastDrawTime >= 30) {  // 30 ms gone since last call

    TimeLastCalled = TimeTemp;         // Remeber last time called

    return( 1);                        // Want to redraw
  }

  TimeLastCalled = TimeTemp;           // Remeber last time called

  MySleep( 10);                        // Sleep some time

  return( 0);                          // No redraw
}

/************************************************************************************
 * IqeB_DispModelGetInfo
 *
 * Get some info about the current displayed model.
 * NOTE: * Info data is from IqeB_Measuremodel() called before.
 *       * Set the an argument point to NULL if you don't need a specific info.
 *
 * return:     0: OK
 *            -1: error, no display model
 */

int IqeB_DispModelGetInfo( vec3 ObjSize, vec3 ObjCenter, float *pGridStep, int *pCoordAfterpointDigits)
{

  if( pDrawModel == NULL) {   // no model loaded

    return( -1);
  }

  if( ObjSize != NULL) {   // have pointer set

    vec_copy( ObjSize, camera.ObjSize);
  }

  if( ObjCenter != NULL) {   // have pointer set

    vec_copy( ObjCenter, camera.ObjCenter);
  }

  if( pGridStep != NULL) {   // have pointer set

    *pGridStep = gridstep;
  }

  if( pCoordAfterpointDigits != NULL) {   // have pointer set

    *pCoordAfterpointDigits = CoordAfterpointDigits;
  }

  return( 0);   // return OK
}

/**************************** End Of File *****************************/


