/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/Color.h>
#include <World/ModelLoader.h>
#include <World/ModelResource.h>
#include <Math/Geometry/Vector.h>
#include <Math/Geometry/Matrix.h>

#include <System/IOHelper.h> // todo: remove this

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

// ..

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef struct ModelLoadRegisterType
{
	Vector Callbacks;

} * ModelLoadRegisterType;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
///	Retreive the model register manager
////////////////////////////////////////////////////////////
static ModelLoadRegisterType ModelLoadRegister()
{
	static struct ModelLoadRegisterType Register = {
		ExprInit(Callbacks, NULL)
	};

	// Initialize register
	if (Register.Callbacks == NULL)
	{
		Register.Callbacks = VectorNew(sizeof(ModelLoadCallbackFunc));
	}

	return &Register;
}

////////////////////////////////////////////////////////////
/// Register on load callback
////////////////////////////////////////////////////////////
ModelLoadBuild ModelLoadRegisterCallback(ModelLoadCallbackFunc Callback)
{
	if (Callback)
	{
		static struct ModelLoadBuildType Builder = {
			TypeInit(Build, &ModelLoadRegisterCallback)
		};

		if (ModelLoadRegister()->Callbacks)
		{
			// Register callback
			VectorPushBack(ModelLoadRegister()->Callbacks, &Callback);
		}

		return &Builder;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Unregister on load callback
////////////////////////////////////////////////////////////
void ModelLoadUnregisterCallback(ModelLoadCallbackFunc Callback)
{
	uinteger i;

	// Iterate through each callback
	for (i = 0; i < VectorSize(ModelLoadRegister()->Callbacks) && Callback != NULL; ++i)
	{
		ModelLoadCallbackFunc * Iterator = (ModelLoadCallbackFunc *)VectorAt(ModelLoadRegister()->Callbacks, i);

		if (Callback == *Iterator)
		{
			// Remove current callback
			VectorErase(ModelLoadRegister()->Callbacks, i);

			// Break
			Callback = NULL;
		}
	}

	// Clear callback list if empty
	if (VectorEmpty(ModelLoadRegister()->Callbacks))
	{
		VectorDestroy(ModelLoadRegister()->Callbacks);
	}
}

////////////////////////////////////////////////////////////
///	Copy resource impl into generic resource
////////////////////////////////////////////////////////////
void ModelLoadCopyResource(struct ModelResourceType * Resource, struct ModelResourceImplType * ResourceImpl)
{
	if (Resource && ResourceImpl)
	{
		// Copy mesh list
		if (ResourceImpl->Meshes)
		{
			uinteger i;

			for (i = 0; i < ResourceImpl->Header->MeshSize; ++i)
			{
				struct ModelMeshType * Mesh;

				// Push new mesh
				VectorPushBackEmpty(Resource->Meshes);

				// Get mesh reference
				Mesh = (struct ModelMeshType *)VectorBack(Resource->Meshes);

				// Copy resource impl mesh
				Mesh->IndexSize = ResourceImpl->Meshes[i].TriangleSize;
				Mesh->IndexOffset = ResourceImpl->Meshes[i].TriangleOffset;
				Mesh->VertexSize = ResourceImpl->Meshes[i].VertexSize;
				Mesh->VertexOffset = ResourceImpl->Meshes[i].VertexOffset;

				// Material is directly mapped to resource textures array
				Mesh->Material = i;
			}
		}

		// Copy triangle list
		if (ResourceImpl->Triangles)
		{
			uinteger i;

			for (i = 0; i < ResourceImpl->Header->TriangleSize; ++i)
			{
				struct Vector3ui * Triangle;

				// Push new triangle
				VectorPushBackEmpty(Resource->Triangles);

				// Get triangle reference
				Triangle = (struct Vector3ui *)VectorBack(Resource->Triangles);

				// Copy resource impl triangle
				Triangle->x = ResourceImpl->Triangles[i].Vertex[0];
				Triangle->y = ResourceImpl->Triangles[i].Vertex[1];
				Triangle->z = ResourceImpl->Triangles[i].Vertex[2];
			}
		}

		// Copy position vertex array
		if (ResourceImpl->Vertex.Positions)
		{
			uinteger i;

			for (i = 0; i < ResourceImpl->Header->VertexSize; ++i)
			{
				uinteger PositionIndex = i * 3;
				struct Vector3f * Position;

				// Push new position
				VectorPushBackEmpty(Resource->Positions);

				// Get position reference
				Position = (struct Vector3f *)VectorBack(Resource->Positions);

				// Copy resource impl position
				Position->x = ResourceImpl->Vertex.Positions[PositionIndex + 0];
				Position->y = ResourceImpl->Vertex.Positions[PositionIndex + 1];
				Position->z = ResourceImpl->Vertex.Positions[PositionIndex + 2];
			}
		}

		// Copy normal vertex array
		if (ResourceImpl->Vertex.Normals)
		{
			uinteger i;

			for (i = 0; i < ResourceImpl->Header->VertexSize; ++i)
			{
				uinteger NormalIndex = i * 3;
				struct Vector3f * Normal;

				// Push new normal
				VectorPushBackEmpty(Resource->Normals);

				// Get normal reference
				Normal = (struct Vector3f *)VectorBack(Resource->Normals);

				// Copy resource impl normal
				Normal->x = ResourceImpl->Vertex.Normals[NormalIndex + 0];
				Normal->y = ResourceImpl->Vertex.Normals[NormalIndex + 1];
				Normal->z = ResourceImpl->Vertex.Normals[NormalIndex + 2];
			}
		}

		// Copy tangent vertex array
		if (ResourceImpl->Vertex.Tangents)
		{
			uinteger i;

			for (i = 0; i < ResourceImpl->Header->VertexSize; ++i)
			{
				uinteger TangentIndex = i * 4;
				struct Vector4f * Tangent;

				// Push new tangent
				VectorPushBackEmpty(Resource->Tangents);

				// Get tangent reference
				Tangent = (struct Vector4f *)VectorBack(Resource->Tangents);

				// Copy resource impl tangent
				Tangent->x = ResourceImpl->Vertex.Tangents[TangentIndex + 0];
				Tangent->y = ResourceImpl->Vertex.Tangents[TangentIndex + 1];
				Tangent->z = ResourceImpl->Vertex.Tangents[TangentIndex + 2];
				Tangent->w = ResourceImpl->Vertex.Tangents[TangentIndex + 3];
			}
		}

		// Copy texture coordinates vertex array
		if (ResourceImpl->Vertex.TextureCoords)
		{
			uinteger i;

			for (i = 0; i < ResourceImpl->Header->VertexSize; ++i)
			{
				uinteger TextureCoordIndex = i * 2;
				struct Vector2f * TextureCoord;

				// Push new texture coordinate
				VectorPushBackEmpty(Resource->TextureCoords);

				// Get texture coordinate reference
				TextureCoord = (struct Vector2f *)VectorBack(Resource->TextureCoords);

				// Copy resource impl texture coordinate
				TextureCoord->x = ResourceImpl->Vertex.TextureCoords[TextureCoordIndex + 0];
				TextureCoord->y = ResourceImpl->Vertex.TextureCoords[TextureCoordIndex + 1];
			}
		}

		// Copy blend indexes vertex array
		if (ResourceImpl->Vertex.BlendIndexes)
		{
			uinteger i;

			for (i = 0; i < ResourceImpl->Header->VertexSize; ++i)
			{
				uinteger BlendIndex = i * 4;
				struct Vector4ub * Blend;

				// Push new blend index
				VectorPushBackEmpty(Resource->BlendIndexes);

				// Get blend index reference
				Blend = (struct Vector4ub *)VectorBack(Resource->BlendIndexes);

				// Copy resource impl blend index
				Blend->x = ResourceImpl->Vertex.BlendIndexes[BlendIndex + 0];
				Blend->y = ResourceImpl->Vertex.BlendIndexes[BlendIndex + 1];
				Blend->z = ResourceImpl->Vertex.BlendIndexes[BlendIndex + 2];
				Blend->w = ResourceImpl->Vertex.BlendIndexes[BlendIndex + 3];
			}
		}

		// Copy blend weights vertex array
		if (ResourceImpl->Vertex.BlendWeights)
		{
			uinteger i;

			for (i = 0; i < ResourceImpl->Header->VertexSize; ++i)
			{
				uinteger BlendWeightIndex = i * 4;
				struct Vector4ub * BlendWeight;

				// Push new blend weight
				VectorPushBackEmpty(Resource->BlendWeights);

				// Get blend weight reference
				BlendWeight = (struct Vector4ub *)VectorBack(Resource->BlendWeights);

				// Copy resource impl blend weight
				BlendWeight->x = ResourceImpl->Vertex.BlendWeights[BlendWeightIndex + 0];
				BlendWeight->y = ResourceImpl->Vertex.BlendWeights[BlendWeightIndex + 1];
				BlendWeight->z = ResourceImpl->Vertex.BlendWeights[BlendWeightIndex + 2];
				BlendWeight->w = ResourceImpl->Vertex.BlendWeights[BlendWeightIndex + 3];
			}
		}

		// Copy colors vertex array
		if (ResourceImpl->Vertex.Colors)
		{
			uinteger i;

			for (i = 0; i < ResourceImpl->Header->VertexSize; ++i)
			{
				uinteger ColorIndex = i * 4;
				struct Color4ub * Color;

				// Push new color
				VectorPushBackEmpty(Resource->Colors);

				// Get color reference
				Color = (struct Color4ub *)VectorBack(Resource->Colors);

				// Copy resource impl color
				Color->r = ResourceImpl->Vertex.Colors[ColorIndex + 0];
				Color->g = ResourceImpl->Vertex.Colors[ColorIndex + 1];
				Color->b = ResourceImpl->Vertex.Colors[ColorIndex + 2];
				Color->a = ResourceImpl->Vertex.Colors[ColorIndex + 3];
			}
		}

		// Copy material ids
		if (ResourceImpl->Text)
		{
			uinteger i;

			for (i = 0; i < ResourceImpl->Header->MeshSize; ++i)
			{
				uint8 * MaterialImplName = &ResourceImpl->Text[ResourceImpl->Meshes[i].Material];
				uint8 ** Texture;

				// Push new material id
				VectorPushBackEmpty(Resource->Textures);

				// Get material id reference
				Texture = (uint8 **)VectorBack(Resource->Textures);

				// Allocate the string
				*Texture = (uint8 *)MemoryAllocate((strlen(MaterialImplName) + 1) * sizeof(uint8));

				// Copy name
				strcpy(*Texture, MaterialImplName);
			}
		}

		// Calculate base frame matrix
		if (ResourceImpl->Joints)
		{
			uinteger i;

			for (i = 0; i < ResourceImpl->Header->JointSize; ++i)
			{
				struct ModelJointType * Joint;
				struct Matrix34f * BaseFrame;
				struct Matrix34f * InverseBaseFrame;

				// Push new joint
				VectorPushBackEmpty(Resource->Joints);

				// Get joint reference
				Joint = (struct ModelJointType *)VectorBack(Resource->Joints);

				// Push new base frame

				// Push new inverted base frame
				VectorPushBackEmpty(Resource->InverseBaseFrame);
				VectorPushBackEmpty(Resource->BaseFrame);

				// Get base frame matrix reference
				BaseFrame = (struct Matrix34f *)VectorBack(Resource->BaseFrame);

				// Get inverted base frame matrix reference
				InverseBaseFrame = (struct Matrix34f *)VectorBack(Resource->InverseBaseFrame);

				// Get joint parent reference
				Joint->Parent = ResourceImpl->Joints[i].Parent;

				// Get joint rotation
				QuaternionSet(&Joint->Rotation,
					ResourceImpl->Joints[i].Rotate[0],
					ResourceImpl->Joints[i].Rotate[1],
					ResourceImpl->Joints[i].Rotate[2],
					ResourceImpl->Joints[i].Rotate[3]);

				// Normalize rotation
				QuaternionNormalize(&Joint->Rotation);

				// Get joint translation
				Vector3fSet(&Joint->Position,
					ResourceImpl->Joints[i].Translate[0],
					ResourceImpl->Joints[i].Translate[1],
					ResourceImpl->Joints[i].Translate[2]);

				// Get joint scaling
				Vector3fSet(&Joint->Scale,
					ResourceImpl->Joints[i].Scale[0],
					ResourceImpl->Joints[i].Scale[1],
					ResourceImpl->Joints[i].Scale[2]);

				// Set base frame
				Matrix34fFromQuaternionRotation(BaseFrame, &Joint->Rotation, &Joint->Position, &Joint->Scale);

				// Calculate inverse base frame
				Matrix34fInvert(InverseBaseFrame, BaseFrame);

				if (Joint->Parent >= 0)
				{
					struct Matrix34f * ParentBaseFrame = (struct Matrix34f *)VectorAt(Resource->BaseFrame, Joint->Parent);
					struct Matrix34f * ParentInverseBaseFrame = (struct Matrix34f *)VectorAt(Resource->InverseBaseFrame, Joint->Parent);
					struct Matrix34f Matrix;

					// Multiply base frame by parent
					Matrix34fMultiply(&Matrix, ParentBaseFrame, BaseFrame);

					// Overwrite base frame
					Matrix34fCopy(BaseFrame, &Matrix);

					// Multiply inverted base frame by parent
					Matrix34fMultiply(&Matrix, InverseBaseFrame, ParentInverseBaseFrame);

					// Overwrite inverse base frame
					Matrix34fCopy(InverseBaseFrame, &Matrix);
				}
			}
		}

		// Calculate frame list (skeleton)
		if (ResourceImpl->Frames)
		{
			uinteger FrameIndex = 0;

			uinteger i, j, k;

			// Reserve space for frame list
			Resource->FrameSize = ResourceImpl->Header->FrameSize;
			Resource->PoseSize = ResourceImpl->Header->PoseSize;

			VectorResize(Resource->Frames, Resource->FrameSize * Resource->PoseSize);

			for (i = 0; i < Resource->FrameSize; ++i)
			{
				for (j = 0; j < Resource->PoseSize; ++j)
				{
					struct ModelResourcePoseType * Pose = &ResourceImpl->Poses[j];
					struct Matrix34f * Frame = (struct Matrix34f *)VectorAt(Resource->Frames, i * Resource->PoseSize + j);

					struct Vector3f Position, Scale;
					struct Quaternion Rotation;

					float PoseChannel[MODEL_RESOURCE_POSE_CHANNEL_SIZE];

					struct Matrix34f PoseMatrix;

					// Calculate channels
					for (k = 0; k < MODEL_RESOURCE_POSE_CHANNEL_SIZE; ++k)
					{
						PoseChannel[k] = Pose->ChannelOffset[k];

						if (Pose->Mask & (0x01 << k))
						{
							PoseChannel[k] += ResourceImpl->Frames[FrameIndex++] * Pose->ChannelScale[k];
						}
					}

					// Set position vector
					Vector3fSet(&Position, PoseChannel[0], PoseChannel[1], PoseChannel[2]);

					// Set rotation quaternion
					QuaternionSet(&Rotation, PoseChannel[3], PoseChannel[4], PoseChannel[5], PoseChannel[6]);

					// Normalize rotation
					QuaternionNormalize(&Rotation);

					// Set scale vector
					Vector3fSet(&Scale, PoseChannel[7], PoseChannel[8], PoseChannel[9]);

					// Concatenate each pose with inverse base pose (avoid doing this at animation time)
					// Joint parent needs to be pre-concatenated with its parent's base pose
					Matrix34fFromQuaternionRotation(&PoseMatrix, &Rotation, &Position, &Scale);

					if (Pose->Parent >= 0)
					{
						struct Matrix34f Matrix;

						struct Matrix34f * BaseFrame = (struct Matrix34f *)VectorAt(Resource->BaseFrame, Pose->Parent);

						struct Matrix34f * InverseBaseFrame = (struct Matrix34f *)VectorAt(Resource->InverseBaseFrame, j);

						Matrix34fMultiply(&Matrix, BaseFrame, &PoseMatrix);

						Matrix34fMultiply(Frame, &Matrix, InverseBaseFrame);
					}
					else
					{
						struct Matrix34f * InverseBaseFrame = (struct Matrix34f *)VectorAt(Resource->InverseBaseFrame, j);

						Matrix34fMultiply(Frame, &PoseMatrix, InverseBaseFrame);
					}

				}
			}
		}
	}
}

////////////////////////////////////////////////////////////
///	Create a generic model resource
////////////////////////////////////////////////////////////
struct ModelResourceType * ModelLoadCreateResource(struct ModelResourceImplType * ModelResourceImpl)
{
	if (ModelResourceImpl)
	{
		struct ModelResourceType * Resource = (struct ModelResourceType *)MemoryAllocate(sizeof(struct ModelResourceType));

		if (Resource)
		{
			// Create vectors
			Resource->Meshes = VectorNew(sizeof(struct ModelMeshType));
			Resource->Triangles = VectorNew(sizeof(struct Vector3ui));
			Resource->Positions = VectorNew(sizeof(struct Vector3f));
			Resource->Normals = VectorNew(sizeof(struct Vector3f));
			Resource->Tangents = VectorNew(sizeof(struct Vector4f));
			Resource->TextureCoords = VectorNew(sizeof(struct Vector2f));
			Resource->BlendIndexes = VectorNew(sizeof(struct Vector4ub));
			Resource->BlendWeights = VectorNew(sizeof(struct Vector4ub));
			Resource->Colors = VectorNew(sizeof(struct Color4ub));
			Resource->Textures = VectorNew(sizeof(uint8 *));
			Resource->Joints = VectorNew(sizeof(struct ModelJointType));
			Resource->BaseFrame = VectorNew(sizeof(struct Matrix34f));
			Resource->InverseBaseFrame = VectorNew(sizeof(struct Matrix34f));
			Resource->Frames = VectorNew(sizeof(struct Matrix34f));
			Resource->FrameSize = 0;
			Resource->PoseSize = 0;

			// Copy resource impl data to resource handle
			ModelLoadCopyResource(Resource, ModelResourceImpl);

			return Resource;
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
///	Destroy a generic model resource
////////////////////////////////////////////////////////////
void ModelLoadDestroyResource(struct ModelResourceType * Resource)
{
	if (Resource)
	{
		uinteger i;

		for (i = 0; i < VectorSize(Resource->Textures); ++i)
		{
			uint8 * Texture = VectorAtT(Resource->Textures, i, uint8 *);

			MemoryDeallocate(Texture);
		}

		VectorDestroy(Resource->Meshes);
		VectorDestroy(Resource->Triangles);
		VectorDestroy(Resource->Positions);
		VectorDestroy(Resource->Normals);
		VectorDestroy(Resource->Tangents);
		VectorDestroy(Resource->TextureCoords);
		VectorDestroy(Resource->BlendIndexes);
		VectorDestroy(Resource->BlendWeights);
		VectorDestroy(Resource->Textures);
		VectorDestroy(Resource->Joints);
		VectorDestroy(Resource->BaseFrame);
		VectorDestroy(Resource->InverseBaseFrame);
		VectorDestroy(Resource->Frames);
	}
}

////////////////////////////////////////////////////////////
///	Execute all callbacks of the register manager
////////////////////////////////////////////////////////////
void ModelLoadRegisterExecute(const char * Name, struct ModelResourceImplType * ModelResource, EntityIdType EntityId)
{
	if (ModelResource)
	{
		uinteger i;

		// Iterate through each callback
		for (i = 0; i < VectorSize(ModelLoadRegister()->Callbacks); ++i)
		{
			struct ModelLoadCallbackType OnLoadCallback;

			// Retreive the function callback
			OnLoadCallback.Func = VectorAtT(ModelLoadRegister()->Callbacks, i, ModelLoadCallbackFunc);

			// Set entity id
			OnLoadCallback.EntityId = EntityId;

			// Set resource
			OnLoadCallback.Resource = ModelLoadCreateResource(ModelResource);

			// On successful resource creation
			if (OnLoadCallback.Resource)
			{
				// Set resource name
				OnLoadCallback.Resource->Name = Name;

				// Set animated flag
				OnLoadCallback.Resource->Animated = (bool)(ModelResource->Header->AnimSize > 0);

				// Execute the callback
				if (OnLoadCallback.Func(&OnLoadCallback) == false)
				{
					// Error
					// ...
				}
			}

			// Clear resource
			ModelLoadDestroyResource(OnLoadCallback.Resource);
		}
	}
}

////////////////////////////////////////////////////////////
/// Load model from file
////////////////////////////////////////////////////////////
bool ModelLoadFromFile(const char * Path, const char * Name, EntityIdType EntityId)
{
	if (Name && EntityId != ENTITY_INDEX_INVALID)
	{
		FILE * File;

		char FileName[0xFF];

		sprintf(FileName, "%s%s.iqm", Path, Name);

		if (fopen(&File, FileName, "rb"))
		{
			struct ModelResourceImplType ModelResource;

			// Parse model
			if (ModelResourceRead(&ModelResource, File))
			{
				// Load meshes
				if (ModelResource.Header->MeshSize > 0 && !ModelResourceLoadMeshes(&ModelResource))
				{
					return false;
				}

				// Load animations
				if (ModelResource.Header->AnimSize > 0 && !ModelResourceLoadAnims(&ModelResource))
				{
					return false;
				}

				// Load comments
				if (ModelResource.Header->CommentSize > 0)
				{
					// TODO
				}

				// Load extensions
				if (ModelResource.Header->ExtensionSize > 0)
				{
					// TODO
				}

				// Execute callbacks
				ModelLoadRegisterExecute(Name, &ModelResource, EntityId);

				// Clear model resource
				ModelResourceDestroy(&ModelResource);

				return true;
			}
			else
			{
				// Close file handle
				fclose(File);
			}
		}
	}

	return false;
}
