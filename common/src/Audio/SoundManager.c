/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Audio/SoundManager.h>
#include <Audio/OpenALHelper.h>
#include <System/IOHelper.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct Sound	SoundData[SOUND_BUFFER_SIZE] = {{0}};	///< Sound buffer array
#if COMPILE_TYPE == DEBUG_MODE
	float 			SoundGeneralVolume = 0;			///< Sound volume (0 to 100)
#else
	float 			SoundGeneralVolume = 100;			///< Sound volume (0 to 100)
#endif

////////////////////////////////////////////////////////////
/// Destroy all sound data and buffer
////////////////////////////////////////////////////////////
void SoundBufferDestroy()
{
	if (AudioIsAvailable())
	{
		uinteger i;

		for (i = 0; i < SOUND_BUFFER_SIZE; i++)
		{
			if (SoundData[i].Buffer != 0)
			{
				// Stop sound
				SoundStop(&SoundData[i]);

				// Delete source
				ALCheck(alDeleteSources(1, &SoundData[i].Source));

				// Delete buffer
				ALCheck(alDeleteBuffers(1, &SoundData[i].Buffer));

				// Free data
				MemoryDeallocate(SoundData[i].Samples);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Play sound from file into buffer
////////////////////////////////////////////////////////////
void SoundBufferPlay(char * File, bool Looping, float x, float y, float z)
{
	if (AudioIsAvailable() && File)
	{
		struct Sound * SoundPtr = NULL;

		SoundBufferLoad(&SoundPtr, File);

		if (SoundPtr && SoundPtr->Buffer != 0)
		{
			if (SoundStatus(SoundPtr) == Stopped)
			{
				// Set volume
				SoundVolume(SoundPtr, SoundGeneralVolume);

				// Set position
				SoundPosition(SoundPtr, x, y, z);

				// Set looping
				SoundLoop(SoundPtr, Looping);

				// Play sound
				SoundPlay(SoundPtr);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Load sound into buffer
////////////////////////////////////////////////////////////
void SoundBufferLoad(struct Sound ** SoundPtr, char * File)
{
	if (AudioIsAvailable() && SoundPtr && File)
	{
		float  MinorTime = 0; // Minor time means the most old file
		uint32 Dimension = 0; // Dimension found
		uinteger i;

		// Search if the sound is loaded
		for (i = 0; i < SOUND_BUFFER_SIZE; i++)
		{
			if (SoundData[i].File != NULL)
			{
				if (strcmp(SoundData[i].File, File) == 0)
				{
					// The sound is loaded.. just play it
					*SoundPtr = &SoundData[i];
					return;
				}
			}
		}

		// Search deallocate sound
		for (i = 0; i < SOUND_BUFFER_SIZE; i++)
		{
			if (SoundData[i].Buffer == 0)
			{
				bool Loaded = false;

				// Load sound
				if (SOUND_FROM_MEMORY)
				{
					char * BufferData;
					uint32 BufferSize;

					// Load buffer from pack
					// if (!PackLoadData(File, BufferData, &BufferSize))
					// {
					//	return;
					// }

					// Load buffer data from memory
					if (SoundFileOpenReadFromMemory(&SoundData[i], BufferData, BufferSize))
					{
						// Destroy data
						MemoryDeallocate(BufferData);
						Loaded = true;
					}
				}
				else
				{
					if (SoundFileOpenReadFromFile(&SoundData[i], File))
					{
						Loaded = true;
					}
				}

				if (Loaded == true)
				{
					strcpy(SoundData[i].File, File);
					*SoundPtr = &SoundData[i];
					return;
				}
			}
		}

		// Search last used
		for (i = 0; i < SOUND_BUFFER_SIZE; i++)
		{
			if (SoundStatus(&SoundData[i]) == Stopped)
			{
				if (SoundData[i].Time < MinorTime)
				{
					MinorTime = SoundData[i].Time;
					Dimension = i;
				}
			}
		}

		if (MinorTime != 0.0f)
		{
			if (SoundData[Dimension].Buffer != 0)
			{
				// Remove data from the previous sound
				ALCheck(alDeleteBuffers(1, &SoundData[Dimension].Buffer));

				// Delete source
				ALCheck(alDeleteSources(1, &SoundData[Dimension].Source));
			}

			// Return last used
			*SoundPtr = &SoundData[Dimension];
		}
		else
		{
			// There isn't sounds available
			*SoundPtr = NULL;
		}
	}
}

////////////////////////////////////////////////////////////
/// Stop all sounds from buffer
////////////////////////////////////////////////////////////
void SoundBufferStop()
{
	if (AudioIsAvailable())
	{
		uinteger i;

		for (i = 0; i < SOUND_BUFFER_SIZE; i++)
		{
			if (SoundData[i].Buffer != 0)
			{
				// Stop sound
				SoundStop(&SoundData[i]);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Stop a sound by its file name
////////////////////////////////////////////////////////////
void SoundBufferStopByFile(char * File)
{
	if (AudioIsAvailable() && File)
	{
		uinteger i;

		for (i = 0; i < SOUND_BUFFER_SIZE; i++)
		{
			// todo: make a map man
			if (SoundData[i].Buffer != 0 && (strcmp(SoundData[i].File, File) == 0) && SoundStatus(&SoundData[i]) != Stopped)
			{
				// Stop sound
				SoundStop(&SoundData[i]);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Update sounds volume
////////////////////////////////////////////////////////////
void SoundBufferVolume(float Volume)
{
	if (AudioIsAvailable())
	{
		uinteger i;

		for (i = 0; i < SOUND_BUFFER_SIZE; i++)
		{
			if (SoundData[i].Buffer != 0)
			{
				// Set volume sound
				SoundVolume(&SoundData[i], Volume);
			}
		}

		SoundGeneralVolume = Volume;
	}
}

////////////////////////////////////////////////////////////
/// Move the listener position
////////////////////////////////////////////////////////////
void SoundBufferMoveListener(float x, float y, float z)
{
	if (AudioIsAvailable())
	{

	}
}
