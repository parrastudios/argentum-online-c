/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

#ifndef CORE_CONSOLE_H
#define CORE_CONSOLE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/String/Parser.h>
#include <System/IOHelper.h>
#include <System/Thread.h>
#include <System/Mutex.h>
#include <Game/Application.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define COMMAND_LIST_PATH				"data/cmd.dat"
#define CONSOLE_BUFFER_SIZE				0xFF
#define CONSOLE_COMMAND_ARG_SIZE		0x0A
#define CONSOLE_COMMAND_SIZE			0x05
#define CONSOLE_ERROR					-1

#define CONSOLE_CMD_HELP				0x00
#define CONSOLE_CMD_TIME				0x01
#define CONSOLE_CMD_CREDITS				0x02
#define CONSOLE_CMD_EXIT				0x03
#define CONSOLE_CMD_TEST				0x04

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct CommandData	///< Command data struct
{
	char 	Data[CONSOLE_BUFFER_SIZE];
	int32	Index;
	char *	Arguments[CONSOLE_COMMAND_ARG_SIZE][CONSOLE_BUFFER_SIZE];
};

static struct CommandData CommandList[CONSOLE_COMMAND_SIZE];

////////////////////////////////////////////////////////////
// Initialize command console
////////////////////////////////////////////////////////////
bool ConsoleInitialize();

////////////////////////////////////////////////////////////
// Command console method
////////////////////////////////////////////////////////////
void ConsoleRoutine();

////////////////////////////////////////////////////////////
// Destroy command console
////////////////////////////////////////////////////////////
void ConsoleDestroy();

#endif // CORE_CONSOLE_H
