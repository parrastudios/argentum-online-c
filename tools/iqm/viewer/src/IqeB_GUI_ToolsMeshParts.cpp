/****************************************************************************

  IqeB_GUI_ToolsMeshPartsWin.cpp

  16.02.2015 RR: First editon of this file.


*****************************************************************************
*/

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Browser.H>

/************************************************************************************
 * Globals
 *
 */

/************************************************************************************
 * Statics
 */

static  Fl_Window  *pMyToolWin;
static int MyWinPosX = IQE_GUI_NO_WINPOS_X, MyWinPosY = IQE_GUI_NO_WINPOS_Y; // last window position

static  Fl_Browser *pMeshBrowser;
static  Fl_Input   *pInputName;
static  Fl_Input   *pInputMaterialStr;
static  Fl_Input   *pInputMaterialTags;

/************************************************************************************
 * IqeB_GUI_ToolsMeshPartUpdate
 *
 * Updates the GUI things for a single mesh part
 *
 * iPart: >= 0  Update for this mesh part element
 *         < 0  No Meshpart to update
 */

static void IqeB_GUI_ToolsMeshPartUpdate( int iPart)
{

  if( pDrawModel == NULL ||
      pDrawModel->mesh  == NULL ||
      pDrawModel->mesh->part  == NULL ||
      pDrawModel->mesh->part_count <= 0 ||
      iPart < 0 || iPart >= pDrawModel->mesh->part_count) {  // in range

    if( pInputName) {
      pInputName->value( "");
      IqeB_GUI_WidgetActivate( pInputName, false); // Set item activated/inactive
    }

    if( pInputMaterialStr) {
      pInputMaterialStr->value( "");
      IqeB_GUI_WidgetActivate( pInputMaterialStr, false); // Set item activated/inactive
    }

    if( pInputMaterialTags) {
      pInputMaterialTags->value( "");
      IqeB_GUI_WidgetActivate( pInputMaterialTags, false); // Set item activated/inactive
    }

    IqeB_DispStyle_MeshPartSelected = -1; // no mesh part selected

    return;
  }

  if( pInputName) {
    pInputName->value( pDrawModel->mesh->part[ iPart].pMeshName);
    IqeB_GUI_WidgetActivate( pInputName, true); // Set item activated/inactive
  }

  if( pInputMaterialStr) {
    pInputMaterialStr->value( pDrawModel->mesh->part[ iPart].pMaterialStr);
    IqeB_GUI_WidgetActivate( pInputMaterialStr, true); // Set item activated/inactive
  }

  if( pInputMaterialTags) {
    pInputMaterialTags->value( pDrawModel->mesh->part[ iPart].pMaterialTags);
    IqeB_GUI_WidgetActivate( pInputMaterialTags, true); // Set item activated/inactive
  }

  IqeB_DispStyle_MeshPartSelected = iPart; // set selected mesh part
}

/************************************************************************************
 * IqeB_GUI_ToolsMeshPartsUpdate
 *
 * Updates the mesh parts view
 */

static void IqeB_GUI_ToolsMeshPartsUpdate( int KeepSelection)
{
  int i;
  int LastSelection;
  char TempString[ MAX_FILENAME_LEN];

  if( pMeshBrowser == NULL) {   // security test, need this pointer

    return;
  }

  LastSelection = pMeshBrowser->value();  // get index of selected item

  pMeshBrowser->clear();                  // empty the list box

  // test for mesh parts to display

  if( pDrawModel == NULL ||
      pDrawModel->mesh  == NULL ||
      pDrawModel->mesh->part  == NULL ||
      pDrawModel->mesh->part_count <= 0) {

    IqeB_GUI_ToolsMeshPartUpdate( -1);   // deselect
    pMeshBrowser->redraw();     // Redraw it

    return;
  }

  // add mesh parts to tree

  for( i = 0; i < pDrawModel->mesh->part_count; i++) {

    sprintf( TempString, "%2d: %s / %s", i + 1, pDrawModel->mesh->part[ i].pMeshName, pDrawModel->mesh->part[ i].pMaterialStr);

    pMeshBrowser->add( TempString);
  }

  if( KeepSelection && LastSelection > 0 && LastSelection <= pMeshBrowser->size()) {

    pMeshBrowser->select( LastSelection);         // set to last selected

    pMeshBrowser->middleline( pDrawModel->mesh->part_count);

    IqeB_GUI_ToolsMeshPartUpdate( LastSelection - 1);

  } else if( pDrawModel->mesh->part_count > 0) {                    // have any

    pMeshBrowser->topline( 1);           // position to top line
    IqeB_GUI_ToolsMeshPartUpdate( -1);   // deselect
  }

  pMeshBrowser->redraw();     // Redraw it
}

/************************************************************************************
 * IqeB_GUI_UpdateCallback
 *
 * Is called from IqeB_GUI_UpdateWidgets() if something has changed
 * what needs an update (new model displayed, ...).
 */

static void IqeB_GUI_UpdateCallback( int UpdateFlags)
{

  if( pMyToolWin == NULL) {  // security test, has main window

    return;
  }

  IqeB_GUI_ToolsMeshPartsUpdate( false);
}

/************************************************************************************
 * IqeB_MeshBrowser_Callback
 */

static void IqeB_MeshBrowser_Callback( Fl_Widget *w, void *data)
{
  int this_item;

  if( pDrawModel == NULL ||
      pDrawModel->mesh  == NULL ||
      pDrawModel->mesh->part  == NULL ||
      pDrawModel->mesh->part_count <= 0) {

    IqeB_GUI_ToolsMeshPartUpdate( -1);   // deselect

    return;
  }

  this_item = pMeshBrowser->value();  // get index of selected item

  if( this_item > 0 && this_item <= pDrawModel->mesh->part_count) { // indexes is in range

    this_item -= 1;   // adapte index from 1 based to 0 based table access

    IqeB_GUI_ToolsMeshPartUpdate( this_item);

  } else {

    IqeB_GUI_ToolsMeshPartUpdate( -1);   // deselect
  }
}

/************************************************************************************
 * IqeB_GUI_Input_SetValue_Callback
 */

static void IqeB_GUI_Input_SetValue_Callback( Fl_Widget *w, void *data)
{
  Fl_Input *pInput;
  int iPart;
  char TempString[ MAX_FILENAME_LEN];

  pInput = (Fl_Input *)w;

  if( pInput == NULL) {   // security test

    return;
  }

  iPart = IqeB_DispStyle_MeshPartSelected;  // selected mesh part

  if( pDrawModel == NULL ||
      pDrawModel->mesh  == NULL ||
      pDrawModel->mesh->part  == NULL ||
      pDrawModel->mesh->part_count <= 0 ||
      iPart < 0 || iPart >= pDrawModel->mesh->part_count) {  // in range

    return;
  }

  // ...

  if( pInput == pInputName) {

    if( strcmp( pDrawModel->mesh->part[ iPart].pMeshName, pInput->value()) != 0) { // string is different

      // set changed string

      if( pDrawModel->mesh->part[ iPart].pMeshName) {

        free( pDrawModel->mesh->part[ iPart].pMeshName);
      }

      pDrawModel->mesh->part[ iPart].pMeshName = strdup( pInput->value());
    }

    // Also update browser

    sprintf( TempString, "%2d: %s / %s", iPart + 1, pDrawModel->mesh->part[ iPart].pMeshName, pDrawModel->mesh->part[ iPart].pMaterialStr);

    pMeshBrowser->text( iPart + 1, TempString);

    return;
  }

  // ...

  if( pInput == pInputMaterialStr) {

    if( strcmp( pDrawModel->mesh->part[ iPart].pMaterialStr, pInput->value()) != 0) { // string is different

      // set changed string

      if( pDrawModel->mesh->part[ iPart].pMaterialStr) {

        free( pDrawModel->mesh->part[ iPart].pMaterialStr);
      }

      pDrawModel->mesh->part[ iPart].pMaterialStr = strdup( pInput->value());

      // Also update browser

      sprintf( TempString, "%2d: %s / %s", iPart + 1, pDrawModel->mesh->part[ iPart].pMeshName, pDrawModel->mesh->part[ iPart].pMaterialStr);

      pMeshBrowser->text( iPart + 1, TempString);
    }

    IqeB_TextureLoadMaterial( pDrawModel->mesh->part + iPart);  // reload texture

    return;
  }

  // ...

  if( pInput == pInputMaterialTags) {

    if( strcmp( pDrawModel->mesh->part[ iPart].pMaterialTags, pInput->value()) != 0) { // string is different

      // set changed string

      if( pDrawModel->mesh->part[ iPart].pMaterialTags) {

        free( pDrawModel->mesh->part[ iPart].pMaterialTags);
      }

      pDrawModel->mesh->part[ iPart].pMaterialTags = strdup( pInput->value());
    }

    IqeB_TextureLoadMaterial( pDrawModel->mesh->part + iPart);  // reload texture

    return;
  }
}

/************************************************************************************
 * close_cb, close this window
 *
 * pValueArg is a pointer to the widget. Set this pointer to NULL on deletion.
 */

static void close_cb( Fl_Widget *w, void *pValueArg)
{

  IqeB_DispStyle_MeshPartSelected = -1; // no mesh part selected

  IqeB_GUI_CloseToolWindow( (void **)&pMyToolWin);
}

/************************************************************************************
 * Presets for this tools window
 *
 */

#ifdef use_again // 16.02.2014 RR: Keep this until we need preferences
 static T_GUI_PreferenceEntry MyPreferences[] =
 {

  // Box display

  { PREF_T_INT,   "BoxEnable",      "0", &IqeB_ToolsDisp_BoxEnable},
  { PREF_T_FLOAT, "BoxHeight1",  "32.0", &IqeB_ToolsDisp_BoxHeight1},
  { PREF_T_FLOAT, "BoxHeight2", "-24.0", &IqeB_ToolsDisp_BoxHeight2},
  { PREF_T_FLOAT, "BoxWidth",    "32.0", &IqeB_ToolsDisp_BoxWidth},
};
#endif

// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "ToolsMeshParts", NULL, 0,
                                              (void **)(&pMyToolWin), &MyWinPosX, &MyWinPosY);

/************************************************************************************
 * IqeB_GUI_ToolsMeshPartsWin
 *
 * Open dialog
 */

void IqeB_GUI_ToolsMeshPartsWin( int xLeft, int xRight, int yTop, int yBotton)
{

  //
  // winodow already created --> show it
  //

  if( pMyToolWin != NULL) {         // already have tool window

    // Show invisble window or bring visible window to foreground

    pMyToolWin->show();          // show it

    // is mesh part selected ? --> show selection

    if( pMeshBrowser != NULL && pMeshBrowser->value() > 0) {   // have a selected line

      IqeB_DispStyle_MeshPartSelected = pMeshBrowser->value() - 1; // set selected mesh part
    }

    return;
  }

  //
  // creation of window on first call
  //

  if( pMyToolWin == NULL) {  // no tool window until now

    int xPos, yPos;

    xPos = xRight;
    yPos = yTop;

    if( MyWinPosX != IQE_GUI_NO_WINPOS_X && MyWinPosY != IQE_GUI_NO_WINPOS_Y) { // have last window position

      xPos = MyWinPosX;
      yPos = MyWinPosY;
    }

    pMyToolWin = new Fl_Window( xPos, yPos, IQE_GUI_TOOLS_STD_WITDH, 400, "Mesh/Parts");

    if( pMyToolWin == NULL) {  // security test

      return;
    }

    IqeB_GUI_RegisterCallback( (void **)&pMyToolWin, IqeB_GUI_UpdateCallback, IQE_GUI_UPDATE_MODEL_CHANGE);
  }

  //
  //  GUI things
  //

  int x1, xx, y1, y, yy, yy2;

  x1  = 4;
  xx  = pMyToolWin->w() - 8;
  yy  = 32;

  y1 = 20;
  y = y1;

  // Parts browser

  yy2 = 160;

  pMeshBrowser = new Fl_Browser( x1, y1, xx, yy2, "Mesh parts");

  pMeshBrowser->align( FL_ALIGN_TOP_LEFT);     // align for label

  pMeshBrowser->type(FL_HOLD_BROWSER);        // use for single selection
  pMeshBrowser->callback( IqeB_MeshBrowser_Callback, NULL);

  y += yy2;

  // Name

  y += 4;

  pInputName = new Fl_Input( x1, y + 12, xx, yy - 12, "Mesh name");
  pInputName->align( FL_ALIGN_TOP_LEFT);     // align for label
  pInputName->labelsize( 10);
  pInputName->tooltip( "Name of mesh");
  pInputName->callback( IqeB_GUI_Input_SetValue_Callback);

  y += yy;

  // Material string

  pInputMaterialStr = new Fl_Input( x1, y + 12, xx, yy - 12, "Material string");
  pInputMaterialStr->align( FL_ALIGN_TOP_LEFT);     // align for label
  pInputMaterialStr->labelsize( 10);
  pInputMaterialStr->tooltip( "Material (texture)");
  pInputMaterialStr->callback( IqeB_GUI_Input_SetValue_Callback);

  y += yy;

  // Material tags

  pInputMaterialTags = new Fl_Input( x1, y + 12, xx, yy - 12, "Material tags");
  pInputMaterialTags->align( FL_ALIGN_TOP_LEFT);     // align for label
  pInputMaterialTags->labelsize( 10);
  pInputMaterialTags->tooltip( "Material tags.\nTags will precede the material string.\nThey are separated by a semicolon.");
  pInputMaterialTags->callback( IqeB_GUI_Input_SetValue_Callback);

  y += yy;

  // finish up

  pMyToolWin->size( pMyToolWin->w(), y + 4);  // final size of window

  pMyToolWin->end();
  pMyToolWin->set_non_modal();
  pMyToolWin->callback( close_cb, &pMyToolWin);

  IqeB_GUI_ToolsMeshPartsUpdate( false); // Update the GUI

  pMyToolWin->show();
}

/********************************** End Of File **********************************/
