/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_LIGHT_PRIMITIVES_H
#define	GRAPHICS_LIGHT_PRIMITIVES_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Vector.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct LightBase
{
    struct Vector3f Color;
    float AmbientIntensity;
    float DiffuseIntensity;
};

struct LightDirectional
{
	struct LightBase Base;
	struct Vector3f Direction;
};

struct LightPoint
{
	struct LightBase Base;
	struct Vector3f Position;

    struct
    {
        float Constant;
        float Linear;
        float Exp;
    } Attenuation;
};

struct LightSpot
{
	struct LightPoint Point;
	struct Vector3f Direction;
    float Cutoff;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

void LightBaseInitialize(struct LightBase * Light);

void LightDirectionalInitialize(struct LightDirectional * Light);

void LightPointInitialize(struct LightPoint * Light);

void LightSpotInitialize(struct LightSpot * Light);

#endif // GRAPHICS_LIGHT_PRIMITIVES_H
