/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataType/BinaryTree.h>

////////////////////////////////////////////////////////////
//	Self balanced binary tree
//	It is going to maintain almost equal depth on both sides
//				A
//			   / \
//		      B	  E		Diff = abs(LeftDepth - RightDepth)
//			 / \
//			C   F		Diff => 2
//		   /   / \
//		  D	  J   G
//		 /         \
//		L		    I
//	   /		   /
//	  M			  K
//	Since left is the larger side we will traverse left by
//	Diff / 2 element(s). Hence the new balanced tree is:
//						B
//					   / \
//					  C	  A
//					 /	   \
//				    D		E
////////////////////////////////////////////////////////////

void BinaryTreeBalance(struct BinaryTree * Tree)
{
	integer Diff;
	bool LeftLarge, RightLarge;

	if (!Tree || !Tree->Root)
	{
		return;
	}

	Diff = Tree->RightDepth - Tree->LeftDepth;

	if (Diff < 0)
	{
		Diff = -Diff;
	}

	if (Diff <= 1)
	{
		return;
	}

	if (Tree->RightDepth > Tree->LeftDepth)
	{
		RightLarge = true;
		LeftLarge = false;
	}
	else
	{
		LeftLarge = true;
		RightLarge = false;
	}

	Diff >>= 1;

	if (LeftLarge)
	{
		do
		{
			Tree->Root = Tree->Root->Left;
			Diff--;
		} while (Diff);
	}

	if (RightLarge)
	{
		do
		{
			Tree->Root->Parent = Tree->Root->Right;
			Tree->Root = Tree->Root->Right;
			Tree->Root->Parent = NULL;
			Diff--;
		} while (Diff);
	}
}

bool BinaryTreeCreate(struct BinaryTree * Tree)
{
	if (Tree)
	{
		Tree->Root = NULL;
		Tree->Size = 0;
		Tree->RightDepth = 0;
		Tree->RightElemCount = 0;
		Tree->RightLeafCount = 0;
		Tree->LeftDepth = 0;
		Tree->LeftElemCount = 0;
		Tree->LeftLeafCount = 0;

		return true;
	}

	return false;
}

void BinaryTreeDestroy(struct BinaryTree * Tree)
{

}

void BinaryTreeAdd(struct BinaryTree * Tree, struct BinaryTreeNode * Node, CompareCallback CompareFunc)
{
	struct BinaryTreeNode * Iterator, * Parent;
	uinteger InsertionDepth;

	if (Tree == NULL || Node == NULL || CompareFunc == NULL)
	{
		return;
	}

	InsertionDepth = 0;
	Iterator = Tree->Root;
	Parent = NULL;

	while (Iterator)
	{
		Parent = Iterator;
		//if (GET_SIZE(Node->size) > GET_SIZE(Iterator->size))
		if (CompareFunc(Node->Data, Iterator->Data) & OPERATOR_GREATER)
		{
			Iterator = Iterator->Right;
		}
		else
		{
			Iterator = Iterator->Left;
		}

		InsertionDepth++;
	}

	if (Parent)
	{
		// if (GET_SIZE(Node->size) > GET_SIZE(Parent->size))
		if (CompareFunc(Node->Data, Parent->Data) & OPERATOR_GREATER)
		{
			Parent->Right = Node;
		}
		else
		{
			Parent->Left = Node;
		}

		Node->Left = NULL;
		Node->Right = NULL;
		Node->Parent = Parent;

		Tree->Size++;
	}
	else
	{
		Tree->Root = Node;
		Node->Left = NULL;
		Node->Right = NULL;
		Node->Parent = NULL;
		Tree->Size = 1;
	}

	if (Tree->Size > 1)
	{
		// if (GET_SIZE(Node->size) > GET_SIZE(Tree->Root->size))
		if (CompareFunc(Node->Data, Tree->Root->Data) & OPERATOR_GREATER)
		{
			if (InsertionDepth > Tree->RightDepth)
			{
				Tree->RightDepth = InsertionDepth;
				Tree->RightLeafCount = 1;
			}
			else if (InsertionDepth == Tree->RightDepth)
			{
				Tree->RightLeafCount++;
			}
		}
		else
		{
			if (InsertionDepth > Tree->LeftDepth)
			{
				Tree->LeftDepth = InsertionDepth;
				Tree->LeftLeafCount = 1;
			}
			else if (InsertionDepth == Tree->LeftDepth)
			{
				Tree->LeftLeafCount++;
			}
		}
	}

	BinaryTreeBalance(Tree);
}

void BinaryTreeRemove(struct BinaryTree * Tree, struct BinaryTreeNode * Node, CompareCallback CompareFunc)
{
	struct BinaryTreeNode * Left, * Right, * Parent, * NewNode;
	bool LeafNode;

	if (Tree->Root == NULL || Tree->Size == 0 || Node == NULL || CompareFunc == NULL)
	{
		return;
	}

	// Remove the node and give priority to the right side
	Left	= Node->Left;
	Right	= Node->Right;
	Parent	= Node->Parent;
	NewNode	= NULL;

	if (!Node->Left && !Node->Right)
	{
		LeafNode = true;
	}
	else
	{
		LeafNode = false;
	}

	// Relocate left part (if possible)
	if (Right == NULL)
	{
		NewNode = Left;
	}
	else
	{
		NewNode	=	Right;
	}

	if (Left && Right)
	{
		struct BinaryTreeNode * Iterator, * Prev;

		Iterator = Right;
		Prev = NULL;

		while (Iterator)
		{
			Prev = Iterator;
			Iterator = Iterator->Left;
		}

		Prev->Left = Left;
		Left->Parent = Prev;
	}

	if (Parent)
	{
		// if (GET_SIZE(Node->size) > GET_SIZE(Parent->size))
		if (CompareFunc(Node->Data, Parent->Data) & OPERATOR_GREATER)
		{
			Parent->Right = NewNode;
		}
		else
		{
			Parent->Left = NewNode;
		}

		if (NewNode)
		{
			NewNode->Parent = Parent;
		}
	}
	else
	{
		Tree->Root = NewNode;
		if (NewNode)
		{
			NewNode->Parent = NULL;
		}
	}

	Tree->Size--;

	if (LeafNode && Tree->Root)
	{
		// if (GET_SIZE(Node->size) > GET_SIZE(Tree->Root->size))
		if (CompareFunc(Node->Data, Tree->Root->Data) & OPERATOR_GREATER)
		{
			Tree->RightLeafCount--;
		}
		else
		{
			Tree->RightLeafCount--;
		}
		BinaryTreeBalance(Tree);
	}
}

bool BinaryTreeIsMember(struct BinaryTree * Tree, struct BinaryTreeNode * Node)
{
	// ...

	return false;
}

void * BinaryTreeCloset(struct BinaryTree * Tree, uinteger Size, BinaryTreeSearch SearchFunc, CompareCallback CompareFunc)
{
	struct BinaryTreeNode * Node, *Iterator;

	// The callback function gets called when SearchFunc returns true
	if (Tree == NULL || Tree->Root == NULL || SearchFunc == NULL || CompareFunc == NULL)
	{
		return NULL;
	}

	Node = NULL;
	Iterator = Tree->Root;

	while (Iterator)
	{
		// if (GET_SIZE(Iterator->size)>= size)
		if (CompareFunc(Iterator->Data, &Size) & OPERATOR_GREATER_EQUAL)
		{
			BinaryTreeSearchType Result = SearchFunc(Iterator->Data, Size);

			Node = Iterator;

			if (Result == BINARY_TREE_SEARCH_ABORT)
			{
				break;
			}
		}

		// if (size < GET_SIZE(Iterator->size))
		if (CompareFunc(Iterator->Data, &Size) & OPERATOR_GREATER)
		{
			Iterator = Iterator->Left;
		}
		else
		{
			Iterator = Iterator->Right;
		}
	}

	if (Node)
	{
		return Node->Data;
	}

	return NULL;
}


void * BinaryTreeBigger(struct BinaryTree * Tree, uinteger Size, BinaryTreeSearch SearchFunc, CompareCallback CompareFunc)
{
	struct BinaryTreeNode * Node, *Iterator, *Prev;

	// The callback function gets called when search_fn returns true
	if (Tree && Tree->Root && SearchFunc && CompareFunc)
	{

		Node = NULL;
		Iterator = Tree->Root;

		while (Iterator)
		{
			if (CompareFunc(Iterator->Data, &Size) & OPERATOR_GREATER_EQUAL)
			{
				BinaryTreeSearchType Result = SearchFunc(Iterator->Data, Size);

				Node = Iterator;

				if (Result == BINARY_TREE_SEARCH_ABORT)
				{
					break;
				}
			}

			Prev = Iterator;
			Iterator = Iterator->Right;

			if (Iterator == NULL)
			{
				Iterator = Prev->Left;
			}

			if (Iterator == NULL)
			{
				Iterator = Prev->Parent;
				if (Iterator)
				{
					Iterator = Iterator->Left;
				}

				if (Iterator == Prev)
				{
					Iterator = NULL;
				}
			}
		}

		if (Node)
		{
			return Node->Data;
		}
	}

	return NULL;
}
