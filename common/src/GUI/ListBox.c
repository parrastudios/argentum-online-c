/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/IOHelper.h>
#include <Graphics/Font.h>
#include <Graphics/TextureRender.h>

#include <GUI/Widget.h>

////////////////////////////////////////////////////////////
// Create a list
////////////////////////////////////////////////////////////
struct GuiWidget * GuiListCreate(int32 X, int32 Y, uint32 Width, uint32 Height, uint32 FontSize)
{
	struct GuiWidget * Widget;

	// Create
	Widget = GuiWidgetCreate(GUI_WIDGET_LIST, X, Y, FontSize, NULL);

	if (Widget)
	{
		// Set the rect
		Widget->Width = Width;
		Widget->Height = Height;

		// Set the attributts
		Widget->Attributes.List.Size = 0;
		Widget->Attributes.List.PrevItemSelected = GUI_LIST_INVALID_ITEM;
		Widget->Attributes.List.ItemSelected = GUI_LIST_INVALID_ITEM;

		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Create a list by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiListCreateDefault()
{
	// Create
	struct GuiWidget * Widget = GuiWidgetCreateDefault(GUI_WIDGET_LIST);

	if (Widget)
	{
		// Set the rect
		Widget->Width = 120;
		Widget->Height = 300;

		// Set the attributts
		Widget->Attributes.List.Size = 0;
		Widget->Attributes.List.PrevItemSelected = GUI_LIST_INVALID_ITEM;
		Widget->Attributes.List.ItemSelected = GUI_LIST_INVALID_ITEM;

		return Widget;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
// Add an item to the list
////////////////////////////////////////////////////////////
void GuiListAddItem(struct GuiList * List, char * Item)
{
	if (Item != NULL)
	{
		if (List->Size < 0x20)
		{
			strcpy(&List->Items[List->Size][0], Item);
			List->Size++;
		}
	}
}

////////////////////////////////////////////////////////////
// Remove an item to the list
////////////////////////////////////////////////////////////
void GuiListRemoveItem(struct GuiList * List, char * Item)
{
	if (Item != NULL)
	{
		uint32 i;

		for (i = 0; i < List->Size; i++)
		{
			if (strcmp(&List->Items[i][0], Item) == 0)
			{
				List->Items[i][0] = NULLCHAR;
				List->Size--;
				return;
			}
		}
	}
}

////////////////////////////////////////////////////////////
// Set the item selected
////////////////////////////////////////////////////////////
void GuiListSetItemSelected(struct GuiWidget * Widget, uint32 Position)
{
	uint32 Item = ((Position / (Widget->FontSize + 2)));

	if (Item >= 0x20 || Item >= Widget->Attributes.List.Size)
		return;

	if (Widget->Attributes.List.Items[Item][0] != NULLCHAR)
	{
		Widget->Attributes.List.PrevItemSelected = Widget->Attributes.List.ItemSelected;
		Widget->Attributes.List.ItemSelected = Item;
	}
}

////////////////////////////////////////////////////////////
// Set the item selected by its id
////////////////////////////////////////////////////////////
void GuiListSetItemSelectedById(struct GuiWidget * Widget, uint32 Id)
{
	if (Id != GUI_LIST_INVALID_ITEM && Id < 0x20)
	{
		if (Widget->Attributes.List.Items[Id][0] != NULLCHAR)
		{
			Widget->Attributes.List.ItemSelected = Id;

			if (GuiListItemChanged(Widget))
			{
				if (Widget->ButtonReleased)
					Widget->ButtonReleased(NULL);
			}
		}
	}
}

////////////////////////////////////////////////////////////
// Check if the item selected has been changed
////////////////////////////////////////////////////////////
bool GuiListItemChanged(struct GuiWidget * Widget)
{
	if (Widget->Attributes.List.PrevItemSelected != Widget->Attributes.List.ItemSelected)
	{
		Widget->Attributes.List.PrevItemSelected = Widget->Attributes.List.ItemSelected;

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
// Get the item selected from the list
////////////////////////////////////////////////////////////
char * GuiListGetItemSelected(struct GuiList * List)
{
	if (List == NULL)
		return NULL;

	if (List->ItemSelected == GUI_LIST_INVALID_ITEM)
		return NULL;

	return List->Items[List->ItemSelected];
}

////////////////////////////////////////////////////////////
// Clear all elements of a list
////////////////////////////////////////////////////////////
void GuiListClear(struct GuiList * List)
{
	// Set the attributts
	List->Size = 0;
	List->PrevItemSelected = GUI_LIST_INVALID_ITEM;
	List->ItemSelected = GUI_LIST_INVALID_ITEM;
}

////////////////////////////////////////////////////////////
// Render a button
////////////////////////////////////////////////////////////
void GuiListRender(struct GuiWidget * Form, struct GuiWidget * Widget, struct Color4ub * BackgroundColor, struct Color4ub * FontColor)
{
	struct GuiList * List = &Widget->Attributes.List;
	uint32 i;

	// Render the background repeating it
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_BACKGROUND,
								Form->X + Widget->X, Form->Y + Widget->Y,
								Form->X + Widget->X + Widget->Width, Form->Y + Widget->Y + Widget->Height,
								BackgroundColor);

	// Render the borders repeating it
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_VBORDER,
								Form->X + Widget->X - IndexGetGrh(GUI_TEXTBOX_VBORDER)->PixelWidth, Form->Y + Widget->Y,
								Form->X + Widget->X, Form->Y + Widget->Y + Widget->Height,
								BackgroundColor);
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_VBORDER + 0x01,
								Form->X + Widget->X + Widget->Width, Form->Y + Widget->Y,
								Form->X + Widget->X + Widget->Width + IndexGetGrh(GUI_TEXTBOX_VBORDER + 0x01)->PixelWidth, Form->Y + Widget->Y + Widget->Height,
								BackgroundColor);
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_HBORDER,
								Form->X + Widget->X, Form->Y + Widget->Y - IndexGetGrh(GUI_TEXTBOX_HBORDER)->PixelHeight,
								Form->X + Widget->X + Widget->Width, Form->Y + Widget->Y,
								BackgroundColor);
	TextureRenderGrhColorRepeat(GUI_TEXTBOX_HBORDER + 0x01,
								Form->X + Widget->X, Form->Y + Widget->Y + Widget->Height,
								Form->X + Widget->X + Widget->Width, Form->Y + Widget->Y + Widget->Height + IndexGetGrh(GUI_TEXTBOX_HBORDER + 0x01)->PixelHeight,
								BackgroundColor);

	// Render the edges
	TextureRenderGrhColor(GUI_TEXTBOX_EDGE, Form->X + Widget->X - IndexGetGrh(GUI_TEXTBOX_EDGE)->PixelWidth, Form->Y + Widget->Y - IndexGetGrh(GUI_TEXTBOX_EDGE)->PixelHeight, BackgroundColor);
	TextureRenderGrhColor(GUI_TEXTBOX_EDGE + 0x01, Form->X + Widget->X + Widget->Width, Form->Y + Widget->Y - IndexGetGrh(GUI_TEXTBOX_EDGE + 0x01)->PixelHeight, BackgroundColor);
	TextureRenderGrhColor(GUI_TEXTBOX_EDGE + 0x02, Form->X + Widget->X - IndexGetGrh(GUI_TEXTBOX_EDGE + 0x02)->PixelWidth, Form->Y + Widget->Y + Widget->Height, BackgroundColor);
	TextureRenderGrhColor(GUI_TEXTBOX_EDGE + 0x03, Form->X + Widget->X + Widget->Width, Form->Y + Widget->Y + Widget->Height, BackgroundColor);

	if (List->ItemSelected != GUI_LIST_INVALID_ITEM)
	{
		uint32 X, Y, Width, Height;

		X = Form->X + Widget->X;
		Y = Form->Y + Widget->Y + (List->ItemSelected * (Widget->FontSize + 3));
		Width = Widget->Width;
		Height = Widget->FontSize;

		// Render the rect
		glBegin(GL_TRIANGLE_STRIP);
			glVertex2i(X, Y);
			glVertex2i(X + Width, Y);
			glVertex2i(X, Y + Height);
			glVertex2i(X + Width, Y + Height);
		glEnd();
	}

	// Render the items
	for (i = 0; i < List->Size; i++)
	{
		FontRenderTextColor((float)(Widget->X + Form->X + 2),
							(float)(Widget->Y + Form->Y + 2 + ((Widget->FontSize + 3) * i)), List->Items[i], FontColor);
	}
}
