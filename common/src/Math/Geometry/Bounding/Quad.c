/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Bounding/Quad.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Internal data
////////////////////////////////////////////////////////////
struct BoundingQuad QuadGetInstance = {{0.0f}};

////////////////////////////////////////////////////////////
/// Create a bounding quad
////////////////////////////////////////////////////////////
struct BoundingQuad * BoundingQuadCreate()
{
	struct BoundingQuad * Ptr = (struct BoundingQuad*)MemoryAllocate(sizeof(struct BoundingQuad));

	Vector2fSetNull(&Ptr->Bounds[0]);
	Vector2fSetNull(&Ptr->Bounds[1]);
	Vector2fSetNull(&Ptr->Center);

	return Ptr;
}

////////////////////////////////////////////////////////////
/// Destroy a bounding quad
////////////////////////////////////////////////////////////
void BoundingQuadDestroy(struct BoundingQuad * Quad)
{
	MemoryDeallocate(Quad);
}

////////////////////////////////////////////////////////////
/// Set the data of a bounding quad
////////////////////////////////////////////////////////////
void BoundingQuadSet(struct BoundingQuad * Quad, struct Vector2f * Min, struct Vector2f * Max)
{
	// Copy the vectors
	Vector2fCopy(&Quad->Bounds[0], Min);
	Vector2fCopy(&Quad->Bounds[1], Max);

	// Recalculate the center
	BoundingQuadUpdateCenter(Quad);
}

////////////////////////////////////////////////////////////
/// Set the data of a bounding quad (in other form)
////////////////////////////////////////////////////////////
void BoundingQuadSetEx(struct BoundingQuad * Quad, float x0, float y0, float x1, float y1)
{
	// Copy the vectors
	Vector2fSet(&Quad->Bounds[0], x0, y0);
	Vector2fSet(&Quad->Bounds[1], x1, y1);

	// Recalculate the center
	BoundingQuadUpdateCenter(Quad);
}

////////////////////////////////////////////////////////////
///  Get an inline quad object set to defined values
////////////////////////////////////////////////////////////
struct BoundingQuad * BoundingQuadGet(float x0, float y0, float x1, float y1)
{
	BoundingQuadSetEx(&QuadGetInstance, x0, y0, x1, y1);

	return &QuadGetInstance;
}

////////////////////////////////////////////////////////////
/// Copy the data of a bounding quad
////////////////////////////////////////////////////////////
void BoundingQuadCopy(struct BoundingQuad * Dest, struct BoundingQuad * Src, bool Recompute)
{
	Vector2fCopy(&Dest->Bounds[0], &Src->Bounds[0]);
	Vector2fCopy(&Dest->Bounds[1], &Src->Bounds[1]);
	
	if (Recompute)
	{
		BoundingQuadUpdateCenter(Dest);
	}
	else
	{
		Vector2fCopy(&Dest->Center, &Src->Center);
	}
}

////////////////////////////////////////////////////////////
/// Update the center of the bounding quad
////////////////////////////////////////////////////////////
void BoundingQuadUpdateCenter(struct BoundingQuad * Quad)
{
	Vector2fSet(&Quad->Center, Quad->Bounds[0].x + (Quad->Bounds[1].x - Quad->Bounds[0].x) * 0.5f,
							  Quad->Bounds[0].y + (Quad->Bounds[1].y - Quad->Bounds[0].y) * 0.5f);
}

////////////////////////////////////////////////////////////
/// Create a bounding quad from a list of vertices
////////////////////////////////////////////////////////////
void BoundingQuadGenerate(struct BoundingQuad * Quad, struct Vector2f * VertList, uint32 Size)
{
	uint32 i;

	// Get the minimum and maximum position
	for (i = 0; i < Size; i++)
	{
		// Check the minimum size
		if (Quad->Bounds[0].x > VertList[i].x)
			Quad->Bounds[0].x = VertList[i].x;

		if (Quad->Bounds[0].y > VertList[i].y)
			Quad->Bounds[0].y = VertList[i].y;

		// Check the maximum size
		if (Quad->Bounds[1].x < VertList[i].x)
			Quad->Bounds[1].x = VertList[i].x;

		if (Quad->Bounds[1].y < VertList[i].y)
			Quad->Bounds[1].y = VertList[i].y;

	}

	// Recalculate the center
	BoundingQuadUpdateCenter(Quad);
}

////////////////////////////////////////////////////////////
/// Check if two bounding quad are colliding
////////////////////////////////////////////////////////////
uint32 BoundingQuadCollision(struct BoundingQuad * Quad, struct BoundingQuad * Other)
{
	// Check if it is outside
	if (Quad->Bounds[1].x < Other->Bounds[0].x || Quad->Bounds[0].x > Other->Bounds[1].x ||
		Quad->Bounds[1].y < Other->Bounds[0].y || Quad->Bounds[0].y > Other->Bounds[1].y)
		return BOUNDING_QUAD_OUTSIDE;

	// Check if it is inside
	if (Quad->Bounds[1].x <= Other->Bounds[1].x && Quad->Bounds[0].x >= Other->Bounds[0].x &&
		Quad->Bounds[1].y <= Other->Bounds[1].y && Quad->Bounds[0].y >= Other->Bounds[0].y)
		return BOUNDING_QUAD_INSIDE;

	// If it aren't outside and inside, so overlap (collision)
	return BOUNDING_QUAD_OVERLAP;
}

////////////////////////////////////////////////////////////
/// Check if a point or circle is contained on the aabq
////////////////////////////////////////////////////////////
bool BoundingQuadContains(struct BoundingQuad * Quad, float x, float y, float Radius)
{
    if (Radius <= 0.0f)
	{
        if (x >= Quad->Bounds[0].x && x <= Quad->Bounds[1].x)
		{
            if (y >= Quad->Bounds[0].y && y <= Quad->Bounds[1].y)
			{
                return true;
            }
        }
    }
	else
	{
        // by Cygon (stackoverflow.com)

        // Find the closest point to the circle within the rectangle
        float ClosestX = clamp(x, Quad->Bounds[0].x, Quad->Bounds[1].x);
        float ClosestY = clamp(y, Quad->Bounds[0].y, Quad->Bounds[1].y);

        // Calculate the distance between the circle's center and this closest point
        float DistanceX = x - ClosestX;
        float DistanceY = y - ClosestY;

        // If the distance is less than the circle's radius, an intersection occurs
        float DistanceSquared = (DistanceX * DistanceX) + (DistanceY * DistanceY);

        return (bool)(DistanceSquared < (Radius * Radius));
    }

    return false;
}

////////////////////////////////////////////////////////////
/// Distance squared to a point from the quad (arvos algorithm)
////////////////////////////////////////////////////////////
float BoundingQuadDistanceSquared(struct BoundingQuad * Quad, struct Vector2f * Point)
{
	struct Vector2f Distance = { 0.0f, 0.0f };


	return 0.0f;
}

////////////////////////////////////////////////////////////
/// Distance to a point from the quad
////////////////////////////////////////////////////////////
float BoundingQuadDistance(struct BoundingQuad * Quad, struct Vector2f * Point)
{
	float DistanceSquared = BoundingQuadDistanceSquared(Quad, Point);

	return (float)sqrt(DistanceSquared);
}
