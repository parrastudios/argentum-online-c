/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/Platform.h>
#include <unistd.h>
#include <sys/time.h>

////////////////////////////////////////////////////////////
// Defines
////////////////////////////////////////////////////////////
#if defined(SYSTEM_LINUX) || defined(SYSTEM_FREEBSD)
    #define TIME_ACCURATE_CLOCK
    #include <time.h>
#endif

////////////////////////////////////////////////////////////
// Declarations
////////////////////////////////////////////////////////////
#ifdef TIME_ACCURATE_CLOCK
static struct timespec StartTicks = {0, 0};
#else
static struct timeval StartTicks = {0, 0};
#endif

static bool Started = false;

////////////////////////////////////////////////////////////
/// Initialize the system time counter
////////////////////////////////////////////////////////////
void SystemTimeInitialize()
{
    #ifdef TIME_ACCURATE_CLOCK
        clock_gettime(CLOCK_MONOTONIC, &StartTicks);
    #else
        gettimeofday(&StartTicks, NULL);
    #endif

    Started = true;
}

////////////////////////////////////////////////////////////
/// Get the current system time
////////////////////////////////////////////////////////////
float SystemGetTime()
{
    #ifdef TIME_ACCURATE_CLOCK
        struct timespec Time = {0, 0};
    #else
        struct timeval Time = {0, 0};
    #endif

    if (Started == false)
        SystemTimeInitialize();

    #ifdef TIME_ACCURATE_CLOCK
        clock_gettime(CLOCK_MONOTONIC, &Time);

        return (Time.tv_sec - StartTicks.tv_sec) * 1000 + (Time.tv_nsec - StartTicks.tv_nsec) / 1000000.0f;
    #else
        gettimeofday(&Time, NULL);

        return (Time.tv_sec - StartTicks.tv_sec) * 1000 + (Time.tv_usec - StartTicks.tv_usec) / 1000.0f;
    #endif
}


////////////////////////////////////////////////////////////
/// Suspend the execution of the current thread for a specified time
////////////////////////////////////////////////////////////
void SystemSleep(float Milliseconds)
{
    usleep((unsigned long)(Milliseconds * 1000.0f));
}
