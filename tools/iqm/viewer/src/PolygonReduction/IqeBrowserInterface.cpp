/* PinocchioInterface.cpp

11.11.2014 RR: * First edition of this file
                 Base was the file bunnygut.cpp from the
                 polygon reduction demo of Stan Melax.
                 Reworked file polygon reduction is usable from IqeBrowser.


*/

#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "vector.h"
#include "progmesh.h"

#include "../IqeBrowser.h"
#include "IqeBrowserInterface.h"

// Note that the use of the Map() function and the collapse_map
// list isn't part of the polygon reduction algorithm.
// We just set up this system here in this module
// so that we could retrieve the model at any desired vertex count.
// Therefore if this part of the program confuses you, then
// dont worry about it.  It might help to look over the progmesh.cpp
// module first.

//       Map()
//
// When the model is rendered using a maximum of mx vertices
// then it is vertices 0 through mx-1 that are used.
// We are able to do this because the vertex list
// gets sorted according to the collapse order.
// The Map() routine takes a vertex number 'a' and the
// maximum number of vertices 'mx' and returns the
// appropriate vertex in the range 0 to mx-1.
// When 'a' is greater than 'mx' the Map() routine
// follows the chain of edge collapses until a vertex
// within the limit is reached.
//   An example to make this clear: assume there is
//   a triangle with vertices 1, 3 and 12.  But when
//   rendering the model we limit ourselves to 10 vertices.
//   In that case we find out how vertex 12 was removed
//   by the polygon reduction algorithm.  i.e. which
//   edge was collapsed.  Lets say that vertex 12 was collapsed
//   to vertex number 7.  This number would have been stored
//   in the collapse_map array (i.e. collapse_map[12]==7).
//   Since vertex 7 is in range (less than max of 10) we
//   will want to render the triangle 1,3,7.
//   Pretend now that we want to limit ourselves to 5 vertices.
//   and vertex 7 was collapsed to vertex 3
//   (i.e. collapse_map[7]==3).  Then triangle 1,3,12 would now be
//   triangle 1,3,3.  i.e. this polygon was removed by the
//   progressive mesh polygon reduction algorithm by the time
//   it had gotten down to 5 vertices.
//   No need to draw a one dimensional polygon. :-)
static int Map( List<int> &collapse_map, int a,int mx)
{
	if(mx<=0) return 0;
	while(a>=mx) {
		a=collapse_map[a];
	}
	return a;
}

/************************************************************************************
  ProgressiveMesh
*/

static void PermuteVertices( List<Vector> &vert, List<tridata> &tri, List<int> &permutation)
{

	// rearrange the vertex list
	List<Vector> temp_list;
	int i;
	assert(permutation.num==vert.num);
	for(i=0;i<vert.num;i++) {
		temp_list.Add(vert[i]);
	}
	for(i=0;i<vert.num;i++) {
		vert[permutation[i]]=temp_list[i];
	}
	// update the changes in the entries in the triangle list
	for(i=0;i<tri.num;i++) {
		for(int j=0;j<3;j++) {
			tri[i].v[j] = permutation[tri[i].v[j]];
		}
	}
}

/************************************************************************************
  IqeB_ModelPolygonReduction

  Arguments:
     pModel                   The model to reduce
     ReduceVertexPercentage   If > 0, reduce # vertices by this percentage
     ReduceMaxVertexAmount    If > 0, this is the max # of vertices we want
     ReduceMinVertexAmount    If > 0, want to keep minimum this # of vertices

  Return:
     0     OK
     < 0   Error

*/

int IqeB_ModelPolygonReduction( IQE_model *pModel,
                                int ReduceVertexPercentage,
                                int ReduceMaxVertexAmount,
                                int ReduceMinVertexAmount
                              )
{
  List<Vector> vert;       // global list of vertices
  List<tridata> tri;       // global list of triangles
  List<int> collapse_map;  // to which neighbor each vertex collapses
	List<int> permutation;
	int i, j, render_num;

	// How many vertices to reduce in percent

	render_num = pModel->mesh->vertex_count / 2;          // preset 50 %

  if( ReduceVertexPercentage < 100 && ReduceVertexPercentage > 0) {  // is in reasonable range

	  render_num = (pModel->mesh->vertex_count * (100 - ReduceVertexPercentage) + 50) / 100;
  }

	if( ReduceMaxVertexAmount > 0 && render_num > ReduceMaxVertexAmount) {  // is valid and reduce by max # vertices

     render_num = ReduceMaxVertexAmount;
	}

	if( ReduceMinVertexAmount > 0 && render_num < ReduceMinVertexAmount) {  // is valid and keep minimum this # of vertices

     render_num = ReduceMinVertexAmount;
	}

	// security tests of # vertices

	if(render_num < 5) {  // the absolute minimim we want

    render_num = 5;
	}

	if(render_num > pModel->mesh->vertex_count) {

    render_num = pModel->mesh->vertex_count;
	}

	// security test, we don't reduce

	if( render_num == pModel->mesh->vertex_count) {   // This would change nothing

	  return( 0);                                     // Simply return OK
	}

	// Convert IqeBrower mesh to data usable for polygon reduction

	for (i = 0; i < pModel->mesh->vertex_count; i++) {

    float x, y, z;

    x = pModel->mesh->position[ i * 3 + 0];
    y = pModel->mesh->position[ i * 3 + 1];
    z = pModel->mesh->position[ i * 3 + 2];

		vert.Add(Vector( x, y, z));
  }

	for (i = 0; i < pModel->mesh->part_count; i++) {

    for( j = 0; j <pModel->mesh->part[i].count; j += 3) {

      // indices will be 0 based
		  tridata td;

      td.v[ 0] = pModel->mesh->element[ pModel->mesh->part[ i].first + j + 0];
      td.v[ 1] = pModel->mesh->element[ pModel->mesh->part[ i].first + j + 1];
      td.v[ 2] = pModel->mesh->element[ pModel->mesh->part[ i].first + j + 2];

		  tri.Add(td);
    }
	}

	// security test, have some minimum amount of data to do

	if( vert.num < 1 || tri.num < 1) {                // Minimum one vertex and one triangle

	  return( -1);                                    // Error, empty mesh
	}

	// Polygon reduction
	ProgressiveMesh( vert, tri, collapse_map, permutation);
	PermuteVertices( vert, tri, permutation);

	// Convert back
	// We have less vertices and less triangles, so simply use the existing
	// allocated memory.
	// NOTE: We get one parial mesh output.
	//       Could be optimized to reduce the mesh parts indepented
	//       from each other.

  pModel->mesh->part_count = 1;         // one mesh part

  pModel->mesh->part[ 0].first = 0;
  pModel->mesh->part[ 0].count = 0;
  pModel->mesh->element_count  = 0;

  j = 0;  // count triangle elements

	for( i = 0; i < tri.num; i++) {

		int p0= Map( collapse_map, tri[i].v[0], render_num);
		int p1= Map( collapse_map, tri[i].v[1], render_num);
		int p2= Map( collapse_map, tri[i].v[2], render_num);

		// note:  serious optimization opportunity here,
		//  by sorting the triangles the following "continue"
		//  could have been made into a "break" statement.
		if(p0==p1 || p1==p2 || p2==p0) {

      continue;
		}

    pModel->mesh->element[ pModel->mesh->part[ 0].first + j + 0] = p0;
    pModel->mesh->element[ pModel->mesh->part[ 0].first + j + 1] = p1;
    pModel->mesh->element[ pModel->mesh->part[ 0].first + j + 2] = p2;

    j += 3;
	}
  pModel->mesh->part[ 0].count = j;   // update element count
  pModel->mesh->element_count  += j;  // The amount of all triangle alements

  // Updating vercies is simple.
  // Only have to set new vertex count and copy the vertexes.

  pModel->mesh->vertex_count = render_num;

	for( i = 0; i < render_num; i++) {

    pModel->mesh->position[ i * 3 + 0] = vert[ i].x;
    pModel->mesh->position[ i * 3 + 1] = vert[ i].y;
    pModel->mesh->position[ i * 3 + 2] = vert[ i].z;
	}

	// reallocated memory

  pModel->mesh->element = (int *)realloc( pModel->mesh->element, sizeof( int) * pModel->mesh->element_count); // reallocte memory
  pModel->mesh->position = (float *)realloc( pModel->mesh->position, sizeof( float) * render_num * 3); // reallocte memory

	// recontruct normals

  IqeB_MeshComputeVertexNormals( pModel->mesh);

  // ...

  return( 0);   // return OK
}

/*********************************** End Of File ***********************************/
