/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/Texture.h>
#ifndef TEXTURE_HELPER_WRITE_READ_IMPL
#	define TEXTURE_HELPER_WRITE_READ_IMPL
#	include <Graphics/TextureHelper.h>
#endif
#include <Graphics/Device.h>
#include <Math/General.h>
#include <System/IOHelper.h>
#include <System/Error.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
/// Update correct size from GL_ARB
////////////////////////////////////////////////////////////
uinteger TextureValidSize(uinteger Size)
{
	// If hardware doesn't support NPOT textures, we calculate the nearest power of two
	if (DeviceGetInfo()->Support.TextureNPOT == false)
		return MathPowerOfTwo(Size);

	// If hardware supports NPOT textures, then just return the unmodified size
	return Size;
}

////////////////////////////////////////////////////////////
/// Create 2D texture with loaded data
////////////////////////////////////////////////////////////
bool TextureCreate(struct Texture * TextureData, bool Mipmap, bool ColorKey)
{
	uinteger TextureWidth, TextureHeight;
	GLint  PreviousTexture = 0;
	GLuint Texture = 0;

	if (!TextureData->Pixels)
		return false;

	// Check if texture parameters are valid before creating it
	if (!TextureData->Width || !TextureData->Height)
		return false;

    // Adjust internal texture dimensions depending on NPOT textures support
    TextureWidth  = TextureValidSize(TextureData->Width);
    TextureHeight = TextureValidSize(TextureData->Height);

    // Check the maximum texture size
	if ((TextureWidth > (uinteger)(DeviceGetInfo()->Limits.Texture)) || (TextureHeight > (uinteger)(DeviceGetInfo()->Limits.Texture)))
    {
        MessageError("TextureCreate", "Failed to create image, its internal size is too high (%dx%d).", TextureWidth, TextureHeight);
        return false;
    }

    TextureData->TextureWidth = TextureWidth;
    TextureData->TextureHeight = TextureHeight;

    // Select format from channels
    if (TextureData->Channels == 1)
    {
        TextureData->Format = GL_ALPHA;
    }
    else if (TextureData->Channels == 2)
    {
        TextureData->Format = GL_LUMINANCE_ALPHA;
    }
    else if (TextureData->Channels == 3)
    {
        TextureData->Format = GL_RGB;
    }
    else if (TextureData->Channels == 4)
    {
        TextureData->Format = GL_RGBA;
    }
    else
    {
        // Invalid
        TextureData->Format = 0;
    }

	// Set 2D texture target by default
	TextureData->Target = GL_TEXTURE_2D;

	glEnable(TextureData->Target);

	glGetIntegerv(DeviceBindingConst(TextureData->Target), &PreviousTexture);

	glGenTextures(1, &Texture);

	// Pixel storage mode (word alignment / 4 bytes)
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	glBindTexture(TextureData->Target, Texture);

	// Color key
	if (ColorKey)
	{
	    /*
		uint32 i, TextureSize = TextureData->Width * TextureData->Height;;

		for (i = 0; i < TextureSize; i++)
		{
			if ( *(uint32*)&TextureData->Pixels[i] == 0xFF000000 ) // { 0, 0, 0, 255 }
				TextureData->Pixels[i].a = 0;
		}*/
	}

	//if (Mipmap)
	//{
	//	glTexParameteri(TextureData->Target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//	glTexParameteri(TextureData->Target, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	//
	//	gluBuild2DMipmaps(TextureData->Target, GL_RGBA, TextureData->TextureWidth, TextureData->TextureHeight,
	//										   GL_RGBA, GL_UNSIGNED_BYTE, &TextureData->Pixels[0]);
	//}
	//else
	{
		glTexParameteri(TextureData->Target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(TextureData->Target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		if (DeviceGetInfo()->Support.TextureNPOT)
		{
			glTexImage2D(TextureData->Target, 0, TextureData->Format, TextureData->TextureWidth, TextureData->TextureHeight,
												0, TextureData->Format, GL_UNSIGNED_BYTE, TextureData->Pixels);
		}
		else
		{
			glTexImage2D(TextureData->Target, 0, TextureData->Format, TextureData->TextureWidth, TextureData->TextureHeight,
												0, TextureData->Format, GL_UNSIGNED_BYTE, NULL);
			glTexSubImage2D(TextureData->Target, 0, 0, 0, TextureData->Width, TextureData->Height, TextureData->Format,
															GL_UNSIGNED_BYTE, TextureData->Pixels);
		}
	}

	glTexParameteri(TextureData->Target, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(TextureData->Target, GL_TEXTURE_WRAP_T, GL_REPEAT);

	TextureData->Id = (integer)(Texture);

	glBindTexture(TextureData->Target, PreviousTexture);

    return true;
}

////////////////////////////////////////////////////////////
/// Bind 2D texture with previous cache
////////////////////////////////////////////////////////////
void TextureBind(struct Texture * TextureData)
{
	GLint PreviousTexture;

	if (TextureData)
	{
		// Check texture
		if (TextureData->Id > 0)
		{
			glEnable(TextureData->Target);

			glGetIntegerv(DeviceBindingConst(TextureData->Target), &PreviousTexture);

			if ( PreviousTexture != (GLint)TextureData->Id )
			{
				// Bind the ID texture specified by the 2nd parameter
				glBindTexture(TextureData->Target, TextureData->Id);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Render a static 2D texture with triangle strip
/// Specifications: The texture doesn't depend on rect
///					the rect can cut it
////////////////////////////////////////////////////////////
void TextureRenderEx(struct Texture * TextureData, integer X, integer Y, struct Rect * RectData, struct Color4ub * Color)
{
	integer Width, Height;

	// Bind texture
	TextureBind(TextureData);

	// Calculate size
	Width = RectData->Right - RectData->Left;
	Height = RectData->Bottom - RectData->Top;

	// Set color
	glColor4ub(Color->r, Color->g, Color->b, Color->a);

	// Render
	glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2i(0, 0);	glVertex2i(X, Y);
		glTexCoord2i(1, 0);	glVertex2i(X + Width, Y);
		glTexCoord2i(0, 1);	glVertex2i(X, Y + Height);
		glTexCoord2i(1, 1);	glVertex2i(X + Width, Y + Height);
	glEnd();
}

/*void TextureRenderVertexArray(struct Texture * TextureData, int32 X, int32 Y, struct Rect * RectData, struct Color4ub * Color)
{

	GLint	Coordinates[] = { 0, 0, 1, 0, 0, 1, 1, 1 };
	GLubyte Colors[] = { Color->r, Color->g, Color->b, Color->a,
						 Color->r, Color->g, Color->b, Color->a,
						 Color->r, Color->g, Color->b, Color->a,
						 Color->r, Color->g, Color->b, Color->a };

	int32 Width, Height;

	// Bind texture
	TextureBind(TextureData);

	// Calculate size
	Width = RectData->Right - RectData->Left;
	Height = RectData->Bottom - RectData->Top;

	// Blend
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	// Enble vertex arrays
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	// Specify pointers to vertex arrays
    glNormalPointer(GL_UNSIGNED_INT, 0, Normals);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, Colors);
    glVertexPointer(3, GL_UNSIGNED_INT, 0, Vertices);
	glTexCoordPointer(2, GL_INT, 0, Coordinates);

    glPushMatrix();

	// Move to the position
    glTranslatef((GLfloat)X, (GLfloat)Y, 0);

	// Render
    glDrawArrays(GL_QUADS, 0, 24);

    glPopMatrix();

	// Disable vertex arrays
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
}*/

////////////////////////////////////////////////////////////
/// Delete the texture from memory and OpenGL
////////////////////////////////////////////////////////////
void TextureDelete(struct Texture * TextureData)
{
	if (TextureData)
	{
		// Delete Texture
		glDeleteTextures(1, &TextureData->Id);

		// Delete memory
		MemoryDeallocate(TextureData->Pixels);
	}
}

////////////////////////////////////////////////////////////
/// Save texture from pixels as a bmp
////////////////////////////////////////////////////////////
bool TextureSave(char * Filename, integer Width, integer Height, integer Channels, uint8 * Data, uinteger Format)
{
	// Check integrity
	if ( (Width < 1) || (Height < 1) ||
		(Channels < 1) || (Channels > 4) ||
		(Data == NULL) ||
		(Filename == NULL) )
	{
		return false;
	}

	switch (Format)
	{
		case TEXTURE_FORMAT_TGA :
			return stbi_write_tga((char*)Filename, Width, Height, Channels, (void*)Data);

		case TEXTURE_FORMAT_PNG :
			return stbi_write_png((char*)Filename, Width, Height, Channels, (void*)Data, 0);

		case TEXTURE_FORMAT_BMP :
			return stbi_write_bmp((char*)Filename, Width, Height, Channels, (void*)Data);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Get the deallocate path to the screenshot
////////////////////////////////////////////////////////////
bool TextureFindScreenShotPath(char * Path, uinteger Format)
{
	uinteger i;

	for (i = 0; i <= TEXTURE_SCREENSHOT_SIZE; i++)
	{
		FILE * File;

		switch (Format)
		{
			case TEXTURE_FORMAT_TGA :
			{
				sprintf(Path, "data/screenshots/%d.tga", i);
				break;
			}

			case TEXTURE_FORMAT_PNG :
			{
				sprintf(Path, "data/screenshots/%d.png", i);
				break;
			}

			case TEXTURE_FORMAT_BMP :
			{
				sprintf(Path, "data/screenshots/%d.bmp", i);
				break;
			}

			default :
				return false;
		}

		if (!fopen(&File, Path, "r")) return true;

		fclose(File);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Take a screenshot
////////////////////////////////////////////////////////////
void TextureScreenShot(uinteger Width, uinteger Height)
{
	char ScreenShotPath[32];
	uint8 * Data;
	uinteger i, j;

	if (TextureFindScreenShotPath(ScreenShotPath, TEXTURE_FORMAT_BMP) == false) // BMP by default
		return;

    // Get the data from OpenGL
    Data = (uint8*)MemoryAllocate( 3 * Width * Height );
    glReadPixels(0, 0, Width, Height, GL_RGB, GL_UNSIGNED_BYTE, Data);

    // Invert the image
    for ( j = 0; j * 2 < Height; ++j )
	{
		uinteger Index1 = j * Width * 3;
		uinteger Index2 = (Height - 1 - j) * Width * 3;

		for (i = Width * 3; i > 0; --i )
		{
			uint8 Temp = Data[Index1];
			Data[Index1] = Data[Index2];
			Data[Index2] = Temp;
			++Index1;
			++Index2;
		}
	}

    // Save the image
    if (TextureSave( ScreenShotPath, Width, Height, 3, Data, TEXTURE_FORMAT_BMP) == false)
    {
		MessageError("Error", "Error when the application tried to save the screenshot.");
    }

    // And deallocate the memory
    MemoryDeallocate(Data);
}
