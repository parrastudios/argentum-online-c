/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Endianness.h>
#include <Portability/Casting.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Endian bitmask index definition
////////////////////////////////////////////////////////////
#define ENDIAN_INDEX_BIT				UINT32_SUFFIX(0x00000003)

////////////////////////////////////////////////////////////
/// Endian index definitions
////////////////////////////////////////////////////////////
#define ENDIAN_INDEX_LITTLE_WORD		UINTEGER_SUFFIX(0x00)	//< Middle-endian, PDP-11 style (0xBBAADDCC & 0x00000003 -> 0x00000000)
#define ENDIAN_INDEX_BIG				UINTEGER_SUFFIX(0x01)	//< Big-endian (0xAABBCCDD & 0x00000003 -> 0x00000001)
#define ENDIAN_INDEX_LITTLE				UINTEGER_SUFFIX(0x02)	//< Little-endian (0xDDCCBBAA & 0x00000003 -> 0x00000002)
#define ENDIAN_INDEX_BIG_WORD			UINTEGER_SUFFIX(0x03)	//< Middle-endian, Honeywell 316 style (0xCCDDAABB & 0x00000003 -> 0x00000003)
#define ENDIAN_INDEX_SIZE				UINTEGER_SUFFIX(0x04)	//< Number of different endianness supported
#define ENDIAN_INDEX_INVALID			UINTEGER_MAX_RANGE		//< Invalid index

////////////////////////////////////////////////////////////
/// Unimplemented table definition
////////////////////////////////////////////////////////////
#define ENDIAN_UNIMPLEMENTED_TABLE \
	{ NULL }

////////////////////////////////////////////////////////////
/// Endian no operation table definition
////////////////////////////////////////////////////////////
#define ENDIAN_NO_OP_TABLE \
	TypeInit(EndianInt16, &EndianNoOpInt16), \
	TypeInit(EndianUInt16, &EndianNoOpUInt16), \
	TypeInit(EndianInt32, &EndianNoOpInt32), \
	TypeInit(EndianUInt32, &EndianNoOpUInt32), \
	TypeInit(EndianInt64, &EndianNoOpInt64), \
	TypeInit(EndianUInt64, &EndianNoOpUInt64), \
	TypeInit(EndianFloat32, &EndianNoOpFloat32), \
	TypeInit(EndianFloat64, &EndianNoOpFloat64)

////////////////////////////////////////////////////////////
/// Endian swap table definition
////////////////////////////////////////////////////////////
#define ENDIAN_SWAP_TABLE \
	TypeInit(EndianInt16, &EndianSwapInt16), \
	TypeInit(EndianUInt16, &EndianSwapUInt16), \
	TypeInit(EndianInt32, &EndianSwapInt32), \
	TypeInit(EndianUInt32, &EndianSwapUInt32), \
	TypeInit(EndianInt64, &EndianSwapInt64), \
	TypeInit(EndianUInt64, &EndianSwapUInt64), \
	TypeInit(EndianFloat32, &EndianSwapFloat32), \
	TypeInit(EndianFloat64, &EndianSwapFloat64)

////////////////////////////////////////////////////////////
/// Endian little to little word table definition
////////////////////////////////////////////////////////////
#define ENDIAN_LITTLE_LITTLE_WORD_TABLE \
	TypeInit(EndianInt16, &EndianLittleLittleWordSwapInt16), \
	TypeInit(EndianUInt16, &EndianLittleLittleWordSwapUInt16), \
	TypeInit(EndianInt32, &EndianLittleLittleWordSwapInt32), \
	TypeInit(EndianUInt32, &EndianLittleLittleWordSwapUInt32), \
	TypeInit(EndianInt64, &EndianNoOpInt64), \
	TypeInit(EndianUInt64, &EndianNoOpUInt64), \
	TypeInit(EndianFloat32, &EndianNoOpFloat32), \
	TypeInit(EndianFloat64, &EndianNoOpFloat64)

////////////////////////////////////////////////////////////
/// Endian big to little word table definition
////////////////////////////////////////////////////////////
#define ENDIAN_BIG_LITTLE_WORD_TABLE \
	TypeInit(EndianInt16, &EndianBigLittleWordSwapInt16), \
	TypeInit(EndianUInt16, &EndianBigLittleWordSwapUInt16), \
	TypeInit(EndianInt32, &EndianBigLittleWordSwapInt32), \
	TypeInit(EndianUInt32, &EndianBigLittleWordSwapUInt32), \
	TypeInit(EndianInt64, &EndianNoOpInt64), \
	TypeInit(EndianUInt64, &EndianNoOpUInt64), \
	TypeInit(EndianFloat32, &EndianNoOpFloat32), \
	TypeInit(EndianFloat64, &EndianNoOpFloat64)

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Swap 16-bit (un)signed integer value (little <-> big)
////////////////////////////////////////////////////////////
#define EndianSwap16(Value, Suffix) ( \
		((((Value) << 8) & Suffix(0xFF00)) | \
		(((Value) >> 8) & Suffix(0x00FF))) \
	)

////////////////////////////////////////////////////////////
/// Swap 32-bit (un)signed integer value (little <-> big)
////////////////////////////////////////////////////////////
#define EndianSwap32(Value, Suffix) ( \
		(((Value) << 24) & Suffix(0xFF000000)) | \
		(((Value) >> 24) & Suffix(0x000000FF)) | \
		(((Value) >> 8) & Suffix(0x0000FF00)) | \
		(((Value) << 8) & Suffix(0x00FF0000)) \
	)

////////////////////////////////////////////////////////////
/// Swap 64-bit (un)signed integer value (little <-> big)
////////////////////////////////////////////////////////////
#define EndianSwap64(Value, Suffix) ( \
		(((Value) << 56) & Suffix(0xFF00000000000000)) | \
		(((Value) >> 56) & Suffix(0x00000000000000FF)) | \
		(((Value) << 40) & Suffix(0x00FF000000000000)) | \
		(((Value) >> 40) & Suffix(0x000000000000FF00)) | \
		(((Value) << 24) & Suffix(0x0000FF0000000000)) | \
		(((Value) >> 24) & Suffix(0x0000000000FF0000)) | \
		(((Value) << 8) & Suffix(0x000000FF00000000)) | \
		(((Value) >> 8) & Suffix(0x00000000FF000000)) \
	)

////////////////////////////////////////////////////////////
/// Swap 16-bit (un)signed integer value (little <-> pdp)
////////////////////////////////////////////////////////////
#define EndianLittleLittleWordSwap16(Value, Suffix) ( \
		(Value) \
	)

////////////////////////////////////////////////////////////
/// Swap 32-bit (un)signed integer value (little <-> pdp)
////////////////////////////////////////////////////////////
#define EndianLittleLittleWordSwap32(Value, Suffix) ( \
		((((Value) << 16) & Suffix(0xFFFF0000)) | \
		(((Value) >> 16) & Suffix(0x0000FFFF))) \
	)

////////////////////////////////////////////////////////////
/// Swap 16-bit (un)signed integer value (big <-> pdp)
////////////////////////////////////////////////////////////
#define EndianBigLittleWordSwap16(Value, Suffix) ( \
		EndianSwap16(Value, Suffix) \
	)

////////////////////////////////////////////////////////////
/// Swap 32-bit (un)signed integer value (big <-> pdp)
////////////////////////////////////////////////////////////
#define EndianBigLittleWordSwap32(Value, Suffix) ( \
		((((Value) << 8) & Suffix(0xFF00FF00)) | \
		(((Value) >> 8) & Suffix(0x00FF00FF))) \
	)

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct EndianCallbackTableType
{
	//< 16-bit integer types
	int16 (*EndianInt16)(int16);
	uint16 (*EndianUInt16)(uint16);

	//< 32-bit integer types
	int32 (*EndianInt32)(int32);
	uint32 (*EndianUInt32)(uint32);

	//< 64-bit integer types
	int64 (*EndianInt64)(int64);
	uint64 (*EndianUInt64)(uint64);

	//< Floating point types
	float32 (*EndianFloat32)(float32);
	float64 (*EndianFloat64)(float64);
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Retreive the endianness type in run-time
////////////////////////////////////////////////////////////
EndianType EndiannessGetType()
{
	EndianType Value;
	uint8 * ValuePointer = (uint8 *)&Value;
	uint8 Buffer[sizeof(EndianType)];

	// Initialize pattern for endianness detection
	Buffer[0] = 0xAA;
	Buffer[1] = 0xBB;
	Buffer[2] = 0xCC;
	Buffer[3] = 0xDD;

	// Copy buffer into endian type (32-bit integer)
	MemoryCopy(ValuePointer, Buffer, sizeof(EndianType));

	// Check endianness correlation with compile-time detection
	if (Value == ENDIAN_TYPE)
	{
		return Value;
	}

	return ENDIAN_UNKNOWN;
}

////////////////////////////////////////////////////////////
/// No operation with 16-bit signed integer
////////////////////////////////////////////////////////////
int16 EndianNoOpInt16(int16 Value)
{
	return Value;
}

////////////////////////////////////////////////////////////
/// No operation with 16-bit unsigned integer
////////////////////////////////////////////////////////////
uint16 EndianNoOpUInt16(uint16 Value)
{
	return Value;
}

////////////////////////////////////////////////////////////
/// No operation with 32-bit signed integer
////////////////////////////////////////////////////////////
int32 EndianNoOpInt32(int32 Value)
{
	return Value;
}

////////////////////////////////////////////////////////////
/// No operation with 16-bit unsigned integer
////////////////////////////////////////////////////////////
uint32 EndianNoOpUInt32(uint32 Value)
{
	return Value;
}

////////////////////////////////////////////////////////////
/// No operation with 64-bit signed integer
////////////////////////////////////////////////////////////
int64 EndianNoOpInt64(int64 Value)
{
	return Value;
}

////////////////////////////////////////////////////////////
/// No operation with 64-bit unsigned integer
////////////////////////////////////////////////////////////
uint64 EndianNoOpUInt64(uint64 Value)
{
	return Value;
}

////////////////////////////////////////////////////////////
/// No operation with 32-bit floating point
////////////////////////////////////////////////////////////
float32 EndianNoOpFloat32(float32 Value)
{
	return Value;
}

////////////////////////////////////////////////////////////
/// No operation with 64-bit floating point
////////////////////////////////////////////////////////////
float64 EndianNoOpFloat64(float64 Value)
{
	return Value;
}

////////////////////////////////////////////////////////////
/// Swap signed 16-bit integer
////////////////////////////////////////////////////////////
int16 EndianSwapInt16(int16 Value)
{
	return EndianSwap16(Value, INT16_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Swap unsigned 16-bit integer
////////////////////////////////////////////////////////////
uint16 EndianSwapUInt16(uint16 Value)
{
	return EndianSwap16(Value, UINT16_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Swap signed 32-bit integer
////////////////////////////////////////////////////////////
int32 EndianSwapInt32(int32 Value)
{
	return EndianSwap32(Value, INT32_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Swap unsigned 32-bit integer
////////////////////////////////////////////////////////////
uint32 EndianSwapUInt32(uint32 Value)
{
	return EndianSwap32(Value, UINT32_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Swap signed 64-bit integer
////////////////////////////////////////////////////////////
int64 EndianSwapInt64(int64 Value)
{
	return EndianSwap64(Value, INT64_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Swap unsigned 64-bit integer
////////////////////////////////////////////////////////////
uint64 EndianSwapUInt64(uint64 Value)
{
	return EndianSwap64(Value, UINT64_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Swap floating point 32-bit precision
////////////////////////////////////////////////////////////
float32 EndianSwapFloat32(float32 Value)
{
	uint32 ReinterpretFloatToUInt = CastReinterpret32FloatUInt(Value);
	uint8 * UIntPointer = (uint8 *)&ReinterpretFloatToUInt;
	uint8 * FloatPointer = (uint8 *)&Value;

	ReinterpretFloatToUInt = EndianSwap32(ReinterpretFloatToUInt, UINT32_SUFFIX);

	// Reinterpret safely the integer into float without
	// violating strict aliasing and type-pruned rules
	MemoryCopy(FloatPointer, UIntPointer, sizeof(float32));

	// Return float value swapped
	return Value;
}

////////////////////////////////////////////////////////////
/// Swap floating point 64-bit precision
////////////////////////////////////////////////////////////
float64 EndianSwapFloat64(float64 Value)
{
	uint64 ReinterpretFloatToUInt = CastReinterpret64FloatUInt(Value);
	uint8 * UIntPointer = (uint8 *)&ReinterpretFloatToUInt;
	uint8 * FloatPointer = (uint8 *)&Value;

	ReinterpretFloatToUInt = EndianSwap64(ReinterpretFloatToUInt, UINT64_SUFFIX);

	// Reinterpret safely the integer into float without
	// violating strict aliasing and type-pruned rules
	MemoryCopy(FloatPointer, UIntPointer, sizeof(float64));

	// Return float value swapped
	return Value;
}

////////////////////////////////////////////////////////////
/// Swap 16-bit signed integer from little to pdp
////////////////////////////////////////////////////////////
int16 EndianLittleLittleWordSwapInt16(int16 Value)
{
	return EndianLittleLittleWordSwap16(Value, INT16_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Swap 16-bit unsigned integer from little to pdp
////////////////////////////////////////////////////////////
uint16 EndianLittleLittleWordSwapUInt16(uint16 Value)
{
	return EndianLittleLittleWordSwap16(Value, UINT16_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Swap 32-bit signed integer from little to pdp
////////////////////////////////////////////////////////////
int32 EndianLittleLittleWordSwapInt32(int32 Value)
{
	return EndianLittleLittleWordSwap32(Value, INT32_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Swap 32-bit unsigned integer from little to pdp
////////////////////////////////////////////////////////////
uint32 EndianLittleLittleWordSwapUInt32(uint32 Value)
{
	return EndianLittleLittleWordSwap32(Value, UINT32_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Swap 16-bit signed integer from big to pdp
////////////////////////////////////////////////////////////
int16 EndianBigLittleWordSwapInt16(int16 Value)
{
	return EndianBigLittleWordSwap16(Value, INT16_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Swap 16-bit unsigned integer from big to pdp
////////////////////////////////////////////////////////////
uint16 EndianBigLittleWordSwapUInt16(uint16 Value)
{
	return EndianBigLittleWordSwap16(Value, UINT16_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Swap 32-bit signed integer from big to pdp
////////////////////////////////////////////////////////////
int32 EndianBigLittleWordSwapInt32(int32 Value)
{
	return EndianLittleLittleWordSwap32(Value, INT32_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Swap 32-bit unsigned integer from big to pdp
////////////////////////////////////////////////////////////
uint32 EndianBigLittleWordSwapUInt32(uint32 Value)
{
	return EndianBigLittleWordSwap32(Value, UINT32_SUFFIX);
}

////////////////////////////////////////////////////////////
/// Convert an endian type into a array index
////////////////////////////////////////////////////////////
uinteger EndiannessTypeIndex(EndianType Endian)
{
	if (Endian != ENDIAN_UNCHECKED && Endian != ENDIAN_UNKNOWN)
	{
		EndianType Index = (EndianType)(Endian & ENDIAN_INDEX_BIT);

		return (uinteger)Index;
	}

	return ENDIAN_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Return the correct conversion table for endianness
////////////////////////////////////////////////////////////
static struct EndianCallbackTableType * EndianConversionTable(EndianType From, EndianType To)
{
	uinteger FromIndex = EndiannessTypeIndex(From);
	uinteger ToIndex = EndiannessTypeIndex(To);

	if (FromIndex != ENDIAN_INDEX_INVALID && ToIndex != ENDIAN_INDEX_INVALID)
	{
		static struct EndianCallbackTableType Table[ENDIAN_INDEX_SIZE][ENDIAN_INDEX_SIZE] = {

			// From little word endian
			ArrayExprInit(ENDIAN_INDEX_LITTLE_WORD,

				// To little word endian
				ArrayExprInit(ENDIAN_INDEX_LITTLE_WORD,
					ENDIAN_NO_OP_TABLE
				),

				// To big endian
				ArrayExprInit(ENDIAN_INDEX_BIG,
					ENDIAN_BIG_LITTLE_WORD_TABLE
				),

				// To little endian
				ArrayExprInit(ENDIAN_INDEX_LITTLE,
					ENDIAN_LITTLE_LITTLE_WORD_TABLE
				),

				// To big word endian
				ArrayExprInit(ENDIAN_INDEX_BIG_WORD,
					ENDIAN_SWAP_TABLE
				)
			),

			// From big endian
			ArrayExprInit(ENDIAN_INDEX_BIG,

				// To little word endian
				ArrayExprInit(ENDIAN_INDEX_LITTLE_WORD,
					ENDIAN_BIG_LITTLE_WORD_TABLE
				),

				// To big endian
				ArrayExprInit(ENDIAN_INDEX_BIG,
					ENDIAN_NO_OP_TABLE
				),

				// To little endian
				ArrayExprInit(ENDIAN_INDEX_LITTLE,
					ENDIAN_SWAP_TABLE
				),

				// To big word endian
				ArrayExprInit(ENDIAN_INDEX_BIG_WORD,
					ENDIAN_UNIMPLEMENTED_TABLE
				)
			),

			// From little endian
			ArrayExprInit(ENDIAN_INDEX_LITTLE,

				// To little word endian
				ArrayExprInit(ENDIAN_INDEX_LITTLE_WORD,
					ENDIAN_LITTLE_LITTLE_WORD_TABLE
				),

				// To big endian
				ArrayExprInit(ENDIAN_INDEX_BIG,
					ENDIAN_SWAP_TABLE
				),

				// To little endian
				ArrayExprInit(ENDIAN_INDEX_LITTLE,
					ENDIAN_NO_OP_TABLE
				),

				// To big word endian
				ArrayExprInit(ENDIAN_INDEX_BIG_WORD,
					ENDIAN_UNIMPLEMENTED_TABLE
				)
			),

			// From big word endian
			ArrayExprInit(ENDIAN_INDEX_BIG_WORD,

				// To little word endian
				ArrayExprInit(ENDIAN_INDEX_LITTLE_WORD,
					ENDIAN_SWAP_TABLE
				),

				// To big endian
				ArrayExprInit(ENDIAN_INDEX_BIG,
					ENDIAN_UNIMPLEMENTED_TABLE
				),

				// To little endian
				ArrayExprInit(ENDIAN_INDEX_LITTLE,
					ENDIAN_UNIMPLEMENTED_TABLE
				),

				// To big word endian
				ArrayExprInit(ENDIAN_INDEX_BIG_WORD,
					ENDIAN_NO_OP_TABLE
				)
			)
		};

		return &Table[FromIndex][ToIndex];
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// 16-bit signed integer from specified endianness to host
////////////////////////////////////////////////////////////
int16 EndiannessToHostInt16(int16 Value, EndianType From)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(From, ENDIAN_TYPE);

	if (ConversionTable && ConversionTable->EndianInt16)
	{
		return ConversionTable->EndianInt16(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 16-bit unsigned integer from specified endianness to host
////////////////////////////////////////////////////////////
uint16 EndiannessToHostUInt16(uint16 Value, EndianType From)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(From, ENDIAN_TYPE);

	if (ConversionTable && ConversionTable->EndianUInt16)
	{
		return ConversionTable->EndianUInt16(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 32-bit signed integer from specified endianness to host
////////////////////////////////////////////////////////////
int32 EndiannessToHostInt32(int32 Value, EndianType From)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(From, ENDIAN_TYPE);

	if (ConversionTable && ConversionTable->EndianInt32)
	{
		return ConversionTable->EndianInt32(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 32-bit unsigned integer from specified endianness to host
////////////////////////////////////////////////////////////
uint32 EndiannessToHostUInt32(uint32 Value, EndianType From)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(From, ENDIAN_TYPE);

	if (ConversionTable && ConversionTable->EndianUInt32)
	{
		return ConversionTable->EndianUInt32(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 64-bit signed integer from specified endianness to host
////////////////////////////////////////////////////////////
int64 EndiannessToHostInt64(int64 Value, EndianType From)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(From, ENDIAN_TYPE);

	if (ConversionTable && ConversionTable->EndianInt64)
	{
		return ConversionTable->EndianInt64(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 64-bit unsigned integer from specified endianness to host
////////////////////////////////////////////////////////////
uint64 EndiannessToHostUInt64(uint64 Value, EndianType From)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(From, ENDIAN_TYPE);

	if (ConversionTable && ConversionTable->EndianUInt64)
	{
		return ConversionTable->EndianUInt64(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 32-bit floating point from specified endianness to host
////////////////////////////////////////////////////////////
float32 EndiannessToHostFloat32(float32 Value, EndianType From)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(From, ENDIAN_TYPE);

	if (ConversionTable && ConversionTable->EndianFloat32)
	{
		return ConversionTable->EndianFloat32(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 64-bit floating point from specified endianness to host
////////////////////////////////////////////////////////////
float64 EndiannessToHostFloat64(float64 Value, EndianType From)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(From, ENDIAN_TYPE);

	if (ConversionTable && ConversionTable->EndianFloat64)
	{
		return ConversionTable->EndianFloat64(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 16-bit signed integer from host to specified endianness
////////////////////////////////////////////////////////////
int16 EndiannessFromHostInt16(int16 Value, EndianType To)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(ENDIAN_TYPE, To);

	if (ConversionTable && ConversionTable->EndianInt16)
	{
		return ConversionTable->EndianInt16(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 16-bit unsigned integer from host to specified endianness
////////////////////////////////////////////////////////////
uint16 EndiannessFromHostUInt16(uint16 Value, EndianType To)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(ENDIAN_TYPE, To);

	if (ConversionTable && ConversionTable->EndianUInt16)
	{
		return ConversionTable->EndianUInt16(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 32-bit signed integer from host to specified endianness
////////////////////////////////////////////////////////////
int32 EndiannessFromHostInt32(int32 Value, EndianType To)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(ENDIAN_TYPE, To);

	if (ConversionTable && ConversionTable->EndianInt32)
	{
		return ConversionTable->EndianInt32(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 32-bit unsigned integer from host to specified endianness
////////////////////////////////////////////////////////////
uint32 EndiannessFromHostUInt32(uint32 Value, EndianType To)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(ENDIAN_TYPE, To);

	if (ConversionTable && ConversionTable->EndianUInt32)
	{
		return ConversionTable->EndianUInt32(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 64-bit signed integer from host to specified endianness
////////////////////////////////////////////////////////////
int64 EndiannessFromHostInt64(int64 Value, EndianType To)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(ENDIAN_TYPE, To);

	if (ConversionTable && ConversionTable->EndianInt64)
	{
		return ConversionTable->EndianInt64(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 64-bit unsigned integer from host to specified endianness
////////////////////////////////////////////////////////////
uint64 EndiannessFromHostUInt64(uint64 Value, EndianType To)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(ENDIAN_TYPE, To);

	if (ConversionTable && ConversionTable->EndianUInt64)
	{
		return ConversionTable->EndianUInt64(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 32-bit floating point from host to specified endianness
////////////////////////////////////////////////////////////
float32 EndiannessFromHostFloat32(float32 Value, EndianType To)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(ENDIAN_TYPE, To);

	if (ConversionTable && ConversionTable->EndianFloat32)
	{
		return ConversionTable->EndianFloat32(Value);
	}

	return Value;
}

////////////////////////////////////////////////////////////
/// 64-bit floating point from host to specified endianness
////////////////////////////////////////////////////////////
float64 EndiannessFromHostFloat64(float64 Value, EndianType To)
{
	struct EndianCallbackTableType * ConversionTable = EndianConversionTable(ENDIAN_TYPE, To);

	if (ConversionTable && ConversionTable->EndianFloat64)
	{
		return ConversionTable->EndianFloat64(Value);
	}

	return Value;
}
