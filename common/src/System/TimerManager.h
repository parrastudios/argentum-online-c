/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_TIMER_MANAGER_H
#define SYSTEM_TIMER_MANAGER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/Timer.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef void (*CallbackType)();

////////////////////////////////////////////////////////////
/// Initialize all timers
////////////////////////////////////////////////////////////
bool TimerManagerInitialize(float Delay);

////////////////////////////////////////////////////////////
/// Destroy all timers
////////////////////////////////////////////////////////////
void TimerManagerDestroy();

////////////////////////////////////////////////////////////
// Add a timer to the manager
////////////////////////////////////////////////////////////
uint32 TimerManagerAdd(float Interval, CallbackType Callback, bool Period);

////////////////////////////////////////////////////////////
// Modify the interval of a timer in the manager
////////////////////////////////////////////////////////////
void TimerManagerSet(uint32 Id, float Interval);

////////////////////////////////////////////////////////////
// Remove a timer to the manager
////////////////////////////////////////////////////////////
void TimerManagerRemove(uint32 Id);

#endif // SYSTEM_TIMER_MANAGER_H
