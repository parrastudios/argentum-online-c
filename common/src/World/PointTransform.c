/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Point.h>

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a transform component of a point
////////////////////////////////////////////////////////////
struct EntityType * PointTransformCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a point transform component
////////////////////////////////////////////////////////////
void PointTransformDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the point transform implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * PointComponentTransformImpl()
{
	static const struct ComponentImplType PointComponentTransformTypeImpl = {
		ExprInit(Base, TypeInit(Create, &PointTransformCreate), TypeInit(Destroy, &PointTransformDestroy)),
		TypeInit(Type, COMPONENT_TYPE_TRANSFORM),
		UnionInit(Attributes, Transform, &EntityComponentTransformAttr)
	};

	return &PointComponentTransformTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a transform component of a point
////////////////////////////////////////////////////////////
struct EntityType * PointTransformCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

        // ..

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a point transform component
////////////////////////////////////////////////////////////
void PointTransformDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Get transform component
		struct ComponentType * Transform = EntityComponentTransform(Entity);
		struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the point transform data size
////////////////////////////////////////////////////////////
uinteger PointComponentTransformSize()
{
    return COMPONENT_DATA_LENGTH_DEFAULT;
}
