/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <GUI/Clipboard.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
char ClipboardText[GUI_CLIPBOARD_SIZE];

////////////////////////////////////////////////////////////
/// Get the clipboard text
////////////////////////////////////////////////////////////
char * GuiClipboardGet()
{
	return ClipboardText;
}

////////////////////////////////////////////////////////////
/// Set the clipboard text
////////////////////////////////////////////////////////////
void GuiClipboardSet(char * Text, uint32 Size)
{
	if (Text)
	{
		if (strlen(Text) < GUI_CLIPBOARD_SIZE)
		{
			MemoryCopy(ClipboardText, Text, Size);

			ClipboardText[Size] = NULLCHAR;
		}
	}
}