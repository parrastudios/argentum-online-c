/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GUI_WINDOW_PARSER_H
#define GUI_WINDOW_PARSER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

///////////////////////////////////////////////////////////////////////////
// DOC: This is the user interface parser, used for read and
//		load the forms from a script ubicated in a some path.
//
//		 e.g.:
//			<form name="frmMain" width="300" height="300" onload="LoadMain">
//				<textbox length="30" text="hello world" />
//				<image width="200 height=300 />
//				<button ... />
//			</form>
//
//      By default :
//          text = ""
//          fontsize = 8
//          x = centred
//          y = centred
//
//      Note :
//			- The parser is case sensitive
//          - Do not use the characters '^', '|', these are reserved for the parser
//          - Remember, use the character ';' to create comments
//          - The function id has to be always like: "(Window)(Widget)_(EventType)"
//              e.g.: "AccConnect_ButtonPressed"
//
///////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define GUI_EVENTS_SIZE                 0xFF
#define GUI_PARSER_WINDOWS_CLIENT_PATH	"data/gui/windows.ui"
#define GUI_PARSER_WINDOWS_EDITOR_PATH	"data/gui/editor.ui"
#define GUI_PARSER_TEMPLATES_PATH       "data/gui/template%d.ui"

#define GUI_PARSER_WIDGET_START			"<"
#define GUI_PARSER_WIDGET_FINISH		"/"
#define GUI_PARSER_WIDGET_CLOSE			">"
#define GUI_PARSER_COMMENT              ";"
#define GUI_PARSER_EQUAL				"="
#define GUI_PARSER_VALUE_TAG			"\""
#define GUI_PARSER_VALUE_EVENT			"_"

#define GUI_PARAMETER_ERROR				0x00
#define GUI_PARAMETER_NAME				0x01
#define GUI_PARAMETER_TEXT				0x02
#define GUI_PARAMETER_STYLE				0x03
#define GUI_PARAMETER_X					0x04
#define GUI_PARAMETER_Y					0x05
#define GUI_PARAMETER_HEIGHT			0x06
#define GUI_PARAMETER_WIDTH				0x07
#define GUI_PARAMETER_FONTSIZE			0x08
#define GUI_PARAMETER_LENGTH			0x09
#define GUI_PARAMETER_HIDE				0x0A
#define GUI_PARAMETER_EVENT				0x0B
#define GUI_PARAMETER_ENABLED			0x0C
#define GUI_PARAMETER_VISIBLE			0x0D
#define GUI_PARAMETER_BORDER			0x0E

#define GUI_PARSER_MAP_LANG_ID			0x00
#define GUI_PARSER_MAP_CONFIG_ID		0x01
#define GUI_PARSER_MAP_COUNT			0x02

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef char * (* WindowParserMapFunc)(char * Key);

static struct WindowParserMapTagType
{
	char								Name[0x60];	//< Tag descriptor

} WindowParserMapTags[GUI_PARSER_MAP_COUNT];

struct WindowParserMapType
{
	char * Tag;
	WindowParserMapFunc MapFunc;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize the template and window functions
////////////////////////////////////////////////////////////
bool GuiParserInitialize(uinteger DefaultTemplate, char * Path);

////////////////////////////////////////////////////////////
/// Destroy all data
////////////////////////////////////////////////////////////
bool GuiParserDestroy();

////////////////////////////////////////////////////////////
/// Load a template or change it if there are one
////////////////////////////////////////////////////////////
bool GuiParserLoadTemplate(uinteger Index);

////////////////////////////////////////////////////////////
/// Register a mapping for script templating
////////////////////////////////////////////////////////////
void GuiParserRegisterMap(uinteger Index, WindowParserMapFunc MapFunction);

////////////////////////////////////////////////////////////
/// Load a template or change it if there are one
////////////////////////////////////////////////////////////
bool GuiParserLoadTemplate(uinteger Index);

////////////////////////////////////////////////////////////
/// Load all windows
////////////////////////////////////////////////////////////
bool GuiParserLoadWindows(char * Path);

#endif // GUI_WINDOW_PARSER_H
