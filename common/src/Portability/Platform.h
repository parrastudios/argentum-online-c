/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PORTABILITY_PLATFORM_H
#define PORTABILITY_PLATFORM_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Compiler.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PLATFORM_UNKNOWN			0x00
#define PLATFORM_LINUX				0x01
#define PLATFORM_FREE_BSD			0x02
#define PLATFORM_NET_BSD			0x03
#define PLATFORM_OPEN_BSD			0x04
#define PLATFORM_BSDI				0x05
#define PLATFORM_DRAGONFLY			0x06
#define PLATFORM_WINDOWS_NT			0x07
#define PLATFORM_WINDOWS_CE			0x08
#define PLATFORM_MSDOS				0x09
#define PLATFORM_MACINTOSH			0x0A
#define PLATFORM_MACOSX				0x0B
#define PLATFORM_HPUX				0x0C
#define PLATFORM_SOLARIS			0x0D
#define PLATFORM_SUNOS				0x0E
#define PLATFORM_AMIGAOS			0x0F
#define PLATFORM_HAIKU				0x10
#define PLATFORM_TRU64				0x11
#define PLATFORM_AIX				0x12
#define PLATFORM_IRIX				0x13
#define PLATFORM_HURD				0x14
#define PLATFORM_CRAY				0x15
#define PLATFORM_DGUX				0x16
#define PLATFORM_OS2				0x17
#define PLATFORM_PYRAMID			0x18
#define PLATFORM_QNX				0x19
#define PLATFORM_SCO				0x1A
#define PLATFORM_SEQUENT			0x1B
#define PLATFORM_SINIX				0x1C
#define PLATFORM_ULTRIX				0x1D
#define PLATFORM_CYGWIN				0x1E
#define PLATFORM_SYMBIAN			0x1F
#define PLATFORM_ANDROID			0x20
#define PLATFORM_IOS				0x21
#define PLATFORM_NACL				0x22
#define PLATFORM_VMS				0x23
#define PLATFORM_UNKNOWN_UNIX		0x24
#define PLATFORM_VXWORKS			0x25
#define PLATFORM_PALMOS				0x26

////////////////////////////////////////////////////////////
// Detection
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Identify the operating system
////////////////////////////////////////////////////////////
#if (defined(linux) || defined(__linux) || defined(__linux__) || defined(__gnu_linux) || defined(__gnu_linux__) || defined(__TOS_LINUX__))
#	define PLATFORM_TYPE PLATFORM_LINUX
#	define PLATFORM_FAMILY_UNIX
#elif defined(__FreeBSD__)
#	define PLATFORM_TYPE PLATFORM_FREE_BSD
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif defined(__NetBSD__)
#	define PLATFORM_TYPE PLATFORM_NET_BSD
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif defined(__OpenBSD__)
#	define PLATFORM_TYPE PLATFORM_OPEN_BSD
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif (defined(bsdi) || defined(__bsdi__))
#	define PLATFORM_TYPE PLATFORM_BSDI
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif defined(__DragonFly__)
#	define PLATFORM_TYPE PLATFORM_DRAGONFLY
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif (defined(_WIN32) || defined(__WIN32__) || defined(_WIN64))
#	define PLATFORM_TYPE PLATFORM_WINDOWS_NT
#	define PLATFORM_FAMILY_WINDOWS
#elif defined(_WIN32_WCE)
#	define PLATFORM_TYPE PLATFORM_WINDOWS_CE
#	define PLATFORM_FAMILY_WINDOWS
#elif (defined(MSDOS) || defined(__MSDOS__) || defined(_MSDOS) || defined(__DOS__))
#	define PLATFORM_TYPE PLATFORM_MSDOS
#	define PLATFORM_FAMILY_WINDOWS
#elif (defined(__MACOS__) || defined(macintosh) || defined(Macintosh) || defined(__TOS_MACOS__))
#	define PLATFORM_TYPE PLATFORM_MACINTOSH
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif (defined(__APPLE__) && defined(__MACH__)) || defined(__MACOSX__)
#	define PLATFORM_TYPE PLATFORM_MACOSX
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif (defined(hpux) || defined(_hpux) || defined(__hpux) || defined(_HPUX_SOURCE))
#	define PLATFORM_TYPE PLATFORM_HPUX
#	define PLATFORM_FAMILY_UNIX
#elif (defined(sun) || defined(__sun) || defined(__sun__))
#	if defined(__SVR4) || defined(__svr4__)
#		define PLATFORM_TYPE PLATFORM_SOLARIS
#	else
#		define PLATFORM_TYPE PLATFORM_SUNOS
#	endif
#	define PLATFORM_FAMILY_UNIX
#elif (defined(__amigaos__) || defined(AMIGA))
#	define PLATFORM_TYPE PLATFORM_AMIGAOS
#	if (COMPIPLER_TYPE == COMPILER_GCC)
#		define PLATFORM_FAMILY_UNIX
#	endif
#elif (defined(__HAIKU__) || defined(__BEOS__))
#	define PLATFORM_TYPE PLATFORM_HAIKU
#	define  PLATFORM_FAMILY_UNIX
#elif (defined(__digital__) || defined(__osf__) || defined(__osf) || (COMPILER_TYPE == COMPILER_DECC))
#	define PLATFORM_TYPE PLATFORM_TRU64
#	define PLATFORM_FAMILY_UNIX
#elif (defined(_AIX) || defined(__TOS_AIX__) || (COMPILER_TYPE == COMPILER_XLC))
#	define PLATFORM_TYPE PLATFORM_AIX
#	define PLATFORM_FAMILY_UNIX
#elif (defined(sgi) || defined(__sgi) || defined(mips) || defined(_SGI_SOURCE))
#	define PLATFORM_TYPE PLATFORM_IRIX
#	define PLATFORM_FAMILY_UNIX
#elif defined(__GNU__)
#	define PLATFORM_TYPE PLATFORM_HURD
#	define PLATFORM_FAMILY_UNIX
#elif (defined(_UNICOS) || defined(_CRAY))
#	define PLATFORM_TYPE PLATFORM_CRAY
#	define PLATFORM_FAMILY_UNIX
#elif (defined(DGUX) || defined(__DGUX__) || defined(__dgux__))
#	define PLATFORM_TYPE PLATFORM_DGUX
#	define PLATFORM_FAMILY_UNIX
#elif (defined(OS2) || defined(_OS2) || defined(__OS2__) || defined(__TOS_OS2__))
#	define PLATFORM_TYPE PLATFORM_OS2
#	define PLATFORM_FAMILY_UNIX
#elif defined(pyr)
#	define PLATFORM_TYPE PLATFORM_PYRAMID
#	define PLATFORM_FAMILY_UNIX
#elif (defined(__QNX__) || defined(__QNXNTO__))
#	define PLATFORM_TYPE PLATFORM_QNX
#	define PLATFORM_FAMILY_UNIX
#elif (defined(M_I386) || defined(M_XENIX) || defined(_SCO_C_DIALECT) || (COMPILER_TYPE == COMPILER_SCO))
#	define PLATFORM_TYPE PLATFORM_SCO
#elif (defined(_SEQUENT_) || defined(sequent))
#	define PLATFORM_TYPE PLATFORM_SEQUENT
#elfi defined(sinix)
#	define PLATFORM_TYPE PLATFORM_SINIX
#elif (defined(ultrix) || defined(__ultrix) || defined(__ultrix__))
#	define PLATFORM_TYPE PLATFORM_ULTRIX
#	define PLATFORM_FAMILY_UNIX
#elif defined(__CYGWIN__)
#	define PLATFORM_TYPE PLATFORM_CYGWIN
#	define PLATFORM_FAMILY_UNIX
#elif (defined(VMS) || defined(__VMS))
#	define PLATFORM_TYPE PLATFORM_VMS
#	define PLATFORM_FAMILY_VMS
#elif defined(__SYMBIAN32__)
#	define PLATFORM_TYPE PLATFORM_SYMBIAN
#	define PLATFORM_FAMILY_UNIX
#elif defined(__ANDROID__)
#	define PLATFORM_TYPE PLATFORM_ANDROID
#	define PLATFORM_FAMILY_UNIX
#elif defined(__APPLE_CC__)
#	if __ENVIRONMENT_IPHONE_OS_VERSION_MIN_REQUIRED__ >= 40000 || __IPHONE_OS_VERSION_MIN_REQUIRED >= 40000
#		define PLATFORM_TYPE PLATFORM_IOS
#	endif
#	define PLATFORM_FAMILY_UNIX
#	define PLATFORM_FAMILY_BSD
#elif defined(__native_client__)
#	define PLATFORM_TYPE PLATFORM_NACL
#elif defined(unix) || defined(__unix) || defined(__unix__)
#	define PLATFORM_TYPE PLATFORM_UNKNOWN_UNIX
#	define PLATFORM_FAMILY_UNIX
#elif (defined(__VXWORKS__) || defined(__vxworks) || (defined(__RTP__) || defined(__WRS_KERNEL)))
#	define PLATFORM_TYPE PLATFORM_VXWORKS
#	define PLATFORM_FAMILY_UNIX
#elif defined(__palmos__)
#	define PLATFORM_TYPE PLATFORM_PALMOS
#	define PLATFORM_FAMILY_UNIX
#else
#	error This operating system is not supported
#endif

#if !defined(PLATFORM_FAMILY_BSD) && (defined(_BSD_SOURCE) || defined(_SYSTYPE_BSD))
#	define PLATFORM_FAMILY_BSD
#endif

#if defined(__sysv__) || defined(__SVR4) || defined(__svr4__) || defined(_SVR4_SOURCE) || defined(_SYSTYPE_SVR4)
#	define LATFORM_FAMILY_SVR4
#endif

#if defined(UWIN)
#	define PLATFORM_FAMILY_UWIN
#endif

#if defined(_WINDU_SOURCE)
#	define PLATFORM_FAMILY_WINDU
#endif

#endif // PORTABILITY_PLATFORM_H
