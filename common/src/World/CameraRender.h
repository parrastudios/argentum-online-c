/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_CAMERA_RENDER_H
#define WORLD_CAMERA_RENDER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Component.h>
#include <Graphics/Color.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define CAMERA_RENDER_COLOR_DEFAULT			0, 0, 255, 255
#define CAMERA_RENDER_WIDTH_LIMIT   		10.0f
#define CAMERA_RENDER_WIDTH_DEFAULT         1.0f

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct CameraComponentRenderData
{
	struct Color4ub	Color;
	float Width;
};

struct CameraComponentRenderType
{
	//< Set camera render color
	void (*Color)(struct EntityType * Entity, struct Color4ub * Color);

	//< Set camera render line width
	void (*LineWidth)(struct EntityType * Entity, float Width);
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the camera render implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentRenderType * CameraComponentRenderAttr();

////////////////////////////////////////////////////////////
/// Return the camera render implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * CameraComponentRenderImpl();

////////////////////////////////////////////////////////////
/// Return the camera render derived data size
////////////////////////////////////////////////////////////
uinteger CameraComponentRenderSize();

#endif // WORLD_CAMERA_RENDER_H
