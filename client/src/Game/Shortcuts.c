/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Shortcuts.h>

#include <Game/Language.h>
#include <Game/User.h>

#include <GUI/WindowManager.h>
#include <GUI/EventHandler.h>

#include <Network/Protocol.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
struct GuiWindow * ShortcutsWindow;
struct GuiWidget * Belt, * Skills;
struct GuiWidget * FrontLayer, * BackLayer;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Item activated event
////////////////////////////////////////////////////////////
void SkillActivated_ButtonPressed(struct MouseButtonEvent * ButtonEvent)
{
	printf("todo\n");
}

////////////////////////////////////////////////////////////
/// Register the shortcuts
////////////////////////////////////////////////////////////
void ShortcutsRegister()
{
	// Shortcuts window
	GuiEventHandlerAdd("SkillActivated_ButtonPressed", (void*)&SkillActivated_ButtonPressed);
}

////////////////////////////////////////////////////////////
/// Initialize the shortcuts
////////////////////////////////////////////////////////////
void ShortcutsInitialize()
{

	// int32 i,j;

	ShortcutsWindow = GuiGetWindow("frmShortcuts");

    /*
	Belt = GuiWindowGetWidget(ShortcutsWindow, "beltTable");
	Skills = GuiWindowGetWidget(ShortcutsWindow, "skillsTable");

	// Create front and back layers of the shorcuts just below or above the table
	BackLayer           = GuiTableCreate(Belt->X, Belt->Y, GUI_SHORTCUT_TILES, 1, GUI_SHORTCUT_SPANX, GUI_TABLE_STYLE_SQUARE);
	FrontLayer          = GuiTableCreate(Skills->X, Skills->Y, GUI_SHORTCUT_TILES, 1, GUI_SHORTCUT_SPANX, GUI_TABLE_STYLE_SQUARE);
	BackLayer->Width	= Belt->Width;
	BackLayer->Height	= Belt->Height;
	FrontLayer->Width	= Skills->Width;
	FrontLayer->Height	= Skills->Height;

	Belt->Attributes.Table.SpanX = GUI_SHORTCUT_SPANX;
	Belt->Attributes.Table.SpanY = GUI_SHORTCUT_SPANX;
	Skills->Attributes.Table.SpanX = GUI_SHORTCUT_SPANX;
	Skills->Attributes.Table.SpanY = GUI_SHORTCUT_SPANX;
	Belt->Attributes.Table.Style = GUI_TABLE_STYLE_ROUND;
	Skills->Attributes.Table.Style = GUI_TABLE_STYLE_ROUND;

	GuiTableSetColumns(&BackLayer->Attributes.Table, 1);
	GuiTableSetColumns(&Belt->Attributes.Table,		 1);
	GuiTableSetColumns(&Skills->Attributes.Table,	 1);
	GuiTableSetColumns(&FrontLayer->Attributes.Table,1);

	GuiTableSetRows(&BackLayer->Attributes.Table,	GUI_SHORTCUT_TILES);
	GuiTableSetRows(&Belt->Attributes.Table,		GUI_SHORTCUT_TILES);
	GuiTableSetRows(&Skills->Attributes.Table,		GUI_SHORTCUT_TILES);
	GuiTableSetRows(&FrontLayer->Attributes.Table,	GUI_SHORTCUT_TILES);

	//@TODO: use user data to insert grh's
	for (i = 0; i < GUI_SHORTCUT_TILES; i++)
	{
		for (j = 0; j < 2; j++)
		{
			GuiTableSetGrh(&FrontLayer->Attributes.Table, SHORTCUTS_TILE_FRONTLAYER, i, j, false);
			GuiTableSetGrh(&BackLayer->Attributes.Table, SHORTCUTS_TILE_BACKLAYER, i, j, false);
			GuiTableSetGrh(&Belt->Attributes.Table, 23687, i, j, false);
			GuiTableSetGrh(&Skills->Attributes.Table, 23687, i, j, false);

		}

	}

	// todo
	// GuiWindowAddWidgetAtIndex(ShortcutsWindow,BackLayer,0);
	GuiWindowAddWidget(ShortcutsWindow,FrontLayer);

	BackLayer->Visible = false;
	Belt->Visible = false;
    */
}

////////////////////////////////////////////////////////////
/// Skill activated event
////////////////////////////////////////////////////////////
void ShortcutActivateSkill(uint32 Index)
{

}

////////////////////////////////////////////////////////////
/// Item activated event
////////////////////////////////////////////////////////////
void ShortcutActivateItem(uint32 Index)
{

}

////////////////////////////////////////////////////////////
/// Toggle between lists
////////////////////////////////////////////////////////////
void ToggleListVisibility()
{
	if (BackLayer->Visible)
	{
		BackLayer->Visible = false;
		Belt->Visible = false;
		FrontLayer->Visible = true;
		Skills->Visible = true;
	}
	else
	{
		BackLayer->Visible = true;
		Belt->Visible = true;
		FrontLayer->Visible = false;
		Skills->Visible = false;
	}
}

////////////////////////////////////////////////////////////
/// Initialize the shortcuts
////////////////////////////////////////////////////////////
void ShortcutsOpen()
{
	if (ShortcutsWindow->hStatus == GUI_WINDOW_CLOSED)
	{
		// Open Shortcuts
		GuiOpenWindow(ShortcutsWindow);
	}

}

////////////////////////////////////////////////////////////
/// Renders all shortcuts
////////////////////////////////////////////////////////////
void ShortcutsRender()
{
	//
}

////////////////////////////////////////////////////////////
/// Close the account
////////////////////////////////////////////////////////////
void ShortcutsClose()
{
	if (ShortcutsWindow->hStatus == GUI_WINDOW_OPENED)
	{
		// Close the window
		GuiCloseWindow(ShortcutsWindow);
	}
}
