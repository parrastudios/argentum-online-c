/*
***********************************************************************************
 IqeB_Export_FileIQM.cpp

 Export a .iqm file.
 This file is based on the 'iqm.cpp' from Salmanns 'IQM Developer Kit 2013-10-02'.

10.01.2015 RR: * First editon of this file.
06.03.2015 RR: * Renamed this file from IqeB_Export_FileIQM.cpp to
                 IqeB_Export_File_IQM_IQE.cpp and added export of IQE files.

*/

#include "IqeBrowser.h"
#include "iqm.h"
#include "util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

/************************************************************************************
 * Utils
 *
 * return:   0    OK
 *           < 0  Error
 */

static bool forcejoints = true;

struct triangle { uint vert[3]; triangle() {} triangle(uint v0, uint v1, uint v2) { vert[0] = v0; vert[1] = v1; vert[2] = v2; } };
static vector<triangle> triangles, neighbors;

struct Smesh { uint name, material; uint firstvert, numverts; uint firsttri, numtris; Smesh() : name(0), material(0), firstvert(0), numverts(0), firsttri(0), numtris(0) {} };
static vector<Smesh> meshes;

struct Sanim { uint name; uint firstframe, numframes; float fps; uint flags; Sanim() : name(0), firstframe(0), numframes(0), fps(0), flags(0) {} };
static vector<Sanim> anims;

struct joint { uint name; int parent; float pos[3], orient[4], scale[3]; joint() : name(0), parent(-1) { memset(pos, 0, sizeof(pos)); memset(orient, 0, sizeof(orient)); memset(scale, 0, sizeof(scale)); } };
static vector<joint> joints;

struct Spose { int parent; uint flags; float offset[10], scale[10]; Spose() : parent(-1), flags(0) { memset(offset, 0, sizeof(offset)); memset(scale, 0, sizeof(scale)); } };
static vector<Spose> poses;

struct framebounds { Vec3 bbmin, bbmax; double xyradius, radius; framebounds() : bbmin(0, 0, 0), bbmax(0, 0, 0), xyradius(0), radius(0) {} };
static vector<framebounds> bounds;

struct transform
{
    Vec3 pos;
    Quat orient;
    Vec3 scale;

    transform() {}
    transform(const Vec3 &pos, const Quat &orient, const Vec3 &scale = Vec3(1, 1, 1)) : pos(pos), orient(orient), scale(scale) {}
};
static vector<transform> frames;

static vector<char> stringdata, commentdata;

struct sharedstring
{
    uint offset;
    sharedstring() {}
    sharedstring(const char *s) : offset(stringdata.length()) {
      stringdata.put(s, strlen(s)+1);
    }
};

static inline bool htcmp(const char *x, const sharedstring &s)
{
    return htcmp(x, &stringdata[s.offset]);
}

static hashtable<sharedstring, uint> stringoffsets;

uint sharestring(const char *s)
{
    if(stringdata.empty()) stringoffsets.access("", 0);
    return stringoffsets.access(s ? s : "", stringdata.length());
}

struct blendcombo
{
    int sorted;
    double weights[4];
    uchar bones[4];

    blendcombo() : sorted(0) {}

    void reset() { sorted = 0; }

    void addweight(double weight, int bone)
    {
        if(weight <= 1e-3) return;
        loopk(sorted) if(weight > weights[k])
        {
            for(int l = min(sorted-1, 2); l >= k; l--)
            {
                weights[l+1] = weights[l];
                bones[l+1] = bones[l];
            }
            weights[k] = weight;
            bones[k] = bone;
            if(sorted<4) sorted++;
            return;
        }
        if(sorted>=4) return;
        weights[sorted] = weight;
        bones[sorted] = bone;
        sorted++;
    }

    void finalize()
    {
        loopj(4-sorted) { weights[sorted+j] = 0; bones[sorted+j] = 0; }
        if(sorted <= 0) return;
        double total = 0;
        loopj(sorted) total += weights[j];
        total = 1.0/total;
        loopj(sorted) weights[j] *= total;
    }

    void serialize(uchar *vweights) const
    {
        int total = 0;
        loopk(4) total += (vweights[k] = uchar(0.5 + weights[k]*255));
        if(sorted <= 0) return;
        while(total > 255)
        {
            loopk(4) if(vweights[k] > 0 && total > 255) { vweights[k]--; total--; }
        }
        while(total < 255)
        {
            loopk(4) if(vweights[k] < 255 && total < 255) { vweights[k]++; total++; }
        }
    }

    bool operator==(const blendcombo &c) { loopi(4) if(weights[i] != c.weights[i] || bones[i] != c.bones[i]) return false; return true; }
    bool operator!=(const blendcombo &c) { loopi(4) if(weights[i] != c.weights[i] || bones[i] != c.bones[i]) return true; return false; }
};

struct ejoint
{
    const char *name;
    int parent;

    ejoint() : name(NULL), parent(-1) {}
};

struct eanim
{
    const char *name;
    int startframe, endframe;
    double fps;
    uint flags;

    eanim() : name(NULL), startframe(0), endframe(INT_MAX), fps(0), flags(0) {}
};

struct emesh
{
    const char *name, *material;
    int firsttri;
    bool used;

    emesh() : name(NULL), material(NULL), firsttri(0), used(false) {}
    emesh(const char *name, const char *material, int firsttri = 0) : name(name), material(material), firsttri(firsttri), used(false) {}
};

struct evarray
{
    string name;
    int type, format, size;

    evarray() : type(IQM_POSITION), format(IQM_FLOAT), size(3) { name[0] = '\0'; }
    evarray(int type, int format, int size, const char *initname = "") : type(type), format(format), size(size) { copystring(name, initname); }
};

struct esmoothgroup
{
    enum
    {
        F_USED     = 1<<0,
        F_UVSMOOTH = 1<<1
    };

    int key;
    float angle;
    int flags;

    esmoothgroup() : key(-1), angle(-1), flags(0) {}
};

struct etriangle
{
    int smoothgroup;
    uint vert[3], weld[3];

    etriangle()
        : smoothgroup(-1)
    {
    }
    etriangle(int v0, int v1, int v2, int smoothgroup = -1)
        : smoothgroup(smoothgroup)
    {
        vert[0] = v0;
        vert[1] = v1;
        vert[2] = v2;
    }
};

static vector<Vec4> mpositions, epositions, etexcoords, etangents, ecolors;
static vector<Vec3> enormals;
static vector<blendcombo> mblends, eblends;
static vector<etriangle> etriangles;
static vector<esmoothgroup> esmoothgroups;
static vector<int> esmoothindexes;
static vector<uchar> esmoothedges;
static vector<ejoint> ejoints;
static vector<transform> eposes;
static vector<Matrix3x4> mjoints;
static vector<int> eframes;
static vector<eanim> eanims;
static vector<emesh> emeshes;
static vector<evarray> evarrays;
static hashtable<const char *, char *> enames;

static const char *getnamekey(const char *name)
{
    char **exists = enames.access(name);
    if(exists) return *exists;
    char *key = newstring(name);
    enames[key] = key;
    return key;
}

struct sharedvert
{
    int index, weld;

    sharedvert() {}
    sharedvert(int index, int weld) : index(index), weld(weld) {}
};

static inline bool htcmp(const sharedvert &v, const sharedvert &s)
{
    if(epositions[v.index] != epositions[s.index]) return false;
    if(etexcoords.length() && etexcoords[v.index] != etexcoords[s.index]) return false;
    if(enormals.length() && enormals[v.weld] != enormals[s.weld]) return false;
    if(eblends.length() && eblends[v.index] != eblends[s.index]) return false;
    if(ecolors.length() && ecolors[v.index] != ecolors[s.index]) return false;
#ifdef use_again  // 10.01.2015 RR: Not used
    loopi(10) if(ecustom[i].length() && ecustom[i][v.index] != ecustom[i][s.index]) return false;
#endif
    return true;
}

inline uint hthash(const sharedvert &v)
{
    return hthash(epositions[v.index]);
}

static const struct vertexarraytype
{
    const char *name;
    int code;
} vatypes[] =
{
    { "position", IQM_POSITION },
    { "texcoord", IQM_TEXCOORD },
    { "normal", IQM_NORMAL  },
    { "tangent", IQM_TANGENT },
    { "blendindexes", IQM_BLENDINDEXES },
    { "blendweights", IQM_BLENDWEIGHTS },
    { "color", IQM_COLOR },
    { "custom0", IQM_CUSTOM + 0 },
    { "custom1", IQM_CUSTOM + 1 },
    { "custom2", IQM_CUSTOM + 2 },
    { "custom3", IQM_CUSTOM + 3 },
    { "custom4", IQM_CUSTOM + 4 },
    { "custom5", IQM_CUSTOM + 5 },
    { "custom6", IQM_CUSTOM + 6 },
    { "custom7", IQM_CUSTOM + 7 },
    { "custom8", IQM_CUSTOM + 8 },
    { "custom9", IQM_CUSTOM + 9 }
};

static int findvertexarraytype(const char *name)
{
    loopi(sizeof(vatypes)/sizeof(vatypes[0]))
    {
        if(!strcasecmp(vatypes[i].name, name))
            return vatypes[i].code;
    }
    return -1;
}

static const struct vertexarrayformat
{
    const char *name;
    int code;
    int size;
} vaformats[] =
{
    { "byte", IQM_BYTE, 1 },
    { "ubyte", IQM_UBYTE, 1 },
    { "short", IQM_SHORT, 2 },
    { "ushort", IQM_USHORT, 2 },
    { "int", IQM_INT, 4 },
    { "uint", IQM_UINT, 4 },
    { "half", IQM_HALF, 2 },
    { "float", IQM_FLOAT, 4 },
    { "double", IQM_DOUBLE, 8 }
};

static int findvertexarrayformat(const char *name)
{
    loopi(sizeof(vaformats)/sizeof(vaformats[0]))
    {
        if(!strcasecmp(vaformats[i].name, name))
            return vaformats[i].code;
    }
    return -1;
}

struct vertexarray
{
    uint type, flags, format, size, offset;

    vertexarray(uint type, uint format, uint size, uint offset) : type(type), flags(0), format(format), size(size), offset(offset) {}

    int formatsize() const
    {
        return vaformats[format].size;
    }

    int bytesize() const
    {
        return size * vaformats[format].size;
    }
};

static vector<sharedvert> vmap;
static vector<vertexarray> varrays;
static vector<uchar> vdata;

struct halfdata
{
    ushort val;

    halfdata(double d)
    {
        union
        {
            ullong i;
            double d;
        } conv;
        conv.d = d;
        ushort signbit = ushort((conv.i>>63)&1);
        ushort mantissa = ushort((conv.i>>(52-10))&0x3FF);
        int exponent = int((conv.i>>52)&0x7FF) - 1023 + 15;
        if(exponent <= 0)
        {
            mantissa |= 0x400;
            mantissa >>= min(1-exponent, 10+1);
            exponent = 0;
        }
        else if(exponent >= 0x1F)
        {
            mantissa = 0;
            exponent = 0x1F;
        }
        val = (signbit<<15) | (ushort(exponent)<<10) | mantissa;
    }
};

template<> inline halfdata endianswap<halfdata>(halfdata n) { n.val = endianswap16(n.val); return n; }

template<int TYPE> static inline int remapindex(int i, const sharedvert &v) { return v.index; }
template<> inline int remapindex<IQM_NORMAL>(int i, const sharedvert &v) { return v.weld; }
template<> inline int remapindex<IQM_TANGENT>(int i, const sharedvert &v) { return i; }

template<class T, class U>
static inline void putattrib(T &out, const U &val) { out = T(val); }

template<class T, class U>
static inline void uroundattrib(T &out, const U &val, double scale) { out = T(clamp(0.5 + val*scale, 0.0, scale)); }
template<class T, class U>
static inline void sroundattrib(T &out, const U &val, double scale, double low, double high) { double n = val*scale*0.5; out = T(clamp(n < 0 ? ceil(n - 1) : floor(n), low, high)); }

template<class T, class U>
static inline void scaleattrib(T &out, const U &val) { putattrib(out, val); }
template<class U>
static inline void scaleattrib(char &out, const U &val) { sroundattrib(out, val, 255.0, -128.0, 127.0); }
template<class U>
static inline void scaleattrib(short &out, const U &val) { sroundattrib(out, val, 65535.0, -32768.0, 32767.0); }
template<class U>
static inline void scaleattrib(int &out, const U &val) { sroundattrib(out, val, 4294967295.0, -2147483648.0, 2147483647.0); }
template<class U>
static inline void scaleattrib(uchar &out, const U &val) { uroundattrib(out, val, 255.0); }
template<class U>
static inline void scaleattrib(ushort &out, const U &val) { uroundattrib(out, val, 65535.0); }
template<class U>
static inline void scaleattrib(uint &out, const U &val) { uroundattrib(out, val, 4294967295.0); }

template<int T>
static inline bool normalizedattrib() { return true; }

template<int TYPE, int FMT, class T, class U>
static inline void serializeattrib(const vertexarray &va, T *data, const U &attrib)
{
    if(normalizedattrib<TYPE>()) switch(va.size)
    {
    case 4: scaleattrib(data[3], attrib.w);
    case 3: scaleattrib(data[2], attrib.z);
    case 2: scaleattrib(data[1], attrib.y);
    case 1: scaleattrib(data[0], attrib.x);
    }
    else switch(va.size)
    {
    case 4: putattrib(data[3], attrib.w);
    case 3: putattrib(data[2], attrib.z);
    case 2: putattrib(data[1], attrib.y);
    case 1: putattrib(data[0], attrib.x);
    }
    lilswap(data, va.size);
}

template<int TYPE, int FMT, class T>
static inline void serializeattrib(const vertexarray &va, T *data, const Vec3 &attrib)
{
    if(normalizedattrib<TYPE>()) switch(va.size)
    {
    case 3: scaleattrib(data[2], attrib.z);
    case 2: scaleattrib(data[1], attrib.y);
    case 1: scaleattrib(data[0], attrib.x);
    }
    else switch(va.size)
    {
    case 3: putattrib(data[2], attrib.z);
    case 2: putattrib(data[1], attrib.y);
    case 1: putattrib(data[0], attrib.x);
    }
    lilswap(data, va.size);
}

template<int TYPE, int FMT, class T>
static inline void serializeattrib(const vertexarray &va, T *data, const blendcombo &blend)
{
    if(TYPE == IQM_BLENDINDEXES)
    {
        switch(va.size)
        {
        case 4: putattrib(data[3], blend.bones[3]);
        case 3: putattrib(data[2], blend.bones[2]);
        case 2: putattrib(data[1], blend.bones[1]);
        case 1: putattrib(data[0], blend.bones[0]);
        }
    }
    else if(FMT == IQM_UBYTE)
    {
        uchar weights[4];
        blend.serialize(weights);
        switch(va.size)
        {
        case 4: putattrib(data[3], weights[3]);
        case 3: putattrib(data[2], weights[2]);
        case 2: putattrib(data[1], weights[1]);
        case 1: putattrib(data[0], weights[0]);
        }
    }
    else
    {
        switch(va.size)
        {
        case 4: scaleattrib(data[3], blend.weights[3]);
        case 3: scaleattrib(data[2], blend.weights[2]);
        case 2: scaleattrib(data[1], blend.weights[1]);
        case 1: scaleattrib(data[0], blend.weights[0]);
        }
    }
    lilswap(data, va.size);
}

template<int TYPE, class T>
void setupvertexarray(const vector<T> &attribs, int type, int fmt, int size)
{
    vertexarray &va = varrays.add(vertexarray(type, fmt, size, vdata.length()));
    const char *name = "";
    loopv(evarrays) if(evarrays[i].type == (int)va.type)
    {
        evarray &info = evarrays[i];
        va.format = info.format;
        va.size = clamp(info.size, 1, 4);
        name = info.name;
        break;
    }
    uint align = max(va.formatsize(), 4);
    if(va.offset%align) { uint pad = align - va.offset%align; va.offset += pad; loopi(pad) vdata.add(0); }
    if(va.type >= IQM_CUSTOM)
    {
        if(!name[0])
        {
            defformatstring(customname, "custom%d", va.type-IQM_CUSTOM);
            va.type = IQM_CUSTOM + sharestring(customname);
        }
        else va.type = IQM_CUSTOM + sharestring(name);
    }
    int totalsize = va.bytesize() * vmap.length();
    uchar *data = vdata.reserve(totalsize);
    vdata.advance(totalsize);
    loopv(vmap)
    {
        const T &attrib = attribs[remapindex<TYPE>(i, vmap[i])];
        switch(va.format)
        {
        case IQM_BYTE: serializeattrib<TYPE, IQM_BYTE>(va, (char *)data, attrib); break;
        case IQM_UBYTE: serializeattrib<TYPE, IQM_UBYTE>(va, (uchar *)data, attrib); break;
        case IQM_SHORT: serializeattrib<TYPE, IQM_SHORT>(va, (short *)data, attrib); break;
        case IQM_USHORT: serializeattrib<TYPE, IQM_USHORT>(va, (ushort *)data, attrib); break;
        case IQM_INT: serializeattrib<TYPE, IQM_INT>(va, (int *)data, attrib); break;
        case IQM_UINT: serializeattrib<TYPE, IQM_UINT>(va, (uint *)data, attrib); break;
        case IQM_HALF: serializeattrib<TYPE, IQM_HALF>(va, (halfdata *)data, attrib); break;
        case IQM_FLOAT: serializeattrib<TYPE, IQM_FLOAT>(va, (float *)data, attrib); break;
        case IQM_DOUBLE: serializeattrib<TYPE, IQM_DOUBLE>(va, (double *)data, attrib); break;
        }
        data += va.bytesize();
    }
}

// linear speed vertex cache optimization from Tom Forsyth

#define MAXVCACHE 32

struct triangleinfo
{
    bool used;
    float score;
    uint vert[3];

    triangleinfo() {}
    triangleinfo(uint v0, uint v1, uint v2)
    {
        vert[0] = v0;
        vert[1] = v1;
        vert[2] = v2;
    }
};

struct vertexcache : listnode<vertexcache>
{
    int index, rank;
    float score;
    int numuses;
    triangleinfo **uses;

    vertexcache() : index(-1), rank(-1), score(-1.0f), numuses(0), uses(NULL) {}

    void calcscore()
    {
        if(numuses > 0)
        {
            score = 2.0f * powf(numuses, -0.5f);
            if(rank >= 3) score += powf(1.0f - (rank - 3)/float(MAXVCACHE - 3), 1.5f);
            else if(rank >= 0) score += 0.75f;
        }
        else score = -1.0f;
    }

    void removeuse(triangleinfo *t)
    {
        loopi(numuses) if(uses[i] == t)
        {
            uses[i] = uses[--numuses];
            return;
        }
    }
};

static int framesize = 0;
static vector<ushort> animdata;

#define QUANTIZE(offset, base, scale) ushort(0.5f + (float(offset) - base) / scale)

/************************************************************************************
 * resetbuffers
 *
 */

void resetbuffers()
{
    // inbut buffers

  mpositions.setsize(0);
  epositions.setsize(0);
  etexcoords.setsize(0);
  etangents.setsize(0);
  ecolors.setsize(0);
  enormals.setsize(0);
  mblends.setsize(0);
  eblends.setsize(0);
  etriangles.setsize(0);
  esmoothgroups.setsize(0);
  esmoothindexes.setsize(0);
  esmoothedges.setsize(0);
  ejoints.setsize(0);
  eposes.setsize(0);
  mjoints.setsize(0);
  eframes.setsize(0);
  eanims.setsize(0);
  emeshes.setsize(0);
  evarrays.setsize(0);
  enames.clear();
  stringoffsets.clear();

  // outbut buffers
  triangles.setsize(0);
  neighbors.setsize(0);
  meshes.setsize(0);
  anims.setsize(0);
  joints.setsize(0);
  poses.setsize(0);
  bounds.setsize(0);
  frames.setsize(0);
  stringdata.setsize(0);
  commentdata.setsize(0);

  vmap.setsize(0);
  varrays.setsize(0);
  vdata.setsize(0);

}

/************************************************************************************
 * ModelToInputBuffers
 *
 */

static void ModelToInputBuffers( IQE_model *pModel, int ExportOptionFlags)
{
	struct skel *skel;
	struct mesh *mesh;
	struct anim *anim;
	int i;
	float *pFloat;
	int   *pInt;

	skel = pModel->skel;
	mesh = pModel->mesh;

	// Add vertex things

  pFloat = mesh->position;
	for( i = 0; i < mesh->vertex_count; i++, pFloat += 3) {

	  epositions.add( Vec4( pFloat[ 0], pFloat[ 1], pFloat[ 2], 1));
  }

  if( mesh->normal != NULL) {
    pFloat = mesh->normal;

  	for( i = 0; i < mesh->vertex_count; i++, pFloat += 3) {

	    enormals.add( Vec3( pFloat[ 0], pFloat[ 1], pFloat[ 2]));
    }
  }

  if( mesh->texcoord != NULL) {
    pFloat = mesh->texcoord;

  	for( i = 0; i < mesh->vertex_count; i++, pFloat += 2) {

	    etexcoords.add( Vec4( pFloat[ 0], pFloat[ 1], 0, 0));
    }
  }

  if( mesh->color != NULL) {
    pFloat = mesh->color;

  	for( i = 0; i < mesh->vertex_count; i++, pFloat += 4) {

	    ecolors.add( Vec4( pFloat[ 0], pFloat[ 1], pFloat[ 2], pFloat[ 3]));
    }
  }

  if( mesh->blendweight != NULL && mesh->blendindex != NULL) {

    pFloat = mesh->blendweight;
    pInt   = mesh->blendindex;

  	for( i = 0; i < mesh->vertex_count; i++, pFloat += 4, pInt += 4) {

      blendcombo b;
      int j;

      for( j = 0; j < 4; j++) {

        if( pFloat[ j] > 0.0) {

          b.addweight( pFloat[ j], pInt[ j]);
        }
      }
      b.finalize();

	    eblends.add( b);
    }
  }

  // add triangles

  pInt = mesh->element;
  for( i = 0; i < mesh->element_count; i += 3, pInt += 3) {

#ifdef use_again
    etriangles.add( etriangle( pInt[ 0], pInt[ 1], pInt[ 2], -1));
#else
    // 11.01.2015 RR: Has to reverse triangle winding
    etriangles.add( etriangle( pInt[ 2], pInt[ 1], pInt[ 0], -1));
#endif
  }

  // add meshes

  for( i = 0; i < mesh->part_count; i++) {

    struct part *part;
    char *pMaterial;
    char TempMaterialString[ MAX_FILENAME_LEN + MAX_FILENAME_LEN];

    part = mesh->part + i;

    pMaterial = part->pMaterialStr;                        // default material string
    if( ExportOptionFlags & IQE_EXPORT_OPTION_COPY_TEX) {  // also copy textures
      if( part->pMaterialLoadedFile != NULL) {             // got a loaded file

        pMaterial = part->pMaterialLoadedFile;             // loaded material

        // first part of 'pMaterialLoadedFile' is same as 'pMaterialBasedir'
        // --> use a relative material reference

        if( _strnicmp( pMaterial, part->pMaterialBasedir, strlen( part->pMaterialBasedir)) == 0) { // is equal

          char *p;

          p = pMaterial + strlen( part->pMaterialBasedir);

          if( *p == '/' || *p == '\\') {   // point to path delimiter

            pMaterial = p + 1;             // material relative to path name
          }
        }
      }
    }

    if( part->pMaterialTags && part->pMaterialTags[0] != '\0') { // Have material tags

      strcpy( TempMaterialString, part->pMaterialTags);
      strcat( TempMaterialString, ";");
      strcat( TempMaterialString, pMaterial);

      pMaterial = TempMaterialString;
    }

    emesh m( getnamekey( part->pMeshName), getnamekey( pMaterial), part->first / 3);

    emeshes.add( m);
  }

  // add skeleton

  for( i = 0; i < skel->joint_count; i++) {

    ejoint &j = ejoints.add();
    j.name   = getnamekey( skel->j[ i].name);
    j.parent = skel->j[ i].parent;

	  transform t;

    t.pos = Vec3( skel->pose[ i].location[ 0], skel->pose[ i].location[ 1], skel->pose[ i].location[ 2]);
    loopk(3) t.orient[k] = skel->pose[ i].rotation[ k];
    t.orient.restorew();
    double w = skel->pose[ i].rotation[ 3];
    if(w != t.orient.w)
    {
      t.orient.w = w;
      t.orient.normalize();
    }
    if(t.orient.w > 0) t.orient.flip();
    t.scale = Vec3( skel->pose[ i].scale[ 0], skel->pose[ i].scale[ 1], skel->pose[ i].scale[ 2]);

    eposes.add(t);
  }

	// load frames

	loopl( pModel->anim_count) {

    anim = pModel->anim_data[ l];

    eanim &a = eanims.add();
    a.name       = getnamekey( anim->name);
    a.fps        = anim->framerate;
    a.flags      = 0;
    if( anim->looped) a.flags |= IQM_LOOP;
    a.startframe = eframes.length();

    loopj( anim->len) {

      struct pose *pose = anim->data[ j];

      eframes.add( eposes.length());

      loopi( pModel->anim_poses) {

    	  transform t;

        t.pos = Vec3( pose->location[ 0], pose->location[ 1], pose->location[ 2]);
        loopk(3) t.orient[k] = pose->rotation[ k];
        t.orient.restorew();
        double w = pose->rotation[ 3];
        if(w != t.orient.w)
        {
          t.orient.w = w;
          t.orient.normalize();
        }
        if(t.orient.w > 0) t.orient.flip();
        t.scale = Vec3( pose->scale[ 0], pose->scale[ 1], pose->scale[ 2]);

        eposes.add(t);

        pose++;
      }
    }
	}
}

/************************************************************************************
 * maketriangles
 *
 */

static void maketriangles(vector<triangleinfo> &tris, const vector<sharedvert> &mmap)
{
    triangleinfo **uses = new triangleinfo *[3*tris.length()];
    vertexcache *verts = new vertexcache[mmap.length()];
    list<vertexcache> vcache;

    loopv(tris)
    {
        triangleinfo &t = tris[i];
        t.used = t.vert[0] == t.vert[1] || t.vert[1] == t.vert[2] || t.vert[2] == t.vert[0];
        if(t.used) continue;
        loopk(3) verts[t.vert[k]].numuses++;
    }
    triangleinfo **curuse = uses;
    loopvrev(tris)
    {
        triangleinfo &t = tris[i];
        if(t.used) continue;
        loopk(3)
        {
            vertexcache &v = verts[t.vert[k]];
            if(!v.uses) { curuse += v.numuses; v.uses = curuse; }
            *--v.uses = &t;
        }
    }
    loopv(mmap) verts[i].calcscore();
    triangleinfo *besttri = NULL;
    float bestscore = -1e16f;
    loopv(tris)
    {
        triangleinfo &t = tris[i];
        if(t.used) continue;
        t.score = verts[t.vert[0]].score + verts[t.vert[1]].score + verts[t.vert[2]].score;
        if(t.score > bestscore) { besttri = &t; bestscore = t.score; }
    }

    //int reloads = 0, n = 0;
    while(besttri)
    {
        besttri->used = true;
        triangle &t = triangles.add();
        loopk(3)
        {
            vertexcache &v = verts[besttri->vert[k]];
            if(v.index < 0) { v.index = vmap.length(); vmap.add(mmap[besttri->vert[k]]); }
            t.vert[k] = v.index;
            v.removeuse(besttri);
            if(v.rank >= 0) vcache.remove(&v)->rank = -1;
            //else reloads++;
            if(v.numuses <= 0) continue;
            vcache.insertfirst(&v);
            v.rank = 0;
        }
        int rank = 0;
        for(vertexcache *v = vcache.first(); v != vcache.end(); v = v->next)
        {
            v->rank = rank++;
            v->calcscore();
        }
        besttri = NULL;
        bestscore = -1e16f;
        for(vertexcache *v = vcache.first(); v != vcache.end(); v = v->next)
        {
            loopi(v->numuses)
            {
                triangleinfo &t = *v->uses[i];
                t.score = verts[t.vert[0]].score + verts[t.vert[1]].score + verts[t.vert[2]].score;
                if(t.score > bestscore) { besttri = &t; bestscore = t.score; }
            }
        }
        while(vcache.size > MAXVCACHE) vcache.removelast()->rank = -1;
        if(!besttri) loopv(tris)
        {
            triangleinfo &t = tris[i];
            if(!t.used && t.score > bestscore) { besttri = &t; bestscore = t.score; }
        }
    }
    //printf("reloads: %d, worst: %d, best: %d\n", reloads, tris.length()*3, mmap.length());

    delete[] uses;
    delete[] verts;
}

/************************************************************************************
 * calctangents
 *
 */

static void calctangents(bool areaweight = true)
{
    Vec3 *tangent = new Vec3[2*vmap.length()], *bitangent = tangent+vmap.length();
    memset(tangent, 0, 2*vmap.length()*sizeof(Vec3));
    loopv(triangles)
    {
        const triangle &t = triangles[i];
        sharedvert &i0 = vmap[t.vert[0]],
                   &i1 = vmap[t.vert[1]],
                   &i2 = vmap[t.vert[2]];

        Vec3 v0(epositions[i0.index]), e1 = Vec3(epositions[i1.index]) - v0, e2 = Vec3(epositions[i2.index]) - v0;

        double u1 = etexcoords[i1.index].x - etexcoords[i0.index].x, v1 = etexcoords[i1.index].y - etexcoords[i0.index].y,
               u2 = etexcoords[i2.index].x - etexcoords[i0.index].x, v2 = etexcoords[i2.index].y - etexcoords[i0.index].y;
        Vec3 u = e2*v1 - e1*v2,
             v = e2*u1 - e1*u2;

        if(e2.cross(e1).dot(v.cross(u)) < 0)
        {
            u = -u;
            v = -v;
        }

        if(!areaweight)
        {
            u = u.normalize();
            v = v.normalize();
        }

        loopj(3)
        {
            tangent[t.vert[j]] += u;
            bitangent[t.vert[j]] += v;
        }
    }
    loopv(vmap)
    {
        const Vec3 &n = enormals[vmap[i].weld],
                   &t = tangent[i],
                   &bt = bitangent[i];
        etangents.add(Vec4((t - n*n.dot(t)).normalize(), n.cross(t).dot(bt) < 0 ? -1 : 1));
    }
    delete[] tangent;
}

/************************************************************************************
 * makemeshes
 *
 */

struct weldinfo
{
    int tri, vert;
    weldinfo *next;
};

static void weldvert(const vector<Vec3> &norms, const Vec4 &pos, weldinfo *welds, int &numwelds, unionfind<int> &welder)
{
    welder.clear();
    int windex = 0;
    for(weldinfo *w = welds; w; w = w->next, windex++)
    {
        etriangle &wt = etriangles[w->tri];
        esmoothgroup &wg = esmoothgroups[wt.smoothgroup];
        int vindex = windex + 1;
        for(weldinfo *v = w->next; v; v = v->next, vindex++)
        {
            etriangle &vt = etriangles[v->tri];
            esmoothgroup &vg = esmoothgroups[vt.smoothgroup];
            if(wg.key != vg.key) continue;
            if(norms[w->tri].dot(norms[v->tri]) < max(wg.angle, vg.angle)) continue;
            if(((wg.flags | vg.flags) & esmoothgroup::F_UVSMOOTH) &&
               etexcoords[wt.vert[w->vert]] != etexcoords[vt.vert[v->vert]])
                continue;
            if(esmoothindexes.length() > max(w->vert, v->vert) && esmoothindexes[w->vert] != esmoothindexes[v->vert])
                continue;
            if(esmoothedges.length())
            {
                int w0 = w->vert, w1 = (w->vert+1)%3, w2 = (w->vert+2)%3;
                const Vec4 &wp1 = epositions[wt.vert[w1]],
                           &wp2 = epositions[wt.vert[w2]];
                int v0 = v->vert, v1 = (v->vert+1)%3, v2 = (v->vert+2)%3;
                const Vec4 &vp1 = epositions[vt.vert[v1]],
                           &vp2 = epositions[vt.vert[v2]];
                int wf = esmoothedges[w->tri], vf = esmoothedges[v->tri];
                if((wp1 != vp1 || !(((wf>>w0)|(vf>>v0))&1)) &&
                   (wp1 != vp2 || !(((wf>>w0)|(vf>>v2))&1)) &&
                   (wp2 != vp1 || !(((wf>>w2)|(vf>>v0))&1)) &&
                   (wp2 != vp2 || !(((wf>>w2)|(vf>>v2))&1)))
                    continue;
            }
            welder.unite(windex, vindex, -1);
        }
    }
    windex = 0;
    for(weldinfo *w = welds; w; w = w->next, windex++)
    {
        etriangle &wt = etriangles[w->tri];
        wt.weld[w->vert] = welder.find(windex, -1, numwelds);
        if(wt.weld[w->vert] == uint(numwelds)) numwelds++;
    }
}

static void smoothverts(bool areaweight = true)
{
    if(etriangles.empty()) return;

    if(enormals.length())
    {
        loopv(etriangles)
        {
            etriangle &t = etriangles[i];
            loopk(3) t.weld[k] = t.vert[k];
        }
        return;
    }

    if(etexcoords.empty()) loopv(esmoothgroups) esmoothgroups[i].flags &= ~esmoothgroup::F_UVSMOOTH;
    if(esmoothedges.length()) while(esmoothedges.length() < etriangles.length()) esmoothedges.add(7);

    vector<Vec3> tarea, tnorms;
    loopv(etriangles)
    {
        etriangle &t = etriangles[i];
        Vec3 v0(epositions[t.vert[0]]),
             v1(epositions[t.vert[1]]),
             v2(epositions[t.vert[2]]);
        tnorms.add(tarea.add((v2 - v0).cross(v1 - v0)).normalize());
    }

    int nextalloc = 0;
    vector<weldinfo *> allocs;
    hashtable<Vec4, weldinfo *> welds(1<<12);

    loopv(etriangles)
    {
        etriangle &t = etriangles[i];
        loopk(3)
        {
            weldinfo **next = &welds.access(epositions[t.vert[k]], NULL);
            if(! (nextalloc % 1024)) allocs.add(new weldinfo[1024]);
            weldinfo &w = allocs[nextalloc/1024][nextalloc%1024];
            nextalloc++;
            w.tri = i;
            w.vert = k;
            w.next = *next;
            *next = &w;
        }
    }

    int numwelds = 0;
    unionfind<int> welder;
    enumerate(welds, Vec4, vpos, weldinfo *, vwelds, weldvert(tnorms, vpos, vwelds, numwelds, welder));

    loopv(allocs) delete[] allocs[i];

    loopi(numwelds) enormals.add(Vec3(0, 0, 0));
    loopv(etriangles)
    {
        etriangle &t = etriangles[i];
        loopk(3) enormals[t.weld[k]]+= areaweight ? tarea[i] : tnorms[i];
    }
    loopv(enormals) if(enormals[i] != Vec3(0, 0, 0)) enormals[i] = enormals[i].normalize();
}

/************************************************************************************
 * makeneighbors
 *
 */

struct neighborkey
{
    uint e0, e1;

    neighborkey() {}
    neighborkey(uint i0, uint i1)
    {
        if(epositions[i0] < epositions[i1]) { e0 = i0; e1 = i1; }
        else { e0 = i1; e1 = i0; }
    }

    uint hash() const { return hthash(epositions[e0]) + hthash(epositions[e1]); }
    bool operator==(const neighborkey &n) const
    {
        return epositions[e0] == epositions[n.e0] && epositions[e1] == epositions[n.e1] &&
               (eblends.empty() || (eblends[e0] == eblends[n.e0] && eblends[e1] == eblends[n.e1]));
    }
};

static inline uint hthash(const neighborkey &n) { return n.hash(); }
static inline bool htcmp(const neighborkey &x, const neighborkey &y) { return x == y; }

struct neighborval
{
    uint tris[2];

    neighborval() {}
    neighborval(uint i) { tris[0] = i; tris[1] = 0xFFFFFFFFU; }

    void add(uint i)
    {
        if(tris[1] != 0xFFFFFFFFU) tris[0] = tris[1] = 0xFFFFFFFFU;
        else if(tris[0] != 0xFFFFFFFFU) tris[1] = i;
    }

    int opposite(uint i) const
    {
        return tris[0] == i ? tris[1] : tris[0];
    }
};

static void makeneighbors()
{
    hashtable<neighborkey, neighborval> nhash;

    loopv(triangles)
    {
        triangle &t = triangles[i];
        for(int j = 0, p = 2; j < 3; p = j, j++)
        {
            neighborkey key(t.vert[p], t.vert[j]);
            neighborval *val = nhash.access(key);
            if(val) val->add(i);
            else nhash[key] = neighborval(i);
        }
    }

    loopv(triangles)
    {
        triangle &t = triangles[i];
        triangle &n = neighbors.add();
        for(int j = 0, p = 2; j < 3; p = j, j++)
            n.vert[p] = nhash[neighborkey(t.vert[p], t.vert[j])].opposite(i);
    }
}

/************************************************************************************
 * makemeshes
 *
 */

static void makemeshes( int ExportOptionFlags)
{
#ifdef use_again // already done in resetbuffers()
    meshes.setsize(0);
    triangles.setsize(0);
    neighbors.setsize(0);
    vmap.setsize(0);
    varrays.setsize(0);
    vdata.setsize(0);
#endif

    hashtable<sharedvert, uint> mshare(1<<12);
    vector<sharedvert> mmap;
    vector<triangleinfo> tinfo;

    loopv(emeshes)
    {
        emesh &em1 = emeshes[i];

        if(em1.used) continue;
        for(int j = i; j < emeshes.length(); j++)
        {
            emesh &em = emeshes[j];
            if(em.name != em1.name || em.material != em1.material) continue;
            int lasttri = emeshes.inrange(i+1) ? emeshes[i+1].firsttri : etriangles.length();
            for(int k = em.firsttri; k < lasttri; k++)
            {
                etriangle &et = etriangles[k];
                triangleinfo &t = tinfo.add();
                loopl(3)
                {
                    sharedvert v(et.vert[l], et.weld[l]);
                    t.vert[l] = mshare.access(v, mmap.length());
                    if(!mmap.inrange(t.vert[l])) mmap.add(v);
                }
            }
            em.used = true;

            // 10.01.2015 RR: Don't optimize meshes, break after first
            break;
        }
        if(tinfo.empty()) continue;

        Smesh &m = meshes.add();
        m.name = sharestring(em1.name);
        m.material = sharestring(em1.material);
        m.firsttri = triangles.length();
        m.firstvert = vmap.length();
        maketriangles(tinfo, mmap);
        m.numtris = triangles.length() - m.firsttri;
        m.numverts = vmap.length() - m.firstvert;

        mshare.clear();
        mmap.setsize(0);
        tinfo.setsize(0);
    }

    if( ExportOptionFlags & IQE_EXPORT_OPTION_ADJACENCY) {  // Generate triangle adjacency data

      if(triangles.length()) makeneighbors();
    }

#ifdef use_again  // 10.01.2015 RR: Not used
    if(escale != 1) loopv(epositions) epositions[i] *= escale;
    if(erotate != Quat(0, 0, 0, 1))
    {
        loopv(epositions) epositions[i].setxyz(erotate.transform(Vec3(epositions[i])));
        loopv(enormals) enormals[i] = erotate.transform(enormals[i]);
        loopv(etangents) etangents[i].setxyz(erotate.transform(Vec3(etangents[i])));
        loopv(ebitangents) ebitangents[i] = erotate.transform(ebitangents[i]);
    }
    if(emeshtrans != Vec3(0, 0, 0)) loopv(epositions) epositions[i] += emeshtrans;
#endif

    if(epositions.length()) setupvertexarray<IQM_POSITION>(epositions, IQM_POSITION, IQM_FLOAT, 3);
    if(etexcoords.length()) setupvertexarray<IQM_TEXCOORD>(etexcoords, IQM_TEXCOORD, IQM_FLOAT, 2);
    if(enormals.length()) setupvertexarray<IQM_NORMAL>(enormals, IQM_NORMAL, IQM_FLOAT, 3);
#ifdef use_again  // 10.01.2015 RR: Not used
    if(etangents.length())
    {
        if(ebitangents.length() && enormals.length())
        {
            loopv(etangents) if(ebitangents.inrange(i) && enormals.inrange(i))
                etangents[i].w = enormals[i].cross(Vec3(etangents[i])).dot(ebitangents[i]) < 0 ? -1 : 1;
        }
        setupvertexarray<IQM_TANGENT>(etangents, IQM_TANGENT, IQM_FLOAT, 4);
    } else
#endif
    if(enormals.length() && etexcoords.length())
    {
      if( ExportOptionFlags & IQE_EXPORT_OPTION_TANGENT) {  // Generate texture tangent data
        calctangents();
        setupvertexarray<IQM_TANGENT>(etangents, IQM_TANGENT, IQM_FLOAT, 4);
      }
    }
    if(eblends.length())
    {
        setupvertexarray<IQM_BLENDINDEXES>(eblends, IQM_BLENDINDEXES, IQM_UBYTE, 4);
        setupvertexarray<IQM_BLENDWEIGHTS>(eblends, IQM_BLENDWEIGHTS, IQM_UBYTE, 4);
    }
    if(ecolors.length()) setupvertexarray<IQM_COLOR>(ecolors, IQM_COLOR, IQM_UBYTE, 4);
#ifdef use_again  // 10.01.2015 RR: Not used
    loopi(10) if(ecustom[i].length()) setupvertexarray<IQM_CUSTOM>(ecustom[i], IQM_CUSTOM + i, IQM_FLOAT, 4);
#endif

    if(epositions.length())
    {
        mpositions.setsize(0);
        mpositions.swap(epositions);
    }
    if(eblends.length())
    {
        mblends.setsize(0);
        mblends.swap(eblends);
    }
}

/************************************************************************************
 * makebounds
 *
 */

static void makebounds(framebounds &bb, Matrix3x4 *buf, Matrix3x4 *invbase, transform *frame)
{
    loopv(ejoints)
    {
        ejoint &j = ejoints[i];
        if(j.parent >= 0) buf[i] = buf[j.parent] * Matrix3x4(frame[i].orient, frame[i].pos, frame[i].scale);
        else buf[i] = Matrix3x4(frame[i].orient, frame[i].pos, frame[i].scale);
    }
    loopv(ejoints) buf[i] *= invbase[i];
    loopv(mpositions)
    {
        const blendcombo &c = mblends[i];
        Matrix3x4 m(Vec4(0, 0, 0, 0), Vec4(0, 0, 0, 0), Vec4(0, 0, 0, 0));
        loopk(4) if(c.weights[k] > 0)
            m += buf[c.bones[k]] * c.weights[k];
        Vec3 p = m.transform(Vec3(mpositions[i]));

        if(!i) bb.bbmin = bb.bbmax = p;
        else
        {
            bb.bbmin.x = min(bb.bbmin.x, p.x);
            bb.bbmin.y = min(bb.bbmin.y, p.y);
            bb.bbmin.z = min(bb.bbmin.z, p.z);
            bb.bbmax.x = max(bb.bbmax.x, p.x);
            bb.bbmax.y = max(bb.bbmax.y, p.y);
            bb.bbmax.z = max(bb.bbmax.z, p.z);
        }
        double xyradius = p.x*p.x + p.y*p.y;
        bb.xyradius = max(bb.xyradius, xyradius);
        bb.radius = max(bb.radius, xyradius + p.z*p.z);
    }
    if(bb.xyradius > 0) bb.xyradius = sqrt(bb.xyradius);
    if(bb.radius > 0) bb.radius = sqrt(bb.radius);
}

/************************************************************************************
 * makeanims
 *
 */

static void makeanims(int ExportOptionFlags)
{
#ifdef use_again  // 10.01.2015 RR: Not used
    if(escale != 1) loopv(eposes) eposes[i].pos *= escale;
    if(erotate != Quat(0, 0, 0, 1))
    {
        for(int i = 0; i < eposes.length(); i += ejoints.length())
        {
            transform &p = eposes[i];
            p.orient = erotate * p.orient;
            p.pos = erotate.transform(p.pos);
        }
    }
#endif

    // Joints
    if (ExportOptionFlags & IQE_EXPORT_OPTION_MODEL || ExportOptionFlags & IQE_EXPORT_OPTION_POSES)
    {
        int numbasejoints = eframes.length() ? eframes[0] : eposes.length();
        if((forcejoints || meshes.length()) && joints.empty())
        {
            mjoints.setsize(0);
            loopv(ejoints)
            {
                ejoint &ej = ejoints[i];
                joint &j = joints.add();
                j.name = sharestring(ej.name);
                j.parent = ej.parent;
                if(i < numbasejoints)
                {
                    mjoints.add().invert(Matrix3x4(eposes[i].orient, eposes[i].pos, eposes[i].scale));
                    loopk(3) j.pos[k] = eposes[i].pos[k];
                    loopk(4) j.orient[k] = eposes[i].orient[k];
                    loopk(3) j.scale[k] = eposes[i].scale[k];
                }
                else mjoints.add().invert(Matrix3x4(Quat(0, 0, 0, 1), Vec3(0, 0, 0), Vec3(1, 1, 1)));
                if(ej.parent >= 0) mjoints[i] *= mjoints[ej.parent];
            }
        }
    }

    // Poses
    if (ExportOptionFlags & IQE_EXPORT_OPTION_POSES)
    {
        if(poses.empty()) loopv(ejoints)
        {
            ejoint &ej = ejoints[i];
            Spose &p = poses.add();
            p.parent = ej.parent;
        }
        if(eanims.empty()) return;
        if(poses.empty()) return;
        int totalframes = frames.length()/poses.length();
        Matrix3x4 *mbuf = mpositions.length() && mblends.length() && mjoints.length() ? new Matrix3x4[poses.length()] : NULL;
        loopv(eanims)
        {
            eanim &ea = eanims[i];
            Sanim &a = anims.add();
            a.name = sharestring(ea.name);
            a.firstframe = totalframes;
            a.numframes = 0;
            a.fps = ea.fps;
            a.flags = ea.flags;
            for(int j = ea.startframe, end = eanims.inrange(i+1) ? eanims[i+1].startframe : eframes.length(); j < end && j <= ea.endframe; j++)
            {
                int offset = eframes[j], range = (eframes.inrange(j+1) ? eframes[j+1] : eposes.length()) - offset;
                if(range <= 0) continue;
                loopk(min(range, poses.length())) frames.add(eposes[offset + k]);
                loopk(max(poses.length() - range, 0)) frames.add(transform(Vec3(0, 0, 0), Quat(0, 0, 0, 1), Vec3(1, 1, 1)));
                if(mbuf) makebounds(bounds.add(), mbuf, mjoints.getbuf(), &frames[frames.length() - poses.length()]);
                a.numframes++;
            }
            totalframes += a.numframes;
        }
        if(mbuf) delete[] mbuf;
    }
}

/************************************************************************************
 * IqmDataPrepare
 *
 */

static void IqmDataPrepare( IQE_model *pModel, int ExportOptionFlags)
{

  resetbuffers();                          // reset input and output buffers

  ModelToInputBuffers( pModel, ExportOptionFlags);   // transfer model to input buffers

  if (ExportOptionFlags & IQE_EXPORT_OPTION_MODEL)
  {
      if( emeshes.length())                    // have meshes
      {
        smoothverts();                         // smooth things
        makemeshes( ExportOptionFlags);        // prepare output data
      }
  }

  makeanims(ExportOptionFlags);

  // animations

  if (ExportOptionFlags & IQE_EXPORT_OPTION_POSES)
  {
      framesize = 0;
      animdata.setsize(0);

        if(frames.length()) loopv(poses)
        {
            Spose &j = poses[i];
            loopk(10) { j.offset[k] = 1e16f; j.scale[k] = -1e16f; }
        }
        loopv(frames)
        {
            Spose &j = poses[i%poses.length()];
            transform &f = frames[i];
            loopk(3)
            {
                j.offset[k] = min(j.offset[k], float(f.pos[k]));
                j.scale[k] = max(j.scale[k], float(f.pos[k]));
            }
            loopk(4)
            {
                j.offset[3+k] = min(j.offset[3+k], float(f.orient[k]));
                j.scale[3+k] = max(j.scale[3+k], float(f.orient[k]));
            }
            loopk(3)
            {
                j.offset[7+k] = min(j.offset[7+k], float(f.scale[k]));
                j.scale[7+k] = max(j.scale[7+k], float(f.scale[k]));
            }
        }
        loopv(poses)
        {
            Spose &j = poses[i];
            loopk(10)
            {
                j.scale[k] -= j.offset[k];
                if(j.scale[k] >= 1e-10f) { framesize++; j.scale[k] /= 0xFFFF; j.flags |= 1<<k; }
                else j.scale[k] = 0.0f;
            }
        }

        loopv(frames)
        {
            Spose &j = poses[i%poses.length()];
            transform &f = frames[i];
            loopk(3) if(j.flags & (0x01<<k)) animdata.add(QUANTIZE(f.pos[k], j.offset[k], j.scale[k]));
            loopk(4) if(j.flags & (0x08<<k)) animdata.add(QUANTIZE(f.orient[k], j.offset[3+k], j.scale[3+k]));
            loopk(3) if(j.flags & (0x80<<k)) animdata.add(QUANTIZE(f.scale[k], j.offset[7+k], j.scale[7+k]));
        }
  }

    while(vdata.length()%4) vdata.add(0);
    while(stringdata.length()%4) stringdata.add('\0');
    while(commentdata.length()%4) commentdata.add('\0');
    while(animdata.length()%2) animdata.add(0);
}

#define EPSILON 0.00001
#define NEAR_0(x) (fabs((x)) < EPSILON)
#define NEAR_1(x) (NEAR_0((x)-1))
#define KILL_0(x) (NEAR_0((x)) ? 0 : (x))
#define KILL_N(x,n) (NEAR_0((x)-(n)) ? (n) : (x))
#define KILL(x) KILL_0(KILL_N(KILL_N(x, 1), -1))

static void IQE_printpose( stream *f, struct pose *p)
{
	if (KILL(p->scale[0]) == 1 && KILL(p->scale[1]) == 1 && KILL(p->scale[2]) == 1) {

		f->printf("pq %.9g %.9g %.9g %.9g %.9g %.9g %.9g\n",
			KILL(p->location[0]), KILL(p->location[1]), KILL(p->location[2]),
			p->rotation[0], p->rotation[1], p->rotation[2], p->rotation[3]);
	} else {

		f->printf("pq %.9g %.9g %.9g %.9g %.9g %.9g %.9g %.9g %.9g %.9g\n",
			KILL(p->location[0]), KILL(p->location[1]), KILL(p->location[2]),
			p->rotation[0], p->rotation[1], p->rotation[2], p->rotation[3],
			KILL(p->scale[0]), KILL(p->scale[1]), KILL(p->scale[2]));
  }
}

/************************************************************************************
 * IqeB_Export_File_IQM_IQE()
 *
 * filename  File to write, is a fill filename (with path)
 * pDstDir   Destination direcory
 *
 * return:   0    OK
 *           < 0  Error
 */

int IqeB_Export_File_IQM_IQE( IQE_model *pModel, char *filename, char *pDstDir,
                              int FileType, int ExportOptionFlags)
{
  uint valign = 0, voffset = 0;
  stream *f;

  // Enshure file path delimiters are normalized

  IqeB_FileNormalizePathChars( filename); // Normalize path slasches
  IqeB_FileNormalizePathChars( pDstDir);  // Normalize path slasches

  //
  // Have to export IQE file
  //

  if( FileType == IQE_EXPORT_FILE_TYPE_IQE) {   // Export IQE file

    // open file

    f = openfile( filename, "wt");
    if(!f) {

      return( -1);
    }

    // ...

    f->printf( "%s\n", IQE_FILE_MAGIC);
    f->printf("#\n");
    f->printf("# This file was createed by the application:\n");
    f->printf("#    %s / %s\n", WIN_PROG_NAME, WIN_PROG_VERSION);
    f->printf("#\n");
    f->printf("\n");

    if( pModel->skel != NULL && pModel->skel->joint_count) {  // have a skeleton

      f->printf("# Skeleton\n");
      f->printf("\n");

      loopk( pModel->skel->joint_count) {

        f->printf("joint %s %d\n", pModel->skel->j[ k].name, pModel->skel->j[ k].parent);
      }
      f->printf("\n");

      loopk( pModel->skel->joint_count) {

        IQE_printpose( f, pModel->skel->pose + k);
      }
      f->printf("\n");
    }

    if( pModel->mesh != NULL &&            // have a mesh and vertex data
        pModel->mesh->part_count > 0 &&
        pModel->mesh->vertex_count > 0) {

      loopk( pModel->mesh->part_count) {

        struct part *part;
        char *pMaterial;
        char TempMaterialString[ MAX_FILENAME_LEN + MAX_FILENAME_LEN];

        part = pModel->mesh->part + k;

        pMaterial = part->pMaterialStr;                        // default material string
        if( ExportOptionFlags & IQE_EXPORT_OPTION_COPY_TEX) {  // also copy textures
          if( part->pMaterialLoadedFile != NULL) {             // got a loaded file

            pMaterial = part->pMaterialLoadedFile;             // loaded material

            // first part of 'pMaterialLoadedFile' is same as 'pMaterialBasedir'
            // --> use a relative material reference

            if( _strnicmp( pMaterial, part->pMaterialBasedir, strlen( part->pMaterialBasedir)) == 0) { // is equal

              char *p;

              p = pMaterial + strlen( part->pMaterialBasedir);

              if( *p == '/' || *p == '\\') {   // point to path delimiter

                pMaterial = p + 1;             // material relative to path name
              }
            }
          }
        }

        if( part->pMaterialTags && part->pMaterialTags[0] != '\0') { // Have material tags

          strcpy( TempMaterialString, part->pMaterialTags);
          strcat( TempMaterialString, ";");
          strcat( TempMaterialString, pMaterial);

          pMaterial = TempMaterialString;
        }

        f->printf("# %d. Mesh\n", k + 1);
        f->printf("\n");

        f->printf("mesh %s\n", part->pMeshName);
        f->printf("material %s\n", pMaterial);
        f->printf("\n");

        // get vertex range of the triangles

	      int first_vert, last_vert;
        int *pPartElement;

        first_vert = 0;    // preset for 'part->count' is zero
        last_vert  = -1;

        pPartElement = pModel->mesh->element + part->first;

        for( int i = 0; i < part->count; i += 3, pPartElement += 3) {

          if( i == 0) {

            first_vert = pPartElement[ 0];
            last_vert  = pPartElement[ 0];
          } else {

            if( first_vert > pPartElement[ 0]) first_vert = pPartElement[ 0];
            if( last_vert  < pPartElement[ 0]) last_vert  = pPartElement[ 0];
          }

          if( first_vert > pPartElement[ 1]) first_vert = pPartElement[ 1];
          if( last_vert  < pPartElement[ 1]) last_vert  = pPartElement[ 1];

          if( first_vert > pPartElement[ 2]) first_vert = pPartElement[ 2];
          if( last_vert  < pPartElement[ 2]) last_vert  = pPartElement[ 2];
        }

	      // output the vertex data

        if( pModel->mesh->position != NULL) {

          for( int i = first_vert; i <= last_vert; i++) {

            f->printf("vp %.9g %.9g %.9g\n", pModel->mesh->position[i*3+0], pModel->mesh->position[i*3+1], pModel->mesh->position[i*3+2]);
          }
          f->printf("\n");
        }

        if( pModel->mesh->texcoord != NULL) {

          for( int i = first_vert; i <= last_vert; i++) {

            f->printf("vt %.9g %.9g\n", pModel->mesh->texcoord[i*2+0], pModel->mesh->texcoord[i*2+1]);
          }
          f->printf("\n");
        }

        if( pModel->mesh->normal != NULL) {

          for( int i = first_vert; i <= last_vert; i++) {

            f->printf("vn %.9g %.9g %.9g\n", pModel->mesh->normal[i*3+0], pModel->mesh->normal[i*3+1], pModel->mesh->normal[i*3+2]);
          }
          f->printf("\n");
        }

        if( pModel->mesh->color != NULL) {

          for( int i = first_vert; i <= last_vert; i++) {

  				  f->printf("vc %.9g %.9g %.9g %.9g\n", pModel->mesh->color[i*4+0], pModel->mesh->color[i*4+1], pModel->mesh->color[i*4+2], pModel->mesh->color[i*4+3]);
          }
          f->printf("\n");
        }

        if( pModel->mesh->blendweight != NULL && pModel->mesh->blendindex != NULL) {

          for( int i = first_vert; i <= last_vert; i++) {

            f->printf("vb");
				    for( int x = 0; x < 4; x++) {
					    if( pModel->mesh->blendweight[i*4+x] > 0.0) {
						    f->printf(" %d %.9g", pModel->mesh->blendindex[i*4+x], pModel->mesh->blendweight[i*4+x]);
					    }
				    }
            f->printf("\n");
          }
          f->printf("\n");
        }

	      // output triangles

        pPartElement = pModel->mesh->element + part->first;

        for( int i = 0; i < part->count; i += 3, pPartElement += 3) {
           f->printf("fm %d %d %d\n",
                   pPartElement[ 2] - first_vert,       // NOTE: Triangle winding is reversed
                   pPartElement[ 1] - first_vert,
                   pPartElement[ 0] - first_vert);
        }
        f->printf("\n");
      }
    }

    if( pModel->anim_count > 0 && pModel->anim_poses > 0 && pModel->anim_data != NULL) {

      f->printf("# Animation\n");
      f->printf("\n");

      loopk( pModel->anim_count) {

        f->printf("animation %s\n", pModel->anim_data[k]->name);
        f->printf("framerate %.2f\n", pModel->anim_data[k]->framerate);
        if( pModel->anim_data[k]->looped) {
          f->printf("loop\n");
        }
        f->printf("\n");

        loopi( pModel->anim_data[k]->len) {

		      f->printf("frame %d\n", i);
          loopj( pModel->anim_poses) {

            IQE_printpose( f, pModel->anim_data[k]->data[i] + j );
          }
          f->printf("\n");
        }
      }
    }

    f->printf("# End Of File\n");

    goto CommonFileWriteDone;
  }

  // Fall into IQM file export

  // open file

  f = openfile( filename, "wb");
  if(!f) {

    return( -1);
  }

  //
  // Export IQM file
  //

  // setup

  IqmDataPrepare( pModel, ExportOptionFlags);

  //---  write iqm file

  iqmheader hdr;
  memset( &hdr, 0, sizeof(hdr));
  copystring( hdr.magic, IQM_MAGIC, sizeof(hdr.magic));
  hdr.filesize = sizeof(hdr);
  hdr.version = IQM_VERSION;

  if( stringdata.length()) {
    hdr.ofs_text = hdr.filesize;
  }
  hdr.num_text = stringdata.length();
  hdr.filesize += hdr.num_text;

  hdr.num_meshes = meshes.length();
  if( meshes.length()) {
    hdr.ofs_meshes = hdr.filesize;
  }
  hdr.filesize += meshes.length() * sizeof( Smesh);

  voffset = hdr.filesize + varrays.length() * sizeof(vertexarray);
  hdr.num_vertexarrays = varrays.length();
  if(varrays.length()) {
    hdr.ofs_vertexarrays = hdr.filesize;
  }
  hdr.filesize += varrays.length() * sizeof(vertexarray);

  valign = (8 - (hdr.filesize%8))%8;
  voffset += valign;
  hdr.filesize += valign + vdata.length();
  hdr.num_vertexes = vmap.length();

  hdr.num_triangles = triangles.length();
  if(triangles.length()) {
    hdr.ofs_triangles = hdr.filesize;
  }
  hdr.filesize += triangles.length() * sizeof(triangle);

  if(neighbors.length()) {
    hdr.ofs_adjacency = hdr.filesize;
  }
  hdr.filesize += neighbors.length() * sizeof(triangle);

  hdr.num_joints = joints.length();
  if(joints.length()) {
    hdr.ofs_joints = hdr.filesize;
  }
  hdr.filesize += joints.length() * sizeof(joint);

  hdr.num_poses = poses.length();
  if(poses.length()) {
    hdr.ofs_poses = hdr.filesize;
  }
  hdr.filesize += poses.length() * sizeof(Spose);

  hdr.num_anims = anims.length();
  if(anims.length()) {
    hdr.ofs_anims = hdr.filesize;
  }
  hdr.filesize += anims.length() * sizeof(Sanim);

  hdr.num_frames = poses.length() ? frames.length()/poses.length() : 0;
  hdr.num_framechannels = framesize;
  if(animdata.length()) {
    hdr.ofs_frames = hdr.filesize;
  }
  hdr.filesize += animdata.length() * sizeof(ushort);

  if(bounds.length()) {
    hdr.ofs_bounds = hdr.filesize;
  }
  hdr.filesize += bounds.length() * sizeof(float[8]);

  if(commentdata.length()) {
    hdr.ofs_comment = hdr.filesize;
  }
  hdr.num_comment = commentdata.length();
  hdr.filesize += hdr.num_comment;

  lilswap(&hdr.version, (sizeof(hdr) - sizeof(hdr.magic))/sizeof(uint));
  f->write(&hdr, sizeof(hdr));

  if(stringdata.length()) {
    f->write(stringdata.getbuf(), stringdata.length());
  }

  loopv(meshes)
  {
    Smesh &m = meshes[i];
    f->putlil(m.name);
    f->putlil(m.material);
    f->putlil(m.firstvert);
    f->putlil(m.numverts);
    f->putlil(m.firsttri);
    f->putlil(m.numtris);
  }

  loopv(varrays)
  {
    vertexarray &v = varrays[i];
    f->putlil(v.type);
    f->putlil(v.flags);
    f->putlil(v.format);
    f->putlil(v.size);
    f->putlil(voffset + v.offset);
  }

  loopi(valign) f->putchar(0);
  f->write(vdata.getbuf(), vdata.length());

  loopv(triangles)
  {
    triangle &t = triangles[i];
    loopk(3) f->putlil(t.vert[k]);
  }

  loopv(neighbors)
  {
    triangle &t = neighbors[i];
    loopk(3) f->putlil(t.vert[k]);
  }

  loopv(joints)
  {
    joint &j = joints[i];
    f->putlil(j.name);
    f->putlil(j.parent);
    loopk(3) f->putlil(float(j.pos[k]));
    loopk(4) f->putlil(float(j.orient[k]));
    loopk(3) f->putlil(float(j.scale[k]));
  }

  loopv(poses)
  {
    Spose &p = poses[i];
    f->putlil(p.parent);
    f->putlil(p.flags);
    loopk(10) f->putlil(p.offset[k]);
    loopk(10) f->putlil(p.scale[k]);
  }

  loopv(anims)
  {
    Sanim &a = anims[i];
    f->putlil(a.name);
    f->putlil(a.firstframe);
    f->putlil(a.numframes);
    f->putlil(a.fps);
    f->putlil(a.flags);
  }

  loopv(animdata) f->putlil(animdata[i]);

  loopv(bounds)
  {
    framebounds &b = bounds[i];
    loopk(3) f->putlil(float(b.bbmin[k]));
    loopk(3) f->putlil(float(b.bbmax[k]));
    f->putlil(float(b.xyradius));
    f->putlil(float(b.radius));
  }

  if(commentdata.length()) f->write(commentdata.getbuf(), commentdata.length());

  //
  // finish up file export
  //

CommonFileWriteDone:;

  delete f;

  // reset the temporary output buffers

  resetbuffers();

  // also copy texture files

  if( ExportOptionFlags & IQE_EXPORT_OPTION_COPY_TEX) {

    IqeB_TextureCopyLoadedImageFiles( pModel, pDstDir);
  }

  // ...

  return( 0);   // return OK
}

/****************************** End Of File ******************************/
