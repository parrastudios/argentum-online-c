/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PORTABILITY_ASSERTION_H
#define PORTABILITY_ASSERTION_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Debug.h>

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Define a custom assert macro
////////////////////////////////////////////////////////////
#if COMPILE_TYPE == DEBUG_MODE

	// Compile time assert
#	ifndef cassert


#		define cassert_paste_impl(a, b) a ## b
#		define cassert_paste(a, b) cassert_paste_impl(a, b)

#		define cassert_expr(predicate, expr) \
			typedef char expr[2 * !!(predicate) - 1]

		// File paste must be done inline to avoid preprocessor errors
#		define cassert_context(predicate, line, file) cassert_expr((predicate), cassert_paste(assertion_failed_ ## file, line))

#		ifdef __FILE__
#			define cassert(predicate) cassert_context((predicate), cassert_paste(_, __LINE__), __FILE__)
#		elif defined(__FUNCTION__)
#			define cassert(predicate) cassert_context((predicate), cassert_paste(_, __LINE__), __FUNCTION__)
#		elif defined(__func__)
#			define cassert(predicate) cassert_context((predicate), cassert_paste(_, __LINE__), __func__)
#		endif

#	endif

	// Run time assert
#	ifndef assert
#		define assert(predicate) // todo
#	endif

#else

#	ifndef cassert
#		define cassert(predicate)
#	endif

#	ifndef assert
#		define assert(predicate)
#	endif
#endif

#endif // PORTABILITY_ASSERTION_H
