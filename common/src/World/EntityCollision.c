/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/EntityCollision.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Check collision of a bounding box component
////////////////////////////////////////////////////////////
bool ComponentCollideBoxImpl(struct ComponentType * Collider, struct ComponentType * Target);

////////////////////////////////////////////////////////////
/// Implementation of box to box collision between entities
////////////////////////////////////////////////////////////
bool CollisionBoxCollideImplBox(struct ComponentType * Collider, struct ComponentCollisionData * Target);

////////////////////////////////////////////////////////////
/// Implementation of box to sphere collision between entities
////////////////////////////////////////////////////////////
bool CollisionBoxCollideImplSphere(struct ComponentType * Collider, struct ComponentCollisionData * Target);

////////////////////////////////////////////////////////////
/// Implementation of box to frustum collision between entities
////////////////////////////////////////////////////////////
bool CollisionBoxCollideImplFrustum(struct ComponentType * Collider, struct ComponentCollisionData * Target);

////////////////////////////////////////////////////////////
/// Set the data of a bounding box
////////////////////////////////////////////////////////////
void CollisionBoxSetBounds(struct EntityType * Entity, struct Vector3f * Min, struct Vector3f * Max);

////////////////////////////////////////////////////////////
/// Set the data of a bounding box by its center and radius
////////////////////////////////////////////////////////////
void CollisionBoxSetRadius(struct EntityType * Entity, struct Vector3f * Center, float Radius);

////////////////////////////////////////////////////////////
///	Create a bounding box from a list of vertices
////////////////////////////////////////////////////////////
void CollisionBoxGenerate(struct EntityType * Entity, struct Vector3f * VertList, uinteger Size);

////////////////////////////////////////////////////////////
/// Check collision of a bounding sphere component
////////////////////////////////////////////////////////////
bool ComponentCollideSphereImpl(struct ComponentType * Collider, struct ComponentType * Target);

////////////////////////////////////////////////////////////
/// Implementation of sphere to box collision between entities
////////////////////////////////////////////////////////////
bool CollisionSphereCollideImplBox(struct ComponentType * Collider, struct ComponentCollisionData * Target);

////////////////////////////////////////////////////////////
/// Implementation of sphere to sphere collision between entities
////////////////////////////////////////////////////////////
bool CollisionSphereCollideImplSphere(struct ComponentType * Collider, struct ComponentCollisionData * Target);

////////////////////////////////////////////////////////////
/// Implementation of sphere to frustum collision between entities
////////////////////////////////////////////////////////////
bool CollisionSphereCollideImplFrustum(struct ComponentType * Collider, struct ComponentCollisionData * Target);

////////////////////////////////////////////////////////////
/// Set the data of a bounding sphere
////////////////////////////////////////////////////////////
void CollisionSphereSetBounds(struct EntityType * Entity, struct Vector3f * Center, float Radius);

////////////////////////////////////////////////////////////
/// Set the radius of a bounding sphere
////////////////////////////////////////////////////////////
void CollisionSphereSetRadius(struct EntityType * Entity, float Radius);

////////////////////////////////////////////////////////////
/// Set the radius of a bounding sphere
////////////////////////////////////////////////////////////
void CollisionSphereSetCenter(struct EntityType * Entity, struct Vector3f * Center);

////////////////////////////////////////////////////////////
/// Check collision of a view frustum volume component
////////////////////////////////////////////////////////////
bool ComponentCollideFrustumImpl(struct ComponentType * Collider, struct ComponentType * Target);

////////////////////////////////////////////////////////////
/// Implementation of frustum to box collision between entities
////////////////////////////////////////////////////////////
bool CollisionFrustumCollideImplBox(struct ComponentType * Collider, struct ComponentCollisionData * Target);

////////////////////////////////////////////////////////////
/// Implementation of frustum to sphere collision between entities
////////////////////////////////////////////////////////////
bool CollisionFrustumCollideImplSphere(struct ComponentType * Collider, struct ComponentCollisionData * Target);

////////////////////////////////////////////////////////////
/// Implementation of frustum to frustum collision between entities
////////////////////////////////////////////////////////////
bool CollisionFrustumCollideImplFrustum(struct ComponentType * Collider, struct ComponentCollisionData * Target);

////////////////////////////////////////////////////////////
/// Update a frustum component
////////////////////////////////////////////////////////////
void CollisionFrustumUpdate(struct EntityType * Entity, struct Matrix4f * ViewProj);

////////////////////////////////////////////////////////////
/// Update frustum using a view projection matrix extended
////////////////////////////////////////////////////////////
void CollisionFrustumUpdateProj(struct EntityType * Entity, struct Matrix4f * View, struct Matrix4f * Proj, struct Matrix4f * ViewProj, float Depth);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the entity collision generic implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentCollisionType * EntityComponentCollisionAttr()
{
	static const struct ComponentCollisionType ComponentCollisionAttr = {
		TypeInit(Collide, NULL),
		ExprInit(DispatchImpl,
			ExprInit(Collide,
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_BOX, NULL),
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_SPHERE, NULL),
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_FRUSTUM, NULL)
				)
			),
		UnionInit(TypeImpl, Derived, NULL)
	};

	return (const struct ComponentCollisionType *)&ComponentCollisionAttr;
}

////////////////////////////////////////////////////////////
/// Return the entity collision generic implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentCollisionImpl()
{
	static const struct ComponentImplType ComponentCollisionImpl = {
		ExprInit(Base, TypeInit(Create, NULL), TypeInit(Destroy, NULL)),
		TypeInit(Type, COMPONENT_TYPE_COLLISION),
		UnionInit(Attributes, Collision, &EntityComponentCollisionAttr)
	};

	return &ComponentCollisionImpl;
}

////////////////////////////////////////////////////////////
/// Create a bounding box component
////////////////////////////////////////////////////////////
struct EntityType * EntityCollisionBoxCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Collision = EntityComponentCollisionBox(Entity);
		struct ComponentCollisionBoxData * CollisionData = ComponentDataCollisionBox(Collision);

		// Initialize bounding box
		BoundingBoxInitialize(&CollisionData->Box);

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a bounding box component
////////////////////////////////////////////////////////////
void EntityCollisionBoxDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Collision = EntityComponentCollisionBox(Entity);
		struct ComponentCollisionBoxData * CollisionData = ComponentDataCollisionBox(Collision);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the entity collision box implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentCollisionType * EntityComponentCollisionBoxAttr()
{
	static struct ComponentCollisionBoxType ComponentCollisionBoxAttrImpl = {
		TypeInit(SetBounds, CollisionBoxSetBounds),
		TypeInit(SetRadius, CollisionBoxSetRadius),
		TypeInit(Generate, CollisionBoxGenerate)
	};

	static const struct ComponentCollisionType ComponentCollisionBoxAttr = {
		TypeInit(Collide, &ComponentCollideBoxImpl),
		ExprInit(DispatchImpl,
			ExprInit(Collide,
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_BOX, &CollisionBoxCollideImplBox),
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_SPHERE, &CollisionBoxCollideImplSphere),
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_FRUSTUM, &CollisionBoxCollideImplFrustum)
				)
			),
		UnionInit(TypeImpl, Box, &ComponentCollisionBoxAttrImpl)
	};

	return (const struct ComponentCollisionType *)&ComponentCollisionBoxAttr;
}

////////////////////////////////////////////////////////////
/// Return the entity collision box implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentCollisionBoxImpl()
{
	static const struct ComponentImplType ComponentCollisionBoxImpl = {
		ExprInit(Base, TypeInit(Create, &EntityCollisionBoxCreate), TypeInit(Destroy, &EntityCollisionBoxDestroy)),
		TypeInit(Type, COMPONENT_TYPE_COLLISION),
		UnionInit(Attributes, Collision, &EntityComponentCollisionBoxAttr)
	};

	return &ComponentCollisionBoxImpl;
}

////////////////////////////////////////////////////////////
/// Check collision of a bounding box component
////////////////////////////////////////////////////////////
bool ComponentCollideBoxImpl(struct ComponentType * Collider, struct ComponentType * Target)
{
	if (Collider && Target)
	{
		struct ComponentCollisionType * TargetType = ComponentTypeCollision(Target);

		return TargetType->DispatchImpl.Collide[ComponentCollisionTypeGetImpl(Collider->DataImpl.Type)](Target, (struct ComponentCollisionData *)ComponentDataCollisionBox(Collider));
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Implementation of box to box collision between entities
////////////////////////////////////////////////////////////
bool CollisionBoxCollideImplBox(struct ComponentType * Collider, struct ComponentCollisionData * Target)
{
	if (Collider && Target)
	{
		struct ComponentCollisionBoxData * ColliderBox = ComponentDataCollisionBox(Collider);
		struct ComponentCollisionBoxData * TargetBox = (struct ComponentCollisionBoxData *)Target;

		// Test bounding box collision
		uinteger Result = BoundingBoxCollision(&ColliderBox->Box, &TargetBox->Box);

		return (Result == BOUNDING_BOX_INSIDE || Result == BOUNDING_BOX_OVERLAP);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Implementation of box to sphere collision between entities
////////////////////////////////////////////////////////////
bool CollisionBoxCollideImplSphere(struct ComponentType * Collider, struct ComponentCollisionData * Target)
{
	if (Collider && Target)
	{
		struct ComponentCollisionBoxData * ColliderBox = ComponentDataCollisionBox(Collider);
		struct ComponentCollisionSphereData * TargetSphere = (struct ComponentCollisionSphereData *)Target;

		// Test bounding box collision with sphere
		uinteger Result = BoundingBoxCollisionSphere(&ColliderBox->Box, &TargetSphere->Sphere);

		return (Result == BOUNDING_BOX_INSIDE || Result == BOUNDING_BOX_OVERLAP);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Implementation of box to frustum collision between entities
////////////////////////////////////////////////////////////
bool CollisionBoxCollideImplFrustum(struct ComponentType * Collider, struct ComponentCollisionData * Target)
{
	if (Collider && Target)
	{
		struct ComponentCollisionBoxData * ColliderBox = ComponentDataCollisionBox(Collider);
		struct ComponentCollisionFrustumData * TargetFrustum = (struct ComponentCollisionFrustumData *)Target;

		uinteger Result = FrustumCheckBoundingBox(&TargetFrustum->Frustum, &ColliderBox->Box, FRUSTUM_OVERLAP);

		return (FrustumInside(Result) || FrustumOverlap(Result));
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Set the data of a bounding box
////////////////////////////////////////////////////////////
void CollisionBoxSetBounds(struct EntityType * Entity, struct Vector3f * Min, struct Vector3f * Max)
{
	if (Entity)
	{
		struct ComponentType * Collision = EntityComponentCollisionBox(Entity);
		struct ComponentCollisionBoxData * CollisionData = ComponentDataCollisionBox(Collision);

		BoundingBoxSet(&CollisionData->Box, Min, Max);
	}
}

////////////////////////////////////////////////////////////
/// Set the data of a bounding box by its center and radius
////////////////////////////////////////////////////////////
void CollisionBoxSetRadius(struct EntityType * Entity, struct Vector3f * Center, float Radius)
{
	if (Entity && Center)
	{
		struct ComponentType * Collision = EntityComponentCollisionBox(Entity);
		struct ComponentCollisionBoxData * CollisionData = ComponentDataCollisionBox(Collision);

		BoundingBoxSetRadius(&CollisionData->Box, Center, Radius);
	}
}

////////////////////////////////////////////////////////////
///	Create a bounding box from a list of vertices
////////////////////////////////////////////////////////////
void CollisionBoxGenerate(struct EntityType * Entity, struct Vector3f * VertList, uinteger Size)
{
	if (Entity && VertList)
	{
		struct ComponentType * Collision = EntityComponentCollisionBox(Entity);
		struct ComponentCollisionBoxData * CollisionData = ComponentDataCollisionBox(Collision);

		BoundingBoxGenerate(&CollisionData->Box, VertList, Size);
	}
}

////////////////////////////////////////////////////////////
/// Create a bounding sphere component
////////////////////////////////////////////////////////////
struct EntityType * EntityCollisionSphereCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Collision = EntityComponentCollisionSphere(Entity);
		struct ComponentCollisionSphereData * CollisionData = ComponentDataCollisionSphere(Collision);

		BoundingSphereInitialize(&CollisionData->Sphere);

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a bounding sphere component
////////////////////////////////////////////////////////////
void EntityCollisionSphereDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Collision = EntityComponentCollisionSphere(Entity);
		struct ComponentCollisionSphereData * CollisionData = ComponentDataCollisionSphere(Collision);

		//  ..
	}
}

////////////////////////////////////////////////////////////
/// Return the entity collision sphere implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentCollisionType * EntityComponentCollisionSphereAttr()
{
	static struct ComponentCollisionSphereType ComponentCollisionSphereAttrImpl = {
		TypeInit(SetBounds, &CollisionSphereSetBounds),
		TypeInit(SetRadius, &CollisionSphereSetRadius),
		TypeInit(SetCenter, &CollisionSphereSetCenter)
	};

	static const struct ComponentCollisionType ComponentCollisionSphereAttr = {
		TypeInit(Collide, &ComponentCollideSphereImpl),
		ExprInit(DispatchImpl,
			ExprInit(Collide,
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_BOX, &CollisionSphereCollideImplBox),
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_SPHERE, &CollisionSphereCollideImplSphere),
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_FRUSTUM, &CollisionSphereCollideImplFrustum)
				)
			),
		UnionInit(TypeImpl, Sphere, &ComponentCollisionSphereAttrImpl)
	};

	return (const struct ComponentCollisionType *)&ComponentCollisionSphereAttr;
}

////////////////////////////////////////////////////////////
/// Return the entity collision sphere implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentCollisionSphereImpl()
{
	static const struct ComponentImplType ComponentCollisionSphereImpl = {
		ExprInit(Base, TypeInit(Create, &EntityCollisionSphereCreate), TypeInit(Destroy, &EntityCollisionSphereDestroy)),
		TypeInit(Type, COMPONENT_TYPE_COLLISION),
		UnionInit(Attributes, Collision, &EntityComponentCollisionSphereAttr)
	};

	return &ComponentCollisionSphereImpl;
}

////////////////////////////////////////////////////////////
/// Check collision of a bounding sphere component
////////////////////////////////////////////////////////////
bool ComponentCollideSphereImpl(struct ComponentType * Collider, struct ComponentType * Target)
{
	if (Collider && Target)
	{
		struct ComponentCollisionType * TargetType = ComponentTypeCollision(Target);

		return TargetType->DispatchImpl.Collide[ComponentCollisionTypeGetImpl(Collider->DataImpl.Type)](Target, (struct ComponentCollisionData *)ComponentDataCollisionSphere(Collider));
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Implementation of sphere to box collision between entities
////////////////////////////////////////////////////////////
bool CollisionSphereCollideImplBox(struct ComponentType * Collider, struct ComponentCollisionData * Target)
{
	if (Collider && Target)
	{
		struct ComponentCollisionSphereData * ColliderSphere = ComponentDataCollisionSphere(Collider);
		struct ComponentCollisionBoxData * TargetBox = (struct ComponentCollisionBoxData *)Target;

		// Test sphere against box
		uinteger Result = BoundingSphereCollisionBox(&ColliderSphere->Sphere, &TargetBox->Box);

		return (Result == BOUNDING_SPHERE_INSIDE || Result == BOUNDING_SPHERE_OVERLAP);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Implementation of sphere to sphere collision between entities
////////////////////////////////////////////////////////////
bool CollisionSphereCollideImplSphere(struct ComponentType * Collider, struct ComponentCollisionData * Target)
{
	if (Collider && Target)
	{
		struct ComponentCollisionSphereData * ColliderSphere = ComponentDataCollisionSphere(Collider);
		struct ComponentCollisionSphereData * TargetSphere = (struct ComponentCollisionSphereData *)Target;

		// Test sphere against sphere
		uinteger Result = BoundingSphereCollision(&ColliderSphere->Sphere, &TargetSphere->Sphere);

		return (Result == BOUNDING_SPHERE_INSIDE || Result == BOUNDING_SPHERE_OVERLAP);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Implementation of sphere to frustum collision between entities
////////////////////////////////////////////////////////////
bool CollisionSphereCollideImplFrustum(struct ComponentType * Collider, struct ComponentCollisionData * Target)
{
	if (Collider && Target)
	{
		struct ComponentCollisionSphereData * ColliderSphere = ComponentDataCollisionSphere(Collider);
		struct ComponentCollisionFrustumData * TargetFrustum = (struct ComponentCollisionFrustumData *)Target;

		// Test sphere against frustum
		uinteger Result = FrustumCheckSpherePosition(&TargetFrustum->Frustum, &ColliderSphere->Sphere);

		return (FrustumInside(Result) || FrustumOverlap(Result));
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Set the data of a bounding sphere
////////////////////////////////////////////////////////////
void CollisionSphereSetBounds(struct EntityType * Entity, struct Vector3f * Center, float Radius)
{
	if (Entity && Center)
	{
		struct ComponentType * Collision = EntityComponentCollisionSphere(Entity);
		struct ComponentCollisionSphereData * CollisionData = ComponentDataCollisionSphere(Collision);

		BoundingSphereSet(&CollisionData->Sphere, Center, Radius);
	}
}

////////////////////////////////////////////////////////////
/// Set the radius of a bounding sphere
////////////////////////////////////////////////////////////
void CollisionSphereSetRadius(struct EntityType * Entity, float Radius)
{
	if (Entity)
	{
		struct ComponentType * Collision = EntityComponentCollisionSphere(Entity);
		struct ComponentCollisionSphereData * CollisionData = ComponentDataCollisionSphere(Collision);

		BoundingSphereSetRadius(&CollisionData->Sphere, Radius);
	}
}

////////////////////////////////////////////////////////////
/// Set the radius of a bounding sphere
////////////////////////////////////////////////////////////
void CollisionSphereSetCenter(struct EntityType * Entity, struct Vector3f * Center)
{
	if (Entity && Center)
	{
		struct ComponentType * Collision = EntityComponentCollisionSphere(Entity);
		struct ComponentCollisionSphereData * CollisionData = ComponentDataCollisionSphere(Collision);

		BoundingSphereMove(&CollisionData->Sphere, Center);
	}
}

////////////////////////////////////////////////////////////
/// Create a frustum component
////////////////////////////////////////////////////////////
struct EntityType * EntityCollisionFrustumCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Collision = EntityComponentCollisionFrustum(Entity);
		struct ComponentCollisionFrustumData * CollisionData = ComponentDataCollisionFrustum(Collision);

		// Initialize frustum
		FrustumInitialize(&CollisionData->Frustum);

		return Entity;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Destroy a frustum component
////////////////////////////////////////////////////////////
void EntityCollisionFrustumDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Collision = EntityComponentCollisionFrustum(Entity);
		struct ComponentCollisionFrustumData * CollisionData = ComponentDataCollisionFrustum(Collision);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the entity collision frustum implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentCollisionType * EntityComponentCollisionFrustumAttr()
{
	static struct ComponentCollisionFrustumType ComponentCollisionFrustumAttrImpl = {
		TypeInit(Update, &CollisionFrustumUpdate),
		TypeInit(UpdateProjection, &CollisionFrustumUpdateProj),
	};

	static const struct ComponentCollisionType ComponentCollisionFrustumAttr = {
		TypeInit(Collide, &ComponentCollideFrustumImpl),
		ExprInit(DispatchImpl,
			ExprInit(Collide,
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_BOX, &CollisionFrustumCollideImplBox),
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_SPHERE, &CollisionFrustumCollideImplSphere),
					ArrayInit(COMPONENT_TYPE_COLLISION_IMPL_FRUSTUM, &CollisionFrustumCollideImplFrustum)
				)
			),
		UnionInit(TypeImpl, Frustum, &ComponentCollisionFrustumAttrImpl)
	};

	return (const struct ComponentCollisionType *)&ComponentCollisionFrustumAttr;
}

////////////////////////////////////////////////////////////
/// Return the entity collision frustum implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentCollisionFrustumImpl()
{
	static const struct ComponentImplType ComponentCollisionFrustumImpl = {
		ExprInit(Base, TypeInit(Create, &EntityCollisionFrustumCreate), TypeInit(Destroy, &EntityCollisionFrustumDestroy)),
		TypeInit(Type, COMPONENT_TYPE_COLLISION),
		UnionInit(Attributes, Collision, &EntityComponentCollisionFrustumAttr)
	};

	return &ComponentCollisionFrustumImpl;
}

////////////////////////////////////////////////////////////
/// Check collision of a view frustum volume component
////////////////////////////////////////////////////////////
bool ComponentCollideFrustumImpl(struct ComponentType * Collider, struct ComponentType * Target)
{
	if (Collider && Target)
	{
		struct ComponentCollisionType * TargetType = ComponentTypeCollision(Target);

		return TargetType->DispatchImpl.Collide[ComponentCollisionTypeGetImpl(Collider->DataImpl.Type)](Target, (struct ComponentCollisionData *)ComponentDataCollisionFrustum(Collider));
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Implementation of frustum to box collision between entities
////////////////////////////////////////////////////////////
bool CollisionFrustumCollideImplBox(struct ComponentType * Collider, struct ComponentCollisionData * Target)
{
	if (Collider && Target)
	{
		struct ComponentCollisionFrustumData * ColliderFrustum = ComponentDataCollisionFrustum(Collider);
		struct ComponentCollisionBoxData * TargetBox = (struct ComponentCollisionBoxData *)Target;

		uinteger Result = FrustumCheckBoundingBox(&ColliderFrustum->Frustum, &TargetBox->Box, FRUSTUM_OVERLAP);

		return (FrustumInside(Result) || FrustumOverlap(Result));
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Implementation of frustum to sphere collision between entities
////////////////////////////////////////////////////////////
bool CollisionFrustumCollideImplSphere(struct ComponentType * Collider, struct ComponentCollisionData * Target)
{
	if (Collider && Target)
	{
		struct ComponentCollisionFrustumData * ColliderFrustum = ComponentDataCollisionFrustum(Collider);
		struct ComponentCollisionSphereData * TargetSphere = (struct ComponentCollisionSphereData *)Target;

		// Test sphere against frustum
		uinteger Result = FrustumCheckSpherePosition(&ColliderFrustum->Frustum, &TargetSphere->Sphere);

		return (FrustumInside(Result) || FrustumOverlap(Result));
	}

	return false;
}


////////////////////////////////////////////////////////////
/// Implementation of frustum to frustum collision between entities
////////////////////////////////////////////////////////////
bool CollisionFrustumCollideImplFrustum(struct ComponentType * Collider, struct ComponentCollisionData * Target)
{
	if (Collider && Target)
	{
		struct ComponentCollisionFrustumData * ColliderFrustum = ComponentDataCollisionFrustum(Collider);
		struct ComponentCollisionFrustumData * TargetSphere = (struct ComponentCollisionFrustumData *)Target;

		// todo: implement frustum vs frustum collision

		return false;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Update a frustum component
////////////////////////////////////////////////////////////
void CollisionFrustumUpdate(struct EntityType * Entity, struct Matrix4f * ViewProj)
{
	if (Entity && ViewProj)
	{
		struct ComponentType * Collision = EntityComponentCollisionFrustum(Entity);
		struct ComponentCollisionFrustumData * CollisionData = ComponentDataCollisionFrustum(Collision);

		FrustumUpdate(&CollisionData->Frustum, ViewProj);
	}
}

////////////////////////////////////////////////////////////
/// Update frustum using a view projection matrix extended
////////////////////////////////////////////////////////////
void CollisionFrustumUpdateProj(struct EntityType * Entity, struct Matrix4f * View, struct Matrix4f * Proj, struct Matrix4f * ViewProj, float Depth)
{
	if (Entity && Proj && ViewProj)
	{
		struct ComponentType * Collision = EntityComponentCollisionFrustum(Entity);
		struct ComponentCollisionFrustumData * CollisionData = ComponentDataCollisionFrustum(Collision);

		FrustumUpdateEx(&CollisionData->Frustum, View, Proj, ViewProj, Depth);
	}
}

////////////////////////////////////////////////////////////
/// Check if entity collides with a collision data impl
////////////////////////////////////////////////////////////
bool EntityCollisionComponentCollide(struct EntityType * Entity, struct ComponentCollisionData * Collider, uint32 DataType)
{
    if (Entity && Collider)
    {
        struct ComponentType * Component = NULL;

        if (EntityComponentDataType(Entity, COMPONENT_DATA_COLLISION))
        {
            Component = EntityComponentCollision(Entity);
        }
        else if (EntityComponentDataType(Entity, COMPONENT_DATA_COLLISION_BOX))
        {
            Component = EntityComponentCollisionBox(Entity);
        }
        else if (EntityComponentDataType(Entity, COMPONENT_DATA_COLLISION_SPHERE))
        {
            Component = EntityComponentCollisionSphere(Entity);
        }

        else if (EntityComponentDataType(Entity, COMPONENT_DATA_COLLISION_FRUSTUM))
        {
            Component = EntityComponentCollisionFrustum(Entity);
        }

        if (Component)
        {
            struct ComponentCollisionType * EntityComponentType = ComponentTypeCollision(Component);

			return EntityComponentType->DispatchImpl.Collide[ComponentCollisionTypeGetImpl(DataType)](Component, Collider);
        }
    }

	return false;
}
