/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WORLD_ENTITY_H
#define WORLD_ENTITY_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Preprocessor/If.h>
#include <Preprocessor/For.h>

#include <DataType/HashMap.h>

#include <World/Build.h>
#include <World/Component.h>
#include <World/EntityFactory.h>
#include <World/EntityTransform.h>
#include <World/EntityCollision.h>
#include <World/EntityAnimation.h>
#include <World/EntityPhysics.h>
#include <World/EntityRender.h>
#include <World/EntityOcclusion.h>
#include <World/EntitySound.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define ENTITY_TYPE_TERRAIN			0x00
#define ENTITY_TYPE_TRIGGER			0x01
#define ENTITY_TYPE_MODEL			0x02
#define ENTITY_TYPE_LIGHT			0x03
#define ENTITY_TYPE_PARTICLE		0x04
#define ENTITY_TYPE_POINT			0x05
#define ENTITY_TYPE_CAMERA			0x06

#define ENTITY_TYPE_COUNT			0x07
#define ENTITY_TYPE_INVALID         UINTEGER_MAX_RANGE

////////////////////////////////////////////////////////////
// Forward declarations
////////////////////////////////////////////////////////////
struct EntityFactoryType;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct EntityIdPairType
{
	uinteger		Type;
	EntityIdType	Id;
};

struct EntityComponentRefType
{
	uinteger Type;

	union
	{
		ComponentIdType Id;
		struct ComponentType * Ptr;
	} Component;

	bool Managed;
};

struct EntityType
{
	EntityIdType				Index;				//< Entity index
	HashMap						Components;			//< Base components
	struct EntityFactoryType *	Factory;			//< Entity factory
};

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define EntityComponentBase(Entity)					EntityComponent(Entity, COMPONENT_DATA_BASE)
#define EntityComponentTransform(Entity)			EntityComponent(Entity, COMPONENT_DATA_TRANSFORM)
#define EntityComponentCollision(Entity)			EntityComponent(Entity, COMPONENT_DATA_COLLISION)
#define EntityComponentCollisionBox(Entity)			EntityComponent(Entity, COMPONENT_DATA_COLLISION_BOX)
#define EntityComponentCollisionSphere(Entity)		EntityComponent(Entity, COMPONENT_DATA_COLLISION_SPHERE)
#define EntityComponentCollisionFrustum(Entity)		EntityComponent(Entity, COMPONENT_DATA_COLLISION_FRUSTUM)
#define EntityComponentAnimation(Entity)			EntityComponent(Entity, COMPONENT_DATA_ANIMATION)
#define EntityComponentPhysics(Entity)				EntityComponent(Entity, COMPONENT_DATA_PHYSICS)
#define EntityComponentRender(Entity)				EntityComponent(Entity, COMPONENT_DATA_RENDER)
#define EntityComponentOcclusion(Entity)			EntityComponent(Entity, COMPONENT_DATA_OCCLUSION)
#define EntityComponentSound(Entity)				EntityComponent(Entity, COMPONENT_DATA_SOUND)

#define EntityFactoryComponentDataSet(Type)			(0x01 << Type)
#define EntityFactoryComponentDataGet(Mask, Type)	(Mask & (EntityFactoryComponentDataSet(Type)))

#define EntityComponentDataType(Entity, Type)		EntityFactoryComponentDataGet(Entity->Factory->ComponentDataMask, Type)

#define EntityComponentMask(BuildType, DataType) \
	PREPROCESSOR_IF(WORLD_BUILD_TYPE_IS(BuildType), | EntityFactoryComponentDataSet(DataType), PREPROCESSOR_EMPTY_SYMBOL())

#define EntityComponentMaskDefine(...) \
	COMPONENT_DATA_MASK_EMPTY PREPROCESSOR_FOR_EACH_TUPLE(EntityComponentMask, __VA_ARGS__)

#define EntityComponentTypes(BuildType, DataType, Pointer) \
	PREPROCESSOR_IF(WORLD_BUILD_TYPE_IS(BuildType), ArrayInit(DataType, Pointer), ArrayInit(DataType, NULL)),

#define EntityComponentTypesDefine(...) \
	PREPROCESSOR_ARGS_LAST_REMOVE(PREPROCESSOR_FOR_EACH_TUPLE(EntityComponentTypes, __VA_ARGS__))

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize all entity factories
////////////////////////////////////////////////////////////
void EntitySystemInitialize();

////////////////////////////////////////////////////////////
/// Destroy all entity factories
////////////////////////////////////////////////////////////
void EntitySystemDestroy();

////////////////////////////////////////////////////////////
/// Create an entity
////////////////////////////////////////////////////////////
EntityIdType EntityCreate(uinteger Type);

////////////////////////////////////////////////////////////
/// Get entity component
////////////////////////////////////////////////////////////
struct ComponentType * EntityComponent(struct EntityType * Entity, uinteger DataTypeId);

////////////////////////////////////////////////////////////
/// Get entity from factory
////////////////////////////////////////////////////////////
struct EntityType * EntityGet(uinteger Type, EntityIdType EntityId);

////////////////////////////////////////////////////////////
/// Destroy an entity
////////////////////////////////////////////////////////////
void EntityDestroy(uinteger Type, EntityIdType EntityId);

////////////////////////////////////////////////////////////
/// Creates a component in the factory
////////////////////////////////////////////////////////////
ComponentIdType EntityCreateComponent(struct EntityType * Entity, uinteger DataType);

////////////////////////////////////////////////////////////
/// Destroy a component of the factory
////////////////////////////////////////////////////////////
bool EntityDestroyComponent(struct EntityType * Entity, uinteger DataType, ComponentIdType ComponentId);

////////////////////////////////////////////////////////////
/// Attach component to an entity
////////////////////////////////////////////////////////////
void EntityAttachComponent(struct EntityType * Entity, struct ComponentType * Component);

////////////////////////////////////////////////////////////
/// Detach component to an entity
////////////////////////////////////////////////////////////
void EntityDetachComponent(struct EntityType * Entity, struct ComponentType * Component);

#endif // WORLD_ENTITY_H
