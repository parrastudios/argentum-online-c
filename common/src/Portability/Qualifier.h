/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PORTABILITY_QUALIFIER_H
#define PORTABILITY_QUALIFIER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Compiler.h>
#include <Portability/Language.h>

////////////////////////////////////////////////////////////
// Detection
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Define portable inline keyword
////////////////////////////////////////////////////////////
#if COMPILER_TYPE == COMPILER_MSVC
#   if COMPILER_VERSION >= CompilerVersionBuild(12, 0, 0)
#       define inline __forceinline
#   endif
#elif COMPILER_TYPE == COMPILER_MINGW
#	if !defined(inline)
#		define inline __inline
#	endif
#elif COMPILER_TYPE == COMPILER_GNUC
#   if !__GNUC_STDC_INLINE__
#       define inline extern inline
#   endif
#elif LANGUAGE_VERSION_TYPE < LANGUAGE_VERSION_C99
#   if !defined(inline)
#	    define inline static
#   endif
#endif

#if !defined(inline)
#	define inline
#endif

////////////////////////////////////////////////////////////
/// Define const and volatile for non-conforming platforms
////////////////////////////////////////////////////////////
#if (LANGUAGE_VERSION_TYPE == LANGUAGE_VERSION_UNKNOWN)
#	ifndef const
#		define const
#	endif
#	ifndef volatile
#		define volatile
#	endif
#endif

#endif // PORTABILITY_QUALIFIER_H
