/////////////////////////////////////////////////////////////////////////////
//	Preprocessor Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A cross-platform macro based library to give support
//	for preprocessor metaprogramming.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PREPROCESSOR_TUPLE_H
#define PREPROCESSOR_TUPLE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define PREPROCESSOR_TUPLE_EXPAND_IMPL(...) \
	__VA_ARGS__

#define PREPROCESSOR_TUPLE_EXPAND(...) \
	PREPROCESSOR_TUPLE_EXPAND_IMPL __VA_ARGS__

#define PREPROCESSOR_TUPLE_MAKE(...) \
	(PREPROCESSOR_TUPLE_EXPAND_IMPL(__VA_ARGS__))

#define PREPROCESSOR_TUPLE_PREPEND(Expr, Tuple) \
	PREPROCESSOR_TUPLE_MAKE(Expr, PREPROCESSOR_TUPLE_EXPAND(Tuple))

#define PREPROCESSOR_TUPLE_APPEND(Expr, Tuple) \
	PREPROCESSOR_TUPLE_MAKE(PREPROCESSOR_TUPLE_EXPAND(Tuple), Expr)

#if 0
#define PREPROCESSOR_TUPLE_PREPEND_VARIDIC(Expr, ...) \
	PREPROCESSOR_FOR_EACH
#endif

#endif // PREPROCESSOR_TUPLE_H
