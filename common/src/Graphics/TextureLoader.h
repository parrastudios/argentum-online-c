/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_TEXTURE_LOADER_H
#define GRAPHICS_TEXTURE_LOADER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <Graphics/Texture.h>

////////////////////////////////////////////////////////////
/// The format of images that may be loaded (force_channels).
/// TEXTURE_LOAD_AUTO leaves the image in whatever format it was found.
/// TEXTURE_LOAD_L forces the image to load as Luminous (greyscale)
/// TEXTURE_LOAD_LA forces the image to load as Luminous with Alpha
/// TEXTURE_LOAD_RGB forces the image to load as Red Green Blue
/// TEXTURE_LOAD_RGBA forces the image to load as Red Green Blue Alpha
////////////////////////////////////////////////////////////
enum
{
	TEXTURE_LOAD_AUTO	= 0x00,
	TEXTURE_LOAD_L		= 0x01,
	TEXTURE_LOAD_LA		= 0x02,
	TEXTURE_LOAD_RGB	= 0x03,
	TEXTURE_LOAD_RGBA	= 0x04
};

////////////////////////////////////////////////////////////
/// Load pixels from an image file
///
/// \param Filename : Path of image file to load
/// \param TextureData : Texture
///
/// \return true if loading was successful
///
////////////////////////////////////////////////////////////
bool TextureLoadFromFile(char * Filename, struct Texture * TextureData);

////////////////////////////////////////////////////////////
/// Load pixels from an image file
///
/// \param Filename : Path of image file to load
/// \param TextureData : Texture
/// \param SubTexture : The sub texture rect
///
/// \return true if loading was successful
///
////////////////////////////////////////////////////////////
bool TextureLoadFromFileEx(char * Filename, struct Texture * TextureData, struct Rect * SubTexture);

////////////////////////////////////////////////////////////
/// Load pixels from an image file in memory
///
/// \param Data :        Pointer to the file data in memory
/// \param SizeInBytes : Size of the data to load, in bytes
/// \param TextureData : Texture
///
/// \return true if loading was successful
///
////////////////////////////////////////////////////////////
bool TextureLoadFromMemory(uint8 * Data, uinteger SizeInBytes, struct Texture * TextureData);

#endif // GRAPHICS_TEXTURE_LOADER_H
