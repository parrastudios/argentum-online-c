Specifications and Developmen Kit can be found in folders:

	/specs
	/devkit

Source: http://sauerbraten.org/iqm

The original repository of the development kit can be retreived from:

	https://github.com/lsalzman/iqm

IQM model viewer (IqeBrowser) can be found in:

	/viewer

Source: http://www.moddb.com/mods/r-reinhard/addons/iqebrowser-v210
