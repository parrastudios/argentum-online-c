/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef WINDOW_EVENT_H
#define WINDOW_EVENT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
/// Definition of key codes for keyboard events
////////////////////////////////////////////////////////////
enum KeyCode
{
	A = 'a',
	B = 'b',
	C = 'c',
	D = 'd',
	E = 'e',
	F = 'f',
	G = 'g',
	H = 'h',
	I = 'i',
	J = 'j',
	K = 'k',
	L = 'l',
	M = 'm',
	N = 'n',
	O = 'o',
	P = 'p',
	Q = 'q',
	R = 'r',
	S = 's',
	T = 't',
	U = 'u',
	V = 'v',
	W = 'w',
	X = 'x',
	Y = 'y',
	Z = 'z',
	Num0 = '0',
	Num1 = '1',
	Num2 = '2',
	Num3 = '3',
	Num4 = '4',
	Num5 = '5',
	Num6 = '6',
	Num7 = '7',
	Num8 = '8',
	Num9 = '9',
	Esc = 256,
	LControl,
	LShift,
	LAlt,
	LSystem,      ///< OS specific key (left side) : windows (Win and Linux), apple (MacOS), ...
	RControl,
	RShift,
	RAlt,
	RSystem,      ///< OS specific key (right side) : windows (Win and Linux), apple (MacOS), ...
	Menu,
	LBracket,     ///< [
	RBracket,     ///< ]
	SemiColon,    ///< ;
	Comma,        ///< ,
	Period,       ///< .
	Quote,        ///< '
	Slash,        ///< /
	BackSlash,
	Tilde,        ///< ~
	Equal,        ///< =
	Dash,         ///< -
	Space,
	Return,
	Back,
	Tab,
	PageUp,
	PageDown,
	End,
	Home,
	Insert,
	Delete,
	Add,          ///< +
	Subtract,     ///< -
	Multiply,     ///< *
	Divide,       ///< /
	Left,         ///< Left arrow
	Right,        ///< Right arrow
	Up,           ///< Up arrow
	Down,         ///< Down arrow
	Numpad0,
	Numpad1,
	Numpad2,
	Numpad3,
	Numpad4,
	Numpad5,
	Numpad6,
	Numpad7,
	Numpad8,
	Numpad9,
	F1,
	F2,
	F3,
	F4,
	F5,
	F6,
	F7,
	F8,
	F9,
	F10,
	F11,
	F12,
	F13,
	F14,
	F15,
	Pause,
	KeyCodeCount
};


////////////////////////////////////////////////////////////
/// Definition of button codes for mouse events
////////////////////////////////////////////////////////////
enum MouseButton
{
	LeftButton,
	RightButton,
	Middle,
	XButton1,
	XButton2
};

////////////////////////////////////////////////////////////
/// Event defines a system event and its parameters
////////////////////////////////////////////////////////////
struct Event
{
    ////////////////////////////////////////////////////////////
    /// Keyboard event parameters
    ////////////////////////////////////////////////////////////
    struct KeyEvent
    {
        enum KeyCode	Code;
        bool      		Alt;
        bool			Control;
        bool			Shift;
    } Key;

    ////////////////////////////////////////////////////////////
    /// Text event parameters
    ////////////////////////////////////////////////////////////
    struct TextEvent
    {
        uint32 Unicode;
    } Text;

    ////////////////////////////////////////////////////////////
    /// Mouse move event parameters
    ////////////////////////////////////////////////////////////
    struct MouseMoveEvent
    {
        int32 X;
        int32 Y;
    } MouseMove;

    ////////////////////////////////////////////////////////////
    /// Mouse buttons events parameters
    ////////////////////////////////////////////////////////////
    struct MouseButtonEvent
    {
        enum MouseButton	Button;
        int32				X;
        int32				Y;
	} MouseButton;

    ////////////////////////////////////////////////////////////
    /// Mouse wheel events parameters
    ////////////////////////////////////////////////////////////
    struct MouseWheelEvent
    {
        int32 Delta;
    } MouseWheel;

    ////////////////////////////////////////////////////////////
    /// Size events parameters
    ////////////////////////////////////////////////////////////
    struct SizeEvent
    {
        uint32 Width;
        uint32 Height;
    } Size;

    ////////////////////////////////////////////////////////////
    /// Enumeration of the different types of events
    ////////////////////////////////////////////////////////////
    enum EventType
    {
        Closed = 1,
        Resized,
        LostFocus,
        GainedFocus,
        TextEntered,
        KeyPressed,
        KeyReleased,
        MouseWheelMoved,
        MouseButtonPressed,
        MouseButtonReleased,
        MouseMoved,
        MouseEntered,
        MouseLeft
    } Type;
};

#endif // WINDOW_EVENT_H
