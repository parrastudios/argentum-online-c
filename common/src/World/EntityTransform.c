/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/EntityTransform.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////

#define FloatAngleRoundUp(Float) \
	do \
	{ \
		if (Float > 360.0f) \
		{ \
			Float -= 360.0f; \
		} \
	} while (0)

#define FloatAngleRoundDown(Float) \
	do \
	{ \
		if (Float < -360.0f) \
		{ \
			Float += 360.0f; \
		} \
	} while (0)

#define FloatAngleRound(Float) \
	do \
	{ \
		FloatAngleRoundUp(Float); \
		FloatAngleRoundDown(Float); \
	} while (0)

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a transform component
////////////////////////////////////////////////////////////
struct EntityType * EntityTransformCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a transform component of an entity
////////////////////////////////////////////////////////////
void EntityTransformDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Set position of the entity
////////////////////////////////////////////////////////////
void EntityTransformPosition(struct EntityType * Entity, struct Vector3f * Position);

////////////////////////////////////////////////////////////
/// Changes the direction the entity is facing. This directly
/// affects the orientation of the entity's right, up, and forward vectors.
/// This is usually called in response to the user's input if the entity
/// is able to be moved by the user.
////////////////////////////////////////////////////////////
void EntityTransformOrient(struct EntityType * Entity, struct Vector3f * Orientation);

////////////////////////////////////////////////////////////
/// EntityRotate does not change the direction the entity is facing. This method
/// allows the entity to deallocately spin around without affecting its orientation
/// and its right, up, and forward vectors. For example, if this entity is
/// a planet, then EntityRotate is used to spin the planet on its y axis. If this
/// entity is an asteroid, then EntityRotate() is used to tumble the asteroid as
/// it moves in space.
////////////////////////////////////////////////////////////
void EntityTransformRotate(struct EntityType * Entity, struct Vector3f * Rotation);

////////////////////////////////////////////////////////////
/// Set scale vector of entity
////////////////////////////////////////////////////////////
void EntityTransformScale(struct EntityType * Entity, struct Vector3f * Scale);

////////////////////////////////////////////////////////////
/// Compute right, up and forward vectors
////////////////////////////////////////////////////////////
void EntityTransformExtractAxes(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Update entity elapsed position, rotation, orientation
////////////////////////////////////////////////////////////
void EntityTransformUpdateElapsed(struct EntityType * Entity, struct Vector3f * VelocityElapsed, struct Vector3f * EulerOrientElapsed, struct Vector3f * EulerRotateElapsed, float ElapsedTime);

////////////////////////////////////////////////////////////
/// Update entity position
////////////////////////////////////////////////////////////
void EntityTransformUpdatePosition(struct EntityType * Entity, struct Vector3f * VelocityElapsed, float ElapsedTime);

////////////////////////////////////////////////////////////
/// Update entity orientation
////////////////////////////////////////////////////////////
void EntityTransformUpdateOrientation(struct EntityType * Entity, struct Vector3f * EulerOrientElapsed, float ElapsedTime);

////////////////////////////////////////////////////////////
/// Update entity rotation
////////////////////////////////////////////////////////////
void EntityTransformUpdateRotation(struct EntityType * Entity, struct Vector3f * EulerRotateElapsed, float ElapsedTime);

////////////////////////////////////////////////////////////
/// Set world matrix of the entity
////////////////////////////////////////////////////////////
void EntityTransformWorldMatrix(struct EntityType * Entity, struct Matrix4f * WorldMatrix);

////////////////////////////////////////////////////////////
/// Update entity world matrix
////////////////////////////////////////////////////////////
void EntityTransformUpdateWorldMatrix(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Update an entity
////////////////////////////////////////////////////////////
void EntityTransformUpdate(struct EntityType * Entity, float ElapsedTime);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a transform component
////////////////////////////////////////////////////////////
struct EntityType * EntityTransformCreate(struct EntityType * Entity)
{
	struct ComponentType * Transform = EntityComponentTransform(Entity);
	struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

	if (TransformData != NULL)
	{
		Matrix4fIdentity(&TransformData->WorldMatrix);

		Vector3fSetNull(&TransformData->Heading);

		QuaternionIdentity(&TransformData->Orientation);

		QuaternionIdentity(&TransformData->OrientationConstraint);

		QuaternionIdentity(&TransformData->Rotation);

		QuaternionIdentity(&TransformData->RotationConstraint);

		Vector3fSet(&TransformData->Scale, 1.0f, 1.0f, 1.0f);

		Vector3fSet(&TransformData->Right, 1.0f, 0.0f, 0.0f);

		Vector3fSet(&TransformData->Up, 0.0f, 1.0f, 0.0f);

		Vector3fSet(&TransformData->Forward, 0.0f, 0.0f, -1.0f);

		Vector3fSetNull(&TransformData->Position);

		Vector3fSetNull(&TransformData->EulerOrient);

		Vector3fSetNull(&TransformData->EulerRotate);

		TransformData->ConstrainedToWorldYAxis = true;

		TransformData->Movement = COMPONENT_TRANSFORM_MOVEMENT_ABSOLUTE;
	}

	return Entity;
}

////////////////////////////////////////////////////////////
/// Destroy a transform component of an entity
////////////////////////////////////////////////////////////
void EntityTransformDestroy(struct EntityType * Entity)
{
	// Nothing to destroy in transform component
}

////////////////////////////////////////////////////////////
/// Return the entity transform implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentTransformType * EntityComponentTransformAttr()
{
	static const struct ComponentTransformType ComponentTransformAttr = {
		TypeInit(Update, &EntityTransformUpdate),
		TypeInit(Move, &EntityTransformPosition),
		TypeInit(Scale, &EntityTransformScale),
		TypeInit(Orient, &EntityTransformOrient),
		TypeInit(Rotate, &EntityTransformRotate),
		TypeInit(Matrix, &EntityTransformWorldMatrix)
	};

	return (const struct ComponentTransformType *)&ComponentTransformAttr;
}

////////////////////////////////////////////////////////////
/// Return the entity transform implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentTransformImpl()
{
	static const struct ComponentImplType EntityComponentTransformTypeImpl = {
		ExprInit(Base, TypeInit(Create, &EntityTransformCreate), TypeInit(Destroy, &EntityTransformDestroy)),
		TypeInit(Type, COMPONENT_TYPE_TRANSFORM),
		UnionInit(Attributes, Transform, &EntityComponentTransformAttr)
	};

	return &EntityComponentTransformTypeImpl;
}

////////////////////////////////////////////////////////////
/// Set position of the entity
////////////////////////////////////////////////////////////
void EntityTransformPosition(struct EntityType * Entity, struct Vector3f * Position)
{
	struct ComponentType * Transform = EntityComponentTransform(Entity);
	struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

    // Copy position vector
	Vector3fCopy(&TransformData->Position, Position);
}

////////////////////////////////////////////////////////////
/// Set direction of entity
////////////////////////////////////////////////////////////
void EntityTransformOrient(struct EntityType * Entity, struct Vector3f * Orientation)
{
	struct ComponentType * Transform = EntityComponentTransform(Entity);
	struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

	if (TransformData->Movement == COMPONENT_TRANSFORM_MOVEMENT_RELATIVE)
	{
		Vector3fAdd(&TransformData->EulerOrient, Orientation);

		FloatAngleRound(TransformData->EulerOrient.x);
		FloatAngleRound(TransformData->EulerOrient.y);
		FloatAngleRound(TransformData->EulerOrient.z);
	}
	else if (TransformData->Movement == COMPONENT_TRANSFORM_MOVEMENT_ABSOLUTE)
	{
		struct Matrix4f OrientationMatrix;
		struct Quaternion Quat;
		struct Vector3f EulerOrient;

		Vector3fCopy(&TransformData->EulerOrient, Orientation);

		clamp(TransformData->EulerOrient.x, -360.0f, 360.0f);
		clamp(TransformData->EulerOrient.y, -360.0f, 360.0f);
		clamp(TransformData->EulerOrient.z, -360.0f, 360.0f);

		QuaternionIdentity(&Quat);

		// Update entity orientation
		QuaternionToMatrix(&Quat, &OrientationMatrix);

		Vector3fSet(&EulerOrient, TransformData->EulerOrient.y, TransformData->EulerOrient.x, TransformData->EulerOrient.z);

		QuaternionFromEulerEx(&Quat, &OrientationMatrix, &EulerOrient, TransformData->ConstrainedToWorldYAxis);

		// When moving backwards invert rotations to match direction of travel
		if (Vector3fDot(&TransformData->Heading, &TransformData->Forward) < 0.0f)
		{
			QuaternionInverse(&Quat);
		}

		QuaternionCopy(&TransformData->OrientationConstraint, &Quat);

		QuaternionNormalize(&TransformData->OrientationConstraint);
	}
}

////////////////////////////////////////////////////////////
/// Set rotation of entity
////////////////////////////////////////////////////////////
void EntityTransformRotate(struct EntityType * Entity, struct Vector3f * Rotation)
{
	struct ComponentType * Transform = EntityComponentTransform(Entity);
	struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

	if (TransformData->Movement == COMPONENT_TRANSFORM_MOVEMENT_RELATIVE)
	{
		Vector3fAdd(&TransformData->EulerRotate, Rotation);

		FloatAngleRound(TransformData->EulerRotate.x);
		FloatAngleRound(TransformData->EulerRotate.y);
		FloatAngleRound(TransformData->EulerRotate.z);
	}
	else if (TransformData->Movement == COMPONENT_TRANSFORM_MOVEMENT_ABSOLUTE)
	{
		struct Matrix4f RotationMatrix;
		struct Quaternion Quat;
		struct Vector3f EulerRotate;

		Vector3fCopy(&TransformData->EulerRotate, Rotation);

		clamp(TransformData->EulerRotate.x, -360.0f, 360.0f);
		clamp(TransformData->EulerRotate.y, -360.0f, 360.0f);
		clamp(TransformData->EulerRotate.z, -360.0f, 360.0f);

		QuaternionIdentity(&Quat);

		Matrix4fIdentity(&RotationMatrix);

		// Update the entity's deallocate rotation
		QuaternionToMatrix(&Quat, &RotationMatrix);

		Vector3fSet(&EulerRotate, TransformData->EulerRotate.y, TransformData->EulerRotate.x, TransformData->EulerRotate.z);

		QuaternionFromEulerEx(&Quat, &RotationMatrix, &EulerRotate, TransformData->ConstrainedToWorldYAxis);

		QuaternionCopy(&TransformData->RotationConstraint, &Quat);

		QuaternionNormalize(&TransformData->RotationConstraint);
	}
}

////////////////////////////////////////////////////////////
/// Set scale vector of entity
////////////////////////////////////////////////////////////
void EntityTransformScale(struct EntityType * Entity, struct Vector3f * Scale)
{
	struct ComponentType * Transform = EntityComponentTransform(Entity);
	struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

    // Set scale vector
	Vector3fCopy(&TransformData->Scale, Scale);
}

////////////////////////////////////////////////////////////
/// Set world matrix of the entity
////////////////////////////////////////////////////////////
void EntityTransformWorldMatrix(struct EntityType * Entity, struct Matrix4f * WorldMatrix)
{
	struct ComponentType * Transform = EntityComponentTransform(Entity);
	struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

	Matrix4fCopy(&TransformData->WorldMatrix, WorldMatrix);

	QuaternionFromMatrix(&TransformData->Orientation, WorldMatrix);

	Vector3fSet(&TransformData->Position, WorldMatrix->m[3][0], WorldMatrix->m[3][1], WorldMatrix->m[3][2]);

	EntityTransformExtractAxes(Entity);
}

////////////////////////////////////////////////////////////
/// Compute right, up and forward vectors
////////////////////////////////////////////////////////////
void EntityTransformExtractAxes(struct EntityType * Entity)
{
	struct ComponentType * Transform = EntityComponentTransform(Entity);
	struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

	struct Matrix4f Matrix;

	Matrix4fIdentity(&Matrix);

	QuaternionToMatrix(&TransformData->Orientation, &Matrix);

	Vector3fSet(&TransformData->Right, Matrix.m[0][0], Matrix.m[0][1], Matrix.m[0][2]);
	Vector3fNormalize(&TransformData->Right);

	Vector3fSet(&TransformData->Up, Matrix.m[1][0], Matrix.m[1][1], Matrix.m[1][2]);
	Vector3fNormalize(&TransformData->Up);

	Vector3fSet(&TransformData->Forward, -Matrix.m[2][0], -Matrix.m[2][1], -Matrix.m[2][2]);
	Vector3fNormalize(&TransformData->Forward);
}

////////////////////////////////////////////////////////////
/// Update entity elapsed position, rotation, orientation
////////////////////////////////////////////////////////////
void EntityTransformUpdateElapsed(struct EntityType * Entity, struct Vector3f * VelocityElapsed, struct Vector3f * EulerOrientElapsed, struct Vector3f * EulerRotateElapsed, float ElapsedTime)
{
	struct ComponentType * Transform = EntityComponentTransform(Entity);
	struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

	struct ComponentType * Physics = EntityComponentPhysics(Entity);
	struct ComponentPhysicsData * PhysicsData = ComponentDataPhysics(Physics);

	// TODO: move to physics
	Vector3fSet(VelocityElapsed,
		PhysicsData->Velocity.x * ElapsedTime,
		PhysicsData->Velocity.y * ElapsedTime,
		PhysicsData->Velocity.z * ElapsedTime);

	// Clear velocity for this frame
	Vector3fSetNull(&PhysicsData->Velocity);

	Vector3fSet(EulerOrientElapsed,
		TransformData->EulerOrient.x * ElapsedTime,
		TransformData->EulerOrient.y * ElapsedTime,
		TransformData->EulerOrient.z * ElapsedTime);

	Vector3fSet(EulerRotateElapsed,
		TransformData->EulerRotate.x * ElapsedTime,
		TransformData->EulerRotate.y * ElapsedTime,
		TransformData->EulerRotate.z * ElapsedTime);
}

////////////////////////////////////////////////////////////
/// Update entity position
////////////////////////////////////////////////////////////
void EntityTransformUpdatePosition(struct EntityType * Entity, struct Vector3f * VelocityElapsed, float ElapsedTime)
{
	struct ComponentType * Transform = EntityComponentTransform(Entity);
	struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

    struct Vector3f PreviousPosition;
	struct Vector3f Right, Up, Forward;

	// Update entitiy position
	EntityTransformExtractAxes(Entity);

	Vector3fCopy(&PreviousPosition, &TransformData->Position);

	Vector3fSet(&Right,
		TransformData->Right.x * VelocityElapsed->x,
		TransformData->Right.y * VelocityElapsed->x,
		TransformData->Right.z * VelocityElapsed->x);

	Vector3fAdd(&TransformData->Position, &Right);

	Vector3fSet(&Up,
		TransformData->Up.x * VelocityElapsed->y,
		TransformData->Up.y * VelocityElapsed->y,
		TransformData->Up.z * VelocityElapsed->y);

	Vector3fAdd(&TransformData->Position, &Up);

	Vector3fSet(&Forward,
		TransformData->Forward.x * VelocityElapsed->z,
		TransformData->Forward.y * VelocityElapsed->z,
		TransformData->Forward.z * VelocityElapsed->z);

	Vector3fAdd(&TransformData->Position, &Forward);

	Vector3fSubtractEx(&TransformData->Heading, &TransformData->Position, &PreviousPosition);

	Vector3fNormalize(&TransformData->Heading);
}

////////////////////////////////////////////////////////////
/// Update entity orientation
////////////////////////////////////////////////////////////
void EntityTransformUpdateOrientation(struct EntityType * Entity, struct Vector3f * EulerOrientElapsed, float ElapsedTime)
{
	struct ComponentType * Transform = EntityComponentTransform(Entity);
	struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

	if (TransformData->Movement == COMPONENT_TRANSFORM_MOVEMENT_RELATIVE)
	{
		struct Matrix4f OrientationMatrix;
		struct Quaternion Quat, Orient;
		struct Vector3f EulerOrient;

		QuaternionIdentity(&Quat);

		QuaternionIdentity(&Orient);

		// Update entity orientation
		QuaternionToMatrix(&TransformData->Orientation, &OrientationMatrix);

		Vector3fSet(&EulerOrient, EulerOrientElapsed->y, EulerOrientElapsed->x, EulerOrientElapsed->z);

		QuaternionFromEulerEx(&Quat, &OrientationMatrix, &EulerOrient, TransformData->ConstrainedToWorldYAxis);

		// When moving backwards invert rotations to match direction of travel
		if (Vector3fDot(&TransformData->Heading, &TransformData->Forward) < 0.0f)
		{
			QuaternionInverse(&Quat);
		}

		QuaternionMultQuaternion(&TransformData->Orientation, &Quat, &Orient);

		QuaternionCopy(&TransformData->Orientation, &Orient);

		QuaternionNormalize(&TransformData->Orientation);

		// Clear the entity's cached euler orientation for this frame
		Vector3fSetNull(&TransformData->EulerOrient);
	}
	else if (TransformData->Movement == COMPONENT_TRANSFORM_MOVEMENT_ABSOLUTE)
	{
		if (!QuaternionIsEqual(&TransformData->Orientation, &TransformData->OrientationConstraint))
		{
			struct Quaternion Quat;

			QuaternionNlerp(&TransformData->Orientation, &TransformData->OrientationConstraint, ElapsedTime, &Quat);

			QuaternionCopy(&TransformData->Orientation, &Quat);
		}

		// Clear the entity's cached euler orientation for this frame
		Vector3fSetNull(&TransformData->EulerOrient);
	}
}

////////////////////////////////////////////////////////////
/// Update entity rotation
////////////////////////////////////////////////////////////
void EntityTransformUpdateRotation(struct EntityType * Entity, struct Vector3f * EulerRotateElapsed, float ElapsedTime)
{
	struct ComponentType * Transform = EntityComponentTransform(Entity);
	struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

	if (TransformData->Movement == COMPONENT_TRANSFORM_MOVEMENT_RELATIVE)
	{
		struct Matrix4f RotationMatrix;
		struct Quaternion Quat, Rotation;
		struct Vector3f EulerRotate;

		QuaternionIdentity(&Quat);

		QuaternionIdentity(&Rotation);

		Matrix4fIdentity(&RotationMatrix);

		// Update the entity's deallocate rotation
		QuaternionToMatrix(&TransformData->Rotation, &RotationMatrix);

		Vector3fSet(&EulerRotate, EulerRotateElapsed->y, EulerRotateElapsed->x, EulerRotateElapsed->z);

		QuaternionFromEulerEx(&Quat, &RotationMatrix, &EulerRotate, TransformData->ConstrainedToWorldYAxis);

		QuaternionMultQuaternion(&TransformData->Rotation, &Quat, &Rotation);

		QuaternionCopy(&TransformData->Rotation, &Rotation);

		QuaternionNormalize(&TransformData->Rotation);

		// Clear the entity's cached euler rotation for this frame
		Vector3fSetNull(&TransformData->EulerRotate);
	}
	else if (TransformData->Movement == COMPONENT_TRANSFORM_MOVEMENT_ABSOLUTE)
	{
		if (!QuaternionIsEqual(&TransformData->Rotation, &TransformData->RotationConstraint))
		{
			struct Quaternion Quat;

			QuaternionNlerp(&TransformData->Rotation, &TransformData->RotationConstraint, ElapsedTime, &Quat);

			QuaternionCopy(&TransformData->Rotation, &Quat);
		}

		// Clear the entity's cached euler rotation for this frame
		Vector3fSetNull(&TransformData->EulerRotate);
	}
}

////////////////////////////////////////////////////////////
/// Update entity world matrix
////////////////////////////////////////////////////////////
void EntityTransformUpdateWorldMatrix(struct EntityType * Entity)
{
	struct ComponentType * Transform = EntityComponentTransform(Entity);
	struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

	struct Matrix4f ScalingMatrix, RotationMatrix;
    struct Quaternion Quat;

	// Calculate scaling matrix
	Matrix4fIdentity(&ScalingMatrix);

	ScalingMatrix.m[0][0] = TransformData->Scale.x;
	ScalingMatrix.m[1][1] = TransformData->Scale.y;
	ScalingMatrix.m[2][2] = TransformData->Scale.z;

	// Calculate rotation matrix
	Matrix4fIdentity(&RotationMatrix);

	QuaternionIdentity(&Quat);

	QuaternionMultQuaternion(&TransformData->Rotation, &TransformData->Orientation, &Quat);

	QuaternionNormalize(&Quat);

	QuaternionToMatrix(&Quat, &RotationMatrix);

	// Generate world matrix
	Matrix4fMultiply(&TransformData->WorldMatrix, &ScalingMatrix, &RotationMatrix);

	// Calculate translation matrix over world matrix
	TransformData->WorldMatrix.m[3][0] = TransformData->Position.x;
	TransformData->WorldMatrix.m[3][1] = TransformData->Position.y;
	TransformData->WorldMatrix.m[3][2] = TransformData->Position.z;
}

////////////////////////////////////////////////////////////
/// Update transform of an entity
////////////////////////////////////////////////////////////
void EntityTransformUpdate(struct EntityType * Entity, float ElapsedTime)
{
	struct Vector3f VelocityElapsed, EulerOrientElapsed, EulerRotateElapsed;

	EntityTransformUpdateElapsed(Entity, &VelocityElapsed, &EulerOrientElapsed, &EulerRotateElapsed, ElapsedTime);

	EntityTransformUpdatePosition(Entity, &VelocityElapsed, ElapsedTime);

	EntityTransformUpdateOrientation(Entity, &EulerOrientElapsed, ElapsedTime);

	EntityTransformUpdateRotation(Entity, &EulerRotateElapsed, ElapsedTime);

	EntityTransformUpdateWorldMatrix(Entity);
}
