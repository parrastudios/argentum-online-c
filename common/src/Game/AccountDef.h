/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GAME_ACCOUNT_DEFINITIONS_H
#define GAME_ACCOUNT_DEFINITIONS_H

////////////////////////////////////////////////////////////
// Header constraints
////////////////////////////////////////////////////////////
#ifndef GAME_ACCOUNT_H
#	error Game/Account must be included instead
#endif

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Character.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define ACCOUNT_PLAYER_NO_LOGGED		0xFFFFFFFF
#define ACCOUNT_NAME_MAX_SIZE			0x40
#define ACCOUNT_PASS_MAX_SIZE			0x40
#define ACCOUNT_MAIL_MAX_SIZE			0xFF
#define ACCOUNT_PLAYER_MAX_SIZE			0x06

#define ACCOUNT_LOG_IN_ERROR_NONE		0x00
#define ACCOUNT_LOG_IN_ERROR_LOGGED		0x01
#define ACCOUNT_LOG_IN_ERROR_BAN		0x02
#define ACCOUNT_LOG_IN_ERROR_PASSWORD	0x03
#define ACCOUNT_LOG_IN_ERROR_NAME		0x04

#define	ACCOUNT_LOG_OUT_ERROR_NONE		0x00
#define ACCOUNT_LOG_OUT_ERROR_NOT_EXIT	0x01

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct AccountPlayerInfo
{
	char							Name[PLAYER_NAME_MAX_SIZE];
	uint16							Level;
	uint32							Gold;
	bool							Dead;

	struct PlayerAparenceInfo
	{
		uint32 Head;
		uint32 Body;
		uint32 Footwear;
	}								Aparence;
};

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#define AccountPlayerInfoCopy(PlayerInfoDest, PlayerInfoSrc)						\
	do																				\
	{																				\
		strcpy((PlayerInfoDest)->Name, (PlayerInfoSrc)->Name);						\
		(PlayerInfoDest)->Level = (PlayerInfoSrc)->Level;							\
		(PlayerInfoDest)->Gold = (PlayerInfoSrc)->Gold;								\
		(PlayerInfoDest)->Dead = (PlayerInfoSrc)->Dead;								\
		(PlayerInfoDest)->Aparence.Head = (PlayerInfoSrc)->Aparence.Head;			\
		(PlayerInfoDest)->Aparence.Body = (PlayerInfoSrc)->Aparence.Body;			\
		(PlayerInfoDest)->Aparence.Footwear = (PlayerInfoSrc)->Aparence.Footwear;	\
	} while (0)

#define AccountPlayerInfoCopyPlayer(PlayerInfo, Player)					\
	do																	\
	{																	\
		strcpy((PlayerInfo)->Name, (Player)->Attributes.Player.Name);	\
		(PlayerInfo)->Level = (Player)->Attributes.Player.Stats.Level;	\
		(PlayerInfo)->Gold = (Player)->Attributes.Player.Stats.Gold;	\
		(PlayerInfo)->Dead = (Player)->Attributes.Player.Flags.Dead;	\
		(PlayerInfo)->Aparence.Head = (Player)->Aparence.Head;			\
		(PlayerInfo)->Aparence.Body = (Player)->Aparence.Body;			\
		(PlayerInfo)->Aparence.Footwear = (Player)->Aparence.Footwear;	\
	} while (0)

#endif // GAME_ACCOUNT_DEFINITIONS_H
