/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Component.h>
#include <World/Entity.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Array holding size of each component data
////////////////////////////////////////////////////////////
static const uinteger ComponentDataSize[COMPONENT_DATA_SIZE] = {
	sizeof(struct ComponentBaseData) - 1,
	sizeof(struct ComponentTransformData),
	sizeof(struct ComponentCollisionData) - 1,
	sizeof(struct ComponentCollisionBoxData),
	sizeof(struct ComponentCollisionSphereData),
	sizeof(struct ComponentCollisionFrustumData),
	sizeof(struct ComponentAnimationData) - 1,
	sizeof(struct ComponentPhysicsData) - 1,
	sizeof(struct ComponentRenderData) - 1,
	sizeof(struct ComponentOcclusionData) - 1,
	sizeof(struct ComponentSoundData)
};

////////////////////////////////////////////////////////////
/// Table of default component types
////////////////////////////////////////////////////////////
#define DEFAULT_COMPONENT_TYPES \
		EntityComponentTypesDefine( \
			(WORLD_BUILD_TYPE_BASE, COMPONENT_DATA_BASE, &EntityComponentBaseImpl), \
			(WORLD_BUILD_TYPE_TRANSFORM, COMPONENT_DATA_TRANSFORM, &EntityComponentTransformImpl), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION, &EntityComponentCollisionImpl), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION_BOX, &EntityComponentCollisionBoxImpl), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION_SPHERE, &EntityComponentCollisionSphereImpl), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION_FRUSTUM, &EntityComponentCollisionFrustumImpl), \
			(WORLD_BUILD_TYPE_ANIMATION, COMPONENT_DATA_ANIMATION, &EntityComponentAnimationImpl), \
			(WORLD_BUILD_TYPE_PHYSICS, COMPONENT_DATA_PHYSICS, &EntityComponentPhysicsImpl), \
			(WORLD_BUILD_TYPE_RENDER, COMPONENT_DATA_RENDER, &EntityComponentRenderImpl), \
			(WORLD_BUILD_TYPE_OCCLUSION, COMPONENT_DATA_OCCLUSION, &EntityComponentOcclusionImpl), \
			(WORLD_BUILD_TYPE_SOUND, COMPONENT_DATA_SOUND, &EntityComponentSoundImpl) \
		)

const ComponentTypesImpl ComponentTypesDefault = {
	DEFAULT_COMPONENT_TYPES
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Map between component data type id and its type id
////////////////////////////////////////////////////////////
uinteger ComponentDataInfoType(uinteger DataType)
{
	static const struct ComponentDataTypeInfo
	{
		uinteger							Type;		//< Type associated to data component
	}
	ComponentDataInfoTypeMap[COMPONENT_DATA_SIZE] =
	{
			{ COMPONENT_TYPE_BASE },					//< COMPONENT_DATA_BASE
			{ COMPONENT_TYPE_TRANSFORM },				//< COMPONENT_DATA_TRANSFORM
			{ COMPONENT_TYPE_COLLISION },				//< COMPONENT_DATA_COLLISION
			{ COMPONENT_TYPE_COLLISION },				//< COMPONENT_DATA_COLLISION_BOX
			{ COMPONENT_TYPE_COLLISION },				//< COMPONENT_DATA_COLLISION_SPHERE
			{ COMPONENT_TYPE_COLLISION },				//< COMPONENT_DATA_COLLISION_FRUSTUM
			{ COMPONENT_TYPE_ANIMATION },				//< COMPONENT_DATA_ANIMATION
			{ COMPONENT_TYPE_PHYSICS },					//< COMPONENT_DATA_PHYSICS
			{ COMPONENT_TYPE_RENDER },					//< COMPONENT_DATA_RENDER
			{ COMPONENT_TYPE_OCCLUSION },				//< COMPONENT_DATA_OCCLUSION
			{ COMPONENT_TYPE_SOUND }					//< COMPONENT_DATA_SOUND
	};

	return ComponentDataInfoTypeMap[DataType].Type;
}

////////////////////////////////////////////////////////////
///  Map between component type and its tag
////////////////////////////////////////////////////////////
const char * ComponentTag(uinteger TypeId)
{
	static struct ComponentTagType
	{
		const char							Name[0x60];	//< Tag descriptor
	}
	ComponentTagMap[COMPONENT_TYPE_SIZE] =
	{
			{ "Base" },									//< COMPONENT_TYPE_BASE
			{ "Transform" },							//< COMPONENT_TYPE_TRANSFORM
			{ "Collision" },							//< COMPONENT_TYPE_COLLISION
			{ "Animation" },							//< COMPONENT_TYPE_ANIMATION
			{ "Physics" },								//< COMPONENT_TYPE_PHYSICS
			{ "Render" },								//< COMPONENT_TYPE_RENDER
			{ "Occlusion" },							//< COMPONENT_DATA_OCCLUSION
			{ "Sound" }									//< COMPONENT_DATA_SOUND
	};

	return ComponentTagMap[TypeId].Name;
}

////////////////////////////////////////////////////////////
/// Definition of interface base (null type implementation)
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentBaseImpl()
{
	static const struct ComponentImplType ComponentBaseImpl = {
		ExprInit(Base, TypeInit(Create, NULL), TypeInit(Destroy, NULL)),
		TypeInit(Type, COMPONENT_TYPE_BASE),
		UnionInit(Attributes, Base, &EntityComponentBaseImpl)
	};

	return &ComponentBaseImpl;
}

////////////////////////////////////////////////////////////
/// Create a component
////////////////////////////////////////////////////////////
struct ComponentType * ComponentCreate(	struct EntityType * Entity,
										struct ComponentImplData * DataImpl,
										struct ComponentImplType * TypeImpl)
{
	struct ComponentType * Component = NULL;

	// Create component by its data
	if (DataImpl != NULL)
	{
		// If not attribute defined (automatically managed)
		if (DataImpl->Attributes == NULL)
		{
			// Calculate the size of component (component size, data type size, derived data size)
			uinteger Size = ComponentGetSize(DataImpl->Type, DataImpl->Size);

			// Allocate it automatically by default
			Component = (struct ComponentType *)MemoryAllocate(Size);

			if (Component)
			{
				// Copy the data type
				Component->DataImpl.Type = DataImpl->Type;

                // Set the attributes (set to null because pointer must be recalculated each access)
                Component->DataImpl.Attributes = NULL; // ComponentGetDataImpl(Component);

				// Deallocation done automatically
				Component->Managed = false;
			}
		}
		else
		{
			Component = (struct ComponentType *)MemoryAllocate(sizeof(struct ComponentType));

			if (Component)
			{
				// Copy the data type
				Component->DataImpl.Type = DataImpl->Type;

				// Copy the size of component
				Component->DataImpl.Size = DataImpl->Size;

				// Assign data to the entity
				Component->DataImpl.Attributes = DataImpl->Attributes;

				// Data deallocation will be done by manager
				Component->Managed = true;
			}
		}
	}
	else
	{
		Component = (struct ComponentType *)MemoryAllocate(sizeof(struct ComponentType));

		if (Component)
		{
			// Delegate creation of data object in the component create method
			Component->DataImpl.Type = COMPONENT_DATA_BASE;
			Component->DataImpl.Size = 0;
			Component->DataImpl.Attributes = NULL;

			// Data allocation / deallocation will be done by manager
			Component->Managed = true;
		}
	}

	// Create component type
	return ComponentCreateType(Component, Entity, TypeImpl);
}

////////////////////////////////////////////////////////////
/// Initialize a component previously created
////////////////////////////////////////////////////////////
struct ComponentType * ComponentInitialize(	struct ComponentType * Component,
											struct EntityType * Entity,
											struct ComponentImplData * DataImpl,
											struct ComponentImplType * TypeImpl)
{
	if (Entity)
	{
		// Create component by its data
		if (DataImpl != NULL)
		{
			// If not attribute defined (automatically managed)
			if (DataImpl->Attributes == NULL && Component == NULL)
			{
				// Calculate the size of component (component size, data type size, derived data size)
				uinteger Size = ComponentGetSize(DataImpl->Type, DataImpl->Size);

				// Allocate it automatically by default
				Component = (struct ComponentType *)MemoryAllocate(Size);

				if (Component)
				{
					// Copy the data type
					Component->DataImpl.Type = DataImpl->Type;

					// Set the attributes (set to null because pointer must be recalculated each access)
					Component->DataImpl.Attributes = NULL; // ComponentGetDataImpl(Component);

					// Deallocation done automatically
					Component->Managed = false;
				}
			}
			else
			{
				if (Component)
				{
					// Copy the data type
					Component->DataImpl.Type = DataImpl->Type;

					// Copy the size of component
					Component->DataImpl.Size = DataImpl->Size;

					// Assign data to the entity
					Component->DataImpl.Attributes = DataImpl->Attributes;

					// Data deallocation will be done by manager
					Component->Managed = true;
				}
			}
		}
		else
		{
			if (Component)
			{
				// Delegate creation of data object in the component create method
				Component->DataImpl.Type = COMPONENT_DATA_BASE;
				Component->DataImpl.Size = 0;
				Component->DataImpl.Attributes = NULL;

				// Data allocation / deallocation will be done by manager
				Component->Managed = true;
			}
		}

		// Create component type
		return ComponentCreateType(Component, Entity, TypeImpl);
	}

	return Component;
}

////////////////////////////////////////////////////////////
/// Create type of component with data previously initialized
////////////////////////////////////////////////////////////
struct ComponentType * ComponentCreateType(	struct ComponentType * Component,
											struct EntityType * Entity,
											struct ComponentImplType * TypeImpl)
{
	// Define the type, tag and attach to entity
	if (Component)
	{
		uinteger TypeId = ComponentDataInfoType(Component->DataImpl.Type);

		// Check if derived type implementation or not
		if (TypeImpl != NULL)
		{
			// Initialize as a derived type component
			Component->TypeImpl = TypeImpl;
		}
		else
		{
			// Initialize as a basic component
			Component->TypeImpl = ComponentTypesDefault[Component->DataImpl.Type].Impl();
		}

		// Check if implementation exists and for type safety
		if (Component->TypeImpl && Component->TypeImpl->Type == TypeId)
		{
			// Initialize tag data
			Component->Tag = ComponentTag(TypeId);

			// Attach component to the entity
			if (Entity)
			{
				EntityAttachComponent(Entity, Component);
			}

			// Initialize component
			if (Component->TypeImpl->Base.Create)
			{
				Component->TypeImpl->Base.Create(Entity);
			}

			// If derivated type, link it and call the constructor
			if (Entity->Factory->ComponentTypes[Component->DataImpl.Type].Impl() != NULL && Entity->Factory->ComponentTypes[Component->DataImpl.Type].Impl()->Type == TypeId)
			{
				// Link derivated constructor
				Component->TypeImpl = Entity->Factory->ComponentTypes[Component->DataImpl.Type].Impl();

				// Initialize derivated component
				if (Component->TypeImpl->Base.Create)
				{
					Component->TypeImpl->Base.Create(Entity);
				}
			}

			return Component;
		}
		else
		{
			MemoryDeallocate(Component);
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Get the size of a component including its data block
////////////////////////////////////////////////////////////
uinteger ComponentGetSize(uinteger DataType, uinteger DerivedSize)
{
	return sizeof(struct ComponentType) + ComponentDataSize[DataType] + DerivedSize;
}

////////////////////////////////////////////////////////////
/// Get attributes of a component as a generic pointer
////////////////////////////////////////////////////////////
union ComponentAttributesData * ComponentGetAttributes(struct ComponentType * Component)
{
    if (Component)
    {
        if (Component->DataImpl.Attributes)
        {
            // Custom management
            return Component->DataImpl.Attributes;
        }
        else
        {
            // Automatically managed
            return ComponentGetDataImpl(Component);
        }
    }

    return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a component
////////////////////////////////////////////////////////////
void ComponentDestroy(struct ComponentType * Component)
{
	if (Component)
	{
		// Detach from entity
		if (Component->EntityId != ENTITY_INDEX_INVALID)
		{
		    struct EntityType * Entity = EntityGet(Component->EntityFactory, Component->EntityId);

		    if (Entity)
            {
                // Destroy component
                if (Component->TypeImpl->Base.Destroy)
                {
                    Component->TypeImpl->Base.Destroy(Entity);
                }

                // Destroy base component
                if (ComponentTypesDefault[Component->DataImpl.Type].Impl()->Base.Destroy &&
					ComponentTypesDefault[Component->DataImpl.Type].Impl()->Base.Destroy != Component->TypeImpl->Base.Destroy)
                {
					ComponentTypesDefault[Component->DataImpl.Type].Impl()->Base.Destroy(Entity);
                }

                // Detach from entity
                EntityDetachComponent(Entity, Component);
            }
		}

		// Set tag reference
		Component->Tag = NULL;

		// Set impl reference
		Component->TypeImpl = NULL;

		// Set data attributes reference
		Component->DataImpl.Attributes = NULL;

		if (!Component->Managed)
		{
			// Deallocate data automatically
			MemoryDeallocate(Component);
		}
	}
}
