/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/Storage.h>
#include <DataType/List.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define METADATA_STORAGE_POOL_SIZE_DEFAULT		UINTEGER_SUFFIX(0x07FF)
#define METADATA_STORAGE_INVALID_DATA_PTR		UINTEGER_MAX_RANGE
#define METADATA_STORAGE_POOL_DEFRAG_RATIO		0.25f
#define METADATA_STORAGE_POOL_CAPACITY_RATIO	0.75f
#define METADATA_STORAGE_POOL_REALLOC_FACTOR	2

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef uint8						MetadataStorageDataType;
typedef MetadataStorageDataType *	MetadataStorageData;
typedef uinteger					MetadataStorageDataPtr;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
typedef struct MetadataStorageDataRegionType
{
	MetadataStorageDataPtr			Ptr;			///< Index refering to MetadataStorageData
	MetadataStorageDataSize			Size;			///< Size of the block

} MetadataStorageDataRegionType;

typedef struct MetadataStorageIndexRegionPair
{
	MetadataStorageIndexType		Index;			///< Index of the resource
	MetadataStorageDataRegionType	Region;			///< Region of the resource
} MetadataStorageIndexRegionPair;

typedef struct MetadataStoragePool
{
	MetadataStorageData				Data;			///< Data of the pool
	MetadataStorageDataPtr			DataCurrentPtr;	///< Current pointer of the pool
	MetadataStorageDataSize			DataSize;		///< Size of data allocated
	MetadataStorageDataSize			GapSize;		///< Size in bytes of gap data
	List							GapList;		///< List of gaps in data pool (MetadataStorageDataRegionType)
	Set								ResourceList;	///< Set containing pointers to the data (MetadataStorageIndexType -> MetadataStorageIndexRegionPair)
	MetadataStorageIndexType		LastIndex;		///< Last unused resource index
	List							FreeIdList;		///< List referencing to free ids of the set (MetadataStorageIndexType)
} MetadataStoragePool;

struct MetadataStorageType
{
	MetadataStoragePool				Pool[METADATA_STORAGE_RESOURCE_SIZE];

	// ...
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get global storage instance
////////////////////////////////////////////////////////////
MetadataStorageType MetadataStorageGlobalInstance()
{
	static MetadataStorageType GlobalStorageInstance = NULL;

	if (GlobalStorageInstance == NULL)
	{
		GlobalStorageInstance = MetadataStorageCreate();
	}

	return GlobalStorageInstance;
}

////////////////////////////////////////////////////////////
/// Generate a hash for a resource
////////////////////////////////////////////////////////////
MetadataStorageIndexType MetadataStorageResourceHash(void * IndexRegionPairPtr)
{
	if (IndexRegionPairPtr)
	{
		MetadataStorageIndexRegionPair * IndexRegionPair = IndexRegionPairPtr;

		return (MetadataStorageIndexType)IndexRegionPair->Index;
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Create a storage manager
////////////////////////////////////////////////////////////
MetadataStorageType MetadataStorageCreate()
{
	struct MetadataStorageType * Storage = MemoryAllocate(sizeof(struct MetadataStorageType));

	if (Storage)
	{
		MetadataStorageResourceType Type;

		for (Type = 0; Type < METADATA_STORAGE_RESOURCE_SIZE; ++Type)
		{
			MetadataStoragePool * Pool = &Storage->Pool[Type];

			Pool->Data = MemoryAllocate(METADATA_STORAGE_POOL_SIZE_DEFAULT * sizeof(MetadataStorageDataType));

			if (Pool->Data)
			{
				Pool->DataCurrentPtr = 0;

				Pool->DataSize = METADATA_STORAGE_POOL_SIZE_DEFAULT;

				Pool->GapSize = 0;

				Pool->GapList = ListNew(sizeof(struct MetadataStorageDataRegionType));

				Pool->ResourceList = SetCreateHashOnly(sizeof(struct MetadataStorageIndexRegionPair), &MetadataStorageResourceHash);

				Pool->LastIndex = 0;

				Pool->FreeIdList = ListNew(sizeof(MetadataStorageIndexType));
			}
			else
			{
				Pool->DataCurrentPtr = 0;

				Pool->DataSize = 0;

				Pool->GapSize = 0;

				Pool->GapList = NULL;

				Pool->ResourceList = NULL;

				Pool->LastIndex = 0;

				Pool->FreeIdList = NULL;
			}
		}

		return Storage;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Defragment a storage pool
////////////////////////////////////////////////////////////
bool MetadataStorageDefragment(MetadataStorageType Storage, MetadataStorageResourceType ResourceType, MetadataStorageData Dest)
{
	if (Storage)
	{
		MetadataStoragePool * Pool = &Storage->Pool[ResourceType];

		struct SetIteratorType Iterator;

		MetadataStorageDataPtr CurrentPtr = 0;

		SetForEach(Pool->ResourceList, &Iterator)
		{
			MetadataStorageIndexRegionPair * Pair = (MetadataStorageIndexRegionPair *)SetIteratorValue(&Iterator);

			if (Pair)
			{
				MetadataStorageDataPtr NextPtr = CurrentPtr + Pair->Region.Size;

				if (NextPtr < Pool->DataSize)
				{
					MemoryCopy(&Dest[CurrentPtr], &Pool->Data[Pair->Region.Ptr], Pair->Region.Size);

					CurrentPtr = NextPtr;
				}
			}
		}

		Pool->DataCurrentPtr = CurrentPtr;

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Reallocate a storage pool
////////////////////////////////////////////////////////////
bool MetadataStorageReallocate(MetadataStorageType Storage, MetadataStorageResourceType ResourceType, MetadataStorageDataSize Request)
{
	if (Storage)
	{
		MetadataStoragePool * Pool = &Storage->Pool[ResourceType];

		uinteger DataSize = 0;

		MetadataStorageData Data = NULL;

		// Check if already used memory is greater than capacity ratio
		if ((((Pool->DataCurrentPtr + Request) - Pool->GapSize) / Pool->DataSize) >= METADATA_STORAGE_POOL_CAPACITY_RATIO)
		{
			DataSize = (Pool->DataSize * METADATA_STORAGE_POOL_REALLOC_FACTOR) * sizeof(MetadataStorageDataType);
		}
		else
		{
			DataSize = Pool->DataSize;
		}

		// Allocate memory block
		Data = (MetadataStorageData)MemoryAllocate(DataSize);

		if (Data != NULL)
		{
			// Sort properly all memory blocks
			if (MetadataStorageDefragment(Storage, ResourceType, Data))
			{
				// Successful defragmentation, clear old data block
				MemoryDeallocate(Pool->Data);

				// Set to the new data block
				Pool->Data = (MetadataStorageData)Data;
				Pool->DataSize = (MetadataStorageDataSize)DataSize;

				return true;
			}
			else
			{
				MemoryDeallocate(Data);
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Find a gap to fit the size in the storage pool
////////////////////////////////////////////////////////////
MetadataStorageDataPtr MetadataStorageFindGap(MetadataStorageType Storage, MetadataStorageResourceType ResourceType, MetadataStorageDataSize Size)
{
	if (Storage)
	{
		MetadataStoragePool * Pool = &Storage->Pool[ResourceType];

		// Try to find a gap
		if (Pool->GapSize >= Size)
		{
			ListIterator Iterator, EraseIt;
			MetadataStorageDataPtr DataPtr = METADATA_STORAGE_INVALID_DATA_PTR;

			// Iterate through gap list
			ListForEach(Pool->GapList, Iterator)
			{
				struct MetadataStorageDataRegionType * Gap = ListItDataPtr(Iterator, struct MetadataStorageDataRegionType);

				// Check if data fits on the gap
				if (Gap->Size >= Size)
				{
					DataPtr = Gap->Ptr;

					// Consume block space of the gap
					Gap->Size -= Size;
					Gap->Ptr += Size;

					// If gap becomes empty, remove it
					if (Gap->Size == 0)
					{
						EraseIt = Iterator;
					}

					// Update global gap size counter
					Pool->GapSize -= Size;

					break;
				}
			}

			// Erase gap if empty
			if (EraseIt)
			{
				ListEraseIt(Pool->GapList, EraseIt);
			}

			return DataPtr;
		}
	}

	return METADATA_STORAGE_INVALID_DATA_PTR;
}

////////////////////////////////////////////////////////////
/// Find a memory block on storage pool
////////////////////////////////////////////////////////////
MetadataStorageDataPtr MetadataStorageFindMemory(MetadataStorageType Storage, MetadataStorageResourceType ResourceType, MetadataStorageDataSize Size)
{
	if (Storage)
	{
		MetadataStoragePool * Pool = &Storage->Pool[ResourceType];

		if (Pool->Data)
		{
			uinteger FreeSize = Pool->DataSize - Pool->DataCurrentPtr;

			if (FreeSize < Size)
			{
				// If not enough space in the pool, find a gap
				MetadataStorageDataPtr DataPtr = MetadataStorageFindGap(Storage, ResourceType, Size);

				if (DataPtr != METADATA_STORAGE_INVALID_DATA_PTR)
				{
					// Return a valid memory address
					return DataPtr;
				}
				else
				{
					// Reallocate pool, not enough space available
					if (MetadataStorageReallocate(Storage, ResourceType, Size))
					{
						// If reallocation valid, try to find memory another time
						return MetadataStorageFindMemory(Storage, ResourceType, Size);
					}
					else
					{
						return METADATA_STORAGE_INVALID_DATA_PTR;
					}
				}
			}
			else
			{
				// Enough space in the pool, give next data ptr
				MetadataStorageDataPtr DataPtr = Pool->DataCurrentPtr;

				// Update data pointer
				Pool->DataCurrentPtr += Size;

				return DataPtr;
			}
		}
	}

	return METADATA_STORAGE_INVALID_DATA_PTR;
}

////////////////////////////////////////////////////////////
/// Find a memory block on storage pool and free it
////////////////////////////////////////////////////////////
bool MetadataStorageFreeMemory(MetadataStorageType Storage, MetadataStorageResourceType ResourceType, MetadataStorageDataPtr Ptr, MetadataStorageDataSize Size)
{
	if (Storage)
	{
		MetadataStoragePool * Pool = &Storage->Pool[ResourceType];

		// Check if memory iS the last block
		if (Pool->DataCurrentPtr == (Ptr + Size))
		{
			// Make that memory as unused
			Pool->DataCurrentPtr -= Size;
		}
		else
		{
			MetadataStorageDataRegionType Gap;

			// Initialize gap values
			Gap.Ptr = Ptr;
			Gap.Size = Size;

			// Insert into list
			ListPushBack(Pool->GapList, (void *)&Gap);

			// Increment global gap size
			Pool->GapSize += Size;

			// Check for defragmentation
			if ((Pool->GapSize / Pool->DataSize) >= METADATA_STORAGE_POOL_DEFRAG_RATIO)
			{
				MetadataStorageData Data = (MetadataStorageData)MemoryAllocate(Pool->DataSize * sizeof(MetadataStorageDataType));

				// On valid allocation
				if (Data != NULL)
				{
					// Defragment it
					if (MetadataStorageDefragment(Storage, ResourceType, Data))
					{
						// Clear old memory block
						MemoryDeallocate(Pool->Data);

						// Assing data to the new block
						Pool->Data = Data;
					}
					else
					{
						// Clear memory on error
						MemoryDeallocate(Data);
					}
				}
			}
		}

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Find a free resource and reserve it in the storage manager
////////////////////////////////////////////////////////////
MetadataStorageIndexType MetadataStorageFindResource(MetadataStorageType Storage, MetadataStorageResourceType ResourceType)
{
	if (Storage)
	{
		MetadataStoragePool * Pool = &Storage->Pool[ResourceType];

		if (!ListEmpty(Pool->FreeIdList))
		{
			MetadataStorageIndexType * Index = (MetadataStorageIndexType *)ListBack(Pool->FreeIdList);

			return *Index;
		}
		else
		{
			// Return the last index
			return Pool->LastIndex;
		}
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Free a resource in the storage manager
////////////////////////////////////////////////////////////
void MetadataStorageFreeResource(MetadataStorageType Storage, MetadataStorageResourceType ResourceType, MetadataStorageIndexType Index)
{
	if (Storage)
	{
		MetadataStoragePool * Pool = &Storage->Pool[ResourceType];

		// Remove resource from the pool
		SetRemoveByHash(Pool->ResourceList, Index);

		// Insert index into the free list
		ListPushBack(Pool->FreeIdList, (void *)&Index);
	}
}

////////////////////////////////////////////////////////////
/// Insert a resource in the storage manager
////////////////////////////////////////////////////////////
MetadataStorageIndexType MetadataStorageInsertResource(MetadataStorageType Storage, MetadataStorageResourceType ResourceType, MetadataStorageIndexType Index, MetadataStorageDataPtr DataPtr, MetadataStorageDataSize Size)
{
	if (Storage)
	{
		MetadataStoragePool * Pool = &Storage->Pool[ResourceType];

		MetadataStorageIndexRegionPair IndexRegionPair;

		// Set resource bucket
		IndexRegionPair.Index = Index;
		IndexRegionPair.Region.Ptr = DataPtr;
		IndexRegionPair.Region.Size = Size;

		// Insert into resource list
		if (SetInsert(Pool->ResourceList, (void *)&IndexRegionPair))
		{
			// Remove from free list if need
			if (!ListEmpty(Pool->FreeIdList))
			{
				MetadataStorageIndexType * Back = (MetadataStorageIndexType *)ListBack(Pool->FreeIdList);

				if (Index == *Back)
				{
					// Remove index from free list
					ListPopBack(Pool->FreeIdList);
				}
			}
			else
			{
				// Increment by one the last used index
				++Pool->LastIndex;
			}

			return Index;
		}
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Allocate a resource in the storage manager
////////////////////////////////////////////////////////////
MetadataStorageIndexType MetadataStorageAllocateResource(MetadataStorageType Storage, MetadataStorageResourceType ResourceType, MetadataStorageDataSize Size)
{
	if (Storage)
	{
		// MetadataStoragePool * Pool = &Storage->Pool[ResourceType];

		MetadataStorageDataPtr DataPtr = MetadataStorageFindMemory(Storage, ResourceType, Size);

		if (DataPtr != METADATA_STORAGE_INVALID_DATA_PTR)
		{
			// Reserve a resource index
			MetadataStorageIndexType Index = MetadataStorageFindResource(Storage, ResourceType);

			// If there is available index
			if (Index != METADATA_STORAGE_INDEX_INVALID)
			{
				// Insert resource into the set
				return MetadataStorageInsertResource(Storage, ResourceType, Index, DataPtr, Size);
			}
		}
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Get a resource from the storage manager
////////////////////////////////////////////////////////////
void * MetadataStorageGetResource(MetadataStorageType Storage, MetadataStorageResourceType ResourceType, MetadataStorageIndexType Index)
{
	if (Storage)
	{
		MetadataStoragePool * Pool = &Storage->Pool[ResourceType];

		// Find resource in the list
		MetadataStorageIndexRegionPair * IndexRegionPair = (MetadataStorageIndexRegionPair *)SetGetValueByHash(Pool->ResourceList, Index);

		if (IndexRegionPair)
		{
			return &Pool->Data[IndexRegionPair->Region.Ptr];
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Deallocate a resource in the storage manager
////////////////////////////////////////////////////////////
bool MetadataStorageDeallocateResource(MetadataStorageType Storage, MetadataStorageResourceType ResourceType, MetadataStorageIndexType Index)
{
	if (Storage)
	{
		MetadataStoragePool * Pool = &Storage->Pool[ResourceType];

		// Find resource in the list
		MetadataStorageIndexRegionPair * IndexRegionPair = (MetadataStorageIndexRegionPair *)SetGetValueByHash(Pool->ResourceList, Index);

		if (IndexRegionPair)
		{
			MetadataStorageDataPtr Ptr = IndexRegionPair->Region.Ptr;
			MetadataStorageDataSize Size = IndexRegionPair->Region.Size;

			// Remove resource from list
			MetadataStorageFreeResource(Storage, ResourceType, Index);

			// Remove memory associated to resource index
			return MetadataStorageFreeMemory(Storage, ResourceType, Ptr, Size);
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Clear a storage manager
////////////////////////////////////////////////////////////
void MetadataStorageClear(MetadataStorageType Storage)
{
	if (Storage)
	{
		MetadataStorageResourceType Type;

		for (Type = 0; Type < METADATA_STORAGE_RESOURCE_SIZE; ++Type)
		{
			MetadataStoragePool * Pool = &Storage->Pool[Type];

			if (Pool->Data)
			{
				MemoryDeallocate(Pool->Data);
			}

			Pool->DataCurrentPtr = 0;

			Pool->DataSize = 0;

			Pool->GapSize = 0;

			ListClear(Pool->GapList);

			SetClear(Pool->ResourceList);

			ListClear(Pool->FreeIdList);

			Pool->Data = MemoryAllocate(METADATA_STORAGE_POOL_SIZE_DEFAULT * sizeof(MetadataStorageDataType));
		}
	}
}

////////////////////////////////////////////////////////////
/// Destroy a storage manager
////////////////////////////////////////////////////////////
void MetadataStorageDestroy(MetadataStorageType Storage)
{
	if (Storage)
	{
		MetadataStorageResourceType Type;

		for (Type = 0; Type < METADATA_STORAGE_RESOURCE_SIZE; ++Type)
		{
			MetadataStoragePool * Pool = &Storage->Pool[Type];

			Pool->DataCurrentPtr = 0;

			Pool->DataSize = 0;

			Pool->GapSize = 0;

			ListDestroy(Pool->GapList);

			SetDestroy(Pool->ResourceList);

			Pool->LastIndex = 0;

			ListDestroy(Pool->FreeIdList);

			MemoryDeallocate(Pool->Data);
		}
	}
}
