/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Memory/General.h>
#include <Math/Geometry/Bounding/Frustum.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Check if frustum flags are inside
////////////////////////////////////////////////////////////
bool FrustumInside(uinteger Flags)
{
    return (Flags == FRUSTUM_INSIDE);
}

////////////////////////////////////////////////////////////
/// Check if frustum flags are outside
////////////////////////////////////////////////////////////
bool FrustumOutside(uinteger Flags)
{
    return (Flags == FRUSTUM_OUTSIDE);
}

////////////////////////////////////////////////////////////
/// Check if frustum flags are overlapping
////////////////////////////////////////////////////////////
bool FrustumOverlap(uinteger Flags)
{
    return (Flags & (FRUSTUM_OVERLAP));
}

////////////////////////////////////////////////////////////
/// Create a frustum type
////////////////////////////////////////////////////////////
struct FrustumType * FrustumCreate()
{
	struct FrustumType * Frustum = MemoryAllocate(sizeof(struct FrustumType));

	return Frustum;
}

////////////////////////////////////////////////////////////
/// Copy a frustum type
////////////////////////////////////////////////////////////
void FrustumCopy(struct FrustumType * Dest, struct FrustumType * Src)
{
	if (Dest && Src)
	{
		uinteger i;

		for (i = 0; i < FRUSTUM_PLANES_SIZE; ++i)
		{
			PlaneCopy(&Dest->Planes[i], &Src->Planes[i]);
		}
	}
}

////////////////////////////////////////////////////////////
/// Initialize a frustum type
////////////////////////////////////////////////////////////
struct FrustumType * FrustumInitialize(struct FrustumType * Frustum)
{
	// ...

	return Frustum;
}

////////////////////////////////////////////////////////////
/// Destroy a frustum type
////////////////////////////////////////////////////////////
void FrustumDestroy(struct FrustumType * Frustum)
{
	if (Frustum)
	{
		MemoryDeallocate(Frustum);
	}
}

////////////////////////////////////////////////////////////
/// Checks if a point is in the viewing volume
////////////////////////////////////////////////////////////
bool FrustumCheckPoint(struct FrustumType * Frustum, struct Vector3f * Point)
{
	if (Frustum)
	{
		uinteger i;

		// Check if the point is inside all six planes of the view frustum
		for (i = 0; i < FRUSTUM_PLANES_SIZE; ++i)
		{
			// if (Frustum->Planes[i].a * Point->x + Frustum->Planes[i].b * Point->y + Frustum->Planes[i].c * Point->z <= 0.0f)
			if (PlaneDotCoord(&Frustum->Planes[i], Point) <= 0.0f)
				return false;
		}

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Checks if a sphere is in the viewing volume
////////////////////////////////////////////////////////////
uinteger FrustumCheckSpherePosition(struct FrustumType * Frustum, struct BoundingSphere * Sphere)
{
	if (Frustum && Sphere)
	{
		uinteger i, Count;
		float Distance;

		// Check if the radius of the sphere is inside the view frustum
		for (i = 0, Count = 0; i < FRUSTUM_PLANES_SIZE; ++i)
		{
			Distance = PlaneDotCoord(&Frustum->Planes[i], &Sphere->Center);

			if (Distance <= -Sphere->Radius)
			{
				return FRUSTUM_OUTSIDE;
			}

			if (Distance > Sphere->Radius)
			{
				++Count;
			}
		}

		return (Count == FRUSTUM_PLANES_SIZE) ? FRUSTUM_INSIDE : FRUSTUM_OVERLAP;
	}

	return FRUSTUM_OUTSIDE;
}

////////////////////////////////////////////////////////////
/// Checks if a sphere is in the viewing volume, return
/// the distance to the frustum on success
////////////////////////////////////////////////////////////
float FrustumCheckSphereDistance(struct FrustumType * Frustum, struct BoundingSphere * Sphere)
{
	if (Frustum && Sphere)
	{
		uinteger i;
		float Distance;

		// Check if the radius of the sphere is inside the view frustum
		for (i = 0; i < FRUSTUM_PLANES_SIZE; ++i)
		{
			Distance = PlaneDotCoord(&Frustum->Planes[i], &Sphere->Center);

			if (Distance < -Sphere->Radius)
			{
				return 0;
			}
		}

		return Distance + Sphere->Radius;
	}

	return -1.0f;
}

////////////////////////////////////////////////////////////
/// Checks if a cube is in the viewing volume
////////////////////////////////////////////////////////////
bool FrustumCheckCube(struct FrustumType * Frustum, struct Vector3f * Position, float Radius)
{
	if (Frustum && Position)
	{
		uinteger i;

		// Check if any one point of the cube is in the view frustum
		for (i = 0; i < FRUSTUM_PLANES_SIZE; ++i)
		{
			struct Vector3f VectorAux;

			Vector3fSet(&VectorAux, Position->x - Radius, Position->y - Radius, Position->z - Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x + Radius, Position->y - Radius, Position->z - Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x - Radius, Position->y + Radius, Position->z - Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x + Radius, Position->y + Radius, Position->z - Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x - Radius, Position->y - Radius, Position->z + Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x + Radius, Position->y - Radius, Position->z + Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x - Radius, Position->y + Radius, Position->z + Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x + Radius, Position->y + Radius, Position->z + Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			return false;
		}

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Checks if a cube is in the viewing volume (indicating where is it)
////////////////////////////////////////////////////////////
uinteger FrustumCheckCubePosition(struct FrustumType * Frustum, struct Vector3f * Position, float Radius)
{
	if (Frustum && Position)
	{
		uinteger i, j, k;

		// Check if any one point of the cube is in the view frustum
		for (i = 0, j = 0; i < FRUSTUM_PLANES_SIZE; ++i)
		{
			struct Vector3f VectorAux;

			k = 0;

			Vector3fSet(&VectorAux, Position->x - Radius, Position->y - Radius, Position->z - Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				k++;

			Vector3fSet(&VectorAux, Position->x + Radius, Position->y - Radius, Position->z - Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				k++;

			Vector3fSet(&VectorAux, Position->x - Radius, Position->y + Radius, Position->z - Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				k++;

			Vector3fSet(&VectorAux, Position->x + Radius, Position->y + Radius, Position->z - Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				k++;

			Vector3fSet(&VectorAux, Position->x - Radius, Position->y - Radius, Position->z + Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				k++;

			Vector3fSet(&VectorAux, Position->x + Radius, Position->y - Radius, Position->z + Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				k++;

			Vector3fSet(&VectorAux, Position->x - Radius, Position->y + Radius, Position->z + Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				k++;

			Vector3fSet(&VectorAux, Position->x + Radius, Position->y + Radius, Position->z + Radius);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				k++;

			if (k == 0)
				return FRUSTUM_OUTSIDE;

			if (k == 8)
				++j;
		}

		return (j == FRUSTUM_PLANES_SIZE) ? FRUSTUM_INSIDE : FRUSTUM_OVERLAP;
	}

	return FRUSTUM_OUTSIDE;
}

////////////////////////////////////////////////////////////
/// Checks if a rectangle is in the viewing volume
////////////////////////////////////////////////////////////
bool FrustumCheckRectangle(struct FrustumType * Frustum, struct Vector3f * Position, struct Vector3f * Size)
{
	if (Frustum && Position && Size)
	{
		uinteger i;

		// Check if any of the 6 planes of the rectangle are inside the view frustum
		for (i = 0; i < FRUSTUM_PLANES_SIZE; ++i)
		{
			struct Vector3f VectorAux;

			Vector3fSet(&VectorAux, Position->x - Size->x, Position->y - Size->y, Position->z - Size->z);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x + Size->x, Position->y - Size->y, Position->z - Size->z);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x - Size->x, Position->y + Size->y, Position->z - Size->z);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x - Size->x, Position->y - Size->y, Position->z + Size->z);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x + Size->x, Position->y + Size->y, Position->z - Size->z);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x + Size->x, Position->y - Size->y, Position->z + Size->z);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x - Size->x, Position->y + Size->y, Position->z + Size->z);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			Vector3fSet(&VectorAux, Position->x + Size->x, Position->y + Size->y, Position->z + Size->z);

			if (PlaneDotCoord(&Frustum->Planes[i], &VectorAux) >= 0.0f)
				continue;

			return false;
		}

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Checks if a polygon is in the viewing volume
////////////////////////////////////////////////////////////
bool FrustumCheckPolygon(struct FrustumType * Frustum, struct Vector3f * Points, uinteger PointSize)
{
	if (Frustum && Points)
	{
		uinteger i, j;

		// Check if the polygon is inside the view frustum
		for (i = 0; i < FRUSTUM_PLANES_SIZE; ++i)
		{
			for (j = 0; j < PointSize; ++j)
			{
				if (PlaneDotCoord(&Frustum->Planes[i], &Points[j]) > 0.0f)
					break;
			}

			if (j == PointSize)
				return false;
		}

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Check if a bounding box is colliding with a view frustum volume
////////////////////////////////////////////////////////////
uinteger FrustumCheckBoundingBox(struct FrustumType * Frustum, struct BoundingBox * Box, uinteger ClipFlags)
{
	if (Frustum && Box)
	{
		// If parent box is wholly inside the frustum there is no need to test again
        if (FrustumInside(ClipFlags))
		{
		    return ClipFlags;
		}
		else
        {
            #if 0
            // todo: optimized version not working properly
            uinteger Side, i;

			// For each clip plane
			for (i = 0; i < FRUSTUM_PLANES_SIZE; ++i)
			{
				// If parent box is wholly inside this frustum plane there is no need to test again
				if (FrustumInside(ClipFlags & (2 << i)))
					continue;

				Side = BoundingBoxCollisionPlane(Box, &Frustum->Planes[i]);

				// If box is wholly outside the clipping plane
				if (Side == BOUNDING_BOX_OUTSIDE)
				{
					// Reject the bounding box completely
					return FRUSTUM_OUTSIDE;
				}

				// If box is wholly inside the clipping plane
				if (Side == BOUNDING_BOX_INSIDE)
				{
					// Accept the bounding box - turn off the clip flag
					ClipFlags &= ~(2 << i);
				}
			}

            return ClipFlags;
            #else
            uinteger i;

            ClipFlags = FRUSTUM_INSIDE;

            for (i = 0; i < FRUSTUM_PLANES_SIZE; ++i)
            {
                struct Vector3f PositiveCorner, NegativeCorner;

                BoundingBoxGetVertexPositive(Box, &Frustum->Planes[i].v, &PositiveCorner);
                BoundingBoxGetVertexNegative(Box, &Frustum->Planes[i].v, &NegativeCorner);

                if (PlaneCheckPoint(&Frustum->Planes[i], &PositiveCorner))
                {
                    return FRUSTUM_OUTSIDE;
                }
                else if (PlaneCheckPoint(&Frustum->Planes[i], &NegativeCorner))
                {
                    ClipFlags = FRUSTUM_OVERLAP;
                }
            }

            return ClipFlags;
            #endif
		}
	}

	return FRUSTUM_OUTSIDE;
}

////////////////////////////////////////////////////////////
/// Normalize frustum planes and update sing bits
////////////////////////////////////////////////////////////
void FrustumUpdatePlanes(struct FrustumType * Frustum)
{
	uinteger i;

	for (i = 0; i < FRUSTUM_PLANES_SIZE; ++i)
	{
		PlaneNormalize(&Frustum->Planes[i]);
		PlaneSetSignBits(&Frustum->Planes[i]);
	}
}

////////////////////////////////////////////////////////////
/// Update frustum using a view projection matrix
////////////////////////////////////////////////////////////
void FrustumUpdate(struct FrustumType * Frustum, struct Matrix4f * ViewProj)
{
	if (Frustum && ViewProj)
	{
		// Calculate the left plane of frustum
		Frustum->Planes[FRUSTUM_LEFT].a = ViewProj->v[3] + ViewProj->v[0];
		Frustum->Planes[FRUSTUM_LEFT].b = ViewProj->v[7] + ViewProj->v[4];
		Frustum->Planes[FRUSTUM_LEFT].c = ViewProj->v[11] + ViewProj->v[8];
		Frustum->Planes[FRUSTUM_LEFT].d = ViewProj->v[15] + ViewProj->v[12];

		// Calculate right plane of frustum
		Frustum->Planes[FRUSTUM_RIGHT].a = ViewProj->v[3] - ViewProj->v[0];
		Frustum->Planes[FRUSTUM_RIGHT].b = ViewProj->v[7] - ViewProj->v[4];
		Frustum->Planes[FRUSTUM_RIGHT].c = ViewProj->v[11] - ViewProj->v[8];
		Frustum->Planes[FRUSTUM_RIGHT].d = ViewProj->v[15] - ViewProj->v[12];

		// Calculate the bottom plane of frustum
		Frustum->Planes[FRUSTUM_BOTTOM].a = ViewProj->v[3] + ViewProj->v[1];
		Frustum->Planes[FRUSTUM_BOTTOM].b = ViewProj->v[7] + ViewProj->v[5];
		Frustum->Planes[FRUSTUM_BOTTOM].c = ViewProj->v[11] + ViewProj->v[9];
		Frustum->Planes[FRUSTUM_BOTTOM].d = ViewProj->v[15] + ViewProj->v[13];

		// Calculate the top plane of frustum
		Frustum->Planes[FRUSTUM_TOP].a = ViewProj->v[3] - ViewProj->v[1];
		Frustum->Planes[FRUSTUM_TOP].b = ViewProj->v[7] - ViewProj->v[5];
		Frustum->Planes[FRUSTUM_TOP].c = ViewProj->v[11] - ViewProj->v[9];
		Frustum->Planes[FRUSTUM_TOP].d = ViewProj->v[15] - ViewProj->v[13];

		// Calculate the near plane of frustum
		Frustum->Planes[FRUSTUM_NEAR].a = ViewProj->v[3] + ViewProj->v[2];
		Frustum->Planes[FRUSTUM_NEAR].b = ViewProj->v[7] + ViewProj->v[6];
		Frustum->Planes[FRUSTUM_NEAR].c = ViewProj->v[11] + ViewProj->v[10];
		Frustum->Planes[FRUSTUM_NEAR].d = ViewProj->v[15] + ViewProj->v[14];

		// Calculate the far plane of frustum
		Frustum->Planes[FRUSTUM_FAR].a = ViewProj->v[3] - ViewProj->v[2];
		Frustum->Planes[FRUSTUM_FAR].b = ViewProj->v[7] - ViewProj->v[6];
		Frustum->Planes[FRUSTUM_FAR].c = ViewProj->v[11] - ViewProj->v[10];
		Frustum->Planes[FRUSTUM_FAR].d = ViewProj->v[15] - ViewProj->v[14];

		// Normalize planes
		FrustumUpdatePlanes(Frustum);
	}
}

////////////////////////////////////////////////////////////
/// Update frustum using a view projection matrix extended
////////////////////////////////////////////////////////////
void FrustumUpdateEx(struct FrustumType * Frustum, struct Matrix4f * View,
					 struct Matrix4f * Proj, struct Matrix4f * ViewProj, float Depth)
{
	if (Frustum && View && Proj && ViewProj)
	{
		float MinZ, Result;

		// Calculate the minimum z distance in the frustum
		MinZ = -Proj->v[14] / Proj->v[10];
		Result = Depth / (Depth - MinZ);
		Proj->v[10] = Result;
		Proj->v[14] = -Result * MinZ;

		// This function will only work if you do not rotate or translate your projection matrix
		ViewProj->v[0] = View->v[0] * Proj->v[0];
		ViewProj->v[1] = View->v[1] * Proj->v[5];
		ViewProj->v[2] = View->v[2] * Proj->v[10] + View->v[3] * Proj->v[14];
		ViewProj->v[3] = View->v[2] * Proj->v[11];

		ViewProj->v[4] = View->v[4] * Proj->v[0];
		ViewProj->v[5] = View->v[5] * Proj->v[5];
		ViewProj->v[6] = View->v[6] * Proj->v[10] + View->v[7] * Proj->v[14];
		ViewProj->v[7] = View->v[6] * Proj->v[11];

		ViewProj->v[8] = View->v[8] * Proj->v[0];
		ViewProj->v[9] = View->v[9] * Proj->v[5];
		ViewProj->v[10] = View->v[10] * Proj->v[10] + View->v[11] * Proj->v[14];
		ViewProj->v[11] = View->v[10] * Proj->v[11];

		ViewProj->v[12] = View->v[12] * Proj->v[0];
		ViewProj->v[13] = View->v[13] * Proj->v[5];
		ViewProj->v[14] = View->v[14] * Proj->v[10] + View->v[15] * Proj->v[14];
		ViewProj->v[15] = View->v[14] * Proj->v[11];

		// Calculate right plane of frustum
		Frustum->Planes[FRUSTUM_RIGHT].a = ViewProj->v[3] - ViewProj->v[0];
		Frustum->Planes[FRUSTUM_RIGHT].b = ViewProj->v[7] - ViewProj->v[4];
		Frustum->Planes[FRUSTUM_RIGHT].c = ViewProj->v[11] - ViewProj->v[8];
		Frustum->Planes[FRUSTUM_RIGHT].d = ViewProj->v[15] - ViewProj->v[12];

		// Calculate the left plane of frustum
		Frustum->Planes[FRUSTUM_LEFT].a = ViewProj->v[3] + ViewProj->v[0];
		Frustum->Planes[FRUSTUM_LEFT].b = ViewProj->v[7] + ViewProj->v[4];
		Frustum->Planes[FRUSTUM_LEFT].c = ViewProj->v[11] + ViewProj->v[8];
		Frustum->Planes[FRUSTUM_LEFT].d = ViewProj->v[15] + ViewProj->v[12];

		// Calculate the bottom plane of frustum
		Frustum->Planes[FRUSTUM_BOTTOM].a = ViewProj->v[3] + ViewProj->v[1];
		Frustum->Planes[FRUSTUM_BOTTOM].b = ViewProj->v[7] + ViewProj->v[5];
		Frustum->Planes[FRUSTUM_BOTTOM].c = ViewProj->v[11] + ViewProj->v[9];
		Frustum->Planes[FRUSTUM_BOTTOM].d = ViewProj->v[15] + ViewProj->v[13];

		// Calculate the top plane of frustum
		Frustum->Planes[FRUSTUM_TOP].a = ViewProj->v[3] - ViewProj->v[1];
		Frustum->Planes[FRUSTUM_TOP].b = ViewProj->v[7] - ViewProj->v[5];
		Frustum->Planes[FRUSTUM_TOP].c = ViewProj->v[11] - ViewProj->v[9];
		Frustum->Planes[FRUSTUM_TOP].d = ViewProj->v[15] - ViewProj->v[13];

		// Calculate the far plane of frustum
		Frustum->Planes[FRUSTUM_FAR].a = ViewProj->v[3] - ViewProj->v[2];
		Frustum->Planes[FRUSTUM_FAR].b = ViewProj->v[7] - ViewProj->v[6];
		Frustum->Planes[FRUSTUM_FAR].c = ViewProj->v[11] - ViewProj->v[10];
		Frustum->Planes[FRUSTUM_FAR].d = ViewProj->v[15] - ViewProj->v[14];

		// Calculate the near plane of frustum
		Frustum->Planes[FRUSTUM_NEAR].a = ViewProj->v[3] + ViewProj->v[2];
		Frustum->Planes[FRUSTUM_NEAR].b = ViewProj->v[7] + ViewProj->v[6];
		Frustum->Planes[FRUSTUM_NEAR].c = ViewProj->v[11] + ViewProj->v[10];
		Frustum->Planes[FRUSTUM_NEAR].d = ViewProj->v[15] + ViewProj->v[14];

		// Normalize planes
		FrustumUpdatePlanes(Frustum);
	}
}
