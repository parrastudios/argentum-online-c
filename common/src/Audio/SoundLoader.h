/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef AUDIO_SOUND_LOADER_H
#define AUDIO_SOUND_LOADER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <Audio/Sound.h>

////////////////////////////////////////////////////////////
/// Open a sound file for reading
///
/// \param Filename : Path of the sound file to load
///
/// \return SNDFILE pointer if the file was
///			successfully opened
///
////////////////////////////////////////////////////////////
bool SoundFileOpenReadFromFile(struct Sound * SoundData, char * Filename);

////////////////////////////////////////////////////////////
/// Open a sound file in memory for reading
///
/// \param Data        : Pointer to the file data in memory
/// \param SizeInBytes : Size of the data to load, in bytes
///
/// \return SNDFILE pointer if the file was
///			successfully opened
///
////////////////////////////////////////////////////////////
bool SoundFileOpenReadFromMemory(struct Sound * SoundData, void * Data, int32 SizeInBytes);

#endif // AUDIO_SOUND_LOADER_H
