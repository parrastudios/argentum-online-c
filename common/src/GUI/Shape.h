/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GUI_SHAPE_H
#define GUI_SHAPE_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define GUI_SHAPE_TYPE_CIRCUMFERENCE                0x00
#define GUI_SHAPE_TYPE_SQUARE                       0x01

#define GUI_SHAPE_DIRECTION_TOP_TO_BOTTOM           0x00
#define GUI_SHAPE_DIRECTION_BOTTOM_TO_TOP           0x01
#define GUI_SHAPE_DIRECTION_RIGTH_TO_LEFT           0x02
#define GUI_SHAPE_DIRECTION_LEFT_TO_RIGTH           0x03

#define GUI_SHAPE_NORMAL_COLOR			            0x5C63
#define GUI_SHAPE_NOT_TEXTURED			            -1

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct GuiShape
{
	uinteger	DisplacementDirection;	//< Displacement direction
	float   	Percentage;				//< Fill percentage
	integer	    TextureFill;            //< Filling texture
	uinteger    StyleType;              //< Type of shape
};

////////////////////////////////////////////////////////////
// Widget include
////////////////////////////////////////////////////////////
#include <GUI/Widget.h>

////////////////////////////////////////////////////////////
// Create a shape
////////////////////////////////////////////////////////////
struct GuiWidget * GuiShapeCreate(int32 X, int32 Y, uint32 Width, uint32 Height, uint32 FontSize);

////////////////////////////////////////////////////////////
// Create a shape by default
////////////////////////////////////////////////////////////
struct GuiWidget * GuiShapeCreateDefault();

////////////////////////////////////////////////////////////
// Set style of the shape
////////////////////////////////////////////////////////////
void GuiShapeSetStyleType(struct GuiShape * Shape, uinteger StyleType);

////////////////////////////////////////////////////////////
// Set displacement direction of the shape
////////////////////////////////////////////////////////////
void GuiShapeSetDisplacementDirection(struct GuiShape * Shape, uinteger Direction);

////////////////////////////////////////////////////////////
// Set filled percent of the shape
////////////////////////////////////////////////////////////
void GuiShapeSetPercent(struct GuiShape * Shape, float Percent);

////////////////////////////////////////////////////////////
// Append percent to the shape
////////////////////////////////////////////////////////////
void GuiShapeAppendPercent(struct GuiShape * Shape, float Percent);

////////////////////////////////////////////////////////////
// Set texture to be rendered on shape
////////////////////////////////////////////////////////////
void GuiShapeSetTexture(struct GuiShape * Shape, integer Tex);

////////////////////////////////////////////////////////////
// Render a shape
////////////////////////////////////////////////////////////
void GuiShapeRender(struct GuiWidget * Form, struct GuiWidget * Widget, struct Color4ub * BackgroundColor, struct Color4ub * FontColor);

#endif // GUI_SHAPE_H
