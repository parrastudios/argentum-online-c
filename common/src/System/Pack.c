/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/Pack.h>
#include <System/IOHelper.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
///	Package structure
///		* Header
/// 	* Files table
///		* Datas (the files)
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Get the file path depending on the file type with index
////////////////////////////////////////////////////////////
char * PackGetFilePath( char * FilesPath, int32 FileIndex )
{
	char * FilePath = (char*)(MemoryAllocate(sizeof(char) * 64));

	switch (FilesPath[5])
	{
		case 's' : // Sounds
			sprintf(FilePath, "%s%d.ogg", FilesPath, FileIndex);
			break;

		case 'g' : // Graphics
			sprintf(FilePath, "%s%d.png", FilesPath, FileIndex);
			break;

		case 'f' : // Fonts
			sprintf(FilePath, "%s%d.ttf", FilesPath, FileIndex);
			break;

		default : // Anyone
			return (char*)0;
	}

	return FilePath;
}

////////////////////////////////////////////////////////////
/// Create a new pack
////////////////////////////////////////////////////////////
bool PackSaveData( char * PackPath, char * FilesPath, int32 FilesSize )
{
	// Pack header and pack file entry
	int32  Index;
	int32  DataOffset = 0;
	struct PackHeader FileHeader;
	struct PackFile FileEntry;
	struct PackFile * FileEntries = (struct PackFile*)(MemoryAllocate(sizeof(struct PackFile) * FilesSize));

	// Streams to open the files
	FILE * FilePack = NULL;
	FILE * File = NULL;

	// Data buffer
	char Buffer[1];

	// Initialize the file header
    MemorySet(&FileHeader, 0, sizeof(struct PackHeader));	// Set memory to 0
    MemoryCopy(&FileHeader.UniqueID, "#0!CPE\0", 7);		// Set unique id
    MemoryCopy(&FileHeader.Version, "0.0.1\0", 6);			// Set the version
    FileHeader.FileCount = FilesSize;					// Set the number of files to include

	// File indexes
    for (Index = 0; Index < FilesSize; Index++)
    {
		fopen(&File, PackGetFilePath(FilesPath, Index), "rb");

		if ( File )
		{
			// Reset the file entry memory
            MemorySet(&FileEntry, 0, sizeof(struct PackFile));

            // Set the index of the file
            FileEntry.Index = Index;

			// Get the length of the file and the offset
			fseek(File, 0L, SEEK_END);
			FileEntry.Size = ftell(File);
            FileEntry.Offset = 0;

			// Close file
			fclose(File);

            // Add the file entry into vector
            MemoryCopy(&FileEntries[Index], &FileEntry, sizeof(struct PackFile));
		}
		else
		{
			// Remove data from memory
			MemoryDeallocate(FileEntries);
			MemoryDeallocate(FilePack);
			MemoryDeallocate(File);
			printf("Error when the program tried to open the file: %s.\n", PackGetFilePath(FilesPath, Index));
			return false;
		}

    }

    // Calculate the offset of the files
    DataOffset += sizeof(struct PackHeader);
    DataOffset += FileHeader.FileCount * sizeof(struct PackFile);

    for (Index = 0; Index < FilesSize; Index++)
    {
        FileEntries[Index].Offset = DataOffset;
        DataOffset += FileEntries[Index].Size;
    }

    // Write the pack file
	fopen(&FilePack, PackPath, "wb");

	// Check if the archive exist
	if ( FilePack )
	{
		// Write the header
		fwrite(&FileHeader, sizeof(struct PackHeader), 1, FilePack);

		// Write the file entries table
		for (Index = 0; Index < FilesSize; Index++)
		{
			fwrite(&FileEntries[Index], sizeof(struct PackFile), 1, FilePack);
		}

		// Write the files data
		for (Index = 0; Index < FilesSize; Index++)
		{
			fopen(&File, PackGetFilePath(FilesPath, FileEntries[Index].Index), "rb");

			if ( File )
			{
				// Set to initial position
				fseek(File, 0L, SEEK_SET);

				// Read and write
				while (fread(Buffer, sizeof(char), 1, File))
				{
					fwrite(Buffer, sizeof(char), 1, FilePack);
				}

				// Close the file
				fclose(File);
			}
		}

		// Close the file
		fclose(FilePack);
	}
	else
	{
		// Remove data from memory
		MemoryDeallocate(FileEntries);
		MemoryDeallocate(FilePack);
		MemoryDeallocate(File);
		printf("Error when the program tried to create the pack: %s.\n", PackPath);
		return false;
	}

	// Remove data from memory
	MemoryDeallocate(FileEntries);
	MemoryDeallocate(FilePack);
	MemoryDeallocate(File);

	return true;
}

////////////////////////////////////////////////////////////
/// Extract all data from pack
////////////////////////////////////////////////////////////
bool PackExtractData(char * PackPath, char * ExtractPath)
{
	// Stream to open file
	FILE * FilePack;
	uint32 Index;

	// Pack header and pack file entry
	struct PackHeader FileHeader;

	// Read the header
	fopen(&FilePack, PackPath, "rb");

	if ( FilePack )
	{
		// Ser memory
		MemorySet(&FileHeader, 0, sizeof(struct PackHeader));

		// Write the header
		fread(&FileHeader, sizeof(struct PackHeader), 1, FilePack);

		if (FileHeader.FileCount == 0)
		{
			fclose(FilePack);
			return false;
		}
	}
	else
	{
		return false;
	}

	// File header readed, so close it
	fclose(FilePack);

	for (Index = 0; Index < FileHeader.FileCount; Index++)
	{
		FILE * File;
		uint8 * Buffer = 0; int32 Size;

		if (!PackLoadData(PackPath, Index, &Buffer, &Size))
		{
			MemoryDeallocate(Buffer);
			return false;
		}

		fopen(&File, PackGetFilePath(ExtractPath, Index), "wb");

		if ( File )
		{
			// Put the buffer into the pack
			fwrite(Buffer, sizeof(uint8), Size, File);

			MemoryDeallocate(Buffer);

			// Close archive
			fclose(File);
		}
		else
		{
			MemoryDeallocate(Buffer);
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////
/// Load file data with index to array
////////////////////////////////////////////////////////////
bool PackLoadData( char * PackPath, uint32 FileIndex, uint8 ** Data, int32 * Size )
{
	// Stream to open file
	FILE * File;

	// Check if the archive exist
	fopen(&File, PackPath, "rb");

	if ( File )
	{
		// Pack header and pack file entry
		struct PackHeader FileHeader;
		struct PackFile FileEntry;

		// Ser memory
		MemorySet(&FileHeader, 0, sizeof(struct PackHeader));

		// Write the header
		fread(&FileHeader, sizeof(struct PackHeader), 1, File);

		// Check pack integrity
		if ( (strcmp(FileHeader.UniqueID, "#0!CPE\0") != 0) ||
			 (strcmp(FileHeader.Version, "0.0.1\0") != 0)	||
			 (FileHeader.FileCount < FileIndex) )
		{
			fclose(File); MemoryDeallocate(File);
			return false;
		}

		// Go to the file entry position
		fseek(File, (FileIndex * sizeof(struct PackFile) + sizeof(struct PackHeader)), SEEK_SET);

		// Read file entry data
		fread(&FileEntry, sizeof(struct PackFile), 1, File);

		if (FileEntry.Index != FileIndex)
		{
			fclose(File); MemoryDeallocate(File);
			return false;
		}

		// Go to file data
		fseek(File, FileEntry.Offset, SEEK_SET);

		// Set size
		*Size = FileEntry.Size;

		// MemoryAllocate memory
		*Data = (uint8*)(MemoryAllocate(*Size));

		// Read the data
		fread(*Data, sizeof(uint8), *Size, File);

		// Close archive
		fclose(File);

		// Destroy memory data
		MemoryDeallocate(File);

		return true;
	}

	// Destroy memory data
	MemoryDeallocate(File);

	return false;
}
