/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_MEMORY_ALLOCATOR_H
#define SYSTEM_MEMORY_ALLOCATOR_H

////////////////////////////////////////////////////////////
//	If N byte alignment is required then the minimum size to
//	be allocated is N bytes
//	____________________________________________________
//	|		 |		 |					|		|		|
//	| Header | Guard |		Size		| Guard	| Block	|
//	|		 | Bytes |	  Requested		| Bytes	| Start	|
//	|		 |		 |					|		|Address|
//	|________|_______|__________________|_______|_______|
//
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Types.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define	MEMORY_CHUNK_GUARD_BYTES		20
#define	MEMORY_CHUNK_ALIGNMENT_BYTES	4
#define	MEMORY_CHUNK_TYPE_FREE			0x00
#define	MEMORY_CHUNK_TYPE_ALLOCATED		0x01

////////////////////////////////////////////////////////////
// Data structure
////////////////////////////////////////////////////////////
struct MemoryChunk
{
	uint32 Size		 : 30;
	uint32 Type		 : 1;
	uint32 Available : 1;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

struct MemoryChunk * MemoryChunkCreate(void * Pointer, uint32 Size);

//uint32 MemoryChunk


//uint32	get_high_n_bytes_aligned_size(uint32 size);
//uint32	get_low_n_bytes_aligned_size(uint32 size);

uint32 MemoryChunkGuardBytes();
uint32 MemoryChunkOverheadSize();
bool MemoryChunkCheckSize(uint32 Size);
bool MemoryChunkSlipt(struct MemoryChunk * Chunk, uint32 Size, struct MemoryChunk **Left, struct MemoryChunk **Right);
void MemoryChunkFooterInsert(struct MemoryChunk * Chunk);
uint32 * MemoryChunkFooterRead(struct MemoryChunk * Chunk);
void * MemoryChunkGetData(struct MemoryChunk * Chunk);

#endif // SYSTEM_MEMORY_ALLOCATOR_H