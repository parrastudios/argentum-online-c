/*
***********************************************************************************
 IqeB_Tools_Voxel.cpp

 Tools for working with voxels.

13.12.2014 RR: * First editon of this file.

*/

#include "IqeBrowser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>

/************************************************************************************

  defines
*/

/************************************************************************************

  pVoxelCoordinate

  Calculat pointer to voxel

*/
static VOX_CEL_TYPE *pVoxelCoordinate( T_VoxelModelData *pVoxelData,   // IN: Voxel mesh
                        int ix, int iy, int iz)
{
  VOX_CEL_TYPE *p;

  // Clipping

  if( ix < 0) {
    ix = 0;
  }
  if( ix >= pVoxelData->Size[ 0]) {
    ix = pVoxelData->Size[ 0] - 1;
  }
  if( iy < 0) {
    iy = 0;
  }
  if( iy >= pVoxelData->Size[ 1]) {
    iy = pVoxelData->Size[ 1] - 1;
  }
  if( iz < 0) {
    iz = 0;
  }
  if( iz >= pVoxelData->Size[ 2]) {
    iz = pVoxelData->Size[ 2] - 1;
  }

  p = pVoxelData->pVoxels;
  p += ix * pVoxelData->Size[ 1] * pVoxelData->Size[ 2];
  p += iy * pVoxelData->Size[ 2];
  p += iz;

  return( p);
}

/************************************************************************************

  IqeModelVoxelTriSet

  Write triangle to voxel.
  Write the corner pixels of the triangle to the voxel data
  than divide the longest side and recursive call itselft.
  NOTE: * Coordinates give are in space of voxel volume.
        * Size of a voxel is 1 unit each side.
*/

static void IqeModelVoxelTriSet( T_VoxelModelData *pVoxelData,  // IN: Voxel mesh
                                 vec3 p1, vec3 p2, vec3 p3,     // IN: Points of triangle
                                 int PointFillMask,
                                 int FillValue)
{
  float d1, d2, d3;
  vec3 m;
  VOX_CEL_TYPE *p;

  if( PointFillMask & 0x01) {  // Fill for first point

    p = pVoxelCoordinate( pVoxelData, (int)(p1[ 0] + 0.5), (int)(p1[ 1] + 0.5), (int)(p1[ 2] + 0.5));
    *p = FillValue;
  }

  if( PointFillMask & 0x02) {  // Fill for second point

    p = pVoxelCoordinate( pVoxelData, (int)(p2[ 0] + 0.5), (int)(p2[ 1] + 0.5), (int)(p2[ 2] + 0.5));
    *p = FillValue;
  }

  if( PointFillMask & 0x04) {  // Fill for third point

    p = pVoxelCoordinate( pVoxelData, (int)(p3[ 0] + 0.5), (int)(p3[ 1] + 0.5), (int)(p3[ 2] + 0.5));
    *p = FillValue;
  }

  // get longest triangle side

  d1 = vec_dist2( p1, p2);           // squared distances
  d2 = vec_dist2( p2, p3);
  d3 = vec_dist2( p3, p1);

  if( d1 >= d2 && d1 >= d3) {              // test first side is longest side

    if( d1 < VOX_CUPE_SET_LEN_THRESHOLD) { // below length threshold
      return;
    }

    // split longest side

    vec_add( m, p1, p2);
    vec_scale_to( m, 0.5);

    // recursive fill

    IqeModelVoxelTriSet( pVoxelData, m, p3, p1, 0x01, FillValue);
    IqeModelVoxelTriSet( pVoxelData, m, p2, p3, 0x00, FillValue);

  } else if( d2 >= d1 && d2 >= d3) { // test second side is longest side

    if( d2 < VOX_CUPE_SET_LEN_THRESHOLD) { // below length threshold
      return;
    }

    // split longest side

    vec_add( m, p2, p3);
    vec_scale_to( m, 0.5);

    // recursive fill

    IqeModelVoxelTriSet( pVoxelData, m, p1, p2, 0x01, FillValue);
    IqeModelVoxelTriSet( pVoxelData, m, p3, p1, 0x00, FillValue);

  } else if( d3 >= d1 && d3 >= d2) { // test third side is longest side

    if( d3 < VOX_CUPE_SET_LEN_THRESHOLD) { // below length threshold
      return;
    }

    // split longest side

    vec_add( m, p3, p1);
    vec_scale_to( m, 0.5);

    // recursive fill

    IqeModelVoxelTriSet( pVoxelData, m, p2, p3, 0x01, FillValue);
    IqeModelVoxelTriSet( pVoxelData, m, p1, p2, 0x00, FillValue);

  } else {

     // this may never happen.
     // Add some code to be able to set a breakpoint for the debugger.

     p = NULL;
  }
}

/************************************************************************************

  Axis perpenticular to index axis.
*/

static int OtherAxis1[ 3] = { 1, 2, 0};
static int OtherAxis2[ 3] = { 2, 0, 1};

/************************************************************************************

  IqeB_VoxelFromIqeModel

  Convert IQE Model to voxel model

  Return:    0 OK
          != 0 error
*/

int IqeB_VoxelFromIqeModel( IQE_model *pModel,             // IN: IQE model
                            T_VoxelModelData *pVoxelData,  // OUT: Voxel mesh
                            int VoxelNormSize,             // IN: Normalize voxel data
                            char *pErrorString)
{
  int ReturnValue;
  int i, j, VoxelAmount;
	vec3 ModelSize;
	float ModelSizeSum;
  int TempCoord[ 3];
  VOX_CEL_TYPE *pVox, *pVoxBase, *pVoxNext;
  int i0, i1, i2, iAxis, iAxis1, iAxis2;
  int VoxStep;

  // Preparations

  ReturnValue = 0;                            // preset OK

  memset( pVoxelData,  0, sizeof( T_VoxelModelData)); // flag no data allocated

  // security test of argument

  if( VoxelNormSize < 16) {          // Below minimum value

    if( pErrorString != NULL && pErrorString[ 0] == '\0') {  // No error until now

      strcpy( pErrorString, "IqeModelToVoxels: VoxelNormSize must be >= 16");
    }

    ReturnValue = -1;              // set error
    goto ErrorExit;                // return error
  }

  if( VoxelNormSize > VOX_MAX_NORM_SIZE) {   // higher than our maximum

    VoxelNormSize = VOX_MAX_NORM_SIZE;       // clip value
  }

  // security test of data

  if( pModel->mesh->vertex_count < 3) {  // Minimum 3 vertexes

    if( pErrorString != NULL && pErrorString[ 0] == '\0') {  // No error until now

      strcpy( pErrorString, "IqeModelToVoxels: No points in mesh data");
    }

    ReturnValue = -2;              // set error
    goto ErrorExit;                // return error
  }

  // get mesh boundary

	for (i = 0; i < pModel->mesh->vertex_count; i++) {

    if( i == 0) {

  		vec_copy( pVoxelData->minbox, pModel->mesh->position + i * 3);
  		vec_copy( pVoxelData->maxbox, pModel->mesh->position + i * 3);
    } else {

      vec_minmax( pVoxelData->minbox, pVoxelData->maxbox, pModel->mesh->position + i * 3);
    }
  }

  // process axis related things

  ModelSizeSum = 0.0;

  for( i = 0; i < 3; i++) {

    ModelSize[ i] = pVoxelData->maxbox[ i] - pVoxelData->minbox[ i];         // size of model per axis

    // sum up model sizes
    ModelSizeSum += ModelSize[ i];
  }

  // security test max size

  if( ModelSizeSum < 0.01) {        // Minimum size

    if( pErrorString != NULL && pErrorString[ 0] == '\0') {  // No error until now

      strcpy( pErrorString, "IqeModelToVoxels: Mesh to small");
    }

    ReturnValue = -3;              // set error
    goto ErrorExit;                // return error
  }

  // Get voxel axis sizes, max size of axis is given argument 'VoxelNormSize'.

#ifdef use_again
  pVoxelData->ScaleMeshToVoxel = VoxelNormSize / ModelSizeMax;
#else
  pVoxelData->ScaleMeshToVoxel = (VoxelNormSize + VoxelNormSize + VoxelNormSize) / ModelSizeSum;
#endif
  pVoxelData->ScaleVoxelToMesh = 1.0 / pVoxelData->ScaleMeshToVoxel;

  for( i = 0; i < 3; i++) {

    pVoxelData->Size[ i] = (int)( ceil( ModelSize[ i] * pVoxelData->ScaleMeshToVoxel));

    if( pVoxelData->Size[ i] < 1) {  // Clip to inimum size
      pVoxelData->Size[ i] = 1;
    }
  }

  // allocate voxel data

  VoxelAmount = pVoxelData->Size[ 0] * pVoxelData->Size[ 1] * pVoxelData->Size[ 2];  // multiply it

  pVoxelData->pVoxels = (VOX_CEL_TYPE *)malloc( VoxelAmount);

  // security test allocated data

  if( pVoxelData->pVoxels == NULL) {  // allocation failed

    if( pErrorString != NULL && pErrorString[ 0] == '\0') {  // No error until now

      strcpy( pErrorString, "IqeModelToVoxels: Memory allocation failed");
    }

    ReturnValue = -4;              // set error
    goto ErrorExit;                // return error
  }

  // set all voxeldata to empty

  memset( pVoxelData->pVoxels, 2, VoxelAmount);

  // Reender the triangles to the mesh.
  // This gives the skin of the mesh in voxels.

	for (i = 0; i < pModel->mesh->part_count; i++) {

    for( j = 0; j < pModel->mesh->part[i].count; j += 3) {

      int i1, i2, i3;
      vec3 p1, p2, p3;

      // indices of triangles

      i1 = pModel->mesh->element[ pModel->mesh->part[ i].first + j + 0];
      i2 = pModel->mesh->element[ pModel->mesh->part[ i].first + j + 1];
      i3 = pModel->mesh->element[ pModel->mesh->part[ i].first + j + 2];

      // get point from triangles and convert to voxel coordinates

      vec_copy( p1, pModel->mesh->position + i1 * 3);  // get point
      vec_sub_from( p1, pVoxelData->minbox);            // relative to minmax box
      vec_scale_to( p1, pVoxelData->ScaleMeshToVoxel);

      vec_copy( p2, pModel->mesh->position + i2 * 3);  // get point
      vec_sub_from( p2, pVoxelData->minbox);            // relative to minmax box
      vec_scale_to( p2, pVoxelData->ScaleMeshToVoxel);

      vec_copy( p3, pModel->mesh->position + i3 * 3);  // get point
      vec_sub_from( p3, pVoxelData->minbox);            // relative to minmax box
      vec_scale_to( p3, pVoxelData->ScaleMeshToVoxel);

      IqeModelVoxelTriSet( pVoxelData, p1, p2, p3, 0x07, 1);
    }
	}

	// Fill the outside of the voxelvolume  with 0.
	// We starting from the voxel side to the inside until
	// we reach the skin (mesh triangles written above)

	int iIteration, AnyDone, LastVal;

	for( iIteration = 0; iIteration < 16; iIteration++) {   // Do it max 16 times

    AnyDone = false;

    for( iAxis = 0; iAxis < 3; iAxis++) {

      // prepare other axis

      iAxis1 = OtherAxis1[ iAxis];
      iAxis2 = OtherAxis2[ iAxis];

      // get step along this axis through the voxel data

      pVoxBase = pVoxelCoordinate( pVoxelData, 0, 0, 0);

      pVoxNext = pVoxelCoordinate( pVoxelData, 0 == iAxis, 1 == iAxis, 2 == iAxis);
      VoxStep  = pVoxNext - pVoxBase;

      for( i1 = 0; i1 < pVoxelData->Size[ iAxis1]; i1++) {

        // 1. side

        for( i2 = 0; i2 < pVoxelData->Size[ iAxis2]; i2++) {

          // start point of voxels
          TempCoord[ iAxis]  = 0;
          TempCoord[ iAxis1] = i1;
          TempCoord[ iAxis2] = i2;

          pVox = pVoxelCoordinate( pVoxelData, TempCoord[ 0], TempCoord[ 1], TempCoord[ 2]);

          LastVal = 0;

          for( i0 = 0; i0 < pVoxelData->Size[ iAxis]; i0++, pVox += VoxStep) {

            if( LastVal == 0 && *pVox == 2) {  // is already set outside and unclearead volume

              AnyDone = true;
              *pVox = 0;          // mark outside
            }

            LastVal = *pVox;
          }
        }

        // 2. side

        for( i2 = 0; i2 < pVoxelData->Size[ iAxis2]; i2++) {

          // start point of voxels
          TempCoord[ iAxis]  = pVoxelData->Size[ iAxis] - 1;
          TempCoord[ iAxis1] = i1;
          TempCoord[ iAxis2] = i2;

          pVox = pVoxelCoordinate( pVoxelData, TempCoord[ 0], TempCoord[ 1], TempCoord[ 2]);

          LastVal = 0;

          for( i0 = 0; i0 < pVoxelData->Size[ iAxis]; i0++, pVox -= VoxStep) {

            if( LastVal == 0 && *pVox == 2) {  // is already set outside and unclearead volume

              AnyDone = true;
              *pVox = 0;          // mark outside
            }

            LastVal = *pVox;
          }
        }
      }
    }

    if( ! AnyDone) {   // Not any done in last round

      break;           // can break iteration loop
    }
	}

	// Count the number of inside voxels and calculate a fill radios

  pVox = pVoxelData->pVoxels;  // point to voxel data

  j = 0;                       // use this as sum of the inside voxels

  for( i = 0; i < VoxelAmount; i++, pVox++) {

    if( *pVox != 0) {          // Inside model

      j += 1;                  // One more
    }
  }

  if( VoxelAmount > 0) {

    pVoxelData->FillFactor = (float)j / (float)VoxelAmount;
  } else {

    pVoxelData->FillFactor = 0.0;
  }

	// ...

  ReturnValue = 0;                // set OK

ErrorExit:

  // Free allocated data in case of an error

  if( ReturnValue != 0) {         // have any error

    if( pVoxelData->pVoxels != NULL) free( pVoxelData->pVoxels);
  }

  return( ReturnValue);           // set error
}

/************************************************************************************

  VoxelsToIqeModelVertex

  Store integer coordinates packed in the float vertex data array.
  Do a lookup of already used coorndiates

  Return: Index of vertex to
*/

static int VoxelsToIqeModelVertex( int p[ 3], float *pVertex, int *nPoints)
{

  // Clipping

  int i, MergedCoord;

  // Merge coordinates to one integer.
  // The three coordinates are always >= 0 and < 1024.

#ifdef _DEBUG
  if( p[ 0] < 0 || p[ 1] < 0 || p[ 2] < 0) {  // dont't like negative values

    i = 0;   // used as breakpoint
  }
#endif

  MergedCoord = ((p[ 0] & 0x3ff) << 0) | ((p[ 1] & 0x3ff) << 10) | ((p[ 2] & 0x3ff) << 20);

  // test for already have this point

  for( i = 0; i < *nPoints; i++, pVertex += 3) {

    if( ((int *)pVertex)[ 0] == MergedCoord) { // got it

      return( i);
    }
  }

  // add new point

  ((int *)pVertex)[ 0] = MergedCoord; // store merged coordinate
  ((int *)pVertex)[ 1] =    *nPoints; // store index of this element

  *nPoints += 1;     // have one more

  return( i);
}

/************************************************************************************

  IqeB_VoxelToIqeMarchingCube

  Convert voxel model to IQE Model
  Use marching cube algorithm to extract the vertiecs.

  Return: NULL   Error
          else   Pointer to IQE model ready to display
*/

#ifndef ABS
#define ABS(x) (x < 0 ? -(x) : (x))
#endif

typedef struct {
   int x,y,z;
} XYZ;

typedef struct {
   XYZ p[3];
} TRIANGLE;

typedef struct {
   XYZ p[8];
   VOX_CEL_TYPE val[8];
} GRIDCELL;

/*-------------------------------------------------------------------------
   Return the point between two points in the same ratio as
   isolevel is between valp1 and valp2
*/
static XYZ VertexInterp( VOX_CEL_TYPE isolevel,
                         XYZ p1,
                         XYZ p2,
                         VOX_CEL_TYPE valp1,
                         VOX_CEL_TYPE valp2)
{
#ifdef VOX_CEL_TYPE_IS_INTEGER   // voxel cel type is an integer type

   // VOX_CEL_TYPE must be byte or short type

   XYZ p;

#ifdef use_again
   int mu;

   if ( isolevel == valp1)
      return(p1);
   if ( isolevel == valp2)
      return(p2);
   if ( valp1 == valp2)
      return(p1);

   mu = ((int)isolevel - (int)valp1) / ((int)valp2 - (int)valp1);
   p.x = p1.x + mu * (p2.x - p1.x);
   p.y = p1.y + mu * (p2.y - p1.y);
   p.z = p1.z + mu * (p2.z - p1.z);

#else

   p.x = (p2.x + p1.x + 1) / 2;
   p.y = (p2.y + p1.y + 1) / 2;
   p.z = (p2.z + p1.z + 1) / 2;

#endif

   return(p);

#else

   // VOX_CEL_TYPE must be float or double type

   VOX_CEL_TYPE mu;
   XYZ p;

   if (ABS(isolevel-valp1) < 0.00001)
      return(p1);
   if (ABS(isolevel-valp2) < 0.00001)
      return(p2);
   if (ABS(valp1-valp2) < 0.00001)
      return(p1);
   mu = (isolevel - valp1) / (valp2 - valp1);
   p.x = p1.x + mu * (p2.x - p1.x);
   p.y = p1.y + mu * (p2.y - p1.y);
   p.z = p1.z + mu * (p2.z - p1.z);

   return(p);
#endif
}

/*-------------------------------------------------------------------------
   Given a grid cell and an isolevel, calculate the triangular
   facets required to represent the isosurface through the cell.
   Return the number of triangular facets, the array "triangles"
   will be loaded up with the vertices at most 5 triangular facets.
	 0 will be returned if the grid cell is either totally above
   of totally below the isolevel.

*/
static int PolygoniseCube(GRIDCELL grid,VOX_CEL_TYPE isolevel,TRIANGLE *triangles)
{
   int i,ntriang;
   int cubeindex;
   XYZ vertlist[12];

/*
   int edgeTable[256].  It corresponds to the 2^8 possible combinations of
   of the eight (n) vertices either existing inside or outside (2^n) of the
   surface.  A vertex is inside of a surface if the value at that vertex is
   less than that of the surface you are scanning for.  The table index is
   constructed bitwise with bit 0 corresponding to vertex 0, bit 1 to vert
   1.. bit 7 to vert 7.  The value in the table tells you which edges of
   the table are intersected by the surface.  Once again bit 0 corresponds
   to edge 0 and so on, up to edge 12.
   Constructing the table simply consisted of having a program run thru
   the 256 cases and setting the edge bit if the vertices at either end of
   the edge had different values (one is inside while the other is out).
   The purpose of the table is to speed up the scanning process.  Only the
   edges whose bit's are set contain vertices of the surface.
   Vertex 0 is on the bottom face, back edge, left side.
   The progression of vertices is clockwise around the bottom face
   and then clockwise around the top face of the cube.  Edge 0 goes from
   vertex 0 to vertex 1, Edge 1 is from 2->3 and so on around clockwise to
   vertex 0 again. Then Edge 4 to 7 make up the top face, 4->5, 5->6, 6->7
   and 7->4.  Edge 8 thru 11 are the vertical edges from vert 0->4, 1->5,
   2->6, and 3->7.
       4--------5     *---4----*
      /|       /|    /|       /|
     / |      / |   7 |      5 |
    /  |     /  |  /  8     /  9
   7--------6   | *----6---*   |
   |   |    |   | |   |    |   |
   |   0----|---1 |   *---0|---*
   |  /     |  /  11 /     10 /
   | /      | /   | 3      | 1
   |/       |/    |/       |/
   3--------2     *---2----*
*/

int edgeTable[256]={
0x0  , 0x109, 0x203, 0x30a, 0x406, 0x50f, 0x605, 0x70c,
0x80c, 0x905, 0xa0f, 0xb06, 0xc0a, 0xd03, 0xe09, 0xf00,
0x190, 0x99 , 0x393, 0x29a, 0x596, 0x49f, 0x795, 0x69c,
0x99c, 0x895, 0xb9f, 0xa96, 0xd9a, 0xc93, 0xf99, 0xe90,
0x230, 0x339, 0x33 , 0x13a, 0x636, 0x73f, 0x435, 0x53c,
0xa3c, 0xb35, 0x83f, 0x936, 0xe3a, 0xf33, 0xc39, 0xd30,
0x3a0, 0x2a9, 0x1a3, 0xaa , 0x7a6, 0x6af, 0x5a5, 0x4ac,
0xbac, 0xaa5, 0x9af, 0x8a6, 0xfaa, 0xea3, 0xda9, 0xca0,
0x460, 0x569, 0x663, 0x76a, 0x66 , 0x16f, 0x265, 0x36c,
0xc6c, 0xd65, 0xe6f, 0xf66, 0x86a, 0x963, 0xa69, 0xb60,
0x5f0, 0x4f9, 0x7f3, 0x6fa, 0x1f6, 0xff , 0x3f5, 0x2fc,
0xdfc, 0xcf5, 0xfff, 0xef6, 0x9fa, 0x8f3, 0xbf9, 0xaf0,
0x650, 0x759, 0x453, 0x55a, 0x256, 0x35f, 0x55 , 0x15c,
0xe5c, 0xf55, 0xc5f, 0xd56, 0xa5a, 0xb53, 0x859, 0x950,
0x7c0, 0x6c9, 0x5c3, 0x4ca, 0x3c6, 0x2cf, 0x1c5, 0xcc ,
0xfcc, 0xec5, 0xdcf, 0xcc6, 0xbca, 0xac3, 0x9c9, 0x8c0,
0x8c0, 0x9c9, 0xac3, 0xbca, 0xcc6, 0xdcf, 0xec5, 0xfcc,
0xcc , 0x1c5, 0x2cf, 0x3c6, 0x4ca, 0x5c3, 0x6c9, 0x7c0,
0x950, 0x859, 0xb53, 0xa5a, 0xd56, 0xc5f, 0xf55, 0xe5c,
0x15c, 0x55 , 0x35f, 0x256, 0x55a, 0x453, 0x759, 0x650,
0xaf0, 0xbf9, 0x8f3, 0x9fa, 0xef6, 0xfff, 0xcf5, 0xdfc,
0x2fc, 0x3f5, 0xff , 0x1f6, 0x6fa, 0x7f3, 0x4f9, 0x5f0,
0xb60, 0xa69, 0x963, 0x86a, 0xf66, 0xe6f, 0xd65, 0xc6c,
0x36c, 0x265, 0x16f, 0x66 , 0x76a, 0x663, 0x569, 0x460,
0xca0, 0xda9, 0xea3, 0xfaa, 0x8a6, 0x9af, 0xaa5, 0xbac,
0x4ac, 0x5a5, 0x6af, 0x7a6, 0xaa , 0x1a3, 0x2a9, 0x3a0,
0xd30, 0xc39, 0xf33, 0xe3a, 0x936, 0x83f, 0xb35, 0xa3c,
0x53c, 0x435, 0x73f, 0x636, 0x13a, 0x33 , 0x339, 0x230,
0xe90, 0xf99, 0xc93, 0xd9a, 0xa96, 0xb9f, 0x895, 0x99c,
0x69c, 0x795, 0x49f, 0x596, 0x29a, 0x393, 0x99 , 0x190,
0xf00, 0xe09, 0xd03, 0xc0a, 0xb06, 0xa0f, 0x905, 0x80c,
0x70c, 0x605, 0x50f, 0x406, 0x30a, 0x203, 0x109, 0x0   };

/*
   int triTable[256][16] also corresponds to the 256 possible combinations
   of vertices.
   The [16] dimension of the table is again the list of edges of the cube
   which are intersected by the surface.  This time however, the edges are
   enumerated in the order of the vertices making up the triangle mesh of
   the surface.  Each edge contains one vertex that is on the surface.
   Each triple of edges listed in the table contains the vertices of one
   triangle on the mesh.  The are 16 entries because it has been shown that
   there are at most 5 triangles in a cube and each "edge triple" list is
   terminated with the value -1.
   For example triTable[3] contains
   {1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}
   This corresponds to the case of a cube whose vertex 0 and 1 are inside
   of the surface and the rest of the verts are outside (00000001 bitwise
   OR'ed with 00000010 makes 00000011 == 3).  Therefore, this cube is
   intersected by the surface roughly in the form of a plane which cuts
   edges 8,9,1 and 3.  This quadrilateral can be constructed from two
   triangles: one which is made of the intersection vertices found on edges
   1,8, and 3; the other is formed from the vertices on edges 9,8, and 1.
   Remember, each intersected edge contains only one surface vertex.  The
   vertex triples are listed in counter clockwise order for proper facing.
*/
int triTable[256][16] =
{{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1},
{3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1, -1},
{3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1, -1},
{3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1, -1},
{9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1, -1},
{1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1},
{9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
{2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1, -1},
{8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1, -1},
{9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
{4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1, -1},
{3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1, -1},
{1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1, -1},
{4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1, -1},
{4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1, -1},
{9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1, -1},
{1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
{5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1, -1},
{2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1, -1},
{9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
{0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
{2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1, -1},
{10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1},
{4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1, -1},
{5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1, -1},
{5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1},
{9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1, -1},
{0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1, -1},
{1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1, -1},
{10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1, -1},
{8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1, -1},
{2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1},
{7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1},
{9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1, -1},
{2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1, -1},
{11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1, -1},
{9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1, -1},
{5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0, -1},
{11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0, -1},
{11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
{1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1, -1},
{9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1, -1},
{5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1, -1},
{2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
{0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
{5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1, -1},
{6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1, -1},
{3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1, -1},
{6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1, -1},
{5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1, -1},
{1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
{10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1, -1},
{6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1},
{1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1, -1},
{8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1, -1},
{7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9, -1},
{3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
{5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1, -1},
{0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1},
{9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6, -1},
{8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1, -1},
{5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11, -1},
{0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7, -1},
{6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1, -1},
{10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1},
{10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1, -1},
{8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1, -1},
{1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1, -1},
{3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1, -1},
{0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1},
{10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1, -1},
{3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1, -1},
{6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1, -1},
{9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1, -1},
{8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1, -1},
{3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1, -1},
{6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1, -1},
{0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1, -1},
{10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1, -1},
{10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1, -1},
{1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1, -1},
{2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9, -1},
{7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1, -1},
{7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1, -1},
{2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7, -1},
{1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11, -1},
{11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1, -1},
{8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6, -1},
{0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1, -1},
{7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
{10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
{2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
{6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1, -1},
{7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1, -1},
{2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1, -1},
{1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1, -1},
{10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1, -1},
{10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1, -1},
{0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1, -1},
{7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1, -1},
{6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1, -1},
{8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1, -1},
{9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1, -1},
{6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1, -1},
{1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1, -1},
{4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1, -1},
{10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3, -1},
{8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1},
{0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1, -1},
{1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1, -1},
{8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1, -1},
{10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1, -1},
{4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3, -1},
{10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
{5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
{11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1, -1},
{9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
{6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1, -1},
{7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1, -1},
{3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6, -1},
{7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1, -1},
{9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1, -1},
{3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1, -1},
{6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8, -1},
{9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1, -1},
{1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4, -1},
{4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10, -1},
{7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1, -1},
{6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1, -1},
{3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1, -1},
{0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1, -1},
{6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1},
{1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1, -1},
{0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10, -1},
{11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5, -1},
{6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1, -1},
{5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1, -1},
{9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1, -1},
{1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8, -1},
{1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6, -1},
{10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1, -1},
{0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1, -1},
{5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1, -1},
{10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1, -1},
{11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1, -1},
{9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1, -1},
{7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2, -1},
{2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1},
{8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1, -1},
{9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1, -1},
{9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2, -1},
{1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1, -1},
{9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1, -1},
{9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1},
{5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1, -1},
{0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1, -1},
{10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4, -1},
{2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1, -1},
{0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11, -1},
{0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5, -1},
{9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1, -1},
{5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1, -1},
{3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9, -1},
{5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1, -1},
{8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1, -1},
{0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1, -1},
{9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1, -1},
{1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1, -1},
{3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4, -1},
{4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1, -1},
{9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3, -1},
{11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1, -1},
{11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1, -1},
{2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1, -1},
{9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7, -1},
{3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10, -1},
{1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1, -1},
{4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1, -1},
{4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1, -1},
{0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1, -1},
{3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1, -1},
{3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1, -1},
{0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1, -1},
{9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1, -1},
{1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}};

   /*
      Determine the index into the edge table which
      tells us which vertices are inside of the surface
   */
   cubeindex = 0;
   if (grid.val[0] != 0) cubeindex |= 1;
   if (grid.val[1] != 0) cubeindex |= 2;
   if (grid.val[2] != 0) cubeindex |= 4;
   if (grid.val[3] != 0) cubeindex |= 8;
   if (grid.val[4] != 0) cubeindex |= 16;
   if (grid.val[5] != 0) cubeindex |= 32;
   if (grid.val[6] != 0) cubeindex |= 64;
   if (grid.val[7] != 0) cubeindex |= 128;

   /* Cube is entirely in/out of the surface */
   if (edgeTable[cubeindex] == 0)
      return(0);

#ifdef _DEBUG

  memset( vertlist, -1, sizeof( vertlist));
#endif

   /* Find the vertices where the surface intersects the cube */
   if (edgeTable[cubeindex] & 1)
      vertlist[0] =
         VertexInterp(isolevel,grid.p[0],grid.p[1],grid.val[0],grid.val[1]);
   if (edgeTable[cubeindex] & 2)
      vertlist[1] =
         VertexInterp(isolevel,grid.p[1],grid.p[2],grid.val[1],grid.val[2]);
   if (edgeTable[cubeindex] & 4)
      vertlist[2] =
         VertexInterp(isolevel,grid.p[2],grid.p[3],grid.val[2],grid.val[3]);
   if (edgeTable[cubeindex] & 8)
      vertlist[3] =
         VertexInterp(isolevel,grid.p[3],grid.p[0],grid.val[3],grid.val[0]);
   if (edgeTable[cubeindex] & 16)
      vertlist[4] =
         VertexInterp(isolevel,grid.p[4],grid.p[5],grid.val[4],grid.val[5]);
   if (edgeTable[cubeindex] & 32)
      vertlist[5] =
         VertexInterp(isolevel,grid.p[5],grid.p[6],grid.val[5],grid.val[6]);
   if (edgeTable[cubeindex] & 64)
      vertlist[6] =
         VertexInterp(isolevel,grid.p[6],grid.p[7],grid.val[6],grid.val[7]);
   if (edgeTable[cubeindex] & 128)
      vertlist[7] =
         VertexInterp(isolevel,grid.p[7],grid.p[4],grid.val[7],grid.val[4]);
   if (edgeTable[cubeindex] & 256)
      vertlist[8] =
         VertexInterp(isolevel,grid.p[0],grid.p[4],grid.val[0],grid.val[4]);
   if (edgeTable[cubeindex] & 512)
      vertlist[9] =
         VertexInterp(isolevel,grid.p[1],grid.p[5],grid.val[1],grid.val[5]);
   if (edgeTable[cubeindex] & 1024)
      vertlist[10] =
         VertexInterp(isolevel,grid.p[2],grid.p[6],grid.val[2],grid.val[6]);
   if (edgeTable[cubeindex] & 2048)
      vertlist[11] =
         VertexInterp(isolevel,grid.p[3],grid.p[7],grid.val[3],grid.val[7]);

   /* Create the triangle */
   ntriang = 0;
   for (i=0;triTable[cubeindex][i]!=-1;i+=3) {
      triangles[ntriang].p[0] = vertlist[triTable[cubeindex][i  ]];
      triangles[ntriang].p[1] = vertlist[triTable[cubeindex][i+1]];
      triangles[ntriang].p[2] = vertlist[triTable[cubeindex][i+2]];
      ntriang++;
   }

   return(ntriang);
}

IQE_model *IqeB_VoxelToIqeMarchingCube( T_VoxelModelData *pVoxelData,   // IN: Voxel mesh
                                        char *pErrorString)
{
  int i0, i1, i2, n, l;
  int Pass, nPoints, nTriangles;
  float *pVertex;

	// allocated an empty model

	struct IQE_model *model = IqeB_ModelCreateEmpty();    // get empty model
	if( model == NULL) {                                   // memory allocation failed

    return( NULL);
	}

  // for less writing, some short cuts

	struct skel *skel = model->skel;
	struct mesh *mesh = model->mesh;

  // ...

	skel->joint_count = 0;

  // march the voxels from all sides

  for( Pass = 0; Pass < 2; Pass++) {

	  VOX_CEL_TYPE isolevel = 1;
	  GRIDCELL grid;
	  TRIANGLE triangles[10];
	  int p1[ 3], p2[ 3], p3[ 3];
	  static XYZ GridOffset[ 8] = {
	    { 0, 0, 0 },
	    { 1, 0, 0 },
	    { 1, 1, 0 },
	    { 0, 1, 0 },
	    { 0, 0, 1 },
	    { 1, 0, 1 },
	    { 1, 1, 1 },
	    { 0, 1, 1 }
	  };

    nTriangles = 0;
    nPoints = 0;

    // Polygonise the grid

    for( i0 = -1; i0 < pVoxelData->Size[ 0]; i0++) {

      for( i1 = -1; i1 < pVoxelData->Size[ 1]; i1++) {

        for( i2 = -1; i2 < pVoxelData->Size[ 2]; i2++) {


				  // multiplay coordinates by two
				  for( n = 0; n < 8; n++) {

            // get coordinate for this cell
            grid.p[ n].x = i0 + GridOffset[ n].x;
            grid.p[ n].y = i1 + GridOffset[ n].y;
            grid.p[ n].z = i2 + GridOffset[ n].z;

            // are we inside the voxel volume
            if( grid.p[ n].x >= 0 && grid.p[ n].x < pVoxelData->Size[ 0] &&
                grid.p[ n].y >= 0 && grid.p[ n].y < pVoxelData->Size[ 1] &&
                grid.p[ n].z >= 0 && grid.p[ n].z < pVoxelData->Size[ 2]) {

              // get voxel volume
			  	    grid.val[ n] = *pVoxelCoordinate( pVoxelData, grid.p[ n].x , grid.p[ n].y, grid.p[ n].z) != 0;

            } else {

              // we are outside the voxel volume
			  	    grid.val[ n] = 0;    // set not solid
            }

            // keep addresses positiv and multply with two
            grid.p[ n].x = (grid.p[ n].x * 2) + 2;
            grid.p[ n].y = (grid.p[ n].y * 2) + 2;
            grid.p[ n].z = (grid.p[ n].z * 2) + 2;
				  }

				  // marching cube, returns number of triangles
				  n = PolygoniseCube( grid, isolevel, triangles);

          if( Pass != 0) {                 // second pass

            // store the trinangles and points
				    for( l = 0; l < n; l++) {

              p1[ 0] = triangles[l].p[0].x;
              p1[ 1] = triangles[l].p[0].y;
              p1[ 2] = triangles[l].p[0].z;

              p2[ 0] = triangles[l].p[1].x;
              p2[ 1] = triangles[l].p[1].y;
              p2[ 2] = triangles[l].p[1].z;

              p3[ 0] = triangles[l].p[2].x;
              p3[ 1] = triangles[l].p[2].y;
              p3[ 2] = triangles[l].p[2].z;

#ifdef use_again
              mesh->element[ nTriangles * 3 + 0] = VoxelsToIqeModelVertex( p1, mesh->position, &nPoints);
              mesh->element[ nTriangles * 3 + 1] = VoxelsToIqeModelVertex( p2, mesh->position, &nPoints);
              mesh->element[ nTriangles * 3 + 2] = VoxelsToIqeModelVertex( p3, mesh->position, &nPoints);
#else
              mesh->element[ nTriangles * 3 + 0] = VoxelsToIqeModelVertex( p3, mesh->position, &nPoints);
              mesh->element[ nTriangles * 3 + 1] = VoxelsToIqeModelVertex( p2, mesh->position, &nPoints);
              mesh->element[ nTriangles * 3 + 2] = VoxelsToIqeModelVertex( p1, mesh->position, &nPoints);
#endif
              nTriangles += 1;
				    }

          } else {

            // first pass, only count trinangles and points

            nTriangles += n;
            nPoints += 3 * n;
          }
        }
      }
    }

    if( Pass == 0) {                 // first pass

      // prepare vertexes
	    mesh->vertex_count = nPoints;

      if( nPoints > 1) {

   	    mesh->position = (float *)malloc( sizeof( float) * nPoints * 3);
      } else {

   	    mesh->position = NULL;
      }

      // prepare edges

	    mesh->element_count = nTriangles * 3;
      if( nTriangles > 1) {

	      mesh->element = (int *)malloc( sizeof( int) * nTriangles * 3);
      } else {

   	    mesh->element = NULL;
      }

    } else {                 // second pass

      // update vertex count, this may be less
	    mesh->vertex_count = nPoints;

      mesh->position = (float *)realloc( mesh->position, sizeof( float) * nPoints * 3); // reallocte memory
    }
  }

  // convert coordinates back to model space
  // NOTE: We multiply here with 0.5 to compensate for the multiply
  //       of 2 we have donw for the integer coordinates.

  pVertex = mesh->position;

	for (i0 = 0; i0 < mesh->vertex_count; i0++, pVertex += 3) {

    int MergedCoord;

    MergedCoord = ((int *)pVertex)[ 0];     // get merged coordinate

    pVertex[ 0] = (((MergedCoord >>  0) & 0x3ff) - 2 ) * pVoxelData->ScaleVoxelToMesh * 0.5 + pVoxelData->minbox[ 0];
    pVertex[ 1] = (((MergedCoord >> 10) & 0x3ff) - 2 ) * pVoxelData->ScaleVoxelToMesh * 0.5 + pVoxelData->minbox[ 1];
    pVertex[ 2] = (((MergedCoord >> 20) & 0x3ff) - 2 ) * pVoxelData->ScaleVoxelToMesh * 0.5 + pVoxelData->minbox[ 2];
	}

  // prepare rest of model

	mesh->normal      = NULL;
	mesh->texcoord    = NULL;
	mesh->color       = NULL;
	mesh->blendindex  = NULL;
	mesh->blendweight = NULL;
	mesh->aposition   = NULL;
	mesh->anormal     = NULL;

	mesh->part_count = 1;
	mesh->part = (part *)malloc( sizeof( struct part));
	memset( mesh->part, 0, sizeof( struct part));

	mesh->part->pMaterialStr     = strdup( "");
	mesh->part->pMaterialTags    = strdup( "");
	mesh->part->pMaterialBasedir = strdup( "");
	mesh->part->material = -1;
	mesh->part->first = 0;
	mesh->part->count = mesh->element_count;

	// recontruct normals if we have none

	if( mesh->normal == NULL) {

    IqeB_MeshComputeVertexNormals( mesh);
	}

  return model;
}

/************************************************************************************

  IqeB_VoxelToIqeCubes

  Convert voxel model to IQE Model.
  We have cubic surfes.

  Return: NULL   Error
          else   Pointer to IQE model ready to display
*/

IQE_model *IqeB_VoxelToIqeCubes( T_VoxelModelData *pVoxelData,   // IN: Voxel mesh
                                 char *pErrorString)
{
  int TempCoord[ 3];
  VOX_CEL_TYPE *pVox, *pVoxBase, *pVoxNext;
  int i0, i1, i2, iAxis, iAxis1, iAxis2;
  int VoxStep, VoxThis, VoxLast;
  int Pass, nPoints, nTriangles;
  float *pVertex;

	// allocated an empty model

	struct IQE_model *model = IqeB_ModelCreateEmpty();    // get empty model
	if( model == NULL) {                                   // memory allocation failed

    return( NULL);
	}

  // for less writing, some short cuts

	struct skel *skel = model->skel;
	struct mesh *mesh = model->mesh;

  // ...

	skel->joint_count = 0;

  // march the voxels from all sides

  for( Pass = 0; Pass < 2; Pass++) {

    nTriangles = 0;
    nPoints = 0;

    for( iAxis = 0; iAxis < 3; iAxis++) {

      // prepare other axis

      iAxis1 = OtherAxis1[ iAxis];
      iAxis2 = OtherAxis2[ iAxis];

      // get step along this axis through the voxel data

      pVoxBase = pVoxelCoordinate( pVoxelData, 0, 0, 0);
      pVoxNext = pVoxelCoordinate( pVoxelData, 0 == iAxis, 1 == iAxis, 2 == iAxis);
      VoxStep  = pVoxNext - pVoxBase;

      for( i1 = 0; i1 < pVoxelData->Size[ iAxis1]; i1++) {

        for( i2 = 0; i2 < pVoxelData->Size[ iAxis2]; i2++) {

          // 1. side

          // start point of voxels
          TempCoord[ iAxis]  = 0;
          TempCoord[ iAxis1] = i1;
          TempCoord[ iAxis2] = i2;

          pVox = pVoxelCoordinate( pVoxelData, TempCoord[ 0], TempCoord[ 1], TempCoord[ 2]);

          // get begin of voxel stripes

          VoxLast = 0;                                       // The last voxel is not set

          for( i0 = 0; i0 < pVoxelData->Size[ iAxis]; i0++, pVox += VoxStep, TempCoord[ iAxis] += 1) {

            VoxThis = *pVox;

            if( VoxLast == 0 && VoxThis != 0) {              // begin of set voxel stripe

              // emit points and traingels for this surface

              if( Pass != 0) {                 // second pass

                int p1[ 3], p2[ 3], p3[ 3], p4[ 3];
                int x1, x2, x3, x4;

                // points for this voxel face

                memcpy( p1, TempCoord, sizeof( p1));

                memcpy( p2, p1, sizeof( p1));
                p2[ iAxis1] += 1;

                memcpy( p3, p1, sizeof( p1));
                p3[ iAxis1] += 1;
                p3[ iAxis2] += 1;

                memcpy( p4, p1, sizeof( p1));
                p4[ iAxis2] += 1;

                x1 = VoxelsToIqeModelVertex( p1, mesh->position, &nPoints);
                x2 = VoxelsToIqeModelVertex( p2, mesh->position, &nPoints);
                x3 = VoxelsToIqeModelVertex( p3, mesh->position, &nPoints);
                x4 = VoxelsToIqeModelVertex( p4, mesh->position, &nPoints);

                // triangles for this voxel face

                mesh->element[ nTriangles * 3 + 0] = x3;
                mesh->element[ nTriangles * 3 + 1] = x2;
                mesh->element[ nTriangles * 3 + 2] = x1;
                nTriangles += 1;

                mesh->element[ nTriangles * 3 + 0] = x4;
                mesh->element[ nTriangles * 3 + 1] = x3;
                mesh->element[ nTriangles * 3 + 2] = x1;
                nTriangles += 1;

              } else {

                nPoints    += 4;
                nTriangles += 2;
              }
            }

            VoxLast = VoxThis;
          }

          // 2. side

          // start point of voxels
          TempCoord[ iAxis]  = pVoxelData->Size[ iAxis] - 1;
          TempCoord[ iAxis1] = i1;
          TempCoord[ iAxis2] = i2;

          pVox = pVoxelCoordinate( pVoxelData, TempCoord[ 0], TempCoord[ 1], TempCoord[ 2]);

          // get begin of voxel stripes

          VoxLast = 0;                                       // The last voxel is not set

          TempCoord[ iAxis] += 1;   // Must point after last voxel
          for( i0 = 0; i0 < pVoxelData->Size[ iAxis]; i0++, pVox -= VoxStep, TempCoord[ iAxis] -= 1) {

            VoxThis = *pVox;

            if( VoxLast == 0 && VoxThis != 0) {              // begin of set voxel stripe

              // emit points and traingels for this surface

              if( Pass != 0) {                 // second pass

                int p1[ 3], p2[ 3], p3[ 3], p4[ 3];
                int x1, x2, x3, x4;

                // points for this voxel face

                memcpy( p1, TempCoord, sizeof( p1));

                memcpy( p2, p1, sizeof( p1));
                p2[ iAxis1] += 1;

                memcpy( p3, p1, sizeof( p1));
                p3[ iAxis1] += 1;
                p3[ iAxis2] += 1;

                memcpy( p4, p1, sizeof( p1));
                p4[ iAxis2] += 1;

                x1 = VoxelsToIqeModelVertex( p1, mesh->position, &nPoints);
                x2 = VoxelsToIqeModelVertex( p2, mesh->position, &nPoints);
                x3 = VoxelsToIqeModelVertex( p3, mesh->position, &nPoints);
                x4 = VoxelsToIqeModelVertex( p4, mesh->position, &nPoints);

                // triangles for this voxel face

                mesh->element[ nTriangles * 3 + 0] = x1;
                mesh->element[ nTriangles * 3 + 1] = x2;
                mesh->element[ nTriangles * 3 + 2] = x3;
                nTriangles += 1;

                mesh->element[ nTriangles * 3 + 0] = x1;
                mesh->element[ nTriangles * 3 + 1] = x3;
                mesh->element[ nTriangles * 3 + 2] = x4;
                nTriangles += 1;

              } else {

                nPoints    += 4;
                nTriangles += 2;
              }
            }

            VoxLast = VoxThis;
          }
        }
      }
    }

    if( Pass == 0) {                 // first pass

      // prepare vertexes
	    mesh->vertex_count = nPoints;

      if( nPoints > 1) {

   	    mesh->position = (float *)malloc( sizeof( float) * nPoints * 3);
      } else {

   	    mesh->position = NULL;
      }

      // prepare edges

	    mesh->element_count = nTriangles * 3;
      if( nTriangles > 1) {

	      mesh->element = (int *)malloc( sizeof( int) * nTriangles * 3);
      } else {

   	    mesh->element = NULL;
      }

    } else {                 // second pass

      // update vertex count, this may be less
	    mesh->vertex_count = nPoints;

      mesh->position = (float *)realloc( mesh->position, sizeof( float) * nPoints * 3); // reallocte memory
    }
  }

  // convert coordinates back to model space

  pVertex = mesh->position;

	for (i0 = 0; i0 < mesh->vertex_count; i0++, pVertex += 3) {

    int MergedCoord;

    MergedCoord = ((int *)pVertex)[ 0];     // get merged coordinate

    pVertex[ 0] = ((MergedCoord >>  0) & 0x3ff) * pVoxelData->ScaleVoxelToMesh + pVoxelData->minbox[ 0];
    pVertex[ 1] = ((MergedCoord >> 10) & 0x3ff) * pVoxelData->ScaleVoxelToMesh + pVoxelData->minbox[ 1];
    pVertex[ 2] = ((MergedCoord >> 20) & 0x3ff) * pVoxelData->ScaleVoxelToMesh + pVoxelData->minbox[ 2];
	}

  // prepare rest of model

	mesh->normal      = NULL;
	mesh->texcoord    = NULL;
	mesh->color       = NULL;
	mesh->blendindex  = NULL;
	mesh->blendweight = NULL;
	mesh->aposition   = NULL;
	mesh->anormal     = NULL;

	mesh->part_count = 1;
	mesh->part = (part *)malloc( sizeof( struct part));
	memset( mesh->part, 0, sizeof( struct part));

	mesh->part->pMaterialStr     = strdup( "");
	mesh->part->pMaterialTags    = strdup( "");
	mesh->part->pMaterialBasedir = strdup( "");
	mesh->part->material = -1;
	mesh->part->first = 0;
	mesh->part->count = mesh->element_count;

	// recontruct normals if we have none

	if( mesh->normal == NULL) {

    IqeB_MeshComputeVertexNormals( mesh);
	}

  return model;
}

/************************************************************************************

  IqeB_VoxelCoordWorld2Voxel

  Transform coordinsates in world space to voxel space.
*/

void IqeB_VoxelCoordWorld2Voxel( T_VoxelModelData *pVoxelData, vec3 pOut, const vec3 pIn)
{

#ifndef use_again
  vec_sub( pOut, pIn, pVoxelData->minbox);            // relative to minmax box
  vec_scale_to( pOut, pVoxelData->ScaleMeshToVoxel);
#else
  vec_copy( pOut, pIn);                               // get point
  vec_sub_from( pOut, pVoxelData->minbox);            // relative to minmax box
  vec_scale_to( pOut, pVoxelData->ScaleMeshToVoxel);
#endif
}

/************************************************************************************

  IqeB_VoxelCoordVoxelInt2Offset

  Transform coordinsates in voxel space to an offset
  relative to the first voxel data.

  SEE also pVoxelCoordinate().

*/
int IqeB_VoxelCoordVoxelInt2Offset( T_VoxelModelData *pVoxelData,     // IN: Voxel mesh
                                    int ix, int iy, int iz)           // IN: Coordinates in voxel space
{
  int Offset;

  // Clipping

  if( ix < 0) {
    ix = 0;
  }
  if( ix >= pVoxelData->Size[ 0]) {
    ix = pVoxelData->Size[ 0] - 1;
  }
  if( iy < 0) {
    iy = 0;
  }
  if( iy >= pVoxelData->Size[ 1]) {
    iy = pVoxelData->Size[ 1] - 1;
  }
  if( iz < 0) {
    iz = 0;
  }
  if( iz >= pVoxelData->Size[ 2]) {
    iz = pVoxelData->Size[ 2] - 1;
  }

  Offset  = ix * pVoxelData->Size[ 1] * pVoxelData->Size[ 2];
  Offset += iy * pVoxelData->Size[ 2];
  Offset += iz;

  return( Offset);
}

/************************************************************************************

  IqeB_VoxelCoordVoxelFloat2Offset

  Transform coordinsates in voxel space to an offset
  relative to the first voxel data.

  SEE also pVoxelCoordinate().

*/
int IqeB_VoxelCoordVoxelFloat2Offset( T_VoxelModelData *pVoxelData,   // IN: Voxel mesh
                                      const vec3 pIn)                 // IN: Coordinates in voxel space
{
  int Offset;

  Offset = IqeB_VoxelCoordVoxelInt2Offset( pVoxelData,
                                           (int)(pIn[ 0] + 0.5), (int)(pIn[ 1] + 0.5), (int)(pIn[ 2] + 0.5));

  return( Offset);
}

/************************************************************************************

  IqeB_VoxelLineSet

  Write line to voxel.
  Write a the enpoints of a line to the voxel data
  than divide the line and recursive call itselft.
  NOTE: * Coordinates give are in space of voxel volume.
        * Size of a voxel is 1 unit each side.
*/

void IqeB_VoxelLineSet( T_VoxelModelData *pVoxelData,  // IN: Voxel mesh
                        vec3 p1, vec3 p2,              // IN: Points of line
                        int PointFillMask,             // IN: 0x01, 0x02, 0x03: bitmask witch point to write, p1 or p2
                        int FillValue)                 // IN: Value to fill
{
  float d;
  vec3 m;
  VOX_CEL_TYPE *p;

  if( PointFillMask & 0x01) {  // Fill for first point

    p = pVoxelCoordinate( pVoxelData, (int)(p1[ 0] + 0.5), (int)(p1[ 1] + 0.5), (int)(p1[ 2] + 0.5));
    *p = FillValue;
  }

  if( PointFillMask & 0x02) {  // Fill for second point

    p = pVoxelCoordinate( pVoxelData, (int)(p2[ 0] + 0.5), (int)(p2[ 1] + 0.5), (int)(p2[ 2] + 0.5));
    *p = FillValue;
  }

  // subdivide line

  d = vec_dist2( p1, p2);                 // squared distance

  if( d < VOX_CUPE_SET_LEN_THRESHOLD) {   // below length threshold
    return;
  }

  // split line

  vec_add( m, p1, p2);
  vec_scale_to( m, 0.5);

  // recursive fill

  IqeB_VoxelLineSet( pVoxelData, m, p1, 0x01, FillValue);
  IqeB_VoxelLineSet( pVoxelData, m, p2, 0x00, FillValue);
}

/************************************************************************************

  IqeB_VoxelLineTest

  Test a line in the voxels.
  Test the enpoints of a line in the voxel data
  than divide the line and recursive call itselft.
  NOTE: * Coordinates give are in world space.
          Will be transformed to space of voxel volume.
        * Size of a voxel is 1 unit each side.

  return: 0: OK, all points on the lines are OK
          1: failed, any point on the line is not OK
*/

static int IqeB_VoxelLineTestSub( T_VoxelModelData *pVoxelData,  // IN: Voxel mesh
                        vec3 p1, vec3 p2,              // IN: Points of line
                        int PointTestMask,             // IN: 0x01 set: Test first point, 0x02 set: test second point
                        int TestType,                  // IN: 0/1 : Test for any cell equal/unequal to 'TestValue' to be OK
                        int TestValue)                 // IN: Value to test
{
  float d;
  vec3 m;
  VOX_CEL_TYPE *p;
  int RetVal;

  if( PointTestMask & 0x01) {  // Test first point

    p = pVoxelCoordinate( pVoxelData, (int)(p1[ 0] + 0.5), (int)(p1[ 1] + 0.5), (int)(p1[ 2] + 0.5));

    if( TestType == 0) {      // Test for any cell equal to 'TestValue' to be OK

      if( *p != TestValue) {  // NOT equal
        return( 1);           // return not OK
      }
    } else {                  // Test for any cell unequal to 'TestValue' to be OK

      if( *p == TestValue) {  // is equal
        return( 1);           // return not OK
      }
    }
  }

  if( PointTestMask & 0x02) {  // Test second point

    p = pVoxelCoordinate( pVoxelData, (int)(p2[ 0] + 0.5), (int)(p2[ 1] + 0.5), (int)(p2[ 2] + 0.5));

    if( TestType == 0) {      // Test for any cell equal to 'TestValue' to be OK

      if( *p != TestValue) {  // NOT equal
        return( 1);           // return not OK
      }
    } else {                  // Test for any cell unequal to 'TestValue' to be OK

      if( *p == TestValue) {  // is equal
        return( 1);           // return not OK
      }
    }
  }

  // subdivide line

  d = vec_dist2( p1, p2);                // squared distance

  if( d < VOX_CUPE_SET_LEN_THRESHOLD) {  // below length threshold
    return( 0);                          // return OK
  }

  // split line

  vec_add( m, p1, p2);
  vec_scale_to( m, 0.5);

  // recursive test

  RetVal = IqeB_VoxelLineTestSub( pVoxelData, m, p1, 0x01, TestType, TestValue);
  if( RetVal != 0) {   // failed

    return( RetVal);   // return failed
  }

  RetVal = IqeB_VoxelLineTestSub( pVoxelData, m, p2, 0x00, TestType, TestValue);
  if( RetVal != 0) {   // failed

    return( RetVal);   // return failed
  }

  return( 0);          // return OK
}

int IqeB_VoxelLineTest( T_VoxelModelData *pVoxelData,  // IN: Voxel mesh
                        vec3 p1, vec3 p2,              // IN: Points of line in worldspace
                        int PointTestMask,             // IN: 0x01 set: Test first point, 0x02 set: test second point
                        int TestType,                  // IN: 0/1 : Test for any cell equal/unequal to 'TestValue' to be OK
                        int TestValue)                 // IN: Value to test
{
  vec3 p1_2, p2_2;
  int RetVal;

  // Transfrom coordianted from world to voxel space

  IqeB_VoxelCoordWorld2Voxel( pVoxelData, p1_2, p1);  // Transform from world to voxel space
  IqeB_VoxelCoordWorld2Voxel( pVoxelData, p2_2, p2);  // Transform from world to voxel space

  // and to the line test

  RetVal = IqeB_VoxelLineTestSub( pVoxelData, p1_2, p2_2, PointTestMask, TestType, TestValue);

  return( RetVal);          // return OK
}

/*********************************** End Of File ***********************************/
