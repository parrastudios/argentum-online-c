/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

/*struct VertexInfo
{
	struct Vector2f Vector;
	union CoordType Coord;
	struct Color4ub Color;
};

struct VertexCache
{
	struct VertexInfo * List;
	uint32				Count;
	uint32				Size;
	struct Texture *	Atlas;
};*/
////////////////
/*
GLenum	PrimitiveType;
GLuint	VertexArray;
GLuint	NormalArray;
GLuint	ColorArray;
GLuint* CoordArray;
GLuint	VertexObjectArray;

GLuint VertexBuilding;		// Building up vertexes counter (immediate mode emulator)
GLuint VertexCount;			// Number of verticies in this batch
GLuint TextureUnitsCount;	// Number of texture coordinate sets

bool    bBatchDone;                             // Batch has been built
 
       
                M3DVector3f *pVerts;
                M3DVector3f *pNormals;
                M3DVector4f *pColors;
                M3DVector2f **pTexCoords;
        */