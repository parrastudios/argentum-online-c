/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Terrain.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define TERRAIN_FACTORY_COMPONENTS \
		(WORLD_BUILD_TYPE_BASE, COMPONENT_DATA_BASE, TerrainComponentBaseSize), \
		(WORLD_BUILD_TYPE_TRANSFORM, COMPONENT_DATA_TRANSFORM, TerrainComponentTransformSize), \
		(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION, TerrainComponentCollisionSize), \
		(WORLD_BUILD_TYPE_PHYSICS, COMPONENT_DATA_PHYSICS, TerrainComponentPhysicsSize), \
		(WORLD_BUILD_TYPE_RENDER, COMPONENT_DATA_RENDER, TerrainComponentRenderSize), \
		(WORLD_BUILD_TYPE_OCCLUSION, COMPONENT_DATA_OCCLUSION, TerrainComponentOcclusionSize), \
		(WORLD_BUILD_TYPE_SOUND, COMPONENT_DATA_SOUND, TerrainComponentSoundSize)

#define TERRAIN_COMPONENT_MASK \
		EntityComponentMaskDefine( \
			(WORLD_BUILD_TYPE_BASE, COMPONENT_DATA_BASE), \
			(WORLD_BUILD_TYPE_TRANSFORM, COMPONENT_DATA_TRANSFORM), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION), \
			(WORLD_BUILD_TYPE_PHYSICS, COMPONENT_DATA_PHYSICS), \
			(WORLD_BUILD_TYPE_RENDER, COMPONENT_DATA_RENDER), \
			(WORLD_BUILD_TYPE_OCCLUSION, COMPONENT_DATA_OCCLUSION), \
			(WORLD_BUILD_TYPE_SOUND, COMPONENT_DATA_SOUND) \
		)

#define TERRAIN_COMPONENT_TYPES \
		EntityComponentTypesDefine( \
			(WORLD_BUILD_TYPE_BASE, COMPONENT_DATA_BASE, &TerrainComponentBaseImpl), \
			(WORLD_BUILD_TYPE_TRANSFORM, COMPONENT_DATA_TRANSFORM, &TerrainComponentTransformImpl), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION, &TerrainComponentCollisionImpl), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION_BOX, NULL), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION_SPHERE, NULL), \
			(WORLD_BUILD_TYPE_COLLISION, COMPONENT_DATA_COLLISION_FRUSTUM, NULL), \
			(WORLD_BUILD_TYPE_ANIMATION, COMPONENT_DATA_ANIMATION, NULL), \
			(WORLD_BUILD_TYPE_PHYSICS, COMPONENT_DATA_PHYSICS, &TerrainComponentPhysicsImpl), \
			(WORLD_BUILD_TYPE_RENDER, COMPONENT_DATA_RENDER, &TerrainComponentRenderImpl), \
			(WORLD_BUILD_TYPE_OCCLUSION, COMPONENT_DATA_OCCLUSION, &TerrainComponentOcclusionImpl), \
			(WORLD_BUILD_TYPE_SOUND, COMPONENT_DATA_SOUND, &TerrainComponentSoundImpl) \
		)

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize component pool of terrain factory
////////////////////////////////////////////////////////////
bool TerrainFactoryInitialize()
{
	// Create all component pools depending on build compilation
	EntityFactoryComponentCreateDefine(TerrainFactoryGet(), TERRAIN_FACTORY_COMPONENTS);

	return true;
}

////////////////////////////////////////////////////////////
/// Get terrain factory instance
////////////////////////////////////////////////////////////
struct EntityFactoryType * TerrainFactoryGet()
{
    static struct EntityFactoryType EntityTerrainFactory = {
        ENTITY_TYPE_TERRAIN,
        TypeInit(Create, &TerrainCreate),
        TypeInit(Destroy, &TerrainDestroy),
        TypeInit(InitializeImpl, &TerrainFactoryInitialize),
        TypeInit(DestroyImpl, &TerrainFactoryDestroy),
        TypeInit(EntityPool, NULL),
        TypeInit(EntityFreeIds, NULL),
        TypeInit(ComponentMap, NULL),
        TypeInit(ComponentDataMask, TERRAIN_COMPONENT_MASK),
        ExprInit(ComponentTypes, TERRAIN_COMPONENT_TYPES)
    };
    return &EntityTerrainFactory;
}

////////////////////////////////////////////////////////////
/// Destroy component pool of terrain factory
////////////////////////////////////////////////////////////
void TerrainFactoryDestroy()
{
	// Destroy all component pools depending on build compilation
	EntityFactoryComponentDestroyDefine(TerrainFactoryGet(), TERRAIN_FACTORY_COMPONENTS);
}
