/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_VIDEO_RENDER_H
#define GRAPHICS_VIDEO_RENDER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <System/Timer.h>

////////////////////////////////////////////////////////////
// Default SO includes and declarations
////////////////////////////////////////////////////////////
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
	#include <vfw.h>				///< Header file for video for windows

	typedef struct
	{
		AVISTREAMINFO	StreamInfo;	///< Pointer to a structure containing stream info
		PAVISTREAM		Stream;		///< Handle to an open stream
		PGETFRAME		File;		///< Pointer to a get frame oject
	} VideoType;

#else

    #ifdef _cplusplus

	#include <avifile/avifile.h>
	#include <avifile/version.h>

	#if AVIFILE_MINOR_VERSION > 6
	  #include <avifile/avm_default.h>
	#else
	  #include <avifile/default.h>
	#endif

	#include <avifile/image.h>
	#include <avifile/StreamInfo.h>

	#endif

	typedef struct
	{
	    #ifdef _cplusplus
		avm::IReadFile		*File;			///< Pointer to a structure containing stream info
		avm::IReadStream	*Stream;		///< Handle to an open stream
		StreamInfo			*StreamInfo;	///< Pointer to a get frame oject
		#endif

	} VideoType;
#endif

struct Video
{
	bool			Loaded;			///< Check if the video is loaded
	bool			Finished;		///< Check if the video is finished
	bool			Started;		///< Check if the video is started
	uint32			Width;			///< Video width size
	uint32			Height;			///< Video height size
	uint32			CurrentFrame;	///< Current video render frame
	uint32			LastFrame;		///< Frame length of the video
	uint32			MsPerFrame;		///< Miliseconds per frame
	VideoType		Object;			///< Video specific SO
	struct Timer	TimerData;		///< Video timer controller
};

////////////////////////////////////////////////////////////
// Load a video
////////////////////////////////////////////////////////////
void VideoLoad(struct Video * VideoData, char * FilePath);

////////////////////////////////////////////////////////////
// Render a frame of the video
////////////////////////////////////////////////////////////
void VideoRender(struct Video * VideoData, bool Loop, uint32 X, uint32 Y, uint32 Width, uint32 Height);

////////////////////////////////////////////////////////////
// Destroy a video
////////////////////////////////////////////////////////////
void VideoDelete(struct Video * VideoData);

////////////////////////////////////////////////////////////
// Destroy the video library
////////////////////////////////////////////////////////////
void VideoDestroy();

#endif // GRAPHICS_VIDEO_RENDER_H
