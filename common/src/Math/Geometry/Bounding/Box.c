/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/General.h>
#include <Math/Geometry/Bounding/Box.h>
#include <Math/Geometry/Plane.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
/// Create a bounding box
////////////////////////////////////////////////////////////
struct BoundingBox * BoundingBoxCreate()
{
	struct BoundingBox * BBoxPtr = (struct BoundingBox*)MemoryAllocate(sizeof(struct BoundingBox));

	Vector3fSetNull(&BBoxPtr->Bounds[BOUNDING_BOX_MIN]);
	Vector3fSetNull(&BBoxPtr->Bounds[BOUNDING_BOX_MAX]);
	Vector3fSetNull(&BBoxPtr->Center);

	return BBoxPtr;
}

////////////////////////////////////////////////////////////
/// Initialize a bounding box
////////////////////////////////////////////////////////////
struct BoundingBox * BoundingBoxInitialize(struct BoundingBox * Box)
{
	if (Box)
	{
		Vector3fSetNull(&Box->Bounds[BOUNDING_BOX_MIN]);
		Vector3fSetNull(&Box->Bounds[BOUNDING_BOX_MAX]);
		Vector3fSetNull(&Box->Center);
	}

	return Box;
}

////////////////////////////////////////////////////////////
/// Destroy a bounding box
////////////////////////////////////////////////////////////
void BoundingBoxDestroy(struct BoundingBox * Box)
{
	MemoryDeallocate(Box);
}

////////////////////////////////////////////////////////////
/// Copy a bounding box
////////////////////////////////////////////////////////////
void BoundingBoxCopy(struct BoundingBox * Dest, struct BoundingBox * Source)
{
	if (Dest && Source)
	{
	    Vector3fCopy(&Dest->Bounds[BOUNDING_BOX_MIN], &Source->Bounds[BOUNDING_BOX_MIN]);
	    Vector3fCopy(&Dest->Bounds[BOUNDING_BOX_MAX], &Source->Bounds[BOUNDING_BOX_MAX]);
	    Vector3fCopy(&Dest->Center, &Source->Center);
	}
}

////////////////////////////////////////////////////////////
/// Set to null a bounding box
////////////////////////////////////////////////////////////
void BoundingBoxSetNull(struct BoundingBox * Box)
{
	if (Box)
	{
		Vector3fSetNull(&Box->Bounds[BOUNDING_BOX_MIN]);
		Vector3fSetNull(&Box->Bounds[BOUNDING_BOX_MAX]);
		Vector3fSetNull(&Box->Center);
	}
}

////////////////////////////////////////////////////////////
/// Set the data of a bounding box
////////////////////////////////////////////////////////////
void BoundingBoxSet(struct BoundingBox * Box, struct Vector3f * Min, struct Vector3f * Max)
{
	// Copy the vectors
    Box->Bounds[BOUNDING_BOX_MIN].x = min(Min->x, Max->x);
    Box->Bounds[BOUNDING_BOX_MIN].y = min(Min->y, Max->y);
    Box->Bounds[BOUNDING_BOX_MIN].z = min(Min->z, Max->z);

    Box->Bounds[BOUNDING_BOX_MAX].x = max(Min->x, Max->x);
    Box->Bounds[BOUNDING_BOX_MAX].y = max(Min->y, Max->y);
    Box->Bounds[BOUNDING_BOX_MAX].z = max(Min->z, Max->z);

	// Recalculate the center
	BoundingBoxUpdateCenter(Box);
}

////////////////////////////////////////////////////////////
/// Set the data of a bounding box by its center and radius
////////////////////////////////////////////////////////////
void BoundingBoxSetRadius(struct BoundingBox * Box, struct Vector3f * Center, float Radius)
{
	if (Box && Center)
	{
		Vector3fCopy(&Box->Center, Center);

		Vector3fSet(&Box->Bounds[BOUNDING_BOX_MIN], Center->x - Radius, Center->y - Radius, Center->z - Radius);
		Vector3fSet(&Box->Bounds[BOUNDING_BOX_MAX], Center->x + Radius, Center->y + Radius, Center->z + Radius);
	}
}

////////////////////////////////////////////////////////////
/// Get positive corner according to the normal
////////////////////////////////////////////////////////////
void BoundingBoxGetVertexPositive(struct BoundingBox * Box, struct Vector3f * Normal, struct Vector3f * Result)
{
    if (Box && Normal && Result)
    {
        if (Normal->x >= 0.0f)
        {
            Result->x = Box->Bounds[BOUNDING_BOX_MAX].x;
        }
        else
        {
            Result->x = Box->Bounds[BOUNDING_BOX_MIN].x;
        }

        if (Normal->y >= 0.0f)
        {
            Result->y = Box->Bounds[BOUNDING_BOX_MAX].y;
        }
        else
        {
            Result->y = Box->Bounds[BOUNDING_BOX_MIN].y;
        }

        if (Normal->z >= 0.0f)
        {
            Result->z = Box->Bounds[BOUNDING_BOX_MAX].z;
        }
        else
        {
            Result->z = Box->Bounds[BOUNDING_BOX_MIN].z;
        }
    }
}

////////////////////////////////////////////////////////////
/// Get negative corner according to the normal
////////////////////////////////////////////////////////////
void BoundingBoxGetVertexNegative(struct BoundingBox * Box, struct Vector3f * Normal, struct Vector3f * Result)
{
    if (Box && Normal && Result)
    {
        if (Normal->x >= 0.0f)
        {
            Result->x = Box->Bounds[BOUNDING_BOX_MIN].x;
        }
        else
        {
            Result->x = Box->Bounds[BOUNDING_BOX_MAX].x;
        }

        if (Normal->y >= 0.0f)
        {
            Result->y = Box->Bounds[BOUNDING_BOX_MIN].y;
        }
        else
        {
            Result->y = Box->Bounds[BOUNDING_BOX_MAX].y;
        }

        if (Normal->z >= 0.0f)
        {
            Result->z = Box->Bounds[BOUNDING_BOX_MIN].z;
        }
        else
        {
            Result->z = Box->Bounds[BOUNDING_BOX_MAX].z;
        }
    }
}

////////////////////////////////////////////////////////////
/// Update the center of the bounding box
////////////////////////////////////////////////////////////
void BoundingBoxUpdateCenter(struct BoundingBox * Box)
{
	Vector3fSet(&Box->Center,
				 Box->Bounds[BOUNDING_BOX_MIN].x +
				 (Box->Bounds[BOUNDING_BOX_MAX].x - Box->Bounds[BOUNDING_BOX_MIN].x) * 0.5f,

				 Box->Bounds[BOUNDING_BOX_MIN].y +
				 (Box->Bounds[BOUNDING_BOX_MAX].y - Box->Bounds[BOUNDING_BOX_MIN].y) * 0.5f,

				 Box->Bounds[BOUNDING_BOX_MIN].z +
				 (Box->Bounds[BOUNDING_BOX_MAX].z - Box->Bounds[BOUNDING_BOX_MIN].z) * 0.5f);
}

////////////////////////////////////////////////////////////
/// Move bounding box into specified position as a center
////////////////////////////////////////////////////////////
void BoundingBoxMove(struct BoundingBox * Box, struct Vector3f * Center)
{
    struct Vector3f Difference;

    // Subtract to the new center, the old center in order to obtain the difference vector
    Vector3fSubtractEx(&Difference, Center, &Box->Center);

    // Increment to the bounds the difference
    Vector3fAdd(&Box->Bounds[BOUNDING_BOX_MIN], &Difference);
    Vector3fAdd(&Box->Bounds[BOUNDING_BOX_MAX], &Difference);

    // Update the current center to the new one
    Vector3fCopy(&Box->Center, Center);
}

////////////////////////////////////////////////////////////
/// Create a bounding box from a list of vertices
////////////////////////////////////////////////////////////
void BoundingBoxGenerate(struct BoundingBox * Box, struct Vector3f * VertList, uinteger Size)
{
	uinteger i;

	// Get the minimum and maximum position
	for (i = 0; i < Size; i++)
	{
		// Check the minimum size
		if (Box->Bounds[BOUNDING_BOX_MIN].x > VertList[i].x)
			Box->Bounds[BOUNDING_BOX_MIN].x = VertList[i].x;

		if (Box->Bounds[BOUNDING_BOX_MIN].y > VertList[i].y)
			Box->Bounds[BOUNDING_BOX_MIN].y = VertList[i].y;

		if (Box->Bounds[BOUNDING_BOX_MIN].z > VertList[i].z)
			Box->Bounds[BOUNDING_BOX_MIN].z = VertList[i].z;

		// Check the maximum size
		if (Box->Bounds[BOUNDING_BOX_MAX].x < VertList[i].x)
			Box->Bounds[BOUNDING_BOX_MAX].x = VertList[i].x;

		if (Box->Bounds[BOUNDING_BOX_MAX].y < VertList[i].y)
			Box->Bounds[BOUNDING_BOX_MAX].y = VertList[i].y;

		if (Box->Bounds[BOUNDING_BOX_MAX].z < VertList[i].z)
			Box->Bounds[BOUNDING_BOX_MAX].z = VertList[i].z;

	}

	// Recalculate the center
	BoundingBoxUpdateCenter(Box);
}

////////////////////////////////////////////////////////////
/// Check if two bounding box are colliding
////////////////////////////////////////////////////////////
uinteger BoundingBoxCollision(struct BoundingBox * Box, struct BoundingBox * Other)
{
	// Check if it is outside
	if (Box->Bounds[BOUNDING_BOX_MAX].x < Other->Bounds[BOUNDING_BOX_MIN].x ||
		Box->Bounds[BOUNDING_BOX_MIN].x > Other->Bounds[BOUNDING_BOX_MAX].x ||
		Box->Bounds[BOUNDING_BOX_MAX].y < Other->Bounds[BOUNDING_BOX_MIN].y ||
		Box->Bounds[BOUNDING_BOX_MIN].y > Other->Bounds[BOUNDING_BOX_MAX].y ||
		Box->Bounds[BOUNDING_BOX_MAX].z < Other->Bounds[BOUNDING_BOX_MIN].z ||
		Box->Bounds[BOUNDING_BOX_MIN].z > Other->Bounds[BOUNDING_BOX_MAX].z)
		return BOUNDING_BOX_OUTSIDE;

	// Check if it is inside
	if (Box->Bounds[BOUNDING_BOX_MAX].x <= Other->Bounds[BOUNDING_BOX_MAX].x &&
		Box->Bounds[BOUNDING_BOX_MIN].x >= Other->Bounds[BOUNDING_BOX_MIN].x &&
		Box->Bounds[BOUNDING_BOX_MAX].y <= Other->Bounds[BOUNDING_BOX_MAX].y &&
		Box->Bounds[BOUNDING_BOX_MIN].y >= Other->Bounds[BOUNDING_BOX_MIN].y &&
		Box->Bounds[BOUNDING_BOX_MAX].z <= Other->Bounds[BOUNDING_BOX_MAX].z &&
		Box->Bounds[BOUNDING_BOX_MIN].z >= Other->Bounds[BOUNDING_BOX_MIN].z)
		return BOUNDING_BOX_INSIDE;

	// If it are not outside and inside, so overlap (collision)
	return BOUNDING_BOX_OVERLAP;
}

////////////////////////////////////////////////////////////
/// Check if a bounding box is colliding with a cube
////////////////////////////////////////////////////////////
uinteger BoundingBoxCollisionCube(struct BoundingBox * Box, struct Vector3f * Center, float Radius)
{
	// Check if it is outside
	if (Box->Bounds[BOUNDING_BOX_MAX].x < Center->x - Radius || Box->Bounds[BOUNDING_BOX_MIN].x > Center->x + Radius ||
		Box->Bounds[BOUNDING_BOX_MAX].y < Center->y - Radius || Box->Bounds[BOUNDING_BOX_MIN].y > Center->y + Radius ||
		Box->Bounds[BOUNDING_BOX_MAX].z < Center->z - Radius || Box->Bounds[BOUNDING_BOX_MIN].z > Center->z + Radius)
		return BOUNDING_BOX_OUTSIDE;

	// Check if it is inside
	if (Box->Bounds[BOUNDING_BOX_MAX].x <= Center->x + Radius && Box->Bounds[BOUNDING_BOX_MIN].x >= Center->x - Radius &&
		Box->Bounds[BOUNDING_BOX_MAX].y <= Center->y + Radius && Box->Bounds[BOUNDING_BOX_MIN].y >= Center->y - Radius &&
		Box->Bounds[BOUNDING_BOX_MAX].z <= Center->z + Radius && Box->Bounds[BOUNDING_BOX_MIN].z >= Center->z - Radius)
		return BOUNDING_BOX_INSIDE;

	// If it are not outside and inside, so overlap (collision)
	return BOUNDING_BOX_OVERLAP;
}

////////////////////////////////////////////////////////////
/// Check if a bounding box is colliding with a sphere (qri algorithm)
////////////////////////////////////////////////////////////
uinteger BoundingBoxCollisionSphere(struct BoundingBox * Box, struct BoundingSphere * Sphere)
{
	// TODO:
	//	http://www.mrtc.mdh.se/projects/3Dgraphics/paperF.pdf

	// Arvo's Algorithm by now

	double SquaredRadius, LocalDistance, SquaredDistance = 0;

	#define CalculateDistance(Component) do { \
			if (Sphere->Center.Component < Box->Bounds[BOUNDING_BOX_MIN].Component) \
			{ \
				LocalDistance = Sphere->Center.Component - Box->Bounds[BOUNDING_BOX_MIN].Component; \
				SquaredDistance += sqr(LocalDistance); \
			} \
			else if (Sphere->Center.Component > Box->Bounds[BOUNDING_BOX_MAX].Component) \
			{ \
				LocalDistance = Sphere->Center.Component - Box->Bounds[BOUNDING_BOX_MAX].Component; \
				SquaredDistance += sqr(LocalDistance); \
			} \
		} while (0)

	// Calculate the square distance from sphere to box

	CalculateDistance(x);
	CalculateDistance(y);
	CalculateDistance(z);

	#undef CalculateDistance

	SquaredRadius = sqr(Sphere->Radius);

	if (SquaredDistance < SquaredRadius)
	{
		return BOUNDING_BOX_INSIDE;
	}
	else if (SquaredDistance == SquaredRadius) // should have some epsilon
	{
		return BOUNDING_BOX_OVERLAP;
	}
	else
	{
		return BOUNDING_BOX_OUTSIDE;
	}
}

////////////////////////////////////////////////////////////
/// Check if a bounding box is colliding with a plane
///
///  We use Hearn & Baker's definition of inside and outside.
///  In a right handed system, the normal abc is directed from inside (behind)
///  the plane to outside (in front of) the plane. The outside of the plane
///  is the visible side.
///  Polygons in the plane with counter-clockwise winding when viewed from
///  outside the plane are only visible from outside the plane (front facing).
///  Polygons in the plane with counter-clockwise winding when viewed from
///  inside the plane are only visible from inside the plane (back facing).
///                                 The plane equation is  ax + by + cz + d = 0.
///       Points inside (behind) the plane are defined by  ax + by + cz + d < 0
///  Points outside (in front of) the plane are defined by  ax + by + cz + d > 0
////////////////////////////////////////////////////////////
uinteger BoundingBoxCollisionPlane(struct BoundingBox * Box, struct PlaneType * Plane)
{
	// Assemble accept / reject points
	struct Vector3f Accept, Reject;
	uinteger i;

	for (i = 0; i < 3; ++i)
	{
		if (Plane->SignBits & (8 << i))
		{
			Reject.v[i] = Box->Bounds[BOUNDING_BOX_MAX].v[i];
			Accept.v[i] = Box->Bounds[BOUNDING_BOX_MIN].v[i];
		}
		else
		{
			Reject.v[i] = Box->Bounds[BOUNDING_BOX_MIN].v[i];
			Accept.v[i] = Box->Bounds[BOUNDING_BOX_MAX].v[i];
		}
	}

	// If reject point is outside the clipping plane
	if (!PlaneCheckPoint(Plane, &Reject))
		return BOUNDING_BOX_OUTSIDE;

	// If accept point is inside the clipping plane
	if (PlaneCheckPoint(Plane, &Accept))
		return BOUNDING_BOX_INSIDE;

	// Otherwise overlap
	return BOUNDING_BOX_OVERLAP;
}

////////////////////////////////////////////////////////////
/// Check if a bounding box is colliding with a line
///
///  Returns
///
///	false if no intersection -	line segment is parallel to and outside box,
///								or stops before box, or starts after box.
///	true if intersection -		some or all of line segment is inside box.
///
///  If intersection
///
///	s0 and s1 are set to the fractional distance along the line segment to
///	the intersection points. If s0 < 0, the line segment starts inside the
///	box, if s1 > 1, the line segment ends inside the box.
///
////////////////////////////////////////////////////////////
bool BoundingBoxCollisionLine(struct BoundingBox * Box, struct Vector3f * p0, struct Vector3f * p1, float * s0, float * s1)
{
	uinteger i;
	float d, t0, t1, temp;

	*s0 = -(float)FLT_MAX;
	*s1 =  (float)FLT_MAX;

	// For each dimension
	for (i = 0; i < 3; i++)
	{
		d = p1->v[i] - p0->v[i];
		// Check parallel
		if (d == 0.0f) {
			// Check outside
			if (p0->v[i] < Box->Bounds[BOUNDING_BOX_MIN].v[i] || p0->v[i] > Box->Bounds[BOUNDING_BOX_MAX].v[i])
				// Line is parallel to side and outside box
				return false;
		}
		else {
			// Not parallel - get intersections
			t0 = (Box->Bounds[BOUNDING_BOX_MIN].v[i] - p0->v[i]) / d;
			t1 = (Box->Bounds[BOUNDING_BOX_MAX].v[i] - p0->v[i]) / d;
			if (t0 > t1) {
				temp = t0; t0 = t1; t1 = temp;
			}
			if (t0 > *s0)
				*s0 = t0;		// biggest s near
			if (t1 < *s1)
				*s1 = t1;		// smallest s far
			if (*s0 > *s1)
				return false;	// line misses box
			if (*s1 < 0.0f)
				return false;	// line starts after box
			if (*s0 > 1.0f)
				return false;	// line ends before box
		}
	}
	return true;
}


////////////////////////////////////////////////////////////
/// Check if a bounding box is colliding with a ray
////////////////////////////////////////////////////////////
bool BoundingBoxCollisionRay(struct BoundingBox * Box, struct RayType * Ray)
{
	struct Vector3f RayDelta;

	Vector3fMultiplyEx(&RayDelta, &Ray->Direction, Ray->Length);

	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Distance squared to a point from the box (arvos algorithm)
////////////////////////////////////////////////////////////
double BoundingBoxDistanceSquared(struct BoundingBox * Box, struct Vector3f * Point)
{
	//vec3 Distance = { 0.0f, 0.0f, 0.0f };
	// todo

	return 0.0f;
}

////////////////////////////////////////////////////////////
/// Distance to a point from the box
////////////////////////////////////////////////////////////
double BoundingBoxDistance(struct BoundingBox * Box, struct Vector3f * Point)
{
	double DistanceSquared = BoundingBoxDistanceSquared(Box, Point);

	return (double)sqrt(DistanceSquared);
}

////////////////////////////////////////////////////////////
/// Longest axis of the bounding box
////////////////////////////////////////////////////////////
uinteger BoundingBoxLongestAxis(struct BoundingBox * Box, double * Distance)
{
	uinteger i, Dimension = 0;
	double Distances[3] = { (double)fabs(Box->Bounds[BOUNDING_BOX_MIN].x - Box->Bounds[BOUNDING_BOX_MAX].x),
						    (double)fabs(Box->Bounds[BOUNDING_BOX_MIN].y - Box->Bounds[BOUNDING_BOX_MAX].y),
						    (double)fabs(Box->Bounds[BOUNDING_BOX_MIN].z - Box->Bounds[BOUNDING_BOX_MAX].z) };

	double Temp = Distances[0];

    for (i = 1; i < 3; i++)
	{
		double Dist = max(Distances[i], Temp);

		if (Dist > Temp)
		{
			Temp = Dist;
			Dimension = i;
		}
    }

	if (Distance)
		*Distance = Temp;

	return Dimension;
}

////////////////////////////////////////////////////////////
/// Shortest axis of the bounding box
////////////////////////////////////////////////////////////
uinteger BoundingBoxShortestAxis(struct BoundingBox * Box, double * Distance)
{
	uinteger i, Dimension = 0;
	double Distances[3] = { (double)fabs(Box->Bounds[BOUNDING_BOX_MIN].x - Box->Bounds[BOUNDING_BOX_MAX].x),
						    (double)fabs(Box->Bounds[BOUNDING_BOX_MIN].y - Box->Bounds[BOUNDING_BOX_MAX].y),
						    (double)fabs(Box->Bounds[BOUNDING_BOX_MIN].z - Box->Bounds[BOUNDING_BOX_MAX].z) };
	double Temp = Distances[0];

    for (i = 1; i < 3; i++)
	{
		double Dist = min(Distances[i], Temp);

		if (Dist < Temp)
		{
			Temp = Dist;
			Dimension = i;
		}
    }

	if (Distance)
		*Distance = Temp;

	return Dimension;
}

////////////////////////////////////////////////////////////
/// Return the distance of an axis of the bounding box
////////////////////////////////////////////////////////////
double BoundingBoxAxisDistance(struct BoundingBox * Box, uinteger Axis)
{
	if (Box && Axis < 3)
	{
		double Distances[3] = { (double)fabs(Box->Bounds[BOUNDING_BOX_MIN].x - Box->Bounds[BOUNDING_BOX_MAX].x),
							    (double)fabs(Box->Bounds[BOUNDING_BOX_MIN].y - Box->Bounds[BOUNDING_BOX_MAX].y),
							    (double)fabs(Box->Bounds[BOUNDING_BOX_MIN].z - Box->Bounds[BOUNDING_BOX_MAX].z) };

		return Distances[Axis];
	}

	return 0.0f;
}
