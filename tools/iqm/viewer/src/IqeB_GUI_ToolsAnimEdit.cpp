/****************************************************************************

  IqeB_GUI_ToolsAnimEdit.cpp

  07.05.2015 RR: First editon of this file.


*****************************************************************************
*/

#ifdef _DEBUG
//#define USE_DRAW_DEBUG_TEXT  1   // uncomment this to display a debug text
#endif

#include "IqeBrowser.h"

#include <math.h>

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Menu.H>
#include <FL/Fl_Scroll.H>
#include <FL/fl_draw.H>
#include <FL/math.h>
#include <FL/Fl_Radio_Round_Button.H>

/************************************************************************************
 * Globals
 *
 */

/************************************************************************************
 * Statics
 */

// define for window sizes
#define MYWIN_SIZE_X_MIN       530  // IQE_GUI_TOOLS_STD_WITDH
#define MYWIN_SIZE_X_MAX      2048
#define MYWIN_SIZE_X_DEFAULT   720

#define MYWIN_SIZE_Y_MIN       451
#define MYWIN_SIZE_Y_MAX      2048
#define MYWIN_SIZE_Y_DEFAULT   451

// ...

static  Fl_Double_Window  *pMyToolWin;
static int MyWinPosX = IQE_GUI_NO_WINPOS_X, MyWinPosY = IQE_GUI_NO_WINPOS_Y;     // last window position
static int MyWinSizeX = MYWIN_SIZE_X_DEFAULT, MyWinSizeY = MYWIN_SIZE_Y_DEFAULT; // last window size

static Fl_Scroll *pGUI_Scroll;            // Timeline

// Pose input elements
static Fl_Float_Input *pFloat_InputPX, *pFloat_InputPY, *pFloat_InputPZ; // Joint position
static Fl_Float_Input *pFloat_InputA0, *pFloat_InputA1, *pFloat_InputA2; // Joint angles
static Fl_Float_Input *pFloat_InputSX, *pFloat_InputSY, *pFloat_InputSZ; // Joint size

// Pose reset buttons
static Fl_Button *pBut_ResetPX, *pBut_ResetPY, *pBut_ResetPZ; // Joint position
static Fl_Button *pBut_ResetA0, *pBut_ResetA1, *pBut_ResetA2; // Joint angles
static Fl_Button *pBut_ResetSX, *pBut_ResetSY, *pBut_ResetSZ; // Joint size

// ...

static struct pose *pPose_Data_Displayed;                                // Point to pose of displayed data

static int PoseLastDrawnFrameNr = -1, PoseLastDrawnPoseNr = -1;      // Last drawn for pose GUI elements

// Groups. Need special attention if size of window is changed.

static  Fl_Group *pGroupViewMode, *pGroupPose, *pGroupPosePart;
static int GroupViewModeSizeXX, GroupViewModeSizeYY;
static int GroupPosePartSizeX, GroupPosePartSizeY, GroupPosePartSizeXX, GroupPosePartSizeYY;
static int GroupPoseSizeXX, GroupPoseSizeYY;

// View type

#define IQE_ANIM_EDIT_VIEW_MODE_TIMELINE   0  // View mode timeline
#define IQE_ANIM_EDIT_VIEW_MODE_PLOT_3     1  // View mode 3 plot graphs
#define IQE_ANIM_EDIT_VIEW_MODE_PLOT_X     2  // View mode X plot graph
#define IQE_ANIM_EDIT_VIEW_MODE_PLOT_Y     3  // View mode Y plot graph
#define IQE_ANIM_EDIT_VIEW_MODE_PLOT_Z     4  // View mode Z plot graph

static int   IqeB_ToolsAnimEdit_ViewMode = IQE_ANIM_EDIT_VIEW_MODE_TIMELINE;    //

// What pose part to edit

int IqeB_ToolsAnimEdit_PosePart = IQE_ANIM_EDIT_POSE_PART_ANGLES;  // What pose part to edit

int IqeB_ToolsAnimEdit_PoseScaleAll = true;  // For pose scale change, change all scales

// Other axis than indexed

static int OtherAxis1[ 3] = { 1, 0, 0};
static int OtherAxis2[ 3] = { 2, 2, 1};

/************************************************************************************
 * IqeB_GUI_ToolsJointUpdate
 *
 * Updates the GUI things for single joint
 *
 * iJoint: >= 0  Update for this joint
 *         < 0   No joint to update
 */

static void IqeB_GUI_ToolsFramePoseUpdate( int iFrame, int iPose)
{
  char TempString[ 256];
  struct pose *pPose;

  if( pDrawModel == NULL ||
      pDrawModel->anim_count <= 0 ||   // No animations
      pDrawModel->anim_poses <= 0 ||   // No poses
      pDrawModelCurAnim == NULL ||     // No current animation
      iFrame < 0 || iFrame >= pDrawModelCurAnim->len ||  // Frame indes out of range
      iPose < 0 || iPose >= pDrawModel->anim_poses) {    // Pose index out of range

    if( pFloat_InputPX) {  // TEST, have GUI elements

      // Pose input elements

      pFloat_InputPX->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputPX, false); // Set item activated/inactive
      pFloat_InputPY->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputPY, false); // Set item activated/inactive
      pFloat_InputPZ->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputPZ, false); // Set item activated/inactive

      pFloat_InputA0->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputA0, false); // Set item activated/inactive
      pFloat_InputA1->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputA1, false); // Set item activated/inactive
      pFloat_InputA2->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputA2, false); // Set item activated/inactive

      pFloat_InputSX->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputSX, false); // Set item activated/inactive
      pFloat_InputSY->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputSY, false); // Set item activated/inactive
      pFloat_InputSZ->value( "");
      IqeB_GUI_WidgetActivate( pFloat_InputSZ, false); // Set item activated/inactive

      // Pose reset buttons

      IqeB_GUI_WidgetActivate( pBut_ResetPX, false); // Set item activated/inactive
      IqeB_GUI_WidgetActivate( pBut_ResetPY, false); // Set item activated/inactive
      IqeB_GUI_WidgetActivate( pBut_ResetPZ, false); // Set item activated/inactive

      IqeB_GUI_WidgetActivate( pBut_ResetA0, false); // Set item activated/inactive
      IqeB_GUI_WidgetActivate( pBut_ResetA1, false); // Set item activated/inactive
      IqeB_GUI_WidgetActivate( pBut_ResetA2, false); // Set item activated/inactive

      IqeB_GUI_WidgetActivate( pBut_ResetSX, false); // Set item activated/inactive
      IqeB_GUI_WidgetActivate( pBut_ResetSY, false); // Set item activated/inactive
      IqeB_GUI_WidgetActivate( pBut_ResetSZ, false); // Set item activated/inactive
    }

    pPose_Data_Displayed = NULL;       // Reset pointer to displayed data
    PoseLastDrawnFrameNr = -1;         // Reset last drawn pose GUI elements
    PoseLastDrawnPoseNr  = -1;

    return;
  }

  PoseLastDrawnFrameNr = iFrame;  // Set last drawn pose GUI elements
  PoseLastDrawnPoseNr  = iPose;

  // ...

  pPose = pDrawModelCurAnim->data[ iFrame];  // This frame

  pPose += iPose;                            // This pose

  pPose_Data_Displayed = pPose;              // Set pointer to displayed data

  if( pFloat_InputPX) {  // TEST, have GUI elements

    // Pose input elements

    sprintf( TempString, "%.3f", pPose->location[ 0]);
    pFloat_InputPX->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputPX, true); // Set item activated/inactive
    sprintf( TempString, "%.3f", pPose->location[ 1]);
    pFloat_InputPY->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputPY, true); // Set item activated/inactive
    sprintf( TempString, "%.3f", pPose->location[ 2]);
    pFloat_InputPZ->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputPZ, true); // Set item activated/inactive

    sprintf( TempString, "%.3f", pPose->angles[ 0]);
    pFloat_InputA0->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputA0, true); // Set item activated/inactive
    sprintf( TempString, "%.3f", pPose->angles[ 1]);
    pFloat_InputA1->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputA1, true); // Set item activated/inactive
    sprintf( TempString, "%.3f", pPose->angles[ 2]);
    pFloat_InputA2->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputA2, true); // Set item activated/inactive

    sprintf( TempString, "%.3f", pPose->scale[ 0]);
    pFloat_InputSX->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputSX, true); // Set item activated/inactive
    sprintf( TempString, "%.3f", pPose->scale[ 1]);
    pFloat_InputSY->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputSY, true); // Set item activated/inactive
    sprintf( TempString, "%.3f", pPose->scale[ 2]);
    pFloat_InputSZ->value( TempString);
    IqeB_GUI_WidgetActivate( pFloat_InputSZ, true); // Set item activated/inactive

    // Pose reset buttons

    IqeB_GUI_WidgetActivate( pBut_ResetPX, true); // Set item activated/inactive
    IqeB_GUI_WidgetActivate( pBut_ResetPY, true); // Set item activated/inactive
    IqeB_GUI_WidgetActivate( pBut_ResetPZ, true); // Set item activated/inactive

    IqeB_GUI_WidgetActivate( pBut_ResetA0, true); // Set item activated/inactive
    IqeB_GUI_WidgetActivate( pBut_ResetA1, true); // Set item activated/inactive
    IqeB_GUI_WidgetActivate( pBut_ResetA2, true); // Set item activated/inactive

    IqeB_GUI_WidgetActivate( pBut_ResetSX, true); // Set item activated/inactive
    IqeB_GUI_WidgetActivate( pBut_ResetSY, true); // Set item activated/inactive
    IqeB_GUI_WidgetActivate( pBut_ResetSZ, true); // Set item activated/inactive
  }
}

/************************************************************************************
 * AnimEditDrawing
 *
 * Drawing for the animation window
 */

static void AnimEditDrawingEventAction( int event);  // Forward
static int AnimEditShortcutKey( int event_key, int event_state);  // Forward

class AnimEditDrawing : public Fl_Widget {

  void draw();

  // overwrite event handler

  int handle(int event) {

    switch( event) {
    case FL_ENTER:      // The mouse has been moved to point at this widget.
    case FL_LEAVE:      // The mouse has moved out of the widget.
    case FL_MOVE:       // The mouse has moved without any mouse buttons held down.
    case FL_DRAG:       // The mouse has moved with a button held down.
    case FL_MOUSEWHEEL: // The user has moved the mouse wheel.
    case FL_PUSH:       // A mouse button has gone down
    case FL_RELEASE:    // A mouse button has been released.

      AnimEditDrawingEventAction( event);

      return( 1);
      break;

    case FL_SHORTCUT:

      // Go for this shortcut key
      return( AnimEditShortcutKey( Fl::event_key(), Fl::event_state()));
      break;

    }  // end switch( event)

    return( Fl_Widget::handle( event));     // do parent handle function
  }  // end function int handle(int event)

public:
  AnimEditDrawing( int X,int Y,int W,int H,const char* L) : Fl_Widget(X,Y,W,H,L) {
    align(FL_ALIGN_TOP);
    box(FL_FLAT_BOX);
  }
};

static AnimEditDrawing *pAnimEditDrawing;
static int AnimEditDrawingResetScrolHack = 0;  // Hack to reset scroll bars 0: start, 1: first draw done, 2: done
static int AnimEditDrawForceRedraw = false;    // Set this to force a redraw
#ifdef USE_DRAW_DEBUG_TEXT   // display a debug text
static char AnimEditDrawDebugStr[ 256];        // Debug string to output on the drawing area
#endif
// Drawing layout, is needed by AnimEditDrawingEventAction() for mouse handling
static int DrawLayout_Valid = false;           // set true if draing layout is valid
static int DrawLayout_JNames_left_X;           // Joint names left side
static int DrawLayout_JNames_right_X;          // Joint names right side
static int DrawLayout_JNames_top_Y;            // Joint names top side
static int DrawLayout_JNames_bottom_y;         // Joint names bottom side
static int DrawLayout_Frames_left_X;           // Frame area left side
static int DrawLayout_Frames_right_X;          // Frame area right side
static int DrawLayout_Frames_top_Y;            // Frame area top side
static int DrawLayout_Frames_bottom_y;         // Frame area bottom side

// Mouse things

static int MouseOverJointNr = -1;              // Mouse is over joint
static int MouseOverFrameNr = -1;              // Mouse is over frame
static int MouseOverAxisNr = -1;               // Mouse is over graph view (of this axis

// Catch some last drawn things, used to monitor reasons to redraw
static int AnimEditDrawLastFrameNr = -1;       // Last drawn frame

// --- Draw --------------------------------------------

#define ANIM_DRAW_COL_JOINT_NAME_SEP_LINES   0, 196,   0  // Color for joint seperated lines

#define ANIM_DRAW_COL_JOINT_SLECTED_BGND   160, 255, 255  // Color for backgrund of selected joint
#define ANIM_DRAW_COL_JOINT_MOUSE_BGND       0, 255, 255  // Color for backgrund of mouse over joint row

#define ANIM_DRAW_COL_FRAME_SLECTED_BGND   ANIM_DRAW_COL_JOINT_SLECTED_BGND  // Color for backgrund of selected frame
#define ANIM_DRAW_COL_FRAME_MOUSE_BGND     ANIM_DRAW_COL_JOINT_MOUSE_BGND    // Color for backgrund of mouse over frame
//#define ANIM_DRAW_COL_FRAME_SLECTED_BGND   255, 128, 255  // Color for backgrund of selected frame
//#define ANIM_DRAW_COL_FRAME_MOUSE_BGND     255,   0, 255  // Color for backgrund of mouse over frame

#define ANIM_DRAW_COL_PLOT_LINE              255,   0, 255  // Plot graph, line color
#define ANIM_DRAW_COL_JOINT_KEY              255,   0,   0  // Plot graph, joint has a keyflag set
#define ANIM_DRAW_COL_JOINT_NO_KEY             0,   0,   0  // Plot graph, joint has no keyflag set
#define ANIM_DRAW_COL_JOINT_SELECTED         255, 255,   0  // Plot graph, joint is selected
#define ANIM_DRAW_COL_PLOT_AXIS_MAIN         192,   0,   0  // Plot graph, axis line main
#define ANIM_DRAW_COL_PLOT_AXIS_MINOR        160, 160, 160  // Plot graph, axis line minor

#define ANIM_DRAW_COL_SECONDS                  0,  32, 255  // Color for the seconds
#define ANIM_DRAW_COL_SECONDS_TICKS            0, 196, 255  // Color for the seconds tickmarks

#define ANIM_DRAW_COL_MAKRED                   0,  192,   0  // Color for marked frames

#define ANIM_DRAW_HEIGHT_NO_DRAW  100      // set heigth if nothing to draw
#define ANIM_DRAW_WIDTH_NO_DRAW   300      // set width if nothing to draw

#define ANIM_DRAW_FONT_NR           0      // Use this fond drawings
#define ANIM_DRAW_FONT_SIZE        14      // Use this font size for drawings

#define ANIM_DRAW_HEADER_YY  (ANIM_DRAW_FONT_SIZE * 2 + 2)      // Drawing ident x
#define ANIM_DRAW_IDENT_X           4      // Drawing ident x
#define ANIM_DRAW_IDENT_Y           4      // Drawing ident y

#define ANIM_DRAW_TIME_STEP_XX     12      // Time step delta

// Defines for plot mode

#define ANIM_DRAW_PLOT_DIST_Y        8     // Distance of plot graphs from each other

#define ANIM_DRAW_PLOT3_SIZE_Y      114     // 3 graphs: Size for single graph in pixel
#define ANIM_DRAW_PLOT3_ANGLE_RANGE 114.0   // 3 graphs: Angle range for graph data (to both side)
#define ANIM_DRAW_PLOT3_YY (3 * ANIM_DRAW_PLOT3_SIZE_Y + 3 * ANIM_DRAW_PLOT_DIST_Y) // Complete height

#define ANIM_DRAW_PLOT1_SIZE_Y      360     // 1 graph: Size for single graph in pixel
#define ANIM_DRAW_PLOT1_ANGLE_RANGE 180.0   // 1 graph: Angle range for graph data (to both side)
#define ANIM_DRAW_PLOT1_YY (ANIM_DRAW_PLOT1_SIZE_Y + ANIM_DRAW_PLOT_DIST_Y) // Complete height

#define ANIM_DRAW_PLOT_SCALE_RANGE  2.5      // Range for scales

// ...

static void AnimEditDrawingChangeSize( int AoiX, int AoiY, int *pAoiXX, int *pAoiYY, int NewXX, int NewYY, int PosToZero)
{

  if( *pAoiXX != NewXX || *pAoiYY != NewYY) {

    *pAoiXX = NewXX;
    *pAoiYY = NewYY;
    pAnimEditDrawing->size( *pAoiXX, *pAoiYY);

    pAnimEditDrawing->redraw();
    pGUI_Scroll->redraw();   // force later redraw

    // Hack to reset scroll bars 0: start, 1: first draw done, 2: done
    AnimEditDrawingResetScrolHack = 1;         // first draw done
  }
}

void AnimEditDrawing::draw()
{
  Fl_Font CurrFont;
  Fl_Fontsize CurrFontSize;
  int AoiX, AoiY, AoiXX, AoiYY, NewXX, NewYY, MaxX;
  int iJoint, x, y, xBase, yBase, xLast, yLast, iFrameNr;
  int   iAxis, PlotAxisBegin, PlotAxisEnd, PlotSizeY2;
  vec3 ObjSize, ObjCenter;
  float PlotAngleRange, ObjSizeMax, GridStep;
  int   CoordAfterpointDigits, CoordWithField;
  char TempString[ 256];
  static char *AxisNames[ 3] = { (char *)"X", (char *)"Y", (char *)"Z"};  // Name of axis

  // Hack to reset scroll bars 0: start, 1: first draw done, 2: done

  if( AnimEditDrawingResetScrolHack == 0) {    // was started

    AnimEditDrawingResetScrolHack = 1;         // first draw done
  }

  // One more of this stupid hacks, after resing the animation editor window,
  // the group sizes has changed --> fix it

  if( pGroupViewMode->w() != GroupViewModeSizeXX ||
      pGroupViewMode->h() != GroupViewModeSizeYY) {    // Size has changed

    pGroupViewMode->size( GroupViewModeSizeXX, GroupViewModeSizeYY);
  }

  if( pGroupPosePart->x() != GroupPosePartSizeX ||
      pGroupPosePart->y() != GroupPosePartSizeY ||
      pGroupPosePart->w() != GroupPosePartSizeXX ||
      pGroupPosePart->h() != GroupPosePartSizeYY) {    // Size has changed

    pGroupPosePart->resize( GroupPosePartSizeX, GroupPosePartSizeY, GroupPosePartSizeXX, GroupPosePartSizeYY);
  }

  if( pGroupPose->w() != GroupPoseSizeXX ||
      pGroupPose->h() != GroupPoseSizeYY) {    // Size has changed

    pGroupPose->size( GroupPoseSizeXX, GroupPoseSizeYY);
  }

  // .....

  DrawLayout_Valid = false;           // Set draing layout to not valid

  // Catch some last drawn things

  AnimEditDrawLastFrameNr = DrawModelCurframe;   // catch last drawn frame

  // Get aoi

  AoiX  = this->x();
  AoiY  = this->y();
  AoiXX = this->w();
  AoiYY = this->h();

  // get current font setting

  CurrFont = fl_font();                        // Current font
  CurrFontSize = fl_size();

  // Draw ...

  xBase = AoiX + ANIM_DRAW_IDENT_X;
  yBase = AoiY + ANIM_DRAW_IDENT_Y;

  x = xBase;
  y = yBase;

  // debug output

#ifdef USE_DRAW_DEBUG_TEXT   // display a debug text

  // Some debug output
  if( AnimEditDrawDebugStr[ 0] != '\0') {  // Have something to draw

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore font
    fl_color( FL_BLACK);

    fl_draw( AnimEditDrawDebugStr, x, y + ANIM_DRAW_FONT_SIZE);
  }
#endif

  // TEST, have an animation to draw

  if( pDrawModel == NULL) {  // no model

    NewXX = ANIM_DRAW_WIDTH_NO_DRAW;     // Change size
    NewYY = ANIM_DRAW_HEIGHT_NO_DRAW;
    AnimEditDrawingChangeSize( AoiX, AoiY,&AoiXX, &AoiYY, NewXX, NewYY, true);

    x += pGUI_Scroll->xposition();
    y += pGUI_Scroll->yposition();

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE * 3 / 2);
    fl_color( FL_DARK_RED);

    y += ANIM_DRAW_FONT_SIZE * 3 / 2;
    fl_draw( "NO model", x, y);

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore font
    fl_color( FL_BLACK);

    y += ANIM_DRAW_FONT_SIZE;
    fl_draw( "Load a model and select an animation.", x, y);

    goto ExitPoint;

  } else if( pDrawModel->skel == NULL || pDrawModel->skel->joint_count <= 0) {  // no skeleton

    NewXX = ANIM_DRAW_WIDTH_NO_DRAW;     // Change size
    NewYY = ANIM_DRAW_HEIGHT_NO_DRAW;
    AnimEditDrawingChangeSize( AoiX, AoiY,&AoiXX, &AoiYY, NewXX, NewYY, true);

    x += pGUI_Scroll->xposition();
    y += pGUI_Scroll->yposition();

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE * 3 / 2);
    fl_color( FL_DARK_RED);

    y += ANIM_DRAW_FONT_SIZE * 3 / 2;
    fl_draw( "NO skeleton", x, y);

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore font
    fl_color( FL_BLACK);

    y += ANIM_DRAW_FONT_SIZE;
    fl_draw( "Model has no skeleton.", x, y);

    goto ExitPoint;

  } else if( pDrawModel->anim_count <= 0) {   // No animations

    NewXX = ANIM_DRAW_WIDTH_NO_DRAW;     // Change size
    NewYY = ANIM_DRAW_HEIGHT_NO_DRAW;
    AnimEditDrawingChangeSize( AoiX, AoiY,&AoiXX, &AoiYY, NewXX, NewYY, true);

    x += pGUI_Scroll->xposition();
    y += pGUI_Scroll->yposition();

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE * 3 / 2);
    fl_color( FL_DARK_RED);

    y += ANIM_DRAW_FONT_SIZE * 3 / 2;
    fl_draw( "NO animations", x, y);

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore font
    fl_color( FL_BLACK);

    y += ANIM_DRAW_FONT_SIZE;
    fl_draw( "Model has no animations.", x, y);

    goto ExitPoint;

  } else if( pDrawModel->skel->joint_count != pDrawModel->anim_poses) {  // Mismatch, num sceleton joints and animation poses

    NewXX = ANIM_DRAW_WIDTH_NO_DRAW;     // Change size
    NewYY = ANIM_DRAW_HEIGHT_NO_DRAW;
    AnimEditDrawingChangeSize( AoiX, AoiY,&AoiXX, &AoiYY, NewXX, NewYY, true);

    x += pGUI_Scroll->xposition();
    y += pGUI_Scroll->yposition();

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE * 3 / 2);
    fl_color( FL_DARK_RED);

    y += ANIM_DRAW_FONT_SIZE * 3 / 2;
    fl_draw( "Mismatch joints / animation poses", x, y);

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore font
    fl_color( FL_BLACK);

    y += ANIM_DRAW_FONT_SIZE;
    fl_draw( "Mismatchh of # skeleton joints and # animation poses.", x, y);

    y += ANIM_DRAW_FONT_SIZE;
    fl_draw( "Model must be fixed.", x, y);

    y += ANIM_DRAW_FONT_SIZE;
    fl_draw( "Maybe recreate of skeleton or animations helps.", x, y);

    goto ExitPoint;

#ifdef IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized
  } else if( ! pDrawModel->skel->IsNormalized) {  // skeleton is NOT normalized

    NewXX = ANIM_DRAW_WIDTH_NO_DRAW;     // Change size
    NewYY = ANIM_DRAW_HEIGHT_NO_DRAW;
    AnimEditDrawingChangeSize( AoiX, AoiY,&AoiXX, &AoiYY, NewXX, NewYY, true);

    x += pGUI_Scroll->xposition();
    y += pGUI_Scroll->yposition();

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE * 3 / 2);
    fl_color( FL_DARK_RED);

    y += ANIM_DRAW_FONT_SIZE * 3 / 2;
    fl_draw( "Skeleton is not NORMALIZED", x, y);

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore font
    fl_color( FL_BLACK);

    y += ANIM_DRAW_FONT_SIZE;
    fl_draw( "Need a normalized skeleton for animation.", x, y);

    y += ANIM_DRAW_FONT_SIZE;
    fl_draw( "See window 'Skeleton/Files + utils', button 'Normalize'.", x, y);

    goto ExitPoint;
#endif // IQE_FEATURE_ANIM_OLY_NORMALIZED // Animation edtior works only if the skeleton is normalized

  } else if( pDrawModelCurAnim == NULL) {  // no animation

    NewXX = ANIM_DRAW_WIDTH_NO_DRAW;     // Change size
    NewYY = ANIM_DRAW_HEIGHT_NO_DRAW;
    AnimEditDrawingChangeSize( AoiX, AoiY,&AoiXX, &AoiYY, NewXX, NewYY, true);

    x += pGUI_Scroll->xposition();
    y += pGUI_Scroll->yposition();

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE * 3 / 2);
    fl_color( FL_DARK_RED);

    y += ANIM_DRAW_FONT_SIZE * 3 / 2;
    fl_draw( "NO animation", x, y);

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore font
    fl_color( FL_BLACK);

    y += ANIM_DRAW_FONT_SIZE;
    fl_draw( "Model has no animations.", x, y);

    goto ExitPoint;

  }

  //
  // Have an model with skeleton and animation
  //

  // Get with of joint names

  fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Must set font for this

  x = xBase;
  MaxX = xBase + 110;   // Minimum, indent 80 pixel

  for( iJoint = 0; iJoint < pDrawModel->skel->joint_count; iJoint++) {

    int dx, dy, wo, ho;

    fl_text_extents( pDrawModel->skel->j[ iJoint].name, dx, dy, wo, ho);

    if( MaxX < x + dx + wo) {

      MaxX = x + dx + wo;
    }
  }

  MaxX += 4;

  // Width of window depents on number of frames

  xLast = MaxX + pDrawModelCurAnim->len * ANIM_DRAW_TIME_STEP_XX;     // Change size
  NewXX = xLast - AoiX + 16;

  if( NewXX < ANIM_DRAW_WIDTH_NO_DRAW) {   // keep some minimum

    NewXX = ANIM_DRAW_WIDTH_NO_DRAW;
  }

  // Height of window depents from number of joints

  NewYY = ANIM_DRAW_HEADER_YY + ANIM_DRAW_IDENT_Y * 2 + pDrawModel->skel->joint_count * (ANIM_DRAW_FONT_SIZE + 3);

  // Or height for plot mode

  if( NewYY < ANIM_DRAW_HEADER_YY + ANIM_DRAW_IDENT_Y * 2 + ANIM_DRAW_PLOT3_YY) {

    NewYY = ANIM_DRAW_HEADER_YY + ANIM_DRAW_IDENT_Y * 2 + ANIM_DRAW_PLOT3_YY - 2;
  }

  if( NewYY < ANIM_DRAW_HEADER_YY + ANIM_DRAW_IDENT_Y * 2 + ANIM_DRAW_PLOT1_YY) {

    NewYY = ANIM_DRAW_HEADER_YY + ANIM_DRAW_IDENT_Y * 2 + ANIM_DRAW_PLOT1_YY - 2;
  }

  // Keep some minimum

  if( NewYY < ANIM_DRAW_HEIGHT_NO_DRAW) {

    NewYY = ANIM_DRAW_HEIGHT_NO_DRAW;
  }

  AnimEditDrawingChangeSize( AoiX, AoiY,&AoiXX, &AoiYY, NewXX, NewYY, false);

  // drawing laout info, is needed by AnimEditDrawingEventAction() for mouse handling

  yLast = yBase + ANIM_DRAW_HEADER_YY + 2 + pDrawModel->skel->joint_count * (ANIM_DRAW_FONT_SIZE + 3) - 1;

  DrawLayout_Valid = true;                           // Draing layout is valid
  DrawLayout_JNames_left_X   = AoiX - AoiX;          // Joint names left side
  DrawLayout_JNames_right_X  = MaxX - AoiX - 1;      // Joint names right side
  DrawLayout_JNames_top_Y    = yBase + ANIM_DRAW_HEADER_YY + 1 - AoiY; // Joint names top side
  DrawLayout_JNames_bottom_y = yLast - AoiY;         // Joint names bottom side

  // Draw info for plot scale graph
  IqeB_DispModelGetInfo( ObjSize, ObjCenter, &GridStep, &CoordAfterpointDigits);

  if( CoordAfterpointDigits >= 0) {
    CoordWithField = 4;
  } else {
    CoordWithField = CoordAfterpointDigits + 4;
  }

  ObjSizeMax = ObjSize[ 0];
  if( ObjSizeMax < ObjSize[ 1]) ObjSizeMax = ObjSize[ 1];
  if( ObjSizeMax < ObjSize[ 2]) ObjSizeMax = ObjSize[ 2];

  // Draw info for plot angle graph

  if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_TIMELINE) {

    // Layout for mode timeline

    // set some dummy values to get no compiler warnings
    PlotAxisBegin = 0;        // Begin with this axis
    PlotAxisEnd   = -1;       // End with this axis
    PlotSizeY2     = ANIM_DRAW_PLOT1_SIZE_Y / 2;   // 1/2 height of graph in pixels
    PlotAngleRange = ANIM_DRAW_PLOT1_ANGLE_RANGE;  // Angle range in degrees (to both side)

  } else {

    // Layout for mode plot

    if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_PLOT_3) {  // Show thre axxis

      PlotAxisBegin = 0;          // Begin with this axis
      PlotAxisEnd   = 2;          // End with this axis
      PlotSizeY2     = ANIM_DRAW_PLOT3_SIZE_Y / 2;   // 1/2 height of graph in pixels
      PlotAngleRange = ANIM_DRAW_PLOT3_ANGLE_RANGE;  // Angle range in degrees (to both side)

      yLast = yBase + ANIM_DRAW_HEADER_YY + ANIM_DRAW_PLOT3_YY + 1;

    } else {   // show single axis

      PlotAxisBegin = IqeB_ToolsAnimEdit_ViewMode - IQE_ANIM_EDIT_VIEW_MODE_PLOT_X;        // Begin with this axis
      PlotAxisEnd   = PlotAxisBegin;  // End with this axis
      PlotSizeY2     = ANIM_DRAW_PLOT1_SIZE_Y / 2;   // 1/2 height of graph in pixels
      PlotAngleRange = ANIM_DRAW_PLOT1_ANGLE_RANGE;  // Angle range in degrees (to both side)

      yLast = yBase + ANIM_DRAW_HEADER_YY + ANIM_DRAW_PLOT1_YY + 1;
    }
  }

  DrawLayout_Frames_left_X   = MaxX - AoiX + 1;      // Frame area left side
  DrawLayout_Frames_right_X  = xLast - AoiX;         // Frame area right side
  DrawLayout_Frames_top_Y    = AoiY - AoiY;          // Frame area top side
  DrawLayout_Frames_bottom_y = yLast - AoiY;         // Frame area bottom side

  // Color background for selected joint

  if( IqeB_DispStyle_JointSelected >= 0) {

    y = yBase + ANIM_DRAW_HEADER_YY + IqeB_DispStyle_JointSelected * (ANIM_DRAW_FONT_SIZE + 3) - 2;

    fl_color( fl_rgb_color( ANIM_DRAW_COL_JOINT_SLECTED_BGND));
    if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_TIMELINE) {

      // Layout for mode timeline
      fl_rectf( AoiX, y + 4, xLast - AoiX, ANIM_DRAW_FONT_SIZE + 2);
    } else {

      // Layout for mode plot

      fl_rectf( AoiX, y + 4, MaxX - AoiX, ANIM_DRAW_FONT_SIZE + 2);
    }
  }

  // Color background for mouse of joint row

  if( MouseOverJointNr >= 0) {

    y = yBase + ANIM_DRAW_HEADER_YY + MouseOverJointNr * (ANIM_DRAW_FONT_SIZE + 3) - 2;

    fl_color( fl_rgb_color( ANIM_DRAW_COL_JOINT_MOUSE_BGND));

    if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_TIMELINE) {

      // Layout for mode timeline
      fl_rectf( AoiX, y + 4, xLast - AoiX, ANIM_DRAW_FONT_SIZE + 2);
    } else {

      // Layout for mode plot

      fl_rectf( AoiX, y + 4, MaxX - AoiX, ANIM_DRAW_FONT_SIZE + 2);
    }
  }

  // Color background for selected frame

  if( DrawModelCurframe >= pDrawModelCurAnim->len) {  //Security test, out of ranme

    DrawModelCurframe = -1;  // Set invalid
  }

  if( DrawModelCurframe >= 0) {

    fl_color( fl_rgb_color( ANIM_DRAW_COL_FRAME_SLECTED_BGND));

    x = MaxX + DrawModelCurframe * ANIM_DRAW_TIME_STEP_XX + 1;

    fl_rectf( x, AoiY, ANIM_DRAW_TIME_STEP_XX - 1, yLast - AoiY);
  }

  // Color background for mouse over frame

  if( MouseOverFrameNr >= pDrawModelCurAnim->len) {  //Security test, out of ranme

    MouseOverFrameNr = -1;  // Set invalid
  }

  if( MouseOverFrameNr >= 0) {

    fl_color( fl_rgb_color( ANIM_DRAW_COL_FRAME_MOUSE_BGND));

    x = MaxX + MouseOverFrameNr * ANIM_DRAW_TIME_STEP_XX + 1;

    fl_rectf( x, AoiY, ANIM_DRAW_TIME_STEP_XX - 1, yLast - AoiY);
  }

  // Draw marked frames
  // Draw from marked frame until mouse over frame column

  if( pDrawModelCurAnim->FrameMarked >= 0){   // A Frame is marked

    int Frame1, Frame2;

    Frame1 = pDrawModelCurAnim->FrameMarked;  // This frame

    if( Frame1 >= pDrawModelCurAnim->len) {   // security test
      Frame1 = -1;
    }

    if( Frame1 >= 0) {

      if( MouseOverFrameNr >= 0) {
        if( MouseOverFrameNr < Frame1) {  // Is lower

          // Keep asscending
          Frame2 = Frame1;
          Frame1 = MouseOverFrameNr;
        } else {
          Frame2 = MouseOverFrameNr;
        }
      } else {
        Frame2 = Frame1;
      }

      fl_color( fl_rgb_color( ANIM_DRAW_COL_MAKRED));

      x = MaxX + Frame1 * ANIM_DRAW_TIME_STEP_XX + 1;

      fl_rectf( x - 1, yBase + ANIM_DRAW_HEADER_YY / 2 - 2, (Frame2 - Frame1 + 1) * ANIM_DRAW_TIME_STEP_XX + 1, ANIM_DRAW_HEADER_YY / 2 + 3);
    }
  }

  // draw the joint names of the skeleton

  fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore font
  fl_color( FL_BLACK);

  x = xBase;
  y = yBase + ANIM_DRAW_HEADER_YY - 2;

  for( iJoint = 0; iJoint < pDrawModel->skel->joint_count; iJoint++) {

    // draw horizontal lines between joint names

    if( iJoint > 0) {
      fl_color( fl_rgb_color( ANIM_DRAW_COL_JOINT_NAME_SEP_LINES));
      if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_TIMELINE) {

        // Layout for mode timeline

        fl_line( AoiX, y + 3, xLast, y + 3);
      } else {

        // Layout for mode plot

        fl_line( AoiX, y + 3, MaxX, y + 3);
      }
    }

    // draw joint name

    fl_color( FL_BLACK);
    y += ANIM_DRAW_FONT_SIZE + 3;
    fl_draw( pDrawModel->skel->j[ iJoint].name, x, y);
  }

  fl_color( FL_BLACK);

  if( MaxX >= 0) {

    if( y + 2 > yLast) {
      fl_line( MaxX, AoiY, MaxX, y + 2);
    } else {
      fl_line( MaxX, AoiY, MaxX, yLast);
    }
  }

  // Info area in the left upper corner

  {
    int dx, dy, wo, ho;

     fl_color( fl_rgb_color( ANIM_DRAW_COL_SECONDS));
     sprintf( TempString, "Seconds:");
     fl_text_extents( TempString, dx, dy, wo, ho);   // measure extents
     fl_draw( TempString, MaxX - wo - 3, yBase + ANIM_DRAW_FONT_SIZE - ANIM_DRAW_IDENT_Y);    // and draw string

     fl_color( FL_BLACK);
     if( DrawModelCurframe >= 0) {     // selected frame
       sprintf( TempString, "%d/%d Frames:", DrawModelCurframe + 1, pDrawModelCurAnim->len);
     } else {                          // No frames selected
       sprintf( TempString, "-/%d Frames:", pDrawModelCurAnim->len);
     }
     fl_text_extents( TempString, dx, dy, wo, ho);   // measure extents
     fl_draw( TempString, MaxX - wo - 3, yBase + ANIM_DRAW_HEADER_YY - 1);    // and draw string
  }

  // draw upper/lower lines

  fl_color( FL_BLACK);

  fl_line( AoiX, yBase + ANIM_DRAW_HEADER_YY + 1, xLast, yBase + ANIM_DRAW_HEADER_YY + 1);

  if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_TIMELINE) {

    // Layout for mode timeline

    fl_line( AoiX, yLast, xLast, yLast);
  } else {

    // Layout for mode plot

    fl_line( AoiX, DrawLayout_JNames_bottom_y + AoiY, MaxX, DrawLayout_JNames_bottom_y + AoiY);

    // plot upper/lower lines

    y = yBase + ANIM_DRAW_HEADER_YY + 1;

    for( iAxis = PlotAxisBegin; iAxis <= PlotAxisEnd; iAxis++) {

      int y2;

      y += ANIM_DRAW_PLOT_DIST_Y + PlotSizeY2;

      // upper/lower line
      fl_line( MaxX, y - PlotSizeY2, xLast, y - PlotSizeY2);
      fl_line( MaxX, y + PlotSizeY2, xLast, y + PlotSizeY2);

      fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE * 2 / 3);

      // center line

      switch( IqeB_ToolsAnimEdit_PosePart) {
      case IQE_ANIM_EDIT_POSE_PART_LOCATION:  // Pose part to edit: location

        // From Zero to positive

        float GridStepUse, PlotMax, PlotMin;

        PlotMax = ObjCenter[iAxis] + ObjSizeMax;
        if( PlotMax < ObjSizeMax) PlotMax = ObjSizeMax;
        PlotMin = ObjCenter[iAxis] - ObjSizeMax;
        if( PlotMin > -ObjSizeMax) PlotMin = -ObjSizeMax;

        if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_PLOT_3) {  // Show thre axxis

          GridStepUse = GridStep * 10;
        } else {

          GridStepUse = GridStep * 5;
        }

        for( float ObjectY = 0.0; ObjectY < PlotMax; ObjectY += GridStepUse) {

          if( ObjectY < ObjCenter[iAxis] - ObjSizeMax) {  // Too low
            continue;   // Out of graph area
          }

          y2 = (int)( (ObjectY * PlotSizeY2 / ObjSizeMax) + 0.5);  // to ...

          if( y2 >= PlotSizeY2 - 12 || y2 <=  12 - PlotSizeY2) {
            continue;   // Out of grah area
          }

          y2 = y - y2;  // Relative to this

          if( ObjectY == 0.0) {
            fl_color( ANIM_DRAW_COL_PLOT_AXIS_MAIN);
          } else {
            fl_color( ANIM_DRAW_COL_PLOT_AXIS_MINOR);
          }
          fl_line( MaxX + 1, y2, xLast, y2);

	        sprintf( TempString, "%*.*f",
                                  CoordWithField, CoordAfterpointDigits,
                                  ObjectY);
          fl_color( ANIM_DRAW_COL_PLOT_AXIS_MAIN);
          fl_draw( TempString, MaxX + 3, y2 - 2);
        }

        // From Zero to negative

        for( float ObjectY = - GridStepUse; ObjectY > PlotMin; ObjectY -= GridStepUse) {

          if( ObjectY > ObjCenter[iAxis] + ObjSizeMax) {  // Too high
            continue;   // Out of graph area
          }

          y2 = (int)( (ObjectY * PlotSizeY2 / ObjSizeMax) + 0.5);  // to ...

          if( y2 >= PlotSizeY2 - 12 || y2 <=  12 - PlotSizeY2) {
            continue;   // Out of grah area
          }

          y2 = y - y2;  // Relative to this

          if( ObjectY == 0.0) {
            fl_color( ANIM_DRAW_COL_PLOT_AXIS_MAIN);
          } else {
            fl_color( ANIM_DRAW_COL_PLOT_AXIS_MINOR);
          }
          fl_line( MaxX + 1, y2, xLast, y2);

	        sprintf( TempString, "%*.*f",
                                  CoordWithField, CoordAfterpointDigits,
                                  ObjectY);
          fl_color( ANIM_DRAW_COL_PLOT_AXIS_MAIN);
          fl_draw( TempString, MaxX + 3, y2 - 2);
        }

        break;

      case IQE_ANIM_EDIT_POSE_PART_ANGLES:    // Pose part to edit: angles

        // 0 degree line
        fl_color( ANIM_DRAW_COL_PLOT_AXIS_MAIN);
        fl_line( MaxX + 1, y, xLast, y);
        fl_draw( "0�", MaxX + 3, y - 2);

        // 90 degree line

        if( PlotAngleRange >= 95.0) {

          y2 = (int)( (90.0 * PlotSizeY2 / PlotAngleRange) + 0.5);  // Angle to ...

          fl_color( ANIM_DRAW_COL_PLOT_AXIS_MINOR);
          fl_line( MaxX + 1, y - y2, xLast, y - y2);
          fl_line( MaxX + 1, y + y2, xLast, y + y2);

          fl_color( ANIM_DRAW_COL_PLOT_AXIS_MAIN);
          fl_draw( "+90�", MaxX + 3, y - y2 - 2);
          fl_draw( "-90�", MaxX + 3, y + y2 - 2);
        }

        // 45 degree line
        if( PlotAngleRange >= 50.0) {

          y2 = (int)( (45.0 * PlotSizeY2 / PlotAngleRange) + 0.5);  // Angle to ...

          fl_color( ANIM_DRAW_COL_PLOT_AXIS_MINOR);
          fl_line( MaxX + 1, y - y2, xLast, y - y2);
          fl_line( MaxX + 1, y + y2, xLast, y + y2);

          fl_color( ANIM_DRAW_COL_PLOT_AXIS_MAIN);
          fl_draw( "+45�", MaxX + 3, y - y2 - 2);
          fl_draw( "-45�", MaxX + 3, y + y2 - 2);
        }

        // 135 degree line
        if( PlotAngleRange >= 140.0) {

          y2 = (int)( (135.0 * PlotSizeY2 / PlotAngleRange) + 0.5);  // Angle to ...

          fl_color( ANIM_DRAW_COL_PLOT_AXIS_MINOR);
          fl_line( MaxX + 1, y - y2, xLast, y - y2);
          fl_line( MaxX + 1, y + y2, xLast, y + y2);

          fl_color( ANIM_DRAW_COL_PLOT_AXIS_MAIN);
          fl_draw( "+135�", MaxX + 3, y - y2 - 2);
          fl_draw( "-135�", MaxX + 3, y + y2 - 2);
        }

        break;

      case IQE_ANIM_EDIT_POSE_PART_SCALE:     // Pose part to edit: scale

        // 1.0 line

        y2 = (int)( (1.0 * PlotSizeY2 * 2 / ANIM_DRAW_PLOT_SCALE_RANGE) + 0.5); // Scale to ...
        y2 = y + PlotSizeY2 - y2;
        fl_color( ANIM_DRAW_COL_PLOT_AXIS_MAIN);
        fl_line( MaxX + 1, y2, xLast, y2);
        fl_draw( "1.0", MaxX + 3, y2 - 2);

        // 0.5 line line

        y2 = (int)( (0.5 * PlotSizeY2 * 2 / ANIM_DRAW_PLOT_SCALE_RANGE) + 0.5); // Scale to ...
        y2 = y + PlotSizeY2 - y2;
        fl_color( ANIM_DRAW_COL_PLOT_AXIS_MINOR);
        fl_line( MaxX + 1, y2, xLast, y2);
        fl_color( ANIM_DRAW_COL_PLOT_AXIS_MAIN);
        fl_draw( "0.5", MaxX + 3, y2 - 2);

        // 1.5 line line

        if( ANIM_DRAW_PLOT_SCALE_RANGE <= 2.5) {

          y2 = (int)( (1.5 * PlotSizeY2 * 2 / ANIM_DRAW_PLOT_SCALE_RANGE) + 0.5); // Scale to ...
          y2 = y + PlotSizeY2 - y2;
          fl_color( ANIM_DRAW_COL_PLOT_AXIS_MINOR);
          fl_line( MaxX + 1, y2, xLast, y2);
          fl_color( ANIM_DRAW_COL_PLOT_AXIS_MAIN);
          fl_draw( "1.5", MaxX + 3, y2 - 2);
        }

        for( int i = 2; i < ANIM_DRAW_PLOT_SCALE_RANGE - 0.25; i++) {

          y2 = (int)( (i * PlotSizeY2 * 2 / ANIM_DRAW_PLOT_SCALE_RANGE) + 0.5); // Scale to ...
          y2 = y + PlotSizeY2 - y2;
          fl_color( ANIM_DRAW_COL_PLOT_AXIS_MINOR);
          fl_line( MaxX + 1, y2, xLast, y2);
          sprintf( TempString, "%.1f", (float)i);
          fl_color( ANIM_DRAW_COL_PLOT_AXIS_MAIN);
          fl_draw( TempString, MaxX + 3, y2 - 2);
        }
        break;
      }

      // Name of this plot view

      fl_color( FL_BLACK);

      y2 = y - PlotSizeY2 + (ANIM_DRAW_FONT_SIZE * 5 / 4);

      fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE * 5 / 4);
      if( pDrawModelCurAnim->len < 5) {

        fl_draw( AxisNames[ iAxis], MaxX + 1, y2);
        fl_draw( AxisNames[ iAxis], MaxX + 1, y2 + 1);
      } else {

        fl_draw( AxisNames[ iAxis], MaxX + 1 + ANIM_DRAW_TIME_STEP_XX * 3, y2);
        fl_draw( AxisNames[ iAxis], MaxX + 1 + ANIM_DRAW_TIME_STEP_XX * 3, y2 + 1);
      }
      fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore font

      // ..

      y += PlotSizeY2;
    }

    fl_font( ANIM_DRAW_FONT_NR, ANIM_DRAW_FONT_SIZE);  // Restore font
  }

  // Draw Seconds

  int iTick, TickMarksPerSecond;

  if( pDrawModelCurAnim->framerate < 4.0) {

    TickMarksPerSecond = 1;

  } else if( pDrawModelCurAnim->framerate < 8.0) {

    TickMarksPerSecond = 5;

  } else {

    TickMarksPerSecond = 10;
  }

  for( iTick = 1; ; iTick += 1) {

    x = MaxX + (int)(iTick * ANIM_DRAW_TIME_STEP_XX * pDrawModelCurAnim->framerate / TickMarksPerSecond + 0.5);

    if( x - ANIM_DRAW_TIME_STEP_XX > pGUI_Scroll->x() + pGUI_Scroll->w()) {  // Would be out of screen

      break;
    }

    if( x >= 0) {

      if( (int)(iTick * pDrawModelCurAnim->framerate / TickMarksPerSecond + 0.5) > pDrawModelCurAnim->len) { // Afteer last frame

        break;
      }

      if( (iTick % TickMarksPerSecond) == 0) {

        int dx, dy, wo, ho;

        fl_color( fl_rgb_color( ANIM_DRAW_COL_SECONDS));
        fl_line( x, AoiY, x, yBase + ANIM_DRAW_HEADER_YY);

        if( TickMarksPerSecond <= 1) {
          sprintf( TempString, "%d", iTick / TickMarksPerSecond);
        } else {
          sprintf( TempString, "%d sec", iTick / TickMarksPerSecond);
        }
        fl_text_extents( TempString, dx, dy, wo, ho);   // measure extents
        fl_draw( TempString, x - wo - 2, yBase + ANIM_DRAW_FONT_SIZE - ANIM_DRAW_IDENT_Y);    // and draw string
      } else {

        fl_color( fl_rgb_color( ANIM_DRAW_COL_SECONDS_TICKS));

        if( TickMarksPerSecond < 10 || (iTick % TickMarksPerSecond) == TickMarksPerSecond / 2) {
          fl_line( x, AoiY, x, yBase + ANIM_DRAW_HEADER_YY / 2);
        } else {

          if( TickMarksPerSecond >= 10) {
            fl_line( x, AoiY, x, yBase + ANIM_DRAW_HEADER_YY / 4);
          } else {
            fl_line( x, AoiY, x, yBase + ANIM_DRAW_HEADER_YY / 3);
          }
        }
      }
    }
  }

  if( iTick > 0 && iTick <= TickMarksPerSecond) {  // No second output

    iTick -= 1;

    x = MaxX + (int)(iTick * ANIM_DRAW_TIME_STEP_XX * pDrawModelCurAnim->framerate / TickMarksPerSecond + 0.5);

    fl_color( fl_rgb_color(   0,  32, 255));
    fl_line( x, AoiY, x, yBase + ANIM_DRAW_HEADER_YY);

    sprintf( TempString, "%.1f sec", (float)iTick / (float)TickMarksPerSecond);

    fl_draw( TempString, x + 4, yBase + ANIM_DRAW_FONT_SIZE - ANIM_DRAW_IDENT_Y);    // and draw string
  }

  // Draw frame separator lines and keyflag markers

  int LastPoseY2[ 3];       // Remember Y Positions for last pose
  int LastPoseSet;          // TRUE if last pose is set
  int LastKeyFlags;         // Keyflags of last pose

  LastPoseSet  = false;     // No last pose set
  LastKeyFlags = 0;

  // ...

  iFrameNr = 0;
  for( x = MaxX + ANIM_DRAW_TIME_STEP_XX; iFrameNr < pDrawModelCurAnim->len && x < AoiX + AoiXX; x += ANIM_DRAW_TIME_STEP_XX, iFrameNr += 1) {

    if( x - ANIM_DRAW_TIME_STEP_XX > pGUI_Scroll->x() + pGUI_Scroll->w()) {  // Would be out of screen

      break;
    }

    if( x >= 0) {

      struct pose *pPose;
      int iPose, LineStyleColSet, x2;

      // Frame nr.

      if( iFrameNr % 5 == 4 || iFrameNr == 0) {

        fl_color( FL_BLACK);
        sprintf( TempString, "%d", iFrameNr + 1);
        fl_draw( TempString, x - ANIM_DRAW_TIME_STEP_XX + 2, yBase + ANIM_DRAW_HEADER_YY);
      }

      // Color for frame separator lines

      if( iFrameNr % 5 == 4 || iFrameNr == pDrawModelCurAnim->len - 1) {

        fl_color( FL_BLACK);
      } else {

        fl_color( fl_rgb_color( ANIM_DRAW_COL_JOINT_NAME_SEP_LINES));
      }

      if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_TIMELINE) {

        //
        // Layout for mode timeline
        //

        // frame separator lines

        fl_line( x, yBase + ANIM_DRAW_HEADER_YY + 2, x, yLast - 1);

        // keyflag markers

        pPose = pDrawModelCurAnim->data[ iFrameNr];  // This frame

        y = yBase + ANIM_DRAW_HEADER_YY + 1 + ANIM_DRAW_TIME_STEP_XX / 2;

        LineStyleColSet = false;
        x2 = x - ANIM_DRAW_TIME_STEP_XX / 2;

        for( iPose = 0; iPose < pDrawModel->skel->joint_count; iPose++, pPose++) {

          if( pPose->KeyFlags != 0) {  // Any keyflag bit set

            if( ! LineStyleColSet) {

              LineStyleColSet = true;
              fl_color( FL_RED);
              fl_line_style( FL_CAP_ROUND, ANIM_DRAW_TIME_STEP_XX - 4);  // line with
            }

            fl_line( x2, y, x2, y);
          }

          y += ANIM_DRAW_FONT_SIZE + 3;
        }

      } else {

        //
        // Layout for mode plot
        //

        // frame separator lines

        y = yBase + ANIM_DRAW_HEADER_YY + 1;

        for( iAxis = PlotAxisBegin; iAxis <= PlotAxisEnd; iAxis++) {

          y += ANIM_DRAW_PLOT_DIST_Y + PlotSizeY2;

          fl_line( x, y - PlotSizeY2 + 1, x, y + PlotSizeY2 - 1);

          y += PlotSizeY2;
        }

        // Plot of this animation

        LineStyleColSet = false;

        if( IqeB_DispStyle_JointSelected >= 0 &&           // Security test, in range
            IqeB_DispStyle_JointSelected < pDrawModel->skel->joint_count) {

          x2 = x - ANIM_DRAW_TIME_STEP_XX / 2;

          pPose = pDrawModelCurAnim->data[ iFrameNr];  // This frame
          pPose += IqeB_DispStyle_JointSelected;       // This joint

          y = yBase + ANIM_DRAW_HEADER_YY + 1;

          for( iAxis = PlotAxisBegin; iAxis <= PlotAxisEnd; iAxis++) {

            float yFloat;
            int y2This, y2Last, KeyFlagsTest;

            y += ANIM_DRAW_PLOT_DIST_Y + PlotSizeY2;

            // Calculate coordinate for plot point

            KeyFlagsTest = 0; // To get no compile warning: 'KeyFlagsTest' may be used uninitialized in this function
            y2This = 0;       // -"-

            switch( IqeB_ToolsAnimEdit_PosePart) {
            case IQE_ANIM_EDIT_POSE_PART_LOCATION:  // Pose part to edit: location

              KeyFlagsTest = KEYFLAG_LOC_0;

              yFloat = -1.0 * (pPose->location[ iAxis] * PlotSizeY2) / ObjSizeMax;  // Location to ...
              if( yFloat >= 0.0) {
                y2This = (int)(yFloat + 0.5);  // relative to center line
              } else {
                y2This = (int)(yFloat - 0.5);
              }

              y2This += y;  // relative to center line

              break;

            case IQE_ANIM_EDIT_POSE_PART_ANGLES:    // Pose part to edit: angles

              KeyFlagsTest = KEYFLAG_ANG_0;

              yFloat = -1.0 * (pPose->angles[ iAxis] * PlotSizeY2) / PlotAngleRange;  // Angle to ...
              if( yFloat >= 0.0) {
                y2This = (int)(yFloat + 0.5);  // relative to center line
              } else {
                y2This = (int)(yFloat - 0.5);
              }

              y2This += y;  // relative to center line

              break;

            case IQE_ANIM_EDIT_POSE_PART_SCALE:     // Pose part to edit: scale

              KeyFlagsTest = KEYFLAG_SCALE_0;

              y2This = (int)( (pPose->scale[ iAxis] * PlotSizeY2 * 2) / ANIM_DRAW_PLOT_SCALE_RANGE + 0.5);  // Scale to ...

              if( y2This < 0) y2This = 0;  // Clipping
              if( y2This >  PlotSizeY2 * 2) y2This =  PlotSizeY2 * 2;  // Clipping

              y2This = y + PlotSizeY2 - y2This;  // relative to center line

              break;
            }  // end switch( IqeB_ToolsAnimEdit_PosePart)

            if( LastPoseSet) {          // Last pose was set

              y2Last = LastPoseY2[ iAxis];

              // Draw line from last pos to here

              if( LineStyleColSet != 3) {

                LineStyleColSet = 3;
                fl_color( ANIM_DRAW_COL_PLOT_LINE);
                fl_line_style( FL_SOLID, 3);    // line with
              }

              fl_line( x2 - ANIM_DRAW_TIME_STEP_XX, y2Last, x2, y2This);

              // Is this a selected point

              if( MouseOverAxisNr == iAxis &&
                  ((MouseOverFrameNr >= 0 && MouseOverFrameNr == iFrameNr - 1) ||
                   (MouseOverFrameNr < 0 && DrawModelCurframe >= 0 && DrawModelCurframe == iFrameNr - 1))) {

                // Draw selection point

                if( LineStyleColSet != 0) {

                  LineStyleColSet = 0;
                  fl_color( FL_BLACK);
                  fl_line_style( 0);  // line with
                }

                fl_circle( x2 - ANIM_DRAW_TIME_STEP_XX, y2Last, ANIM_DRAW_TIME_STEP_XX / 2 + 3);
              }

              // Draw point on last

              if( (LastKeyFlags & (KeyFlagsTest << iAxis)) != 0) {  // Keyflag bit set

                if( LineStyleColSet != 1) {

                  LineStyleColSet = 1;
                  fl_color( ANIM_DRAW_COL_JOINT_KEY);
                  fl_line_style( FL_CAP_ROUND, ANIM_DRAW_TIME_STEP_XX - 4);  // line with
                }
              } else {

                if( LineStyleColSet != 2) {

                  LineStyleColSet = 2;
                  fl_color( ANIM_DRAW_COL_JOINT_NO_KEY);
                  fl_line_style( FL_CAP_ROUND, ANIM_DRAW_TIME_STEP_XX - 6);  // line with
                }
              }

              fl_line( x2 - ANIM_DRAW_TIME_STEP_XX, y2Last, x2 - ANIM_DRAW_TIME_STEP_XX, y2Last);
            }

            if( iFrameNr >= pDrawModelCurAnim->len - 1) {  // Was last column

              // Is this a selected point

              if( MouseOverAxisNr == iAxis &&
                 ((MouseOverFrameNr >= 0 && MouseOverFrameNr == iFrameNr) ||
                  (MouseOverFrameNr < 0 && DrawModelCurframe >= 0 && DrawModelCurframe == iFrameNr))) {

                // Draw selection point

                if( LineStyleColSet != 0) {

                  LineStyleColSet = 0;
                  fl_color( FL_BLACK);
                  fl_line_style( 0);  // line with
                }

                fl_circle( x2, y2This, ANIM_DRAW_TIME_STEP_XX / 2 + 3);
              }

              // Draw point on this

              if( (pPose->KeyFlags & (KeyFlagsTest << iAxis)) != 0) {  // Keyflag bit set

                if( LineStyleColSet != 1) {

                  LineStyleColSet = 1;
                  fl_color( ANIM_DRAW_COL_JOINT_KEY);
                  fl_line_style( FL_CAP_ROUND, ANIM_DRAW_TIME_STEP_XX - 4);  // line with
                }
              } else {

                if( LineStyleColSet != 2) {

                  LineStyleColSet = 2;
                  fl_color( ANIM_DRAW_COL_JOINT_NO_KEY);
                  fl_line_style( FL_CAP_ROUND, ANIM_DRAW_TIME_STEP_XX - 6);  // line with
                }
              }

              fl_line( x2, y2This, x2, y2This);

            }

            LastPoseY2[ iAxis] = y2This;

            // ...

            y += PlotSizeY2;
          }

          LastKeyFlags = pPose->KeyFlags;
          LastPoseSet  = true;  // Have set last pose
        }
      }

      if( LineStyleColSet) {  // Must reset linestyle

        fl_line_style( 0);
      }
    }
  }

  // ...

ExitPoint:

  fl_font( CurrFont, CurrFontSize);  // Restore default font

}

/************************************************************************************
 * AnimEditKeyFlagsUpdateThings
 *
 * Do things necessary after chaning somethint on
 * the current animations (a pose value) or a keyflag
 * bit.
 *
 * * Recalcs the animations (Interpolate poses between set keyflags)
 * * Update the current animation/current frame for display
 * * Updates GUI for animation changes.
 */

static void AnimEditUpdateAterCurAnimChange()
{

  // Recalc all poses of this animation
  IqeB_AnimKeyFlagsRecalcAnimation( pDrawModelCurAnim, pDrawModel->skel->joint_count);

  // And update for display
  IqeB_DispAnimateModel( pDrawModel, pDrawModelCurAnim, DrawModelCurframe);

  // Data of selected joint has changed
  IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_ANIMATION); // Also update the GUI
}

/************************************************************************************
 * Copy Paste animation frame things.
 *
 */

static struct anim *pClipBoardAnim = NULL;  // The clipboard animation
static int ClipBoardAnimPoses = 0;          // # poses of clipboard animation

// Clear animation clipboard to empty
static void AnimEditClipBoardClear()
{
  struct anim *pAnim;
  int iFrame;

  if( pClipBoardAnim == NULL) {   // Is already empty ?

    return;                       // Nothing to do
  }

  pAnim = pClipBoardAnim;         // Get local pointer

  free( pAnim->name);

  if( pAnim->data) {

    for( iFrame = 0; iFrame < pAnim->len; iFrame++) {

      free( pAnim->data[ iFrame]);
    }

    free( pAnim->data);
  }

  free( pAnim);

  pClipBoardAnim     = NULL;       // Reset global pointer to cleared
  ClipBoardAnimPoses = 0;          // Reset # poses of clipboard animation
}

// Test animation clipboard to have valid contents.
// Return TRUE if clipboard has valid data
static int AnimEditClipBoardTest()
{
  if( pClipBoardAnim == NULL) {    // Have no clipboard data

    return( false);                // No data
  }

  // Test for problems

  if( pDrawModel == NULL ||            // No model loaded
      pDrawModel->skel  == NULL ||
      pDrawModel->anim_count <= 0 ||   // No animations
      pDrawModel->anim_poses <= 0 ||   // No poses
      pDrawModelCurAnim == NULL ||     // No current animation
      ClipBoardAnimPoses != pDrawModel->skel->joint_count||  // Mismatch of joint count
      pClipBoardAnim->len == 0) {      // No frame in clipboard

    // Clear the clipboard

    AnimEditClipBoardClear();

    return( false);                // No data
  }

  return( true);   // Return OK
}

// Copy animation to clipboard
static void AnimEditClipBoardCopy( struct anim *pAnimSrc, int nPoses, int FrameIdx1, int FrameIdx2)
{
  int iFrame, Frame1, Frame2;
  struct pose *pPose;

  // Check / Prepare arguments

  if( FrameIdx1 < 0 && FrameIdx2 < 0) {          // Both are not valid

    return;                       // Nothing to do

  } else if( FrameIdx1 >= 0 && FrameIdx2 < 0) {  // Only first is valid

    Frame1 = FrameIdx1;
    Frame2 = FrameIdx1;

  } else if( FrameIdx1 < 0 && FrameIdx2 >= 0) {  // Only second is valid

    Frame1 = FrameIdx2;
    Frame2 = FrameIdx2;

  } else {                                      // Both are valid

    if( FrameIdx1 <= FrameIdx2) {               // Check order, keep ascending

      Frame1 = FrameIdx1;
      Frame2 = FrameIdx2;
    } else {

      Frame1 = FrameIdx2;
      Frame2 = FrameIdx1;
    }
  }

  // Clear the clipboard

  AnimEditClipBoardClear();

  // create new clipboard animation
  // NOTE: the 'NULL' in place of a model
  pClipBoardAnim = pushanim( NULL, (char *)"ClipBoard");

  if( pClipBoardAnim == NULL) {    // Security test

    return;
  }

  ClipBoardAnimPoses = nPoses;     // Set poses of clipboard animation

  // Add the poses to the clipbaord

  for( iFrame = Frame1; iFrame <= Frame2; iFrame++) {

    if( iFrame >= pAnimSrc->len) {     // Security test, out of range

      break;
    }

    // Add one frame
    pPose = pushframe( pClipBoardAnim, nPoses);

    if( pPose == NULL) {    // Security test

      return;
    }

    // Copy pose

    memcpy( pPose, pAnimSrc->data[ iFrame], sizeof( struct pose) * nPoses);
  }
}

// Paste animation from clipboard.
// DoApppend is true: The data is appended after the indexed frame.
// DoApppend is false: The data is inserted before the indexed frame.
static void AnimEditClipBoardPaste( struct anim *pAnimDst, int nPoses, int FrameIdx, int DoApppend)
{
  int iFrame, nFrames, iCopy;
  struct pose *pPose;

  if( ! AnimEditClipBoardTest()) {      // Test Clipboard for OK

    return;                             // Has a problem
  }

  // Test some other things

  if( FrameIdx >= pAnimDst->len) {      // Out of range

    return;                             // Has a problem
  }

  if( ClipBoardAnimPoses != nPoses) {   // Out of range

    return;                             // Has a problem
  }

  // append the frames at the end

  nFrames = pClipBoardAnim->len;        // Number of frames to insert

  if( DoApppend) {
    FrameIdx += 1;                      // Preincrement, so we append after it
  }

  if( FrameIdx < 0) {                   // Out of range

    return;                             // Has a problem
  }

  for( iFrame = 0; iFrame < nFrames; iFrame++) {

    // Add one frame
    pPose = pushframe( pAnimDst, nPoses);

    // Copy pose, is appended at the end

    memcpy( pPose, pClipBoardAnim->data[ iFrame], sizeof( struct pose) * nPoses);

    // Copy up animations after the insert point

    for( iCopy = pAnimDst->len - 1; iCopy > FrameIdx; iCopy--) {

      pAnimDst->data[ iCopy] = pAnimDst->data[ iCopy - 1];
    }

    pAnimDst->data[ FrameIdx] = pPose;

    FrameIdx += 1;
  }
}

// Delete frames
static void AnimEditDeleteFrames( struct anim *pAnimSrc, int nPoses, int FrameIdx1, int FrameIdx2)
{
  int iFrame, Frame1, Frame2, iCopy;

  // Check / Prepare arguments

  if( FrameIdx1 < 0 && FrameIdx2 < 0) {          // Both are not valid

    return;                       // Nothing to do

  } else if( FrameIdx1 >= 0 && FrameIdx2 < 0) {  // Only first is valid

    Frame1 = FrameIdx1;
    Frame2 = FrameIdx1;

  } else if( FrameIdx1 < 0 && FrameIdx2 >= 0) {  // Only second is valid

    Frame1 = FrameIdx2;
    Frame2 = FrameIdx2;

  } else {                                      // Both are valid

    if( FrameIdx1 <= FrameIdx2) {               // Check order, keep ascending

      Frame1 = FrameIdx1;
      Frame2 = FrameIdx2;
    } else {

      Frame1 = FrameIdx2;
      Frame2 = FrameIdx1;
    }
  }

  // Delete poses from the animation

  for( iFrame = Frame1; iFrame <= Frame2; iFrame++) {

    if( pAnimSrc->len <= 1) {          // Must keep one frame

      break;
    }

    if( Frame1 >= pAnimSrc->len) {     // Security test, out of range

      break;
    }

    free( pAnimSrc->data[ Frame1]);    // free this data
    pAnimSrc->data[ Frame1] = NULL;    // Zero pointer (is overwritten aferwards)

    // copy down the remainders

    for( iCopy = Frame1; iCopy < pAnimSrc->len - 1; iCopy++) {

      pAnimSrc->data[ iCopy] = pAnimSrc->data[ iCopy + 1];
    }

    // Delete the pointer to the last entry (just copied down)
    pAnimSrc->data[ pAnimSrc->len - 1] = NULL;    // Zero pointer

    pAnimSrc->len -= 1;  // Have one less
  }
}

// Delete frames
static void AnimEditClipBoardReplaceFrames( struct anim *pAnimSrc, int nPoses, int FrameIdx1, int FrameIdx2)
{
  int iFrame, Frame1, Frame2, iCopy;

  // Tests

  if( ! AnimEditClipBoardTest()) {      // Test Clipboard for OK

    return;                             // Has a problem
  }

  if( ClipBoardAnimPoses != nPoses) {   // Out of range

    return;                             // Has a problem
  }

  // Check / Prepare arguments

  if( FrameIdx1 >= pAnimSrc->len) {
    FrameIdx1 = pAnimSrc->len - 1;
  }
  if( FrameIdx2 >= pAnimSrc->len) {
    FrameIdx2 = pAnimSrc->len - 1;
  }

  if( FrameIdx1 < 0 && FrameIdx2 < 0) {          // Both are not valid

    return;                       // Nothing to do

  } else if( FrameIdx1 >= 0 && FrameIdx2 < 0) {  // Only first is valid

    Frame1 = FrameIdx1;
    Frame2 = FrameIdx1;

  } else if( FrameIdx1 < 0 && FrameIdx2 >= 0) {  // Only second is valid

    Frame1 = FrameIdx2;
    Frame2 = FrameIdx2;

  } else {                                      // Both are valid

    if( FrameIdx1 <= FrameIdx2) {               // Check order, keep ascending

      Frame1 = FrameIdx1;
      Frame2 = FrameIdx2;
    } else {

      Frame1 = FrameIdx2;
      Frame2 = FrameIdx1;
    }
  }

  // Special case, replace one line with on line from the clippoard

  if( Frame1 == Frame2 && pClipBoardAnim->len == 1) {

    // Simply replace

    memcpy( pAnimSrc->data[ Frame1], pClipBoardAnim->data[ 0], sizeof( struct pose) * nPoses);

    return;
  }

  // Delete poses from the animation

  for( iFrame = Frame1; iFrame <= Frame2; iFrame++) {

    if( pAnimSrc->len <= 0) {          // All deleted

      break;
    }

    if( Frame1 >= pAnimSrc->len) {     // Security test, out of range

      break;
    }

    free( pAnimSrc->data[ Frame1]);    // free this data
    pAnimSrc->data[ Frame1] = NULL;    // Zero pointer (is overwritten aferwards)

    // copy down the remainders

    for( iCopy = Frame1; iCopy < pAnimSrc->len - 1; iCopy++) {

      pAnimSrc->data[ iCopy] = pAnimSrc->data[ iCopy + 1];
    }

    // Delete the pointer to the last entry (just copied down)
    pAnimSrc->data[ pAnimSrc->len - 1] = NULL;    // Zero pointer

    pAnimSrc->len -= 1;  // Have one less
  }

  // Append from the clippboard

  AnimEditClipBoardPaste( pAnimSrc, nPoses, Frame1 - 1, true);
}

/************************************************************************************
 * AnimEditPopupMenuOrShortcut
 *
 * Process a shortcut key or bring up a popup menu.
 *
 * Valid arguments have a value of >= 0.
 *
 */

#define POPUA_MASK          0x00ff    // Popup action mask

// Use this to pop up a context dialog
#define POPUA_DO_POPUP      0x0000    // Popup action: pop up a context dialog

// Keyflag manipulation
#define POPUA_POSE_BIT      0x0001    // Popup action: Pose bit
#define POPUA_POSE_ALL      0x0002    // Popup action: Pose all bits
#define POPUA_COLUMN        0x0003    // Popup action: Frame column
#define POPUA_COL_MARK      0x0004    // Popup action: Marked frame columns
#define POPUA_ROW           0x0005    // Popup action: Joint row
#define POPUA_ALL_KEYS      0x0006    // Popup action: All poses

// Others
#define POPUA_FRAME_COPY    0x0010    // Popup action: Copy frame(s) to clipboard
#define POPUA_FRAME_REPLACE 0x0011    // Popup action: Replace frame(s) from clipboard
#define POPUA_FRAME_INSERT  0x0012    // Popup action: Insert frame(s) from clipboard
#define POPUA_FRAME_APPEND  0x0013    // Popup action: Append frame(s) from clipboard
#define POPUA_FRAME_DEL     0x0014    // Popup action: Delete frame(s)
#define POPUA_FRAME_MARK    0x0015    // Popup action: Mark this frame
#define POPUA_FRAME_SKEL    0x0016    // Popup action: Copy skeleton to this frame
#define POPUA_FRAME_INAC    0x0017    // Popup action: For submenu, set FL_MENU_INACTIVE in no column option

#define POPUA_KEYS_AUTO     0x0020    // Popup action: Auto-Set keyflag bits

// Options for key manipulation
#define POPUK_CLEAR        0x0100    // Key clear
#define POPUK_SET          0x0200    // Key set

//----------------------------------------------------------

// Plot graph, selected graph point
static Fl_Menu_Item menu_Popup[] = {
  { "&Clear key", 0, 0, 0, FL_SUBMENU},
    { "Key-bit for &this pose",       0, NULL, (void*)(POPUA_POSE_BIT | POPUK_CLEAR) },
    { "Keys for this &pose",          0, NULL, (void*)(POPUA_POSE_ALL | POPUK_CLEAR) },
    { "Keys in this &column (frame)", 0, NULL, (void*)(POPUA_COLUMN | POPUK_CLEAR) },
    { "Keys for &marked columns (frames)", 0, NULL, (void*)(POPUA_COL_MARK | POPUK_CLEAR) },
    { "Keys in this &row (joint)",    0, NULL, (void*)(POPUA_ROW | POPUK_CLEAR) },
    { "&All keys",                    0, NULL, (void*)(POPUA_ALL_KEYS | POPUK_CLEAR) },
    { 0 },
  { "&Set key", 0, 0, 0, FL_SUBMENU | FL_MENU_DIVIDER},
    { "Key-bit for &this pose",       0, NULL, (void*)(POPUA_POSE_BIT | POPUK_SET) },
    { "Keys for this &pose",          0, NULL, (void*)(POPUA_POSE_ALL | POPUK_SET) },
    { "Keys in this &column (frame)", 0, NULL, (void*)(POPUA_COLUMN | POPUK_SET) },
    { "Keys for &marked columns (frames)", 0, NULL, (void*)(POPUA_COL_MARK | POPUK_SET) },
    { "Keys in this &row (joint)",    0, NULL, (void*)(POPUA_ROW | POPUK_SET) },
    { "&All keys",                    0, NULL, (void*)(POPUA_ALL_KEYS | POPUK_SET) },
    { 0 },
  { "&Frame", 0, 0, (void*)(POPUA_FRAME_INAC), FL_SUBMENU | FL_MENU_DIVIDER},
    { "&Copy frame(s) to clipboard",      0, NULL, (void*)(POPUA_FRAME_COPY) },
    { "&Replace frame(s) from clipboard", 0, NULL, (void*)(POPUA_FRAME_REPLACE) },
    { "&Insert frame(s) from clipboard",  0, NULL, (void*)(POPUA_FRAME_INSERT) },
    { "&Append frame(s) from clipboard",  0, NULL, (void*)(POPUA_FRAME_APPEND) },
    { "&Delete frame(s)",                 0, NULL, (void*)(POPUA_FRAME_DEL), FL_MENU_DIVIDER},
    { "&Mark this frame",                 0, NULL, (void*)(POPUA_FRAME_MARK), FL_MENU_DIVIDER},
    { "&Skeleton copy to this frame",     0, NULL, (void*)(POPUA_FRAME_SKEL) },
    { 0 },
  { "&Auto set keys",                 0, NULL, (void*)(POPUA_KEYS_AUTO) },
  { 0 },
{0}};

//----------------------------------------------------------

static const Fl_Menu_Item *PopupMenuThis( int UseRow, int UseColumn, int UseColMarked, int UseAxis)
{
  Fl_Menu_Item *pMenu, *pItem;
  int i, SizeMenu;
  int ClipBoardValid;

  // Working with this menu

  pMenu = menu_Popup;

  // Get reasons to modify the menu

  ClipBoardValid = AnimEditClipBoardTest();    // Test for valid clipboard

  // Walk the menu to make enables/disables

  pItem    = pMenu->first();
  SizeMenu = pMenu->size();

  for( i = 0; i < SizeMenu; i++, pItem++) {

    switch( (int)pItem->user_data_ & POPUA_MASK) {
    case POPUA_POSE_BIT:
    case POPUA_POSE_ALL:

      if( UseAxis) pItem->flags &= ~FL_MENU_INVISIBLE;
      else         pItem->flags |= FL_MENU_INVISIBLE;

      break;

    case POPUA_COLUMN:
    case POPUA_FRAME_INAC:

      if( UseColumn) pItem->flags &= ~FL_MENU_INVISIBLE;
      else           pItem->flags |= FL_MENU_INVISIBLE;

      break;

    case POPUA_COL_MARK:

      if( UseColumn && UseColMarked) pItem->flags &= ~FL_MENU_INVISIBLE;
      else                           pItem->flags |= FL_MENU_INVISIBLE;

      break;

    case POPUA_ROW:

      if( UseRow) pItem->flags &= ~FL_MENU_INVISIBLE;
      else           pItem->flags |= FL_MENU_INVISIBLE;

      break;

    case POPUA_FRAME_REPLACE:
    case POPUA_FRAME_INSERT:
    case POPUA_FRAME_APPEND:

      if( ClipBoardValid) pItem->flags &= ~FL_MENU_INACTIVE;
      else                pItem->flags |= FL_MENU_INACTIVE;

      break;

    case POPUA_FRAME_DEL:

      // Delete keeps one animation always, so disable if have only one
      if( pDrawModelCurAnim->len > 1) pItem->flags &= ~FL_MENU_INACTIVE;
      else                            pItem->flags |= FL_MENU_INACTIVE;

      break;

    }
  }

  // Popup the menu

  return( pMenu->popup( Fl::event_x(), Fl::event_y(), 0, 0, 0));
}

//----------------------------------------------------------

static void AnimEditPopupMenuOrShortcut( int Action, int ThisJointNr, int ThisFrameNr, int ThisAxisNr)
{
  const Fl_Menu_Item *m;
  int UseRow, UseColumn, UseColMarked, UseAxis;

  // Security tests

  if( pDrawModel == NULL ||            // No model loaded
      pDrawModel->skel  == NULL ||
      pDrawModel->anim_count <= 0 ||   // No animations
      pDrawModel->anim_poses <= 0 ||   // No poses
      pDrawModelCurAnim == NULL) {     // No current animation

    return;
  }

  //
  // What popup to use ?
  //

  m = NULL;                     // Preset, no popup

  UseRow = false;
  UseColumn = false;
  UseColMarked = false;
  UseAxis = false;

  // Plot graph, selected graph point
  // If ThisAxisNr is valid only for one of the plot view modes.
  // The others must be valid too.

  if( IqeB_ToolsAnimEdit_ViewMode > IQE_ANIM_EDIT_VIEW_MODE_TIMELINE) {  // one of the plot view modes

    ThisJointNr = IqeB_DispStyle_JointSelected;  // Must use the selected joint

    if( ThisAxisNr >= 0 && ThisAxisNr < 3 && ThisJointNr >= 0 && ThisFrameNr >= 0){

      UseColumn = true;
      UseAxis = true;

      goto ActionProcessing;
    }
  }

  // Click into the timeline table

  if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_TIMELINE && // timeline view mode
      ThisJointNr >= 0 && ThisFrameNr >= 0){

    UseRow = true;
    UseColumn = true;

    goto ActionProcessing;
  }

  // Frame column selected

  if( ThisFrameNr >= 0){

    UseColumn = true;

    goto ActionProcessing;
  }

  // Joint row selected

  if( ThisJointNr >= 0){

    UseRow = true;

    goto ActionProcessing;
  }

  // Background selected

  //
  // pop up is done, process the selected entry
  //

ActionProcessing:

  if( UseColumn && pDrawModelCurAnim->FrameMarked >= 0 &&  // Doing frames and have a marked frame
      ThisFrameNr != pDrawModelCurAnim->FrameMarked) {     // Selected frame is different from marked frame

    UseColMarked = true;   // Make sense to process marked frames
  }

  if( Action == POPUA_DO_POPUP) {   // Do the popup dialog

    m = PopupMenuThis( UseRow, UseColumn, UseColMarked, UseAxis);

    if( m ) {                       // User selected something
      Action = (int)m->user_data();
    }
  }

  if( Action != POPUA_DO_POPUP) { // Something to do

    int KeyFlagMask, KeyFlagValue;

    KeyFlagMask = KEYFLAGS_ALL;   // Preset, all keyflag bits

    if( Action & POPUK_CLEAR) {   // Clear keybits is set

      KeyFlagValue = 0;
    } else {                      // Set keybits is set

      KeyFlagValue = KEYFLAGS_ALL;
    }

    switch( Action & POPUA_MASK) {
    case POPUA_POSE_BIT:     // Popup action: Pose bit

      switch( IqeB_ToolsAnimEdit_PosePart) {
      case IQE_ANIM_EDIT_POSE_PART_LOCATION:  // Pose part to edit: location
        KeyFlagMask = KEYFLAG_LOC_0 << ThisAxisNr;  // Set this single
        break;
      case IQE_ANIM_EDIT_POSE_PART_ANGLES:    // Pose part to edit: angles
        KeyFlagMask = KEYFLAG_ANG_0 << ThisAxisNr;  // Set this single
        break;
      case IQE_ANIM_EDIT_POSE_PART_SCALE:     // Pose part to edit: scale
        KeyFlagMask = KEYFLAG_SCALE_0 << ThisAxisNr;  // Set this single
        break;
      }  // end switch( IqeB_ToolsAnimEdit_PosePart)

      IqeB_AnimKeyFlagsChangeAnimation( pDrawModelCurAnim, pDrawModel->skel->joint_count, ThisFrameNr, ThisJointNr, KeyFlagMask, KeyFlagValue);

      AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
      break;

    case POPUA_POSE_ALL:     // Popup action: Pose all bits

      IqeB_AnimKeyFlagsChangeAnimation( pDrawModelCurAnim, pDrawModel->skel->joint_count, ThisFrameNr, ThisJointNr, KeyFlagMask, KeyFlagValue);

      AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
      break;

    case POPUA_COLUMN:       // Popup action: Frame column

      ThisJointNr = -1;      // Motify all poses

      IqeB_AnimKeyFlagsChangeAnimation( pDrawModelCurAnim, pDrawModel->skel->joint_count, ThisFrameNr, ThisJointNr, KeyFlagMask, KeyFlagValue);

      AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
      break;

    case POPUA_COL_MARK:       // Popup action: Marked frame columns

      ThisJointNr = -1;      // Motify all poses

      {
        int iFrame, Frame1, Frame2;

        if( pDrawModelCurAnim->FrameMarked >= pDrawModelCurAnim->len) { // Security test

          pDrawModelCurAnim->FrameMarked = pDrawModelCurAnim->len - 1;
        }

        if( pDrawModelCurAnim->FrameMarked < 0) {  // Not selected

          Frame1 = ThisFrameNr;
          Frame2 = Frame1;

        } else {

          if( pDrawModelCurAnim->FrameMarked < ThisFrameNr) {

            Frame1 = pDrawModelCurAnim->FrameMarked;
            Frame2 = ThisFrameNr;
          } else {

            Frame1 = ThisFrameNr;
            Frame2 = pDrawModelCurAnim->FrameMarked;
          }
        }

        for( iFrame = Frame1; iFrame <= Frame2; iFrame++) {

          IqeB_AnimKeyFlagsChangeAnimation( pDrawModelCurAnim, pDrawModel->skel->joint_count, iFrame, ThisJointNr, KeyFlagMask, KeyFlagValue);
        }
      }

      AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
      break;

    case POPUA_ROW:          // Popup action: Joint row

      ThisFrameNr = -1;      // Motify all frames

      IqeB_AnimKeyFlagsChangeAnimation( pDrawModelCurAnim, pDrawModel->skel->joint_count, ThisFrameNr, ThisJointNr, KeyFlagMask, KeyFlagValue);

      AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
      break;

    case POPUA_ALL_KEYS:     // Popup action: Joint row

      ThisFrameNr = -1;      // Motify all frames
      ThisJointNr = -1;      // Motify all poses

      IqeB_AnimKeyFlagsChangeAnimation( pDrawModelCurAnim, pDrawModel->skel->joint_count, ThisFrameNr, ThisJointNr, KeyFlagMask, KeyFlagValue);

      AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
      break;

    case POPUA_KEYS_AUTO:    // Popup action: Auto-Set keyflag bits

      IqeB_AnimKeyFlagsPresetAnimation( pDrawModelCurAnim, pDrawModel->skel->joint_count);

      AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
      break;

    case POPUA_FRAME_COPY:   // Popup action: Copy frame(s) to clipboard

      if( ThisFrameNr >= 0)  { // Need this selected

        AnimEditClipBoardCopy( pDrawModelCurAnim, pDrawModel->skel->joint_count, ThisFrameNr, pDrawModelCurAnim->FrameMarked);

        // In any case, reset the marked frames
        pDrawModelCurAnim->FrameMarked = -1;          // Unmark it

        // Force a redraw
        AnimEditDrawForceRedraw = true;    // Force a redraw
      }
      break;

    case POPUA_FRAME_REPLACE: // Popup action: Replace frame(s) from clipboard
      if( ThisFrameNr >= 0)  { // Need this selected

        AnimEditClipBoardReplaceFrames( pDrawModelCurAnim, pDrawModel->skel->joint_count, ThisFrameNr, pDrawModelCurAnim->FrameMarked);

        // In any case, reset the marked frames
        pDrawModelCurAnim->FrameMarked = -1;          // Unmark it

        AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
      }
      break;

    case POPUA_FRAME_INSERT: // Popup action: Insert frame(s) from clipboard
    case POPUA_FRAME_APPEND: // Popup action: Append frame(s) from clipboard

      if( ThisFrameNr >= 0)  { // Need this selected

        AnimEditClipBoardPaste( pDrawModelCurAnim, pDrawModel->skel->joint_count, ThisFrameNr, (Action & POPUA_MASK) == POPUA_FRAME_APPEND);

        // In any case, reset the marked frames
        pDrawModelCurAnim->FrameMarked = -1;          // Unmark it

        AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
      }
      break;

    case POPUA_FRAME_DEL:    // Popup action: Delete frame(s)

      if( ThisFrameNr >= 0)  { // Need this selected

        AnimEditDeleteFrames( pDrawModelCurAnim, pDrawModel->skel->joint_count, ThisFrameNr, pDrawModelCurAnim->FrameMarked);

        // In any case, reset the marked frames
        pDrawModelCurAnim->FrameMarked = -1;          // Unmark it

        AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
      }
      break;

    case POPUA_FRAME_MARK:   // Popup action: Mark this frame

      if( ThisFrameNr >= 0)  { // Need this selected

        if( ThisFrameNr != pDrawModelCurAnim->FrameMarked) { // Not marked until now or other frame number

          pDrawModelCurAnim->FrameMarked = ThisFrameNr; // Mark this frame

        } else {                                        // This frame was marked before

          pDrawModelCurAnim->FrameMarked = -1;          // Unmark it
        }

        // Force a redraw
        AnimEditDrawForceRedraw = true;    // Force a redraw
      }

      break;

    case POPUA_FRAME_SKEL:    // Popup action: Copy skeleton to this frame

      if( ThisFrameNr >= 0 && ThisFrameNr < pDrawModelCurAnim->len) { // Need this selected

        int iPose;
        struct pose *pPose;

        // Overwrite pose with the skeleton

        memcpy( pDrawModelCurAnim->data[ ThisFrameNr], pDrawModel->skel->pose, sizeof( struct pose) * pDrawModel->skel->joint_count);

        // Set all keyflag bits

        pPose = pDrawModelCurAnim->data[ ThisFrameNr];
        for( iPose = 0; iPose < pDrawModel->skel->joint_count; iPose++) {

          pPose[ iPose].KeyFlags = KEYFLAGS_ALL;
        }

        // ...

        AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
      }
      break;

    }
  }

  if( pDrawModelCurAnim != NULL && DrawModelCurframe >= pDrawModelCurAnim->len) {  // To high

    DrawModelCurframe = pDrawModelCurAnim->len - 1;
  }

  return;
}

/************************************************************************************
 * AnimEditDrawingEventAction
 *
 * Mouse event inside the animation drawing area
 */

static void AnimEditDrawingEventAction( int event)
{
  int MInX, MInY, MPosX, MPosY, MDeltaX, MDeltaY;
  int AoiX, AoiY;
  int mouseleft, MouseButton, is_click;
  int MouseOverJointNrLast, MouseOverFrameNrLast, MouseOverAxisNrLast;
  int   iAxis, PlotAxisBegin, PlotAxisEnd, PlotSizeY2, JointLineTest_right_X;
  vec3 ObjSize, ObjCenter;
  float PlotAngleRange, ObjSizeMax;
  static int MPosXLast, MPosYLast;  // Mouse positin from last call

  MInX = Fl::event_x();             // Mouse coordinates in
  MInY = Fl::event_y();

  // Get aoi

  AoiX  = pAnimEditDrawing->x();   // Corner of drawing area
  AoiY  = pAnimEditDrawing->y();

  // Mouse position relative to drawing area

  MPosX = MInX - AoiX;
  MPosY = MInY - AoiY;

  // Delta mouse move to last call

  MDeltaX = MPosX - MPosXLast;
  MDeltaY = MPosY - MPosYLast;

#ifdef USE_DRAW_DEBUG_TEXT   // display a debug text

  // Some debug output
  sprintf( AnimEditDrawDebugStr, "Mouse event %2d, mPos %3d / %3d, AOI %2d / %2d  %3d*%3d", event, MPosX, MPosY, AoiX, AoiY, AoiXX, AoiYY);

  AnimEditDrawForceRedraw = true;    // Force a redraw
#endif

  // Reset some test things we decode here

  MouseOverJointNrLast = MouseOverJointNr;
  MouseOverFrameNrLast = MouseOverFrameNr;
  MouseOverAxisNrLast  = MouseOverAxisNr;

  MouseOverJointNr = -1;              // Reset mouse is over joint
  MouseOverFrameNr = -1;              // Reset mouse is over frame
  MouseOverAxisNr  = -1;              // Reset mouse is over graph view

  // Test for some reasons to leave

  if( event == FL_LEAVE || event == FL_ENTER || // Mouse enter or leave the view
      pDrawModel == NULL ||                     // Have no draw model
      pDrawModel->skel == NULL ||               // Have no skeleton
      pDrawModel->anim_count == 0 ||            // Have no animations
      ! DrawLayout_Valid) {                     // Draw layout must be valid

    goto ExitPoint;
  }

  // mouse buttons

	mouseleft   = Fl::event_state() & FL_BUTTON1;
	is_click    = Fl::event_is_click();  // Mouse is click
	MouseButton = Fl::event_button();    // mouse button from last event

  // Prepare some things which depents from view mode

  // Draw info for plot scale graph
  IqeB_DispModelGetInfo( ObjSize, ObjCenter);

  ObjSizeMax = ObjSize[ 0];
  if( ObjSizeMax < ObjSize[ 1]) ObjSizeMax = ObjSize[ 1];
  if( ObjSizeMax < ObjSize[ 2]) ObjSizeMax = ObjSize[ 2];

  if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_TIMELINE) {

    // Layout for mode timeline

    JointLineTest_right_X = DrawLayout_Frames_right_X;

    // Set some dummy values to get no compiler warnings
    PlotAxisBegin = 0;          // Begin with this axis
    PlotAxisEnd   = -1;         // Unvalidate
    PlotSizeY2     = ANIM_DRAW_PLOT3_SIZE_Y / 2;   // 1/2 height of graph in pixels
    PlotAngleRange = ANIM_DRAW_PLOT3_ANGLE_RANGE;  // set a dummy to value to get no compiler warning

  } else {

    // Layout for mode plot

    JointLineTest_right_X = DrawLayout_JNames_right_X;

    if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_PLOT_3) {  // Show thre axxis

      PlotAxisBegin = 0;          // Begin with this axis
      PlotAxisEnd   = 2;          // End with this axis
      PlotSizeY2     = ANIM_DRAW_PLOT3_SIZE_Y / 2;   // 1/2 height of graph in pixels
      PlotAngleRange = ANIM_DRAW_PLOT3_ANGLE_RANGE;  // Angle range in degrees (to both side)

    } else {   // show single axis

      PlotAxisBegin = IqeB_ToolsAnimEdit_ViewMode - IQE_ANIM_EDIT_VIEW_MODE_PLOT_X;        // Begin with this axis
      PlotAxisEnd   = PlotAxisBegin;  // End with this axis
      PlotSizeY2     = ANIM_DRAW_PLOT1_SIZE_Y / 2;   // 1/2 height of graph in pixels
      PlotAngleRange = ANIM_DRAW_PLOT1_ANGLE_RANGE;  // Angle range in degrees (to both side)
    }
  }

  //
  // Can anything select ?
  //

  // Test for movint an point on a plot grap axis

  if( IqeB_ToolsAnimEdit_ViewMode >= IQE_ANIM_EDIT_VIEW_MODE_PLOT_3 &&  // Any of the graph plot modes
       mouseleft && event != FL_PUSH &&                                 // Mouse button is down (but has gone down in the past)
       IqeB_DispStyle_JointSelected >= 0 &&                             // Proper value for selected joint
       IqeB_DispStyle_JointSelected < pDrawModel->skel->joint_count &&
       DrawModelCurframe >= 0 &&                                        // Proper value for selected joint
       DrawModelCurframe < pDrawModelCurAnim->len &&
       MouseOverAxisNrLast >= 0) {                                      // Got an axis from before

    MouseOverAxisNr = MouseOverAxisNrLast;  // Keep this axis nr. selected

  	if( MDeltaY != 0) {                           // Mouse has moved in Y

      struct pose *pPose;
      float yFloat;
      int y2;

      // Pose to modify

      iAxis = MouseOverAxisNr;

      pPose = pDrawModelCurAnim->data[ DrawModelCurframe];  // This frame
      pPose += IqeB_DispStyle_JointSelected;       // This joint

      switch( IqeB_ToolsAnimEdit_PosePart) {
      case IQE_ANIM_EDIT_POSE_PART_LOCATION:  // Pose part to edit: location

        yFloat = -1.0 * (pPose->location[ iAxis] * PlotSizeY2) / ObjSizeMax;  // Location to ...
        if( yFloat >= 0.0) {
          y2 = (int)(yFloat + 0.5);  // relative to center line
        } else {
          y2 = (int)(yFloat - 0.5);
        }

        // Calculate backwards to set location
        pPose->location[ iAxis] = (y2 + MDeltaY) * -1.0 * ObjSizeMax / PlotSizeY2;

        // In any case, we have a keyflag for this, so set it
        pPose->KeyFlags |= (KEYFLAG_LOC_0 << iAxis);

        break;

      case IQE_ANIM_EDIT_POSE_PART_ANGLES:    // Pose part to edit: angles

        // Get angle to height (relative to center line)

        yFloat = -1.0 * (pPose->angles[ iAxis] * PlotSizeY2) / PlotAngleRange;  // Angle to ...
        if( yFloat >= 0.0) {
          y2 = (int)(yFloat + 0.5);  // relative to center line
        } else {
          y2 = (int)(yFloat - 0.5);
        }

        // Calculate backwards to set angle
        pPose->angles[ iAxis] = (y2 + MDeltaY) * -1.0 * PlotAngleRange / PlotSizeY2;

        // In any case, we have a keyflag for this, so set it
        pPose->KeyFlags |= (KEYFLAG_ANG_0 << iAxis);

        // Must converte angle to quaterion
        IqeB_AnimAnglesToRotPose( pPose);

        break;

      case IQE_ANIM_EDIT_POSE_PART_SCALE:     // Pose part to edit: scale

        y2 = (int)( (pPose->scale[ iAxis] * PlotSizeY2 * 2) / ANIM_DRAW_PLOT_SCALE_RANGE + 0.5);  // Scale to ...

        if( y2 < 0) y2 = 0;  // Clipping
        //x/if( y2 >  PlotSizeY2 * 2) y2 =  PlotSizeY2 * 2;  // Clipping

        // Calculate backwards to set scale
        pPose->scale[ iAxis] = (y2 - MDeltaY) * ANIM_DRAW_PLOT_SCALE_RANGE / (PlotSizeY2 * 2);

        if( pPose->scale[ iAxis] < 0.001) pPose->scale[ iAxis] = 0.001; // Clipping

        // In any case, we have a keyflag for this, so set it
        pPose->KeyFlags |= (KEYFLAG_SCALE_0 << iAxis);

        if( IqeB_ToolsAnimEdit_PoseScaleAll) { // For pose scale change, change all scales

          pPose->scale[ OtherAxis1[ iAxis]] = pPose->scale[ iAxis];
          pPose->scale[ OtherAxis2[ iAxis]] = pPose->scale[ iAxis];

          pPose->KeyFlags |= KEYFLAG_SCALE_ALL;  // All scales are modified
        }

        break;
      }  // end switch( IqeB_ToolsAnimEdit_PosePart)

      AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
    }

    // Have done out work, can exit this function
    goto ExitPoint;
  }

	// Test for joint row
	if( MPosX >= DrawLayout_JNames_left_X && MPosX <= JointLineTest_right_X &&
      MPosY >= DrawLayout_JNames_top_Y  && MPosY <= DrawLayout_JNames_bottom_y) {

	  MouseOverJointNr = (MPosY - DrawLayout_JNames_top_Y) / (ANIM_DRAW_FONT_SIZE + 3);
    if( MouseOverJointNr >= pDrawModel->skel->joint_count) {  // security test
      MouseOverJointNr = pDrawModel->skel->joint_count - 1;
    }
    if( MouseOverJointNr < 0) {
      MouseOverJointNr = 0;
    }

	  // Left mouse button is down and mouse is over an not selected joint row
    if( mouseleft && event == FL_PUSH &&                   // Mouse button is down
        MouseOverJointNr != IqeB_DispStyle_JointSelected) {

      IqeB_DispStyle_JointSelected = MouseOverJointNr;  // Select this joint

      // Selected joint has changed
      IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_JOINT_SELECTED); // Also update the GUI
	  }
  }

	// Test for frame column
	if( pDrawModelCurAnim != NULL &&
      MPosX >= DrawLayout_Frames_left_X && MPosX <= DrawLayout_Frames_right_X &&
      MPosY >= DrawLayout_Frames_top_Y  && MPosY <= DrawLayout_Frames_bottom_y) {

    // Over what frame column

	  MouseOverFrameNr = (MPosX - DrawLayout_Frames_left_X) / ANIM_DRAW_TIME_STEP_XX;
    if( MouseOverFrameNr >= pDrawModelCurAnim->len) {  // security test
      MouseOverFrameNr = pDrawModelCurAnim->len - 1;
    }
    if( MouseOverFrameNr < 0) {
      MouseOverFrameNr = 0;
    }

    // Test for axis

    if( IqeB_ToolsAnimEdit_ViewMode >= IQE_ANIM_EDIT_VIEW_MODE_PLOT_3 &&  // Any of the graph plot modes
        IqeB_DispStyle_JointSelected >= 0 &&                              // Proper value for selected joint
        IqeB_DispStyle_JointSelected < pDrawModel->skel->joint_count) {

      struct pose *pPose;
      float yFloat;
      int y, y2;

      pPose = pDrawModelCurAnim->data[ MouseOverFrameNr];  // This frame
      pPose += IqeB_DispStyle_JointSelected;       // This joint

      y = DrawLayout_JNames_top_Y;

      for( iAxis = PlotAxisBegin; iAxis <= PlotAxisEnd; iAxis++) {

        y += ANIM_DRAW_PLOT_DIST_Y + PlotSizeY2;

        y2 = 0; // To get no compile warning: 'KeyFlagsTest' may be used uninitialized in this function

        // Calculate coordinate for plot point

        switch( IqeB_ToolsAnimEdit_PosePart) {
        case IQE_ANIM_EDIT_POSE_PART_LOCATION:  // Pose part to edit: location

          yFloat = -1.0 * (pPose->location[ iAxis] * PlotSizeY2) / ObjSizeMax;  // Location to ...
          if( yFloat >= 0.0) {
            y2 = (int)(yFloat + 0.5);  // relative to center line
          } else {
            y2 = (int)(yFloat - 0.5);
          }

          y2 += y;  // relative to center line

          break;

        case IQE_ANIM_EDIT_POSE_PART_ANGLES:    // Pose part to edit: angles

          yFloat = -1.0 * (pPose->angles[ iAxis] * PlotSizeY2) / PlotAngleRange;  // Angle to ...
          if( yFloat >= 0.0) {
            y2 = (int)(yFloat + 0.5);  // relative to center line
          } else {
            y2 = (int)(yFloat - 0.5);
          }

          y2 += y;  // relative to center line

          break;

        case IQE_ANIM_EDIT_POSE_PART_SCALE:     // Pose part to edit: scale

          y2 = (int)( (pPose->scale[ iAxis] * PlotSizeY2 * 2) / ANIM_DRAW_PLOT_SCALE_RANGE + 0.5);  // Scale to ...

          if( y2 < 0) y2 = 0;  // Clipping
          if( y2 >  PlotSizeY2 * 2) y2 =  PlotSizeY2 * 2;  // Clipping

          y2 = y + PlotSizeY2 - y2;  // relative to center line

          break;
        }  // end switch( IqeB_ToolsAnimEdit_PosePart)

        if( MPosY >= y2 - 12 && MPosY <= y2 + 12) {  // In the near

          MouseOverAxisNr = iAxis;
          break;                                   // Skip other axis
        }

        y += PlotSizeY2;
      }
    }

	  // Left mouse button is down and mouse is over an not selected frame column
    if( mouseleft && event == FL_PUSH) {              // Mouse button is down

      if(  MouseOverFrameNr != DrawModelCurframe) {

        // Selected frame has changed
        IqeB_AnimAction( IQE_ANIMATE_ACTION_FRAME_SET, MouseOverFrameNr); // Change to other frane
        IqeB_GUI_UpdateWidgets(); // Also update the GUI
      }

      // Enshure animation play is off

      if( IqeB_DispStyle_doplay) {   // Animation play is on

         IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doplay, 0); // Update variable and GUI
      }
	  }
  }

  // Test for right mouse button to bring up a popuplist

  if( event == FL_RELEASE  && ! mouseleft &&             // release a button and left not pressed
      is_click  && MouseButton == FL_RIGHT_MOUSE) {      // and right mouse button click

	  // Mouse over joint row --> select joint row
    if( MouseOverJointNr >= 0 &&                        // Mouse over joint row
        MouseOverJointNr != IqeB_DispStyle_JointSelected) {

      IqeB_DispStyle_JointSelected = MouseOverJointNr;  // Select this joint

      // Selected joint has changed
      IqeB_GUI_UpdateWidgets( IQE_GUI_UPDATE_JOINT_SELECTED); // Also update the GUI
	  }

	  // Mouse over frame column --> select frame column
    if( MouseOverFrameNr >= 0) {                         // Mouse over frame column

      if( MouseOverFrameNr != DrawModelCurframe) {

        // Selected frame has changed
        IqeB_AnimAction( IQE_ANIMATE_ACTION_FRAME_SET, MouseOverFrameNr); // Change to other frane
        IqeB_GUI_UpdateWidgets(); // Also update the GUI
      }

      // Enshure animation play is off

      if( IqeB_DispStyle_doplay) {   // Animation play is on

         IqeB_GUI_OpenGLUpdateGUISettingVariable( &IqeB_DispStyle_doplay, 0); // Update variable and GUI
      }
	  }

    // Popup menu

    AnimEditPopupMenuOrShortcut( POPUA_DO_POPUP, MouseOverJointNr, MouseOverFrameNr, MouseOverAxisNr);
  }

	//

ExitPoint:

  // Test something has changed
  if( MouseOverJointNrLast != MouseOverJointNr ||
      MouseOverFrameNrLast != MouseOverFrameNr ||
      MouseOverAxisNrLast  != MouseOverAxisNr) {

    // Force a redraw
    AnimEditDrawForceRedraw = true;    // Force a redraw
  }

  // Remember last mouse position

  MPosXLast = MPosX;
  MPosYLast = MPosY;

  // To keep compiler happy (not warning aboud 'set but not used' variable)
  if( MDeltaX == 0) {  // Use the variable
    MDeltaX = 0;
  }
}

/************************************************************************************
 * AnimEditShortcutKey
 *
 * Test for a shortcut key.
 *
 * return:  1 : processed the key
 *          0 : not processed
 */

static int AnimEditShortcutKey( int event_key, int event_state)
{
  struct pose *pPose;

  switch( event_key) {

  case 'a':       // ^A: Append frame(s) from clipboard

    // Popup action: Mark this frame

    if( event_state & FL_CTRL) {

      AnimEditPopupMenuOrShortcut( POPUA_FRAME_APPEND, MouseOverJointNr, MouseOverFrameNr, MouseOverAxisNr);

      return( 1);   // Processed it
    }
    break;

  case 'c':       // ^C: Copy frame(s) to clipboard

    // Popup action: Mark this frame

    if( event_state & FL_CTRL) {

      AnimEditPopupMenuOrShortcut( POPUA_FRAME_COPY, MouseOverJointNr, MouseOverFrameNr, MouseOverAxisNr);

      return( 1);   // Processed it
    }
    break;

  case 'd':       // ^D: Delete frame(s)

    // Popup action: Mark this frame

    if( event_state & FL_CTRL) {

      AnimEditPopupMenuOrShortcut( POPUA_FRAME_DEL, MouseOverJointNr, MouseOverFrameNr, MouseOverAxisNr);

      return( 1);   // Processed it
    }
    break;

  case 'i':       // ^I: Insert frame(s) from clipboard

    // Popup action: Mark this frame

    if( event_state & FL_CTRL) {

      AnimEditPopupMenuOrShortcut( POPUA_FRAME_INSERT, MouseOverJointNr, MouseOverFrameNr, MouseOverAxisNr);

      return( 1);   // Processed it
    }
    break;

  case 'k':       // K: Toggle keybit

    if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_TIMELINE) {

      // Layout for mode timeline

      if( pDrawModelCurAnim != NULL &&
          MouseOverJointNr >= 0 && MouseOverFrameNr >= 0) {

        pPose = pDrawModelCurAnim->data[ MouseOverFrameNr];  // This frame
        pPose += MouseOverJointNr;

        if( (pPose->KeyFlags & KEYFLAGS_ALL) == 0) {  // All cleared

          pPose->KeyFlags |= KEYFLAGS_ALL;            // Set all
        } else {                                      // Any set

          pPose->KeyFlags &= ~KEYFLAGS_ALL;           // Clear all
        }

        AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
      }

    } else {

      // Layout for mode plot

      if( pDrawModelCurAnim != NULL &&
          IqeB_DispStyle_JointSelected >= 0 && MouseOverFrameNr >= 0) {

        int KeyGroup, KeyTest;

        pPose = pDrawModelCurAnim->data[ MouseOverFrameNr];  // This frame
        pPose += IqeB_DispStyle_JointSelected;

        if( IqeB_ToolsAnimEdit_PosePart == IQE_ANIM_EDIT_POSE_PART_LOCATION) {

          KeyGroup = KEYFLAG_LOC_ALL;
          KeyTest  = KEYFLAG_LOC_0;

        } else if( IqeB_ToolsAnimEdit_PosePart == IQE_ANIM_EDIT_POSE_PART_ANGLES) {

          KeyGroup = KEYFLAG_ANG_ALL;
          KeyTest  = KEYFLAG_ANG_0;

        } else {   // Must be IQE_ANIM_EDIT_POSE_PART_SCALE

          KeyGroup = KEYFLAG_SCALE_ALL;
          KeyTest  = KEYFLAG_SCALE_0;
        }

        if( MouseOverAxisNr >= 0) {   // Have an axis selected

          KeyTest = KeyTest << MouseOverAxisNr;  // Shift single bit to positon
        } else {

          KeyTest = KeyGroup;  // Use the group
        }

        if( (pPose->KeyFlags & KeyTest) == 0) {  // All cleared

          pPose->KeyFlags |= KeyTest;            // Set all
        } else {                                      // Any set

          pPose->KeyFlags &= ~KeyTest;           // Clear all
        }

        AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
      }
    }

    return( 1);   // Processed it
    break;

  case 'm':       // ^M: Mark this frame

    // Popup action: Mark this frame

    if( event_state & FL_CTRL) {

      AnimEditPopupMenuOrShortcut( POPUA_FRAME_MARK, MouseOverJointNr, MouseOverFrameNr, MouseOverAxisNr);

      return( 1);   // Processed it
    }
    break;

  case 'r':       // ^R: Replace frame(s) from clipboard

    // Popup action: Replace frame(s) from clipboard

    if( event_state & FL_CTRL) {

      AnimEditPopupMenuOrShortcut( POPUA_FRAME_REPLACE, MouseOverJointNr, MouseOverFrameNr, MouseOverAxisNr);

      return( 1);   // Processed it
    }
    break;

  case 's':       // ^S: Skeleton copy to this frame

    // Popup action: Mark this frame

    if( event_state & FL_CTRL) {

      AnimEditPopupMenuOrShortcut( POPUA_FRAME_SKEL, MouseOverJointNr, MouseOverFrameNr, MouseOverAxisNr);

      return( 1);   // Processed it
    }
    break;
  }

  return( 0);  // NOT processed
}

/************************************************************************************
 * IqeB_GUI_Float_SetValue_Callback
 */

static void IqeB_GUI_Float_SetValue_Callback( Fl_Widget *w, void *pDummy)
{
  float Value;
  Fl_Float_Input *pThis;
  struct pose *pPose;

  // ...

  pThis  = (Fl_Float_Input *)w;

  Value  = atof( pThis->value());         // update the variable

  if( pPose_Data_Displayed != NULL &&     // security test, haved data to displayed pose
      pDrawModel != NULL &&               // Have drawing model
      pDrawModelCurAnim  != NULL) {       // Have current animation

    pPose = pPose_Data_Displayed;

    // Position
    if( pThis == pFloat_InputPX) {

      if( pPose->location[ 0] != Value) {

        pPose->KeyFlags |= (KEYFLAG_LOC_0 << 0);  //Value has changed, so set keyflag bit
        pPose->location[ 0] = Value;
      }

    } else if( pThis == pFloat_InputPY) {

      if( pPose->location[ 1] != Value) {

        pPose->KeyFlags |= (KEYFLAG_LOC_0 << 1);  //Value has changed, so set keyflag bit
        pPose->location[ 1] = Value;
      }

    } else if( pThis == pFloat_InputPZ) {

      if( pPose->location[ 2] != Value) {

        pPose->KeyFlags |= (KEYFLAG_LOC_0 << 2);  //Value has changed, so set keyflag bit
        pPose->location[ 2] = Value;
      }

    // angle
    } else if( pThis == pFloat_InputA0) {

      if( pPose->angles[ 0] != Value) {

        pPose->KeyFlags |= (KEYFLAG_ANG_0 << 0);  //Value has changed, so set keyflag bit
        pPose->angles[ 0] = Value;
        IqeB_AnimAnglesToRotPose( pPose);
      }

    } else if( pThis == pFloat_InputA1) {

      if( pPose->angles[ 1] != Value) {

        pPose->KeyFlags |= (KEYFLAG_ANG_0 << 1);  //Value has changed, so set keyflag bit
        pPose->angles[ 1] = Value;
        IqeB_AnimAnglesToRotPose( pPose);
      }

    } else if( pThis == pFloat_InputA2) {

      if( pPose->angles[ 2] != Value) {

        pPose->KeyFlags |= (KEYFLAG_ANG_0 << 2);  //Value has changed, so set keyflag bit
        pPose->angles[ 2] = Value;
        IqeB_AnimAnglesToRotPose( pPose);
      }

    // scale
    } else if( pThis == pFloat_InputSX) {

      if( Value < 0.001) Value = 0.001; // Clipping

      if( IqeB_ToolsAnimEdit_PoseScaleAll) { // For pose scale change, change all scales

        if( pPose->scale[ 1] != Value) {

          pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 1);  //Value has changed, so set keyflag bit
          pPose->scale[ 1] = Value;
        }

        if( pPose->scale[ 2] != Value) {

          pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 2);  //Value has changed, so set keyflag bit
          pPose->scale[ 2] = Value;
        }
      }

      if( pPose->scale[ 0] != Value) {

        pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 0);  //Value has changed, so set keyflag bit
        pPose->scale[ 0] = Value;
      }

    } else if( pThis == pFloat_InputSY) {

      if( Value < 0.001) Value = 0.001; // Clipping

      if( IqeB_ToolsAnimEdit_PoseScaleAll) { // For pose scale change, change all scales

        if( pPose->scale[ 0] != Value) {

          pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 0);  //Value has changed, so set keyflag bit
          pPose->scale[ 0] = Value;
        }

        if( pPose->scale[ 2] != Value) {

          pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 2);  //Value has changed, so set keyflag bit
          pPose->scale[ 2] = Value;
        }
      }

      if( pPose->scale[ 1] != Value) {

        pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 1);  //Value has changed, so set keyflag bit
        pPose->scale[ 1] = Value;
      }

    } else if( pThis == pFloat_InputSZ) {

      if( Value < 0.001) Value = 0.001; // Clipping

      if( IqeB_ToolsAnimEdit_PoseScaleAll) { // For pose scale change, change all scales

        if( pPose->scale[ 0] != Value) {

          pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 0);  //Value has changed, so set keyflag bit
          pPose->scale[ 0] = Value;
        }

        if( pPose->scale[ 1] != Value) {

          pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 1);  //Value has changed, so set keyflag bit
          pPose->scale[ 1] = Value;
        }
      }

      if( pPose->scale[ 2] != Value) {

        pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 2);  //Value has changed, so set keyflag bit
        pPose->scale[ 2] = Value;
      }

    }

    AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
  }
}

/************************************************************************************
 * IqeB_GUI_Button_Reset_Callback
 */

static void IqeB_GUI_Button_Reset_Callback( Fl_Widget *w, void *pDummy)
{
  float Value;
  Fl_Button *pThis;
  struct pose *pPose;

  // ...

  pThis  = (Fl_Button *)w;

  if( pPose_Data_Displayed != NULL &&     // security test, haved data to displayed pose
      pDrawModel != NULL &&               // Have drawing model
      pDrawModel->skel != NULL &&         // Have a skeleton
      pDrawModelCurAnim  != NULL) {       // Have current animation

    pPose = pPose_Data_Displayed;

    // Position
    if( pThis == pBut_ResetPX) {

      // Get value from sceleton
      Value  = pDrawModel->skel->pose[ PoseLastDrawnPoseNr].location[ 0];         // update to this value

      if( pPose->location[ 0] != Value) {

        pPose->KeyFlags |= (KEYFLAG_LOC_0 << 0);  //Value has changed, so set keyflag bit
        pPose->location[ 0] = Value;
      }

    } else if( pThis == pBut_ResetPY) {

      // Get value from sceleton
      Value  = pDrawModel->skel->pose[ PoseLastDrawnPoseNr].location[ 1];         // update to this value

      if( pPose->location[ 1] != Value) {

        pPose->KeyFlags |= (KEYFLAG_LOC_0 << 1);  //Value has changed, so set keyflag bit
        pPose->location[ 1] = Value;
      }

    } else if( pThis == pBut_ResetPZ) {

      // Get value from sceleton
      Value  = pDrawModel->skel->pose[ PoseLastDrawnPoseNr].location[ 2];         // update to this value

      if( pPose->location[ 2] != Value) {

        pPose->KeyFlags |= (KEYFLAG_LOC_0 << 2);  //Value has changed, so set keyflag bit
        pPose->location[ 2] = Value;
      }

    // angle
    } else if( pThis == pBut_ResetA0) {

      Value  = 0.0;         // update to this value

      if( pPose->angles[ 0] != Value) {

        pPose->KeyFlags |= (KEYFLAG_ANG_0 << 0);  //Value has changed, so set keyflag bit
        pPose->angles[ 0] = Value;
        IqeB_AnimAnglesToRotPose( pPose);
      }

    } else if( pThis == pBut_ResetA1) {

      Value  = 0.0;         // update to this value

      if( pPose->angles[ 1] != Value) {

        pPose->KeyFlags |= (KEYFLAG_ANG_0 << 1);  //Value has changed, so set keyflag bit
        pPose->angles[ 1] = Value;
        IqeB_AnimAnglesToRotPose( pPose);
      }

    } else if( pThis == pBut_ResetA2) {

      Value  = 0.0;         // update to this value

      if( pPose->angles[ 2] != Value) {

        pPose->KeyFlags |= (KEYFLAG_ANG_0 << 2);  //Value has changed, so set keyflag bit
        pPose->angles[ 2] = Value;
        IqeB_AnimAnglesToRotPose( pPose);
      }

    // scale
    } else if( pThis == pBut_ResetSX) {

      Value  = 1.0;         // update to this value

      if( IqeB_ToolsAnimEdit_PoseScaleAll) { // For pose scale change, change all scales

        if( pPose->scale[ 1] != Value) {

          pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 1);  //Value has changed, so set keyflag bit
          pPose->scale[ 1] = Value;
        }

        if( pPose->scale[ 2] != Value) {

          pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 2);  //Value has changed, so set keyflag bit
          pPose->scale[ 2] = Value;
        }
      }

      if( pPose->scale[ 0] != Value) {

        pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 0);  //Value has changed, so set keyflag bit
        pPose->scale[ 0] = Value;
      }

    } else if( pThis == pBut_ResetSY) {

      Value  = 1.0;         // update to this value

      if( IqeB_ToolsAnimEdit_PoseScaleAll) { // For pose scale change, change all scales

        if( pPose->scale[ 0] != Value) {

          pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 0);  //Value has changed, so set keyflag bit
          pPose->scale[ 0] = Value;
        }

        if( pPose->scale[ 2] != Value) {

          pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 2);  //Value has changed, so set keyflag bit
          pPose->scale[ 2] = Value;
        }
      }

      if( pPose->scale[ 1] != Value) {

        pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 1);  //Value has changed, so set keyflag bit
        pPose->scale[ 1] = Value;
      }

    } else if( pThis == pBut_ResetSZ) {

      Value  = 1.0;         // update to this value

      if( IqeB_ToolsAnimEdit_PoseScaleAll) { // For pose scale change, change all scales

        if( pPose->scale[ 0] != Value) {

          pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 0);  //Value has changed, so set keyflag bit
          pPose->scale[ 0] = Value;
        }

        if( pPose->scale[ 1] != Value) {

          pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 1);  //Value has changed, so set keyflag bit
          pPose->scale[ 1] = Value;
        }
      }

      if( pPose->scale[ 2] != Value) {

        pPose->KeyFlags |= (KEYFLAG_SCALE_0 << 2);  //Value has changed, so set keyflag bit
        pPose->scale[ 2] = Value;
      }
    }

    AnimEditUpdateAterCurAnimChange();  // Update current animation and prepare for display
  }
}

/************************************************************************************
 * IqeB_ToolsAnimEdit_ViewMode_Callback
 */

static void IqeB_ToolsAnimEdit_ViewMode_Callback( Fl_Widget *w, void *data)
{

  if( IqeB_ToolsAnimEdit_ViewMode != (int)data) {

    IqeB_ToolsAnimEdit_ViewMode = (int)data;    // Set radio button value

    pGUI_Scroll->redraw();     // Redraw it
  }
}

/************************************************************************************
 * IqeB_ToolsAnimEdit_PosePart_Callback
 */

static void IqeB_ToolsAnimEdit_PosePart_Callback( Fl_Widget *w, void *data)
{

  if( IqeB_ToolsAnimEdit_PosePart != (int)data) {

    IqeB_ToolsAnimEdit_PosePart = (int)data;    // Set radio button value

    pGUI_Scroll->redraw();     // Redraw it
  }
}

/************************************************************************************
 * IqeB_GUI_CBox_SetValue_Callback
 */

static void IqeB_GUI_CBox_SetValue_Callback( Fl_Widget *w, void *pValueArg)
{
  int *pValue;
  Fl_Check_Button *pThis;

  // ...

  pThis  = (Fl_Check_Button *)w;
  pValue = (int *)pValueArg;             // get pointer to associated variable

  *pValue = pThis->value();              // update the variable
}

/************************************************************************************
 * DrawingAreaScrollX_cb
 *
 * Someone moved a scrollbar of the drawing area
 */

void DrawingAreaScrollX_cb( Fl_Widget *o, void *data)
{
  Fl_Scroll* s = (Fl_Scroll*)(o->parent());

  s->scroll_to(s->xposition(), int(((Fl_Scrollbar*)o)->value()));

  pAnimEditDrawing->redraw();
  pGUI_Scroll->redraw();   // force later redraw
}

/************************************************************************************
 * DrawingAreaScrollY_cb
 *
 * Someone moved a scrollbar of the drawing area
 */

void DrawingAreaScrollY_cb( Fl_Widget *o, void *data)
{
  Fl_Scroll* s = (Fl_Scroll*)(o->parent());

  s->scroll_to(int(((Fl_Scrollbar*)o)->value()), s->yposition());

  pAnimEditDrawing->redraw();
  pGUI_Scroll->redraw();   // force later redraw
}

/************************************************************************************
 * IqeB_GUI_ToolsAnimEditUpdate
 *
 * Updates the joints view
 */

static void IqeB_GUI_ToolsAnimEditUpdate()
{

  // Update the pose GUI with current GUI selection.
  IqeB_GUI_ToolsFramePoseUpdate( DrawModelCurframe,IqeB_DispStyle_JointSelected);  // Update the pose GUI

  pAnimEditDrawing->redraw();
  pGUI_Scroll->redraw();     // Redraw it
}

/************************************************************************************
 * IqeB_GUI_ToolsMyIdleAction
 *
 * Idle action for this window.
 */

static void IqeB_GUI_ToolsMyIdleAction( void *)
{
  int DoRedraw;
  unsigned int TimeTemp;
  static unsigned int TimeLastCalled = 0;

  TimeTemp = IqeB_Disp_GetTickCount(); // Get current time

  //

  if( pMyToolWin == NULL) {     // Security testm window must exist

    return;
  }

  //
  // have to redraw the scrollable timeline area
  //

  DoRedraw = false;

  // Hack to reset scroll bars 0: start, 1: first draw done, 2: done

  if( AnimEditDrawingResetScrolHack == 1) {    // first draw done

    AnimEditDrawingResetScrolHack = 2;         // done

    pGUI_Scroll->scroll_to( 1, 1);
    pGUI_Scroll->scroll_to( 0, 0);

    DoRedraw = true;
  }

  if( AnimEditDrawForceRedraw) {      // Other reason to forc a redraw

    AnimEditDrawForceRedraw = false;    // Reset force a redraw

    DoRedraw = true;
  }

  // Catch some last drawn things

  if( AnimEditDrawLastFrameNr != DrawModelCurframe) {   // Has changed

    DoRedraw = true;
  }

  // ...

  if( DoRedraw) {                          // Have to redraw and window is existing

    pGUI_Scroll->redraw();   // force later redraw
  }

  //
  // have to update the pose GUI elements
  //

  int iFrame, iPose;

  if( pDrawModel == NULL ||
      pDrawModel->anim_count <= 0 ||   // No animations
      pDrawModel->anim_poses <= 0 ||   // No poses
      pDrawModelCurAnim == NULL ||     // No current animation
      DrawModelCurframe < 0 || DrawModelCurframe >= pDrawModelCurAnim->len ||  // Frame indes out of range
      IqeB_DispStyle_JointSelected < 0 || IqeB_DispStyle_JointSelected >= pDrawModel->anim_poses) {    // Pose index out of range

    iFrame = -1;     // Invalidate selection
    iPose  = -1;
  } else {

    iFrame = DrawModelCurframe;               // Current displayed values
    iPose  = IqeB_DispStyle_JointSelected;
  }

  if( PoseLastDrawnFrameNr != iFrame || PoseLastDrawnPoseNr != iPose) {  // Have to update

    IqeB_GUI_ToolsFramePoseUpdate( iFrame, iPose);
  }

  //
  // some timed actions (not each
  //

  if( TimeTemp - TimeLastCalled >= 100) {  // 30 ms gone since last call

    TimeLastCalled = TimeTemp;             // Remeber last time called

    // some time gone, do ...


    // get window size

    MyWinSizeX = pMyToolWin->w();  // update window size
    MyWinSizeY = pMyToolWin->h();  // update window size

  }

}

/************************************************************************************
 * IqeB_GUI_UpdateCallback
 *
 * Is called from IqeB_GUI_UpdateWidgets() if something has changed
 * what needs an update (new model displayed, ...).
 */

static void IqeB_GUI_UpdateCallback( int UpdateFlags)
{
  //x/int LastSelection;

  if( pMyToolWin == NULL) {  // security test, has main window

    return;
  }

  IqeB_GUI_ToolsAnimEditUpdate();
}

/************************************************************************************
 * close_cb, close this window
 *
 * pValueArg is a pointer to the widget. Set this pointer to NULL on deletion.
 */

static void close_cb( Fl_Widget *w, void *pValueArg)
{

  // Remove window idle action

  Fl::remove_idle( IqeB_GUI_ToolsMyIdleAction);   // Remove idle function

  // ...

  IqeB_DispStyle_JointSelected = -1; // no joint selected

  IqeB_GUI_CloseToolWindow( (void **)&pMyToolWin);
}

/************************************************************************************
 * Presets for this tools window
 *
 */

 static T_GUI_PreferenceEntry MyPreferences[] =
 {

  // Window size

  { PREF_T_INT,   "WinSizeX", "720", &MyWinSizeX},
  { PREF_T_INT,   "WinSizeY", "451", &MyWinSizeY},

  // Others

  { PREF_T_INT,   "ViewMode",   "0", &IqeB_ToolsAnimEdit_ViewMode},
  { PREF_T_INT,   "PosePart",   "1", &IqeB_ToolsAnimEdit_PosePart},
  { PREF_T_INT,   "ScaleAll",   "1", &IqeB_ToolsAnimEdit_PoseScaleAll},
};

// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "ToolsAnimEdit", MyPreferences, sizeof( MyPreferences) / sizeof( T_GUI_PreferenceEntry),
                                               (void **)(&pMyToolWin), &MyWinPosX, &MyWinPosY);

/************************************************************************************
 * IqeB_GUI_ToolsAnimEditWin
 *
 * Open dialog
 */

void IqeB_GUI_ToolsAnimEditWin( int xLeft, int xRight, int yTop, int yBotton)
{

  //
  // winodow already created --> show it
  //

  if( pMyToolWin != NULL) {         // already have tool window

    // Show invisble window or bring visible window to foreground

    pMyToolWin->show();          // show it

    return;
  }

  //
  // creation of window on first call
  //

  if( pMyToolWin == NULL) {  // no tool window until now

    int xPos, yPos, MouseMode;

    xPos = xRight;
    yPos = yTop;

    MouseMode = IQE_GUI_MOUSE_MODE_ANIM_EDIT;     // Mosue mode i wand to set

    if( IqeB_GUI_ToolWindowTestFreeEditMode( MouseMode)) {  // Test to have a free edit mode

      return;
    }

    if( MyWinPosX != IQE_GUI_NO_WINPOS_X && MyWinPosY != IQE_GUI_NO_WINPOS_Y) { // have last window position

      xPos = MyWinPosX;
      yPos = MyWinPosY;
    }

    // clip window sizes

    if( MyWinSizeX < MYWIN_SIZE_X_MIN) MyWinSizeX = MYWIN_SIZE_X_MIN;
    if( MyWinSizeX > MYWIN_SIZE_X_MAX) MyWinSizeX = MYWIN_SIZE_X_MAX;

    if( MyWinSizeY < MYWIN_SIZE_Y_MIN) MyWinSizeY = MYWIN_SIZE_Y_MIN;
    if( MyWinSizeY > MYWIN_SIZE_Y_MAX) MyWinSizeY = MYWIN_SIZE_Y_MAX;

    // create the window

    pMyToolWin = new Fl_Double_Window( xPos, yPos, MyWinSizeX, MyWinSizeY, "Animation/Editor");

    if( pMyToolWin == NULL) {  // security test

      return;
    }

    IqeB_GUI_RegisterCallback( (void **)&pMyToolWin, IqeB_GUI_UpdateCallback,
                               IQE_GUI_UPDATE_MODEL_CHANGE | IQE_GUI_UPDATE_JOINT_SELECTED | IQE_GUI_UPDATE_JOINT_CHANGED | IQE_GUI_UPDATE_ANIMATION,
                               MouseMode, (void(*)(void *, void *))close_cb);
  }

  // Add idle action for this window

  Fl::add_idle( IqeB_GUI_ToolsMyIdleAction);      // Redraw window during idle

  //
  //  GUI things
  //

  int x, x1, xx, xFrameInfo, xxFrameInfo, y1, y, yy, yy2;
  Fl_Box *pTempBox;
  Fl_Radio_Round_Button *pRadioButTemp;
  Fl_Check_Button *pTemp_Check_Button;

  x1  = 2;
  xxFrameInfo = 76;
  xFrameInfo  = pMyToolWin->w() - xxFrameInfo - 2;

  xx  = xFrameInfo - x1 - 2;

  y1 = 24;
  y = y1;

  // Scroll area

  yy2 = MyWinSizeY - y - 2;

  pGUI_Scroll = new Fl_Scroll( x1, y1, xx, yy2);

  pGUI_Scroll->box( FL_DOWN_FRAME /* FL_NO_BOX */);
  pGUI_Scroll->type( Fl_Scroll::BOTH_ALWAYS);
  pGUI_Scroll->scrollbar.align( FL_ALIGN_LEFT+FL_ALIGN_BOTTOM);
  pGUI_Scroll->color( FL_BACKGROUND2_COLOR);  // set color for background

#ifdef use_again // 24.05.2015 RR: No tooltip. Is disturbing.
  pGUI_Scroll->tooltip( "Timeline / plot graph view.\n"
                        "* Left mouse button:\n"
                        "  * Selects joints (line) or frames (column)\n"
                        "  * Plot view: change selected pose data\n"
                        "* Right mouse button:\n"
                        "  Pop up a context menu.\n"
                        "  The menu depents on the view mode and the\n"
                        "  mouse click location.");
#endif

  pGUI_Scroll->scrollbar.callback( DrawingAreaScrollX_cb);
  pGUI_Scroll->hscrollbar.callback( DrawingAreaScrollY_cb);

  pAnimEditDrawing = new AnimEditDrawing( x1, y1, xx * 2, yy2 * 2, 0);
  pAnimEditDrawing->color( FL_BACKGROUND2_COLOR);  // set color for background

  y += yy2;

  // End scroll

  pGUI_Scroll->end();

  //
  // Group around view GUI elements.
  //

  x1 = 4;
  x  = x1;
  y  = 3;

  yy = 20;
  xx = 56;

  pGroupViewMode = new Fl_Group( 0, y, x + xx * 5 + 4, yy);

  pRadioButTemp = new Fl_Radio_Round_Button( x, y, xx, yy, "&Timel.");
  pRadioButTemp->tooltip( "View mode: Timeline\n"
                          "Shows timeline of current animation.");
  pRadioButTemp->callback( IqeB_ToolsAnimEdit_ViewMode_Callback, (void *)IQE_ANIM_EDIT_VIEW_MODE_TIMELINE);
  if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_TIMELINE) pRadioButTemp->value( 1);

  x += xx;
  x += 2;

  pRadioButTemp = new Fl_Radio_Round_Button( x, y, xx, yy, "Plot &3");
  pRadioButTemp->tooltip( "View mode: Plot 3\n"
                          "Shows 3 graphs (over time) of the selected joint.");
  pRadioButTemp->callback( IqeB_ToolsAnimEdit_ViewMode_Callback, (void *)IQE_ANIM_EDIT_VIEW_MODE_PLOT_3);
  if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_PLOT_3) pRadioButTemp->value( 1);

  x += xx;
  x += 2;

  pRadioButTemp = new Fl_Radio_Round_Button( x, y, xx, yy, "Plot &X");
  pRadioButTemp->tooltip( "View mode: Plot X\n"
                          "Shows X graph (over time) of the selected joint.");
  pRadioButTemp->callback( IqeB_ToolsAnimEdit_ViewMode_Callback, (void *)IQE_ANIM_EDIT_VIEW_MODE_PLOT_X);
  if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_PLOT_X) pRadioButTemp->value( 1);

  x += xx;
  x += 2;

  pRadioButTemp = new Fl_Radio_Round_Button( x, y, xx, yy, "Plot &Y");
  pRadioButTemp->tooltip( "View mode: Plot Y\n"
                          "Shows Y graph (over time) of the selected joint.");
  pRadioButTemp->callback( IqeB_ToolsAnimEdit_ViewMode_Callback, (void *)IQE_ANIM_EDIT_VIEW_MODE_PLOT_Y);
  if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_PLOT_Y) pRadioButTemp->value( 1);

  x += xx;
  x += 2;

  pRadioButTemp = new Fl_Radio_Round_Button( x, y, xx, yy, "Plot &Z");
  pRadioButTemp->tooltip( "View mode: Plot Z\n"
                          "Shows Z graph (over time) of the selected joint.");
  pRadioButTemp->callback( IqeB_ToolsAnimEdit_ViewMode_Callback, (void *)IQE_ANIM_EDIT_VIEW_MODE_PLOT_Z);
  if( IqeB_ToolsAnimEdit_ViewMode == IQE_ANIM_EDIT_VIEW_MODE_PLOT_Z) pRadioButTemp->value( 1);

  x += xx;

  // finish up group around pose GUI elements.

  pGroupViewMode->end();
  pGroupViewMode->resizable( NULL);
  GroupViewModeSizeXX = x + 2;  // Remember size of group
  GroupViewModeSizeYY = yy;
  pGroupViewMode->size( GroupViewModeSizeXX, GroupViewModeSizeYY);

  //
  // Group around view GUI elements.
  //

  x1 = x + 12;
  x  = x1;
  y  = 3;

  yy = 20;
  xx = 46;

  pGroupPosePart = new Fl_Group( x1, y, x + xx * 3 + 4, yy);

  pRadioButTemp = new Fl_Radio_Round_Button( x, y, xx, yy, "&Pos.");
  pRadioButTemp->tooltip( "Edit pose positions.");
  pRadioButTemp->callback( IqeB_ToolsAnimEdit_PosePart_Callback, (void *)IQE_ANIM_EDIT_POSE_PART_LOCATION);
  if( IqeB_ToolsAnimEdit_PosePart == IQE_ANIM_EDIT_POSE_PART_LOCATION) pRadioButTemp->value( 1);

  x += xx;
  x += 2;

  pRadioButTemp = new Fl_Radio_Round_Button( x, y, xx, yy, "&Ang.");
  pRadioButTemp->tooltip( "Edit pose angles.");
  pRadioButTemp->callback( IqeB_ToolsAnimEdit_PosePart_Callback, (void *)IQE_ANIM_EDIT_POSE_PART_ANGLES);
  if( IqeB_ToolsAnimEdit_PosePart == IQE_ANIM_EDIT_POSE_PART_ANGLES) pRadioButTemp->value( 1);

  x += xx;
  x += 2;

  pRadioButTemp = new Fl_Radio_Round_Button( x, y, xx, yy, "&Sca.");
  pRadioButTemp->tooltip( "Edit pose scales.");
  pRadioButTemp->callback( IqeB_ToolsAnimEdit_PosePart_Callback, (void *)IQE_ANIM_EDIT_POSE_PART_SCALE);
  if( IqeB_ToolsAnimEdit_PosePart == IQE_ANIM_EDIT_POSE_PART_SCALE) pRadioButTemp->value( 1);

  x += xx;

  // finish up group around pose GUI elements.

  pGroupPosePart->end();
  pGroupPosePart->resizable( NULL);
  GroupPosePartSizeXX = x - x1 + 2;  // Remember size of group
  GroupPosePartSizeYY = yy;
  GroupPosePartSizeX = pGroupPosePart->x();
  GroupPosePartSizeY = pGroupPosePart->y();
  pGroupPosePart->size( GroupViewModeSizeXX, GroupViewModeSizeYY);

  // Group around pose GUI elements.
  // Need this to make it not 'resizeable'.
  // Otherwise they will change with the window size!

  x1 = xFrameInfo;
  y = y1; //2;

  yy = 20;
  xx = xxFrameInfo;

  pGroupPose = new Fl_Group( x1, 0, xx, y + (yy + 2) * 9 + 12);

  // header

  pTempBox = new Fl_Box( FL_NO_BOX, x1, 2, xx, y - 4, "Pose");
  pTempBox->labelsize( 20);
  pTempBox->tooltip( "The pose group shows the\n"
                     "settings of the current selected:\n"
                     "* Animation\n"
                     "* Frame of this animation\n"
                     "* Pose (joint) of the skeleton");

  // Pose position

  y += 12;   // Indent for label of next GUI element

  pFloat_InputPX = new Fl_Float_Input( x1, y, xx - 12, yy, "Position");
  pFloat_InputPX->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputPX->labelsize( 10);
  pFloat_InputPX->tooltip( "Pose position X");
  pFloat_InputPX->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pBut_ResetPX = new Fl_Button( x1 + xx - 12, y, 12, yy, "R");
  pBut_ResetPX->labelsize( 10);
  pBut_ResetPX->tooltip( "Reset pose position X");
  pBut_ResetPX->callback( IqeB_GUI_Button_Reset_Callback, NULL);

  y += yy;
  y += 2;

  pFloat_InputPY = new Fl_Float_Input( x1, y, xx - 12, yy);
  pFloat_InputPY->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputPY->labelsize( 10);
  pFloat_InputPY->tooltip( "Pose position Y");
  pFloat_InputPY->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pBut_ResetPY = new Fl_Button( x1 + xx - 12, y, 12, yy, "R");
  pBut_ResetPY->labelsize( 10);
  pBut_ResetPY->tooltip( "Reset pose position Y");
  pBut_ResetPY->callback( IqeB_GUI_Button_Reset_Callback, NULL);

  y += yy;
  y += 2;

  pFloat_InputPZ = new Fl_Float_Input( x1, y, xx - 12, yy);
  pFloat_InputPZ->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputPZ->labelsize( 10);
  pFloat_InputPZ->tooltip( "Pose position Z");
  pFloat_InputPZ->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pBut_ResetPZ = new Fl_Button( x1 + xx - 12, y, 12, yy, "R");
  pBut_ResetPZ->labelsize( 10);
  pBut_ResetPZ->tooltip( "Reset pose position Y");
  pBut_ResetPZ->callback( IqeB_GUI_Button_Reset_Callback, NULL);

  y += yy;
  y += 2;

  // Pose angles

  y += 12;   // Indent for label of next GUI element

  pFloat_InputA0 = new Fl_Float_Input( x1, y, xx - 12, yy, "Angles (degree)");
  pFloat_InputA0->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputA0->labelsize( 10);
  pFloat_InputA0->tooltip( "Pose angle X axis");
  pFloat_InputA0->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pBut_ResetA0 = new Fl_Button( x1 + xx - 12, y, 12, yy, "R");
  pBut_ResetA0->labelsize( 10);
  pBut_ResetA0->tooltip( "Reset pose angle X to 0�");
  pBut_ResetA0->callback( IqeB_GUI_Button_Reset_Callback, NULL);

  y += yy;
  y += 2;

  pFloat_InputA1 = new Fl_Float_Input( x1, y, xx - 12, yy);
  pFloat_InputA1->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputA1->labelsize( 10);
  pFloat_InputA1->tooltip( "Pose angle Y axis");
  pFloat_InputA1->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pBut_ResetA1 = new Fl_Button( x1 + xx - 12, y, 12, yy, "R");
  pBut_ResetA1->labelsize( 10);
  pBut_ResetA1->tooltip( "Reset pose angle Y to 0�");
  pBut_ResetA1->callback( IqeB_GUI_Button_Reset_Callback, NULL);

  y += yy;
  y += 2;

  pFloat_InputA2 = new Fl_Float_Input( x1, y, xx - 12, yy);
  pFloat_InputA2->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputA2->labelsize( 10);
  pFloat_InputA2->tooltip( "Pose angle Z axis");
  pFloat_InputA2->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pBut_ResetA2 = new Fl_Button( x1 + xx - 12, y, 12, yy, "R");
  pBut_ResetA2->labelsize( 10);
  pBut_ResetA2->tooltip( "Reset pose angle Z to 0�");
  pBut_ResetA2->callback( IqeB_GUI_Button_Reset_Callback, NULL);

  y += yy;
  y += 2;

  // Pose size

  y += 12;   // Indent for label of next GUI element

  pFloat_InputSX = new Fl_Float_Input( x1, y, xx - 12, yy, "Scales");
  pFloat_InputSX->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputSX->labelsize( 10);
  pFloat_InputSX->tooltip( "Pose scale X");
  pFloat_InputSX->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pBut_ResetSX = new Fl_Button( x1 + xx - 12, y, 12, yy, "R");
  pBut_ResetSX->labelsize( 10);
  pBut_ResetSX->tooltip( "Reset pose scale X to 1.0");
  pBut_ResetSX->callback( IqeB_GUI_Button_Reset_Callback, NULL);

  y += yy;
  y += 2;

  pFloat_InputSY = new Fl_Float_Input( x1, y, xx - 12, yy);
  pFloat_InputSY->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputSY->labelsize( 10);
  pFloat_InputSY->tooltip( "Pose scale Y");
  pFloat_InputSY->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pBut_ResetSY = new Fl_Button( x1 + xx - 12, y, 12, yy, "R");
  pBut_ResetSY->labelsize( 10);
  pBut_ResetSY->tooltip( "Reset pose scale Y to 1.0");
  pBut_ResetSY->callback( IqeB_GUI_Button_Reset_Callback, NULL);

  y += yy;
  y += 2;

  pFloat_InputSZ = new Fl_Float_Input( x1, y, xx - 12, yy);
  pFloat_InputSZ->align( FL_ALIGN_TOP_LEFT);     // align for label
  pFloat_InputSZ->labelsize( 10);
  pFloat_InputSZ->tooltip( "Pose scale Z");
  pFloat_InputSZ->callback( IqeB_GUI_Float_SetValue_Callback, NULL);

  pBut_ResetSZ = new Fl_Button( x1 + xx - 12, y, 12, yy, "R");
  pBut_ResetSZ->labelsize( 10);
  pBut_ResetSZ->tooltip( "Reset pose scale Z to 1.0");
  pBut_ResetSZ->callback( IqeB_GUI_Button_Reset_Callback, NULL);

  y += yy;
  y += 2;

  // Checkboxes, Size change changes all sizes

  y += 16;

  pTemp_Check_Button = new Fl_Check_Button( x1, y, xx, yy, "Scale all");
  pTemp_Check_Button->tooltip( "Changing a pose scale\n"
                               "will sets all pose scales to the same value.");
  pTemp_Check_Button->value( IqeB_ToolsAnimEdit_PoseScaleAll);
  pTemp_Check_Button->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_ToolsAnimEdit_PoseScaleAll);

  y += yy;
  y += 2;

  // finish up group around pose GUI elements.

  pGroupPose->end();
  pGroupPose->resizable( NULL);
  GroupPoseSizeXX = xx;  // Remember size of group
  GroupPoseSizeYY = y;
  pGroupPose->size( GroupPoseSizeXX, GroupPoseSizeYY);

  // finish up

  pMyToolWin->resizable( pGUI_Scroll);

  //pMyToolWin->size( pMyToolWin->w(), y + 4);  // final size of window
  pMyToolWin->size_range( MYWIN_SIZE_X_MIN, MYWIN_SIZE_Y_MIN, MYWIN_SIZE_X_MAX, MYWIN_SIZE_Y_MAX);   // min/max window size

  pMyToolWin->end();
  pMyToolWin->set_non_modal();
  pMyToolWin->callback( close_cb, &pMyToolWin);

  // Hack to reset scroll bars 0: start, 1: first draw done, 2: done
  AnimEditDrawingResetScrolHack = 0;  // Start hack
  AnimEditDrawForceRedraw = false;    // Reset force a redraw
#ifdef USE_DRAW_DEBUG_TEXT   // display a debug text
  AnimEditDrawDebugStr[ 0] = '\0';    // Reset debug string to output on the drawing area
#endif
  DrawLayout_Valid = false;           // Set draing layout to not valid
  MouseOverJointNr = -1;              // Reset mouse is over joint
  MouseOverFrameNr = -1;              // Reset mouse is over frame
  MouseOverAxisNr  = -1;              // Reset mouse is over graph view

  PoseLastDrawnFrameNr = -1;          // Last drawn for pose GUI elements
  PoseLastDrawnPoseNr  = -1;

  // show the window

  IqeB_GUI_ToolsAnimEditUpdate();    // Update the timelineGUI
  pMyToolWin->show();

}

/********************************** End Of File **********************************/
