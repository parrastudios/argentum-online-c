/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/MatrixStack.h>
#include <Memory/General.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

struct MatrixStack * StackCreate(uint32 Depth)
{
	struct MatrixStack * Stack = MemoryAllocate(sizeof(struct MatrixStack));

	if (!Stack)
		return NULL;

	Stack->Depth	= Depth;
	Stack->Pointer	= 0;
	Stack->Data		= MemoryAllocate(sizeof(struct Matrix4f) * Depth);
	Stack->Error	= MATRIX_STACK_ERROR_OK;

	if (!Stack->Data)
	{
		MemoryDeallocate(Stack);
		return NULL;
	}

	Matrix4fIdentity(&Stack->Data[0]);

	return Stack;
}

void StackDestroy(struct MatrixStack * Stack)
{
	if (Stack)
	{
		if (Stack->Data)
		{
			MemoryDeallocate(Stack->Data);
		}

		MemoryDeallocate(Stack);
	}
}

void StackLoadIdentity(struct MatrixStack * Stack)
{
	Matrix4fIdentity(&Stack->Data[Stack->Pointer]);
}

void StackLoadMatrix(struct MatrixStack * Stack, struct Matrix4f * Matrix)
{
	Matrix4fCopy(&Stack->Data[Stack->Pointer], Matrix);
}

//void StackLoadTransform(struct MatrixStack * Stack, struct TransformType * Transform)
//{
	// struct Matrix4f Matrix;

	// TransformGetMatrix(Transform, &Matrix);

	// StackLoadMatrix(Stack, &Matrix);
//}

void StackMultMatrix(struct MatrixStack * Stack, struct Matrix4f * Matrix)
{
	struct Matrix4f Temp;

	Matrix4fCopy(&Temp, &Stack->Data[Stack->Pointer]);

	Matrix4fMultiply(&Stack->Data[Stack->Pointer], &Temp, Matrix);
}

//void StackMultTransform(struct MatrixStack * Stack, struct TransformType * Transform)
//{
	// struct Matrix4f Matrix;

	// TransformGetMatrix(Transform, &Matrix);

	// StackMultMatrix(Stack, &Matrix);
//}

void StackPush(struct MatrixStack * Stack)
{
	if (Stack->Pointer < (Stack->Depth - 1))
	{
		Stack->Pointer++;
		Matrix4fCopy(&Stack->Data[Stack->Pointer], &Stack->Data[Stack->Pointer - 1]);
	}
	else
	{
		Stack->Error = MATRIX_STACK_ERROR_OVERFLOW;
	}
}

void StackPop(struct MatrixStack * Stack)
{
	if (Stack->Pointer > 0)
	{
		Stack->Pointer--;
	}
	else
	{
		Stack->Error = MATRIX_STACK_ERROR_UNDERFLOW;
	}
}

void StackScale(struct MatrixStack * Stack, float x, float y, float z)
{
	struct Matrix4f Temp, Scale;

	Matrix4fScale(&Scale, x, y, z);

	Matrix4fCopy(&Temp, &Stack->Data[Stack->Pointer]);

	Matrix4fMultiply(&Stack->Data[Stack->Pointer], &Temp, &Scale);
}

void StackTranslate(struct MatrixStack * Stack, float x, float y, float z)
{
	struct Matrix4f Temp, Translation;

	Matrix4fTranslate(&Translation, x, y, z);

	Matrix4fCopy(&Temp, &Stack->Data[Stack->Pointer]);

	Matrix4fMultiply(&Stack->Data[Stack->Pointer], &Temp, &Translation);
}

void StackRotate(struct MatrixStack * Stack, float Angle, uint32 Axis)
{
	struct Matrix4f Temp, Rotate;

	Matrix4fRotateAngle(&Rotate, degtorad(Angle), Axis);

	Matrix4fCopy(&Temp, &Stack->Data[Stack->Pointer]);

	Matrix4fMultiply(&Stack->Data[Stack->Pointer], &Temp, &Rotate);
}

void StackScale3f(struct MatrixStack * Stack, struct Vector3f * Vector)
{
	struct Matrix4f Temp, Scale;

	Matrix4fScaleVector(&Scale, Vector);

	Matrix4fCopy(&Temp, &Stack->Data[Stack->Pointer]);

	Matrix4fMultiply(&Stack->Data[Stack->Pointer], &Temp, &Scale);
}

void StackTranslate3f(struct MatrixStack * Stack, struct Vector3f * Vector)
{
	struct Matrix4f Temp, Translation;

	Matrix4fIdentity(&Translation);

	Vector3fCopy((struct Vector3f*)&Translation.m[3][0], Vector);

	Matrix4fCopy(&Temp, &Stack->Data[Stack->Pointer]);

	Matrix4fMultiply(&Stack->Data[Stack->Pointer], &Temp, &Translation);
}

void StackPushMatrix(struct MatrixStack * Stack, struct Matrix4f * Matrix)
{
	if (Stack->Pointer < Stack->Depth)
	{
		Stack->Pointer++;
		Matrix4fCopy(&Stack->Data[Stack->Pointer], Matrix);
	}
	else
	{
		Stack->Error = MATRIX_STACK_ERROR_OVERFLOW;
	}
}

//void StackPushTransform(struct MatrixStack * Stack, struct TransformType * Transform)
//{
	// struct Matrix4f Matrix;

	// TransformGetMatrix(Transform, &Matrix);

	// StackPushMatrix(Stack, &Matrix);
//}

struct Matrix4f * StackGetMatrix(struct MatrixStack * Stack)
{
	return &Stack->Data[Stack->Pointer];
}

void StackCopyMatrix(struct MatrixStack * Stack, struct Matrix4f * Matrix)
{
	Matrix4fCopy(Matrix, &Stack->Data[Stack->Pointer]);
}

uint32 StackGetError(struct MatrixStack * Stack)
{
	uint32 Error = Stack->Error;
	Stack->Error = MATRIX_STACK_ERROR_OK;
	return Error;
}
