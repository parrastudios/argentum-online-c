/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GAME_PLAYER_DEFINITIONS_H
#define GAME_PLAYER_DEFINITIONS_H

////////////////////////////////////////////////////////////
// Header constraints
////////////////////////////////////////////////////////////
#ifndef GAME_PLAYER_H
#	error Game/Player must be included instead
#endif

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PLAYER_INV_MAX_SLOTS			32		///< Inventory Definitions
#define PLAYER_INV_MAX_OBJ				100000

#define PLAYER_SPELLS_MAX_SLOTS			32		///< Spell Definitons

#define PLAYER_NAME_MAX_SIZE			0x10	///< Player Info Definitions
#define PLAYER_DESC_MAX_SIZE			0xFF

#define PLAYER_STATS_MIN				0x00	///< Stats Definitions
#define PLAYER_STATS_MAX				0x01
#define PLAYER_STATS_COUNT				0x02

#endif // GAME_PLAYER_DEFINITIONS_H
