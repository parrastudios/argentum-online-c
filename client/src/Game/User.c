/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//	Copyright (C) 2009-2015 Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
//
//	A cross-platform mmorpg which keeps the original essence of
//	Argentum Online as combats, magics, guilds, although it has new
//	implementations as 3D graphics engine, new gameplay,
//	and a better performance, among others.
//
//	GNU Affero General Public License 3.0
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
//
//	Apache 2.0
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/User.h>
#include <Game/Login.h>
#include <Game/Movement.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct UserType	UserData;	///< User global data

////////////////////////////////////////////////////////////
/// User initialize
////////////////////////////////////////////////////////////
void UserReset()
{
	// todo: do it properly

	UserGetData()->Account.Logged = false;
	UserGetData()->Account.Count = 0;

	UserGetData()->Character.Movement = MoveNot;
}


////////////////////////////////////////////////////////////
/// User destroy
////////////////////////////////////////////////////////////
void UserDestroy()
{
	// TO DO: deallocate mutex (multithread)
	// ...
}

////////////////////////////////////////////////////////////
/// Return user global data
////////////////////////////////////////////////////////////
struct UserType * UserGetData()
{
	return &UserData;
}

////////////////////////////////////////////////////////////
/// Open the login panel
////////////////////////////////////////////////////////////
void UserLoginOpen()
{

}

////////////////////////////////////////////////////////////
/// Open the account panel
////////////////////////////////////////////////////////////
void UserAccountOpen()
{
	// Update account state
	// GameSetStatus(GAME_ACCOUNT);

	// Show accounts
	AccountOpen();
}

////////////////////////////////////////////////////////////
/// Move the character of user
////////////////////////////////////////////////////////////
bool UserCharacterMove(struct UserType * User, uint8 Key, bool Pressed, bool * Updated)
{
	uint32 PreviousMovement = User->Character.Movement;
	uint32 NextMovement = CharacterMove(PreviousMovement, Key, Pressed);

	// Get update flag
	*Updated = (NextMovement != PreviousMovement);

	if (NextMovement != MoveInvalid)
	{
		// Update next movement
		User->Character.Movement = NextMovement;

		return (NextMovement != MoveNot);
	}

	return false;
}