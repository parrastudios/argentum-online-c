![](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2012/Dec/26/argentum-online-c-logo-1554735323-0_avatar.png?raw=true "Argentum Online C")

# __Argentum Online C__
##### Copyright (C) 2009 - 2014
##### Vicente Eduardo Ferrer Garc�a - Parra Studios (<vic798@gmail.com>)


- - -


##### CHANGELOG
	
0.1 Alpha Release 2012-09-01
	
* Cross-platform networking support (TCP/IP v4)
* Connection management
* Protocol definiton and integration with event handlers
* Cross-platform multithreading support
* Cross-platform window handle support with multiple resolution format available
* Generic user input management
* Cross-platform 3D multi-buffer sound engine with ogg format support (based on OpenAL)
* Terrain engine based on heightmaps
* Basic graphics engine (based on OpenGL)
* Dynamic texture management with LRU (based on png format)
* Static and dynamic model support with animation based on skeleton
* Generic DataBase system with implementation using MySQL
* Pack resource system for managing assets
* Account system management
* Multi-language support (with English and Spanish translations)
* Basic entity system
* Configurable GUI system with integrated script
* Support for configuration file
* User management
* Math library (general functions, Rect, Triangle, Vector, Plane, Frustum and Quaternion support)
* Basic Data Types implementation
* Added memory management
* MD5 algorithm implementation
* I/O helper functions
* Timer management (sync and async versions based on threading)
* String basic library

0.1 Beta Release 2014-MM-DD

* Improved account system (mixed with web frontend management)
* Player and Npc management
* Entity system derived to robust component system
* World generic implementation with scene management
* Improved user input management (allowing to extend with handlers in the future)
* Device interface for allowing multiple versions of render libraries
* Support for Player movement
* Basic broadcast chat implementation
* Implemented display for debug information
* Added multiple primitives to the graphics engine
* Matrix stack and render states management
* GUI system extended (adding more widgets and possibility of key mapping)
* Extended math library (with multiple bounding volumes and Ray implementation)
* Extended Data Types (Vector, List, HashMap, Queue, Set)
* Added alignement cross-platform support for memory management
* Implementing tick system for task syncronization
* Created a base for deployer (implemented with nodejs)
* Implemented a base for security based on FANN library
* Base implementation for GUI Editor
* New backend for webpage (implemented with nodejs)

	
- - -

