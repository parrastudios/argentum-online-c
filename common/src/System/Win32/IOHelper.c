/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/IOHelper.h>
#include <Memory/General.h>

#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT && COMPILER_TYPE == COMPILER_MSVC

#ifndef vsscanf
//#	include <System/Win32/vsscanf.win.c>
#endif

////////////////////////////////////////////////////////////
// String print helper method
////////////////////////////////////////////////////////////
void IOHelperStrPrint(char * str, const char * format, ...)
{
	// Argument list
	va_list ArgList;

	// Initialize the argument list
	va_start(ArgList, format);

	// sprintf arguments on string
	vsprintf_s(str, _vscprintf( format, ArgList ) + 1, format, ArgList);

	// Destroy argument list
	va_end(ArgList);
}

////////////////////////////////////////////////////////////
// String copy helper method
////////////////////////////////////////////////////////////
void IOHelperStrCpy(char * destination, const char * source)
{
    if (destination)
		strcpy_s(destination, strlen(source) + 1, source);
}

////////////////////////////////////////////////////////////
// Sscan helper method
////////////////////////////////////////////////////////////
int32 IOHelperSscan(const char * str, const char * format, ...)
{
	// Argument list
	va_list ArgList;
	int32	Count;

	// Initialize the argument list
	va_start(ArgList, format);

	// Scan
	Count = vsscanf(str, format, ArgList);

	// Destroy argument list
	va_end(ArgList);

	return Count;
}

////////////////////////////////////////////////////////////
// Open helper method
////////////////////////////////////////////////////////////
bool IOHelperOpen(FILE** pFile, const char * filename, const char * mode)
{
	fopen_s(pFile, filename, mode);

	if (!*pFile)
		return false;

	return true;
}

#else

#ifdef fopen
#   undef fopen
#endif

#include <stdio.h>

////////////////////////////////////////////////////////////
// Open helper method
////////////////////////////////////////////////////////////
bool IOHelperOpen(FILE** pFile, const char * filename, const char * mode)
{
	*pFile = fopen(filename, mode);

	if (*pFile == NULL)
		return false;

	return true;
}

#endif

////////////////////////////////////////////////////////////
// Read field helper method
////////////////////////////////////////////////////////////
void IOHelperReadField(char * Destination, char * Source, char Separator, uint32 Position)
{
	uint32 SrcCur, DestCur = 0, PosCur = 0;

	for (SrcCur = 0; Source[SrcCur] != NULLCHAR && PosCur <= Position; ++SrcCur)
	{
		if (Source[SrcCur] == Separator)
		{
			++PosCur;
		}
		else
		{
			if (PosCur == Position)
			{
				Destination[DestCur] = Source[SrcCur];

				++DestCur;
			}
		}
	}

	Destination[DestCur] = NULLCHAR;
}

uint32 IOHelperReadFieldEx(char * Destination, uint32 DestSize, char * Source, uint32 SrcSize, char Separator, uint32 Position)
{
	uint32 SrcCur, DestCur = 0, PosCur = 0;

	for (SrcCur = 0; SrcCur < SrcSize && DestCur < DestSize && PosCur <= Position; ++SrcCur)
	{
		if (Source[SrcCur] == Separator)
		{
			++PosCur;
		}
		else
		{
			if (PosCur == Position)
			{
				Destination[DestCur] = Source[SrcCur];

				++DestCur;
			}
		}
	}

	Destination[DestCur] = NULLCHAR;

	return (DestCur + 1);
}
