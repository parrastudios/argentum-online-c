/****************************************************************************

  IqeB_GUI_ToolsDispBox.cpp

  05.02.2015 RR: First editon of this file.


*****************************************************************************
*/

#include "IqeBrowser.h"

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Box.H>

/************************************************************************************
 * Globals
 *
 * NOTE: A Quake2 palyer has a height of 56 and a witdh of 32 units.
 */

// show box (as reference object with definded height)

int   IqeB_ToolsDisp_BoxEnable  = 0;       // Shows a box in the display area
float IqeB_ToolsDisp_BoxHeight1 =  32.0;   // Height 1 of box
float IqeB_ToolsDisp_BoxHeight2 = -24.0;   // Height 2 of box
float IqeB_ToolsDisp_BoxWidth   =  32.0;   // Width of box

/************************************************************************************
 * Statics
 */

static  Fl_Window *pMyToolWin;
static int MyWinPosX = IQE_GUI_NO_WINPOS_X, MyWinPosY = IQE_GUI_NO_WINPOS_Y; // last window position

/************************************************************************************
 * IqeB_GUI_CBox_SetValue_Callback
 */

static void IqeB_GUI_CBox_SetValue_Callback( Fl_Widget *w, void *pValueArg)
{
  int *pValue;
  Fl_Check_Button *pThis;

  // ...

  pThis  = (Fl_Check_Button *)w;
  pValue = (int *)pValueArg;             // get pointer to associated variable

  *pValue = pThis->value();              // update the variable

  if( pValueArg == &IqeB_ToolsDisp_BoxEnable &&  // changed the enable
      pDrawModel != NULL) {                       // and showing a model

    IqeB_DispViewAdaptFromModel( pDrawModel);     // update
  }
}

/************************************************************************************
 * IqeB_GUI_CBox_SetValue_Callback
 */

static void IqeB_GUI_Float_SetValue_Callback( Fl_Widget *w, void *pValueArg)
{
  float *pValue;
  Fl_Float_Input *pThis;

  // ...

  pThis  = (Fl_Float_Input *)w;
  pValue = (float *)pValueArg;             // get pointer to associated variable

  *pValue = atof( pThis->value());         // update the variable

  if( IqeB_ToolsDisp_BoxEnable &&         // enabled
      pDrawModel != NULL) {                       // and showing a model

    IqeB_DispViewAdaptFromModel( pDrawModel);     // update
  }
}

/************************************************************************************
 * close_cb, close this window
 *
 * pValueArg is a pointer to the widget. Set this pointer to NULL on deletion.
 */

static void close_cb( Fl_Widget *w, void *pValueArg)
{

  IqeB_GUI_CloseToolWindow( (void **)&pMyToolWin);
}

/************************************************************************************
 * Presets for this tools window
 *
 */

 static T_GUI_PreferenceEntry MyPreferences[] =
 {

  // Box display

  { PREF_T_INT,   "BoxEnable",      "0", &IqeB_ToolsDisp_BoxEnable},
  { PREF_T_FLOAT, "BoxHeight1",  "32.0", &IqeB_ToolsDisp_BoxHeight1},
  { PREF_T_FLOAT, "BoxHeight2", "-24.0", &IqeB_ToolsDisp_BoxHeight2},
  { PREF_T_FLOAT, "BoxWidth",    "32.0", &IqeB_ToolsDisp_BoxWidth},
};

// Automatic add this preference settings at startup of the program.
static IqeB_PreferencesGroup MyPreferencesAdd( "ToolsDispBox", MyPreferences, sizeof( MyPreferences) / sizeof( T_GUI_PreferenceEntry),
                                               (void **)(&pMyToolWin), &MyWinPosX, &MyWinPosY);

/************************************************************************************
 * IqeB_GUI_ToolsDispBoxWin
 *
 * Open dialog
 */

void IqeB_GUI_ToolsDispBoxWin( int xLeft, int xRight, int yTop, int yBotton)
{

  //
  // winodow already created --> show it
  //

  if( pMyToolWin != NULL) {         // already have tool window

    // Show invisble window or bring visible window to foreground

    pMyToolWin->show();          // show it

    return;
  }

  //
  // creation of window on first call
  //

  if( pMyToolWin == NULL) {  // no tool window until now

    int xPos, yPos;

    xPos = xRight;
    yPos = yTop;

    if( MyWinPosX != IQE_GUI_NO_WINPOS_X && MyWinPosY != IQE_GUI_NO_WINPOS_Y) { // have last window position

      xPos = MyWinPosX;
      yPos = MyWinPosY;
    }

    pMyToolWin = new Fl_Window( xPos, yPos, IQE_GUI_TOOLS_STD_WITDH, 400, "Display/Box");

    if( pMyToolWin == NULL) {  // security test

      return;
    }
  }

  //
  //  GUI things
  //

  int x1, x2, xx2, y1, y, yy;
  char TempBuffer[ 256];

  Fl_Check_Button *pTemp_Check_Button;
  Fl_Float_Input  *pTemp_Float_Input;
  Fl_Box          *pTemp_Box;

  x1  = 8;
  xx2 = (pMyToolWin->w() - 16) / 2;
  x2  = x1 + xx2 / 2;
  yy  = 30;

  y1 = 8;
  y = y1;

  // frame it

  pTemp_Box = new Fl_Box( x1 - 4, y1 - 4, pMyToolWin->w() - 8, pMyToolWin->h() - 8, "Box");
  pTemp_Box->align( FL_ALIGN_TOP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
  pTemp_Box->box( FL_DOWN_FRAME);

  // enable checkbox

  pTemp_Check_Button = new Fl_Check_Button( x2, y, pMyToolWin->w() - x2 - 8, yy, "Show box");
  pTemp_Check_Button->tooltip( "Shows a framed box in the display area.\nUse as reference object with definded size.");
  pTemp_Check_Button->value( IqeB_ToolsDisp_BoxEnable);
  pTemp_Check_Button->callback( IqeB_GUI_CBox_SetValue_Callback, &IqeB_ToolsDisp_BoxEnable);

  y += yy;

  // Height 1 input

  pTemp_Float_Input = new Fl_Float_Input( x1 + xx2, y, xx2, yy, "Height top:");
  pTemp_Float_Input->type( FL_FLOAT_INPUT);
  pTemp_Float_Input->tooltip( "Height of top (Z axis)");
  sprintf( TempBuffer, "%.2f", IqeB_ToolsDisp_BoxHeight1);
  pTemp_Float_Input->value( TempBuffer);
  pTemp_Float_Input->callback( IqeB_GUI_Float_SetValue_Callback, &IqeB_ToolsDisp_BoxHeight1);

  y += yy;

  // Height 2 input

  pTemp_Float_Input = new Fl_Float_Input( x1 + xx2, y, xx2, yy, "Height bottom:");
  pTemp_Float_Input->type( FL_FLOAT_INPUT);
  pTemp_Float_Input->tooltip( "Height of bottom (Z axis)");
  sprintf( TempBuffer, "%.2f", IqeB_ToolsDisp_BoxHeight2);
  pTemp_Float_Input->value( TempBuffer);
  pTemp_Float_Input->callback( IqeB_GUI_Float_SetValue_Callback, &IqeB_ToolsDisp_BoxHeight2);

  y += yy;

  // Width input

  pTemp_Float_Input = new Fl_Float_Input( x1 + xx2, y, xx2, yy, "Width:");
  pTemp_Float_Input->type( FL_FLOAT_INPUT);
  pTemp_Float_Input->tooltip( "Width of box (X and Y axis)");
  sprintf( TempBuffer, "%.2f", IqeB_ToolsDisp_BoxWidth);
  pTemp_Float_Input->value( TempBuffer);
  pTemp_Float_Input->callback( IqeB_GUI_Float_SetValue_Callback, &IqeB_ToolsDisp_BoxWidth);

  y += yy;

  // update frame height

  pTemp_Box->size( pTemp_Box->w(), y - y1 + 8);

  // finish up

  pMyToolWin->size( pMyToolWin->w(), pTemp_Box->y() + pTemp_Box->h() + 4);  // final size of window

  pMyToolWin->end();
  pMyToolWin->set_non_modal();
  pMyToolWin->callback( close_cb, &pMyToolWin);
  pMyToolWin->show();
}

/********************************** End Of File **********************************/
