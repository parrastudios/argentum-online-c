#!/bin/bash

# script includes
include_path=$(dirname $0)

# define copyright interface path
copyright_interface_path=${include_path}/Interface/Copyright.h

# define header interface path
header_interface_path=${include_path}/Interface/Header.h

# define section interface path
section_interface_path=${include_path}/Interface/Section.h

# define footer interface path
footer_interface_path=${include_path}/Interface/Footer.h

# in{1}: file name
# brief: write the copyright
function module_copyright {

	# get current year
	current_year=$(date +"%Y")

	# set filter properties
	sed_filter="s/<@current_year>/${current_year}/"

	# apply filtering
	sed ${sed_filter} < ${copyright_interface_path} >> ${1}

	# new line
	echo >> ${1}
}

# in{1}: file name
# in{2}: header name in upercase and
#	 separated by underscore
# brief: write the header
function module_header {

	# apply filtering
	sed "s/<@header_name_upercase>/${2}/" < ${header_interface_path} >> ${1}

	# new line
	echo >> ${1}
}

# in{1}: file name
# in{2}: section name
# brief: write a secction
function module_section {

	# apply filtering
	sed "s/<@section_name>/${2}/" < ${section_interface_path} >> ${1}

	# new line
	echo >> ${1}
}

# in{1}: file name
# in{2}: header name in upercase and
#	 separated by underscore
# brief: write the footer
function module_footer {

	# apply filtering
	sed "s/<@header_name_upercase>/${2}/" < ${footer_interface_path} >> ${1}
}
