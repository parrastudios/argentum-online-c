/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/VertexTypes.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct VertexAttrib VERTEX_TYPE_ATTRIBS[] =
{
	"vertPoint",		VERTEX_ATTRIB_FLOAT_3,	OffsetOf(struct VertexType, Position),
	"vertNormal",		VERTEX_ATTRIB_FLOAT_3,	OffsetOf(struct VertexType, Normal),
	"vertTexCoord0",	VERTEX_ATTRIB_FLOAT_2,	OffsetOf(struct VertexType, Coord),
	NULL,				0,						-1  
};

struct VertexAttrib VERTEX_TYPE_TA_ATTRIBS[] =
{
	"vertPoint",		VERTEX_ATTRIB_FLOAT_3,	OffsetOf(struct VertexType, Position), 
	"vertNormal",		VERTEX_ATTRIB_FLOAT_3,	OffsetOf(struct VertexType, Normal), 
	"vertTexCoord0",	VERTEX_ATTRIB_FLOAT_3,	OffsetOf(struct VertexType, CoordTA),
	NULL,				0,						-1
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////
void VertexSetPosition(struct VertexType * Vertex, float x, float y, float z)
{
	Vector3fSet(&Vertex->Position, x, y, z);
}

void VertexSetNormal(struct VertexType * Vertex, float x, float y, float z)
{
	Vector3fSet(&Vertex->Normal, x, y, z);
}

void VertexSetTexture(struct VertexType * Vertex, float x, float y)
{
	Coord2fSet(&Vertex->Coord, x, y);
}

void VertexSetTextureA(struct VertexType * Vertex, float x, float y, float z)
{
	Coord3fSet(&Vertex->CoordTA, x, y, z);
}

void VertexAddToBuffer(struct VertexType * Vertex, struct VertexBuffer * Buffer)
{
	VertexBufferAdd(Buffer, Vertex);
}

struct VertexBuffer * VertexCreateBuffer(uint32 Size, bool Dynamic)
{
	return VertexBufferCreate(sizeof(struct Vector3f) * 2 + sizeof(struct Vector2f), VERTEX_TYPE_ATTRIBS, Size, Dynamic);
}

struct VertexBuffer * VertexCreateBufferA(uint32 Size, bool Dynamic)
{
	return VertexBufferCreate(sizeof(struct Vector3f) * 3, VERTEX_TYPE_TA_ATTRIBS, Size, Dynamic);
}

//void VertexLoadShader(char * Name)
//{
//	display->loadShader(Name, VERTEX_ATTRIBS);
//}

//void VertexLoadShaderA(char * Name)
//{
//	display->loadShader(Name, VERTEX_TA_ATTRIBS);
//}
