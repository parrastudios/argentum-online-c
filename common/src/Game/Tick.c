/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Game/Tick.h>

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct TickManagerType DefaultTickManager;

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Returns the default tick manager
////////////////////////////////////////////////////////////
struct TickManagerType * TickManagerDefault()
{
	return &DefaultTickManager;
}

////////////////////////////////////////////////////////////
/// Updater callback implementation
////////////////////////////////////////////////////////////
void TickManagerUpdaterImpl()
{
	// note: this breaks abstraction (this level should be
	//			moved to timer manager or another superclass
	//			like event)

	TickManagerUpdate(TickManagerDefault());
}

////////////////////////////////////////////////////////////
/// Register callbacks of a task
////////////////////////////////////////////////////////////
void TickTaskRegister(struct TickTaskType * TickTask, TickTaskEvent Init, TickTaskEvent Update, TickTaskEvent Destroy)
{
	// Register callbacks
	TickTask->Initializer = Init;
	TickTask->Updater = Update;
	TickTask->Destroyer = Destroy;
}

////////////////////////////////////////////////////////////
/// Initialize a tick task
////////////////////////////////////////////////////////////
void TickTaskInitialize(struct TickTaskType * TickTask, uint32 Priority, uint32 Interval)
{
	// Set priority and interval
	TickTask->Priority = Priority;
	TickTask->Interval = Interval;
	TickTask->Current = 0;
	
	// Call to initializer
	TickTaskEventCall(TickTask->Initializer);
}

////////////////////////////////////////////////////////////
/// Get tick task context
////////////////////////////////////////////////////////////
TickFuncContext * TickTaskGetContext(struct TickTaskType * TickTask, uint32 EventType)
{
	if (EventType == TICK_TASK_INITIALIZER)
	{
		return &TickTask->Initializer.Context;
	}
	else if (EventType == TICK_TASK_UPDATER)
	{
		return &TickTask->Updater.Context;
	}
	else if (EventType == TICK_TASK_DESTROYER)
	{
		return &TickTask->Destroyer.Context;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Initialize a tick manager
////////////////////////////////////////////////////////////
void TickManagerInitialize(struct TickManagerType * TickManager, float Interval)
{
	// Initialize task list
	TickManager->Tasks = ListNew(sizeof(struct TickTaskType));

	// Initialize attributes
	TickManager->Pause = true;
	TickManager->Interval = Interval;
	TickManager->TimerId = TICK_TIMER_INVALID;

	// Initialize by default (breaks abstraction)
	TickManager->TasksUpdaterImpl = (CallbackType)&TickManagerUpdaterImpl;
}

////////////////////////////////////////////////////////////
/// Add task to a tick manager
////////////////////////////////////////////////////////////
void TickManagerAddTask(struct TickManagerType * TickManager, TickTaskEvent Init, TickTaskEvent Update, TickTaskEvent Destroy, uint32 Priority, uint32 Interval)
{
	// Create the task
	struct TickTaskType TickTask;

	// Register callbacks
	TickTaskRegister(&TickTask, Init, Update, Destroy);

	// Initialize task
	TickTaskInitialize(&TickTask, Priority, Interval);

	// Append task to the list
	ListPushBack(TickManager->Tasks, (void*)&TickTask);
}

////////////////////////////////////////////////////////////
/// Clear all tasks of a tick manager
////////////////////////////////////////////////////////////
void TickManagerClearTask(struct TickManagerType * TickManager)
{
	ListIterator Iterator;

	// Remove from timer manager
	TickManagerStop(TickManager);

	// Call to each task destructor
	ListForEach(TickManager->Tasks, Iterator)
	{
		TickTaskEventCall(ListItData(Iterator, struct TickTaskType).Destroyer);
	}

	// Clear task list
	ListClear(TickManager->Tasks);
}

////////////////////////////////////////////////////////////
/// Start a tick manager
////////////////////////////////////////////////////////////
void TickManagerStart(struct TickManagerType * TickManager)
{
	// Tick manager is not registered on timer manager
	if (TickManager->TimerId == TICK_TIMER_INVALID)
	{
		// Add tick timer to the manager
		TickManager->TimerId = TimerManagerAdd(TickManager->Interval, TickManager->TasksUpdaterImpl, true);

		// Running
		TickManager->Pause = false;
	}
	else
	{
		// If it is registered, restart
		TickManagerRestart(TickManager);
	}
}

////////////////////////////////////////////////////////////
/// Stop a tick manager
////////////////////////////////////////////////////////////
void TickManagerStop(struct TickManagerType * TickManager)
{
	if (!TickManager->Pause)
	{
		// Remove from manager
		TimerManagerRemove(TickManager->TimerId);

		// Set id to invalid
		TickManager->TimerId = TICK_TIMER_INVALID;

		// Stop
		TickManager->Pause = true;
	}
}

////////////////////////////////////////////////////////////
/// Restart a tick manager
////////////////////////////////////////////////////////////
void TickManagerRestart(struct TickManagerType * TickManager)
{
	if (TickManager->TimerId != TICK_TIMER_INVALID && TickManager->Pause)
	{
		// Remove from manager
		TimerManagerRemove(TickManager->TimerId);

		// Add tick timer to the manager
		TickManager->TimerId = TimerManagerAdd(TickManager->Interval, TickManager->TasksUpdaterImpl, true);

		// Running
		TickManager->Pause = false;
	}
}

////////////////////////////////////////////////////////////
/// Update all tasks synchronously of a tick manager
////////////////////////////////////////////////////////////
void TickManagerUpdate(struct TickManagerType * TickManager)
{
	ListIterator Iterator;

	ListForEach(TickManager->Tasks, Iterator)
	{
		struct TickTaskType * TickTaskIt = &ListItData(Iterator, struct TickTaskType);

		if (TickTaskIt->Interval == TICK_TASK_SLEEP)
		{
			// Nothing
		}
		else if (TickTaskIt->Interval == TICK_TASK_REALTIME)
		{
			// Call to updater function
			TickTaskEventCall(TickTaskIt->Updater);
		}
		else
		{
			if (++TickTaskIt->Current == TickTaskIt->Interval)
			{
				// Call to updater function
				TickTaskEventCall(TickTaskIt->Updater);

				// Restart counter
				TickTaskIt->Current = 0;
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Destroy a tick manager
////////////////////////////////////////////////////////////
void TickManagerDestroy(struct TickManagerType * TickManager)
{
	// Clear tick manager
	TickManagerClearTask(TickManager);

	// Destroy task list
	ListDestroy(TickManager->Tasks);
}
