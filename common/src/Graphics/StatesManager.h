/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_STATES_MANAGER_H
#define GRAPHICS_STATES_MANAGER_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define STATES_INVALID_ID				0xFF

#define STATES_BLEND_MODE				0x00
#define		STATES_BLEND_NONE			0x00
#define		STATES_BLEND_NORMAL			0x01
#define		STATES_BLEND_ALPHA			0x02
#define		STATES_BLEND_ADD			0x03
#define		STATES_BLEND_SUBTRACT		0x04
#define		STATES_BLEND_MULTIPLY		0x05
#define		STATES_BLEND_MULTITEXTURE	0x06

#define STATES_POLYGON_MODE				0x01
#define		STATES_POLYGON_WIREFRAME	0x00
#define		STATES_POLYGON_FILL			0x01

#define STATES_COLOR_MODE				0x02
#define		STATES_COLOR_DISABLED		0x00
#define		STATES_COLOR_ENABLED		0x01

#define STATES_DEPTH_MODE				0x03
#define		STATES_DEPTH_DISABLED		0x00
#define		STATES_DEPTH_ENABLED		0x01

#define STATES_LIGHTING_MODE			0x04
#define		STATES_LIGHTING_DISABLED	0x00
#define		STATES_LIGHTING_ENABLED		0x01

#define STATES_TEXTURING_MODE			0x05
#define		STATES_TEXTURING_DISABLED	0x00
#define		STATES_TEXTURING_2D			0x01
#define		STATES_TEXTURING_3D			0x02

#define STATES_DITHER_MODE				0x06
#define		STATES_DITHER_DISABLED		0x00
#define		STATES_DITHER_ENABLED		0x01

#define STATES_CULLFACE_MODE			0x07
#define		STATES_CULLFACE_DISABLED	0x00
#define		STATES_CULLFACE_ENABLED		0x01

#define STATES_MODE_SIZE				0x08
#define STATES_STACK_SIZE				0x40

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct StatesStack
{
	uint8	Data[STATES_MODE_SIZE * STATES_STACK_SIZE];
	uint32	Pointer;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize by default the render states stack
////////////////////////////////////////////////////////////
void StatesManagerInitialize(struct StatesStack * States);

////////////////////////////////////////////////////////////
/// Reset the render states stack
////////////////////////////////////////////////////////////
void StatesManagerReset(struct StatesStack * States);

////////////////////////////////////////////////////////////
/// Save current render states
////////////////////////////////////////////////////////////
void StatesManagerSave(struct StatesStack * States);

////////////////////////////////////////////////////////////
/// Restore previous render states
////////////////////////////////////////////////////////////
void StatesManagerRestore(struct StatesStack * States);

////////////////////////////////////////////////////////////
/// Clears current render states
////////////////////////////////////////////////////////////
void StatesManagerClear(struct StatesStack * States);

////////////////////////////////////////////////////////////
/// Apply current render states
////////////////////////////////////////////////////////////
void StatesManagerApply(struct StatesStack * States);

////////////////////////////////////////////////////////////
/// Set a current rendering state
////////////////////////////////////////////////////////////
void StatesManagerSet(struct StatesStack * States, uint32 State, uint8 Value);

////////////////////////////////////////////////////////////
/// Get a current rendering state
////////////////////////////////////////////////////////////
uint8 StatesManagerGet(struct StatesStack * States, uint32 State);

#endif // GRAPHICS_STATES_MANAGER_H
