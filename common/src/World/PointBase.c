/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Point.h>
#include <System/IOHelper.h> // todo: refactor this

////////////////////////////////////////////////////////////
// Member prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a base component of a point
////////////////////////////////////////////////////////////
struct EntityType * PointBaseCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Set base point name
////////////////////////////////////////////////////////////
void PointBaseSetName(struct EntityType * Entity, char * Name);

////////////////////////////////////////////////////////////
/// Destroy a point base component
////////////////////////////////////////////////////////////
void PointBaseDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the point base implementation attributes
////////////////////////////////////////////////////////////
const void * PointComponentBaseAttr()
{
	static const struct PointComponentBaseType PointComponentBaseAttr = {
		TypeInit(Name, &PointBaseSetName)
	};

	return (const void *)&PointComponentBaseAttr;
}

////////////////////////////////////////////////////////////
/// Return the point base implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * PointComponentBaseImpl()
{
	static const struct ComponentImplType PointComponentBaseTypeImpl = {
		ExprInit(Base, TypeInit(Create, &PointBaseCreate), TypeInit(Destroy, &PointBaseDestroy)),
		TypeInit(Type, COMPONENT_TYPE_BASE),
		UnionInit(Attributes, Derived, &PointComponentBaseAttr)
	};

	return &PointComponentBaseTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a base component of a point
////////////////////////////////////////////////////////////
struct EntityType * PointBaseCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct PointComponentBaseData * BaseData = ComponentDataDerived(Base, struct PointComponentBaseData);

		// Set default base name for point
		sprintf(BaseData->Name, "Point <%d>", Entity->Index);

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Set base point name
////////////////////////////////////////////////////////////
void PointBaseSetName(struct EntityType * Entity, char * Name)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct PointComponentBaseData * BaseData = ComponentDataDerived(Base, struct PointComponentBaseData);

		if (Name && Name[0] != NULLCHAR)
		{
			// Copy predefined name
			strncpy(BaseData->Name, Name, POINT_BASE_NAME_LENGTH);
		}
		else
		{
			// Set default base name for point
			sprintf(BaseData->Name, "Point <%d>", Entity->Index);
		}
	}
}

////////////////////////////////////////////////////////////
/// Destroy a point base component
////////////////////////////////////////////////////////////
void PointBaseDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Obtain base component data reference
		struct ComponentType * Base = EntityComponentBase(Entity);
		struct PointComponentBaseData * BaseData = ComponentDataDerived(Base, struct PointComponentBaseData);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the point base derived data size
////////////////////////////////////////////////////////////
uinteger PointComponentBaseSize()
{
    return sizeof(struct PointComponentBaseData);
}
