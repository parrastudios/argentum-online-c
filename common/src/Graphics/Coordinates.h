/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_COORDINATES_H
#define GRAPHICS_COORDINATES_H

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct Coord2f
{
	union
	{
		struct
		{
			float u, v;
		};

		float uv[2];
	};
};

struct Coord3f
{
	union
	{
		struct
		{
			float u, v, w;
		};

		float uvw[2];
	};
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

void Coord2fSet(struct Coord2f * Coord, float u, float v);

void Coord2fMult(struct Coord2f * Coord, float Value);

void Coord2fDiv(struct Coord2f * Coord, float Value);

void Coord2fAdd(struct Coord2f * Left, struct Coord2f * Right);

void Coord2fSubEx(struct Coord2f * Dest, struct Coord2f * Left, struct Coord2f * Right);

void Coord3fSet(struct Coord3f * Coord, float u, float v, float w);

void Coord3fMult(struct Coord3f * Coord, float Value);

void Coord3fDiv(struct Coord3f * Coord, float Value);

void Coord3fAdd(struct Coord3f * Left, struct Coord3f * Right);

#endif // GRAPHICS_COORDINATES_H
