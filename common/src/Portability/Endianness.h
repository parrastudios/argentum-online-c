/////////////////////////////////////////////////////////////////////////////
//	Portability Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A library designed to give support for creating cross-platform software,
//	taking into account multiple architectures, operative systems, compilers,
//	and standards, providing an abstraction layer over the low level when
//	developing portable software.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PORTABILITY_ENDIANNESS_H
#define PORTABILITY_ENDIANNESS_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Architecture.h>
#include <Portability/Type.h>
#include <Preprocessor/Concatenation.h>
#include <Preprocessor/Serial.h>

#if (defined(PLATFORM_FAMILY_UNIX) || defined(PLATFORM_FAMILY_BSD))
#	include <sys/param.h>
#elif (COMPIER_TYPE == COMPILER_GNUC)
#	include <endian.h>
#endif

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define ENDIAN_UNKNOWN				UINT32_MIN_RANGE			//< Unknown endianness
#define ENDIAN_LITTLE				UINT32_SUFFIX(0xDDCCBBAA)	//< Little-endian
#define ENDIAN_BIG					UINT32_SUFFIX(0xAABBCCDD)	//< Big-endian
#define ENDIAN_BIG_WORD				UINT32_SUFFIX(0xCCDDAABB)	//< Middle-endian, Honeywell 316 style
#define ENDIAN_LITTLE_WORD			UINT32_SUFFIX(0xBBAADDCC)	//< Middle-endian, PDP-11 style
#define ENDIAN_UNCHECKED			UINT32_MAX_RANGE			//< Not possible to check endianness in compile-time

#define ENDIAN_TYPE					ENDIAN_UNCHECKED

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////
typedef uint32 EndianType;

////////////////////////////////////////////////////////////
// Detection
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Identify the architecture endianess
////////////////////////////////////////////////////////////
#if (defined(alpha) || defined(__ALPHA) || defined(__alpha) || defined(__alpha__) || defined(_M_ALPHA)) || \
	(defined(__386__) || defined(i386) || defined(__i386) || defined(__i386__) || defined(__X86) || defined(_M_IX86)) || \
	((defined(_IA64) || defined(__IA64__) || defined(__ia64__) || defined(__ia64) || defined(_M_IA64)) && !(defined(hpux) || defined(_hpux))) || \
	(defined(__x86_64__) || defined(_M_X64) || defined(_M_AMD64) || defined(__amd64)) || \
	((defined(__mips__) || defined(__mips) || defined(__MIPS__) || defined(_M_MRX000)) && (defined(_MIPSEL) || defined(__MIPSEL) || defined(__MIPSEL__))) || \
	((defined(__arm__) || defined(__arm) || defined(ARM) || defined(_ARM) || defined(_ARM_) || defined(__ARM__) || defined(_M_ARM)) && (defined(__ARMEL__))) || \
	((defined(__sh__) || defined(__sh) || defined(SHx) || defined(_SHX_)) && \
	(defined(__LITTLE_ENDIAN__) || (PLATFORM_TYPE == PLATFORM_WINDOWS_CE))) || \
	((defined(nios2) || defined(__nios2) || defined(__nios2__)) && \
	(defined(__nios2_little_endian) || defined(nios2_little_endian) || defined(__nios2_little_endian__))) || \
	(defined(__THUMBEL__) || defined(__AARCH64EL__)) || \
	(defined(__BYTE_ORDER__) && (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)) || \
	(defined(__FLOAT_WORD_ORDER__) && (__FLOAT_WORD_ORDER__ == __ORDER_LITTLE_ENDIAN__)) || \
	(defined(vax)) || \
	(defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(_WIN32_WCE) || defined(__NT__))

#	if (defined(ENDIAN_TYPE) && ENDIAN_TYPE == ENDIAN_UNCHECKED)
#		undef ENDIAN_TYPE
#		define ENDIAN_TYPE ENDIAN_LITTLE
#	endif

#elif ((defined(_IA64) || defined(__IA64__) || defined(__ia64__) || defined(__ia64) || defined(_M_IA64)) && (defined(hpux) || defined(_hpux))) || \
	((defined(__mips__) || defined(__mips) || defined(__MIPS__) || defined(_M_MRX000)) && (defined(_MIPSEB) || defined(__MIPSEB) || defined(__MIPSEB__))) || \
	(defined(__hppa) || defined(__hppa__)) || \
	(defined(__PPC) || defined(__POWERPC__) || defined(__powerpc) || defined(__powerpc64__) || defined(__PPC__) || \
	defined(__powerpc__) || defined(__ppc__) || defined(__ppc) || defined(_ARCH_PPC) || defined(_M_PPC)) || \
	(defined(_POWER) || defined(_ARCH_PWR) || defined(_ARCH_PWR2) || defined(_ARCH_PWR3) || defined(_ARCH_PWR4) || defined(__THW_RS6000)) || \
	(defined(__sparc__) || defined(__sparc) || defined(sparc)) || \
	((defined(__arm__) || defined(__arm) || defined(ARM) || defined(_ARM) || defined(_ARM_) || defined(__ARM__) || defined(_M_ARM)) && (defined(__ARMEB__))) || \
	(defined(__m68k__) || defined(mc68000) || defined(_M_M68K)) || \
	(defined(__s390__) || defined(__s390x__)) || \
	((defined(__sh__) || defined(__sh) || defined(SHx) || defined(_SHX_)) && (defined(__BIG_ENDIAN__))) || \
	((defined(nios2) || defined(__nios2) || defined(__nios2__)) && \
	(defined(__nios2_big_endian) || defined(nios2_big_endian) || defined(__nios2_big_endian__))) || \
	(defined(__THUMBEB__) || defined(__AARCH64EB__)) || \
	(defined(__BYTE_ORDER__) && (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)) || \
	(defined(__FLOAT_WORD_ORDER__) && (__FLOAT_WORD_ORDER__ == __ORDER_BIG_ENDIAN__))

#	if defined(ENDIAN_TYPE) && (ENDIAN_TYPE == ENDIAN_UNCHECKED)
#		undef ENDIAN_TYPE
#		define ENDIAN_TYPE ENDIAN_BIG
#	endif

#elif (defined(PLATFORM_FAMILY_UNIX) || defined(PLATFORM_FAMILY_BSD) || (COMPIER_TYPE == COMPILER_GNUC))
#	if defined(__BYTE_ORDER)
#		if defined(ENDIAN_TYPE) && (ENDIAN_TYPE == ENDIAN_UNCHECKED)
#			undef ENDIAN_TYPE
#			if __BYTE_ORDER == __BIG_ENDIAN
#				define ENDIAN_TYPE ENDIAN_BIG
#			elif __BYTE_ORDER == __LITTLE_ENDIAN
#				define ENDIAN_TYPE ENDIAN_LITTLE
#			elif __BYTE_ORDER == __PDP_ENDIAN
#				define ENDIAN_TYPE ENDIAN_LITTLE_WORD
#			else
#				define ENDIAN_TYPE ENDIAN_UNKNOWN
#			endif
#		endif
#	else
#		if defined(ENDIAN_TYPE) && (ENDIAN_TYPE == ENDIAN_UNCHECKED)
#			undef ENDIAN_TYPE
#			define ENDIAN_TYPE ENDIAN_UNKNOWN
#		endif
#	endif

#endif

#if ENDIAN_TYPE == ENDIAN_UNCHECKED
#	warning The endianness cannot be determined at compile-time
#elif ENDIAN_TYPE == ENDIAN_UNKNOWN
#	error This endianness is not supported
#endif

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Definitions for type deducing in endian conversion
////////////////////////////////////////////////////////////
#define ENDIANNESS_TYPE_IMPL_int16		Int16
#define ENDIANNESS_TYPE_IMPL_uint16		UInt16
#define ENDIANNESS_TYPE_IMPL_int32		Int32
#define ENDIANNESS_TYPE_IMPL_uint32		UInt32
#define ENDIANNESS_TYPE_IMPL_int64		Int64
#define ENDIANNESS_TYPE_IMPL_uint64		UInt64
#define ENDIANNESS_TYPE_IMPL_float32	Float32
#define ENDIANNESS_TYPE_IMPL_float64	Float64

#define ENDIANNESS_TYPE_IMPL(Type)		PREPROCESSOR_CONCAT(ENDIANNESS_TYPE_IMPL_, Type)

////////////////////////////////////////////////////////////
///	Convert to host endianness by type
////////////////////////////////////////////////////////////
#define EndiannessToHostType(Type, Value, FromEndian) \
	PREPROCESSOR_CONCAT(EndiannessToHost, ENDIANNESS_TYPE_IMPL(Type))(Value, FromEndian)

////////////////////////////////////////////////////////////
///	Convert from host endianness by type
////////////////////////////////////////////////////////////
#define EndiannessFromHostType(Type, Value, FromEndian) \
	PREPROCESSOR_CONCAT(EndiannessFromHost, ENDIANNESS_TYPE_IMPL(Type))(Value, ToEndian)

////////////////////////////////////////////////////////////
/// Foreach element in array convert them from any to host
////////////////////////////////////////////////////////////
#define EndiannessForEachToHost(Type, Array, Size, FromEndian) \
	do \
	{ \
		uinteger PREPROCESSOR_SERIAL_ID(i); \
		\
		for (PREPROCESSOR_SERIAL_ID(i) = 0; PREPROCESSOR_SERIAL_ID(i) < (Size); ++PREPROCESSOR_SERIAL_ID(i)) \
		{ \
			(Array)[PREPROCESSOR_SERIAL_ID(i)] = EndiannessToHostType(Type, (Array)[PREPROCESSOR_SERIAL_ID(i)], FromEndian); \
		} \
	} while (0)

////////////////////////////////////////////////////////////
/// Foreach element in array convert them from host to any
////////////////////////////////////////////////////////////
#define EndiannessForEachFromHost(Type, Array, Size, ToEndian) \
	do \
	{ \
		uinteger PREPROCESSOR_SERIAL_ID(i); \
		\
		for (PREPROCESSOR_SERIAL_ID(i) = 0; PREPROCESSOR_SERIAL_ID(i) < (Size); ++PREPROCESSOR_SERIAL_ID(i)) \
		{ \
			(Array)[PREPROCESSOR_SERIAL_ID(i)] = EndiannessFromHostType(Type, (Array)[PREPROCESSOR_SERIAL_ID(i)], ToEndian); \
		} \
	} while (0)

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Retreive the endianness type in run-time
////////////////////////////////////////////////////////////
EndianType EndiannessGetType();

////////////////////////////////////////////////////////////
/// 16-bit signed integer from specified endianness to host
////////////////////////////////////////////////////////////
int16 EndiannessToHostInt16(int16 Value, EndianType From);

////////////////////////////////////////////////////////////
/// 16-bit unsigned integer from specified endianness to host
////////////////////////////////////////////////////////////
uint16 EndiannessToHostUInt16(uint16 Value, EndianType From);

////////////////////////////////////////////////////////////
/// 32-bit signed integer from specified endianness to host
////////////////////////////////////////////////////////////
int32 EndiannessToHostInt32(int32 Value, EndianType From);

////////////////////////////////////////////////////////////
/// 32-bit unsigned integer from specified endianness to host
////////////////////////////////////////////////////////////
uint32 EndiannessToHostUInt32(uint32 Value, EndianType From);

////////////////////////////////////////////////////////////
/// 64-bit signed integer from specified endianness to host
////////////////////////////////////////////////////////////
int64 EndiannessToHostInt64(int64 Value, EndianType From);

////////////////////////////////////////////////////////////
/// 64-bit unsigned integer from specified endianness to host
////////////////////////////////////////////////////////////
uint64 EndiannessToHostUInt64(uint64 Value, EndianType From);

////////////////////////////////////////////////////////////
/// 32-bit floating point from specified endianness to host
////////////////////////////////////////////////////////////
float32 EndiannessToHostFloat32(float32 Value, EndianType From);

////////////////////////////////////////////////////////////
/// 64-bit floating point from specified endianness to host
////////////////////////////////////////////////////////////
float64 EndiannessToHostFloat64(float64 Value, EndianType From);

////////////////////////////////////////////////////////////
/// 16-bit signed integer from host to specified endianness
////////////////////////////////////////////////////////////
int16 EndiannessFromHostInt16(int16 Value, EndianType To);

////////////////////////////////////////////////////////////
/// 16-bit unsigned integer from host to specified endianness
////////////////////////////////////////////////////////////
uint16 EndiannessFromHostUInt16(uint16 Value, EndianType To);

////////////////////////////////////////////////////////////
/// 32-bit signed integer from host to specified endianness
////////////////////////////////////////////////////////////
int32 EndiannessFromHostInt32(int32 Value, EndianType To);

////////////////////////////////////////////////////////////
/// 32-bit unsigned integer from host to specified endianness
////////////////////////////////////////////////////////////
uint32 EndiannessFromHostUInt32(uint32 Value, EndianType To);

////////////////////////////////////////////////////////////
/// 64-bit signed integer from host to specified endianness
////////////////////////////////////////////////////////////
int64 EndiannessFromHostInt64(int64 Value, EndianType To);

////////////////////////////////////////////////////////////
/// 64-bit unsigned integer from host to specified endianness
////////////////////////////////////////////////////////////
uint64 EndiannessFromHostUInt64(uint64 Value, EndianType To);

////////////////////////////////////////////////////////////
/// 32-bit floating point from host to specified endianness
////////////////////////////////////////////////////////////
float32 EndiannessFromHostFloat32(float32 Value, EndianType To);

////////////////////////////////////////////////////////////
/// 64-bit floating point from host to specified endianness
////////////////////////////////////////////////////////////
float64 EndiannessFromHostFloat64(float64 Value, EndianType To);

#endif // PORTABILITY_ENDIANNESS_H
