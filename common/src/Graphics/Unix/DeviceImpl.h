/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

//#ifndef GRAPHICS_WIN32_DEVICEIMPL_H
//#define GRAPHICS_WIN32_DEVICEIMPL_H



//typedef bool (APIENTRY * PFNWGLSWAPINTERVALEXTPROC)(int32 Interval);
//PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT;

//typedef bool (WINAPI * PFNWGLCHOOSEPIXELFORMATARBPROC) (HDC hdc, const int32 * piAttribIList, const float * pfAttribFList, uint32 nMaxFormats, uint32 * piFormats, uint32 * nNumFormats);
//PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB;

/*
////////////////////////////////////////////////////////////
// VBOs extension
////////////////////////////////////////////////////////////
extern PFNGLGENBUFFERSARBPROC			pglGenBuffersARB;			// VBO Name Generation Procedure
extern PFNGLBINDBUFFERARBPROC			pglBindBufferARB;			// VBO Bind Procedure
extern PFNGLBUFFERDATAARBPROC			pglBufferDataARB;			// VBO Data Loading Procedure
extern PFNGLBUFFERSUBDATAARBPROC		pglBufferSubDataARB;		// VBO Sub Data Loading Procedure
extern PFNGLDELETEBUFFERSARBPROC		pglDeleteBuffersARB;		// VBO Deletion Procedure
extern PFNGLGETBUFFERPARAMETERIVARBPROC pglGetBufferParameterivARB; // return various parameters of VBO
extern PFNGLMAPBUFFERARBPROC			pglMapBufferARB;			// map VBO procedure
extern PFNGLUNMAPBUFFERARBPROC			pglUnmapBufferARB;			// unmap VBO procedure

#define glGenBuffersARB					pglGenBuffersARB
#define glBindBufferARB					pglBindBufferARB
#define glBufferDataARB					pglBufferDataARB
#define glBufferSubDataARB				pglBufferSubDataARB
#define glDeleteBuffersARB				pglDeleteBuffersARB
#define glGetBufferParameterivARB		pglGetBufferParameterivARB
#define glMapBufferARB					pglMapBufferARB
#define glUnmapBufferARB				pglUnmapBufferARB

////////////////////////////////////////////////////////////
// Shaders extensions
////////////////////////////////////////////////////////////
extern PFNGLCREATEPROGRAMOBJECTARBPROC		pglCreateProgramObjectARB;
extern PFNGLDELETEOBJECTARBPROC				pglDeleteObjectARB;
extern PFNGLUSEPROGRAMOBJECTARBPROC			pglUseProgramObjectARB;
extern PFNGLCREATESHADEROBJECTARBPROC		pglCreateShaderObjectARB;
extern PFNGLSHADERSOURCEARBPROC				pglShaderSourceARB;
extern PFNGLCOMPILESHADERARBPROC			pglCompileShaderARB;
extern PFNGLGETOBJECTPARAMETERIVARBPROC		pglGetObjectParameterivARB;
extern PFNGLATTACHOBJECTARBPROC				pglAttachObjectARB;
extern PFNGLGETINFOLOGARBPROC				pglGetInfoLogARB;
extern PFNGLLINKPROGRAMARBPROC				pglLinkProgramARB;
extern PFNGLGETUNIFORMLOCATIONARBPROC		pglGetUniformLocationARB;
extern PFNGLUNIFORM4FARBPROC				pglUniform4fARB;
extern PFNGLUNIFORM3FARBPROC				pglUniform3fARB;   
extern PFNGLUNIFORM2FARBPROC				pglUniform2fARB;   
extern PFNGLUNIFORM1FARBPROC				pglUniform1fARB;
extern PFNGLUNIFORM1IARBPROC				pglUniform1iARB;
extern PFNGLGETATTRIBLOCATIONARBPROC		pglGetAttribLocationARB;
extern PFNGLVERTEXATTRIB3FARBPROC			pglVertexAttrib3fARB;
extern PFNGLVERTEXATTRIBPOINTERARBPROC		pglVertexAttribPointerARB;
extern PFNGLENABLEVERTEXATTRIBARRAYARBPROC	pglEnableVertexAttribArrayARB;
extern PFNGLDISABLEVERTEXATTRIBARRAYARBPROC pglDisableVertexAttribArrayARB;
extern PFNGLACTIVETEXTUREARBPROC			pglActiveTextureARB;

#define glCreateProgramObjectARB			pglCreateProgramObjectARB
#define glDeleteObjectARB					pglDeleteObjectARB
#define glUseProgramObjectARB				pglUseProgramObjectARB
#define glCreateShaderObjectARB				pglCreateShaderObjectARB
#define glShaderSourceARB					pglShaderSourceARB
#define glCompileShaderARB					pglCompileShaderARB
#define glGetObjectParameterivARB			pglGetObjectParameterivARB  
#define glAttachObjectARB					pglAttachObjectARB
#define glGetInfoLogARB						pglGetInfoLogARB 
#define glLinkProgramARB					pglLinkProgramARB 
#define glGetUniformLocationARB				pglGetUniformLocationARB 
#define glUniform4fARB						pglUniform4fARB  
#define glUniform3fARB						pglUniform3fARB   
#define glUniform2fARB						pglUniform2fARB 
#define glUniform1fARB						pglUniform1fARB 
#define glUniform1iARB						pglUniform1iARB
#define glGetAttribLocationARB				pglGetAttribLocationARB
#define glVertexAttrib3fARB					pglVertexAttrib3fARB
#define glVertexAttribPointerARB			pglVertexAttribPointerARB
#define glEnableVertexAttribArrayARB		pglEnableVertexAttribArrayARB
#define glDisableVertexAttribArrayARB		pglDisableVertexAttribArrayARB
#define glActiveTextureARB					pglActiveTextureARB

////////////////////////////////////////////////////////////
// FBO and PBO extensions
////////////////////////////////////////////////////////////
extern PFNGLGENRENDERBUFFERSEXTPROC			pglGenRenderbuffersEXT;
extern PFNGLBINDRENDERBUFFEREXTPROC			pglBindRenderbufferEXT;
extern PFNGLRENDERBUFFERSTORAGEEXTPROC		pglRenderbufferStorageEXT;
extern PFNGLGENFRAMEBUFFERSEXTPROC			pglGenFramebuffersEXT;
extern PFNGLBINDFRAMEBUFFEREXTPROC			pglBindFramebufferEXT;
extern PFNGLFRAMEBUFFERTEXTURE2DEXTPROC		pglFramebufferTexture2DEXT;
extern PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC	pglFramebufferRenderbufferEXT;
extern PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC	pglCheckFramebufferStatusEXT;

#define glGenRenderbuffersEXT				pglGenRenderbuffersEXT 
#define glBindRenderbufferEXT				pglBindRenderbufferEXT 
#define glRenderbufferStorageEXT			pglRenderbufferStorageEXT 
#define glGenFramebuffersEXT				pglGenFramebuffersEXT 
#define glBindFramebufferEXT				pglBindFramebufferEXT 
#define glFramebufferTexture2DEXT			pglFramebufferTexture2DEXT 
#define glFramebufferRenderbufferEXT		pglFramebufferRenderbufferEXT
#define glCheckFramebufferStatusEXT			pglCheckFramebufferStatusEXT

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////
bool glCreateExtensions();
*/

//#endif // GRAPHICS_WIN32_DEVICEIMPL_H
