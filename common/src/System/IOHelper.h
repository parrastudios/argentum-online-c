/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_IO_HELPER_H
#define SYSTEM_IO_HELPER_H

// todo: this must be removed, new module IO implements this and much more functionalities

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#if PLATFORM_TYPE == PLATFORM_LINUX || PLATFORM_TYPE == PLATFORM_BSD

	#include <unistd.h>

#endif

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
typedef enum
{
	IO_TYPE_FILE	= 1 << 0,
	IO_TYPE_MEMORY	= 1 << 1,
	IO_TYPE_NETWORK = 1 << 2,

	IO_TYPE_UNKNOWN	= 0xFFFFFFFF
} IOType;

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct ResourceFileHeader
{
	uint32	Version;			//< Resource version
	char	Description[0xFF];	//< Resource description
};

////////////////////////////////////////////////////////////
// Open helper method
////////////////////////////////////////////////////////////
bool IOHelperOpen(FILE** pFile, const char * filename, const char * mode);

#define fopen       IOHelperOpen

////////////////////////////////////////////////////////////
// Read field helper method (todo: remove this)
////////////////////////////////////////////////////////////
void IOHelperReadField(char * Destination, char * Source, char Separator, uint32 Position);

#define sreadfield	IOHelperReadField

uint32 IOHelperReadFieldEx(char * Destination, uint32 DestSize, char * Source, uint32 SrcSize, char Separator, uint32 Position);

#define snreadfield IOHelperReadFieldEx

////////////////////////////////////////////////////////////
// Function defines
////////////////////////////////////////////////////////////
#if PLATFORM_TYPE == PLATFORM_WINDOWS_NT && COMPILER_TYPE == COMPILER_MSVC

    ////////////////////////////////////////////////////////////
    // String print helper method
    ////////////////////////////////////////////////////////////
    void IOHelperStrPrint(char * str, const char * format, ...);

    ////////////////////////////////////////////////////////////
    // String copy helper method
    ////////////////////////////////////////////////////////////
    void IOHelperStrCpy(char * destination, const char * source);

    ////////////////////////////////////////////////////////////
    // Sscan helper method
    ////////////////////////////////////////////////////////////
    int32 IOHelperSscan(const char * str, const char * format, ...);

	#define printf		printf_s
	#define sprintf		IOHelperStrPrint
	#define strcpy		IOHelperStrCpy
	#define sscanf		IOHelperSscan
	#define exvsnprintf	IOHelperVsprintf

	#undef strdup
	#define strdup		_strdup

#endif

#endif // SYSTEM_IO_HELPER_H
