/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <DataBase/MySQL/General.h>
#include <DataBase/StoredProcedure.h>
#include <DataBase/Query.h>
#include <DataBase/Default.h>

#include <Memory/General.h>

#include <System/IOHelper.h> // todo: remove this

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define STORED_PROCEDURE_PROTOTYPE_SIZE		UINTEGER_SUFFIX(0x03FF)

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct StoredProcedureImplType
{
	MYSQL_STMT * Statement;
	MYSQL_BIND * Parameters;
};

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
/// Returns the implementation MySQL procedure type id
/////////////////////////////////////////////////////////////
enum enum_field_types StoredProcedureMySQLBufferType(enum StoredProcedureArgumentTypeId TypeId)
{
	static enum enum_field_types FieldTypesMapping[STORED_PROCEDURE_COUNT] =
	{
		ArrayInit(STORED_PROCEDURE_TINY, MYSQL_TYPE_TINY),
		ArrayInit(STORED_PROCEDURE_SHORT, MYSQL_TYPE_SHORT),
		ArrayInit(STORED_PROCEDURE_LONG, MYSQL_TYPE_LONG),
		ArrayInit(STORED_PROCEDURE_LONG_LONG, MYSQL_TYPE_LONGLONG),
		ArrayInit(STORED_PROCEDURE_FLOAT, MYSQL_TYPE_FLOAT),
		ArrayInit(STORED_PROCEDURE_DOUBLE, MYSQL_TYPE_DOUBLE),
		ArrayInit(STORED_PROCEDURE_DATE, MYSQL_TYPE_DATE),
		ArrayInit(STORED_PROCEDURE_TIME, MYSQL_TYPE_TIME),
		ArrayInit(STORED_PROCEDURE_STRING, MYSQL_TYPE_STRING),
		ArrayInit(STORED_PROCEDURE_BLOB, MYSQL_TYPE_BLOB),
		ArrayInit(STORED_PROCEDURE_NULL, MYSQL_TYPE_NULL)
	};

	if (TypeId < STORED_PROCEDURE_COUNT)
	{
		return FieldTypesMapping[TypeId];
	}

	return MYSQL_TYPE_NULL;
}

/////////////////////////////////////////////////////////////
/// Returns the procedure type id from MySQL implementation
/////////////////////////////////////////////////////////////
enum StoredProcedureArgumentTypeId StoredProcedureMySQLDataType(enum enum_field_types TypeId)
{
	// MySQL has no portable type enumeration, so it's not possible to implement
	// easily a lookup table, instead of that we leave the compiler to
	// optimize the switch into a lookup table in the compilation stage

	switch (TypeId)
	{
		case MYSQL_TYPE_TINY :
			return STORED_PROCEDURE_TINY;

		case MYSQL_TYPE_SHORT :
			return STORED_PROCEDURE_SHORT;

		case MYSQL_TYPE_LONG :
			return STORED_PROCEDURE_LONG;

		case MYSQL_TYPE_LONGLONG :
			return STORED_PROCEDURE_LONG_LONG;

		case MYSQL_TYPE_FLOAT :
			return STORED_PROCEDURE_FLOAT;

		case MYSQL_TYPE_DOUBLE :
			return STORED_PROCEDURE_DOUBLE;

		case MYSQL_TYPE_DATE :
			return STORED_PROCEDURE_DATE;

		case MYSQL_TYPE_TIME :
			return STORED_PROCEDURE_TIME;

		case MYSQL_TYPE_VARCHAR :
		case MYSQL_TYPE_STRING :
		case MYSQL_TYPE_VAR_STRING :
			return STORED_PROCEDURE_STRING;

		case MYSQL_TYPE_BLOB :
			return STORED_PROCEDURE_BLOB;

		case MYSQL_TYPE_NULL :
			return STORED_PROCEDURE_NULL;

        default :
			return STORED_PROCEDURE_NULL;
	}

	return STORED_PROCEDURE_NULL;
}

/////////////////////////////////////////////////////////////
/// Generates the implementation MySQL procedure call args
/////////////////////////////////////////////////////////////
void StoredProcedureMySQLGeneratePrototypeArgs(uinteger ArgCount, char * PrototypeArgs, uinteger PrototypeSize)
{
	if (PrototypeArgs && PrototypeSize > 0)
	{
		if (ArgCount > 0)
		{
			uinteger Arg, Iterator;

			for (Arg = 0, Iterator = 0; Arg < ArgCount && Iterator < PrototypeSize; ++Arg, Iterator += 3)
			{
				PrototypeArgs[Iterator] = '?';
				PrototypeArgs[Iterator + 1] = ',';
				PrototypeArgs[Iterator + 2] = ' ';
			}

			PrototypeArgs[Iterator - 2] = NULLCHAR;
		}
		else
		{
			PrototypeArgs[0] = NULLCHAR;
		}
	}
}

/////////////////////////////////////////////////////////////
/// Generates the implementation MySQL procedure call proto
/////////////////////////////////////////////////////////////
void StoredProcedureMySQLGeneratePrototypeCall(const char * Name, uinteger ArgCount, char * Prototype, uinteger PrototypeSize, uinteger * PrototypeLength)
{
	if (Name && Prototype && PrototypeLength)
	{
		char PrototypeArguments[STORED_PROCEDURE_PROTOTYPE_SIZE];

		// Generate prototype arguments
		StoredProcedureMySQLGeneratePrototypeArgs(ArgCount, PrototypeArguments, PrototypeSize);

		// Format the prototype
		if(ArgCount > 0)
		{
			sprintf(Prototype, "CALL %s(%s)", Name, PrototypeArguments);
		}
		else
		{
			sprintf(Prototype, "CALL %s()", Name);
		}

		// Retreive the prototype length
		*PrototypeLength = strlen(Prototype);
	}
}

/////////////////////////////////////////////////////////////
/// Initialize the implementation MySQL procedure call args
/////////////////////////////////////////////////////////////
void StoredProcedureInitializeMySQLArgs(MYSQL_BIND * Parameters, uinteger ParameterSize)
{
	if (Parameters)
	{
		MemorySet(Parameters, 0, sizeof(MYSQL_BIND) * ParameterSize);
	}
}

/////////////////////////////////////////////////////////////
/// Prints implementation MySQL procedure call error if any
/////////////////////////////////////////////////////////////
void StoredProcedureMySQLError(MYSQL_STMT * Statement, const char * Tag)
{
	if (Statement && Tag)
	{
		MessageError("StoredProcedure",
			"Error produced in %s - Description: %s [ID: %d]",
			Tag, mysql_stmt_error(Statement), mysql_stmt_errno(Statement));
	}
}

/////////////////////////////////////////////////////////////
/// Create a procedure call to the database
/////////////////////////////////////////////////////////////
bool StoredProcedureCreate(const char * Name, const char * Prototype, const char * Body)
{
	QueryResultType Result;

	// Create procedure call by its prototype and body
	if (Query(&Result,
		"CREATE PROCEDURE %s(%s) "
		"BEGIN "
		"%s "
		"END",
		Name, Prototype, Body))
	{
		// Release the query
		QueryRelease(Result);

		return true;
	}

	return false;
}

/////////////////////////////////////////////////////////////
/// Destroy a procedure call from the database
/////////////////////////////////////////////////////////////
bool StoredProcedureDestroy(const char * Name)
{
	QueryResultType Result;

	// Drop procedure call by name
	if (Query(&Result, "DROP PROCEDURE IF EXISTS %s", Name))
	{
		// Release the query
		QueryRelease(Result);

		return true;
	}

	return false;
}

/////////////////////////////////////////////////////////////
/// Prepares a procedure call to the database
/////////////////////////////////////////////////////////////
bool StoredProcedurePrepare(struct StoredProcedureType * StoredProcedure)
{
	if (StoredProcedure && StoredProcedure->Impl && StoredProcedure->Impl->Statement)
	{
		char Prototype[STORED_PROCEDURE_PROTOTYPE_SIZE];
		uinteger PrototypeLength;

		// Generate prototype name
		StoredProcedureMySQLGeneratePrototypeCall(
			StoredProcedure->Name,
			StoredProcedureArgumentSize(StoredProcedure->Prototype),
			Prototype,
			STORED_PROCEDURE_PROTOTYPE_SIZE,
			&PrototypeLength);

		// Prepare the MySQL stored procedure call
		if (PrototypeLength > 0)
		{
			unsigned long Length = (unsigned long)PrototypeLength;

			if (mysql_stmt_prepare(StoredProcedure->Impl->Statement, Prototype, Length))
			{
				// Show error
				StoredProcedureMySQLError(StoredProcedure->Impl->Statement, "StoredProcedurePrepare");

				return false;
			}

			return true;
		}
	}

	return false;
}

/////////////////////////////////////////////////////////////
/// Initialize and prepare a stored procedure call
/////////////////////////////////////////////////////////////
bool StoredProcedureInitialize(struct StoredProcedureType * StoredProcedure, const char * Name, uinteger ArgumentCount, StoredProcedureFetchCallback Fetch)
{
	struct DataBase * DB = DataBaseGetDefault();

	if (DB && DB->Connection)
	{
		MYSQL_STMT * Statement = mysql_stmt_init((MYSQL *)DB->Connection);

		// Initialize stored procedure properties
		StoredProcedure->Name = Name;
		StoredProcedure->Fetch = Fetch;

		// Initialize argument list
		StoredProcedure->Prototype = StoredProcedureArgumentCreate(STORED_PROCEDURE_BY_REFERENCE, ArgumentCount);

		// Initialize result set
		StoredProcedure->Result = VectorNewT(StoredProcedureArgumentList);

		if (Statement)
		{
			// Create stored procedure implementation
			StoredProcedure->Impl = (struct StoredProcedureImplType *)MemoryAllocate(sizeof(struct StoredProcedureImplType));

			if (StoredProcedure->Impl)
			{
				uinteger ArgsSize = StoredProcedureArgumentSize(StoredProcedure->Prototype);

				// Initialize implementation
				StoredProcedure->Impl->Statement = Statement;

				// Initialize implementation arguments
				StoredProcedure->Impl->Parameters = NULL;

				if (ArgsSize > UINTEGER_SUFFIX(0))
				{
					StoredProcedure->Impl->Parameters = (MYSQL_BIND *)MemoryAllocate(sizeof(MYSQL_BIND) * ArgsSize);

					if (StoredProcedure->Impl->Parameters)
					{
						// Initialize arguments
						StoredProcedureInitializeMySQLArgs(StoredProcedure->Impl->Parameters, ArgsSize);
					}
				}

				return true;
			}
			else
			{
				// Clear stored procedure
				mysql_stmt_close(Statement);
			}
		}
	}

	return false;
}

/////////////////////////////////////////////////////////////
/// Binds parameters to a procedure call
/////////////////////////////////////////////////////////////
bool StoredProcedureBind(struct StoredProcedureType * StoredProcedure)
{
	if (StoredProcedure && StoredProcedure->Impl && StoredProcedure->Impl->Statement && StoredProcedure->Prototype)
	{
		if (StoredProcedureArgumentSize(StoredProcedure->Prototype) > UINTEGER_SUFFIX(0))
		{
			uinteger Parameter;

			// Convert parameters to MySQL implementation
			for (Parameter = 0; Parameter < StoredProcedureArgumentSize(StoredProcedure->Prototype); ++Parameter)
			{
				enum StoredProcedureArgumentTypeId Type = StoredProcedureArgumentListGetType(StoredProcedure->Prototype, Parameter);
				StoredProcedureArgumentDataRefType Reference = StoredProcedureArgumentGetRef(StoredProcedure->Prototype, Parameter, Type);

				if (Type < STORED_PROCEDURE_COUNT && Reference != NULL)
				{
					StoredProcedure->Impl->Parameters[Parameter].buffer_type = StoredProcedureMySQLBufferType(Type);
					StoredProcedure->Impl->Parameters[Parameter].buffer = (void *)Reference;

					// TODO: REVIEW THIS!!!
					// TODO: make a function for retrieving sizes?
					StoredProcedure->Impl->Parameters[Parameter].buffer_length = (Type == STORED_PROCEDURE_STRING) ? STORED_PROCEDURE_ARGUMENT_STRING_SIZE : UINTEGER_SUFFIX(0);
				}
			}

			// Prepare stored procedure
			if (StoredProcedurePrepare(StoredProcedure))
			{
				// Bind MySQL implementation parameters
				if (mysql_stmt_bind_param(StoredProcedure->Impl->Statement, StoredProcedure->Impl->Parameters))
				{
					// Show error if any
					StoredProcedureMySQLError(StoredProcedure->Impl->Statement, "StoredProcedureBind");

					return false;
				}

				return true;
			}
		}
		else
		{
			// Prepare stored procedure
			if (StoredProcedurePrepare(StoredProcedure))
			{
				return true;
			}
		}
	}

	return false;
}

/////////////////////////////////////////////////////////////
/// Retrieves the result set of a stored procedure call
/////////////////////////////////////////////////////////////
void StoredProcedureRetrieve(struct StoredProcedureType * StoredProcedure)
{
	if (StoredProcedure && StoredProcedure->Impl && StoredProcedure->Impl->Statement)
	{
		do
		{
			// Retreive the number of fields in the set
			uinteger FieldSize = mysql_stmt_field_count(StoredProcedure->Impl->Statement);

			if (FieldSize > 0)
			{
				MYSQL_RES * ResultMetaData = mysql_stmt_result_metadata(StoredProcedure->Impl->Statement);

				if (ResultMetaData != NULL)
				{
					// Retreive
					MYSQL_FIELD * Fields = mysql_fetch_fields(ResultMetaData);

					if (Fields)
					{
						MYSQL_BIND * Result = (MYSQL_BIND *)MemoryAllocate(sizeof(MYSQL_BIND) * FieldSize);

						if (Result)
						{
							// Initialize argument list
							StoredProcedureArgumentList Arguments = StoredProcedureArgumentCreate(STORED_PROCEDURE_BY_VALUE, FieldSize);

							if (Arguments)
							{
								uinteger Parameter;

								// Initialize parameters
								StoredProcedureInitializeMySQLArgs(Result, FieldSize);

								// Set up and bind result set output buffers
								for (Parameter = 0; Parameter < FieldSize; ++Parameter)
								{
									// Set current parameter type from MySQL type
									enum StoredProcedureArgumentTypeId Type = StoredProcedureMySQLDataType(Fields[Parameter].type);

									// Set argument type
									StoredProcedureArgumentListSetType(Arguments, Parameter, Type);

									if (Type < STORED_PROCEDURE_COUNT)
									{
										// Get current value
										StoredProcedureArgumentDataValueType * Value = StoredProcedureArgumentGetValue(Arguments, Parameter, Type);

										// Initialize current value
										void * Buffer = StoredProcedureArgumentValueInitialize(Value, Type);

										if (Buffer)
										{
											// Bind result field
											Result[Parameter].buffer_type = Fields[Parameter].type;
											Result[Parameter].buffer = (void *)Buffer;
											Result[Parameter].buffer_length = StoredProcedureArgumentSizeOf(Type);
											Result[Parameter].length = StoredProcedureArgumenValueLength(Value, Type);
										}
									}
								}

								// Bind result set
								if (mysql_stmt_bind_result(StoredProcedure->Impl->Statement, Result))
								{
									// Show error if any
									StoredProcedureMySQLError(StoredProcedure->Impl->Statement, "StoredProcedureRetrieve");

									// Clear result set
									MemoryDeallocate(Result);

									return;
								}

								// Fetch results
								while (mysql_stmt_fetch(StoredProcedure->Impl->Statement) == 0)
								{
									// Copy argument from binding
									StoredProcedureArgumentList Copy = StoredProcedureArgumentCopy(Arguments);

									// Insert copy into result set
									VectorPushBack(StoredProcedure->Result, &Copy);
								}

								// Destroy arguments
								StoredProcedureArgumentDestroy(Arguments);
							}

							// Release result set parameters
							MemoryDeallocate(Result);
						}
					}

					// Release metadata
					mysql_free_result(ResultMetaData);
				}
			}

		} while (mysql_stmt_next_result(StoredProcedure->Impl->Statement) > 0);
	}
}

/////////////////////////////////////////////////////////////
/// Executes a stored procedure call to the database
/////////////////////////////////////////////////////////////
bool StoredProcedureCall(struct StoredProcedureType * StoredProcedure)
{
	if (StoredProcedure)
	{
		// Bind stored procedure parameters
		if (StoredProcedureBind(StoredProcedure))
		{

			// Execute stored procedure call
			if (mysql_stmt_execute(StoredProcedure->Impl->Statement))
			{
				// Show error if any
				StoredProcedureMySQLError(StoredProcedure->Impl->Statement, "StoredProcedureCall");

				return false;
			}

			// Retreive results
			StoredProcedureRetrieve(StoredProcedure);

			// Execute fetch callback
			StoredProcedureFetch(StoredProcedure);

			// Clear result set
			StoredProcedureClear(StoredProcedure);

			return true;
		}
	}

	return false;
}

/////////////////////////////////////////////////////////////
/// Release a stored procedure call
/////////////////////////////////////////////////////////////
void StoredProcedureRelease(struct StoredProcedureType * StoredProcedure)
{
	if (StoredProcedure)
	{
		// Destroy implementation stored procedure
		if (StoredProcedure->Impl)
		{
			if (StoredProcedure->Impl->Statement)
			{
				// Clear stored procedure
				mysql_stmt_close(StoredProcedure->Impl->Statement);
			}

			// Clear implementation argument list
			if (StoredProcedure->Impl->Parameters)
			{
				MemoryDeallocate(StoredProcedure->Impl->Parameters);
			}

			// Clear implementation
			MemoryDeallocate(StoredProcedure->Impl);
		}

		// Clear prototype argument list
		StoredProcedureArgumentDestroy(StoredProcedure->Prototype);

		// Clear result set
		StoredProcedureClear(StoredProcedure);

		// Destroy result set vector
		VectorDestroy(StoredProcedure->Result);

		// Reset stored procedure properties
		StoredProcedure->Fetch = NULL;
		StoredProcedure->Name = NULL;
	}
}
