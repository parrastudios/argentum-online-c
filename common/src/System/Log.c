/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <System/Log.h>
#include <System/Platform.h>
#include <System/IOHelper.h>
#include <Memory/General.h>

#include <string.h> // strcpy, strlen

////////////////////////////////////////////////////////////
/// Encapsulates the callback of on destroy event
////////////////////////////////////////////////////////////
void LogManagerDestroyEvent(struct LogManagerType * LogManager)
{
	if (LogManager && LogManager->First)
	{
		struct LogEntryNode * Iterator = LogManager->First;
		char * Buffer = NULL;
		uinteger BufferLen = 0;

		// Get the length of the buffer
		while (Iterator != NULL)
		{
			BufferLen += strlen(Iterator->Data.Time) + 1;			// ' '
			BufferLen += strlen(Iterator->Data.Title) + 1;			// ':'
			BufferLen += strlen(Iterator->Data.Description) + 1;	// '\n'

			Iterator = Iterator->Next;
		}

		// MemoryAllocate the buffer
		Buffer = (char *)MemoryAllocate(sizeof(char) * BufferLen);

		// Copy contents to buffer
		LogManagerToBuffer(LogManager, Buffer, BufferLen);

		// Raise callback
		LogManager->DestroyCallback(Buffer, BufferLen);

		// Destroy the buffer
		MemoryDeallocate(Buffer);
	}
}

////////////////////////////////////////////////////////////
/// Initialize the log manager
////////////////////////////////////////////////////////////
void LogManagerInitialize(struct LogManagerType * LogManager, LogWriteCallback WriteCallback, LogDestroyCallback DestroyCallback)
{
	if (LogManager)
	{
		// Initialize log list
		LogManager->WriteCallback = WriteCallback;
		LogManager->DestroyCallback = DestroyCallback;
		LogManager->First = LogManager->Last = NULL;
		LogManager->Count = 0;

		// Insert the first log
		LogManagerWrite(LogManager, "LogManager", "Log manager initialized");
	}
}

////////////////////////////////////////////////////////////
/// Destroy the log manager
////////////////////////////////////////////////////////////
void LogManagerClear(struct LogManagerType * LogManager)
{
	if (LogManager)
	{
		struct LogEntryNode * Iterator = LogManager->First;

		while (Iterator != NULL)
		{
			struct LogEntryNode * Next = Iterator->Next;

			// MemoryReallocate the data
			MemoryDeallocate(Iterator->Data.Time);
			MemoryDeallocate(Iterator->Data.Title);
			MemoryDeallocate(Iterator->Data.Description);

			// MemoryReallocate the node
			MemoryDeallocate(Iterator);

			// Skip to the next node
			Iterator = Next;
		}

		LogManager->First = LogManager->Last = NULL;

		LogManager->WriteCallback = NULL;

		if (LogManager->DestroyCallback != NULL)
		{
			// Raise destroy event to the callback
			LogManagerDestroyEvent(LogManager);

			// Set callback to null
			LogManager->DestroyCallback = NULL;
		}
	}
}

////////////////////////////////////////////////////////////
/// Insert a new error in the list of errors
////////////////////////////////////////////////////////////
void LogManagerWrite(struct LogManagerType * LogManager, char * Title, char * Description)
{
	if (LogManager && Title && Description)
	{
		struct LogEntryNode * Node = (struct LogEntryNode*)MemoryAllocate(sizeof(struct LogEntryNode));
		char TimeString[0xFF];

		// Get the time when log was generated
		sprintf(TimeString, "%f", SystemGetTime()); // todo: improve time format

		// MemoryAllocate the data
		Node->Data.Time = (char*)MemoryAllocate(sizeof(char) * strlen(TimeString));
		Node->Data.Title = (char*)MemoryAllocate(sizeof(char) * strlen(Title));
		Node->Data.Description = (char*)MemoryAllocate(sizeof(char) * strlen(Description));

		// Get log data
		strcpy(Node->Data.Time, TimeString);
		strcpy(Node->Data.Title, Title);
		strcpy(Node->Data.Description, Description);

		// Initialize node
		Node->Next = Node->Prev = NULL;

		// Insert into the list
		if (LogManager->First == NULL)
		{
			// List empty insert first
			LogManager->First = LogManager->Last = Node;
		}
		else
		{
			// Put the node at the end
			LogManager->Last->Next = Node;
			Node->Prev = LogManager->Last;
			LogManager->Last = Node;
		}

		// Increment the count of logs
		LogManager->Count++;

		// Callback
		if (LogManager->WriteCallback)
		{
			LogManager->WriteCallback(&Node->Data);
		}
	}
}

////////////////////////////////////////////////////////////
/// Retrieve the error in the given position
////////////////////////////////////////////////////////////
struct LogEntryType * LogManagerRead(struct LogManagerType * LogManager, uinteger Position)
{
	if (LogManager && LogManager->First && Position < LogManager->Count)
	{
		struct LogEntryNode * Iterator = LogManager->First;
		uinteger i;

		// Skip to the position in the list
		for (i = 0; i < Position && Iterator != NULL; i++, Iterator = Iterator->Next);
			
		return &Iterator->Data;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Delete the error in the given Position
////////////////////////////////////////////////////////////
void LogManagerDelete(struct LogManagerType * LogManager, uinteger Position)
{
	if (LogManager && LogManager->First && Position < LogManager->Count)
	{
		struct LogEntryNode * Iterator = LogManager->First;
		uinteger i;

		// Skip to the position in the list
		for (i = 0; i < Position && Iterator != NULL; i++, Iterator = Iterator->Next);

		if (Iterator)
		{
			struct LogEntryNode * Prev, * Next;

			Prev = Iterator->Prev;
			Next = Iterator->Next;

			// Remove from list
			if (Prev)
			{
				if (Next)
				{
					Prev->Next = Next;
					Next->Prev = Prev;
				}
				else
				{
					Prev->Next = NULL;
					LogManager->Last = Prev;
				}
			}
			else
			{
				if (Next)
				{
					Next->Prev = NULL;
					LogManager->First = Next;
				}
				else
				{
					LogManager->First = LogManager->Last = NULL;
				}
			}

			// MemoryReallocate the data
			MemoryDeallocate(Iterator->Data.Time);
			MemoryDeallocate(Iterator->Data.Title);
			MemoryDeallocate(Iterator->Data.Description);

			// Delete node
			MemoryDeallocate(Iterator);

			// Decrement the number of logs
			LogManager->Count--;
		}
	}
}

////////////////////////////////////////////////////////////
/// Converts the log to a buffer
////////////////////////////////////////////////////////////
void LogManagerToBuffer(struct LogManagerType * LogManager, char * Buffer, uinteger Size)
{
	if (LogManager && LogManager->First)
	{
		struct LogEntryNode * Iterator = LogManager->First;
		uinteger Count = 0;

		while (Iterator != NULL && Count < Size)
		{
			// Copy time
			strcpy(&Buffer[Count], Iterator->Data.Time);
			Count += strlen(Iterator->Data.Time);

			Buffer[Count++] = ' ';

			// Copy title
			strcpy(&Buffer[Count], Iterator->Data.Title);
			Count += strlen(Iterator->Data.Title);

			Buffer[Count++] = ':';

			// Copy description
			strcpy(&Buffer[Count], Iterator->Data.Description);
			Count += strlen(Iterator->Data.Description);

			Buffer[Count++] = '\n';

			Iterator = Iterator->Next;
		}
	}
}
