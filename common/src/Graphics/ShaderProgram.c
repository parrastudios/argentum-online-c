/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Graphics/ShaderProgram.h>
#include <Graphics/ShaderLoader.h>

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

bool ShaderProgramInitialize(struct ShaderProgramType * Program)
{
	if (Program)
	{
		Program->Id = DeviceGetExt()->glCreateProgram();

		if (Program->Id == 0)
		{
			// Handle error

			return false;
		}

		Program->ShaderList = ListNew(sizeof(struct ShaderType));

		return true;
	}

	return false;
}

void ShaderProgramDestroy(struct ShaderProgramType * Program)
{
	if (Program && Program->ShaderList)
	{
		ListIterator Iterator;

		ListForEach(Program->ShaderList, Iterator)
		{
			struct ShaderType * Shader = ListItDataPtr(Iterator, struct ShaderType);

			// Delete each shader
			ShaderDestroy(Shader);
		}

		if (Program->Id != 0)
		{
			// Delete program
			DeviceGetExt()->glDeleteProgram(Program->Id);

			Program->Id = 0;
		}

		// Destroy the shader list
		ListDestroy(Program->ShaderList);
	}
}

void ShaderProgramEnable(struct ShaderProgramType * Program)
{
	if (Program)
	{
		DeviceGetExt()->glUseProgram(Program->Id);
	}
}

void ShaderProgramDisable(struct ShaderProgramType * Program)
{
	if (Program)
	{
		DeviceGetExt()->glUseProgram(0);
	}
}

bool ShaderProgramAppend(struct ShaderProgramType * Program, GLenum Type, const char * Path)
{
	if (Program && Path)
	{
		struct ShaderType Shader;

		if (ShaderCreate(&Shader, Type, Path))
		{
			// Attach shader to the program
			DeviceGetExt()->glAttachShader(Program->Id, Shader.Id);

			// Create a new entry in the list
			ListPushBack(Program->ShaderList, &Shader);

			return true;
		}
		else
		{
			// Handle error
		}
	}

	return false;
}

bool ShaderProgramCheckError(struct ShaderProgramType * Program, GLenum State)
{
	if (Program)
	{
		GLint Result = 0;

		// Retreive the result of the operation
		DeviceGetExt()->glGetProgramiv(Program->Id, State, &Result);

		// Check for errors
		if (Result == 0)
		{
			GLchar Error[1024] = { 0 };

			DeviceGetExt()->glGetProgramInfoLog(Program->Id, sizeof(Error), NULL, Error);

			// Handle error

			return false;
		}

		return true;
	}

	return false;
}

bool ShaderProgramLink(struct ShaderProgramType * Program)
{
	if (Program && Program->Id != 0)
	{
		// Link the program
		DeviceGetExt()->glLinkProgram(Program->Id);

		// Check the result of the linking
		if (ShaderProgramCheckError(Program, GL_LINK_STATUS))
		{
			// Validate program
			DeviceGetExt()->glValidateProgram(Program->Id);

			// Check the result of the validation
			if (ShaderProgramCheckError(Program, GL_VALIDATE_STATUS))
			{
				ListIterator Iterator;

				// Delete intermediate shader objects
				ListForEach(Program->ShaderList, Iterator)
				{
					struct ShaderType * Shader = ListItDataPtr(Iterator, struct ShaderType);

					// Delete each shader
					ShaderDestroy(Shader);
				}

				// Clear the list
				ListClear(Program->ShaderList);

				// return GLCheckError();

				return true;
			}
		}
	}

	return false;
}

GLint ShaderProgramGetUniformLocation(struct ShaderProgramType * Program, const char * UniformName)
{
	if (Program && UniformName)
	{
		GLuint Location = DeviceGetExt()->glGetUniformLocation(Program->Id, UniformName);

		if (Location == GL_UNIFORM_LOCATION_INVALID)
		{
			// Handle error
		}

		return Location;
	}

	return GL_UNIFORM_LOCATION_INVALID;
}

GLint ShaderProgramGetParameter(struct ShaderProgramType * Program, GLint Parameter)
{
	if (Program)
	{
		GLint Result;

		DeviceGetExt()->glGetProgramiv(Program->Id, Parameter, &Result);

		return Result;
	}

	return GL_PARAMETER_INVALID;
}

void ShaderProgramBindTexture(struct ShaderProgramType * Program, const char * UniformName, GLint Index)
{
	if (Program && UniformName)
	{
		GLint Location = ShaderProgramGetUniformLocation(Program, UniformName);

		if (Location != GL_UNIFORM_LOCATION_INVALID)
		{
			// Bind texture
			DeviceGetExt()->glUniform1i(Location, Index);
		}
	}
}
