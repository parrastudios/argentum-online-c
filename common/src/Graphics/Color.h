/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_COLOR_H
#define GRAPHICS_COLOR_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Type.h>


////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////

struct Color4ub
{
	union
	{
		struct
		{
			uint8 r; ///< Red component
			uint8 g; ///< Green component
			uint8 b; ///< Blue component
			uint8 a; ///< Alpha (transparency) component
		};

		uint8 rgba[4];
	};
};

struct Color4f
{
	union
	{
		struct
		{
			float r;
			float g;
			float b;
			float a;
		};

		float rgba[4];
	};
};

////////////////////////////////////////////////////////////
/// Color4ub is an utility for manipulating
/// 32-bits RGBA colors
////////////////////////////////////////////////////////////

void Color4ubCopy(struct Color4ub * Dest, struct Color4ub * Source);

void Color4ubSet(struct Color4ub * Color, uint8 r, uint8 g, uint8 b, uint8 a);

struct Color4ub * Color4ubGet(uint8 r, uint8 g, uint8 b, uint8 a);

uint32 Color4ubPack(struct Color4ub * Color);

void Color4ubToFloat(struct Color4ub * Color, struct Color4f * Result);

////////////////////////////////////////////////////////////
/// Color4f is an utility for manipulating
/// floating RGBA colors
////////////////////////////////////////////////////////////

void Color4fCopy(struct Color4f * Dest, struct Color4f * Source);

void Color4fSet(struct Color4f * Color, float r, float g, float b, float a);

struct Color4f * Color4fGet(float r, float g, float b, float a);

uint32 Color4fPack(struct Color4f * Color);

void Color4fToByte(struct Color4f * Color, struct Color4ub * Result);

#endif // GRAPHICS_COLOR_H
