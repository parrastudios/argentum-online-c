/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef NETWORK_SOCKET_H
#define NETWORK_SOCKET_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Network/SocketHelper.h>

////////////////////////////////////////////////////////////
/// Socket wraps a socket using TCP protocol to
/// send data safely (but a bit slower)
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct SocketData {
	SocketType 	Type;		///< Socket descriptor
	char *		Adress;		///< Socket IP Adress
	bool 		IsBlocking;	///< Is the socket blocking or non-blocking ?
};

////////////////////////////////////////////////////////////
/// Connect to another computer on a specified port
///
/// \param Port :        Port to use for transfers (warning : ports < 1024 are reserved)
/// \param HostAddress : IP Address of the host to connect to
/// \param Timeout :     Maximum time to wait, in seconds (0 by default : no timeout) (this parameter is ignored for non-blocking sockets)
///
/// \return True if operation has been successful
///
////////////////////////////////////////////////////////////
enum SocketStatus SocketConnect(struct SocketData * Socket, uint16 Port, char * HostAddress, float Timeout);

////////////////////////////////////////////////////////////
/// Listen to a specified port for incoming data or connections
///
/// \param Port : Port to listen to
///
/// \return True if operation has been successful
///
////////////////////////////////////////////////////////////
bool SocketListenPort(struct SocketData * Socket, uint16 Port);

////////////////////////////////////////////////////////////
/// Wait for a connection (must be listening to a port).
/// This function will block if the socket is blocking
///
/// \param Connected : Socket containing the connection with the connected client
/// \param Address :   Pointer to an address to fill with client infos (NULL by default)
///
/// \return Status code
///
////////////////////////////////////////////////////////////
enum SocketStatus SocketAccept(struct SocketData * SocketListener, struct SocketData * Connected);

////////////////////////////////////////////////////////////
/// Send an array of bytes to the host (must be connected first)
///
/// \param Data : Pointer to the bytes to send
/// \param Size : Number of bytes to send
///
/// \return Status code
///
////////////////////////////////////////////////////////////
enum SocketStatus SocketSend(struct SocketData * Socket, uint8 * Data, uinteger Size);

////////////////////////////////////////////////////////////
/// Receive an array of bytes from the host (must be connected first).
/// This function will block if the socket is blocking
///
/// \param Data :         Pointer to a byte array to fill (make sure it is big enough)
/// \param MaxSize :      Maximum number of bytes to read
/// \param SizeReceived : Number of bytes received
///
/// \return Status code
///
////////////////////////////////////////////////////////////
enum SocketStatus SocketReceive(struct SocketData * Socket, uint8 * Data, uinteger MaxSize, uinteger * SizeReceived);

////////////////////////////////////////////////////////////
/// Close the socket
///
/// \return True if operation has been successful
///
////////////////////////////////////////////////////////////
bool SocketClose(struct SocketData * Socket);

////////////////////////////////////////////////////////////
/// Check if the socket is in a valid state ; this function
/// can be called any time to check if the socket is OK
///
/// \return True if the socket is valid
///
////////////////////////////////////////////////////////////
bool SocketIsValid(struct SocketData * Socket);

#endif // NETWORK_SOCKET_H
