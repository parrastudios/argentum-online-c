/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef	MATH_GEOMETRY_BOUNDINGVOLUME_QUAD_H
#define MATH_GEOMETRY_BOUNDINGVOLUME_QUAD_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Math/Geometry/Vector.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define BOUNDING_QUAD_OUTSIDE	0x00
#define BOUNDING_QUAD_INSIDE	0x01
#define BOUNDING_QUAD_OVERLAP	0x02

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////
struct BoundingQuad
{
	struct Vector2f Bounds[2];	///< AABQ bounds [min, max]
	struct Vector2f Center;		///< AABQ center position
};

////////////////////////////////////////////////////////////
/// Create a bounding quad
////////////////////////////////////////////////////////////
struct BoundingQuad * BoundingQuadCreate();

////////////////////////////////////////////////////////////
/// Destroy a bounding quad
////////////////////////////////////////////////////////////
void BoundingQuadDestroy(struct BoundingQuad * Quad);

////////////////////////////////////////////////////////////
/// Set the data of a bounding quad
////////////////////////////////////////////////////////////
void BoundingQuadSet(struct BoundingQuad * Quad, struct Vector2f * Min, struct Vector2f * Max);

////////////////////////////////////////////////////////////
/// Set the data of a bounding quad (in other form)
////////////////////////////////////////////////////////////
void BoundingQuadSetEx(struct BoundingQuad * Quad, float x0, float y0, float x1, float y1);

////////////////////////////////////////////////////////////
///  Get an inline quad object set to defined values
////////////////////////////////////////////////////////////
struct BoundingQuad * BoundingQuadGet(float x0, float y0, float x1, float y1);

////////////////////////////////////////////////////////////
/// Copy the data of a bounding quad
////////////////////////////////////////////////////////////
void BoundingQuadCopy(struct BoundingQuad * Dest, struct BoundingQuad * Src, bool Recompute);

////////////////////////////////////////////////////////////
/// Update the center of the bounding quad
////////////////////////////////////////////////////////////
void BoundingQuadUpdateCenter(struct BoundingQuad * Quad);

////////////////////////////////////////////////////////////
/// Create a bounding quad from a list of vertices
////////////////////////////////////////////////////////////
void BoundingQuadGenerate(struct BoundingQuad * Quad, struct Vector2f * VertList, uint32 Size);

////////////////////////////////////////////////////////////
/// Check if two bounding quad are colliding
////////////////////////////////////////////////////////////
uint32 BoundingQuadCollision(struct BoundingQuad * Quad, struct BoundingQuad * Other);

////////////////////////////////////////////////////////////
/// Check if a point or circle is contained on the aabq
////////////////////////////////////////////////////////////
bool BoundingQuadContains(struct BoundingQuad * Quad, float x, float y, float Radius);

////////////////////////////////////////////////////////////
/// Distance squared to a point from the quad (arvos algorithm)
////////////////////////////////////////////////////////////
float BoundingQuadDistanceSquared(struct BoundingQuad * Quad, struct Vector2f * Point);

////////////////////////////////////////////////////////////
/// Distance to a point from the quad
////////////////////////////////////////////////////////////
float BoundingQuadDistance(struct BoundingQuad * Quad, struct Vector2f * Point);

#endif // MATH_GEOMETRY_BOUNDINGVOLUME_QUAD_H