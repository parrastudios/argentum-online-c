/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2014 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SYSTEM_MEMORY_ALIGNMENT_H
#define SYSTEM_MEMORY_ALIGNMENT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Platform.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#if COMPILER_TYPE == COMPILER_MSVC

#	define SYSTEM_MEMORY_PACK_ENABLE(align)	__pragma(pack(push)) \
											__pragma(pack(align))
#	define SYSTEM_MEMORY_PACK_DISABLE()		__pragma(pack(pop))
#	define SYSTEM_MEMORY_PACK

#elif COMPILER_TYPE == COMPILER_GNUC || COMPILER_TYPE == COMPILER_MINGW

#	define SYSTEM_MEMORY_PACK_ENABLE(align)
#	define SYSTEM_MEMORY_PACK_DISABLE()
#	define SYSTEM_MEMORY_PACK				__attribute__((packed))

#endif

#define SYSTEM_MEMORY_ALIGN_SIZE(Size, Align) do { Size = (Size + (Align - 1)) & ~(Align - 1); } while (0)

// todo: add implementations for more compilers

#endif // SYSTEM_MEMORY_ALIGNMENT_H
