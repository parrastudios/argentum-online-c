/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <MetaData/MetaManager.h>
#include <MetaData/MetaManagerImpl.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Type definitions
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Member data
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Initialize metadata manager implementation singleton
////////////////////////////////////////////////////////////
bool MetadataManagerInitialize()
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	// Only to allow reinitialization if object is destroyed
	return MetadataManagerInitializeImpl(MetadataManagerImpl);
}
////////////////////////////////////////////////////////////
/// Create a value in the manager
////////////////////////////////////////////////////////////
MetadataValueType MetadataManagerValueCreate(MetadataValueNameType Name, MetadataType Type, MetadataValueCountType Count, void * Data)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	// Create the value
	MetadataValueType Value = MetadataValueCreate(Scope, Type, Count, Data);

	// Register into scope
	if (Value != METADATA_STORAGE_INDEX_INVALID)
	{
		if (MetadataScopeKeywordCreate(Scope, Name, Value, METADATA_STORAGE_RESOURCE_VALUE))
		{
			return Value;
		}
		else
		{
			MetadataValueDestroy(Scope, Value);
		}
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Bind a value in the manager
////////////////////////////////////////////////////////////
MetadataValueType MetadataManagerValueBind(MetadataValueNameType Name, MetadataType Type, MetadataValueCountType Count, void * Data)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	// Bind the value
	MetadataValueType Value = MetadataValueBind(Scope, Type, Count, Data);

	// Register into scope
	if (Value != METADATA_STORAGE_INDEX_INVALID)
	{
		if (MetadataScopeKeywordCreate(Scope, Name, Value, METADATA_STORAGE_RESOURCE_VALUE))
		{
			return Value;
		}
		else
		{
			MetadataValueDestroy(Scope, Value);
		}
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Create a value signature in the manager
////////////////////////////////////////////////////////////
MetadataValueType MetadataManagerValueSignature(MetadataValueNameType Name, MetadataType Type, MetadataValueCountType Count)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	// Bind the value
	MetadataValueType Value = MetadataValueSignature(Scope, Type, Count);

	// Register into scope
	if (Value != METADATA_STORAGE_INDEX_INVALID)
	{
		if (MetadataScopeKeywordCreate(Scope, Name, Value, METADATA_STORAGE_RESOURCE_VALUE))
		{
			return Value;
		}
		else
		{
			MetadataValueDestroy(Scope, Value);
		}
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Get a value from the manager by name
////////////////////////////////////////////////////////////
MetadataValueType MetadataManagerValueGet(MetadataValueNameType Name)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	// If is a correct value
	if (MetadataScopeKeywordGetType(Scope, Name) == METADATA_STORAGE_RESOURCE_VALUE)
	{
		MetadataValueType Value = MetadataScopeKeywordGetIndex(Scope, Name);

		// Return value index
		return Value;
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Check if a value in the manager is valid
////////////////////////////////////////////////////////////
bool MetadataManagerValueValid(MetadataValueType Value)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	return MetadataValueValid(Scope, Value);
}

////////////////////////////////////////////////////////////
/// Check if a value in the manager is abstract
////////////////////////////////////////////////////////////
bool MetadataManagerValueAbstract(MetadataValueType Value)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	return MetadataValueAbstract(Scope, Value);
}

////////////////////////////////////////////////////////////
/// Check if a value in the manager is an abstract builtin
////////////////////////////////////////////////////////////
bool MetadataManagerValueAbstractBuiltin(MetadataValueType Value)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	return MetadataValueAbstractBuiltin(Scope, Value);
}

////////////////////////////////////////////////////////////
/// Check if a value in the manager is an abstract array
////////////////////////////////////////////////////////////
bool MetadataManagerValueAbstractArray(MetadataValueType Value)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	return MetadataValueAbstractArray(Scope, Value);
}

////////////////////////////////////////////////////////////
/// Check if a value in the manager is instanced
////////////////////////////////////////////////////////////
bool MetadataManagerValueInstanced(MetadataValueType Value)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	return MetadataValueInstanced(Scope, Value);
}

////////////////////////////////////////////////////////////
/// Check if a value in the manager is builtin
////////////////////////////////////////////////////////////
bool MetadataManagerValueBuiltin(MetadataValueType Value)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	return MetadataValueBuiltin(Scope, Value);
}

////////////////////////////////////////////////////////////
/// Check if a value in the manager is an array
////////////////////////////////////////////////////////////
bool MetadataManagerValueArray(MetadataValueType Value)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	return MetadataValueArray(Scope, Value);
}

////////////////////////////////////////////////////////////
/// Get the type of a value in the manager
////////////////////////////////////////////////////////////
MetadataType MetadataManagerValueTypeOf(MetadataValueType Value)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	return MetadataValueTypeOf(Scope, Value);
}

////////////////////////////////////////////////////////////
/// Get the size of a value type in the manager
////////////////////////////////////////////////////////////
MetadataTypeSize MetadataManagerValueTypeSize(MetadataValueType Value)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	return MetadataValueTypeSize(Scope, Value);
}

////////////////////////////////////////////////////////////
/// Get the type count a value in the manager
////////////////////////////////////////////////////////////
MetadataValueCountType MetadataManagerValueCount(MetadataValueType Value)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	return MetadataValueCount(Scope, Value);
}

////////////////////////////////////////////////////////////
/// Get value impl as a defined type
////////////////////////////////////////////////////////////
#define MetadataValueModifierGetDecl(Type, Name, Id) \
	MetadataTypeDecl(Type) MetadataManagerValueGet ## Name(MetadataValueType Value) \
	{ \
		MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl(); \
		\
		MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl); \
		\
		MetadataTypeDeclPtr(Type) ValueDataPtr = MetadataTypeCastPtr(Type)MetadataValueGet(Scope, Value); \
		\
		return (*ValueDataPtr); \
	}

	// Define metadata type declaration
	METADATA_TYPE_DECL(MetadataValueModifierGetDecl)

#undef MetadataValueModifierGetDecl

////////////////////////////////////////////////////////////
/// Get value impl by index as a defined type
////////////////////////////////////////////////////////////
#define MetadataValueModifierGetAtDecl(Type, Name, Id) \
	MetadataTypeDecl(Type) MetadataManagerValueGetAt ## Name(MetadataValueType Value, MetadataValueCountType Count) \
	{ \
		MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl(); \
		\
		MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl); \
		\
		MetadataTypeDeclPtr(Type) ValueDataPtr = MetadataTypeCastPtr(Type)MetadataValueGet(Scope, Value); \
		\
		if (Count < MetadataManagerValueCount(Value)) \
		{ \
			return ValueDataPtr[Count]; \
		} \
		else \
		{ \
			return MetadataTypeNull(Type); \
		} \
	}

	// Define metadata type declaration
	METADATA_TYPE_DECL(MetadataValueModifierGetAtDecl)

#undef MetadataValueModifierGetAtDecl

////////////////////////////////////////////////////////////
/// Set value impl as a defined type
////////////////////////////////////////////////////////////
#define MetadataValueModifierSetDecl(Type, Name, Id) \
	void MetadataManagerValueSet ## Name(MetadataValueType Value, MetadataTypeDecl(Type) Data) \
	{ \
		MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl(); \
		\
		MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl); \
		\
		MetadataValueSet(Scope, Value, (void *)&Data); \
	}

	// Define metadata type declaration
	METADATA_TYPE_DECL(MetadataValueModifierSetDecl)

#undef MetadataValueModifierSetDecl

////////////////////////////////////////////////////////////
/// Set value impl by index as a defined type
////////////////////////////////////////////////////////////
#define MetadataValueModifierSetAtDecl(Type, Name, Id) \
	void MetadataManagerValueSetAt ## Name(MetadataValueType Value, MetadataValueCountType Count, MetadataTypeDecl(Type) Data) \
	{ \
		MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl(); \
		\
		MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl); \
		\
		MetadataTypeDeclPtr(Type) ValueDataPtr = MetadataTypeCastPtr(Type)MetadataValueGet(Scope, Value); \
		\
		if (MetadataValueArray(Scope, Value) && Count < MetadataValueCount(Scope, Value)) \
		{ \
			ValueDataPtr[Count] = Data; \
		} \
	}

	// Define metadata type declaration
	METADATA_TYPE_DECL(MetadataValueModifierSetAtDecl)

#undef MetadataValueModifierSetAtDecl

////////////////////////////////////////////////////////////
/// Destroy a value in the metadata manager
////////////////////////////////////////////////////////////
void MetadataManagerValueDestroy(MetadataValueType Value)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	// Get by index and type from the scope
	MetadataScopeKey Keyword = MetadataScopeKeywordGetName(Scope, Value, METADATA_STORAGE_RESOURCE_VALUE);

	if (Keyword)
	{
		// Destroy in the scope
		MetadataScopeKeywordDestroy(Scope, Keyword);
	}

	// Destroy value
	MetadataValueDestroy(Scope, Value);
}

////////////////////////////////////////////////////////////
/// Create a reference in the metadata manager
////////////////////////////////////////////////////////////
MetadataReferenceType MetadataManagerReferenceCreate(MetadataReferenceNameType Name, MetadataReferenceTypeInfo Type, MetadataStorageIndexType Index)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	// Create the reference
	MetadataReferenceType Reference = MetadataReferenceCreate(Scope, Type, Index);

	// Register into scope
	if (Reference != METADATA_STORAGE_INDEX_INVALID)
	{
		if (MetadataScopeKeywordCreate(Scope, Name, Reference, METADATA_STORAGE_RESOURCE_REFERENCE))
		{
			return Reference;
		}
		else
		{
			MetadataReferenceDestroy(Scope, Reference);
		}
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Create a reference signature in the metadata manager
////////////////////////////////////////////////////////////
MetadataReferenceType MetadataManagerReferenceSignature(MetadataReferenceNameType Name, MetadataReferenceTypeInfo Type)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	// Create the reference
	MetadataReferenceType Reference = MetadataReferenceSignature(Scope, Type);

	// Register into scope
	if (Reference != METADATA_STORAGE_INDEX_INVALID)
	{
		if (MetadataScopeKeywordCreate(Scope, Name, Reference, METADATA_STORAGE_RESOURCE_REFERENCE))
		{
			return Reference;
		}
		else
		{
			MetadataReferenceDestroy(Scope, Reference);
		}
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Get reference by name from the metadata manager
////////////////////////////////////////////////////////////
MetadataReferenceType MetadataManagerReferenceGet(MetadataReferenceNameType Name)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	// If is a correct reference
	if (MetadataScopeKeywordGetType(Scope, Name) == METADATA_STORAGE_RESOURCE_REFERENCE)
	{
		MetadataReferenceType Reference = MetadataScopeKeywordGetIndex(Scope, Name);

		// Return reference index
		return Reference;
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Set reference pointer in the metadata manager
////////////////////////////////////////////////////////////
void MetadataManagerReferenceSetPointer(MetadataReferenceType Reference, MetadataStorageIndexType Index)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	MetadataReferenceSetPointer(Scope, Reference, Index);
}

////////////////////////////////////////////////////////////
/// Get reference pointer from the metadata manager
////////////////////////////////////////////////////////////
MetadataStorageIndexType MetadataManagerReferenceGetPointer(MetadataReferenceType Reference)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	return MetadataReferenceGetPointer(Scope, Reference);
}

////////////////////////////////////////////////////////////
/// Get type which reference is pointing to in the metadata manager
////////////////////////////////////////////////////////////
MetadataReferenceTypeInfo MetadataManagerReferenceTypeOf(MetadataReferenceType Reference)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	return MetadataReferenceTypeOf(Scope, Reference);
}

////////////////////////////////////////////////////////////
/// Destroy a reference in the metadata manager
////////////////////////////////////////////////////////////
void MetadataManagerReferenceDestroy(MetadataReferenceType Reference)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	// Get by index and type from the scope
	MetadataScopeKey Keyword = MetadataScopeKeywordGetName(Scope, Reference, METADATA_STORAGE_RESOURCE_REFERENCE);

	if (Keyword)
	{
		// Destroy in the scope
		MetadataScopeKeywordDestroy(Scope, Keyword);
	}

	MetadataReferenceDestroy(Scope, Reference);
}

////////////////////////////////////////////////////////////
/// Create a signature given by resource signatures
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataManagerParameterListSignature(MetadataParameterListCountType Count, MetadataParameterSignatureList SignatureList)
{
	if (Count > 0 && Count < METADATA_PARAMETER_LIST_LENGTH)
	{
		MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

		MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

		// Return parameter list signature without naming it
		return MetadataParameterListBuildSignature(Scope, Count, SignatureList);
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Retreive a parameter by id from parameter list
////////////////////////////////////////////////////////////
MetadataStorageIndexType MetadataManagerParameterRetrieve(MetadataParameterListType ParameterList, MetadataParameterType Parameter)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	return MetadataParameterListGetParameterIndex(Scope, ParameterList, Parameter);
}

////////////////////////////////////////////////////////////
/// Create a function by given signature
////////////////////////////////////////////////////////////
MetadataFunctionType MetadataManagerFunctionCreate(MetadataFunctionNameType Name, MetadataParameterListType Signature)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	// Create the function
	MetadataFunctionType Function = MetadataFunctionCreate(Scope, Signature);

	// Register into scope
	if (Function != METADATA_STORAGE_INDEX_INVALID)
	{
		if (MetadataScopeKeywordCreate(Scope, Name, Function, METADATA_STORAGE_RESOURCE_FUNCTION))
		{
			return Function;
		}
		else
		{
			MetadataFunctionDestroy(Scope, Function);
		}
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Binds a function to an address and given signature
////////////////////////////////////////////////////////////
MetadataFunctionType MetadataManagerFunctionBind(MetadataFunctionNameType Name, MetadataParameterListType Signature, void * FunctionPtr)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	// Bind the function
	MetadataFunctionType Function = MetadataFunctionBind(Scope, Signature, FunctionPtr);

	// Register into scope
	if (Function != METADATA_STORAGE_INDEX_INVALID)
	{
		if (MetadataScopeKeywordCreate(Scope, Name, Function, METADATA_STORAGE_RESOURCE_FUNCTION))
		{
			return Function;
		}
		else
		{
			MetadataFunctionDestroy(Scope, Function);
		}
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Get function by name
////////////////////////////////////////////////////////////
MetadataFunctionType MetadataManagerFunctionGet(MetadataFunctionNameType Name)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	// If is a correct function
	if (MetadataScopeKeywordGetType(Scope, Name) == METADATA_STORAGE_RESOURCE_FUNCTION)
	{
		MetadataFunctionType Function = MetadataScopeKeywordGetIndex(Scope, Name);

		// Return function index
		return Function;
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Get function signature specification
////////////////////////////////////////////////////////////
MetadataParameterListType MetadataManagerFunctionGetSignature(MetadataFunctionType Function)
{
	if (Function != METADATA_STORAGE_INDEX_INVALID)
	{
		MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

		MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

		return MetadataFunctionGetSignature(Scope, Function);
	}

	return METADATA_STORAGE_INDEX_INVALID;
}

////////////////////////////////////////////////////////////
/// Execute a function passing dinamically the arguments
////////////////////////////////////////////////////////////
void MetadataManagerFunctionExecute(MetadataFunctionNameType Name, ...)
{
	// Retreive function associated to name
	MetadataFunctionType Function = MetadataManagerFunctionGet(Name);

	if (Function != METADATA_STORAGE_INDEX_INVALID)
	{
		// Retreive function signature
		MetadataParameterListType Signature = MetadataManagerFunctionGetSignature(Function);

		if (Signature != METADATA_STORAGE_INDEX_INVALID)
		{
			MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

			MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

			MetadataParameterListType ParameterList = METADATA_STORAGE_INDEX_INVALID;

			// Retreive the number of arguments
			MetadataParameterListCountType Count = MetadataParameterListSize(Scope, Signature);

			// Parse arguments
			if (Count > 0)
			{
				VariableArgumentList Arguments;

				// Initialize variable argument list
				va_start(Arguments.List, Name);

				// Create parameter list from arguments that fits signature
				// Note: Not sure if passig va_list by reference is valid on multiple platforms
				// using an intermediate structure by now (tested on x86)
				ParameterList = MetadataParameterListGenerate(Scope, Signature, &Arguments);

				va_end(Arguments.List);
			}

			// Make the call to metadata function stub
			MetadataFunctionExecute(Scope, Function, ParameterList);

			// Destroy parameter list
			if (ParameterList != METADATA_STORAGE_INDEX_INVALID)
			{
				MetadataParameterListDestroy(Scope, ParameterList);
			}
		}
	}
}

////////////////////////////////////////////////////////////
/// Destroy a function instance
////////////////////////////////////////////////////////////
void MetadataManagerFunctionDestroy(MetadataFunctionType Function)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataScopeType Scope = MetadataManagerScopeCurrentImpl(MetadataManagerImpl);

	// Get by index and type from the scope
	MetadataScopeKey Keyword = MetadataScopeKeywordGetName(Scope, Function, METADATA_STORAGE_RESOURCE_FUNCTION);

	if (Keyword)
	{
		// Destroy in the scope
		MetadataScopeKeywordDestroy(Scope, Keyword);
	}

	MetadataReferenceDestroy(Scope, Function);
}

////////////////////////////////////////////////////////////
/// Creates a new scope instance associated to a name
////////////////////////////////////////////////////////////
bool MetadataManagerScopeCreate(MetadataScopeName ScopeName, MetadataStorageName StorageName)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataStorageType Storage = MetadataManagerStorageGetImpl(MetadataManagerImpl, StorageName);

	if (Storage)
	{
		MetadataScopeType Scope = MetadataManagerScopeGetImpl(MetadataManagerImpl, ScopeName);

		if (Scope)
		{
			return true;
		}
		else
		{
			// Allocate scope
			Scope = MetadataScopeCreate(ScopeName, Storage);

			if (Scope)
			{
				// Insert into scope tree and reference to map
				return MetadataManagerScopeCreateImpl(MetadataManagerImpl, ScopeName, Scope, StorageName);
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Enable a defined scope by name in the manager
////////////////////////////////////////////////////////////
bool MetadataManagerScopeBegin(MetadataScopeName Name)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Disable a defined scope by name in the manager
////////////////////////////////////////////////////////////
bool MetadataManagerScopeEnd(MetadataScopeName Name)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Destroy a scope instance associated to a name
////////////////////////////////////////////////////////////
bool MetadataManagerScopeDestroy(MetadataScopeName Name)
{
	// todo

	return false;
}

////////////////////////////////////////////////////////////
/// Creates a new storage instance associated to a name
////////////////////////////////////////////////////////////
bool MetadataManagerStorageCreate(MetadataStorageName Name)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataStorageType Storage = MetadataStorageCreate();

	if (Storage)
	{
		// Create and assing to name key
		return MetadataManagerStorageCreateImpl(MetadataManagerImpl, Name, Storage);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Associates a scope to a storage
////////////////////////////////////////////////////////////
bool MetadataManagerStorageAttachScope(MetadataStorageName StorageName, MetadataScopeName ScopeName)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataStorageType Storage = MetadataManagerStorageGetImpl(MetadataManagerImpl, StorageName);

	MetadataScopeType Scope = MetadataManagerScopeGetImpl(MetadataManagerImpl, ScopeName);

	if (Storage && Scope)
	{
		// Attach to storage
		MetadataScopeSetStorage(Scope, Storage);

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Destroy a storage instance associated to a name
////////////////////////////////////////////////////////////
bool MetadataManagerStorageDestroy(MetadataStorageName Name)
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataStorageType Storage = MetadataManagerStorageGetImpl(MetadataManagerImpl, Name);

	if (Storage)
	{
		// Destroy storage
		MetadataStorageDestroy(Storage);

		// Destroy a storage associated to a key
		return MetadataManagerStorageDestroyImpl(MetadataManagerImpl, Name);
	}

	return false;
}

////////////////////////////////////////////////////////////
/// Destroy metadata manager implementation singleton
////////////////////////////////////////////////////////////
void MetadataManagerDestroy()
{
	MetadataManagerImplType MetadataManagerImpl = MetadataManagerGlobalInstanceImpl();

	MetadataManagerDestroyImpl(MetadataManagerImpl);
}

////////////////////////////////////////////////////////////
/// Get global metadata manager instance interface
////////////////////////////////////////////////////////////
MetadataManagerType MetadataManagerGlobalInstance()
{
	static struct MetadataManagerType GlobalMetadataManagerInstance =
	{
		// Type block
		TypeInit(SizeOf, &MetadataTypeGetSize),

		// Value block
		TypeInit(ValueCreateType, &MetadataManagerValueCreate),

		TypeInit(ValueBindType, &MetadataManagerValueBind),

		TypeInit(ValueSignatureType, &MetadataManagerValueSignature),

		TypeInit(ValueCopy, NULL),

		TypeInit(ValueGet, &MetadataManagerValueGet),

		TypeInit(ValueValid, &MetadataManagerValueValid),

		TypeInit(ValueAbstract, &MetadataManagerValueAbstract),

		TypeInit(ValueAbstractBuiltin, &MetadataManagerValueAbstractBuiltin),

		TypeInit(ValueAbstractArray, &MetadataManagerValueAbstractArray),

		TypeInit(ValueInstanced, &MetadataManagerValueInstanced),

		TypeInit(ValueBuiltin, &MetadataManagerValueBuiltin),

		TypeInit(ValueArray, &MetadataManagerValueArray),

		TypeInit(ValueTypeOf, &MetadataManagerValueTypeOf),

		TypeInit(ValueTypeSize, &MetadataManagerValueTypeSize),

		TypeInit(ValueCount, &MetadataManagerValueCount),

		#define MetadataDataModifierGetDeclImpl(Type, Name, Id) \
			TypeInit(ValueGet ## Name, &MetadataManagerValueGet ## Name),

			// Value getters
			METADATA_TYPE_DECL(MetadataDataModifierGetDeclImpl)

		#undef MetadataDataModifierGetDeclImpl

		#define MetadataDataModifierGetAtDeclImpl(Type, Name, Id) \
			TypeInit(ValueGetAt ## Name, &MetadataManagerValueGetAt ## Name),

			// Value array getters
			METADATA_TYPE_DECL(MetadataDataModifierGetAtDeclImpl)

		#undef MetadataDataModifierGetAtDeclImpl

		#define MetadataDataModifierSetDeclImpl(Type, Name, Id) \
			TypeInit(ValueSet ## Name, &MetadataManagerValueSet ## Name),

			// Value setters
			METADATA_TYPE_DECL(MetadataDataModifierSetDeclImpl)

		#undef MetadataDataModifierSetDeclImpl

		#define MetadataDataModifierSetAtDeclImpl(Type, Name, Id) \
			TypeInit(ValueSetAt ## Name, &MetadataManagerValueSetAt ## Name),

			// Value array setters
			METADATA_TYPE_DECL(MetadataDataModifierSetAtDeclImpl)

		#undef MetadataDataModifierSetAtDeclImpl

		TypeInit(ValueDestroy, &MetadataManagerValueDestroy),

		// Reference block
		TypeInit(ReferenceCreate, &MetadataManagerReferenceCreate),

		TypeInit(ReferenceSignature, &MetadataManagerReferenceSignature),

		TypeInit(ReferenceCopy, NULL),

		TypeInit(ReferenceGet, &MetadataManagerReferenceGet),

		TypeInit(ReferenceSetPointer, &MetadataManagerReferenceSetPointer),

		TypeInit(ReferenceGetPointer, &MetadataManagerReferenceGetPointer),

		TypeInit(ReferenceTypeOf, &MetadataManagerReferenceTypeOf),

		TypeInit(ReferenceDestroy, &MetadataManagerReferenceDestroy),

		// Parameter block
		TypeInit(ParameterSignature, &MetadataParameterSignatureInitialize),

		TypeInit(ParameterListSignature, &MetadataManagerParameterListSignature),

		TypeInit(ParameterRetrieve, &MetadataManagerParameterRetrieve),

		// Function block
		TypeInit(FunctionCreate, &MetadataManagerFunctionCreate),

		TypeInit(FunctionBind, &MetadataManagerFunctionBind),

		TypeInit(FunctionCopy, NULL),

		TypeInit(FunctionGet, &MetadataManagerFunctionGet),

		TypeInit(FunctionGetSignature, &MetadataManagerFunctionGetSignature),

		TypeInit(FunctionExecute, &MetadataManagerFunctionExecute),

		TypeInit(FunctionDestroy, &MetadataManagerFunctionDestroy),

		// Object block

		// Annotation block

		// Runtime block

		// Scope block
		TypeInit(ScopeCreate, &MetadataManagerScopeCreate),

		TypeInit(ScopeBegin, &MetadataManagerScopeBegin),

		TypeInit(ScopeEnd, &MetadataManagerScopeEnd),

		TypeInit(ScopeDestroy, &MetadataManagerScopeDestroy),

		// Storage block
		TypeInit(StorageCreate, &MetadataManagerStorageCreate),

		TypeInit(StorageAttachScope, &MetadataManagerStorageAttachScope),

		TypeInit(StorageDestroy, &MetadataManagerStorageDestroy),

		// General block
		TypeInit(Initialize, &MetadataManagerInitialize),

		TypeInit(Destroy, &MetadataManagerDestroy)
	};

	// Perform static implementation initialization if required
	MetadataManagerGlobalInstanceImpl();

	// Return the interface
	return &GlobalMetadataManagerInstance;
}
