/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Entity.h>
#include <World/EntitySound.h>

#include <Audio/SoundManager.h>

#include <System/IOHelper.h> // todo: refactor this

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define ENTITY_SOUND_PATH				"data/audio/%d.ogg"

////////////////////////////////////////////////////////////
// Method prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Play sound of component
////////////////////////////////////////////////////////////
void EntitySoundPlay(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Stop sound of component
////////////////////////////////////////////////////////////
void EntitySoundStop(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Set a new stream sound to the component
////////////////////////////////////////////////////////////
void EntitySoundStream(struct EntityType * Entity, uinteger Index);

////////////////////////////////////////////////////////////
/// Set loop property of sound component
////////////////////////////////////////////////////////////
void EntitySoundLoop(struct EntityType * Entity, bool Loop);

////////////////////////////////////////////////////////////
/// Set origin of sound component
////////////////////////////////////////////////////////////
void EntitySoundOrigin(struct EntityType * Entity, struct Vector3f * Position);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a sound component
////////////////////////////////////////////////////////////
struct EntityType * EntitySoundCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Sound = EntityComponentSound(Entity);
		struct ComponentSoundData * SoundData = ComponentDataSound(Sound);
		struct ComponentSoundType * SoundType = ComponentTypeSound(Sound);

		// Initialize sound stream
		SoundType->Stream(Entity, ENTITY_SOUND_INDEX_INVALID);

		// Set sound loop
		SoundType->Loop(Entity, false);

		// Set origin
		Vector3fSetNull(&SoundData->Origin);

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a sound component
////////////////////////////////////////////////////////////
void EntitySoundDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Sound = EntityComponentSound(Entity);
		struct ComponentSoundData * SoundData = ComponentDataSound(Sound);

		// Stop audio
		if (SoundData->Index != ENTITY_SOUND_INDEX_INVALID)
		{
			struct ComponentSoundType * SoundType = ComponentTypeSound(Sound);

			SoundType->Stop(Entity);
		}
	}
}

////////////////////////////////////////////////////////////
/// Return the entity sound implementation attributes
////////////////////////////////////////////////////////////
const struct ComponentSoundType * EntityComponentSoundAttr()
{
	static const struct ComponentSoundType EntityComponentSoundAttr = {
		TypeInit(Play, &EntitySoundPlay),
		TypeInit(Stop, &EntitySoundStop),
		TypeInit(Stream, &EntitySoundStream),
		TypeInit(Loop, &EntitySoundLoop),
		TypeInit(Origin, &EntitySoundOrigin)
	};

	return (const struct ComponentSoundType *)&EntityComponentSoundAttr;
}

////////////////////////////////////////////////////////////
/// Return the entity sound implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * EntityComponentSoundImpl()
{
	static const struct ComponentImplType EntityComponentSoundTypeImpl = {
		ExprInit(Base, TypeInit(Create, &EntitySoundCreate), TypeInit(Destroy, &EntitySoundDestroy)),
		TypeInit(Type, COMPONENT_TYPE_SOUND),
		UnionInit(Attributes, Sound, &EntityComponentSoundAttr)
	};

	return &EntityComponentSoundTypeImpl;
}

////////////////////////////////////////////////////////////
/// Play sound of component
////////////////////////////////////////////////////////////
void EntitySoundPlay(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Sound = EntityComponentSound(Entity);
		struct ComponentSoundData * SoundData = ComponentDataSound(Sound);

		if (SoundData->Index != ENTITY_SOUND_INDEX_INVALID)
		{
			SoundBufferPlay(SoundData->Path, SoundData->Loop, SoundData->Origin.x, SoundData->Origin.y, SoundData->Origin.z);
		}
	}
}

////////////////////////////////////////////////////////////
/// Stop sound of component
////////////////////////////////////////////////////////////
void EntitySoundStop(struct EntityType * Entity)
{
	if (Entity)
	{
		struct ComponentType * Sound = EntityComponentSound(Entity);
		struct ComponentSoundData * SoundData = ComponentDataSound(Sound);

		if (SoundData->Index != ENTITY_SOUND_INDEX_INVALID)
		{
			SoundBufferStopByFile(SoundData->Path);
		}
	}
}

////////////////////////////////////////////////////////////
/// Set a new stream sound to the component
////////////////////////////////////////////////////////////
void EntitySoundStream(struct EntityType * Entity, uinteger Index)
{
	if (Entity)
	{
		struct ComponentType * Sound = EntityComponentSound(Entity);
		struct ComponentSoundData * SoundData = ComponentDataSound(Sound);

		SoundData->Index = Index;

		if (Index != ENTITY_SOUND_INDEX_INVALID)
		{
			sprintf(SoundData->Path, ENTITY_SOUND_PATH, SoundData->Index);
		}
	}
}

////////////////////////////////////////////////////////////
/// Set loop property of sound component
////////////////////////////////////////////////////////////
void EntitySoundLoop(struct EntityType * Entity, bool Loop)
{
	if (Entity)
	{
		struct ComponentType * Sound = EntityComponentSound(Entity);
		struct ComponentSoundData * SoundData = ComponentDataSound(Sound);

		// Set loop
		SoundData->Loop = Loop;

		// ...

	}
}

////////////////////////////////////////////////////////////
/// Set origin of sound component
////////////////////////////////////////////////////////////
void EntitySoundOrigin(struct EntityType * Entity, struct Vector3f * Position)
{
	if (Entity)
	{
		struct ComponentType * Sound = EntityComponentSound(Entity);
		struct ComponentSoundData * SoundData = ComponentDataSound(Sound);

		// Set origin
		Vector3fCopy(&SoundData->Origin, Position);

		// ...

	}
}
