/////////////////////////////////////////////////////////////////////////////
//	Preprocessor Library by Parra Studios
//
//	Copyright (C) 2009 - 2015 Vicente Ferrer Garcia <vic798@gmail.com>
//
//  A cross-platform macro based library to give support
//	for preprocessor metaprogramming.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef PREPROCESSOR_COMPLEMENT_H
#define PREPROCESSOR_COMPLEMENT_H

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Portability/Compiler.h>

////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////
#define PREPROCESSOR_COMPL_IMPL_0 1
#define PREPROCESSOR_COMPL_IMPL_1 0

////////////////////////////////////////////////////////////
// Macros
////////////////////////////////////////////////////////////
#if COMPILER_TYPE == COMPILER_MSVC

#	define PREPROCESSOR_COMPL_IMPL_EXPAND(Expr) Expr

#	define PREPROCESSOR_COMPL_IMPL(Expr) PREPROCESSOR_COMPL_IMPL_EXPAND(PREPROCESSOR_COMPL_IMPL_ ## Expr)

#else

#	define PREPROCESSOR_COMPL_IMPL(Expr) PREPROCESSOR_COMPL_IMPL_ ## Expr

#endif

#if COMPILER_TYPE == COMPILER_MWERKS

#	define PREPROCESSOR_COMPL_TOKEN(Expr) PREPROCESSOR_COMPL_IMPL ## Expr

#	define PREPROCESSOR_COMPL(Expr) PREPROCESSOR_COMPL_TOKEN((Expr))

#else

#	define PREPROCESSOR_COMPL(Expr) PREPROCESSOR_COMPL_IMPL(Expr)

#endif

#endif // PREPROCESSOR_COMPLEMENT_H
