/////////////////////////////////////////////////////////////////////////////
//  Argentum Online C by Parra Studios
//
//  A cross-platform mmorpg which keeps the original essence of
//  Argentum Online created by Pablo Ignacio Marquez (Gulfas Morgolock),
//  as combats, magics, guilds, although it has new implementations such as
//  3D graphics engine, new gameplay, and a better performance, among others.
//
//  Copyright (C) 2009-2015 Vicente Ferrer Garcia (Parra) - vic798@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <World/Point.h>

////////////////////////////////////////////////////////////
// Member prototypes
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Create a collision shapere component of a point
////////////////////////////////////////////////////////////
struct EntityType * PointCollisionCreate(struct EntityType * Entity);

////////////////////////////////////////////////////////////
/// Destroy a point collision sphere component
////////////////////////////////////////////////////////////
void PointCollisionDestroy(struct EntityType * Entity);

////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
/// Return the point collision implementation
////////////////////////////////////////////////////////////
const struct ComponentImplType * PointComponentCollisionImpl()
{
	static const struct ComponentImplType PointComponentCollisionSphereTypeImpl = {
		ExprInit(Base, TypeInit(Create, &PointCollisionCreate), TypeInit(Destroy, &PointCollisionDestroy)),
		TypeInit(Type, COMPONENT_TYPE_COLLISION),
		UnionInit(Attributes, Collision, &EntityComponentCollisionSphereAttr)
	};

	return &PointComponentCollisionSphereTypeImpl;
}

////////////////////////////////////////////////////////////
/// Create a collision sphere component of a point
////////////////////////////////////////////////////////////
struct EntityType * PointCollisionCreate(struct EntityType * Entity)
{
	if (Entity)
	{
		// Create transform component data reference
        struct ComponentType * Transform = EntityComponentTransform(Entity);
        struct ComponentTransformData * TransformData = ComponentDataTransform(Transform);

		// Create collision component data reference
        struct ComponentType * Collision = EntityComponentCollisionSphere(Entity);
		struct ComponentCollisionSphereType * CollisionType = ComponentTypeCollisionSphere(Collision);

		// Set default bounding sphere size and position
		CollisionType->SetBounds(Entity, &TransformData->Position, POINT_COLLISION_RADIUS_DEFAULT);

		return Entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////
/// Destroy a point collision sphere component
////////////////////////////////////////////////////////////
void PointCollisionDestroy(struct EntityType * Entity)
{
	if (Entity)
	{
		// Create collision component data reference
		struct ComponentType * Collision = EntityComponentCollisionSphere(Entity);
		struct ComponentCollisionSphereData * CollisionData = ComponentDataCollisionSphere(Collision);

		// ..
	}
}

////////////////////////////////////////////////////////////
/// Return the point collision data size
////////////////////////////////////////////////////////////
uinteger PointComponentCollisionSize()
{
    return COMPONENT_DATA_LENGTH_DEFAULT;
}
